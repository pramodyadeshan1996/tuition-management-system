
<?php
include('../connect/connect.php');

    $teach_id = '8'; 
    $style_video_btn = '';

      $sql0011 = mysqli_query($conn,"SELECT * FROM `teacher_details` WHERE `TEACH_ID` = '$teach_id' "); //check Classes for registered teachers details
      while($row0011 = mysqli_fetch_assoc($sql0011))
      {

          $teacher_position = $row0011['POSITION']; //Teacher Position
          $teacher_f_name = $row0011['F_NAME']; //Teacher First Name
          $teacher_l_name = $row0011['L_NAME']; //Teacher Last Name
          $teacher_qualification = $row0011['QUALIFICATION']; //Teacher Qualification
          $intro_video = $row0011['INTRO_VIDEO']; //Teacher video

          if($intro_video == '0')
          {
            $style_video_btn = 'style="opacity:0.6;pointer-events:none;"';
          }

          $teacher_picture = $row0011['PICTURE']; //Teacher Picture
          $teacher_tp = $row0011['TP']; //Teacher Picture

          $teacher_full_name = $teacher_position.". ".$teacher_f_name." ".$teacher_l_name;


          //If Teacher Picture zero,Check gender and gender to select picture 
          if($teacher_picture == '0')
          {
              if($teacher_gender == 'Male')
              {
                  $teacher_picture = 'b_teacher.jpg';
              }else
              if($teacher_gender == 'Female')
              {
                  $teacher_picture = 'g_teacher.jpg';
              }
          }
          //If Teacher Picture zero,Check gender and gender to select picture 


      }


    ?>



<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from trimatrixlab.com/store/flatrica/index-dark.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 09 Jun 2021 15:38:25 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="author" content="Trimatrix Lab">
    <meta name="description" content="">
    <meta name="keywords" content="">


    <title>SIPSATHA.LK | Help Page</title>
    
    <link rel="icon" type="image/png" href="../../admin/images/institute/sipsatha.png">

    <!--APPLE TOUCH ICON-->
    <link rel="apple-touch-icon" href="images/site/apple-touch-icon.png">


    <!-- GOOGLE FONT -->
    <link href='https://fonts.googleapis.com/css?family=Raleway:500' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Muli' rel='stylesheet' type='text/css'>


    <!-- MATERIAL ICON FONT -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- FONT AWESOME -->
    <link href="stylesheets/vendors/font-awesome.min.css" rel="stylesheet">


    <!-- ANIMATION -->
    <link href="stylesheets/vendors/animate.min.css" rel="stylesheet">

    <!-- MAGNIFICENT POPUP -->
    <link href="stylesheets/vendors/magnific-popup.css" rel="stylesheet">

    <!-- SWIPER -->
    <link href="stylesheets/vendors/swiper.min.css" rel="stylesheet">


    <!-- MATERIALIZE -->
    <link href="stylesheets/vendors/materialize.css" rel="stylesheet">
    <!-- BOOTSTRAP -->
    <link href="stylesheets/vendors/bootstrap.min.css" rel="stylesheet">

    <!-- CUSTOM STYLE -->
    <link href="stylesheets/style_dark_dark.css" id="switch_style" rel="stylesheet">

</head>
<body>


<!--==========================================
                  PRE-LOADER
===========================================-->
<div id="loading">
    <div id="loading-center">
        <div id="loading-center-absolute">
            <div class="box-holder animated bounceInDown">
                <span class="load-box"><span class="box-inner"></span></span>
            </div>
            <!-- NAME & STATUS -->
            <div class="text-holder text-center">
                <h2>Sipsatha.lk</h2>
                <h6 style="margin-top:10px;">Developed By IBS Developer Team</h6>
            </div>
        </div>
    </div>
</div>

<!--==========================================
                    HEADER
===========================================-->
<header id="home">
    

    <!--HEADER BACKGROUND-->
    <div class="header-background section">
        
        
        
        

        


        

    </div>

</header>


<!--==========================================
                   V-CARD
===========================================-->
<div id="v-card-holder" class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">

                <!-- V-CARD -->
                <div id="v-card" class="card" style="height: 400px">

                    <!-- PROFILE PICTURE -->
                    <div id="profile" class="right">
                        <img alt="profile-image" class="img-responsive" src="../teacher/images/profile/<?php echo $teacher_picture; ?>">
                        <div class="slant"></div>

                        <!--EMPTY PLUS BUTTON-->
                        <!--<div class="btn-floating btn-large add-btn"><i class="material-icons">add</i></div>-->

                        <!--VIDEO PLAY BUTTON-->
                        <div id="button-holder" class="btn-holder" <?php echo $style_video_btn; ?>>
                            <div id="play-btn" class="btn-floating btn-large btn-play">
                                <i id="icon-play" class="material-icons">play_arrow</i>
                            </div>
                        </div>
                    </div>
                    <!--VIDEO CLOSE BUTTON-->
                    <div id="close-btn" class="btn-floating icon-close">
                        <i class="fa fa-close"></i>
                    </div>

                    <div class="card-content">
                        <!-- CONTACT INFO -->
                        <div class="infos" style="margin-top: 10px;"><!-- NAME & STATUS -->
                        <div class="info-headings" style="padding-top: 10px;">
                            <h5 class="text-uppercase left" style="font-size:20px;color:white;"><?php echo $teacher_full_name; ?></h5>
                            <h6 class="text-capitalize left" style="font-size:12px;text-align: left;"><?php echo $teacher_qualification; ?></h6>

                            <div class="card-content" style="padding-left:0px;">
                                <!-- ABOUT PARAGRAPH -->
                                <p style="text-align:left;color:white;font-weight: 200;">
                                    <br>
                                    කුසල් අකලංක සර් විසින් මෙහෙයවනු ලබන<br> 11 ශ්‍රේණිය සිංහල පන්තිය අද <br>සවස 4.00 - 6.00 දක්වා පැවැත්වේ.<br>ඒ සඳහා සහභාහී වන දරුවන් පහත Link <br>එක භාවිතා කර sipsatha.lk වෙබ් අඩවිය<br> හරහා පංතියට සහභාගී වන්න.
                                    <br>
                                    <a href="https://www.sipsatha.lk/app/student/login/index.php">https://www.sipsatha.lk/app/student/login/</a>
                                </p>
                            </div>

                        </div>

                        </div>

                    </div>
                    <!--HTML 5 VIDEO-->

                    <?php 

                        $shortUrlRegex = '/youtu.be\/([a-zA-Z0-9_-]+)\??/i';
                        $longUrlRegex = '/youtube.com\/((?:embed)|(?:watch))((?:\?v\=)|(?:\/))([a-zA-Z0-9_-]+)/i';

                        if (preg_match($longUrlRegex, $intro_video, $matches)) {
                            
                            $youtube_id = $matches[count($matches) - 1];
                        }

                        if (preg_match($shortUrlRegex, $intro_video, $matches)) {
                            
                            $youtube_id = $matches[count($matches) - 1];
                        }
                        $emb_link = 'https://www.youtube.com/embed/' . $youtube_id ;

                    ?>

                    <iframe id="html-video" class="video" oncontextmenu="return false;" scrolling="yes" src="<?php echo $emb_link; ?>?autoplay=0&enablejsapi=1&rel=0&modestbranding=1&showsearch=0&amp;wmode=transparent&enablejsapi=1&mode=opaque&rel=0&autohide=1&showinfo=0" style="widows: 100%;height: 100%;" allowfullscreen sandbox="allow-forms allow-scripts allow-pointer-lock allow-same-origin allow-top-navigation" ></iframe>

                    <script type="text/javascript">document.getElementsByTagName('iframe')[0].contentWindow.getElementsByClassName('ytp-watch-later-button')[0].style.display = 'none';</script>

                    <script type="text/javascript">
                        new YouTubeToHtml5();
                    </script> 



                </div>
            </div>
        </div>
    </div>
</div>


<!--==========================================
             PRICING TABLE
===========================================-->
<section id="pricing-table" class="section">
    <div class="container">
        <!--SECTION TITLE-->
        <div class="section-title">
            <h4 class="text-uppercase text-center"><img src="images/icons/lightning.png" alt="demo">පන්ති සදහා සහභාගී වන ආකාරය</h4>
        </div>
        <!--PRICING TABLES-->
        <div id="pricing-card" class="row">
            <!--PRICING ONE-->
            <div id="p-one" class="col-md-4 col-sm-4 col-xs-12">
                <div class="pricing">
                    <div class="card">
                        <!--PRICING TOP-->
                        <div class="pricing-top" style="background-color:white;">
                            <span class="pricing-top" style="color:white;border:none">පළමු පියවර</span>
                            <br>
                            <img src="images/login001.png" class="image-responsive" style="width:100%;height: auto;margin-top: 20px;">
                        </div>
                        <!--PRICING DETAILS-->
                        <div class="pricing-bottom text-center text-capitalize" style="padding:10px 10px 10px 10px;color: white;">
                            <ul>
                                <li>

                                    ඉහත Login Screen එකේදී ඔබගේ Register ID ( ලියාපදිංචි අංකය ) සහ Password ( මුරපදය ) ලබාදී "ගිණුමට පිවිසීම" යන්න තෝරා ගන්න. ඉන්පසු ඔබට ඔබගේ ගිණුම වෙත ප්‍රවේශ විය හැකිය.

                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <!--PRICING TWO-->
            <div id="p-three" class="col-md-4 col-sm-4 col-xs-12">
                <div class="pricing">
                    <div class="card">
                        <!--PRICING TOP-->
                        <div class="pricing-top" style="background-color:white;">
                            <span class="pricing-top" style="color:white;border:none">දෙවන පියවර</span>
                            <br>
                            <img src="images/profile.jpeg" class="image-responsive" style="width:100%;height: auto;margin-top: 20px;margin-bottom: 22px;">
                        </div>
                        <!--PRICING DETAILS-->
                        <div class="pricing-bottom text-center text-capitalize" style="padding:10px 10px 10px 10px;color: white;">
                            <ul>
                                <li>

                                    දැන් Join Class තෝරා ගැනීමෙන් ඔබට පංතිය සඳහා සහභාහී විය හැකිය.

                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>


            <!--PRICING TWO-->
            <div id="p-two" class="col-md-4 col-sm-4 col-xs-12">
                <div class="pricing">
                    <div class="card">
                        <!--PRICING TOP-->
                        <div class="pricing-top" style="background-color:white;padding-bottom: 45px;">
                            <span class="pricing-top" style="color:white;border:none">විමසීම</span>
                            <br>
                            <img src="images/support.jpg" class="image-responsive" style="width:100%;height: auto;margin-top: 20px;margin-bottom: 12px;">
                            <div class="col-md-12 text-center" style="">
                                <i class="fa fa-whatsapp" style="font-size:36px;color: #16a085;"></i>
                                <i class="fa fa-phone text-danger" style="font-size:36px; color: #e74c3c;"></i>
                            </div>
                        </div>
                        <!--PRICING DETAILS-->
                        <div class="pricing-bottom text-center text-capitalize" style="padding:10px 10px 10px 10px;color: white;">
                            <ul>
                                <li>

                                    ඔබට මේ පිළිබඳ යම්කිසි ගැටලුවක් ඇත්නම් 0777 650 121 අංකයට WhatsApp පණිවිඩයක් ලබාදෙන්න

                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<script>

document.getElementById('link_btn').style.pointerEvents = 'none';
document.getElementById('link_btn').style.opacity = '0.4';

// Set the date we're counting down to
var countDownDate = new Date("<?php echo $set_time; ?>").getTime();

// Update the count down every 1 second
var x = setInterval(function() {

  // Get today's date and time
  var now = new Date().getTime();

  // Find the distance between now and the count down date
  var distance = countDownDate - now;

  // Time calculations for days, hours, minutes and seconds
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);

  // Display the result in the element with id="demo"
  document.getElementById("count_time").innerHTML = days + "d " + hours + "h "
  + minutes + "m " + seconds + "s ";

  if(days == '0' && hours == '0' && minutes < 30)
  {
    //document.getElementById("count_time").innerHTML = "සම්මන්ත්‍රණය සදහා සම්බන්ධවන්න.";

    document.getElementById("my_msg").style.display = "none";
    document.getElementById('link_btn').style.pointerEvents = 'auto';
    document.getElementById('link_btn').style.opacity = '6';
  }

  // If the count down is finished, write some text
  if (distance < 0) {
    clearInterval(x);
    document.getElementById("count_time").innerHTML = "සම්මන්ත්‍රණය ආරම්භ වී ඇත.";

    document.getElementById("my_msg").style.display = "none";
    document.getElementById('link_btn').style.pointerEvents = 'auto';
    document.getElementById('link_btn').style.opacity = '6';
    
  }
}, 1000);
</script>



<footer style="margin-top:10px;">
    <div class="container">
        <!--FOOTER DETAILS-->
        <p class="text-center">
            © All right reserved by
            <a href="https://www.ibslanka.com" target="_blank" style="cursor:pointer;">
                <strong>Interlink Business Solutions</strong>
            </a>
        </p>
    </div>
</footer>


<!--==========================================
                  SCRIPTS
===========================================-->
<script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="javascript/vendors/jquery-2.1.3.min.js"></script>
<script src="javascript/vendors/bootstrap.min.js"></script>
<script src="javascript/vendors/materialize.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBI14J_PNWVd-m0gnUBkjmhoQyNyd7nllA"></script>
<script src="javascript/vendors/markerwithlabel.min.js"></script>
<script src="javascript/vendors/retina.min.js"></script>
<script src="javascript/vendors/scrollreveal.min.js"></script>
<script src="javascript/vendors/swiper.jquery.min.js"></script>
<script src="javascript/vendors/jquery.magnific-popup.min.js"></script>
<script src="javascript/custom-dark.js"></script>


</body>

<!-- Mirrored from trimatrixlab.com/store/flatrica/index-dark.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 09 Jun 2021 15:40:26 GMT -->
</html>


<div id="logout" class="modal fade" role="dialog" style="margin-top: 120px;">
  <div class="modal-dialog"  style="padding: 10px 10px 10px 10px;">

    <!-- Modal content-->
    <div class="modal-content" style="padding: 10px 10px 10px 10px;">
      <div class="modal-header" style="border:none;">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
          <center><span class="fa fa-power-off" style="font-size: 60px;margin-bottom: 10px;color:white; "></span></center>

        <h3 style="color:white;"><center>පිටවීම</center></h3>

        <h5> 
            <center><label style="color:white;">ඔබගේ ගිණුමෙන් පිටවීමට අවශ්‍යද?</label></center>
        </h5>
      </div>
          <div class="row pb-10">
              <div class="col-md-12">
                  <a href="../logout.php" class="btn btn-default btn-block" style="border-radius: 0px;color: white;"><i class="fa fa-check-circle"></i> ඔව්</a>
              </div>

              <div class="col-md-12" style="margin-top:6px;">
                  <button class="btn btn-default btn-block" data-dismiss="modal" style="border-radius: 0px;background-color: #8e8386;color: white"><span class="fa fa-times-circle"></span> නැත</button>
              </div>
          </div>
    </div>

  </div>
</div>