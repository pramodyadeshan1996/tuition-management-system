
    <div id="register">   
        <div class="row">
            <div class="col-md-12">
                <div class="section-title">
                    <h5 class="mt-2">Add-On Payment</h5>
                </div>
            </div>
            
        </div>
        
    <form action="../student/query/insert.php" method="POST" enctype="multipart/form-data">
        <div class="row mb-4">
            <div class="col-md-5">

                <label>Payment Month</label>
                    <div class="row">
                        <div class="col-md-6">

                            <?php   
                                $str2 = strtotime('+1 Year'); 
                                $nxt_year = date('Y',$str2);

                             ?>
                            
                            <select class="form-control mt-2" id="fees_year" name="fees_year" onchange="check_fees_date();">
                                <option value="<?php echo date('Y'); ?>"><?php echo date('Y'); ?></option>
                                <option value="<?php echo $nxt_year; ?>"><?php echo $nxt_year; ?></option>
                            </select>

                        </div>
                        <div class="col-md-6">

                            <?php   
                                $i = date('m');
                                $str = strtotime('+1 month'); 
                                $i2 = date('m',$str);


                                $str2 = strtotime('+2 month'); 
                                $i3 = date('m',$str2);

                                $months = array (1=>'ජනවාරි',2=>'පෙබරවාරි',3=>'මාර්තු',4=>'අප්‍රේල්',5=>'මැයි',6=>'ජුනි',7=>'ජුලි',8=>'අගෝස්තු',9=>'සැප්තැම්බර්',10=>'ඔක්තෝම්බර්',11=>'නොවැම්බර්',12=>'දෙසැම්බර්');

                                $this_month_name = $months[(int)$i];
                                $next_month_name = $months[(int)$i2];
                                $two_month_name = $months[(int)$i3];

                             ?>

                            <select class="form-control mt-2" id="fees_month" name="fees_month" onchange="check_fees_date();">
                                <option value="<?php echo date('m') ?>"><?php echo $this_month_name; ?></option>
                                <option value="<?php $str = strtotime('+1 month'); echo date('m',$str) ?>"><?php echo $next_month_name; ?></option>
                                <option value="<?php $str = strtotime('+2 month'); echo date('m',$str) ?>"><?php echo $two_month_name; ?></option>
                            </select>

                        </div>
                    </div>
                    
            </div>
            <div class="col-md-4"></div>
            <div class="col-md-3 text-center mt-4" id="total_fees">
              <?php echo '<small class="text-muted pt-10">Total Payment</small><h4 style="font-weight: bold;">LKR '.number_format(0,2).'</h4>'; ?>
                
            </div>
        </div>



      <script type="text/javascript">
        function check_fees_date()
        {

              var fees_year = document.getElementById('fees_year').value;
              var fees_month = document.getElementById('fees_month').value;

              //alert(subject)
              $.ajax({  
              url:"../student/query/check.php",  
              method:"POST",  
              data:{check_ym:fees_year,month:fees_month},  
              success:function(data){ 
                 
                //alert(data)
              
                document.getElementById('show_data').innerHTML=data;
                
                 
               }           
             });

        }
     </script>
        

    
        <div class="table-responsive" style="overflow:auto;height: 300px;border-bottom: 1px solid #cccc;margin-bottom: 6px;">
            <table class="table">
                <thead>
                    <th>#</th>
                    <th>Class</th>
                </thead>
                <tbody id="show_data">
                    <?php 
                        $this_year = date('Y');
                        $this_month = date('m');

                        $sql001 = mysqli_query($conn,"SELECT * FROM `transactions` WHERE `STU_ID` = '$stu_id'");
                        $no_reg = mysqli_num_rows($sql001);

                        if($no_reg > 0)
                        {

                            while($row001 = mysqli_fetch_assoc($sql001))
                            {
                                $teacher_id = $row001['TEACH_ID'];
                                $class_id = $row001['CLASS_ID'];

                                $sql0016 = mysqli_query($conn,"SELECT * FROM `classes` WHERE `CLASS_ID` = '$class_id'"); //check Classes for registered teachers details
                                while($row0016 = mysqli_fetch_assoc($sql0016))
                                {
                                    $student_subject_id002 = $row0016['SUB_ID'];
                                    $class_name = $row0016['CLASS_NAME'];
                                    $class_fees = $row0016['FEES'];
                                }

                                /* Subject Details */

                                $sql0015 = mysqli_query($conn,"SELECT * FROM `subject` WHERE `SUB_ID` = '$student_subject_id002' "); //Institute Data assign to variables
                                while($row0015 = mysqli_fetch_assoc($sql0015))
                                {
                                    $subject_name = $row0015['SUBJECT_NAME']; //Subject Name

                                    $level_id = $row0015['LEVEL_ID'];

                                    $sql0017 = mysqli_query($conn,"SELECT * FROM level WHERE LEVEL_ID = '$level_id'");
                                    while($row0017=mysqli_fetch_assoc($sql0017))
                                    {
                                      $level_name = $row0017['LEVEL_NAME'];
                                    }

                                }

                                /* Subject Details */

                                 $sql0011 = mysqli_query($conn,"SELECT * FROM `teacher_details` WHERE `TEACH_ID` = '$teacher_id' "); //check Classes for registered teachers details
                                while($row0011 = mysqli_fetch_assoc($sql0011))
                                {

                                    $teacher_position = $row0011['POSITION']; //Teacher Position
                                    $teacher_f_name = $row0011['F_NAME']; //Teacher First Name
                                    $teacher_l_name = $row0011['L_NAME']; //Teacher Last Name

                                    $teacher_full_name = $teacher_position.". ".$teacher_f_name." ".$teacher_l_name;

                                }

                                $disable_ch = '';
                                $msg = '';

                                $sql002 = mysqli_query($conn,"SELECT * FROM `payment_data` WHERE `YEAR` = '$this_year' AND `MONTH` = '$this_month' AND `CLASS_ID` = '$class_id' AND `STU_ID` = '$stu_id'");
                                $check_data = mysqli_num_rows($sql002);
                                if($check_data > 0)
                                {

                                    while($row002 = mysqli_fetch_assoc($sql002))
                                    {
                                        $status001 = $row002['ADMIN_SUBMIT'];

                                        if($status001 == '0')
                                        {
                                            //Pending
                                            $disable_ch = 'disabled';
                                            $msg = '<label class="badge badge-warning badge-lg" style="padding: 4px 4px 4px 4px;font-size: 12px;"><span class="fa fa-spinner"></span> Payment Pending</label>';
                                        }

                                        if($status001 == '1')
                                        {
                                            //Approved
                                            $disable_ch = 'disabled';
                                            $msg = '<label class="badge badge-success badge-lg" style="padding: 4px 4px 4px 4px;font-size: 12px;"><span class="fa fa-check-circle"></span> Paid</label>';
                                        }

                                        if($status001 == '2')
                                        {
                                            //Reject
                                            $disable_ch = '';
                                            $msg = '<label class="badge badge-primary badge-lg" style="padding: 4px 4px 4px 4px;font-size: 12px;"><span class="fa fa-times-circle"></span> Payment Rejected</label>';

                                        }
                                    }
                                }else
                                if($check_data == '0')
                                {

                                    $disable_ch = '';
                                    $msg = '<label class="badge badge-danger badge-lg" style="padding: 4px 4px 4px 4px;font-size: 12px;"><span class="fa fa-times"></span> Not Paid</label>';
                                }
                                
                                    echo '

                                         <tr style="border-bottom:1px solid #cccc;">
                                            <td>';?>

                                                <input type="checkbox" <?php echo $disable_ch; ?> style="zoom:1.6;margin-top: 20px;" name="clz_id[]" id="clz_id" class="cls" value="<?php echo $class_id ?>" onclick="document.getElementById('fees_ch<?php echo $class_id; ?>').click();">

                                                  <!-- Hidden -->
                                                  <input type="checkbox" name="fees_ch" id="fees_ch<?php echo $class_id; ?>" style="display: none" value="<?php echo $class_fees; ?>" onclick="calculate();">
                                                  
                                            <?php echo '
                                            </td>
                                            <td>
                                                <h5>'.$class_name.'
                                                   <br> <small>'.$subject_name.' ('.$level_name.')</small>
                                                </h5>
                                                <small>'.$teacher_full_name.'</small>
                                                <h6>LKR '.number_format($class_fees,2).' '.$msg.'</h6>
                                            </td>
                                        </tr>

                                      ';

                                
                            }
                        }else
                        if($no_reg == '0')
                        {
                            echo '<tr><td colspan="2" class="text-center text-danger"><span class="fa fa-warning"></span> Not Found Data!</td></tr>';
                        }

                    ?>
                   
                </tbody>
            </table>
        </div>

        <script type="text/javascript">

            /*Realtime Calculate course Fees*/

              function calculate() {
              const inputs = document.querySelectorAll("input[name=fees_ch]");
              let result = 0;
              
              for (let i = 0; i < inputs.length; i++) {
                if (inputs[i].checked) {
                  result += Number(inputs[i].value);
                }
              }
              
              document.getElementById('total_fees').innerHTML = '<small class="text-muted pt-10">Total Payment</small><h4 style="font-weight: bold;">LKR '+result.toFixed(2)+'</h4>';
            }
            /*Realtime Calculate course Fees*/
        </script>

        <div class="row mb-4">
            <div class="col-md-12">
                <input type="file" id="slip_file" name="slip_file[]"  style="display: none;" onchange="upload_addon_slip()"/>
                <div class="col-md-12 mt-2" style="padding: 0px;" class="pt-10" id="show_btn" onclick="document.getElementById('slip_file').click();">
                    <button type="button" class="btn btn-block btn-sm" name="create_payment" id="uploaded_btn" style="padding: 20px 20px 20px 20px;border:3px dashed gray;font-size: 18px;outline: none;background-color: white;color:<?php echo $ins_bg_color; ?>;" value="Browse..."> <i class="fa fa-plus"></i> <label style="font-weight: bold;">Upload Bank Slip</label>
                    </button>

                </div>
            </div>
        </div>

        <div class="row mb-4">
            <div class="col-md-4"></div>
            <div class="col-md-4"></div>
            <div class="col-md-4 mt-2">
                <button type="submit" class="btn btn-success btn-block btn-sm" id="btnUpload" name="adddon_payment" style="cursor: pointer;">
                            
                    <label style="font-size: 18px;cursor: pointer;"><span class="fas fa-check"></span> Submit</label>

                </button>
            </div>
        </div>

    </form>
    </div>

    <script type="text/javascript">
                    

                    function upload_addon_slip()
                    {
                        
                        var slip_file = document.getElementById('slip_file').value;
                        

                        if(slip_file == '')
                        {
                          $("#show_btn").html('<button type="submit" class="btn btn-block btn-sm" name="create_payment" id="uploaded_btn" style="padding: 20px 20px 20px 20px;border:3px dashed gray;font-size: 18px;outline: none;background-color: white;" value="Browse..."> <i class="fa fa-plus"></i> <label style="font-weight: 1000px;">Upload Bank Slip</label></button>');
                          $('#slip_upload_btn').prop('disabled',true);

                        }else
                        if(slip_file !== '')
                        {

                            $("#show_btn").html('<button type="button" class="btn btn-block btn-sm" id="uploaded_btn" style="padding: 20px 20px 20px 20px;font-size: 18px;outline: none;background-color: #ff6500;color:white;" value="Browse..." > <i class="fa fa-spinner fa-pulse"></i> <label style="font-weight: 1000px;">Changing..</label></button>');

                            setTimeout(function() { 

                                $("#show_btn").html('<button type="submit" class="btn btn-block btn-sm" name="create_payment" id="uploaded_btn" style="padding: 20px 20px 20px 20px;font-size: 18px;outline: none;background-color: <?php echo $ins_bg_color; ?>;color:white;" value="Browse..." > <i class="fa fa-check-circle"></i> <label style="font-weight: 1000px;">Upload successfully</label></button>');
                                $('#slip_upload_btn').prop('disabled',false);

                            }, 1000);
                          

                          
                        }

                        
                    }

                  </script>

   