
<?php

$page = "Setup Links";
$folder_in = '0';

  include('header/header.php'); 
 //ini_set( "display_errors", 0); 

  ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<div class="row" style="border-bottom: 1px solid #cccc;margin-top: 55px;">
  <div class="col-md-12">

    <ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-bottom: 04%;margin-bottom: 0;">
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="Payment"> Setup Links</li>
    </ol>
  </div>
</div>




<div class="col-md-12" style="margin-top: 0;">
  <h3 style="text-transform: capitalize;"><a href="dashboard.php" data-toggle="tooltip" data-title="Back"><i class="pg-icon" style="font-size: 22px;">chevron_left</i></a> Setup Links </h3>


<div class=" container-fluid   container-fixed-lg bg-white">
<div class="card card-transparent">
<div class="card-header ">
<div class="card-title">Here are all the all classes link you have made so far.
  
</div>
<div class="pull-right">
<div class="col-xs-12">
<input type="text" id="myInput" class="form-control pull-right" placeholder="Search" data-toggle="tooltip" data-title="අවශ්‍ය දත්ත සෙවීම'">
</div>
</div>
<div class="clearfix"></div>
</div>

<div class="card-body table-responsive" style="height: 800px;overflow: auto;margin-bottom: 0;">
<table class="table demo-table-search table-responsive-block text-left table-striped" id="tableWithSearch">
<thead>

  <th>Subject Name</th>
  <th>Class Name</th>
  <th style="text-align: center;">Setup Link</th>

</thead>
<tbody id="myTable">
  <tr class="no-data col-md-12 alert alert-danger" style="margin-top: 20px;display: none;text-align: center;">
    <td colspan="3" class="text-danger"><span class="fa fa-warning"></span> Not Found Data</td>
  </tr>
<?php 

if(isset($_GET['teach_id']) || !empty($_SESSION['search_teacher']))
{
    if(!empty($_GET['teach_id']) ||  empty($_SESSION['search_teacher']))
    {
      $teacher_id001 = $_GET['teach_id'];
      $_SESSION['search_teacher'] = $teacher_id001;
    }else
    if(empty($_GET['teach_id']) ||  !empty($_SESSION['search_teacher']))
    {
      $teacher_id001 = $_SESSION['search_teacher'];
    }
    
    $sql001 = mysqli_query($conn,"SELECT * FROM `classes` WHERE `TEACH_ID` = '$teacher_id001'");
    $ch = mysqli_num_rows($sql001);

    if($ch > 0 )
    {

    while($row001 = mysqli_fetch_assoc($sql001))
    {
      $class_id = $row001['CLASS_ID'];
      $start = $row001['START_TIME'];
      $end = $row001['END_TIME'];

      $str = strtotime($start);
      $start_time = date('h:i A',$str);

      $str2 = strtotime($end);
      $end_time = date('h:i A',$str2);



      $day = $row001['DAY'];
      $clz_name = $row001['CLASS_NAME'];
      $sub_id = $row001['SUB_ID'];

      $z = $row001['ZOOM_LINK'];
      $m = $row001['MS_LINK'];
      $g = $row001['GOOGLE_LINK'];
      $j = $row001['JITSI_LINK'];
      $w = $row001['WHATSAPP_LINK'];
      $t = $row001['TELEGRAM_LINK'];


      if($z == '0')
      {
        $z = 'N/A';
        $zbtn= '<i class="pg-icon">close</i>';
        $ztooltip1 = '<a data-toggle="tooltip" data-title="Not available Link">';
        $ztooltip2 = '</a>';
        
      }else
      if($z !== '0')
      {
        $zbtn= '<i class="pg-icon">tick_circle</i>';
        $ztooltip1 = '<a data-toggle="tooltip" data-title="Available Link">';
        $ztooltip2 = '</a>';
      }

      if($m == '0')
      {
        $m = 'N/A';
        $mbtn= '<i class="pg-icon">close</i>';
        $mtooltip1 = '<a data-toggle="tooltip" data-title="Not available Link">';
        $mtooltip2 = '</a>';
        
      }else
      if($m !== '0')
      {
        $mbtn= '<i class="pg-icon">tick_circle</i>';
        $mtooltip1 = '<a data-toggle="tooltip" data-title="Available Link">';
        $mtooltip2 = '</a>';
      }

      if($g == '0')
      {
        $g = 'N/A';
        $gbtn= '<i class="pg-icon">close</i>';
        $gtooltip1 = '<a data-toggle="tooltip" data-title="Not available Link">';
        $gtooltip2 = '</a>';
        
      }else
      if($g !== '0')
      {
        $gbtn= '<i class="pg-icon">tick_circle</i>';
        $gtooltip1 = '<a data-toggle="tooltip" data-title="Available Link">';
        $gtooltip2 = '</a>';
      }

      if($j == '0')
      {
        $j = 'N/A';
        $jbtn= '<i class="pg-icon">close</i>';
        $jtooltip1 = '<a data-toggle="tooltip" data-title="Not available Link">';
        $jtooltip2 = '</a>';
        
      }else
      if($j !== '0')
      {
        $jbtn= '<i class="pg-icon">tick_circle</i>';
        $jtooltip1 = '<a data-toggle="tooltip" data-title="Available Link">';
        $jtooltip2 = '</a>';
      }

      if($w == '0' || $w == '')
      {
        $w = 'N/A';
        $wbtn= '<i class="pg-icon">close</i>';
        $wtooltip1 = '<a data-toggle="tooltip" data-title="Not available Link">';
        $wtooltip2 = '</a>';
        
      }else
      if($w !== '0' || $w !== '')
      {
        $wbtn= '<i class="pg-icon">tick_circle</i>';
        $wtooltip1 = '<a data-toggle="tooltip" data-title="Available Link">';
        $wtooltip2 = '</a>';
      }

      if($t == '0' || $t == '')
      {
        $t = 'N/A';
        $tbtn= '<i class="pg-icon">close</i>';
        $ttooltip1 = '<a data-toggle="tooltip" data-title="Not available Link">';
        $ttooltip2 = '</a>';
        
      }else
      if($t !== '0' || $t !== '')
      {
        $tbtn= '<i class="pg-icon">tick_circle</i>';
        $ttooltip1 = '<a data-toggle="tooltip" data-title="Available Link">';
        $ttooltip2 = '</a>';
      }

      $sql002 = mysqli_query($conn,"SELECT * FROM `subject` WHERE `SUB_ID` = '$sub_id'");
      while($row002 = mysqli_fetch_assoc($sql002))
      {
        $sub_name = $row002['SUBJECT_NAME'];
      }

      echo '<tr>
    <td class="v-align-middle">'.$sub_name.'</td>
    <td class="v-align-middle">'.$clz_name.'<br><b class="text-danger">'.$start_time.' - '.$end_time.'</b><br><label class="label label-success" style="text-transform:capitalize;">'.$day.'</label></td>

    <td class="v-align-middle">
      
              '.$ztooltip1.'<button class="btn btn-success btn-rounded" style="margin-bottom: 4px;padding: 8px 8px;text-align:left;" data-target="#zoom'.$class_id.'" data-toggle="modal">'.$zbtn.' Zoom Link</button>'.$ztooltip2.'

                '.$gtooltip1.'<button class="btn btn-success btn-rounded " style="margin-bottom: 4px;padding: 8px 8px;text-align:left;" data-target="#google'.$class_id.'" data-toggle="modal">'.$gbtn.' Google Meet Link</button>'.$gtooltip2.'

                '.$jtooltip1.'<button class="btn btn-success btn-rounded " style="margin-bottom: 4px;padding: 8px 8px;text-align:left;" data-target="#jitsi'.$class_id.'" data-toggle="modal">'.$jbtn.' Jitsi Meet Link</button>'.$jtooltip2.'

                '.$mtooltip1.'<button class="btn btn-success btn-rounded" style="margin-bottom: 4px;padding: 8px 8px;text-align:left;" data-target="#ms'.$class_id.'" data-toggle="modal">'.$mbtn.' Microsoft Team Link</button>'.$mtooltip2.'
                
                '.$wtooltip1.'<button class="btn btn-success btn-rounded" style="margin-bottom: 4px;padding: 8px 8px;text-align:left;" data-target="#wa'.$class_id.'" data-toggle="modal">'.$wbtn.' Whatsapp Link</button>'.$wtooltip2.'

                '.$ttooltip1.'<button class="btn btn-success btn-rounded" style="margin-bottom: 4px;padding: 8px 8px;text-align:left;" data-target="#tg'.$class_id.'" data-toggle="modal">'.$tbtn.' Telegram Link</button>'.$ttooltip2.'
    </td>
  </tr>


                        <div class="modal fade slide-up disable-scroll" id="zoom'.$class_id.'" tabindex="-1" role="dialog" aria-hidden="false">
                        <div class="modal-dialog ">
                        <div class="modal-content-wrapper">
                        <div class="modal-content">
                        <div class="modal-header clearfix text-left">
                        <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
                        </button>
                        <h5>Edit Zoom Link</h5>
                        </div>
                        <div class="modal-body">
                          <form action="../teacher/query/update.php" method="POST">
                          <input type="hidden" name="teach_id" value="'.$teacher_id001.'">
                          <div class="form-group form-group-default" style="margin-top: 10px;">
                            <label>Zoom Link</label>
                            <textarea name="zoom_link" required placeholder="XXXXXXXX" style="margin-bottom: 10px;height: 100px;" class="form-control change_inputs" >'.$z.'</textarea>
                          </div>

                          <button type="submit" class="btn btn-success btn-block btn-lg" name="update_zoom_link" value="'.$class_id.'"><i class="pg-icon">tick_circle</i> Update</button>
                          </form>
                          </div>
                          </div>
                          </div>
                          </div>

                          </div>
                        </div>



                        <div class="modal fade slide-up disable-scroll" id="google'.$class_id.'" tabindex="-1" role="dialog" aria-hidden="false">
                        <div class="modal-dialog ">
                        <div class="modal-content-wrapper">
                        <div class="modal-content">
                        <div class="modal-header clearfix text-left">
                        <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
                        </button>
                        <h5>Edit Google Meet Link</h5>
                        </div>
                        <div class="modal-body">
                          <form action="../teacher/query/update.php" method="POST">
                          <input type="hidden" name="teach_id" value="'.$teacher_id001.'">
                          <div class="form-group form-group-default" style="margin-top: 10px;">
                            <label>Google Meet Link</label>
                            <textarea name="google_link" required placeholder="XXXXXXXX" style="margin-bottom: 10px;height: 100px;" class="form-control change_inputs">'.$g.'</textarea>
                          </div>

                          <button type="submit" class="btn btn-success btn-block btn-lg" name="update_google_link" value="'.$class_id.'"><i class="pg-icon">tick_circle</i> Update</button>
                          </form>
                          </div>
                          </div>
                          </div>
                          </div>

                          </div>
                        </div>


                        <div class="modal fade slide-up disable-scroll" id="jitsi'.$class_id.'" tabindex="-1" role="dialog" aria-hidden="false">
                        <div class="modal-dialog ">
                        <div class="modal-content-wrapper">
                        <div class="modal-content">
                        <div class="modal-header clearfix text-left">
                        <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
                        </button>
                        <h5>Edit Jitsi Link</h5>
                        </div>
                        <div class="modal-body">
                          <form action="../teacher/query/update.php" method="POST">
                          <input type="hidden" name="teach_id" value="'.$teacher_id001.'">
                          <div class="form-group form-group-default" style="margin-top: 10px;">
                            <label>Jitsi Link</label>

                            <textarea name="jitsi_link" required placeholder="XXXXXXXX" style="margin-bottom: 10px;height: 100px;" class="form-control change_inputs">'.$j.'</textarea>
                          </div>

                          <button type="submit" class="btn btn-success btn-block btn-lg" name="update_jitsi_link" value="'.$class_id.'"><i class="pg-icon">tick_circle</i> Update</button>
                          </form>
                          </div>
                          </div>
                          </div>
                          </div>

                          </div>
                        </div>


                        <div class="modal fade slide-up disable-scroll" id="ms'.$class_id.'" tabindex="-1" role="dialog" aria-hidden="false">
                        <div class="modal-dialog ">
                        <div class="modal-content-wrapper">
                        <div class="modal-content">
                        <div class="modal-header clearfix text-left">
                        <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
                        </button>
                        <h5>Edit Microsoft Team Link</h5>
                        </div>
                        <div class="modal-body">
                          <form action="../teacher/query/update.php" method="POST">
                          <input type="hidden" name="teach_id" value="'.$teacher_id001.'">
                          <div class="form-group form-group-default" style="margin-top: 10px;">
                            <label>Microsoft Team Link</label>
                            <textarea name="ms_link" required placeholder="XXXXXXXX" style="margin-bottom: 10px;height: 100px;" class="form-control change_inputs">'.$m.'</textarea>
                          </div>

                          <button type="submit" class="btn btn-success btn-block btn-lg" name="update_ms_link" value="'.$class_id.'"><i class="pg-icon">tick_circle</i> Update</button>
                          </form>
                          </div>
                          </div>
                          </div>
                          </div>

                          </div>
                        </div>

                        <div class="modal fade slide-up disable-scroll" id="wa'.$class_id.'" tabindex="-1" role="dialog" aria-hidden="false">
                        <div class="modal-dialog ">
                        <div class="modal-content-wrapper">
                        <div class="modal-content">
                        <div class="modal-header clearfix text-left">
                        <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
                        </button>
                        <h5>Edit Whatsapp Link</h5>
                        </div>
                        <div class="modal-body">
                          <form action="../teacher/query/update.php" method="POST">
                          <input type="hidden" name="teach_id" value="'.$teacher_id001.'">
                          <div class="form-group form-group-default" style="margin-top: 10px;">
                            <label>Whatsapp Link</label>
                            <textarea name="wa_link" required placeholder="XXXXXXXX" style="margin-bottom: 10px;height: 100px;" class="form-control change_inputs">'.$w.'</textarea>
                          </div>

                          <button type="submit" class="btn btn-success btn-block btn-lg" name="update_wa_link" value="'.$class_id.'"><i class="pg-icon">tick_circle</i> Update</button>
                          </form>
                          </div>
                          </div>
                          </div>
                          </div>

                          </div>
                        </div>


                        <div class="modal fade slide-up disable-scroll" id="tg'.$class_id.'" tabindex="-1" role="dialog" aria-hidden="false">
                        <div class="modal-dialog ">
                        <div class="modal-content-wrapper">
                        <div class="modal-content">
                        <div class="modal-header clearfix text-left">
                        <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
                        </button>
                        <h5>Edit Telegram Link</h5>
                        </div>
                        <div class="modal-body">
                          <form action="../teacher/query/update.php" method="POST">
                          <input type="hidden" name="teach_id" value="'.$teacher_id001.'">
                          <div class="form-group form-group-default" style="margin-top: 10px;">
                            <label>Telegram Link</label>
                            <textarea name="tg_link" required placeholder="XXXXXXXX" style="margin-bottom: 10px;height: 100px;" class="form-control change_inputs">'.$t.'</textarea>
                          </div>

                          <button type="submit" class="btn btn-success btn-block btn-lg" name="update_tg_link" value="'.$class_id.'"><i class="pg-icon">tick_circle</i> Update</button>
                          </form>
                          </div>
                          </div>
                          </div>
                          </div>

                          </div>
                        </div>

                        ';



    }
  }else
  if($ch == '0')
  {
    echo '<tr><td colspan="3" class="text-danger text-center"><span class="fa fa-warning text-danger"></span> Empty Data!</td></tr>';
  }
}else
{
    echo '<tr><td colspan="3" class="text-danger text-center"><span class="fa fa-warning text-danger"></span> Empty Data!</td></tr>';

}

 ?>

</tbody>
</table>
</div>
</div>

</div>
</div>

</div>

</div>

<script type="text/javascript">
  setInterval(function() {
    $('blink').fadeIn(1300).fadeOut(1500);
}, 1000);
</script>

<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

<script type="text/javascript">
  $(document).ready(function () {

    (function ($) {

        $('#myInput').keyup(function () {
            var rex = new RegExp($(this).val(), 'i');
            $('#myTable tr').hide();
            $('#myTable tr').filter(function () {
                return rex.test($(this).text());
            }).show();
            $('.no-data').hide();
            if($('#myTable tr:visible').length == 0)
            {
                $('.no-data').show();
            }

        })

    }(jQuery));

});
</script>


<script>
$(document).ready(function(){

  $("#up_rec").on("change", function() {
      $('.show_image').hide();
  });
});
</script>
<script type="text/javascript">
  setInterval(function() {
    $('.blink').fadeIn(1300).fadeOut(1500);
}, 1000);
</script>

<?php  include('footer/footer.php'); ?>


