<?php 
    session_start();
    include '../../connect/connect.php';
    //$today = date('Y-m-d h:i:s A');
  	//$sql001 = mysqli_query($conn,"UPDATE `classes` SET `FREE` = '0' WHERE `END_DATE` < '$today' ");

 ?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Login</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link href="https://fonts.googleapis.com/css?family=Muli&display=swap" rel="stylesheet">

  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

  <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<style type="text/css">


button {
    --c: goldenrod;
    color: var(--c);
    font-size: 16px;
    border: 0.3em solid var(--c);
    border-radius: 0.5em;
    text-transform: uppercase;
    font-weight: bold;
    font-family: sans-serif;
    letter-spacing: 0.1em;
    text-align: center;
    position: relative;
    overflow: hidden;
    z-index: 1;
    transition: 0.5s;
    margin: 0em;
}

button span {
    position: absolute;
    width: 25%;
    height: 100%;
    background-color: var(--c);
    transform: translateY(150%);
    border-radius: 50%;
    left: calc((var(--n) - 1) * 25%);
    transition: 0.9s;
    transition-delay: calc((var(--n) - 1) * 0.1s);
    z-index: -1;
}

button:hover {
    color: black;
}

button:hover span {
    transform: translateY(0) scale(2);
}

button span:nth-child(1) {
    --n: 1;
}

button span:nth-child(2) {
    --n: 2;
}

button span:nth-child(3) {
    --n: 3;
}

button span:nth-child(4) {
    --n: 4;
}



</style>


</head>

<body class="bg-gradient-primary"  style="font-family: 'Muli', sans-serif;background: #ff00cc;  /* fallback for old browsers */
background: -webkit-linear-gradient(to right, #333399, #ff00cc);  /* Chrome 10-25, Safari 5.1-6 */
background: linear-gradient(to right, #333399, #ff00cc); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
">
  <div class="container">
    <div class="col-md-12">
    <!-- Outer Row -->
    <div class="row justify-content-center">
     <div class="col-xl-4 col-lg-4 col-md-4"></div>

      <div class="col-md-4" style="border:1px solid #00000000;margin-top: 10%;background-color: #0000007d;padding: 30px 40px 30px 40px;border-radius: 4px;">

                  <div class="text-center">
                    <h1 style="color: white;">Welcome!</h1>
                  </div>
                    <br>

                    <div class="form-group">

                      <?php 

                        if(!empty($_GET['reg_id']))
                        {
                          $registered_id = $_GET['reg_id'];
                        }else
                        if(empty($_GET['reg_id']))
                        {
                          $registered_id = '';
                        }
                       ?>

                      <input type="text" class="form-control form-control-user" aria-describedby="emailHelp" placeholder="Enter Register ID..." name="username" style="padding-left: 24px;height:50px;border-radius: 50px;" id="user" required autocomplete="off" autofocus="on" value="<?php echo $registered_id; ?>">
                    </div>
                    <div class="form-group" id="show_hide_password" style="position: relative;">
                      <input type="password" class="form-control form-control-user" placeholder="Enter Password" name="password" style="padding-left: 24px;height:50px;border-radius: 50px;" id="psw" required autocomplete="off">

                          <a href=""><i class="fa fa-eye-slash" aria-hidden="true" style="font-size: 18px;color: black;position: absolute;right:18px;top:15px;"></i></a>
                    </div>
                    
                    <button type="submit" class="btn btn-info btn-active btn-user btn-block" name="submit" id="log" style="margin-top: 40px;height:50px;border-radius: 50px;font-size: 16px;background: rgb(38,158,245);background: linear-gradient(191deg, rgba(38,158,245,1) 47%, rgba(54,163,245,1) 87%);font-weight: bold;outline: none;border:rgb(218 165 32);"><span></span><span></span><span></span><span></span> 
                      Login
                    </button>

                <div id="result" style="margin-top: 10px;"></div>
               
                <div class="col-md-12" style="text-align: center;margin-top: 10px;font-weight: bold"><blink><small style="color:white;">ඔබගේ අලුත් ලියාපදිංචි අංකය, <a  data-toggle="modal" data-target="#find_no" style="color: yellow;cursor: pointer;"><br>මෙතනින් සොයාගන්න.</a></small></blink></div>

                <div class="col-md-12" style="text-align: center;"><small style="color:white;">To create a new account, <a href="register/form/reg_student_sin.php" style="color: yellow">Click Here</a></small></div>
                <br>
                <br>
                <div class="col-md-12" style="text-align: center;padding-top: 10px"><small style="color:white;">Student Login Page - &copy;IBS Developer Team</small>
                  <center><a href="../../index.php"><span class="fa fa-home" style="color:#ffff;font-size: 22px;margin-top: 10px;"></span></a></center>
                </div>

          </div>

      <div class="col-xl-4 col-lg-4 col-md-4"></div>

    </div>
</div>
  </div>
<script type="text/javascript">
  setInterval(function() {
    $('blink').fadeIn(1600).fadeOut(1000);
}, 1200);
</script>
    
 <script type="text/javascript">
  $(document).ready(function(){
  /*login script*/
    $('#table1').hide();

  $("#name").keyup(function(){
      
      var name = $(this).val();
      if(name == '')
      {
          $('#table1').hide();


      }else
      {
          $('#table1').show();

          $.ajax({  
                       url:"../query/check.php",  
                       method:"POST",  
                       data:{find_regid:name},  
                       success:function(data){ 
                        
                        $('#table1').html(data);

                      }
        });
      }
      
  });

  });



</script>

   <script type="text/javascript">
  
  /*login script*/
  $("#log").click(function(){
    var user = $('#user').val();
    var pasw = $('#psw').val();

    if(user == '')
    {
      $("#result").html("<div class='alert alert-danger alert-dismissible' id='alert'><button type='button' class='close' data-dismiss='alert'>&times;</button><span class='fa fa-warning'></span> <strong>Warning : </strong> Empty Feild.Please fill the Inputs.</div>");
      $("#alert").delay(3000).slideUp(500, function() {
            $("#alert").slideUp(800);              
      });
    }
    else
    if( pasw == '')
    {
      $("#result").html("<div class='alert alert-danger alert-dismissible' id='alert'><button type='button' class='close' data-dismiss='alert'>&times;</button><span class='fa fa-warning'></span> <strong>Warning : </strong> Empty Feild.Please fill the Inputs.</div>");
      $("#alert").delay(3000).slideUp(500, function() {
            $("#alert").slideUp(800);              
      });
    }
    else
    if(user !== '' && pasw !== '')
    {
      $.ajax({  
                     url:"log.php",  
                     method:"POST",  
                     data:{user:user,psw:pasw},  
                     success:function(data){ 
                      
                     if(data == 0)
                     {
                          Swal.fire({
                          position: 'top-middle',
                          type: 'error',
                          title: 'Username Or Password Incorrect',
                          showConfirmButton: false,
                          timer: 1500
                          })
                          $("#psw").focus();
                          $("#psw").val('');
                     }

                     if(data == 2)
                     {
                          $("#result").html("<div class='alert alert-danger alert-dismissible' id='alert'><button type='button' class='close' data-dismiss='alert'>&times;</button><span class='fa fa-warning'></span> <strong>Warning : </strong> Sorry, your registered account has not been verified yet.</div>");
                     }

                     if(data == 1)
                     {
                        window.location.href = "../app/index/index.php";
                     }

/*
                     if(data == 3)
                     {
                          $("#result").html("<div class='alert alert-danger alert-dismissible' id='alert'><button type='button' class='close' data-dismiss='alert'>&times;</button><span class='fa fa-warning'></span> <strong>Warning : </strong> Sorry, Already Logged in this account!</div>");
                     }*/
                    }
      });
    }
  });
  /*login script*/
  

  /*Enter key press after login to page*/
  $('#user').on( 'keyup', function( e ) {
            if( e.which == 13 ) {
              //GOTO DOWN
              $('#psw').focus();
            }
            
        } );

  $('#psw').on( 'keyup', function( e ) {
            if( e.which == 13 ) {
              //GOTO DOWN
              $('#log').click();
            }
            
        } );
  /*Enter key press after login to page*/


  $(document).ready(function() {
    $("#show_hide_password a").on('click', function(event) {
        event.preventDefault();
        if($('#show_hide_password input').attr("type") == "text"){
            $('#show_hide_password input').attr('type', 'password');
            $('#show_hide_password i').addClass( "fa-eye-slash" );
            $('#show_hide_password i').removeClass( "fa-eye" );
        }else if($('#show_hide_password input').attr("type") == "password"){
            $('#show_hide_password input').attr('type', 'text');
            $('#show_hide_password i').removeClass( "fa-eye-slash" );
            $('#show_hide_password i').addClass( "fa-eye" );
        }
    });
});

</script>

</body>

</html>


<div id="find_no" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content" style="border-radius: 0px;">
      <div class="modal-header" style="border-radius: 0px;">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">New Regsitration ID</h4>
      </div>
      <div class="modal-body col-md-12" style="background: #642B73;  /* fallback for old browsers */
background: -webkit-linear-gradient(to left, #C6426E, #642B73);  /* Chrome 10-25, Safari 5.1-6 */
background: linear-gradient(to left, #C6426E, #642B73); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
">
          <div class="col-md-12">
                  <div class="text-center">
                    <h1 style="color: white;">New Registration ID!</h1>
                  </div>
                    <div style="text-align: center;padding: 10px 10px 10px 10px;"><span style="color: white;font-size: 14px;text-align: center;">ඔබගේ නව ලියාපදිංචි අංකය සොයාගැනීම සදහා ඔබගේ නම ඇතුලත් කරන්න.</span></div>
                    <div class="form-group">
                      <input type="text" class="form-control form-control-user" aria-describedby="emailHelp" placeholder="Enter Your Name..." name="name" style="padding-left: 24px;height:50px;border-radius: 50px;" id="name" required autocomplete="off" autofocus="on">
                    </div>

                    <div style="overflow: auto;height: 200px;" id="table1"></div>
                   <div class="col-md-12" style="text-align: center;padding-top: 10px;"><small style="color:white;">&copy;IBS Developer Team</small></div>

          </div>
          </div>
      </div>
    </div>

  </div>