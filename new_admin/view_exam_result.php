
<?php

$page = "View Examination Result";
$folder_in = '3';

  include('header/header.php'); 
 ini_set( "display_errors", 0); 


  ?>
  <style type="text/css">
    .btn_pulse {
  
  display: block;
  border-radius: 10%;
  cursor: pointer;
  animation: none;
  float: left;
  bottom: 5px;
  right: 5px;
  font-weight: bold;
  padding-top: 2px;
  text-align: center;
  color: white;
  font-size: 14px;
  animation: pulse 1.6s infinite;
}


@keyframes pulse {
  0% {
    -moz-box-shadow: 0 0 0 0 #7252d3;
    box-shadow: 0 0 0 0 #7252d3;
  }
  70% {
    -moz-box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
  }
  100% {
    -moz-box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
  }
}

table {
  border: 0px solid black;
}
th, td {
  border: 0px solid black;
}
td {
  padding: 5px;
}

#pagination {
  width: 100%;
  text-align: center;
}

#pagination ul li {
  display: inline;
  margin-left: 10px;
}
  </style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script src="https://pagination.js.org/dist/2.1.4/pagination.min.js"></script>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<div class="row" style="border-bottom: 1px solid #cccc;">
  <div class="col-md-9">
    <ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-bottom: 04%;margin-bottom: 0;">
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="exam.php">Online Examination</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="View Examination Result"> View Examination Result</li>
    </ol>
  </div>
  </div>
</div>



<div class="col-md-12" style="margin-top: 4%;">

  <div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-4">
      <div class="col-md-12 bg-white" style="padding: 10px 10px 25px 10px;">
        <h3 class="text-center">
          <span class="fa fa-search"></span> Search Result
        </h3>

        <form action="final_result_report.php" method="POST" target="_blank">
          <div class="col-md-12" style="margin-top: 10px;border-top: 1px solid #cccc;padding-top: 10px;">
            <div class="form-group form-group-default input-group">
              <div class="form-input-group">
                <label class="pull-left">Exam Key</label>
                <datalist id="exam_key">
                    <?php 

                        $sql003 = mysqli_query($conn,"SELECT * FROM `paper_master`");
                        while($row003 = mysqli_fetch_assoc($sql003))
                        {
                            $exam_key = $row003['EXAM_KEY'];

                            echo '<option value="'.$exam_key.'"></option>';
                        }

                     ?>

                </datalist>


                <input list="exam_key" name="exam_key" class="form-control" placeholder="Enter Exam Key" onclick="this.value = '';">

              </div>
            </div>
          </div>

          <center><div style="width: 40px;"><h4 class="text-center text-muted label label-danger text-white" style="font-size: 16px;">OR</h4></div></center>

          <div class="col-md-12">
            <div class="form-group form-group-default input-group">
              <div class="form-input-group">
                <label class="pull-left">Exam Date</label>
                <input type="date" name="exam_date" max="<?php echo date('Y-m-d'); ?>" class="form-control">

              </div>
            </div>
          </div>


          <div class="col-md-12">
            <div class="row">
              <div class="col-md-12">
                <button type="submit" class="btn btn-success btn-block btn-lg" style="font-size: 18px;"><i class="fa fa-search"></i>&nbsp; Search</button>
              </div>
            </div>
          </div>

          </form>


        </div>

      </div>
    </div>
    <div class="col-md-4"></div>
  </div>

</div>
</div>

<script type="text/javascript">
  
    $(document).ready(function(){  
     $('#level_clz').change(function(){

       var level_clz = $(this).val();
       var teach_id = $('#teach_id').val();

       $.ajax({
        url:'../teacher/query/check.php',
        method:"POST",
        data:{create_clz:level_clz,teach_id:teach_id},
        success:function(data)
        {
          //alert(data)
            $('#subject').html(data);
          
        }
       })
     

    });

     
   });

  </script>

<script type="text/javascript">
  setInterval(function() {
    $('blink').fadeIn(1300).fadeOut(1500);
}, 1000);
</script>

<script>
$(document).ready(function(){
  $("#search").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $(".myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

<script type="text/javascript">
  $(document).ready(function () {

    (function ($) {

        $('#search').keyup(function () {
            var rex = new RegExp($(this).val(), 'i');
            $('#myTable tr').hide();
            $('#myTable tr').filter(function () {
                return rex.test($(this).text());
            }).show();
            $('.no-data').hide();
            if($('#myTable tr:visible').length == 0)
            {
                $('.no-data').show();
            }

        })

    }(jQuery));

});
</script>

<script>
$(document).ready(function(){

  $("#up_rec").on("change", function() {
      $('.show_image').hide();
  });
});
</script>
<script type="text/javascript">
  setInterval(function() {
    $('.blink').fadeIn(1300).fadeOut(1500);
}, 1000);
</script>

<script type="text/javascript">
  $("#show_hide_password a").on('click', function(event) {
        event.preventDefault();
        if($('#show_hide_password input').attr("type") == "text"){
            $('#show_hide_password input').attr('type', 'password');
            $('#show_hide_password i').addClass( "fa-eye-slash" );
            $('#show_hide_password i').removeClass( "fa-eye" );
        }else if($('#show_hide_password input').attr("type") == "password"){
            $('#show_hide_password input').attr('type', 'text');
            $('#show_hide_password i').removeClass( "fa-eye-slash" );
            $('#show_hide_password i').addClass( "fa-eye" );
        }
    });
</script>

<script type="text/javascript">
  let rows = []
$('table tbody tr').each(function(i, row) {
  return rows.push(row);
});

$('#pagination').pagination({
    dataSource: rows,
    pageSize: 8,
    callback: function(data, pagination) {
        $('tbody').html(data);
    }
})
</script>



<script type="text/javascript">
  $('#error').hide(); //error message hidden

  $(document).ready(function(){


    $('#error').hide(); //error message hidden



/*   validation form*/

   $('#password,#re_password').keyup(function(){

   var psw = $('#password').val();
   var re_psw = $('#re_password').val();
  
  if(psw !== '' && re_psw !== '')
  {
        if(psw !== re_psw)
       {    
            $('#error').show();

            $('#save').prop("disabled",true);
            $('#save').css('opacity','0.8');
            $('#save').css('cursor','not-allowed');

            $('#error').html('<div class="alert alert-danger col-md-12 text-center" style="color:red;font-size:16px;width:100%;"><span class="fa fa-warning text-danger"> The password you entered is incorrect.</span></div>');

       }else
       if(psw == re_psw)
       {    
            $('#error').show();

            $('#save').prop("disabled",false);
            $('#save').css('opacity','3.5');
            $('#save').css('cursor','pointer');

            $('#error').html('<div class="alert alert-success col-md-12 text-center" style="color:green;font-size:16px;width:100%;"><span class="fa fa-check-circle text-success"> The password you entered is correct.</span></div>');
       }
   }
   
   });

/*   validation form*/

});
</script>

<script>
$(document).ready(function () {

    $('#f_name,#l_name').bind('keyup blur',function()
    { 
           var thistext = $(this);
           thistext.val(thistext.val().replace(/[^a-z A-Z .]/g,'') ); 

           
    });

})
</script>

<script>
$(document).ready(function () {

    $('#address').bind('keyup blur',function()
    { 
           var thistext = $(this);
           thistext.val(thistext.val().replace(/[^a-z A-z . 0-9-/,]/g,'') ); 

           
    });

})
</script>

<script>
$(document).ready(function () {

    $('#email').bind('keyup blur',function()
    { 
           var thistext = $(this);
           thistext.val(thistext.val().replace(/[^a-zA-z0-9!#$%&'*+-/=?^_`{|,@]/g,'') ); 

           
    });

})
</script>

<script>
$(document).ready(function () {

    $('#number').bind('keyup blur',function()
    { 
           var thistext = $(this);
           thistext.val(thistext.val().replace(/[^0-9]/g,'') ); 

           
    });

})
</script>

<?php  include('footer/footer.php'); ?>


