<?php
	$page = "Sent SMS";
	$folder_in = '0';

      include('header/header.php');

        $s04 = mysqli_query($conn,"SELECT * FROM admin_login WHERE ADMIN_ID = '$admin_id'");
        while($row4 = mysqli_fetch_assoc($s04))
        {
          $name = $row4['ADMIN_NAME'];
        }

 ?>

 <style type="text/css">
   .count {
  
  animation: pulse 1s infinite;
}


@keyframes pulse {
  0% {
    -moz-box-shadow: 0 0 0 0 #ffd945;
    box-shadow: 0 0 0 0 #ffd945;
  }
  70% {
    -moz-box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
  }
  100% {
    -moz-box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
  }
}
 </style>

    <ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-top: 04%;margin-bottom: 0;">
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="SMS Main Menu"> <a href="sms.php">SMS Main Menu</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="SMS Paid"> Sent SMS</li>
    </ol>


  <div class="col-md-12">
    
    <div class="row" style="padding-top: 10px;">
      <div class="col-md-12 bg-white" style="border:1px solid #cccc;height: auto;">
      
      <div class="col-md-12" style="border-bottom: 1px solid #cccc;">
        <div class="row">

            <div class="col-md-9" style="margin-bottom: 10px;"><h2><a href="sms.php" style="color:black;"><span class="fa fa-angle-left"></span></a> Sent SMS</h2></div>
            <div class="col-md-3" style="float: right;"><?php 
                            $sql0021 = mysqli_query($conn,"SELECT * FROM `sms_counter`");

                            if(mysqli_num_rows($sql0021)>0)
                            { 

                              echo '<button class="btn btn-success pull-right" data-toggle="modal" data-target="#filter"  data-dismiss="modal" style="border-radius: 80px;padding:10px 10px 10px 10px;float: right;margin-top:10px;"><span class="fa fa-print" style="font-size: 19px;"></span></button>';

                            }else
                            if(mysqli_num_rows($sql0021)== '0')
                            { 
                              echo '<button class="btn btn-success disabled pull-right" style="border-radius: 80px;padding:10px 10px 10px 10px;cursor:not-allowed;float: right;margin-top:10px;"><span class="fa fa-print" style="font-size: 19px;"></span></button>';
                            }
                           ?></div>

                           <div class="modal fade slide-up disable-scroll" id="filter" tabindex="-1" role="dialog" aria-hidden="false">
                      <div class="modal-dialog ">
                      <div class="modal-content-wrapper">
                      <div class="modal-content">
                      <div class="modal-header clearfix text-left">
                      <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
                      </button>
                      <h5>Print Sent SMS Details</h5>
                      </div>
                      <div class="modal-body">
                          <div class="col-md-12">
                                  <form action="sms_print_sent_report.php" method="POST" target="_blank">
                                    <div class="row">
                                    <div class="col-md-12">
                                      <div class="form-group form-group-default" style="margin-top: 10px;">
                                        <label class="pull-left">Start Date</label>
                                        
                                        <input type="date" name="start_date" class="form-control" required max="<?php echo date('Y-m-d'); ?>">

                                      </div>
                                      </div>
                                    </div>
                                  
                                    <div class="row">
                                    <div class="col-md-12">
                                      <div class="form-group form-group-default" style="margin-top: 10px;">
                                        <label class="pull-left">End Date</label>
                                        
                                        <input type="date" name="end_date" class="form-control" required min="<?php echo date('Y-m-d'); ?>">

                                      </div>
                                      </div>
                                    </div>

                                     <div class="row">
                                    <div class="col-md-12">
                                      <div class="form-group form-group-default" style="margin-top: 10px;">
                                        <label class="pull-left">End Date</label>
                                        
                                          <select class="form-control" name="sms_type" required>
                                            <option value="">Select SMS Type</option>
                                            <option value="1">Single SMS Type</option>
                                            <option value="2">Multi SMS Type</option>
                                          </select>


                                      </div>
                                      </div>
                                    </div>  


                      <button type="submit" class="btn btn-success btn-lg btn-block" name="filter_data" value="<?php echo $stu_id; ?>" style="width: 100%;margin: none;" id="search_btn"><i class="pg-icon">search</i> Search</button>

                      <button type="submit" class="btn btn-default btn-lg btn-block" data-dismiss="modal" data-toggle="modal" data-target="#payments<?php echo $stu_id; ?>" style="width: 100%;margin: none;"><i class="pg-icon">close</i> Cancel</button>


                                  </form>
                                  </div>


                        </div>
                      </div>
                      </div>
                      </div>

                      </div>
                    </div>
        </div>

      </div>
          

<div class="card card-borderless">
<div class="col-md-12" style="padding-top:10px;">

<div class="tab-content">
<div class="tab-pane active" id="home">
<div class="row column-seperation">



<div class="col-lg-12">
  
  <div class="row">
    <div class="col-md-7">

      <form action="" method="POST">

      <?php 

        if(empty($_SESSION['start_date']) && empty($_SESSION['end_date'])&& empty($_SESSION['type']))
        {
           $st = date('Y-m-d');
           $en = date('Y-m-d');

           $show_ty = '<option value="">Select SMS Type</option>
            <option value="1">Single SMS Type</option>
            <option value="2">Multi SMS Type</option>';
        }else
        if(!empty($_SESSION['start_date']) && !empty($_SESSION['end_date']) && !empty($_SESSION['type']))
        {
           $st = $_SESSION['start_date'];
           $en = $_SESSION['end_date'];
           $ty = $_SESSION['type'];

           if($ty == '1')
           {
              $show_ty = '<option value="1">Single SMS Type</option>
                          <option value="2">Multi SMS Type</option>';
           }else
           if($ty == '2')
           {
              $show_ty = '<option value="2">Multi SMS Type</option>
                          <option value="1">Single SMS Type</option>';
           }
        }

       ?>
      <div class="row">
        <div class="col-md-3" style="margin-top: 6px;">
          <small>Start Date</small>
          <input type="date" name="start" max="<?php echo date('Y-m-d'); ?>" value="<?php echo $st; ?>" required class="form-control">
        </div>
        <div class="col-md-3" style="margin-top: 6px;">
          <small>End Date</small>
          <input type="date" name="end" min="<?php echo date('Y-m-d'); ?>" value="<?php echo $en; ?>" required class="form-control">
        </div>
        <div class="col-md-3" style="margin-top: 6px;">
          <small>SMS Type</small>
          <select class="form-control" name="sms_type" required>

            <?php echo $show_ty; ?>
            
          </select>
        </div>

        <div class="col-md-3" style="margin-top: 6px;">
          <small><br></small>
          <button class="btn btn-success btn-lg btn-block" type="submit" name="search"><i class="pg-icon">search</i>&nbsp;Search</button>
        </div>
      </div>
      </form>
    </div>
    <div class="col-md-1"></div>
    <div class="col-md-4">
      <small><br></small>
        <input type="text" id="myInput" class="form-control pull-right" style="width: 100%;margin-top: 6px;" placeholder="Search" data-toggle="tooltip" data-title="අවශ්‍ය දත්ත සෙවීම'">
      
  </div>

</div>
<div class="col-md-12" style="border-top: 1px solid #cccc;margin-top: 15px;"></div>

<div class="table-responsive" style="height: 550px;overflow: auto;margin-top: 4px;">
<table class="table demo-table-search table-responsive-block text-left" id="tableWithSearch">
<thead>


<?php 
  $today = date('Y-m-d');
  if(isset($_POST['search']))
  {
    $type = $_POST['sms_type'];

    $start = $_POST['start'];
    $end = $_POST['end'];

    $_SESSION['start_date'] = $start;
    $_SESSION['end_date'] = $end;
    $_SESSION['type'] = $type;


    if($type == '1')
    {//Single Type

      echo '<tr>
            <th style="width: 1%;">Sent Time</th>
            <th style="width: 1%;">Sent Date</th>
            <th style="width: 1%;">Student Name</th>
            <th style="width: 1%;">Telephone No</th>
            <th style="width: 1%;">Message Body</th>
            </tr>
            </thead>
            <tbody id="myTable">

            <tr class="no-data alert alert-danger" style="margin-top: 20px;display: none;">
              <td colspan="5" class="text-danger"><span class="fa fa-warning"></span> Not Found Data</td>
            </tr>';


            $sql0015 = mysqli_query($conn,"SELECT * FROM `single_sms` WHERE `SEND_DATE` BETWEEN '$start' AND '$end' ORDER BY `SEND_TIME` DESC");
            $check = mysqli_num_rows($sql0015);

            if($check>0)
            {

                while($row0015=mysqli_fetch_assoc($sql0015))
                {
                  $sms_body = $row0015['SMS_BODY'];
                  $tp = $row0015['TP'];
                  $st_name = $row0015['STUDENT_NAME'];

                  $add_date = $row0015['SEND_DATE'];
                  $add_time = $row0015['SEND_TIME'];
                echo '<tr>
                        <td class="v-align-middle">'.$add_time.'</td>
                        <td class="v-align-middle">'.$add_date.'</td>
                        <td class="v-align-middle">'.$st_name.'</td>
                        <td class="v-align-middle">'.$tp.'</td>
                        <td class="v-align-middle">'.$sms_body.'</td>
                      </tr>'; 
                }
            }else
            if($check == '0')
            {
              echo '<tr><td colspan="5" class="text-center text-danger"><span class="fa fa-warning"></span> Not Found Data</td></tr>';
            }
      //Single Type
    }else
    if($type == '2')
    {//Multi Type
        echo '<tr>
            <th style="width: 1%;">Sent Time</th>
            <th style="width: 1%;">Sent Date</th>
            <th style="width: 1%;">Register ID</th>
            <th style="width: 1%;">Student Name</th>
            <th style="width: 1%;">Telephone No</th>
            </tr>
            </thead>
            <tbody id="myTable">

            <tr class="no-data alert alert-danger" style="margin-top: 20px;display: none;">
              <td colspan="5" class="text-danger"><span class="fa fa-warning"></span> Not Found Data</td>
            </tr>';
          $sql001 = mysqli_query($conn,"SELECT * FROM `master_sms` WHERE `SENT_DATE` BETWEEN '$start' AND '$end' ORDER BY `SENT_TIME` DESC");

          $check = mysqli_num_rows($sql001);

          if($check>0){
          while($row001 = mysqli_fetch_assoc($sql001))
          {
            $add_time = $row001['SENT_TIME'];
            $add_date = $row001['SENT_DATE'];
            $master_id = $row001['MASTER_SMS_ID'];

            $sql0015 = mysqli_query($conn,"SELECT * FROM `multi_sms` WHERE `MASTER_SMS_ID` = '$master_id'");
            while($row0015=mysqli_fetch_assoc($sql0015))
            {
              $sms_body = $row0015['SMS_BODY'];
              $tp = $row0015['TP'];
              $stu_id = $row0015['STUDENT_ID'];

              $sql0017 = mysqli_query($conn,"SELECT * FROM `stu_login` WHERE `STU_ID` = '$stu_id'");
              while($row0017=mysqli_fetch_assoc($sql0017))
              {
                $reg_id = $row0017['REGISTER_ID'];
                
                $sql0018 = mysqli_query($conn,"SELECT * FROM `student_details` WHERE `STU_ID` = '$stu_id'");
                while($row0018=mysqli_fetch_assoc($sql0018))
                {
                  $st_name = $row0018['F_NAME']." ".$row0018['L_NAME'];

                }
              }

              

           

            echo '

              <tr>
                <td class="v-align-middle">'.$add_time.'</td>
                <td class="v-align-middle">'.$add_date.'</td>
                <td class="v-align-middle">'.$reg_id.'</td>
                <td class="v-align-middle">'.$st_name.'</td>
                <td class="v-align-middle">'.$tp.'</td></tr>'; 
            
            }   
          }
        }else
        if($check == '0')
        {
          echo '<tr><td colspan="5" class="text-center text-danger"><span class="fa fa-warning"></span> Not Found Data</td></tr>';
        }

      //Multi Type
    }
  }else
  {
      echo '<tr><td  style="border-top:1px solid white;" colspan="5" class="text-center text-danger"><span class="fa fa-warning"></span> Not Found Data</td></tr>';
  }
  

 ?>


</tbody>
</table>
</div>

              
</div>
</div>
</div>
</div>




</div>
</div>



      </div>
    </div>


  </div>





<script type="text/javascript">
  $('.save_btn').click(function(){
    
    var upload_btn0 = $('#selectedFile0').val();
    var upload_btn = $('#sel_file').val();
      
    if(empty(upload_btn0))
    {
      alert('Please select your audio file.');
    }else
    if(empty(upload_btn))
    {
      alert('Please select your document file.');
    }
   });
</script>

<script type="text/javascript">
  $('.navi').click(function(){
    $('.frm')[0].reset();
      
   });
</script>

<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
<!-- search data -->

<!-- no data message/no found data show in search-->

<script type="text/javascript">
  $(document).ready(function () {

    (function ($) {

        $('#myInput').keyup(function () {
            var rex = new RegExp($(this).val(), 'i');
            $('#myTable tr').hide();
            $('#myTable tr').filter(function () {
                return rex.test($(this).text());
            }).show();
            $('.no-data').hide();
            if($('#myTable tr:visible').length == 0)
            {
                $('.no-data').show();
            }

        })

    }(jQuery));

});
</script>




<?php include('footer/footer.php'); ?>