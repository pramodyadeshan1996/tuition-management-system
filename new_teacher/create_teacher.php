
<?php

$page = "Create Teacher";
$folder_in = '0';

  include('header/header.php'); 
 ini_set( "display_errors", 0); 


  ?>
  <style type="text/css">
    .btn_pulse {
  
  display: block;
  border-radius: 10%;
  cursor: pointer;
  animation: none;
  float: left;
  bottom: 5px;
  right: 5px;
  font-weight: bold;
  padding-top: 2px;
  text-align: center;
  color: white;
  font-size: 14px;
  animation: pulse 1.6s infinite;
}


@keyframes pulse {
  0% {
    -moz-box-shadow: 0 0 0 0 #7252d3;
    box-shadow: 0 0 0 0 #7252d3;
  }
  70% {
    -moz-box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
  }
  100% {
    -moz-box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
  }
}
  </style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<div class="row" style="border-bottom: 1px solid #cccc;padding-left: 8px;font-weight: bold;margin-top: 04%;margin-bottom: 0;">
  <div class="col-md-11">
    <ol class="breadcrumb" style="">
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="Create Administrators"> Create Teacher</li>
    </ol>
  </div>
  <div class="col-md-1">
        <div class="row">
          <divc class="col-md-5">

            <a data-toggle="tooltip" data-title="Create Teacher" data-placement="right"><button class="btn btn-primary btn-lg btn-rounded btn_pulse pull-right" data-toggle="modal" data-target="#create_teacher" style="padding: 10px 10px;margin-top: 10px;"><i class="pg-icon">add</i></button></a></div>
          <divc class="col-md-7"></div>
        </div>



         <div class="modal fade slide-up disable-scroll" id="create_teacher" tabindex="-1" role="dialog" aria-hidden="false" style="overflow: auto;">
          <div class="modal-dialog modal-lg">
          <div class="modal-content-wrapper">
          <div class="modal-content">
          <div class="modal-header clearfix text-left">
          <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
          </button>
          <h5>Create Teacher</h5>
          </div>
          <div class="modal-body">
            <form  action="../admin/query/insert.php" method="POST" id="form-personal" role="form" autocomplete="off" enctype="multipart/form-data">

                            <div class="row">

                            <div class="col-md-12">
                          <div class="form-group form-group-default input-group">
                          <div class="form-input-group">
                          <label class="pull-left">Salutation</label>

                            <select class="form-control change_inputs" name="position">
                                <option value="Mr">Mr.</option>
                                <option value="Mrs">Mrs.</option>
                                <option value="Ms">Ms.</option>
                                <option value="Rev">Rev.</option>
                                <option value="Dr">Dr.</option>
                                <option value="Prof">Prof.</option>

                              </select>

                            </div>
                            </div>
                            </div>
                            </div>

                            <div class="row">
                            <div class="col-md-6">
                            <div class="form-group form-group-default input-group">
                            <div class="form-input-group">
                            <label class="pull-left">First Name</label>
                            <input type="text" name="fnm" class="form-control change_inputs" pattern="[a-zA-Z. ]+"  required placeholder="XXXXXXXX">
                            </div>
                            </div>
                            </div>

                            <div class="col-md-6">
                            <div class="form-group form-group-default input-group">
                            <div class="form-input-group">
                            <label class="pull-left">Last Name</label>
                            <input type="text" name="lnm" class="form-control change_inputs" pattern="[a-zA-Z. ]+"  required placeholder="XXXXXXXX">
                            </div>
                            </div>
                            </div>

                            </div>

                            <div class="row">
                            <div class="col-md-6">
                            <div class="form-group form-group-default input-group">
                            <div class="form-input-group">
                            <label class="pull-left">Date Of Birth</label>
                            <input class="form-control change_inputs" type="date" name="dob" required min="<?php $d = strtotime("-60 year"); echo date('Y-m-d',$d); ?>" max="<?php $d = strtotime("-5 year"); echo date('Y-m-d',$d); ?>" style="margin-bottom: 10px;">
                            </div>
                            </div>
                            </div>

                            <div class="col-md-6">
                            <div class="form-group form-group-default input-group">
                            <div class="form-input-group">
                            <label class="pull-left">Telephone No</label>
                            <input class="form-control change_inputs" type="telephone" pattern="[0-9]{10}" minlength="10" maxlength="10" name="number" id="number" required placeholder="0xxx xxxx xx" style="margin-bottom: 10px;">
                            </div>
                            </div>
                            </div>

                            </div>

                            <div class="row">
                            <div class="col-md-12">
                            <div class="form-group form-group-default input-group">
                            <div class="form-input-group">
                            <label class="pull-left">E-mail Address</label>
                            <input type="email" class="form-control change_inputs" name="email" required  style="margin-bottom: 10px;" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" placeholder="XXXXXXXX">
                            </div>
                            </div>
                            </div>
                            </div>
                            <div class="row">
                            <div class="col-md-12">
                            <div class="form-group form-group-default">
                            <label class="pull-left">Address</label>
                            <textarea name="address" required placeholder="XXXXXXXX" style="margin-bottom: 10px;" class="form-control change_inputs"></textarea>
                            </div>
                            </div>
                            </div>
                            <div class="row">
                            <div class="col-md-6">
                                              <div class="form-group form-group-default input-group">
                                              <div class="form-input-group">
                                              <label class="pull-left">Gender</label>
                                              <br>
                                              <div class="row">

                                                    <div class="col-md-6" style="padding-left: 6px;">
                                                      <label class="pull-left"><span class="fa fa-male"></span> Male : <input type="radio" checked  name="gender" value="Male"></label>
                                                    </div>

                                                    <div class="col-md-6" style="padding-left: 6px;">
                                                      <label class="pull-left" style="padding-left: 10px;margin-bottom: 10px;"><span class="fa fa-female"></span> Female : <input type="radio" name="gender" value="Female"> </label>
                                                    </div>
                                                
                                                    
                                              </div>

                                              </div>
                                              </div>
                                              </div>
                            <div class="col-md-6">
                            <div class="form-group form-group-default">
                              <label class="pull-left">Qualification</label>
                              
                              <textarea name="qualification" required placeholder="XXXXXXXX" style="margin-bottom: 10px;" class="form-control change_inputs"><?php echo $qualification; ?></textarea>


                            </div>
                            </div>
                            </div>

                            <div class="row">
                              <div class="col-md-6">
                              <div class="form-group form-group-default input-group">
                              <div class="form-input-group">
                              <label>Username</label>
                              <input type="text" name="username" class="form-control change_inputs" placeholder="XXXXXXXX"  required>
                              </div>
                              </div>
                              </div>

                              <div class="col-md-6">
                                <div class="form-group form-group-default input-group">
                                <div class="form-input-group" id="show_hide_password" style="position: relative;">
                               
                                <label>Password</label>
                                <input type="password" name="password" placeholder="XXXXXXXX" class="form-control" required>

                                <a href=""><i class="fa fa-eye-slash" aria-hidden="true" style="position: absolute;right:10px;top:25px;"></i></a>

                                </div>
                              </div>
                              </div>

                              </div>

                            <div class="row">
                            <div class="col-md-12">
                            <div class="form-group form-group-default text-left">
                            <label style="padding-bottom: 10px;">Picture</label>
                              <input type="file" name="picture" class="form-control" accept="image/*" value="0">
                            </div>
                            </div>
                            </div>
                            <div class="clearfix"></div>
                            <button aria-label="" id="up_btn" class="btn btn-success btn-block pull-right btn-lg btn-block" type="submit" name="reg_teacher"><i class="pg-icon">plus</i> Add
                            </button>

                            </form>
            </div>
            </div>
            </div>
            </div>

            </div>
            

    </div>
  </div>
</div>




<div class="col-md-12" style="margin-top: 0;">
  <h3 style="text-transform: capitalize;"><a href="dashboard.php" data-toggle="tooltip" data-title="Back"><i class="pg-icon" style="font-size: 22px;">chevron_left</i></a> Create Teacher</h3>

<div class=" container-fluid   container-fixed-lg bg-white">
<div class="card card-transparent">
<div class="card-header ">
<div class="card-title">
</div>
<div class="pull-right">
<div class="col-xs-12">
<input type="text" id="search" class="form-control pull-right" placeholder="Search" data-toggle="tooltip" data-title="අවශ්‍ය දත්ත සෙවීම'">
</div>
</div>
<div class="clearfix"></div>
</div>

<div class="card-body table-responsive" style="height: 500px;overflow: auto;margin-bottom: 0;">
  <table class="table demo-table-search table-responsive-block text-left" id="tableWithSearch">
  <thead>

    <th>Registered Date</th>
    <th style="width: 11%;text-align: center;">Picture</th>
    <th>Teacher Name</th>
    <th>Gender</th>
    <th style="width: 10%;">Status</th>
    <th class="text-center">Action</th>

  </thead>
  <tbody id="myTable">

    <tr class="no-data alert alert-danger" style="margin-top: 20px;display: none;">
      <td colspan="4" class="text-danger"><span class="fa fa-warning"></span> Not Found Data</td>
    </tr>
      
   <?php 

          $sql001 = mysqli_query($conn,"SELECT * FROM teacher_login ORDER BY REG_DATE DESC");
          $ch = mysqli_num_rows($sql001);
          if($ch > 0)
          {
                while($row001 = mysqli_fetch_assoc($sql001))
  
                {
                  $teach_id = $row001['TEACH_ID'];
                  $reg_date = $row001['REG_DATE'];
                  $status = $row001['STATUS'];


                  $sql002 = mysqli_query($conn,"SELECT * FROM teacher_details WHERE TEACH_ID = '$teach_id'");
                  while($row002 = mysqli_fetch_assoc($sql002))
                  {
                    $name = $row002['POSITION'].". ".$row002['F_NAME']." ".$row002['L_NAME'];
                    $fnm = $row002['F_NAME'];
                    $lnm = $row002['L_NAME'];

                    $address = $row002['ADDRESS'];
                    $tp = $row002['TP'];
                    $dob = $row002['DOB'];
                    $gender = $row002['GENDER'];
                    $email = $row002['EMAIL'];
                    $position = $row002['POSITION'];
                    $qualification = $row002['QUALIFICATION'];
                    $picture = $row002['PICTURE'];
                  }
                  $real_picture = $picture;
                    if($picture == '0')
                   {
                      if($gender == 'Female')
                      {
                        $picture = 'girl.jpg';
                      }else
                      if($gender == 'Male')
                      {
                        $picture = 'boy.jpg';
                      }
                   }



                    if($status == 'Active')
                    {
                      $alert = 'success';
                      $icon = 'check-circle';
                      $btn = '<a href="../admin/query/update.php?status_teacher_data='.$teach_id.'&&status=Active" class="btn btn-danger btn-rounded btn-xs" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;" data-toggle="tooltip" data-title="Deactive Teacher"><i class="pg-icon">close</i></a>';
                    }else
                    if($status == 'Deactive')
                    {
                      $alert = 'danger';
                      $icon = 'times-circle';
                      $btn = '<a href="../admin/query/update.php?status_teacher_data='.$teach_id.'&&status=Deactive" class="btn btn-success btn-rounded btn-xs" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;" data-toggle="tooltip" data-title="Active Teacher"><i class="pg-icon">tick_circle</i></a>';
                    }
                    
                    
                    echo '<tr>';

                    echo '<td class="v-align-middle">'.$reg_date.'</td>
                          <td class="v-align-middle"><center><img src="../teacher/images/profile/'.$picture.'" class="rounded-circle image-responsive" id="show_image" style="height: auto;border:1px solid #cccc;width: 78%;border: 10px solid white;"></center></td>
                          <td class="v-align-middle bold">'.$name.'</td>
                          <td class="v-align-middle bold">'.$gender.'</td>
                          <td class="v-align-middle bold"><label class="label label-'.$alert.'"><span class="fa fa-'.$icon.'"></span> '.$status.'</label></td>
                          <td class="v-align-middle text-center">

                              '.$btn.'
                          ';


                          echo '

                                      <a data-toggle="tooltip" data-title="Edit Teacher Data"><button type="button" class="btn btn-complete  btn-xs btn-rounded" data-target="#update_teacher'.$teach_id.'" data-toggle="modal" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;margin-right:10px;margin-left:10px;"><i class="pg-icon">edit</i></button></a>';
                                        
                                        ?>
                                          <a href="../admin/query/delete.php?permanant_delete=<?php echo $teach_id; ?>" onclick="return confirm('Are you sure Permanently Delete?')" class="btn btn-danger btn-rounded btn-xs" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;" data-toggle="tooltip" data-title="Delete Class"><i class="pg-icon">trash_alt</i></a>

                                          <div class="modal fade slide-up disable-scroll" id="update_teacher<?php echo $teach_id; ?>" tabindex="-1" role="dialog" aria-hidden="false" style="overflow: auto;">
                                            <div class="modal-dialog modal-lg">
                                            <div class="modal-content-wrapper">
                                            <div class="modal-content">
                                            <div class="modal-header clearfix text-left">
                                            <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
                                            </button>
                                            <h5>Edit Teacher Details</h5>
                                            </div>
                                            <div class="modal-body">

                                              <form  action="../admin/query/update.php" method="POST" id="form-personal" role="form" autocomplete="off" enctype="multipart/form-data">

                          <div class="row">

                            <div class="col-md-12">
                          <div class="form-group form-group-default input-group">
                          <div class="form-input-group">
                          <label class="pull-left">Salutation</label>

                          <input type="hidden" value="<?php echo $real_picture; ?>" name="recent_file">

                            <select class="form-control change_inputs" name="position">
                              
                              <?php 
                              echo '
                              <option value="'.$position.'">'.$position.'.</option>';

                              if($position == 'Mr')
                              {
                                  echo '<option value="Mrs">Mrs.</option>
                              <option value="Ms">Ms.</option>
                              <option value="Rev">Rev.</option>
                              <option value="Dr">Dr.</option>
                              <option value="Prof">Prof.</option>';
                              }else
                              if($position == 'Mrs')
                              {
                                echo '<option value="Mr">Mr.</option>
                                      <option value="Ms">Ms.</option>
                                      <option value="Rev">Rev.</option>
                                      <option value="Dr">Dr.</option>
                                      <option value="Prof">Prof.</option>';
                              }else
                              if($position == 'Ms')
                              {
                                echo '<option value="Mr">Mr.</option>
                                      <option value="Mrs">Mrs.</option>
                                      <option value="Rev">Rev.</option>
                                      <option value="Dr">Dr.</option>
                                      <option value="Prof">Prof.</option>';
                              }else
                              if($position == 'Rev')
                              {
                                echo '<option value="Mr">Mr.</option>
                                      <option value="Mrs">Mrs.</option>
                                      <option value="Ms">Ms.</option>
                                      <option value="Dr">Dr.</option>
                                      <option value="Prof">Prof.</option>';
                              }else
                              if($position == 'Dr')
                              {
                                echo '<option value="Mr">Mr.</option>
                                      <option value="Mrs">Mrs.</option>
                                      <option value="Ms">Ms.</option>
                                      <option value="Rev">Rev.</option>
                                      <option value="Prof">Prof.</option>';
                              }else
                              if($position == 'Prof')
                              {
                                echo '<option value="Mr">Mr.</option>
                                      <option value="Mrs">Mrs.</option>
                                      <option value="Ms">Ms.</option>
                                      <option value="Rev">Rev.</option>
                                      <option value="Dr">Dr.</option>';
                              }


                             ?>

                              </select>

                            </div>
                            </div>
                            </div>
                            </div>


                            <div class="row">
                            <div class="col-md-6">
                            <div class="form-group form-group-default input-group">
                            <div class="form-input-group">
                            <label class="pull-left">First Name</label>
                            <input type="text" name="fnm" class="form-control change_inputs" value="<?php echo $fnm; ?>" pattern="[a-zA-Z. ]+"  required>
                            </div>
                            </div>
                            </div>

                            <div class="col-md-6">
                            <div class="form-group form-group-default input-group">
                            <div class="form-input-group">
                            <label class="pull-left">Last Name</label>
                            <input type="text" name="lnm" class="form-control change_inputs" value="<?php echo $lnm; ?>" pattern="[a-zA-Z. ]+"  required>
                            </div>
                            </div>
                            </div>

                            </div>

                            <div class="row">
                            <div class="col-md-6">
                            <div class="form-group form-group-default input-group">
                            <div class="form-input-group">
                            <label class="pull-left">Date Of Birth</label>
                            <input class="form-control change_inputs" type="date" name="dob" required min="<?php $d = strtotime("-60 year"); echo date('Y-m-d',$d); ?>" max="<?php $d = strtotime("-5 year"); echo date('Y-m-d',$d); ?>" style="margin-bottom: 10px;" value="<?php echo $dob; ?>">
                            </div>
                            </div>
                            </div>

                            <div class="col-md-6">
                            <div class="form-group form-group-default input-group">
                            <div class="form-input-group">
                            <label class="pull-left">Telephone No</label>
                            <input class="form-control change_inputs" type="telephone" pattern="[0-9]{10}" minlength="10" maxlength="10" name="number" id="number" value="<?php echo $tp; ?>" required placeholder="0xxx xxxx xx" style="margin-bottom: 10px;">
                            </div>
                            </div>
                            </div>

                            </div>

                            <div class="row">
                            <div class="col-md-12">
                            <div class="form-group form-group-default input-group">
                            <div class="form-input-group">
                            <label class="pull-left">E-mail Address</label>
                            <input type="email" class="form-control change_inputs" name="email" value="<?php echo $email; ?>" required  style="margin-bottom: 10px;" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$">
                            </div>
                            </div>
                            </div>
                            </div>
                            <div class="row">
                            <div class="col-md-12">
                            <div class="form-group form-group-default">
                            <label class="pull-left">Address</label>
                            <textarea name="address" required placeholder="xxxxxxxx" style="margin-bottom: 10px;" class="form-control change_inputs"><?php echo $address; ?></textarea>
                            </div>
                            </div>
                            </div>
                            <div class="row">
                            <div class="col-md-6">
                                              <div class="form-group form-group-default input-group">
                                              <div class="form-input-group">
                                              <label class="pull-left">Gender</label>
                                              <br>
                                              <div class="row">
                                                <?php 

                                                  if($gender == 'Male')
                                                  {?>

                                                    <div class="col-md-6" style="padding-left: 6px;">
                                                      <label class="pull-left"><span class="fa fa-male"></span> Male : <input type="radio" checked  name="gender" value="Male" class="change_inputs"></label>
                                                    </div>

                                                    <div class="col-md-6" style="padding-left: 6px;">
                                                      <label class="pull-left" style="padding-left: 10px;margin-bottom: 10px;"><span class="fa fa-female"></span> Female : <input type="radio" name="gender" value="Female" class="change_inputs"> </label>
                                                    </div>

                                                  <?php 
                                                  }else
                                                  if($gender == 'Female')
                                                  {?>

                                                    <div class="col-md-6" style="padding-left: 6px;">
                                                      <label class="pull-left"><span class="fa fa-male"></span> Male : <input type="radio" name="gender" value="Male" class="change_inputs"></label>
                                                    </div>

                                                    <div class="col-md-6" style="padding-left: 6px;">
                                                      <label class="pull-left" style="padding-left: 10px;margin-bottom: 10px;"><span class="fa fa-female"></span> Female : <input type="radio" name="gender" value="Female" class="change_inputs" checked> </label>
                                                    </div>

                                                  <?php 
                                                  }

                                                 ?>
                                                
                                                    
                                              </div>

                                              </div>
                                              </div>
                                              </div>
                            <div class="col-md-6">
                            <div class="form-group form-group-default">
                              <label class="pull-left">Qualification</label>
                              
                              <textarea name="qualification" required placeholder="xxxxxxxx" style="margin-bottom: 10px;" class="form-control change_inputs"><?php echo $qualification; ?></textarea>


                            </div>
                            </div>
                            </div>

                            <div class="row">
                            <div class="col-md-12">
                            <div class="form-group form-group-default text-left">
                            <label style="padding-bottom: 10px;">Picture</label>
                              <input type="file" name="picture" class="form-control" accept="image/*" value="0">
                            </div>
                            </div>
                            </div>


                            <div class="clearfix"></div>
                            <div class="row m-t-25">
                            <div class="col-xl-6 p-b-10">
                            <p class="small-text hint-text">Click the Update button to change all the data you have changed. (ඔබ වෙනස් කල සියලු දත්තයන් වෙනස් කිරීමට Update බොත්තම ඔබන්න.)</p>
                            </div>
                            <div class="col-xl-6">
                            <button aria-label="" id="up_btn" class="btn btn-primary pull-right btn-lg btn-block" type="submit" name="update_new_teacher" value="<?php echo $teach_id; ?>">Update
                            </button>
                            </div>
                            </div>
                            </form>


                                            </div>
                                            </div>
                                            </div>
                                            </div>

                                            </div>

                                          <?php


                          
                        }
                      }else
                      if( $ch == '0')
                      {
                          echo '<tr><td colspan="4" class="text-danger text-center"><span class="fa fa-warning text-danger"></span> Empty Data!</td></tr>';
                      }
 ?>
    
    </tr>
  </tbody>
  </table>
</div>
</div>

</div>

<script type="text/javascript">
  
    $(document).ready(function(){  
     $('#level_clz').change(function(){

       var level_clz = $(this).val();
       var teach_id = $('#teach_id').val();

       $.ajax({
        url:'../teacher/query/check.php',
        method:"POST",
        data:{create_clz:level_clz,teach_id:teach_id},
        success:function(data)
        {
          //alert(data)
            $('#subject').html(data);
          
        }
       })
     

    });

     
   });

  </script>

<script type="text/javascript">
	setInterval(function() {
    $('blink').fadeIn(1300).fadeOut(1500);
}, 1000);
</script>

<script>
$(document).ready(function(){
  $("#search").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $(".myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

<script type="text/javascript">
  $(document).ready(function () {

    (function ($) {

        $('#search').keyup(function () {
            var rex = new RegExp($(this).val(), 'i');
            $('#myTable tr').hide();
            $('#myTable tr').filter(function () {
                return rex.test($(this).text());
            }).show();
            $('.no-data').hide();
            if($('#myTable tr:visible').length == 0)
            {
                $('.no-data').show();
            }

        })

    }(jQuery));

});
</script>

<script>
$(document).ready(function(){

  $("#up_rec").on("change", function() {
      $('.show_image').hide();
  });
});
</script>
<script type="text/javascript">
  setInterval(function() {
    $('.blink').fadeIn(1300).fadeOut(1500);
}, 1000);
</script>

<script type="text/javascript">
  $("#show_hide_password a").on('click', function(event) {
        event.preventDefault();
        if($('#show_hide_password input').attr("type") == "text"){
            $('#show_hide_password input').attr('type', 'password');
            $('#show_hide_password i').addClass( "fa-eye-slash" );
            $('#show_hide_password i').removeClass( "fa-eye" );
        }else if($('#show_hide_password input').attr("type") == "password"){
            $('#show_hide_password input').attr('type', 'text');
            $('#show_hide_password i').removeClass( "fa-eye-slash" );
            $('#show_hide_password i').addClass( "fa-eye" );
        }
    });
</script>

<?php  include('footer/footer.php'); ?>


