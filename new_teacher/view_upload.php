<?php
	$page = "View Uploads";
	$folder_in = '0';

      include('header/header.php');

        $teach_id = $_GET['teach_id'];

        $s04 = mysqli_query($conn,"SELECT * FROM teacher_details WHERE TEACH_ID = '$teach_id'");
        while($row4 = mysqli_fetch_assoc($s04))
        {
          $teacher_name = $row4['POSITION'].". ".$row4['F_NAME']." ".$row4['L_NAME'];
          $teacher_name2 = $row4['F_NAME']." ".$row4['L_NAME'];

          $t_pic = $row4['PICTURE'];

          if($t_pic == '0')
          {
          	$t_pic = 'teacher.png';
          }
        }


  if(!empty($_SESSION['click_view_upload_tab']))
    {
      $click_view_upload_tab = $_SESSION['click_view_upload_tab'];

      if($click_view_upload_tab == 'video')
      {
        $video = 'active';
      }else
      if($click_view_upload_tab == 'audio')
      {
        $audio = 'active';
      }else
      if($click_view_upload_tab == 'document')
      {
        $document = 'active';
      }else
      if($click_view_upload_tab == 'record')
      {
        $record = 'active';
      }

    }else
    if(empty($_SESSION['click_view_upload_tab']))
    {
      $video = 'active';

      $audio = '';

      $document = '';

      $record = '';

    }

 ?>
 <div class="row" style="border-bottom: 1px solid #cccc;">
  <div class="col-md-7">
 <ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-top: 5%;margin-bottom: 0;">

<li class="breadcrumb-item">
  <a href="dashboard.php" data-toggle="tooltip" data-title="Dashboard"> Dashboard</a>
</li>
<li class="breadcrumb-item" data-toggle="tooltip" data-title="Edit Upload"> Edit Upload
</li>


</ol>
</div>
  <div class="col-md-5">
    <h5 class="pull-right"><?php echo $teacher_name; ?></h5>
  </div>
</div>


  <div class="col-md-12">
    
    <div class="row" style="padding-top: 10px;">
      <div class="col-md-1"></div>
      <div class="col-md-10 bg-white" style="border:1px solid #cccc;height: auto;">
      
      <div class="col-md-12" style="border-bottom: 1px solid #cccc;">
        <div class="row">

            <div class="col-md-9" style="margin-bottom: 10px;">

                <label style="font-size: 12px;padding-top: 10px;">
              <a href="dashboard.php" class="text-muted" data-toggle="tooltip" data-title="Back" style="font-size: 12px;"><span class="fa fa-angle-left"></span> Back</a>
            </label>

            <h2>View Upload</h2>

            </div>

            <div class="col-md-3" style="padding-top: 20px;"><a href="upload.php?teach_id=<?php echo $teach_id; ?>" class="btn btn-success" style="float: right;margin-bottom: 4px;"><span class="fa fa-plus"></span>&nbsp;New Upload</a></div>




        </div>
      </div>
          

<div class="card card-borderless">


<ul class="nav nav-tabs nav-tabs-simple" data-init-reponsive-tabs="dropdownfx">
<li class="nav-item navi" style="width: 25%;text-align: center;">
<a class="<?php echo $video; ?>" data-toggle="tab" data-target="#home" href="#" onclick="click_view_upload_tab('video');"><span><span class="fa fa-video-camera" style="font-size: 16px;line-height: 0.8;"></span> Video</span></a>
</li>
<li class="nav-item navi" style="width: 25%;text-align: center;">
<a class="<?php echo $audio; ?>" data-toggle="tab" data-target="#profile" href="#" onclick="click_view_upload_tab('audio');"><span><span class="fa fa-volume-up" style="font-size: 16px;line-height: 0.8;"></span> Audio</span></a>
</li>
<li class="nav-item navi" style="width: 25%;text-align: center;">
<a class="<?php echo $document; ?>" data-toggle="tab" data-target="#messages" href="#" onclick="click_view_upload_tab('document');"><span><span class="fa fa-file" style="font-size: 16px;line-height: 0.8;"></span> Document</span></a>
</li>
<li class="nav-item navi" style="width: 25%;text-align: center;">
<a class="<?php echo $record; ?>" data-toggle="tab" data-target="#record" href="#" onclick="click_view_upload_tab('record');"><span><span class="fa fa-circle text-danger" style="font-size: 16px;line-height: 0.8;"></span> Records</span></a>
</li>
</ul>


 <script type="text/javascript">
    function click_view_upload_tab(tab_name)
    {
          
          $.ajax({  
          url:"../teacher/query/check.php",  
          method:"POST",  
          data:{click_view_upload_tab:tab_name},  
          success:function(data){ 
             
            //alert(data);

           }           
         });

    }
 </script>

<div class="tab-content">
<div class="tab-pane <?php echo $video; ?>" id="home">
<div class="row column-seperation">



<div class="col-lg-12">
  
  <div class="row">
    <div class="col-md-8">

      <div class="pull-left">
        <h3>
          <span class="semi-bold"><span class="fa fa-video-camera"></span>&nbsp;Video</span>
        </h3>
      </div>

    </div>
    <div class="col-md-4">
        <input type="text" id="myInput1" class="form-control pull-right" style="width: 100%;" placeholder="Search" data-toggle="tooltip" data-title="අවශ්‍ය දත්ත සෙවීම'">
      
  </div>

</div>



<div class="table-responsive" style="height: 800px;overflow: auto;margin-top: 4px;">
<table class="table demo-table-search table-responsive-block text-left" id="tableWithSearch">
<thead>
<tr>
<th>Upload Time</th>
<th>Title</th>
<th style="width: 10%;">Subject Name</th>
<th style="width: 10%;">Class Name</th>
<th class="text-center">Action</th>
</tr>
</thead>
<tbody id="myTable1">

<tr class="no-data1 col-md-12 alert alert-danger" style="margin-top: 20px;display: none;text-align: center;">
  <td colspan="6" class="text-danger"><span class="fa fa-warning"></span> Not Found Data</td>
</tr>

<?php 

  $i = 0;

  $sql002 = mysqli_query($conn,"SELECT * FROM teacher_subject WHERE TEACH_ID = '$teach_id'");
  
      while($row002 = mysqli_fetch_assoc($sql002))
      {
        $subj_id = $row002['SUB_ID'];

         $sql0016 = mysqli_query($conn,"SELECT * FROM subject WHERE SUB_ID = '$subj_id'");
        while($row0016=mysqli_fetch_assoc($sql0016))
        {
          $sub_name00 = $row0016['SUBJECT_NAME'];
        }

        $sql003 = mysqli_query($conn,"SELECT * FROM classes WHERE SUB_ID = '$subj_id' AND TEACH_ID = '$teach_id'");
        while($row003 = mysqli_fetch_assoc($sql003))
        {
              $class_id = $row003['CLASS_ID'];

              $class_name = $row003['CLASS_NAME'];

              $i++;

              $sql001 = mysqli_query($conn,"SELECT * FROM uploads WHERE CLASS_ID = '$class_id' AND TYPE = 'Video' ORDER BY UPLOAD_TIME DESC");
              
              if(mysqli_num_rows($sql001)>0)
                {

                  while($row001 = mysqli_fetch_assoc($sql001))
                  {
                    $up_file = $row001['FILE_NAME'];
                    $up_title = $row001['TITLE'];
                    $up_desc = $row001['DESCRIPTION'];
                    $up_time = $row001['UPLOAD_TIME'];
                    $up_id = $row001['UPLOAD_ID'];
                    $extention = $row001['EXTENSION'];
                    $ytd = $row001['YOUTUBE_LINK'];

                     if($up_file == '0' && $up_desc == '0')
                          {
                            $up_file =  "N/A";
                            $up_desc = '';
                          }
                    echo '

                      <tr>';

                      /*<video controls style="border:0px solid black;width: 100%;height: 150px;outline: none;" class="image-responsive">
                        <source src="video/'.$up_file.'" type="video/'.$extention.'">
                      </video>*/
                      
                        $url = $ytd;
                           $shortUrlRegex = '/youtu.be\/([a-zA-Z0-9_-]+)\??/i';
                           $longUrlRegex = '/youtube.com\/((?:embed)|(?:watch))((?:\?v\=)|(?:\/))([a-zA-Z0-9_-]+)/i';

                          if (preg_match($longUrlRegex, $url, $matches)) {
                              $youtube_id = $matches[count($matches) - 1];
                          }

                          if (preg_match($shortUrlRegex, $url, $matches)) {
                              $youtube_id = $matches[count($matches) - 1];
                          }
                          $emb_link = 'https://www.youtube.com/embed/' . $youtube_id ;
                      

                      


                      echo '

                  <td class="v-align-middle">'.$up_time.'</td>

                  <td class="v-align-middle"><h5 style="text-align:left;text-transform: capitalize;">'.$up_title.'</h5></td>

                  <td class="v-align-middle">'.$sub_name00.'</td>

                  <td class="v-align-middle">'.$class_name.'</td>

                  <td class="v-align-middle">';

                    /*<button class="btn btn-danger btn-rounded btn-xs" data-target="#modalFillIn" data-toggle="modal" ><span class="fa fa-question"></span>&nbsp; Info</button>*/

                    echo '
                    
                    <center>
                    
                    <button type="button" class="btn btn-complete  btn-xs btn-rounded" data-target="#edit_video'.$up_id.'" data-toggle="modal" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;"><i class="pg-icon">edit</i></button>';?>


                     <a href="<?php echo $emb_link ?>" target="_blank" type="button" class="btn btn-info btn-rounded btn-xs" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;"><i class="pg-icon">link</i></button>

                    <a href="../teacher/query/delete.php?delete_video_upload=<?php echo $up_id; ?>&&teach_id=<?php echo $teach_id; ?>&&type=video" onclick="return confirm('Are you sure Delete?')" class="btn btn-danger btn-rounded btn-xs" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;"><i class="pg-icon">trash_alt</i></a>

                  </center>


                    <?php
                    echo '

                      <div class="modal fade slide-up disable-scroll" id="edit_video'.$up_id.'" tabindex="-1" role="dialog" aria-hidden="false">
                      <div class="modal-dialog ">
                      <div class="modal-content-wrapper">
                      <div class="modal-content">
                      <div class="modal-header clearfix text-left">
                      <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
                      </button>
                      <h5>Edit Video</h5>
                      </div>
                      <div class="modal-body">


                      <form action="../teacher/query/update.php" method="POST" class="frm">

                                    <div class="form-group form-group-default input-group">
                                      <div class="form-input-group">
                                      <label>Title</label>
                                        <input type="text" name="title" class="form-control change_inputs"  placeholder="XXXXXXXX" value="'.$up_title.'" required onclick="this.select();" >
                                      </div>
                                    </div>

                                  <div class="form-group form-group-default" style="margin-top: 10px;">
                                    <label>Description</label>
                                    <textarea name="descip" required placeholder="XXXXXXXX" style="margin-bottom: 10px;height: 100px;" class="form-control change_inputs" onclick="this.select();" >'.$up_desc.'</textarea>
                                  </div>

                                  <div class="form-group form-group-default" style="margin-top: 10px;">
                                    <label>Subjects</label>
                                     <select class="form-control select_subject1'.$up_id.'" name="select_subject" required style="cursor: pointer;">
                                      <option value="'.$subj_id.'">'.$sub_name00.'</option>';

                                                      $sql0014 = mysqli_query($conn,"SELECT * FROM teacher_subject WHERE TEACH_ID = '$teach_id' AND SUB_ID != '$subj_id'");
                                                      while($row0014=mysqli_fetch_assoc($sql0014))
                                                      {
                                                        $sub_id = $row0014['SUB_ID'];

                                                        $sql0015 = mysqli_query($conn,"SELECT * FROM subject WHERE SUB_ID = '$sub_id'");
                                                        while($row0015=mysqli_fetch_assoc($sql0015))
                                                        {
                                                          $sub_name = $row0015['SUBJECT_NAME'];
                                                          $subject_id = $row0015['SUB_ID'];

                                                          echo '<option value='.$subject_id.'>'.$sub_name.'</option>';

                                                        }

                                                      } 

                                                      ?>

                                                      <script type="text/javascript">
  
                                                        $(document).ready(function(){  
                                                         $('.select_subject1<?php echo $up_id; ?>').change(function(){

                                                           var subject = $(this).val();
                                                           var teach_id1 = $('#teach_id1').val();

                                                           $.ajax({
                                                            url:'../teacher/query/check.php',
                                                            method:"POST",
                                                            data:{update_media:subject,teach_id1:teach_id1},
                                                            success:function(data)
                                                            {
                                                              //alert(data)
                                                                $('#select_clz1<?php echo $up_id; ?>').html(data);
                                                              
                                                            }
                                                           })
                                                         

                                                        });
                                                       });

                                                      </script>

                                                      <?php
                                      
                                    echo '</select>
                                  </div>

                                  <input type="hidden" id="teach_id1" value="'.$teach_id.'">

                                  <input type="hidden" name="teach_id" value="'.$teach_id.'">
                                  <input type="hidden" name="type" value="Video">


                                  <div class="form-group form-group-default" style="margin-top: 10px;">
                                    <label>Classes</label>
                                    <select class="form-control" id="select_clz1'.$up_id.'" name="clz" required style="cursor: pointer;">
                                      <option value="'.$class_id.'">'.$class_name.'</option>
                                    </select>
                                  </div>

                                  <div class="form-group form-group-default" style="margin-top: 10px;">
                                    <label>Youtube Link </label>';
                                    ?>
                                    <input type="text" name="ytd" class="form-control change_inputs"  placeholder="Ex-https://www.youtube.com/watch?v=xxxxxxx" value="<?php echo $ytd; ?>" onclick="this.select();" required>

                                    <?php echo '
                                  </div>

                                  <button type="submit" class="btn btn-success btn-block btn-lg" name="edit_video_btn" value="'.$up_id.'"><i class="pg-icon">tick_circle</i> Update</button>
                                </form>


                                  </div>
                                  </div>
                                  </div>
                                  </div>

                        </div>
                      </div>


                  </td>
                </tr>';

            }
            }else
            if(mysqli_num_rows($sql001)== '0')
            {     if($i == '1')
                  {
                    echo '<tr><td colspan="5" class="text-danger text-center"><span class="fa fa-warning text-danger"></span> Empty Data!</td></tr>';
                  }
                     

            }
          }
        }
      
 ?>

</tbody>
</table>
</div>

              
</div>
</div>
</div>



<div class="tab-pane <?php echo $audio; ?>" id="profile">

<div class="col-lg-12">

  <div class="row">
    <div class="col-md-8">

      <div class="pull-left">
        <h3>
          <span class="semi-bold"><span class="fa fa-volume-up"></span>&nbsp;Audio</span>
        </h3>
      </div>

    </div>
    <div class="col-md-4">
        <input type="text" id="myInput2" class="form-control pull-right" style="width: 100%;" placeholder="Search" data-toggle="tooltip" data-title="අවශ්‍ය දත්ත සෙවීම'">
      
  </div>

</div>

<div class="table-responsive" style="height: 800px;overflow: auto;margin-top: 4px;">
<table class="table demo-table-search table-responsive-block text-left" id="tableWithSearch">
<thead>
<tr>
<th>Upload Time</th>
<th>Title</th>
<th style="width: 10%;">Subject Name</th>
<th style="width: 10%;">Class Name</th>
<th class="text-center">Action</th>
</tr>
</thead>
<tbody id="myTable2">

  <tr class="no-data2 col-md-12 alert alert-danger" style="margin-top: 20px;display: none;text-align: center;">
    <td colspan="6" class="text-danger"><span class="fa fa-warning"></span> Not Found Data</td>
  </tr>

<?php 

$i2 = 0;
  $sql002 = mysqli_query($conn,"SELECT * FROM teacher_subject WHERE TEACH_ID = '$teach_id'");
  
      while($row002 = mysqli_fetch_assoc($sql002))
      {
        $subj_id = $row002['SUB_ID'];

         $sql0016 = mysqli_query($conn,"SELECT * FROM subject WHERE SUB_ID = '$subj_id'");
        while($row0016=mysqli_fetch_assoc($sql0016))
        {
          $sub_name00 = $row0016['SUBJECT_NAME'];

        }

        $sql003 = mysqli_query($conn,"SELECT * FROM classes WHERE SUB_ID = '$subj_id' AND TEACH_ID = '$teach_id'");
        while($row003 = mysqli_fetch_assoc($sql003))
        {
              $class_id = $row003['CLASS_ID'];

              $class_name = $row003['CLASS_NAME'];

              $i2++;

              $sql001 = mysqli_query($conn,"SELECT * FROM uploads WHERE CLASS_ID = '$class_id' AND TYPE = 'Audio' ORDER BY UPLOAD_TIME DESC");
              
              if(mysqli_num_rows($sql001)>0)
              {

                  while($row001 = mysqli_fetch_assoc($sql001))
                  {
                    $up_file = $row001['FILE_NAME'];
                    $up_title = $row001['TITLE'];
                    $up_desc = $row001['DESCRIPTION'];
                    $up_time = $row001['UPLOAD_TIME'];
                    $up_id = $row001['UPLOAD_ID'];
                    $audio_extention = $row001['EXTENSION'];
                    $file_types = 'audio/*';

                     if($up_file == '0' && $up_desc == '0')
                          {
                            $up_file =  "N/A";
                            $up_desc = '';
                          }
                    echo '

                      <tr>


                  <td class="v-align-middle">'.$up_time.'</td>  

                  <td class="v-align-middle">
                    <h5 style="text-align:left;text-transform: capitalize;">'.$up_title.'</h5>

                  </td>
                  <td class="v-align-middle">'.$sub_name00.'</td>

                  <td class="v-align-middle">'.$class_name.'</td>

                  <td class="v-align-middle" style="padding:0;">';

                    /*<button class="btn btn-danger btn-rounded btn-xs" data-target="#modalFillIn" data-toggle="modal" ><span class="fa fa-question"></span>&nbsp; Info</button>*/

                    echo '
                    <center>

                      <button type="button" class="btn btn-complete  btn-xs btn-rounded" data-target="#edit_audio'.$up_id.'" data-toggle="modal" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;"><i class="pg-icon">edit</i></button>';?>

                       <button type="button" class="btn btn-info btn-rounded btn-xs" data-toggle="modal" data-target="#preview_audio<?php echo $up_id; ?>" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;"><i class="pg-icon">expand</i></button>

                      <a href="../teacher/query/delete.php?delete_video_upload=<?php echo $up_id; ?>&&teach_id=<?php echo $teach_id; ?>&&type=audio" onclick="return confirm('Are you sure Delete?')" class="btn btn-danger btn-rounded btn-xs" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;"><i class="pg-icon">trash_alt</i></a>

                    </center>

                    <div id="preview_audio<?php echo $up_id; ?>" class="modal fade" role="dialog">
                      <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content modal-sm">
                          
                          <div class="col-md-12" style="padding-bottom: 6px;padding-top: 6px;border-bottom: 1px solid #cccc;margin-bottom: 10px;">
                            <div class="row">
                              <div class="col-md-8"><h4>Preview</h4></div>
                              <div class="col-md-4"><button type="button" class="close" data-dismiss="modal">&times;</button></div>
                            </div>
                          </div>
                            
                          <div class="modal-body" style="padding: 10px 10px 25px 10px;">
                            <div class="col-md-12" style="padding: 4px 4px 4px 4px;">
                              <audio controls style="border:0px solid black;width: 100%;height: 60px;outline: none;margin-top: 0px;">
                                <source src="horse.ogg" type="audio/ogg">
                                <source src="../uploads/audio/<?php echo $up_file; ?>" type="audio/<?php echo $audio_extention; ?>">
                              </audio>
                            </div>
                            
                          </div>
                        </div>

                      </div>
                    </div>

                    <?php
                    echo '

                      <div class="modal fade slide-up disable-scroll" id="edit_audio'.$up_id.'" tabindex="-1" role="dialog" aria-hidden="false">
                      <div class="modal-dialog ">
                      <div class="modal-content-wrapper">
                      <div class="modal-content">
                      <div class="modal-header clearfix text-left">
                      <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
                      </button>
                      <h5>Edit Video</h5>
                      </div>
                      <div class="modal-body">


                      <form action="../teacher/query/update.php" method="POST" class="frm" enctype="multipart/form-data">
                                    <div class="form-group form-group-default input-group">
                                      <div class="form-input-group">
                                      <label>Title</label>
                                        <input type="text" name="title" class="form-control change_inputs"  placeholder="XXXXXXXX" value="'.$up_title.'" required onclick="this.select();" >
                                      </div>
                                    </div>

                                  <div class="form-group form-group-default" style="margin-top: 10px;">
                                    <label>Description</label>
                                    <textarea name="descip" required placeholder="XXXXXXXX" style="margin-bottom: 10px;height: 100px;" class="form-control change_inputs" onclick="this.select();" >'.$up_desc.'</textarea>
                                  </div>

                                  <div class="form-group form-group-default" style="margin-top: 10px;">
                                    <label>Subjects</label>
                                     <select class="form-control select_subject2'.$up_id.'" name="select_subject" required style="cursor: pointer;">
                                      <option value="'.$subj_id.'">'.$sub_name00.'</option>';

                                                      $sql0014 = mysqli_query($conn,"SELECT * FROM teacher_subject WHERE TEACH_ID = '$teach_id' AND SUB_ID != '$subj_id'");
                                                      while($row0014=mysqli_fetch_assoc($sql0014))
                                                      {
                                                        $sub_id = $row0014['SUB_ID'];

                                                        $sql0015 = mysqli_query($conn,"SELECT * FROM subject WHERE SUB_ID = '$sub_id'");
                                                        while($row0015=mysqli_fetch_assoc($sql0015))
                                                        {
                                                          $sub_name = $row0015['SUBJECT_NAME'];
                                                          $subject_id = $row0015['SUB_ID'];

                                                          echo '<option value='.$subject_id.'>'.$sub_name.'</option>';

                                                        }

                                                      } 

                                                      ?>

                                                      <script type="text/javascript">
  
                                                        $(document).ready(function(){  
                                                         $('.select_subject2<?php echo $up_id; ?>').change(function(){

                                                           var subject = $(this).val();
                                                           var teach_id1 = $('#teach_id1').val();

                                                           $.ajax({
                                                            url:'../teacher/query/check.php',
                                                            method:"POST",
                                                            data:{update_media:subject,teach_id1:teach_id1},
                                                            success:function(data)
                                                            {
                                                              //alert(data)
                                                                $('#select_clz2<?php echo $up_id; ?>').html(data);
                                                              
                                                            }
                                                           })
                                                         

                                                        });
                                                       });

                                                      </script>

                                                      <?php


                                      
                                    echo '</select>
                                  </div>

                                  <input type="hidden" id="teach_id1" value="'.$teach_id.'">
                                  <input type="hidden" name="type" value="Audio">
                                  <input type="hidden" name="recent_file" value="'.$up_file.'">
                                  <input type="hidden" name="teach_id" value="'.$teach_id.'">


                                  <div class="form-group form-group-default" style="margin-top: 10px;">
                                    <label>Classes</label>
                                    <select class="form-control" id="select_clz2'.$up_id.'" name="clz" required style="cursor: pointer;">
                                      <option value="'.$class_id.'">'.$class_name.'</option>
                                    </select>
                                  </div>

                                  <div class="form-group form-group-default" style="margin-top: 10px;">
                                    <label>Upload File </label>';

                                    $file_types = 'audio/*';
                                    ?> 

                                    <input type="file" value="<?php echo $up_file; ?>" id="selectedFile<?php echo $up_id; ?>" style="display: none;" class="form-control selectedFile" name="upload_file1" accept="<?php echo $file_types; ?>" required />

                                    <button type="button" class="btn btn-info active" style="padding: 20px 20px 20px 20px;width: 100%;height: 100px;border:3px dashed #cccc;font-size: 10px;font-weight: bold;color:white;" onclick="document.getElementById('selectedFile<?php echo $up_id; ?>').click();" id="select_btn<?php echo $up_id; ?>" value="<?php echo $up_file; ?>">

                                        <h6 id="h6_text"><span class="fa fa-plus" style="font-size:30px;"></span><br> Please choose audio file.</h6>
                                    </button>

                                    <?php echo '
                                  </div>

                                  <button type="submit" class="btn btn-success btn-block btn-lg" name="edit_audio_btn" value="'.$up_id.'"><i class="pg-icon">tick_circle</i> Update</button>
                                </form>


                                  </div>
                                  </div>
                                  </div>
                                  </div>

                        </div>
                      </div>


                  </td>
                </tr>';
                

                      }

                    }
              }
        }
      
 ?>





</tbody>
</table>         

</div>
</div>
</div>



<div class="tab-pane <?php echo $document; ?>" id="messages">

<div class="col-lg-12">

  

  <div class="row">
    <div class="col-md-8">

      <div class="pull-left">
        <h3>
          <span class="semi-bold"><span class="fa fa-file"></span>&nbsp;Document</span>
        </h3>
      </div>

    </div>
    <div class="col-md-4">
        <input type="text" id="myInput3" class="form-control pull-right" style="width: 100%;" placeholder="Search" data-toggle="tooltip" data-title="අවශ්‍ය දත්ත සෙවීම'">
      
  </div>

</div>

<div class="table-responsive" style="height: 800px;overflow: auto;margin-top: 4px;">
<table class="table demo-table-search table-responsive-block text-left" id="tableWithSearch">
<thead>
<tr>
<th>Upload Time</th>
<th>Title</th>
<th style="width: 10%;">Subject Name</th>
<th style="width: 10%;">Class Name</th>
<th class="text-center">Action</th>
</tr>
</thead>
<tbody id="myTable3">

  <tr class="no-data3 col-md-12 alert alert-danger" style="margin-top: 20px;display: none;text-align: center;">
    <td colspan="6" class="text-danger"><span class="fa fa-warning"></span> Not Found Data</td>
  </tr>

<?php 
$i3=0;
  $sql002 = mysqli_query($conn,"SELECT * FROM teacher_subject WHERE TEACH_ID = '$teach_id'");
  if(mysqli_num_rows($sql002)>0)
  {
      while($row002 = mysqli_fetch_assoc($sql002))
      {
        $subj_id = $row002['SUB_ID'];

         $sql0016 = mysqli_query($conn,"SELECT * FROM subject WHERE SUB_ID = '$subj_id'");
        while($row0016=mysqli_fetch_assoc($sql0016))
        {
          $sub_name00 = $row0016['SUBJECT_NAME'];

        }

        $sql003 = mysqli_query($conn,"SELECT * FROM classes WHERE SUB_ID = '$subj_id' AND TEACH_ID = '$teach_id'");
        while($row003 = mysqli_fetch_assoc($sql003))
        {
              $class_id = $row003['CLASS_ID'];

              $class_name = $row003['CLASS_NAME'];

              $i3++;

              $sql001 = mysqli_query($conn,"SELECT * FROM uploads WHERE CLASS_ID = '$class_id' AND TYPE = 'Document' ORDER BY UPLOAD_TIME DESC");
            
              if(mysqli_num_rows($sql001)>0)
              {
                  while($row001 = mysqli_fetch_assoc($sql001))
                  {
                    $up_file = $row001['FILE_NAME'];
                    $up_title = $row001['TITLE'];
                    $up_desc = $row001['DESCRIPTION'];
                    $up_time = $row001['UPLOAD_TIME'];
                    $up_id = $row001['UPLOAD_ID'];
                    $extention = $row001['EXTENSION'];
                    $file_types = 'audio/*';

                     if($up_file == '0' && $up_desc == '0')
                          {
                            $up_file =  "N/A";
                            $up_desc = '';
                          }
                    echo '

                <tr>

                  <td class="v-align-middle">'.$up_time.'</td>
                  <td class="v-align-middle">
                    
                    <h5 style="text-align:left;text-transform: capitalize;">'.$up_title.'</h5>
                    
                  </td>
                  <td class="v-align-middle">'.$sub_name00.'</td>

                  <td class="v-align-middle">'.$class_name.'</td>

                  <td class="v-align-middle">';

                    /*<button class="btn btn-danger btn-rounded btn-xs" data-target="#modalFillIn" data-toggle="modal" ><span class="fa fa-question"></span>&nbsp; Info</button>*/

                    echo '
                    <center>
                    <button type="button" class="btn btn-complete  btn-xs btn-rounded" data-target="#edit_audio'.$up_id.'" data-toggle="modal" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;"><i class="pg-icon">edit</i></button>';?>


                    <button type="button" class="btn btn-info btn-rounded btn-xs" data-toggle="modal" data-target="#preview_doc<?php echo $up_id; ?>" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;"><i class="pg-icon">expand</i></button>
                    

                    <a href="../teacher/query/delete.php?delete_video_upload=<?php echo $up_id; ?>&&teach_id=<?php echo $teach_id; ?>&&type=document" onclick="return confirm('Are you sure Delete?')" class="btn btn-danger btn-rounded btn-xs" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;"><i class="pg-icon">trash_alt</i></a>

                  </center>

                    <div id="preview_doc<?php echo $up_id; ?>" class="modal fade" role="dialog">
                      <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content modal-lg">
                          
                          <div class="col-md-12" style="padding-bottom: 6px;padding-top: 6px;border-bottom: 1px solid #cccc;margin-bottom: 10px;">
                            <div class="row">
                              <div class="col-md-8"><h4>Preview</h4></div>
                              <div class="col-md-4"><button type="button" class="close" data-dismiss="modal">&times;</button></div>
                            </div>
                          </div>
                            
                          <div class="modal-body">
                            
                              <iframe src="../uploads/document/<?php echo $up_file; ?>" style="width: 100%;height: 400px;"></iframe>
                            
                            
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          </div>
                        </div>

                      </div>
                    </div>

                    <?php
                    echo '

                      <div class="modal fade slide-up disable-scroll" id="edit_audio'.$up_id.'" tabindex="-1" role="dialog" aria-hidden="false">
                      <div class="modal-dialog ">
                      <div class="modal-content-wrapper">
                      <div class="modal-content">
                      <div class="modal-header clearfix text-left">
                      <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
                      </button>
                      <h5>Edit Document</h5>
                      </div>
                      <div class="modal-body">


                      <form action="../teacher/query/update.php" method="POST" class="frm" enctype="multipart/form-data">
                                    <div class="form-group form-group-default input-group">
                                      <div class="form-input-group">
                                      <label>Title</label>
                                        <input type="text" name="title" class="form-control change_inputs"  placeholder="XXXXXXXX" value="'.$up_title.'" required onclick="this.select();" >
                                      </div>
                                    </div>

                                  <div class="form-group form-group-default" style="margin-top: 10px;">
                                    <label>Description</label>
                                    <textarea name="descip" required placeholder="XXXXXXXX" style="margin-bottom: 10px;height: 100px;" class="form-control change_inputs" onclick="this.select();" >'.$up_desc.'</textarea>
                                  </div>

                                  <div class="form-group form-group-default" style="margin-top: 10px;">
                                    <label>Subjects</label>
                                     <select class="form-control select_subject3'.$up_id.'" name="select_subject" required style="cursor: pointer;">
                                      <option value="'.$subj_id.'">'.$sub_name00.'</option>';

                                                      $sql0014 = mysqli_query($conn,"SELECT * FROM teacher_subject WHERE TEACH_ID = '$teach_id' AND SUB_ID != '$subj_id'");
                                                      while($row0014=mysqli_fetch_assoc($sql0014))
                                                      {
                                                        $sub_id = $row0014['SUB_ID'];

                                                        $sql0015 = mysqli_query($conn,"SELECT * FROM subject WHERE SUB_ID = '$sub_id'");
                                                        while($row0015=mysqli_fetch_assoc($sql0015))
                                                        {
                                                          $sub_name = $row0015['SUBJECT_NAME'];
                                                          $subject_id = $row0015['SUB_ID'];

                                                          echo '<option value='.$subject_id.'>'.$sub_name.'</option>';

                                                        }

                                                      } 

                                                      ?>

                                                      <script type="text/javascript">
  
                                                        $(document).ready(function(){  
                                                         $('.select_subject3<?php echo $up_id; ?>').change(function(){

                                                           var subject = $(this).val();
                                                           var teach_id1 = $('#teach_id1').val();

                                                           $.ajax({
                                                            url:'../teacher/query/check.php',
                                                            method:"POST",
                                                            data:{update_media:subject,teach_id1:teach_id1},
                                                            success:function(data)
                                                            {
                                                              //alert(data)
                                                                $('#select_clz3<?php echo $up_id; ?>').html(data);
                                                              
                                                            }
                                                           })
                                                         

                                                        });
                                                       });

                                                      </script>

                                                      <?php
                                      
                                    echo '</select>
                                  </div>

                                  <input type="hidden" id="teach_id1" value="'.$teach_id.'">
                                  <input type="hidden" name="type" value="Audio">
                                  <input type="hidden" name="recent_file" value="'.$up_file.'">
                                  <input type="hidden" name="teach_id" value="'.$teach_id.'">


                                  <div class="form-group form-group-default" style="margin-top: 10px;">
                                    <label>Classes</label>
                                    <select class="form-control" id="select_clz3'.$up_id.'" name="clz" required style="cursor: pointer;">
                                      <option value="'.$class_id.'">'.$class_name.'</option>
                                    </select>
                                  </div>

                                  <div class="form-group form-group-default" style="margin-top: 10px;">
                                    <label>Upload File </label>';

                                    $file_types = '.xlsx,.xls,.doc, .docx,.ppt, .pptx,.txt,.pdf';

                                    ?> 

                                    <input type="file" id="selectedFile<?php echo $up_id; ?>" style="display: none;" class="form-control selectedFile" name="upload_file1" accept="<?php echo $file_types; ?>" required />

                                    <button type="button" class="btn btn-info active" style="padding: 20px 20px 20px 20px;width: 100%;height: 100px;border:3px dashed #cccc;font-size: 10px;font-weight: bold;color:white;" onclick="document.getElementById('selectedFile<?php echo $up_id; ?>').click();" id="select_btn<?php echo $up_id; ?>">

                                        <h6 id="h6_text"><span class="fa fa-plus" style="font-size:30px;"></span><br> Please choose audio file.</h6>
                                    </button>

                                    <?php echo '
                                  </div>

                                  <button type="submit" class="btn btn-success btn-block btn-lg" name="edit_document_btn" value="'.$up_id.'"><i class="pg-icon">tick_circle</i> Update</button>
                                </form>


                                  </div>
                                  </div>
                                  </div>
                                  </div>

                        </div>
                      </div>


                  </td>
                </tr>';
                

                }
                }else
                if(mysqli_num_rows($sql001)== '0')
                {     if($i == '1')
                      {
                        echo '<tr><td colspan="5" class="text-danger text-center"><span class="fa fa-warning text-danger"></span> Empty Data!</td></tr>';
                      }
                         

                }
            }}
        }
 ?>





</tbody>
</table>         

</div>
</div>
</div>


<div class="tab-pane <?php echo $record; ?>" id="record">
<div class="row column-seperation">



<div class="col-lg-12">
  
  <div class="row">
    <div class="col-md-8">

      <div class="pull-left">
        <h3>
          <span class="semi-bold"><span class="fa fa-circle text-danger"></span>&nbsp;Recordings</span>
        </h3>
      </div>

    </div>
    <div class="col-md-4">
        <input type="text" id="myInput1" class="form-control pull-right" style="width: 100%;" placeholder="Search" data-toggle="tooltip" data-title="අවශ්‍ය දත්ත සෙවීම'">
      
  </div>

</div>



<div class="table-responsive" style="height: 800px;overflow: auto;margin-top: 4px;">
<table class="table demo-table-search table-responsive-block text-left" id="tableWithSearch">
<thead>
<tr>
<th>Upload Time</th>
<th>Title</th>
<th style="width: 10%;">Subject Name</th>
<th style="width: 10%;">Class Name</th>
<th class="text-center">Action</th>
</tr>
</thead>
<tbody id="myTable1">

<tr class="no-data1 col-md-12 alert alert-danger" style="margin-top: 20px;display: none;text-align: center;">
  <td colspan="6" class="text-danger"><span class="fa fa-warning"></span> Not Found Data</td>
</tr>

<?php 

  $i = 0;

  $sql002 = mysqli_query($conn,"SELECT * FROM teacher_subject WHERE TEACH_ID = '$teach_id'");
  
      while($row002 = mysqli_fetch_assoc($sql002))
      {
        $subj_id = $row002['SUB_ID'];

         $sql0016 = mysqli_query($conn,"SELECT * FROM subject WHERE SUB_ID = '$subj_id'");
        while($row0016=mysqli_fetch_assoc($sql0016))
        {
          $sub_name00 = $row0016['SUBJECT_NAME'];
        }

        $sql003 = mysqli_query($conn,"SELECT * FROM classes WHERE SUB_ID = '$subj_id' AND TEACH_ID = '$teach_id'");
        while($row003 = mysqli_fetch_assoc($sql003))
        {
              $class_id = $row003['CLASS_ID'];

              $class_name = $row003['CLASS_NAME'];

              $i++;

              $sql001 = mysqli_query($conn,"SELECT * FROM uploads WHERE CLASS_ID = '$class_id' AND TYPE = 'Record' ORDER BY UPLOAD_ID DESC");
              
              if(mysqli_num_rows($sql001)>0)
                {

                  while($row001 = mysqli_fetch_assoc($sql001))
                  {
                    $up_file = $row001['FILE_NAME'];
                    $up_title = $row001['TITLE'];
                    $up_desc = $row001['DESCRIPTION'];
                    $up_time = $row001['UPLOAD_TIME'];
                    $up_id = $row001['UPLOAD_ID'];
                    $extention = $row001['EXTENSION'];
                    $ytd = $row001['YOUTUBE_LINK'];

                     if($up_file == '0' && $up_desc == '0')
                          {
                            $up_file =  "N/A";
                            $up_desc = '';
                          }
                    echo '

                      <tr>';

                      /*<video controls style="border:0px solid black;width: 100%;height: 150px;outline: none;" class="image-responsive">
                        <source src="video/'.$up_file.'" type="video/'.$extention.'">
                      </video>*/

                      echo '

                  <td class="v-align-middle">'.$up_time.'</td>

                  <td class="v-align-middle"><h5 style="text-align:left;text-transform: capitalize;">'.$up_title.'</h5></td>

                  <td class="v-align-middle">'.$sub_name00.'</td>

                  <td class="v-align-middle">'.$class_name.'</td>

                  <td class="v-align-middle">';

                    /*<button class="btn btn-danger btn-rounded btn-xs" data-target="#modalFillIn" data-toggle="modal" ><span class="fa fa-question"></span>&nbsp; Info</button>*/

                    echo '
                    
                    <center>
                    
                    <button type="button" class="btn btn-complete  btn-xs btn-rounded" data-target="#edit_recording'.$up_id.'" data-toggle="modal" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;"><i class="pg-icon">edit</i></button>';?>


                     <a href="<?php echo $emb_link ?>" target="_blank" type="button" class="btn btn-info btn-rounded btn-xs" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;display: none;"><i class="pg-icon">link</i></button>

                    <a href="../teacher/query/delete.php?delete_video_upload=<?php echo $up_id; ?>&&teach_id=<?php echo $teach_id; ?>&&type=Record" onclick="return confirm('Are you sure Delete?')" class="btn btn-danger btn-rounded btn-xs" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;"><i class="pg-icon">trash_alt</i></a>


                    <?php

                    echo '</center>';

                    echo '

                      <div class="modal fade slide-up disable-scroll" id="edit_recording'.$up_id.'" tabindex="-1" role="dialog" aria-hidden="false">
                      <div class="modal-dialog ">
                      <div class="modal-content-wrapper">
                      <div class="modal-content">
                      <div class="modal-header clearfix text-left">
                      <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
                      </button>
                      <h5>Edit Recording</h5>
                      </div>
                      <div class="modal-body">


                      <form action="../teacher/query/update.php" method="POST" class="frm" enctype="multipart/form-data">

                                    <div class="form-group form-group-default input-group">
                                      <div class="form-input-group">
                                      <label>Title</label>
                                        <input type="text" name="title" class="form-control change_inputs"  placeholder="XXXXXXXX" value="'.$up_title.'" required onclick="this.select();" >
                                      </div>
                                    </div>

                                  <div class="form-group form-group-default" style="margin-top: 10px;">
                                    <label>Description</label>
                                    <textarea name="descip" required placeholder="XXXXXXXX" style="margin-bottom: 10px;height: 100px;" class="form-control change_inputs" onclick="this.select();" >'.$up_desc.'</textarea>
                                  </div>

                                  <div class="form-group form-group-default" style="margin-top: 10px;">
                                    <label>Subjects</label>
                                     <select class="form-control select_subject1'.$up_id.'" name="select_subject" required style="cursor: pointer;">
                                      <option value="'.$subj_id.'">'.$sub_name00.'</option>';

                                                      $sql0014 = mysqli_query($conn,"SELECT * FROM teacher_subject WHERE TEACH_ID = '$teach_id' AND SUB_ID != '$subj_id'");
                                                      while($row0014=mysqli_fetch_assoc($sql0014))
                                                      {
                                                        $sub_id = $row0014['SUB_ID'];

                                                        $sql0015 = mysqli_query($conn,"SELECT * FROM subject WHERE SUB_ID = '$sub_id'");
                                                        while($row0015=mysqli_fetch_assoc($sql0015))
                                                        {
                                                          $sub_name = $row0015['SUBJECT_NAME'];
                                                          $subject_id = $row0015['SUB_ID'];

                                                          echo '<option value='.$subject_id.'>'.$sub_name.'</option>';

                                                        }

                                                      } 

                                                      ?>

                                                      <script type="text/javascript">
  
                                                        $(document).ready(function(){  
                                                         $('.select_subject1<?php echo $up_id; ?>').change(function(){

                                                           var subject = $(this).val();
                                                           var teach_id1 = $('#teach_id1').val();

                                                           $.ajax({
                                                            url:'../teacher/query/check.php',
                                                            method:"POST",
                                                            data:{update_media:subject,teach_id1:teach_id1},
                                                            success:function(data)
                                                            {
                                                              //alert(data)
                                                                $('#select_clz1<?php echo $up_id; ?>').html(data);
                                                              
                                                            }
                                                           })
                                                         

                                                        });
                                                       });

                                                      </script>

                                                      <?php
                                      
                                    echo '</select>
                                  </div>

                                  <input type="hidden" id="teach_id1" value="'.$teach_id.'">

                                  <input type="hidden" name="teach_id" value="'.$teach_id.'">
                                  <input type="hidden" name="type" value="Video">


                                  <div class="form-group form-group-default" style="margin-top: 10px;">
                                    <label>Classes</label>
                                    <select class="form-control" id="select_clz1'.$up_id.'" name="clz" required style="cursor: pointer;">
                                      <option value="'.$class_id.'">'.$class_name.'</option>
                                    </select>
                                  </div>
';


                                  $active_up_file = 'active';
                                  $active_url = '';

                                  if($up_file !== '0')
                                  {
                                    echo '<label Class="text-danger"><span class="fa fa-upload"></span> File Name - '.$up_file.'</label>';

                                    $active_up_file = 'active';
                                    $active_url = '';

                                  }else
                                  if($up_file == '0')
                                  {
                                    $active_up_file = '';
                                    $active_url = 'active';
                                  }

                                 ?>

                                  <ul class="nav nav-tabs">
                                    <li class="<?php echo $active_up_file; ?>"><a data-toggle="tab" href="#up_file<?php echo $up_id; ?>" onclick="document.getElementById('record_link').value = '';"><i class="fa fa-upload"></i> Upload File</a></li>
                                    <li class="<?php echo $active_url; ?>"><a data-toggle="tab" href="#link<?php echo $up_id; ?>" onclick="document.getElementById('file_name_id').value = ''; uploaded_record_file();"><i class="fa fa-link"></i> Setup Link</a></li>
                                  </ul>

                                  <div class="tab-content" style="margin-bottom: 10px;padding: 0px;">
                                    <div id="up_file<?php echo $up_id; ?>" class="tab-pane in <?php echo $active_up_file; ?>" style="padding: 0px;">
                                      
                                      <div style="margin-top: 10px;">
                                        <input type="hidden" name="current_file" value="<?php echo $up_file; ?>">

                                        <input type="file" name="upload_file" class="form-control" id="file_name_id" accept="video/*"/>
                                      </div>

                                    </div>
                                    <div id="link<?php echo $up_id; ?>" class="tab-pane in <?php echo $active_url; ?>">
                                      
                                      <div class="form-group form-group-default" style="margin-top: 10px;">
                                        <label>Zoom Recording Link </label>
                                        <input type="text" name="record_link" class="form-control change_inputs"  placeholder="Zoom Recording Link.." id="record_link" value="<?php echo $ytd; ?>">
                                      </div>

                                    </div>
                                  </div>
                                  <?php 

                                  echo '
                                  <button type="submit" class="btn btn-success btn-block btn-lg" name="edit_record_btn" value="'.$up_id.'"><i class="pg-icon">tick_circle</i> Update</button>
                                </form>


                                  </div>
                                  </div>
                                  </div>
                                  </div>

                        </div>
                      </div>


                  </td>
                </tr>';

            }
            }else
            if(mysqli_num_rows($sql001)== '0')
            {     if($i == '1')
                  {
                    echo '<tr><td colspan="5" class="text-danger text-center"><span class="fa fa-warning text-danger"></span> Empty Data!</td></tr>';
                  }
                     

            }
          }
        }
      
 ?>

</tbody>
</table>
</div>

              
</div>
</div>
</div>  


</div>
</div>



      </div>
      <div class="col-md-1"></div>
    </div>


  </div>





<script type="text/javascript">
  $('.save_btn').click(function(){
    
    var upload_btn0 = $('#selectedFile0').val();
    var upload_btn = $('#sel_file').val();
      
    if(empty(upload_btn0))
    {
      alert('Please select your audio file.');
    }else
    if(empty(upload_btn))
    {
      alert('Please select your document file.');
    }
   });
</script>

<script type="text/javascript">
  $('.navi').click(function(){
    $('.frm')[0].reset();
      
   });
</script>

<script>
$(document).ready(function(){
  $("#myInput1").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable1 tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

<!-- search data -->

<!-- no data message/no found data show in search-->

<script type="text/javascript">
  $(document).ready(function () {

    (function ($) {

        $('#myInput1').keyup(function () {
            var rex = new RegExp($(this).val(), 'i');
            $('#myTable1 tr').hide();
            $('#myTable1 tr').filter(function () {
                return rex.test($(this).text());
            }).show();
            $('.no-data1').hide();
            if($('#myTable1 tr:visible').length == 0)
            {
                $('.no-data1').show();
            }

        })

    }(jQuery));

});
</script>

<script>
$(document).ready(function(){
  $("#myInput2").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable2 tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

<!-- search data -->

<!-- no data message/no found data show in search-->

<script type="text/javascript">
  $(document).ready(function () {

    (function ($) {

        $('#myInput2').keyup(function () {
            var rex = new RegExp($(this).val(), 'i');
            $('#myTable2 tr').hide();
            $('#myTable2 tr').filter(function () {
                return rex.test($(this).text());
            }).show();
            $('.no-data2').hide();
            if($('#myTable2 tr:visible').length == 0)
            {
                $('.no-data2').show();
            }

        })

    }(jQuery));

});
</script>


<script>
$(document).ready(function(){
  $("#myInput3").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable3 tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

<!-- search data -->

<!-- no data message/no found data show in search-->

<script type="text/javascript">
  $(document).ready(function () {

    (function ($) {

        $('#myInput3').keyup(function () {
            var rex = new RegExp($(this).val(), 'i');
            $('#myTable3 tr').hide();
            $('#myTable3 tr').filter(function () {
                return rex.test($(this).text());
            }).show();
            $('.no-data3').hide();
            if($('#myTable3 tr:visible').length == 0)
            {
                $('.no-data3').show();
            }

        })

    }(jQuery));

});
</script>




<?php include('footer/footer.php'); ?>