

<?php
  $page = "Teacher Pending";
  $folder_in = '3';

      include('header/header.php');

        $s04 = mysqli_query($conn,"SELECT * FROM admin_login WHERE ADMIN_ID = '$admin_id'");
        while($row4 = mysqli_fetch_assoc($s04))
        {
          $name = $row4['ADMIN_NAME'];
        }

 ?>

    <ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-bottom: 04%;margin-bottom: 0;">
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="Teacher Pending"> Teacher Pending</li>
    </ol>


      <div class="col-md-12 bg-white" style="border:1px solid #cccc;height: auto;">
      
      <div class="col-md-12" style="border-bottom: 1px solid #cccc;">
        <div class="row">

            <div class="col-md-9" style="margin-bottom: 10px;"><h2><a href="dashboard.php" data-toggle="tooltip" data-title="Back"><i class="pg-icon" style="font-size: 22px;">chevron_left</i></a> Teacher Pending</h2></div>

        </div>
      </div>

          
          <div class=" container-fluid container-fixed-lg bg-white" style="margin-top: 0px;">
            <div class="card card-transparent">
              <div class="card-header ">
                    <div class="card-title">
                    </div>
                    <div>
                      <div class="row">
                        <div class="col-md-6">
                        </div>
                      <div class="col-md-2"></div>
                      <div class="col-md-4">
                      <div class="col-md-12" style="padding-top: 18px;"></div>
                      <input type="text" id="search-table" class="form-control pull-right" placeholder="Search">
                      </div>

                    </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="card-body">
                <table class="table table-hover demo-table-search table-responsive-block" id="tableWithSearch">
                  <thead>
                      <tr>
                        <th style="width: 1%;" class="text-danger">Registered Date</th>
                        <th  style="width: 1%;">Teacher Name</th>
                        <th style="width: 1%;">TP No</th>
                        <th style="width: 1%;">DOB</th>
                        <th style="width: 1%;">Gender</th>
                        <th style="width: 1%;">Status</th>
                        <th style="width: 1%;text-align: center;">Action</th>
                      </tr>
                    </thead>
                    <tbody>

                      <?php 

                          $today = date('Y-m-d h:i:s A');

                          //mysqli_query($conn,"UPDATE stu_login SET REG_DATE = '$today'");

                          $sql001 = mysqli_query($conn,"SELECT * FROM `teacher_login` WHERE `STATUS` = 'Deactive' ORDER BY `REG_DATE` DESC");

                          $check = mysqli_num_rows($sql001);

                          if($check>0){
                          while($row001 = mysqli_fetch_assoc($sql001))
                          {
                            $teach_id = $row001['TEACH_ID'];
                            $register_date = $row001['REG_DATE'];

                            $sql002 = mysqli_query($conn,"SELECT * FROM `teacher_details` WHERE `TEACH_ID` = '$teach_id'");
                            while($row002 = mysqli_fetch_assoc($sql002))
                            {
                              $name = $row002['POSITION'].". ".$row002['F_NAME']." ".$row002['L_NAME'];
                              $tp = $row002['TP'];
                              $dob = $row002['DOB'];
                              $gender = $row002['GENDER'];
                              $email = $row002['EMAIL'];
                            }
                            

                            echo '

                              <tr>
                                <td class="v-align-left text-danger">'.$register_date.'</td>
                                <td class="v-align-middle">'.$name.'</td>
                                <td class="v-align-middle">'.$tp.'</td>
                                <td class="v-align-middle">'.$dob.'</td>
                                <td class="v-align-middle">'.$gender.'</td>
                                <td class="v-align-middle">
                                <label class="label label-warning"> <span class="fa fa-spinner"></span> Pending</label></td>
                                 <td class="v-align-middle text-center">';

                                ?>
                                <a href="../admin/query/update.php?approve_teach=<?php echo $teach_id; ?>&&approve=Approve" class="btn btn-success  btn-xs btn-rounded" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;" data-title="Approve" data-toggle="tooltip" onclick="return confirm('Are you sure Active?')"><i class="pg-icon">tick_circle</i></a>
                                
                                <a href="../admin/query/update.php?approve_teach=<?php echo $teach_id; ?>&&approve=Disapprove" onclick="return confirm('Are you sure disapprove?')" class="btn btn-danger  btn-xs btn-rounded" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;" data-title="Disapprove" data-toggle="tooltip"><i class="pg-icon">close</i></a>

                                <?php echo '

                                </td>
                              </tr>

                            ';
                          }
                        }
                      
                         ?>


                    </tbody>
                </table>
              </div>
            </div>
            </div>
            </div>
            </div>

<?php include('footer/footer.php'); ?>