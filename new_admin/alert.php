<?php
	$page = "Alert Management";
	$folder_in = '0';

      include('header/header.php');

        $s04 = mysqli_query($conn,"SELECT * FROM admin_login WHERE ADMIN_ID = '$admin_id'");
        while($row4 = mysqli_fetch_assoc($s04))
        {
          $name = $row4['ADMIN_NAME'];
        }

 ?>

    <ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-bottom: 04%;margin-bottom: 0;">
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="Alert Management"> Alert Management</li>
    </ol>


  <div class="col-md-12">
    
    <div class="row" style="padding-top: 10px;">
      <div class="col-md-1"></div>
      <div class="col-md-10 bg-white" style="border:1px solid #cccc;height: auto;">
      
      <div class="col-md-12" style="border-bottom: 1px solid #cccc;">
        <div class="row">

            <div class="col-md-9" style="margin-bottom: 10px;"><h2><a href="dashboard.php" data-toggle="tooltip" data-title="Back"><i class="pg-icon" style="font-size: 22px;">chevron_left</i></a> Alert Management</h2></div>

            <div class="col-md-3">
                <a href="new_alert.php" data-toggle="tooltip" data-title="Create New Alert" class="btn btn-success btn-rounded" style="float: right;padding: 10px 10px;margin-top: 10px;color:white;"><i class="pg-icon">plus</i></a>



            </div>

        </div>
      </div>
          

<div class="card card-borderless">
<div class="col-md-12" style="padding-top:10px;">

<div class="tab-content">
<div class="tab-pane active" id="home">
<div class="row column-seperation">



<div class="col-lg-12">
  
  <div class="row">
    <div class="col-md-8">

      <div class="pull-left">
      </div>

    </div>
    <div class="col-md-4">
        <input type="text" id="myInput" class="form-control pull-right" style="width: 100%;" placeholder="Search" data-toggle="tooltip" data-title="අවශ්‍ය දත්ත සෙවීම'">
      
  </div>

</div>

<?php 

$sql00100 =  mysqli_query($conn,"SELECT * FROM teacher_login WHERE STATUS = 'Active' ORDER BY REG_DATE DESC");

$count = mysqli_num_rows($sql00100);

 ?>
<!-- <label class="label label-success"><?php //echo  $count; ?></label> -->

<div class="table-responsive" style="height: 550px;overflow: auto;margin-top: 4px;">
<table class="table demo-table-search table-responsive-block text-left" id="tableWithSearch" style="font-size: 12px;">
<thead>
<tr>
<th style="width: 16%;">Publish Date</th>
<th style="width: 14%;">Title</th>
<th style="width: 10%;">Message</th>
<th style="width: 10%;">Audience</th>
<th style="width: 10%;">Published</th>

<th>Action</th>
</tr>
</thead>
<tbody id="myTable">

<tr class="no-data alert alert-danger" style="margin-top: 20px;display: none;">
  <td colspan="5" class="text-danger"><span class="fa fa-warning"></span> Not Found Data</td>
</tr>

<?php 

  $today = date('Y-m-d h:i:s A');

  //mysqli_query($conn,"UPDATE teacher_login SET REG_DATE = '$today'");

  $sql001 = mysqli_query($conn,"SELECT * FROM alert ORDER BY PUB_DATE DESC");

  $check = mysqli_num_rows($sql001);

  if($check>0){
  while($row001 = mysqli_fetch_assoc($sql001))
  
  {
    $alert_id = $row001['ALERT_ID'];
    $title = $row001['TITLE'];
    $add_date = $row001['PUB_DATE'];
    $message = $row001['MESSAGE'];
    $aud = $row001['AUDIENCE'];
    $publish = $row001['PUBLISH'];

    if($publish == 'Yes')
    {
      $st = "success";
    }else
    if($publish == 'No')
    {
      $st = "danger";
    }

    if($message == '' || $message == '0')
    {
      $message = 'N/A';
    }
    

    echo '

      <tr>
        <td class="v-align-middle">'.$add_date.'</td>
        <td><div style="width:120px;overflow-x:auto;height:100px;">'.$title.'</div></td>
        <td><div style="width:300px;overflow-x:auto;height:100px;">'.$message.'</div></td>
        <td class="v-align-middle">'.$aud.'</td>
        <td class="v-align-middle"><label class="label label-'.$st.'">'.$publish.'</label></td>
         <td class="v-align-middle" style="padding:0;">
         ';
         if($publish == 'Yes')
         {?>
         <a href="../admin/query/update.php?publish=<?php echo $alert_id; ?>&&approve=OK" onclick="return confirm('Are you sure Disable Publish?')" class="btn btn-danger  btn-xs btn-rounded" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;margin-right: 4px;" data-title="Disable Publish" data-toggle="tooltip"><i class="pg-icon">close</i></a><?php

       }else
       if($publish == 'No')
       {?>

         <a href="../admin/query/update.php?publish=<?php echo $alert_id; ?>&&approve=NO" onclick="return confirm('Are you sure Enable Publish?')" class="btn btn-success  btn-xs btn-rounded" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;margin-right: 4px;" data-title="Enable Publish" data-toggle="tooltip"><i class="pg-icon">tick</i></a><?php     

       }
        echo '<a href="edit_alert.php?alert_id='.$alert_id.'" data-toggle="tooltip" data-title="Edit Alert" class="btn btn-primary btn-xs btn-rounded" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;"><i class="pg-icon">edit</i></a>
        '; ?>
        <a href="../admin/query/delete.php?delete_alert=<?php echo $alert_id; ?>" onclick="return confirm('Are you sure delelete alert?')" class="btn btn-danger  btn-xs btn-rounded" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;" data-title="Delete Alert Message" data-toggle="tooltip"><i class="pg-icon">trash_alt</i></a>

        <?php echo '

        </td>
      </tr>

    ';
?>
    
<?php

  }
}else
if($check == '0')
{
  echo '<tr><td colspan="6" class="text-center text-danger"><span class="fa fa-warning"></span> Not Found Data</td></tr>';
}

 ?>


</tbody>
</table>
</div>

              
</div>
</div>
</div>
</div>




</div>
</div>



      </div>
      <div class="col-md-1"></div>
    </div>


  </div>





<script type="text/javascript">
  $('.save_btn').click(function(){
    
    var upload_btn0 = $('#selectedFile0').val();
    var upload_btn = $('#sel_file').val();
      
    if(empty(upload_btn0))
    {
      alert('Please select your audio file.');
    }else
    if(empty(upload_btn))
    {
      alert('Please select your document file.');
    }
   });
</script>

<script type="text/javascript">
  $('.navi').click(function(){
    $('.frm')[0].reset();
      
   });
</script>

<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
<!-- search data -->

<!-- no data message/no found data show in search-->

<script type="text/javascript">
  $(document).ready(function () {
 
    (function ($) {

        $('#myInput').keyup(function () {
            var rex = new RegExp($(this).val(), 'i');
            $('#myTable tr').hide();
            $('#myTable tr').filter(function () {
                return rex.test($(this).text());
            }).show();
            $('.no-data').hide();
            if($('#myTable tr:visible').length == 0)
            {
                $('.no-data').show();
            }

        })

    }(jQuery));

});
</script>




<?php include('footer/footer.php'); ?>