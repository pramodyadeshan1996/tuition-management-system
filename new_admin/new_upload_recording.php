

<?php
  $page = "Recordings";
  $folder_in = '3';

      include('header/header.php');

        $s04 = mysqli_query($conn,"SELECT * FROM admin_login WHERE ADMIN_ID = '$admin_id'");
        while($row4 = mysqli_fetch_assoc($s04))
        {
          $name = $row4['ADMIN_NAME'];
        }

 ?>

 <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

    <ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-bottom: 04%;margin-bottom: 0;">
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="Todo List"> Recordings</li>
    </ol>


      <div class="col-md-12 bg-white" style="border:1px solid #cccc;height: auto;">
      
        <div class="col-md-12" style="border-bottom: 1px solid #cccc;">
          <div class="row">

              <div class="col-md-9" style="margin-bottom: 10px;"><h2><a href="dashboard.php" data-toggle="tooltip" data-title="Back"><i class="pg-icon" style="font-size: 22px;">chevron_left</i></a> Recordings</h2></div>

          </div>
        </div>

          
          <div class=" container-fluid container-fixed-lg bg-white" style="margin-top: 0px;">
            <div class="card card-transparent">

                <ul class="nav nav-tabs">
                  <li class="active"><a data-toggle="tab" href="#home">Home</a></li>
                  <li><a data-toggle="tab" href="#menu1">Menu 1</a></li>
                  <li><a data-toggle="tab" href="#menu2">Menu 2</a></li>
                  <li><a data-toggle="tab" href="#menu3">Menu 3</a></li>
                </ul>

                <div class="tab-content">
                  <div id="home" class="tab-pane fade in active">

                    <div class="row">
                      <div class="col-md-4"></div>
                      <div class="col-md-4" style="padding: 20px 20px 20px 20px;border: 1px solid #cccc;">

                        <h3 class="text-center"><span class="fa fa-upload"></span> Upload Recording</h3>
                        <hr>
                        <form action="" method="POST" enctype="multipart/form-data">

                            <h4 style="font-weight: bold;">Title</h4>
                            <input type="text" name="title" placeholder="Enter Title.." class="form-control">

                            <h4 style="font-weight: bold;">Description</h4>
                            <textarea name="title" placeholder="Enter Description.." class="form-control"></textarea>
                            
                            <div style="margin-top: 10px;">
                              <input type="file" id="my_selected_file" name="upload_file" required style="display: none;" onchange="uploaded_record_file();"/>

                              <div style="padding: 0px;margin: 0px;" class="pt-10" id="changed_btn" onclick="document.getElementById('my_selected_file').click();">
                                  <button type="button" class="btn btn-block btn-sm" name="create_payment" id="upload_btn" style="padding: 15px 20px 15px 20px;border:3px dashed gray;font-size: 18px;outline: none;background-color: white;" value="Browse..."> <i class="fa fa-plus" style="font-size: 20px;"></i> <label style="font-weight: bold;font-size: 20px;">Upload Recording File</label>


                                  </button>
                              </div>
                            </div>
                              <label id="required_msg"></label>

                              <button class="btn btn-success btn-block" style="background-color: #1abc9c;font-size: 18px;" onclick="check_input();">
                                <span class="fa fa-check-circle"></span> Submit
                              </button>

                          
                        </form>
                      </div>
                      <div class="col-md-4"></div>
                    </div>
                    
                  </div>
                  <div id="menu1" class="tab-pane fade">
                    <h3>Menu 1</h3>
                    <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                  </div>
                  <div id="menu2" class="tab-pane fade">
                    <h3>Menu 2</h3>
                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
                  </div>
                  <div id="menu3" class="tab-pane fade">
                    <h3>Menu 3</h3>
                    <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>


<?php include('footer/footer.php'); ?>
              
              <button type="button" data-toggle="modal" data-target="#submit_modal" id="open_modal_btn"></button>
                <div id="submit_modal" class="modal fade" role="dialog" style="margin-top: 100px;" data-backdrop="static" data-keyboard="false">
                  <div class="modal-dialog modal-sm">

                    <!-- Modal content-->
                    <div class="modal-content">
                      <div class="modal-body">
                          
                        <img src="../student/images/uploading.gif" width="100%" height="200px">
                        <h4 class="text-muted" style="position: fixed;bottom: 40px;right: 100px;font-weight: bold;">Uploading..</h4>

                        <center><small class="text-center" style="text-align: center;position: fixed;bottom: 30px;left: 55px;">Please Don't close or reload this page.</small></center>

                      </div>
                    </div>

                  </div>
                </div>


                  <script type="text/javascript">

                    function check_input()
                    {
                      var my_selected_file = document.getElementById('my_selected_file').value;

                      if(my_selected_file == '')
                      {
                        document.getElementById('required_msg').innerHTML = '<h4 class="text-danger mt-4"><span class="fa fa-warning"></span> Please select upload file..</h4>';
                      }else
                      if(my_selected_file !== '')
                      {
                        document.getElementById('required_msg').innerHTML = '';

                        document.getElementById('open_modal_btn').click();
                      }
                    }
                    

                    function uploaded_record_file()
                    {
                        //alert('selectedFile1')
                        var selectedFile1 = document.getElementById('my_selected_file').value;


                        if(selectedFile1 !== '')
                        {
                          
                          document.getElementById('required_msg').innerHTML = ''; //file required message

                          document.getElementById('changed_btn').innerHTML = '<button type="button" class="btn btn-block btn-sm" id="upload_btn" style="padding: 20px 20px 20px 20px;font-size: 18px;outline: none;background-color: #ff6500;color:white;" value="Browse..." > <i class="fa fa-spinner fa-pulse" style="font-size: 22px;"></i> <label style="font-weight: 1000px;font-size: 22px;">Changing..</label></button>';

                          setTimeout(function() { 

                            document.getElementById('changed_btn').innerHTML = '<button type="submit" class="btn btn-block btn-sm" name="create_payment" id="upload_btn" style="padding: 20px 20px 20px 20px;font-size: 22px;outline: none;background-color: #16a085;color:white;" value="Browse..." > <i class="fa fa-check-circle"></i> <label style="font-weight: 700px;font-size: 22px;">Selected successfully</label></button>';
                                
                            document.getElementById('slip_upload_btn').disabled = false;

                          }, 1000);

                        }else
                        if(selectedFile1 == '')
                        {
                          document.getElementById('required_msg').innerHTML = ''; //file required message

                          document.getElementById('changed_btn').innerHTML = '<button type="submit" class="btn btn-block btn-sm" name="create_payment" id="upload_btn" style="padding: 20px 20px 20px 20px;border:3px dashed gray;font-size: 18px;outline: none;background-color: white;" value="Browse..."> <i class="fa fa-plus" style="font-size: 22px;"></i> <label style="font-weight: 1000px;font-size: 22px;">Upload Recording File</label></button>';

                          document.getElementById('slip_upload_btn').disabled = true;

                        }

                        
                    }

                  </script>