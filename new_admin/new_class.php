
<?php

$page = "New Classes";
$folder_in = '0';

  include('header/header.php'); 
  ini_set( "display_errors", 0); 

  $teach_id = $_GET['teach_id'];
  $s04 = mysqli_query($conn,"SELECT * FROM teacher_details WHERE TEACH_ID = '$teach_id'");
  while($row4 = mysqli_fetch_assoc($s04))
  {
    $teacher_name = $row4['POSITION'].". ".$row4['F_NAME']." ".$row4['L_NAME'];
    $show_teacher_name = "(".$teacher_name.")";
  }


  ?>
  <style type="text/css">
    .btn_pulse {
  
  display: block;
  border-radius: 10%;
  cursor: pointer;
  animation: none;
  float: left;
  bottom: 5px;
  right: 5px;
  font-weight: bold;
  padding-top: 2px;
  text-align: center;
  color: white;
  font-size: 14px;
  animation: pulse 1.6s infinite;
}


@keyframes pulse {
  0% {
    -moz-box-shadow: 0 0 0 0 #7252d3;
    box-shadow: 0 0 0 0 #7252d3;
  }
  70% {
    -moz-box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
  }
  100% {
    -moz-box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
  }
}
  </style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<div class="row" style="border-bottom: 1px solid #cccc;margin-top: 10px;">
  <div class="col-md-12">
    <ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-top: 04%;margin-bottom: 0;">
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="New Classes"> New Classes</li>
    </ol>
  </div>
  </div>


         <div class="modal fade slide-up disable-scroll" id="create_class<?php echo $teach_id; ?>" tabindex="-1" role="dialog" aria-hidden="false" style="overflow: auto;">
          <div class="modal-dialog ">
          <div class="modal-content-wrapper">
          <div class="modal-content modal-lg">
          <div class="modal-header clearfix text-left">
          <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
          </button>
          <h5>New Classes</h5>
          
          
          </div>
          <div class="modal-body">
            
            <form action="../admin/query/insert.php" method="POST">

            <div class="row">
              <div class="col-md-6">
                
                <div class="form-group form-group-default" style="margin-top: 10px;">
                  <input type="hidden" id="teacher_id" value="<?php echo $teach_id; ?>">
                  <label>Classes</label>
                  <select class="form-control select_class" name="clz" id="level_clz" required style="cursor: pointer;">
                    <option value="">Select Classes </option>

                      <?php 

                          $sql0015 = mysqli_query($conn,"SELECT * FROM level");
                          while($row0015=mysqli_fetch_assoc($sql0015))
                          {
                            $level_name = $row0015['LEVEL_NAME'];
                            $level_id = $row0015['LEVEL_ID'];

                            echo '<option value='.$level_id.'>'.$level_name.'</option>';

                          }

                           ?>
                    
                  </select>
                </div>

              </div>
              <div class="col-md-6">
                
                <div class="form-group form-group-default" style="margin-top: 10px;">
                  <label>Subject</label>
                  <select class="form-control"  id="subject"  name="subject" required style="cursor: pointer;">
                    <option value="">Select Subject</option>
                  </select>
                </div>

              </div>
            </div>

            <div class="row">
              <div class="col-md-6">
                
                <div class="form-group form-group-default" style="margin-top: 10px;">
                  <label>Class Name</label>
                  
                  <input type="text" name="clz_name" class="form-control change_inputs"  placeholder="XXXXXXXX" required >

                </div>

              </div>
                <div class="col-md-6">

                <div class="form-group form-group-default" style="margin-top: 10px;">
                  <label>Help Contact No</label>
                  
                  <input type="text" name="contact_no" class="form-control check_contact" value="0" maxlength="10" required>

                </div>


              </div>
            </div>
            
            <div class="row">
              <div class="col-md-6">

                <div class="form-group form-group-default" style="margin-top: 10px;">
                  <label>Start Time</label>
                  
                  <input type="time" name="start" class="form-control change_inputs" required>

                </div>

              </div>
              <div class="col-md-6">

                <div class="form-group form-group-default" style="margin-top: 10px;">
                  <label>End Time</label>
                  
                  <input type="time" name="end" class="form-control change_inputs" required>

                </div>

              </div>
            </div>
            

            <div class="row">
              
              <div class="col-md-6">
                  
                  <div class="form-group form-group-default" style="margin-top: 10px;">
                    <label>Class Fees</label>
                    
                    <input type="number" name="clz_fees" class="form-control change_inputs" placeholder="XXXXXXXX" value="0" onclick="this.select();" required>

                  </div>

              </div>
              <div class="col-md-6">
                
                <div class="form-group form-group-default" style="margin-top: 10px;">
                  <label>Age Range</label>
                  
                  <input type="number" name="age_range" class="form-control change_inputs" value="0" min="0" required>

                </div>

              </div>
            </div>


            <script type="text/javascript">
              function checkbox(){
  
                var checkboxes = document.getElementsByName('weekdays');
                var checkboxesChecked = [];
                // loop over them all
                for (var i=0; i<checkboxes.length; i++) {
                   // And stick the checked ones onto an array...
                   if (checkboxes[i].checked) {
                      checkboxesChecked.push(checkboxes[i].value);
                   }
                }
                document.getElementById("show").value = checkboxesChecked;

              }
            </script>

            <div class="col-md-12">

            <div class="row">
              <div class="col-md-4">
                <label>
                <input type="checkbox" id="bk" name="weekdays" onClick="checkbox();" value="Monday" style="-ms-transform: scale(2);-moz-transform: scale(2);-webkit-transform: scale(2);-o-transform: scale(2); transform: scale(2);padding: 1px;margin-top: 12px;"> <b style="font-size: 16px;padding-left: 10px; ">Monday</b> &nbsp;
                <label>
              </div>
              <div class="col-md-4">
                <label>
                <input type="checkbox" id="cr" name="weekdays" onClick="checkbox();" value="Tuesday" style="-ms-transform: scale(2);-moz-transform: scale(2);-webkit-transform: scale(2);-o-transform: scale(2); transform: scale(2);padding: 1px;margin-top: 12px;"> <b style="font-size: 16px;padding-left: 10px; ">Tuesday</b> &nbsp;
                </label>
              </div>
              <div class="col-md-4">
                <label>
                <input type="checkbox" id="cr" name="weekdays" onClick="checkbox();" value="Wednesday" style="-ms-transform: scale(2);-moz-transform: scale(2);-webkit-transform: scale(2);-o-transform: scale(2); transform: scale(2);padding: 1px;margin-top: 12px;"> <b style="font-size: 16px;padding-left: 10px; "> Wednesday</b> &nbsp;
                </label>
              </div>
            </div>

            <div class="row">
              <div class="col-md-4">
                <label>
                <input type="checkbox" id="cr" name="weekdays" onClick="checkbox();" value="Thursday" style="-ms-transform: scale(2);-moz-transform: scale(2);-webkit-transform: scale(2);-o-transform: scale(2); transform: scale(2);padding: 1px;margin-top: 12px;"> <b style="font-size: 16px;padding-left: 10px; "> Thursday</b> &nbsp;
                </label>
              </div>
              <div class="col-md-4">
                <label>
                <input type="checkbox" id="cr" name="weekdays" onClick="checkbox();" value="Friday" style="-ms-transform: scale(2);-moz-transform: scale(2);-webkit-transform: scale(2);-o-transform: scale(2); transform: scale(2);padding: 1px;margin-top: 12px;"> <b style="font-size: 16px;padding-left: 10px; "> Friday </b> &nbsp;
                </label>
              </div>
              <div class="col-md-4">
                <label>
                  <input type="checkbox" id="cr" name="weekdays" onClick="checkbox();" value="Saturday" style="-ms-transform: scale(2);-moz-transform: scale(2);-webkit-transform: scale(2);-o-transform: scale(2); transform: scale(2);padding: 1px;margin-top: 12px;"> <b style="font-size: 16px;padding-left: 10px; "> Saturday</b>&nbsp;
                </label>
              </div>
            </div>

            <div class="row">
              <div class="col-md-4">
                
                <label><input type="checkbox" id="cr" name="weekdays" onClick="checkbox();" value="Sunday" style="-ms-transform: scale(2);-moz-transform: scale(2);-webkit-transform: scale(2);-o-transform: scale(2); transform: scale(2);padding: 1px;margin-top: 12px;"> <b style="font-size: 16px;padding-left: 10px; "> Sunday</b></label> &nbsp;

              </div>
            </div>
          </div>

            <div class="form-group form-group-default" style="margin-top: 10px;">
              <label>Day(s)</label>
              <textarea id="show" name="weekday" required class="form-control" style="height: 40px;font-weight: bold;" placeholder="Days.."></textarea>

            </div>

            <button type="submit" class="btn btn-success btn-block btn-lg" name="add_clz" value="<?php echo $teach_id; ?>"><i class="pg-icon">add</i> Add</button>
            </form>
            </div>
            </div>
            </div>
            </div>

            </div>
            





<div class="col-md-12" style="margin-top: 0;">
  <h3 style="text-transform: capitalize;"><a href="dashboard.php" data-toggle="tooltip" data-title="Back"><i class="pg-icon" style="font-size: 22px;">chevron_left</i></a> New Classes <small><?php echo $show_teacher_name; ?></small></h3>

<div class=" container-fluid   container-fixed-lg bg-white">
<div class="card card-transparent">
<div class="card-header ">

  <div class="row">
    <div class="col-md-3 mt-2">

      <?php
        $disable_btn = '';
        $err_msg = '';

        if(empty($_GET['teach_id']))
        {
          $disable_btn = 'pointer-events:none;opacity:0.4;';
          $err_msg = '<label class="text-danger" style="font-weight:bold;margin-top:6px;"><i class="fa fa-warning"></i> Please select teacher</label>';
        }

      ?>
        
        <form action="new_class.php" method="GET">
          <input class="form-control" list="teachers" id="browser" name="teach_id" onchange="document.getElementById('submit_btn').click();" placeholder="Type Or Select Teacher name">
            <datalist id="teachers">
              <?php 

                $sql003 = mysqli_query($conn,"SELECT * FROM `teacher_details` ORDER BY `TEACH_ID` DESC");
                while($row003 = mysqli_fetch_assoc($sql003))
                {
                  $teacher_name = $row003['POSITION'].". ".$row003['F_NAME']." ".$row003['L_NAME'];
                  $teacher_id = $row003['TEACH_ID'];

                  echo '<option value="'.$teacher_id.'">'.$teacher_name.'</option>';

                }
              
              ?>

          </datalist>

      <button type="submit" class="btn btn-success" id="submit_btn" style="display: none;">X</button>
    </form>
    <?php echo $err_msg; ?>

    </div>
    <div class="col-md-7 mt-2"></div>
    <div class="col-md-2 mt-2">

      <a href="create_class.php?teach_id=<?php echo $teach_id; ?>" class="btn btn-primary btn-block btn-lg btn-rounded btn_pulse pull-right" data-toggle="tooltip" data-title="Create Classes" data-placement="top" style="opacity: 3;padding-top:10px;<?php echo $disable_btn; ?>"><i class="fa fa-plus"></i> Create Class</a>
      
    </div>
  </div>
<div class="clearfix"></div>
</div>

<div class="card-body table-responsive" style="height: 500px;overflow: auto;margin-bottom: 0;">
  <table class="table table-dark">
  <thead>

    <th style="color:white;">Level</th>
    <th style="color:white;">Subject Name</th>
    <th style="color:white;">Class Name</th>

  </thead>
  <tbody class="myTable">

    
      
   <?php 

      if(isset($_GET['teach_id']) || !empty($_SESSION['search_teacher']))
      {
          if(!empty($_GET['teach_id']) ||  empty($_SESSION['search_teacher']))
          {
            $teach_id = $_GET['teach_id'];
            $_SESSION['search_teacher'] = $teach_id;
          }else
          if(empty($_GET['teach_id']) ||  !empty($_SESSION['search_teacher']))
          {
            $teach_id = $_SESSION['search_teacher'];
          }
    

          $sql001 = mysqli_query($conn,"SELECT * FROM teacher_subject WHERE TEACH_ID = '$teach_id'");

          if(mysqli_num_rows($sql001)>0)
          {
                while($row = mysqli_fetch_assoc($sql001))
                {

                  echo '<tr>';
                $sub_id = $row['SUB_ID'];
                $level_id = $row['LEVEL_ID'];
                $ts_id = $row['T_S_ID'];

                $sql0015 = mysqli_query($conn,"SELECT * FROM subject WHERE SUB_ID = '$sub_id'");
                $row0015=mysqli_fetch_assoc($sql0015);
                $sub_name = $row0015['SUBJECT_NAME'];
                $subject_id = $row0015['SUB_ID'];

                $sql0016 = mysqli_query($conn,"SELECT * FROM level WHERE LEVEL_ID = '$level_id'");
                $row0016=mysqli_fetch_assoc($sql0016);
                $level_name = $row0016['LEVEL_NAME'];

                $sql0017 = mysqli_query($conn,"SELECT * FROM `teacher_details` WHERE TEACH_ID = '$teach_id'");
                while($row0017 = mysqli_fetch_assoc($sql0017))
                {
                  $teacher_name = $row0017['POSITION'].". ".$row0017['F_NAME']." ".$row0017['L_NAME'];

                }
                
                

                echo '<td class="v-align-middle bold">'.$level_name.'<br><small>('.$teacher_name.')</small></td>
                      <td class="v-align-middle bold">'.$sub_name.' ';

                          ?>

                     <a data-toggle="tooltip" data-title="Unregister subject"><button type="button" class="btn btn-xs btn-danger" style="border-radius:80px;" data-toggle="modal" data-target="#delete_t_s<?php echo $ts_id; ?>"><span class="fa fa-times"></span></button></a>

                     <div id="delete_t_s<?php echo $ts_id; ?>" class="modal fade" role="dialog" style="padding: 10px 10px 10px 10px;margin-top: 10px;">
                        <div class="modal-dialog" style="">
                          <!-- Modal content-->
                          <div class="modal-content">
                            <div class="modal-header" style="border:none;">
                              
                            </div>
                          <div class="modal-body">
                          
                              <label>
                                
                                <h6 style="font-weight:600;">Are you sure delete this subject?</h6>

                                <p style="font-weight:bold;" class="text-danger">👉 Remove teacher registered all classes</a></p>
                                
                                <p style="font-weight:bold;" class="text-danger">👉 Remove all classes under registered this subject from student</p>

                                <p style="font-weight:bold;" class="text-danger">👉 Remove all payment data</p>
                                
                                <p style="font-weight:bold;" class="text-danger">👉 Remove all link clicks</p>

                              </label>
                            </div>
                            <div class="modal-footer">

                            <a href="../admin/query/delete.php?delete_t_reg_subject=<?php echo $teach_id; ?>&&subject_id=<?php echo $sub_id; ?>&&level_id=<?php echo $level_id; ?>" class="btn btn-success btn-lg" onclick="return confirm('Finally, Are you sure?')"><i class="fa fa-check"></i>&nbsp; It's OK</a>

                            <button type="button" class="btn btn-danger btn-lg" data-dismiss="modal" style="float: right;border:1px solid gray;"><i class="fa fa-times-circle"></i>&nbsp; No</button>


                            </div>

                          </div>

                        </div>
                      </div>

                      <?php echo '
                    </td>
                      <td class="v-align-middle">
                      <div style="height: 250px;overflow-y: auto;overflow-x: hidden;">';


                    $sql0014 = mysqli_query($conn,"SELECT * FROM `classes` WHERE `TEACH_ID` = '$teach_id' AND `SUB_ID` = '$sub_id' ORDER BY `CLASS_ID` DESC");

                    if(mysqli_num_rows($sql0014)>0)
                    {
                        while($row0014=mysqli_fetch_assoc($sql0014))
                        {
                          $class_id = $row0014['CLASS_ID'];
                          $class_name = $row0014['CLASS_NAME'];
                          $start = $row0014['START_TIME'];
                          $end = $row0014['END_TIME'];
                          $day = $row0014['DAY'];
                          $subj_id = $row0014['SUB_ID'];
                          $fees = $row0014['FEES'];
                          $age_range = $row0014['AGE_RANGE'];
                          $class_contact = $row0014['CONTACT_NO'];
                          $hide_tg_link = $row0014['HIDE_TG_BTN']; //Hide Telegram Link

                          if($hide_tg_link == '0')
                          {
                              //Show Btn
                              $hide_telegram_btn = '';

                          }else
                          if($hide_tg_link == '1')
                          {
                              //Hide Btn
                              $hide_telegram_btn = 'checked';

                          }

                          $sql00151 = mysqli_query($conn,"SELECT * FROM subject WHERE SUB_ID = '$sub_id'");
                          $row00151=mysqli_fetch_assoc($sql00151);
                          $sub_name = $row00151['SUBJECT_NAME'];
                          $level_id = $row00151['LEVEL_ID'];

                          $sql00161 = mysqli_query($conn,"SELECT * FROM level WHERE LEVEL_ID = '$level_id'");
                          $row00161=mysqli_fetch_assoc($sql00161);
                          $level_name = $row00161['LEVEL_NAME'];


                          $str = strtotime($start);
                          $class_start_time = date('h:i A',$str);

                          $str2 = strtotime($end);
                          $class_end_time = date('h:i A',$str2);

                          $class_time = $class_start_time."-".$class_end_time;

                          echo '

                                    <div class="row" style="margin-top:10px;">
                                      <div class="col-md-7 bold">
                                      '.$class_name.'

                                      <br> <small>'.$class_time.'</small>
                                      <br><small class="label label-default">'.$day.'</small>
                                      <br> <small style="font-weight:bold;">LKR '.number_format($fees,2).'</small>

                                      </div>
                                    
                                      <div class="col-md-5">

                                      <a data-toggle="tooltip" data-title="Class Informations"><button type="button" class="btn btn-success btn-xs btn-rounded" data-target="#clz_info'.$class_id.'" data-toggle="modal" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;margin-right:10px;"><i class="pg-icon">alert_info</i></button></a>

                                      <a data-toggle="tooltip" data-title="Edit Class"><button type="button" class="btn btn-complete  btn-xs btn-rounded" data-target="#edit_class'.$class_id.'" data-toggle="modal" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;margin-right:10px;"><i class="pg-icon">edit</i></button></a>';
                                        
                                        ?>
                                          <a href="../admin/query/delete.php?delete_added_clz=<?php echo $class_id; ?>&&teach_id=<?php echo $teach_id; ?>" onclick="return confirm('Are you sure Delete?')" class="btn btn-danger btn-rounded btn-xs" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;" data-toggle="tooltip" data-title="Delete Class"><i class="pg-icon">trash_alt</i></a>

                                          <div class="modal fade slide-up disable-scroll" id="clz_info<?php echo $class_id; ?>" tabindex="-1" role="dialog" aria-hidden="false" style="overflow: auto;">
                                            <div class="modal-dialog ">
                                            <div class="modal-content-wrapper">
                                            <div class="modal-content">
                                            <div class="modal-header clearfix text-left">
                                            <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
                                            </button>
                                            <h5>Class Information</h5>
                                            </div>
                                            <div class="modal-body">

                                              <div class="form-group form-group-default" style="margin-top: 4px;">
                                                <input type="hidden" id="teach_id" value="<?php echo $teach_id; ?>">
                                                <label>Classes</label>
                                                <h6 style="text-transform: capitalize;font-size: 14px;font-weight:bold;"><?php echo $level_name; ?></h6>
                                              </div>

                                              <div class="form-group form-group-default" style="margin-top: 4px;">
                                                <label>Subject</label>
                                                <h6 style="text-transform: capitalize;font-size: 14px;font-weight:bold;"><?php echo $sub_name; ?></h6>
                                                
                                              </div>


                                              <div class="form-group form-group-default" style="margin-top: 4px;">
                                                <label>Class Name</label>
                                                
                                                <h6 style="text-transform: capitalize;font-size: 14px;font-weight:bold;"><?php echo $class_name; ?></h6>

                                              </div>


                                              <div class="form-group form-group-default" style="margin-top: 4px;">
                                                <label>Contact No</label>
                                                
                                                <h6 style="text-transform: capitalize;font-size: 14px;font-weight:bold;"><?php echo $class_contact; ?></h6>

                                              </div>

                                              <div class="form-group form-group-default" style="margin-top: 4px;">
                                                <label>Class Fees(LKR)</label>
                                                
                                                <h6 style="text-transform: capitalize;font-size: 14px;font-weight:bold;"><?php echo number_format($fees,2); ?></h6>
                                              </div>

                                              <div class="form-group form-group-default" style="margin-top: 4px;">
                                                <label>Start Time</label>
                                                
                                                <h6 style="text-transform: capitalize;font-size: 14px;font-weight:bold;"><?php echo $start; ?></h6>

                                              </div>

                                              <div class="form-group form-group-default" style="margin-top: 4px;">
                                                <label>End Time</label>
                                                
                                                <h6 style="text-transform: capitalize;font-size: 14px;font-weight:bold;"><?php echo $end; ?></h6>

                                              </div>

                                              <div class="form-group form-group-default" style="margin-top: 4px;">
                                                <label>Day</label>
                                                
                                                <h6 style="text-transform: capitalize;font-size: 14px;font-weight:bold;"><?php echo $day; ?></h6>
                                                

                                              </div>
                                              </div>
                                              </div>
                                              </div>
                                              </div>

                                              </div>

                                          <?php



                                        ?>

                                          <div class="modal fade slide-up disable-scroll" id="edit_class<?php echo $class_id; ?>" tabindex="-1" role="dialog" aria-hidden="false" style="overflow: auto;">
                                            <div class="modal-dialog ">
                                            <div class="modal-content-wrapper">
                                            <div class="modal-content modal-lg">
                                            <div class="modal-header clearfix text-left">
                                            <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
                                            </button>
                                            <h5>Edit Class</h5>
                                            </div>
                                           <div class="modal-body">
                                              <form action="../admin/query/update.php" method="POST">
                                              
                                                <div class="row">
                                                  <div class="col-md-6">
                                                    <div class="form-group form-group-default" style="margin-top: 10px;">

                                                      <input type="hidden" id="teach_id" value="<?php echo $teach_id; ?>">
                                                      <input type="hidden" name="teach_id" value="<?php echo $teach_id; ?>">

                                                      <label>Classes</label>
                                                      <select class="form-control select_class" name="clz" id="edit_clz<?php echo $class_id; ?>" required style="cursor: pointer;">
                                                        <option value="<?php echo $level_id ?>"><?php echo $level_name; ?> </option>

                                                          <?php 

                                                              $sql0015 = mysqli_query($conn,"SELECT * FROM level WHERE LEVEL_ID != '$level_id'");
                                                              while($row0015=mysqli_fetch_assoc($sql0015))
                                                              {
                                                                $level_name = $row0015['LEVEL_NAME'];
                                                                $level_id = $row0015['LEVEL_ID'];

                                                                echo '<option value='.$level_id.'>'.$level_name.'</option>';

                                                              }

                                                               ?>
                                                        
                                                      </select>
                                                    </div>

                                                  </div>
                                                  <div class="col-md-6">
                                                    
                                                    <div class="form-group form-group-default" style="margin-top: 10px;">
                                                      <label>Subject</label>
                                                      <select class="form-control"  id="subject0<?php echo $class_id; ?>"  name="subject" required style="cursor: pointer;">
                                                        <option value="<?php echo $sub_id; ?>"><?php echo $sub_name; ?></option>
                                                      </select>
                                                    </div>

                                                  </div>
                                                
                                              </div>

                                              

                                              <script type="text/javascript">
                                                $('#edit_clz<?php echo $class_id; ?>').change(function(){

                                                     var level_clz = $(this).val();
                                                     

                                                     var teach_id = $('#teach_id').val();

                                                     $.ajax({
                                                      url:'../teacher/query/check.php',
                                                      method:"POST",
                                                      data:{edit_clz:level_clz,teach_id:teach_id},
                                                      success:function(data)
                                                      {
                                                        //alert(data)
                                                        $('#subject0<?php echo $class_id; ?>').html(data);
                                                        
                                                      }
                                                     })
                                                   

                                                  });
                                              </script>

                                              <div class="row">
                                                <div class="col-md-6">

                                                    <div class="form-group form-group-default" style="margin-top: 10px;">
                                                      <label>Class Name</label>
                                                      
                                                      <input type="text" name="clz_name" class="form-control change_inputs"  placeholder="XXXXXXXX" value="<?php echo $class_name; ?>" required >

                                                    </div>

                                                </div>

                                                <div class="col-md-6">
                                                  <div class="form-group form-group-default" style="margin-top: 10px;">
                                                    <label>Help Contact No</label>
                                                    
                                                    <input type="text" maxlength="10" name="contact_no" class="form-control check_contact" value="<?php echo $class_contact; ?>" required>
                                                    

                                                  </div>
                                                </div>
                                              </div>

                                              <div class="row">
                                                <div class="col-md-6">
                                                  

                                                    <div class="form-group form-group-default" style="margin-top: 10px;">
                                                      <label>Start Time</label>
                                                      
                                                      <input type="time" name="start" class="form-control change_inputs" value="<?php echo $start; ?>" required>

                                                    </div>

                                                </div>

                                                <div class="col-md-6">
                                                  
                                                  <div class="form-group form-group-default" style="margin-top: 10px;">
                                                    <label>End Time</label>
                                                    
                                                    <input type="time" name="end" class="form-control change_inputs"  value="<?php echo $end; ?>" required>

                                                  </div>

                                                </div>

                                              </div>

                                              <div class="row">

                                                <div class="col-md-6">
                                                  
                                                  <div class="form-group form-group-default" style="margin-top: 10px;">
                                                    <label>Class Fees</label>
                                                    
                                                    <input type="number" name="clz_fees" class="form-control change_inputs"  placeholder="XXXXXXXX" value="<?php echo $fees; ?>" value="0" required>

                                                  </div>

                                                </div>


                                                <div class="col-md-6">
                                                  
                                                  <?php 

                                                    $disable_age = '';

                                                    if($_SESSION['POSITION'] !== 'S_ADMIN')
                                                    {
                                                      $disable_age = 'disabled';
                                                    }

                                                   ?>

                                                  <div class="form-group form-group-default" style="margin-top: 10px;">
                                                    <label>Age Range</label>
                                                    
                                                    <input type="number" name="age_range" <?php echo $disable_age; ?> class="form-control change_inputs" value="<?php echo $age_range; ?>" min="0">

                                                    <input type="hidden" name="recent_age" value="<?php echo $age_range; ?>">

                                                  </div>

                                                </div>
                                              </div>

                                              <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group form-group-default" style="margin-top: 10px;">
                                                    <label for="">Hide Telegram Button</label>

                                                    <div class="form-check form-check-inline switch switch-lg success" style="width:100%;">
                                                      <input type="checkbox" id="active_tg_btn<?php echo $class_id ?>" value="1" <?php echo $hide_telegram_btn; ?> name="hide_tg_btn">
                                                      <label for="active_tg_btn<?php echo $class_id ?>" class="text-danger" style="text-transform: capitalize;cursor: pointer;"></label>

                                                    </div>

                                                  </div>
                                                </div>

                                              </div>


                                                <div class="col-md-12">


                                                      <script type="text/javascript">
                                                        function edit_checkbox<?php echo $class_id ?>(){
                                            
                                                          var checkboxes = document.getElementsByName('weekdays0012<?php echo $class_id ?>');
                                                          var checkboxesChecked = [];
                                                          // loop over them all
                                                          for (var i=0; i<checkboxes.length; i++) {
                                                             // And stick the checked ones onto an array...
                                                             if (checkboxes[i].checked) {
                                                                checkboxesChecked.push(checkboxes[i].value);
                                                             }
                                                          }
                                                          document.getElementById("edit_show<?php echo $class_id ?>").value = checkboxesChecked;

                                                        }
                                                      </script>

                                                  <div class="row">
                                                    <div class="col-md-4">
                                                      <label>
                                                      <input type="checkbox" id="bk" name="weekdays0012<?php echo $class_id ?>" onClick="edit_checkbox<?php echo $class_id ?>();" value="Monday" style="-ms-transform: scale(2);-moz-transform: scale(2);-webkit-transform: scale(2);-o-transform: scale(2); transform: scale(2);padding: 1px;margin-top: 12px;"> <b style="font-size: 16px;padding-left: 10px; ">Monday</b> &nbsp;
                                                      <label>
                                                    </div>
                                                    <div class="col-md-4">
                                                      <label>
                                                      <input type="checkbox" id="cr" name="weekdays0012<?php echo $class_id ?>" onClick="edit_checkbox<?php echo $class_id ?>();" value="Tuesday" style="-ms-transform: scale(2);-moz-transform: scale(2);-webkit-transform: scale(2);-o-transform: scale(2); transform: scale(2);padding: 1px;margin-top: 12px;"> <b style="font-size: 16px;padding-left: 10px; ">Tuesday</b> &nbsp;
                                                      <label>
                                                    </div>
                                                    <div class="col-md-4">
                                                      <label>
                                                      <input type="checkbox" id="cr" name="weekdays0012<?php echo $class_id ?>" onClick="edit_checkbox<?php echo $class_id ?>();" value="Wednesday" style="-ms-transform: scale(2);-moz-transform: scale(2);-webkit-transform: scale(2);-o-transform: scale(2); transform: scale(2);padding: 1px;margin-top: 12px;"> <b style="font-size: 16px;padding-left: 10px; "> Wednesday</b> &nbsp;
                                                      <label>
                                                    </div>
                                                  </div>

                                                  <div class="row">
                                                    <div class="col-md-4">
                                                      <label>
                                                      <input type="checkbox" id="cr" name="weekdays0012<?php echo $class_id ?>" onClick="edit_checkbox<?php echo $class_id ?>();" value="Thursday" style="-ms-transform: scale(2);-moz-transform: scale(2);-webkit-transform: scale(2);-o-transform: scale(2); transform: scale(2);padding: 1px;margin-top: 12px;"> <b style="font-size: 16px;padding-left: 10px; "> Thursday</b> &nbsp;
                                                      <label>
                                                    </div>
                                                    <div class="col-md-4">
                                                      <label>
                                                      <input type="checkbox" id="cr" name="weekdays0012<?php echo $class_id ?>" onClick="edit_checkbox<?php echo $class_id ?>();" value="Friday" style="-ms-transform: scale(2);-moz-transform: scale(2);-webkit-transform: scale(2);-o-transform: scale(2); transform: scale(2);padding: 1px;margin-top: 12px;"> <b style="font-size: 16px;padding-left: 10px; "> Friday </b> &nbsp;
                                                      <label>
                                                    </div>
                                                    <div class="col-md-4">
                                                      <label>
                                                        <input type="checkbox" id="cr" name="weekdays0012<?php echo $class_id ?>" onClick="edit_checkbox<?php echo $class_id ?>();" value="Saturday" style="-ms-transform: scale(2);-moz-transform: scale(2);-webkit-transform: scale(2);-o-transform: scale(2); transform: scale(2);padding: 1px;margin-top: 12px;"> <b style="font-size: 16px;padding-left: 10px; "> Saturday</b>&nbsp;
                                                      </label>
                                                    </div>
                                                  </div>

                                                  <div class="row">
                                                    <div class="col-md-4">
                                                      
                                                      <label><input type="checkbox" id="cr" name="weekdays0012<?php echo $class_id ?>" onClick="edit_checkbox<?php echo $class_id ?>();" value="Sunday" style="-ms-transform: scale(2);-moz-transform: scale(2);-webkit-transform: scale(2);-o-transform: scale(2); transform: scale(2);padding: 1px;margin-top: 12px;"> <b style="font-size: 16px;padding-left: 10px; "> Sunday</b></label> &nbsp;

                                                    </div>
                                                  </div>
                                                </div>
                                                
                                                  <div class="form-group form-group-default" style="margin-top: 10px;">
                                                    <label>Day(s)</label>
                                                    <textarea id="edit_show<?php echo $class_id ?>" name="weekday" class="form-control" style="height: 40px;font-weight: bold;" placeholder="Days.."><?php echo $day; ?></textarea>

                                                  </div>
                                                

                                              <button type="submit" class="btn btn-success btn-block btn-lg" name="edit_clz" value="<?php echo $class_id; ?>"><i class="pg-icon">edit</i> Update</button>
                                              </form>
                                              </div>
                                              </div>
                                              </div>
                                              </div>

                                              </div>

                                          <?php

                                      echo '</div>

                                    </div>
                                  ';

                          
                        }
                      }else
                      if(mysqli_num_rows($sql0014)== '0')
                      {
                          echo '<div class="v-align-middle" colspan="3" class="text-danger text-center" style="text-align:center;padding-top:16%;"><span class="fa fa-warning text-danger"></span> Empty Data!</div>';
                      }
                      } 
                      echo "</div></td>";
          }else
          {
              echo '<tr><td colspan="3" class="text-danger text-center"><span class="fa fa-warning text-danger"></span> Empty Data!</td></tr>';
          }
        }
 ?>
    
    </tr>
  </tbody>
  </table>
</div>
</div>

</div>

<script type="text/javascript">
  
    $(document).ready(function(){  
     $('#level_clz').change(function(){

       var level_clz = $(this).val();
       var teach_id = $('#teacher_id').val();
       //alert(teach_id);

       $.ajax({
        url:'../admin/query/check.php',
        method:"POST",
        data:{create_clz:level_clz,teach_id:teach_id},
        success:function(data)
        {
            //alert(data)
            $('#subject').html(data);
          
        }
       })
     

    });

     
   });

  </script>

<script type="text/javascript">
  setInterval(function() {
    $('blink').fadeIn(1300).fadeOut(1500);
}, 1000);
</script>

<script>
$(document).ready(function(){
  $("#search-table").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $(".myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

<script>
$(document).ready(function(){

  $("#up_rec").on("change", function() {
      $('.show_image').hide();
  });
});
</script>
<script type="text/javascript">
  setInterval(function() {
    $('.blink').fadeIn(1300).fadeOut(1500);
}, 1000);
</script>

<script>

//Check TP No and disable characters

$(document).ready(function () {

    $('.check_contact').bind('keyup blur',function()
    { 
           var thistext = $(this);
           thistext.val(thistext.val().replace(/[^0-9]/g,'') ); 

           
    });

})

//Check TP No and disable characters
</script>



<?php  include('footer/footer.php'); ?>


