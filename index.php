<?php 
    session_start();
 ?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Login</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link href="https://fonts.googleapis.com/css?family=Muli&display=swap" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

  <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  
  <style type="text/css">
    .box{
      transition: 0.5s;
      background-color: #000000a6;
      text-decoration: none;
    }
    .box:hover{
      background-color: black;
      text-decoration: none;
    }

    a{
      text-decoration: none;
    }

    a:hover{
      text-decoration: none;
      
    }
  </style>

</head>
<body class="bg-gradient-primary" style="font-family: 'Muli', sans-serif;background: #ad5389;  /* fallback for old browsers */
background: -webkit-linear-gradient(to right, #3c1053, #ad5389);  /* Chrome 10-25, Safari 5.1-6 */
background: linear-gradient(to right, #3c1053, #ad5389); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
">
<div class="container-fluid"><br>
   <!--<a href="../index.php" style="color:white;">Back</a>-->
    <center class="col-md-12" style="padding-top:10px;">
      <h1 style="color: white;font-size:60px;">Login</h1>
    </center>
  <div style="padding-top: 12%;">
    
    <div class="col-md-3" style="padding: 15px 15px 15px 15px;">
      
      <a href="student/login/index.php">
        <div class="col-md-12 box" style="border:0px solid black;height: 250px;border-radius: 20px;padding-top: 30px;" id="box">
          
          <center><img src="index_images/student.png" style="width: 140px;height: 140px;"></center>

        <h2 style="text-align: center;color:white;">Student</h2>

        </div>
      </a>

    </div>

     <div class="col-md-2" style="padding: 15px 15px 15px 15px;">
      
      <a href="teacher/login/index.php">
        <div class="col-md-12 box" style="border:0px solid black;height: 250px;border-radius: 20px;padding-top: 30px;" id="box">
          
          <center><img src="index_images/teacher.png" style="width: 140px;height: 140px;"></center>

        <h2 style="text-align: center;color:white;">Teacher</h2>

        </div>
      </a>

    </div>

     <div class="col-md-2" style="padding: 15px 15px 15px 15px;">
      
      <a href="staff/login/index.php"> <!-- staff/login/index.php -->
        <div class="col-md-12 box" style="border:0px solid black;height: 250px;border-radius: 20px;padding-top: 30px;" id="box">
          
          <center><img src="index_images/staff.png" style="width: 140px;height: 140px;"></center>

        <h2 style="text-align: center;color:white;">Staff</h2>

        </div>
      </a>

    </div>

     <div class="col-md-3" style="padding: 15px 15px 15px 15px;">
      
      <a href="admin/login/index.php">
        <div class="col-md-12 box" style="border:0px solid black;height: 250px;border-radius: 20px;padding-top: 30px;" id="box">
          
          <center><img src="index_images/admin.png" style="width: 140px;height: 140px;"></center>

        <h2 style="text-align: center;color:white;">Admin</h2>

        </div>
      </a>

    </div>

     <div class="col-md-2" style="padding: 15px 15px 15px 15px;">
      
      <a href="attendence_app/login/index.php">
        <div class="col-md-12 box" style="border:0px solid black;height: 250px;border-radius: 20px;padding-top: 30px;" id="box">
          
          <center><img src="index_images/attendance.png" style="width: 140px;height: 140px;"></center>

        <h2 style="text-align: center;color:white;">Attendence</h2>

        </div>
      </a>

    </div>
  </div>

  </div>
  <div class="col-md-12" style="height: 50px;"></div>
<footer class="site-footer navbar-inverse" style="position: fixed;left: 0;bottom: 0;width: 100%;background-color: #000000c9;padding-top: 10px;color: white;">
      <div class="text-center">
        <p>
          &copy; Copyrights <lable style="font-weight: bold;">IBS</lable>. All Rights Reserved (2017 - <?php echo date('Y'); ?>)
        </p>
        <div class="credits">
          <!--
            You are NOT allowed to delete the credit link to TemplateMag with free version.
            You can delete the credit link only if you bought the pro version.
            Buy the pro version with working PHP/AJAX contact form: https://templatemag.com/dashio-bootstrap-admin-template/
            Licensing information: https://templatemag.com/license/
          -->
        </div>
      </div>
    </footer>
  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

</body>

</html>