<?php 
  $page = '';
  include('header.php');

  $teach_id = $_GET['teach_id'];

  $sql004 = mysqli_query($conn, "SELECT * FROM `teacher_details` WHERE `TEACH_ID` = '$teach_id'");

  while($row004 = mysqli_fetch_assoc($sql004))
  {
    $name = $row004['POSITION'].". ".$row004['F_NAME']." ".$row004['L_NAME'];
    $q = $row004['QUALIFICATION'];
    if($q == '')
    {
      $q = 'Teacher';
    }

    $introduction = $row004['INTRODUCTION'];
    if($introduction == '0')
    {
      $introduction = 'අප ආයතනය සතු දක්ෂ ගුරුවරයෙකි.';
    }

    $picture = $row004['PICTURE'];
    $gender = $row004['GENDER'];
    $teach_id = $row004['TEACH_ID'];
    $video_link = $row004['INTRO_VIDEO'];

    if($video_link == '0')
    {
      $video_tag = '<div class="about__pic__item set-bg" data-setbg="img/icons/play.gif"></div>';
    }else
    if($video_link !== '0')
    {

      $shortUrlRegex = '/youtu.be\/([a-zA-Z0-9_-]+)\??/i';
      $longUrlRegex = '/youtube.com\/((?:embed)|(?:watch))((?:\?v\=)|(?:\/))([a-zA-Z0-9_-]+)/i';

      if (preg_match($longUrlRegex, $video_link, $matches)) {
          
          $youtube_id = $matches[count($matches) - 1];
      }

      if (preg_match($shortUrlRegex, $video_link, $matches)) {
          
          $youtube_id = $matches[count($matches) - 1];
      }

      $emb_link = 'https://www.youtube.com/embed/' . $youtube_id ;

     


      $video_tag = '<iframe style="height:240px;width:100%;" src="'.$emb_link.'?autoplay=0&enablejsapi=1&rel=0&modestbranding=1&showsearch=0&amp;wmode=transparent&enablejsapi=1&mode=opaque&rel=0&autohide=1&showinfo=0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
    }

    if($picture == '0')
    {
      if($gender == 'Male')
      {
        $picture = 'b_teacher.jpg';
      }else
      if($gender == 'Female')
      {
        $picture = 'g_teacher.jpg';
      }
    }
  }

 ?>


<div class="breadcrumb-option spad set-bg" data-setbg="img/breadcrumb-bg.jpg">
<div class="container">
<div class="row">
<div class="col-lg-12 text-center">
<div class="breadcrumb__text" style="margin-top: 60px;">
<h2>Profile</h2>
<div class="breadcrumb__links">
<a href="index.php">Home</a>
<span>Teachers</span>
</div>
</div>
</div>
</div>
</div>
</div>

 
<section class="about spad">
<div class="container">
<div class="row">
<div class="col-lg-6">
<div class="about__pic">
<div class="row">
<div class="col-lg-6 col-md-6 col-sm-6">
<div class="about__pic__item about__pic__item--large set-bg" data-setbg="img/icons/teacher.jpg"></div>
</div>
<div class="col-lg-6 col-md-6 col-sm-6">
<div class="row">
<div class="col-lg-12">

<div class="about__pic__item set-bg"><?php echo $video_tag; ?></div>

</div>
<div class="col-lg-12" oncontextmenu="return false">
<div class="about__pic__item set-bg" data-setbg="../teacher/images/profile/<?php echo $picture; ?>"></div> 
<!-- teacher profile image size = Width - 343px , Height - 240px -->
</div>
</div>
</div>
</div>
</div>
</div>
<div class="col-lg-6">
<div class="about__text">
<div class="section-title">
<span>About Teacher</span>
<h2 style="padding-bottom: 2px;"><?php echo $name; ?>
  <p style="font-size: 12px;line-height: 1.5;"><?php echo $q; ?></p>
</h2>
</div>
<div class="about__text__desc">
<p style="overflow: auto;height: 350px;"><?php echo $introduction; ?></p>
</div>
</div>
</div>
</div>



  <?php 

    $sql4 = mysqli_query($conn,"SELECT * FROM `teacher_subject` WHERE `TEACH_ID` = '$teach_id'");

    if(mysqli_num_rows($sql4)> 0)
    {
        echo '

          <hr style="border-top:1px solid #cccc;">

          <h2 style="color: white;margin-bottom: 20px;">Subject</h2>
          <div class="row">

        ';

          while($row004 = mysqli_fetch_assoc($sql4))
          {
              $subject_id = $row004['SUB_ID'];
              $level_id = $row004['LEVEL_ID'];

              $sql5 = mysqli_query($conn,"SELECT * FROM `subject` WHERE `SUB_ID` = '$subject_id'");
              while($row005 = mysqli_fetch_assoc($sql5))
              {
                  $subject_name = $row005['SUBJECT_NAME'];
              }

              $sql6 = mysqli_query($conn,"SELECT * FROM `level` WHERE `LEVEL_ID` = '$level_id'");
              while($row006 = mysqli_fetch_assoc($sql6))
              {
                  $level_name = $row006['LEVEL_NAME'];
              }

              echo '

                <div class="col-lg-2 col-md-2 col-sm-2">
                  <div class="services__item">

                    <div class="services__item__icon">
                      <img src="img/icons/xsi-3.png.pagespeed.ic.YeZy-1cmly.png" alt="">
                    </div>

                    <h4>'.$subject_name.'</h4>
                    <small style="color:white;line-height:0.2;">'.$level_name.'</small>

                  </div>
                </div>

              ';

          }

          echo "

          </div>
          ";
      }
   ?>

  <hr style="border-top:1px solid #cccc;">
  

    <div class="row">
      <div class="col-lg-4"><h2 style="color: white;margin-bottom: 20px;">Time table</h2></div>
      <div class="col-lg-4"></div>
      <div class="col-lg-4" style="padding-bottom: 20px;"><input class="form-control" style="width: 100%;padding: 6px 6px;font-size: 18px;" id="myInput" type="text" placeholder="Search.."></div>
    </div>
    
<div class="table-responsive" style="height: 500px;overflow: auto;">
    <table class="table table-bordered table-striped" style="color: white;">
    <thead>
      <tr>
        <th colspan="2">Time</th>
        <th>Class</th>
        <th>Subject</th>
      </tr>
    </thead>
    <tbody id="myTable">
      <tr class="no-data alert alert-danger" style="margin-top: 20px;display: none;text-align: center">
        <td colspan="6" class="text-danger"><span class="fa fa-warning"></span> Not Found Data!</td>
      </tr>
      <?php 

        $sql001 = mysqli_query($conn,"SELECT * FROM `classes` WHERE `TEACH_ID` = '$teach_id'");
        $check = mysqli_num_rows($sql001);
        if($check > 0)
        {

            while($row001 = mysqli_fetch_assoc($sql001))
            {
              $class_day = $row001['DAY'];
              $class_name = $row001['CLASS_NAME'];

              $strt = strtotime($row001['START_TIME']);
              $start_time = date('h:i A',$strt);


              $end = strtotime($row001['END_TIME']);
              $end_time = date('h:i A',$end);

              $check_str = strtotime($class_day);
              $day = date('l',$check_str);

              $time = $start_time." - ".$end_time;
              $subject_id = $row001['SUB_ID'];

              $sql5 = mysqli_query($conn,"SELECT * FROM `subject` WHERE `SUB_ID` = '$subject_id'");
              while($row005 = mysqli_fetch_assoc($sql5))
              {
                  $subject_name = $row005['SUBJECT_NAME'];
              }

              $sql6 = mysqli_query($conn,"SELECT * FROM `level` WHERE `LEVEL_ID` = '$level_id'");
              while($row006 = mysqli_fetch_assoc($sql6))
              {
                  $level_name = $row006['LEVEL_NAME'];
              }

              $next_date = date('Y-m-d',strtotime($class_day));

              

              echo '

                <tr>
                  <td colspan="2" style="padding-top:20px;">'.$day.' <br><small>'.$time.'</small></td>
                  <td style="padding-top:20px;">'.$class_name.'</td>
                  <td>'.$subject_name.'</td>
                </tr>

              ';
            }
          }else
          if($check == '0')
          {
            echo '<tr><td colspan="6" style="text-align:center;"><span class="fa fa-warning"></span> Not Found Data!</td></tr>';
          }

       ?>
    </tbody>
  </table>
</div>

  </div>


</div>

</section>


<?php 
  include('footer.php');

 ?>


<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>


<script type="text/javascript">
  $(document).ready(function () {

    (function ($) {

        $('#myInput').keyup(function () {
            var rex = new RegExp($(this).val(), 'i');
            $('#myTable tr').hide();
            $('#myTable tr').filter(function () {
                return rex.test($(this).text());
            }).show();
            $('.no-data').hide();
            if($('#myTable tr:visible').length == 0)
            {
                $('.no-data').show();
            }

        })

    }(jQuery));

});
</script>

<script type="text/javascript">
   function getIP(json) {  
   // document.write("My public IP address is: ", json.ip);  
 }  
 </script>
<script type="application/javascript" src="https://api.ipify.org?format=jsonp&callback=getIP"></script>  