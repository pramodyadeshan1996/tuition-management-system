  
<?php

$page = "Free Card";
$folder_in = '0';

  include('header/header.php'); 
 ini_set( "display_errors", 0); 

$teach_id = '';
$class_id = '';
$pagination_link = '';


 $admin_id = $_SESSION['ADMIN_ID'];

 if(!empty($_GET['teach_id']))
 {
  $teach_id = $_GET['teach_id'];
  $class_id = $_GET['class_id'];

  $pagination_link = '&&class_id='.$class_id.'&&teach_id='.$teach_id.'';
 }

 /*------------------ Header Pagination --------------------------------*/
  $record_per_page = 10;

  $nxt_five_loop = 0;

  $page = '';

  if(isset($_GET["page"]))
  {
    $page = $_GET["page"];
  }
  else
  {
   $page = 1;
  }

  $start_from = ($page-1)*$record_per_page; //Current page no -1 X Per Page records

  /*------------------ Header Pagination --------------------------------*/

  ?>
  <style type="text/css">
    .btn_pulse {
  
  display: block;
  border-radius: 10%;
  cursor: pointer;
  animation: none;
  float: left;
  bottom: 5px;
  right: 5px;
  font-weight: bold;
  padding-top: 2px;
  text-align: center;
  color: white;
  font-size: 14px;
  animation: pulse 1.6s infinite;
}


@keyframes pulse {
  0% {
    -moz-box-shadow: 0 0 0 0 #7252d3;
    box-shadow: 0 0 0 0 #7252d3;
  }
  70% {
    -moz-box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
  }
  100% {
    -moz-box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
  }
}
 .no-data {
    display: none;
    text-align: center;
}

.pagination li a {
   padding:8px 16px;
   border:1px solid #ccc;
   color:#333;
   font-weight:bold;
  }
  </style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


      <ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-bottom: 04%;margin-bottom: 0;">
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title=" Free Card"> Free Card</li>
    </ol>
    


<div class="col-md-12" style="border-top: 1px solid #cccc;">
  
<div class="row" style="margin-top: 3%;margin-bottom: 3%;">
<div class="col-md-1"></div>
<div class="col-md-10 bg-white" style="padding: 10px 20px 15px 20px;">



        <h2><a href="dashboard.php" style="color: #4b4b4b;"><span class="fa fa-angle-left"></span></a> Free Card</h2>
      
  <form action="free_card.php" method="GET">
    <div class="row">
      <div class="col-md-5">
          <div class="form-group form-group-default" style="margin-top: 10px;">
              <label>Teacher Name</label>

              <select class="form-control" id="teacher" name="teach_id" onchange="select_teacher();" required>
                <?php 

                if(empty($_GET['teach_id']))
                {
                  echo '<option value="">Select Teacher Name</option>';

                  $sql0015 = mysqli_query($conn,"SELECT * FROM `teacher_details`");
                  while($row0015=mysqli_fetch_assoc($sql0015))
                  {
                    $f_name = $row0015['F_NAME'];
                    $l_name = $row0015['L_NAME'];
                    $position = $row0015['POSITION'];
                    $teach_id12 = $row0015['TEACH_ID'];

                      echo '<option value='.$teach_id12.'>'.$f_name.' '.$l_name.'</option>';

                  }
                }else
                if(!empty($_GET['teach_id']))
                {
                  $teach_id = $_GET['teach_id'];

                  $sql00154 = mysqli_query($conn,"SELECT * FROM `teacher_details` WHERE `TEACH_ID` = '$teach_id'");
                      while($row00154=mysqli_fetch_assoc($sql00154))
                      {
                        $f_name3 = $row00154['F_NAME'];
                        $l_name3 = $row00154['L_NAME'];
                        $position3 = $row00154['POSITION'];
                        $teach_id13 = $row00154['TEACH_ID'];

                          echo '<option value='.$teach_id13.'>'.$f_name3.' '.$l_name3.'</option>';

                  } 

                  $sql00155 = mysqli_query($conn,"SELECT * FROM `teacher_details` WHERE `TEACH_ID` != '$teach_id'");
                  while($row00155=mysqli_fetch_assoc($sql00155))
                  {
                    $f_name5 = $row00155['F_NAME'];
                    $l_name5 = $row00155['L_NAME'];
                    $position5 = $row00155['POSITION'];
                    $teach_id5 = $row00155['TEACH_ID'];

                      echo '<option value='.$teach_id5.'>'.$f_name5.' '.$l_name5.'</option>';

                  }
                }

                     

                ?>

              </select>

            </div>
      </div>

      <div class="col-md-5">
          <div class="form-group form-group-default" style="margin-top: 10px;">
              <label>Class Name</label>
              <select class="form-control" name="class_id" id="check_class_name" required>
                <?php 

                if(empty($_GET['class_id']))
                {
                  echo '<option value="">Select Class Name</option>';

                  $sql0015 = mysqli_query($conn,"SELECT * FROM `classes` WHERE `TEACH_ID` = '$teach_id'");
                  while($row0015=mysqli_fetch_assoc($sql0015))
                  {
                    $class_name1 = $row0015['CLASS_NAME'];
                    $class_id1 = $row0015['CLASS_ID'];
                    $subject_id = $row0015['SUB_ID'];

                    $sql007 = mysqli_query($conn,"SELECT * FROM `subject` WHERE `SUB_ID` = '$subject_id'");
                    while($row007=mysqli_fetch_assoc($sql007))
                    {
                        $subject_name = $row007['SUBJECT_NAME'];
                        $level_id = $row007['LEVEL_ID'];

                        $sql008 = mysqli_query($conn,"SELECT * FROM `level` WHERE `LEVEL_ID` = '$level_id'");
                        while($row008=mysqli_fetch_assoc($sql008))
                        {
                            $level_name = $row008['LEVEL_NAME'];

                        }

                    }

                    echo '<option value='.$class_id1.'>'.$class_name1.' ('.$subject_name.')</option>';

                  }
                }else
                if(!empty($_GET['class_id']))
                {

                  $teach_id = $_GET['teach_id'];
                  $class_id = $_GET['class_id'];

                  $sql0015 = mysqli_query($conn,"SELECT * FROM `classes` WHERE `CLASS_ID` = '$class_id' AND `TEACH_ID` = '$teach_id'");
                  while($row0015=mysqli_fetch_assoc($sql0015))
                  {
                    $class_name2 = $row0015['CLASS_NAME'];
                    $class_id2 = $row0015['CLASS_ID'];

                    $subject_id = $row0015['SUB_ID'];

                    $sql007 = mysqli_query($conn,"SELECT * FROM `subject` WHERE `SUB_ID` = '$subject_id'");
                    while($row007=mysqli_fetch_assoc($sql007))
                    {
                        $subject_name = $row007['SUBJECT_NAME'];
                        $level_id = $row007['LEVEL_ID'];

                        $sql008 = mysqli_query($conn,"SELECT * FROM `level` WHERE `LEVEL_ID` = '$level_id'");
                        while($row008=mysqli_fetch_assoc($sql008))
                        {
                            $level_name = $row008['LEVEL_NAME'];

                        }

                    }

                    echo '<option value='.$class_id2.'>'.$class_name2.' ('.$subject_name.')</option>';

                  } 

                  $sql00153 = mysqli_query($conn,"SELECT * FROM `classes` WHERE `CLASS_ID` != '$class_id' AND `TEACH_ID` = '$teach_id'");
                  while($row00153=mysqli_fetch_assoc($sql00153))
                  {
                    $class_name3 = $row00153['CLASS_NAME'];
                    $class_id3 = $row00153['CLASS_ID'];
                    $subject_id3 = $row00153['SUB_ID'];

                    $sql0073 = mysqli_query($conn,"SELECT * FROM `subject` WHERE `SUB_ID` = '$subject_id3'");
                    while($row0073=mysqli_fetch_assoc($sql0073))
                    {
                        $subject_name3 = $row0073['SUBJECT_NAME'];
                        $level_id3 = $row0073['LEVEL_ID'];

                        $sql0083 = mysqli_query($conn,"SELECT * FROM `level` WHERE `LEVEL_ID` = '$level_id3'");
                        while($row0083=mysqli_fetch_assoc($sql0083))
                        {
                            $level_name = $row0083['LEVEL_NAME'];

                        }

                    }


                    echo '<option value='.$class_id3.'>'.$class_name3.' ('.$subject_name3.')</option>';

                  } 
                }

                     

                ?>
                
              </select>
            </div>
      </div>

      <div class="col-md-2" style="padding-top: 10px;">
        <button type="submit" class="btn btn-success btn-block btn-lg" id="search_free" style="padding-top: 10px;font-size: 22px;padding-bottom: 10px;"><i class="pg-icon" style="font-size: 30px;">search</i> Search</button>
      </div>
    </div>
  </form>

  <div class="col-md-12" style="border-top: 1px solid #cccc;padding: 10px 0 10px 0;"></div>

  <div class="row">
    <div class="col-md-12">

      <div class="row">
        <div class="col-md-6"><h5>Student Details </h5></div>
        <div class="col-md-6">

          <form action="free_card.php" method="GET">

          <input type="hidden" name="class_id" value="<?php echo $class_id; ?>">
          <input type="hidden" name="teach_id" value="<?php echo $teach_id; ?>">
          <input type="hidden" name="page" value="<?php echo $page; ?>">

          <div class="row">
            <div class="col-md-7">

                <input list="student_search" id="search_student_data" name="stu_id" class="form-control" placeholder="Search Register ID.." required>

                <datalist id="student_search">
                  <?php 

                  $sql003 = mysqli_query($conn,"SELECT * FROM `transactions` WHERE `CLASS_ID` = '$class_id' AND `TEACH_ID` = '$teach_id'");
                  while($row003 = mysqli_fetch_assoc($sql003))
                  {
                        
                      $student_uniq_id = $row003['STU_ID'];

                      $sql002 = mysqli_query($conn,"SELECT * FROM `student_details` WHERE `STU_ID` = '$student_uniq_id'");
                      while($row002 = mysqli_fetch_assoc($sql002))
                      {
                        $name = $row002['F_NAME']." ".$row002['L_NAME'];
                        $student_id = $row002['STU_ID'];
                        $tp = $row002['TP'];

                        $sql0032 = mysqli_query($conn,"SELECT * FROM `stu_login` WHERE `STU_ID` = '$student_id' AND `STATUS` = 'Active'");
                        while($row0032 = mysqli_fetch_assoc($sql0032))
                        {
                          
                          $register_id = $row0032['REGISTER_ID'];
                        
                          echo '<option value="'.$register_id.'">'.$name.' | '.$tp.' | '.$register_id.'</option>';
                        }

                        
                      }

                  }

                   ?>
                  
                </datalist>
              </div>
            <div class="col-md-5">

              <button type="submit" class="btn btn-success btn-lg" style="border-radius: 20px;" id="search_btn"><i class="fa fa-search"></i>&nbsp;Search</button>

              <a href="free_card.php?class_id=<?php echo $class_id; ?>&teach_id=<?php echo $teach_id; ?>&page=<?php echo $page; ?>" class="btn btn-danger btn-lg" style="border-radius: 20px;"><i class="fa fa-times"></i>&nbsp;Show All</a>
            </div>
          </div>
        
            
          </form>
        </div>
      </div>
      <div class="table-responsive" style="height: auto;border-top: 1px solid #cccc;">
            <table class="table table-hover text-left">
            <thead>
            <tr>
              <th style="width: 1%;">Student Name</th>
              <th style="width: 1%;">Status</th>
              <th style="width: 1%;">LMS Status</th>
              <th style="width: 1%;" class="text-center">Action</th>
            </tr>
            </thead>
            <tbody id="students_table" class="searchable">

            <?php 
              
              if(isset($_GET['stu_id']))
              {
                $reg_id = $_GET['stu_id']; //GET REGISTER ID

                $sql003 = mysqli_query($conn,"SELECT * FROM `stu_login` WHERE `REGISTER_ID` = '$reg_id'");
                while($row003 = mysqli_fetch_assoc($sql003))
                {
                  $stu_id = $row003['STU_ID'];
                }

                $sql002 = mysqli_query($conn,"SELECT * FROM `transactions` WHERE `TEACH_ID` = '$teach_id' AND `CLASS_ID` = '$class_id' AND `STU_ID` = '$stu_id' order by `UPDATE_TIME` DESC LIMIT $start_from, $record_per_page");

                $page_result = mysqli_query($conn,"SELECT * FROM `transactions` WHERE `TEACH_ID` = '$teach_id' AND `CLASS_ID` = '$class_id' AND `STU_ID` = '$stu_id' ORDER BY `UPDATE_TIME` DESC");

              }else
              {
                $sql002 = mysqli_query($conn,"SELECT * FROM `transactions` WHERE `TEACH_ID` = '$teach_id' AND `CLASS_ID` = '$class_id' order by `UPDATE_TIME` DESC LIMIT $start_from, $record_per_page");

                $page_result = mysqli_query($conn,"SELECT * FROM `transactions` WHERE `TEACH_ID` = '$teach_id' AND `CLASS_ID` = '$class_id' ORDER BY `UPDATE_TIME` DESC");

              }
              

              

              if(mysqli_num_rows($sql002)>0)
              {

                  while($row003 = mysqli_fetch_assoc($sql002))
                  {
                      
                      $tra_id = $row003['TRA_ID'];
                      $stu_id = $row003['STU_ID'];
                      $free_class = $row003['FREE_CLASS'];
                      $free_start = $row003['FREE_START_DATE'];
                      $free_end = $row003['FREE_END_DATE'];
                      $lms_status = $row003['LMS_STATUS'];



                      if($free_start !== '0' && $free_end !== '0')
                      {
                        $free_period = $free_start.' - '.$free_end;
                      }else
                      {
                        $free_period = '';
                      }

                      if($lms_status == 0)
                      {
                        $lms_label = 'danger';
                        $lms_result = '';
                        $lms_message = 'Deactive';

                      }else
                      if($lms_status == 1)
                      {
                        $lms_label = 'success';
                        $lms_result = 'checked';
                        $lms_message = 'Active';

                      }

                      if($free_class == '0')
                      {
                        $icon = '<label class="label label-danger" style="font-size:14px;">Payment</label>';
                        $btn = '<button type="button" class="btn btn-success" style="box-shadow:0px 1px 10px 2px #cccc;font-size:14px;" data-toggle="modal" data-target="#free_active'.$tra_id.'"><span class="fa fa-plus"></span>&nbsp; Add Free Card</button>';
                      }else
                      if($free_class == '1')
                      {
                        $icon = '<label class="label label-success" style="font-size:14px;">Free Card</label>';
                        $btn = '<button type="button" value="1" style="display:none;"></button>
                        <button type="button" class="btn btn-danger" style="box-shadow:0px 1px 10px 2px #cccc;font-size:14px;" data-toggle="modal" data-target="#cancel_free_card'.$tra_id.'"><span class="fa fa-times"></span>&nbsp; Cancel</button>

                        <button type="button" class="btn btn-success" style="box-shadow:0px 1px 10px 2px #cccc;font-size:14px;" data-toggle="modal" data-target="#free_active'.$tra_id.'"><span class="fa fa-cog"></span>&nbsp; Edit</button>
                        ';
                      }

                      $sql00004 = mysqli_query($conn,"SELECT * FROM `student_details` WHERE `STU_ID` = '$stu_id'");
                      while($row00004 = mysqli_fetch_assoc($sql00004))
                      {
                        $student_name = $row00004['F_NAME']." ".$row00004['L_NAME'];
                      }

                      $sql00005 = mysqli_query($conn,"SELECT * FROM `stu_login` WHERE `STU_ID` = '$stu_id'");
                      while($row00005 = mysqli_fetch_assoc($sql00005))
                      {
                        $reg = $row00005['REGISTER_ID'];
                      }

                      echo '
                        <tr>
                          <td style="display:none;">'.$reg.'</td>
                          <td>'.$student_name.'<br> <small>'.$reg.'</small></td>

                          <td id="free_card_status'.$tra_id.'" style="font-weight:bold;">'.$icon.'<br> <small>'.$free_period.'</small></td>
                          <td><label class="label label-'.$lms_label.'" style="font-size:14px;">'.$lms_message.'</label></td>

                          <td class="text-center" id="free_card_btn'.$tra_id.'">'.$btn.'</td>
                        </tr>

                        <div id="cancel_free_card'.$tra_id.'" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
                          <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" id="close_free_modal<?php echo $class_id; ?>" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Cancel Free Card</h4>
                              </div>
                              <form action="../admin/query/update.php" method="POST">
                              <div class="modal-body">
                                <h4 class="text-muted"><span class="fa fa-trash"></span> Are you sure cancel free card activation?</h4>

                                  <input type="hidden" name="page" value="'.$page.'">

                                  <input type="hidden" name="class_id" value="'.$class_id.'">
                                  <input type="hidden" name="teach_id" value="'.$teach_id.'">';

                                  if(!empty($_GET['stu_id']))
                                  {
                                    echo '<input type="hidden" name="stu_id" value="'.$_GET['stu_id'].'">';
                                  }

                              echo '
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default btn-lg" data-dismiss="modal"><span class="fa fa-times"></span>&nbsp;No</button>

                                <button type="submit" class="btn btn-success btn-lg" name="cancel_free_card" value="'.$tra_id.'"><span class="fa fa-check"></span>&nbsp;Yes</button>
                              </div>
                              </form>
                            </div>

                            </div>
                          </div>



                        <div id="free_active'.$tra_id.'" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
                          <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" id="close_free_modal<?php echo $class_id; ?>" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Modify Free Card</h4>
                              </div>
                              <form action="../admin/query/update.php" method="POST">
                              <div class="modal-body">

                                  <div class="form-group form-group-default">
                                  <label>Start Date</label>
                                    <input type="date" name="start_date" value="'.$free_start.'" class="form-control">
                                  </div>

                                  <div class="form-group form-group-default">
                                  <label>End Date</label>
                                    <input type="date" name="end_date" value="'.$free_end.'" class="form-control">
                                  </div>

                                  <div class="form-group form-group-default">
                                  <label>LMS Status</label>
                                    <div class="form-check form-check-inline switch switch-lg success" style="margin-top:8px;">

                                      <input type="checkbox" id="lms_active'.$tra_id.'" name="lms_active" value="1" '.$lms_result.' style="float:right;">
                                      <label for="lms_active'.$tra_id.'" class="text-danger" style="text-transform: capitalize;cursor: pointer;">
                                      </label>
                                    </div>
                                  </div>
                                  <input type="hidden" name="page" value="'.$page.'">

                                  <input type="hidden" name="class_id" value="'.$class_id.'">
                                  <input type="hidden" name="teach_id" value="'.$teach_id.'">';

                                  if(!empty($_GET['stu_id']))
                                  {
                                    echo '<input type="hidden" name="stu_id" value="'.$_GET['stu_id'].'">';
                                  }

                              echo '    
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default btn-lg" data-dismiss="modal"><span class="fa fa-times"></span>&nbsp;Close</button>

                                <button type="submit" class="btn btn-success btn-lg" name="update_free_card" value="'.$tra_id.'"><span class="fa fa-check"></span>&nbsp;Submit</button>
                              </div>
                              </form>
                            </div>

                            </div>
                          </div>

                      ';
                      
                  }
                 
              }else
              if(mysqli_num_rows($sql002) == '0')
              {
                  echo '<tr><td colspan="4" class="text-center text-danger"><span class="fa fa-warning"></span> Not Found Data</td></tr>';
              }

            
           ?>

            </tbody>
            </table>
            </div>

            <div class="col-md-12" style="width: 100%;overflow: auto;">
                  <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-8" class="php_pagination">
                      
                      <div class="col-md-12">
                    <br />
                    <?php
                    $total_records = mysqli_num_rows($page_result);
                    $total_pages = ceil($total_records/$record_per_page);
                    $start_loop = $page;
                    $difference = $total_pages - $page;
                    if($difference <= 5)
                    {
                     $start_loop = $total_pages - 5;
                    }else
                    if($difference <= 0)
                    {
                      $start_loop = '0';
                    }

                    echo '<ul class="pagination">';
                    $end_loop = $start_loop + 4;
                    $five_loop = $page-5;
                    if($five_loop <= 0)
                    {
                      $five_loop = 1;
                    }
                    if($page > 1)
                    {

                      echo '<li><a href="free_card.php?page=1'.$pagination_link.'">First</a></li>';
                      echo '<li><a href="free_card.php?page='.($page - 1).$pagination_link.'"> < </a></li>';
                      echo '<li><a href="free_card.php?page='.$five_loop.''.$pagination_link.'"> <<< </a></li>';

                    }
                    if($start_loop !== '1')
                    {
                      $few_no = $start_loop - 1;
                    }else
                    if($start_loop > 0)
                    {
                      $few_no = 1;
                    }
                    
                    for($i=$few_no; $i<=$end_loop+1; $i++)
                    {     
                      if($i == $page)
                      {
                        echo '<li class="active"><a href="free_card.php?page='.$i.''.$pagination_link.'" style="font-weight:bold;background-color:#2980b9;color:white;">'.$i.'</a></li>';
                      }else
                      {
                        if($i == '0')
                        {
                          continue;
                        }else
                        if($i>0)
                        {
                          echo '<li><a href="free_card.php?page='.$i.''.$pagination_link.'" >'.$i.'</a></li>';
                        }
                        
                      }

                    }
                    if($page <= $end_loop)
                    {
                      
                      if($page == '1')
                      {
                        $nxt_five_loop = 5;
                      }else
                      if($page >= 5)
                      {
                        $nxt_five_loop = $page+5;

                        if($total_pages < $nxt_five_loop)
                        {
                          $nxt_five_loop = $total_pages;
                        }
                      }else
                      {
                        $nxt_five_loop = $page+5;
                      }

                        echo '<li><a href="free_card.php?page='.($page + 1).''.$pagination_link.'" > > </a></li>';
                        echo '<li><a href="free_card.php?page='.$nxt_five_loop.''.$pagination_link.'" > >>> </a></li>';
                        echo '<li><a href="free_card.php?page='.$total_pages.''.$pagination_link.'" >Last</a></li>';
                      
                    }
                    
                    echo '</ul>';
                    
                    ?>
                    </div>

                    </div>
                  </div>
                    
                </div>
    </div>
  </div>
    
 


</div>
<div class="col-md-1"></div>

</div>


<?php  include('footer/footer.php'); ?>


<script type="text/javascript">
  
    function select_teacher()
    {
        var teach_id = document.getElementById('teacher').value;
        
         $.ajax({
          url:'../admin/query/check.php',
          method:"POST",
          data:{get_classes_data:teach_id},
          success:function(data)
          {
            //alert(data)
            $('#check_class_name').html(data);
          }
        });
    }
</script>


<script>
$(document).ready(function(){
  $("#search").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#students_table tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
  (function ($) {

        $('#search').keyup(function () {
            var rex = new RegExp($(this).val(), 'i');
            $('.searchable tr').hide();
            $('.searchable tr').filter(function () {
                return rex.test($(this).text());
            }).show();
            $('.no-data').hide();
            if($('.searchable tr:visible').length == 0)
            {
                $('.no-data').show();
            }

        })

    }(jQuery));
});
</script>