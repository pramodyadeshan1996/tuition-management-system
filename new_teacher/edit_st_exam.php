  
<?php

$page = "Update Examination";
$folder_in = '0';

$paper_id = $_GET['paper_id'];

  include('header/header.php'); 
 ini_set( "display_errors", 0); 

 $admin_id = $_SESSION['ADMIN_ID'];

  ?>
  <style type="text/css">
    .btn_pulse {
  
  display: block;
  border-radius: 10%;
  cursor: pointer;
  animation: none;
  float: left;
  bottom: 5px;
  right: 5px;
  font-weight: bold;
  padding-top: 2px;
  text-align: center;
  color: white;
  font-size: 14px;
  animation: pulse 1.6s infinite;
}


@keyframes pulse {
  0% {
    -moz-box-shadow: 0 0 0 0 #7252d3;
    box-shadow: 0 0 0 0 #7252d3;
  }
  70% {
    -moz-box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
  }
  100% {
    -moz-box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
  }
}
  </style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


      <ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-bottom: 04%;margin-bottom: 0;">
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="Update Examination"> Update Examination</li>
    </ol>
    




<div class="col-md-12" style="border-top: 1px solid #cccc;">


<form action="../admin/query/update.php" method="POST" enctype="multipart/form-data">
<div class="row" style="margin-top: 1%;margin-bottom: 3%;">
  <div class="col-md-3"></div>
  <div class="col-md-6">

    <div class="col-md-12 bg-white" style="padding: 20px 20px 20px 20px;">
      <div style="border-bottom: 1px solid #cccc;">
        <label class="text-muted text-center" style="font-size: 30px;">

          <a href="exam.php" style="text-decoration: none;color: gray;" data-toggle="tooltip" data-title="Go Back"><span class="fa fa-angle-left"></span></a> 

          <span class="fa fa-file" style="font-size: 30px;"></span><strong> Paper Details</strong></label>
      </div>

      <?php 

          $sql001 = mysqli_query($conn,"SELECT * FROM `paper_master` WHERE `PAPER_ID` = '$paper_id'");

          while($row001 = mysqli_fetch_assoc($sql001))
          {
            $paper_name = $row001['PAPER_NAME'];
            $exam_key = $row001['EXAM_KEY'];
            $marks = $row001['ANSWER_MARKS'];
            $pdf_name = $row001['PDF_NAME'];
            $teacher_id = $row001['UPLOAD_BY'];  
            $hour = $row001['TIME_DURATION_HOUR'];  
            $minute = $row001['TIME_DURATION_MIN'];  
            $start_date = $row001['START_DATE'];  
            $start_time = $row001['START_TIME'];  
            $class_id = $row001['CLASS_ID'];  

            $end_date = $row001['FINISH_DATE'];  
            $end_time = $row001['FINISH_TIME'];

            $max_hour = $row001['MAX_HOUR'];
            $max_minu = $row001['MAX_MINUTE'];

            $sql002 = mysqli_query($conn,"SELECT * FROM `classes` WHERE `CLASS_ID` = '$class_id'");

            while($row002 = mysqli_fetch_assoc($sql002))
            {
                $class_name = $row002['CLASS_NAME'];
                $teach_id = $row002['TEACH_ID'];
            
                $sql003 = mysqli_query($conn,"SELECT * FROM `teacher_details` WHERE `TEACH_ID` = '$teach_id'");

                while($row003 = mysqli_fetch_assoc($sql003))
                {
                  $teacher_name = $row003['POSITION'].". ".$row003['F_NAME']." ".$row003['L_NAME'];
                }

            }


          }

       ?>


      <div style="padding-top: 20px;">
        
        <div class="form-group form-group-default input-group">
            <div class="form-input-group">
              <label>Paper Name</label>
              <input type="text" name="paper_name" class="form-control" placeholder="Write.." required data-toggle="tooltip" data-title="ප්‍රශ්න පත්‍රයේ නම" value="<?php echo $paper_name; ?>">
            </div>
        </div>

        <div class="form-group form-group-default input-group">
            <div class="form-input-group">
              <label>Exam Key</label>
              <input type="text" name="exam_key" list="exam_key" class="form-control" placeholder="Write.." required data-toggle="tooltip" data-title="බහුවරණ ප්‍රශ්න පත්‍ර්‍ර්‍රය හා රචනා ප්‍රශ්න පත්‍රය සමග සම්බන්ධ කිරීමේදී පොදු අංකයක් භාවිතා කල යුතුය." value="<?php echo $exam_key; ?>" onclick="this.select();">
              <datalist id="exam_key">
                    <?php 

                        $sql003 = mysqli_query($conn,"SELECT * FROM `paper_master`");
                        while($row003 = mysqli_fetch_assoc($sql003))
                        {
                            $exam_key = $row003['EXAM_KEY'];

                            echo '<option value="'.$exam_key.'"></option>';
                        }

                     ?>

                </datalist>
            </div>
        </div>

        <div class="row">
          <div class="col-md-6">
            
              <div class="form-group form-group-default input-group">
                  <div class="form-input-group">
                    <label>Teacher Name</label>
                    <select class="form-control" name="teacher_id" id="teacher_id" onchange="find_class();" data-toggle="tooltip" data-title="ගුරුවරයාගේ නම">
                      <option value="<?php echo $teach_id; ?>"><?php echo $teacher_name; ?></option>
                      <?php 
                        $sql001 = mysqli_query($conn,"SELECT * FROM `teacher_details` WHERE `TEACH_ID` != '$teach_id'"); //check Classes for registered teachers details
                        while($row001 = mysqli_fetch_assoc($sql001))
                        {

                            $teacher_id = $row001['TEACH_ID']; //Teacher Position
                            $teacher_f_name = $row001['F_NAME']; //Teacher First Name
                            $teacher_l_name = $row001['L_NAME']; //Teacher Last Name
                            $teacher_position = $row001['POSITION']; //Teacher Last Name

                            $teacher_full_name = $teacher_position.". ".$teacher_f_name." ".$teacher_l_name;

                            echo '<option value="'.$teacher_id.'">'.$teacher_full_name.'</option>';


                        }
                       ?>
                    </select>
                  </div>
              </div>

          </div>

          <div class="col-md-6">
            
              <div class="form-group form-group-default input-group">
                  <div class="form-input-group">
                    <label>Class Name</label>
                    <select class="form-control" name="class_id" id="class_id" data-toggle="tooltip" data-title="පන්තියේ නම">
                      <option value="<?php echo $class_id; ?>"><?php echo $class_name; ?></option>

                      <?php 

                        $sql0021 = mysqli_query($conn,"SELECT * FROM `classes` WHERE `CLASS_ID` != '$class_id' AND `TEACH_ID` = '$teach_id'");

                        while($row0021 = mysqli_fetch_assoc($sql0021))
                        {
                            $class_name01 = $row0021['CLASS_NAME'];
                            $class_id01 = $row0021['CLASS_ID'];

                            echo '<option value="'.$class_id01.'">'.$class_name01.'</option>';

                        }
                       ?>
                    </select>
                  </div>
              </div>
        
          </div>
  </div>

        <div class="row">
          <div class="col-md-6">
            
              <div class="form-group form-group-default input-group">
                  <div class="form-input-group">
                    <label>Time Duration(Hours)</label>
                    <select class="form-control" name="hour" data-toggle="tooltip" data-title="ලබාදෙන කාලය(පැය ගණන)">
                      <option value="<?php echo $hour; ?>"><?php echo $hour; ?></option>
                      <?php 
                        for ($h=0; $h <= 12; $h++) 
                        { 

                          if($h<10)
                          {
                            $h = '0'.$h;
                          }

                          echo '<option value="'.$h.'">'.$h.'</option>';

                        }
                       ?>
                    </select>
                  </div>
              </div>

          </div>

          <div class="col-md-6">
            
              <div class="form-group form-group-default input-group">
                  <div class="form-input-group">
                    <label>Time Duration(Minute)</label>
                    <select class="form-control" name="minute" data-toggle="tooltip" data-title="ලබාදෙන කාලය(විනාඩි ගණන)">
                      <option value="<?php echo $minute; ?>"><?php echo $minute; ?></option>
                      <?php 
                        for ($m=0; $m <= 60; $m++) 
                        { 

                          if($m<10)
                          {
                            $m = '0'.$m;
                          }

                          echo '<option value="'.$m.'">'.$m.'</option>';

                        }
                       ?>
                    </select>
                  </div>
              </div>
        
          </div>


        </div>

        <div class="row">
            <div class="col-md-6">
              <div class="form-group form-group-default input-group">
              <div class="form-input-group">
                <label>Start Date & Time</label>
                <input type="datetime-local" name="start_date" id="start" max ='<?php echo date('Y-m-d');?>T00:00' value="<?php echo $start_date;?>T<?php echo $start_time; ?>" class="form-control" required data-toggle="tooltip" data-title="විභාගය ආරම්භවන දිනය හා වේලාව">
              </div>
            </div>
          </div>
          <div class="col-md-6">
            
            <div class="form-group form-group-default input-group">
                <div class="form-input-group">
                  <label>Finish Date & Time</label>
                  <input type="datetime-local" name="end_date" id="end" class="form-control" min ='<?php echo date('Y-m-d');?>T00:00' required data-toggle="tooltip" data-title="විභාගය අවසන් වන දිනය හා වේලාව" value="<?php echo $end_date;?>T<?php echo $end_time; ?>">
                </div>
            </div>
            
          </div>
        </div>


      <div class="row">
          <div class="col-md-6">
            <div class="form-group form-group-default input-group">
            <div class="form-input-group">
              <label>Maximum Attendance Hours</label>

              <input type="number" name="max_hour" class="form-control" value="<?php echo $max_hour; ?>" data-toggle="tooltip" data-title="සහභාගීවීමේ අවසන් කාලය(පැය)" min="0">

            </div>
          </div>
        </div>

          <div class="col-md-6">
            <div class="form-group form-group-default input-group">
            <div class="form-input-group">
              <label>Maximum Attendance Minute</label>

              <input type="number" name="max_minu" class="form-control" value="<?php echo $max_minu; ?>" data-toggle="tooltip" data-title="සහභාගීවීමේ අවසන් කාලය(විනාඩි)" min="0">

            </div>
          </div>
        </div>
      </div>

      

        <div class="form-group" style="padding-bottom: 10px;border-bottom: 1px solid #cccc;">
            <div class="form-input-group" style="outline: none;padding: 10px 6px 10px 6px;">
              <label style="padding-bottom: 4px;">Upload Paper File <strong class="text-danger">(PDF Only)</strong></label>

              <input type="hidden" name="recent_file" value="<?php echo $pdf_name; ?>">

               <input type="file" id="upload_pdf" name="upload_file" accept="application/pdf" style="display: none;" onchange="uploaded_rejected()"/>

               <a onclick="document.getElementById('upload_pdf').click();" style="cursor: pointer;" id="td_btn" data-toggle="tooltip" data-title="ප්‍රශ්නපත්‍රය අප්ලෝඩ් කිරීම.(PDF File පමණි.)">
                    <button type="button" class="btn btn-block btn-sm" name="update_payment" id="show_upload_file" style="padding: 20px 20px 20px 20px;font-size: 18px;outline: none;background-color: #28a745;color:white;"  value="Browse..."> <i class="fa fa-check-circle"></i> <label style="font-weight: bold;cursor: pointer;color:white;padding-top: 10px;">&nbsp;Uploaded Document</label></button>
                </a>
            </div>

            <script type="text/javascript">
                      

                      function uploaded_rejected()
                      {
                        var uploaded = document.getElementById('upload_pdf').value;

                          if(uploaded == '')
                          {
                            $("#td_btn").html('<button type="button" class="btn btn-block btn-sm" name="update_payment" id="reject_upload_btn" style="padding: 20px 20px 20px 20px;font-size: 18px;outline: none;background-color: #28a745;color:white;" value="Browse...">  <i class="fa fa-check-circle"></i> <label style="font-weight: bold;cursor: pointer;color:white;padding-top: 10px;">&nbsp;Uploaded Document</label></button>');
                            $('#td_btn').prop('disabled',true);

                          }else
                          if(uploaded !== '')
                          {

                              $("#td_btn").html('<button type="button" class="btn btn-block btn-sm" id="upload_btn" style="padding: 20px 20px 20px 20px;font-size: 18px;outline: none;background-color: #ff6500;color:white;" value="Browse..." > <i class="fa fa-spinner fa-pulse"></i> <label style="font-weight: 1000px;color:white;padding-top: 10px;">&nbsp;Changing..</label></button>');

                              setTimeout(function() { 

                                  $("#td_btn").html('<button type="button" class="btn btn-block btn-sm" name="update_payment" id="upload_btn" style="padding: 20px 20px 20px 20px;font-size: 18px;outline: none;background-color: #28a745;color:white;" value="Browse..." > <i class="fa fa-check-circle"></i> <label style="font-weight: 1000px;color:white;padding-top: 10px;">&nbsp;Upload successfully</label></button>');
                                  $('#show_upload_file').prop('disabled',false);

                              }, 1000);
                            

                            
                          }

                          
                      }

                    </script>
        </div>

        <button type="submit" class="btn btn-success btn-lg btn-block" data-toggle="tooltip" data-title="වෙනස් කිරීම" style="padding: 10px 10px 10px 10px;font-size: 20px;" name="update_st_paper" value="<?php echo $paper_id; ?>"><i class="pg-icon" style="font-size: 30px;">edit</i> Edit Paper</button>


        <center style="padding-top: 10px;"><a href="exam.php" style="text-decoration: none;color: gray;text-align: center;" data-toggle="tooltip" data-title="Go Back">Back</a> </center>

        
      </div>
    </div>

    

  </div>

<div class="col-md-3">

</div>


</div>

</form>

<script type="text/javascript">
  $(document).ready(function(){
      //$('#pay_btn').prop('disabled',true); //refresh with disable button

      setTimeout(function() {
          <?php $_SESSION['msg'] = ''; ?>
          $('#successMessage').fadeOut('slow');
      }, 3000); // <-- time in milliseconds
  });
</script>

<?php  include('footer/footer.php'); ?>


<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
<!-- search data -->

<!-- no data message/no found data show in search-->

<script type="text/javascript">
  $(document).ready(function () {

    (function ($) {

        $('#myInput').keyup(function () {
            var rex = new RegExp($(this).val(), 'i');
            $('#myTable tr').hide();
            $('#myTable tr').filter(function () {
                return rex.test($(this).text());
            }).show();
            $('.no-data').hide();
            if($('#myTable tr:visible').length == 0)
            {
                $('.no-data').show();
            }

        })

    }(jQuery));

});
</script>

<script type="text/javascript">

      
    function find_class()
    {
                  
        var teacher_id = document.getElementById('teacher_id').value;

         $.ajax({
          url:'../admin/query/check.php',
          method:"POST",
          data:{check_teacher_att:teacher_id},
          success:function(data)
          {
            //alert(data)
            document.getElementById('class_id').innerHTML = data;
          }
        });
  }


</script>

<script type="text/javascript">


    function add_questions()
    {
      var show_question = document.getElementById('question').value;
      var answer_no = document.getElementById('answer_no').value;
      //alert(show_question)
      $.ajax({
          url:'../admin/query/check.php',
          method:"POST",
          data:{que_ans:show_question,answer_no:answer_no},
          success:function(data)
          {
            //alert(data)
            document.getElementById('que_ans').innerHTML = data;
          }
        });
  }


</script>