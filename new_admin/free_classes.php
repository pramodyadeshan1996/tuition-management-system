<?php
	$page = "Free Classes";
	$folder_in = '3';

  
      include('header/header.php');


        $s04 = mysqli_query($conn,"SELECT * FROM teacher_details WHERE TEACH_ID = '$teach_id'");
        while($row4 = mysqli_fetch_assoc($s04))
        {
          $teacher_name = $row4['POSITION'].". ".$row4['F_NAME']." ".$row4['L_NAME'];
          $teacher_name2 = $row4['F_NAME']." ".$row4['L_NAME'];

          $t_pic = $row4['PICTURE'];

          if($t_pic == '0')
          {
          	$t_pic = 'teacher.png';
          }
        }

         $sql0015 = mysqli_query($conn,"SELECT * FROM level");
        while($row0015=mysqli_fetch_assoc($sql0015))
        {
          $level_name = $row0015['LEVEL_NAME'];

        }

 ?>
<script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>

<ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-bottom: 04%;margin-bottom: 0;">
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
       <li class="breadcrumb-item" data-toggle="tooltip" data-title="Free Classes"> Free Classes</li>
    </ol>


  <div class="col-md-12 bg-white" style="padding-top: 20px;padding-bottom: 20px;">
    
      
      <div class="col-md-12" style="border-bottom: 1px solid #cccc;">
        <div class="row">

            <div class="col-md-9" style="margin-bottom: 10px;"><h2><a href="dashboard.php" style="color: #626462;"><span class="fa fa-angle-left"></span></a> Free Classes</h2></div>
            <div class="col-md-3" style="padding-top: 20px;">
            </div>

        </div>
      </div>

<?php 

  $sql0011 = mysqli_query($conn,"SELECT * FROM `classes`");
  $count = mysqli_num_rows($sql0011);

 ?>

            <div class="row" style="margin-top: 15px;">
                <div class="col-md-8">

                  <div class="pull-left">

                  </div>

                </div>
                <div class="col-md-4">
                    <input type="text" id="search" class="form-control pull-right" style="width: 100%;" placeholder="Search" data-toggle="tooltip" data-title="අවශ්‍ය දත්ත සෙවීම'">
                  
              </div>

            </div>
                <div class="table-responsive" style="margin-top: 10px;height: 480px;overflow: auto;">
                  <table class="table table-hover">
                  <thead>
                    <tr>
                      <th style="width: 1%;">Category</th>
                      <th style="width: 1%;">Subject</th>
                      <th style="width: 1%;">Class Name <span class="label label-success"><?php echo $count; ?></span></th>
                      <th style="width: 1%;">Teacher</th>
                      <th style="width: 1%;">Status</th>
                      <th style="width: 1%;text-align: center;">Action</th>
                    </tr>
                  </thead>
                  <tbody id="table" class="searchable">
                  <tr class="no-data">
                      <td colspan="6" class="text-danger"><span class="fa fa-warning text-danger"></span> No data</td>
                  </tr>

                  <?php 
                  $last_payment = 0;

                  $sql001 = mysqli_query($conn,"SELECT * FROM classes ORDER BY `TEACH_ID` DESC");

                  $ch = mysqli_num_rows($sql001);

                  if($ch > 0)
                  {

                        while($row001=mysqli_fetch_assoc($sql001))
                        {

                          $sub_id = $row001['SUB_ID'];
                          $teach_id = $row001['TEACH_ID'];
                          $class_id = $row001['CLASS_ID'];
                          $class_name = $row001['CLASS_NAME'];
                          $free = $row001['FREE'];
                          $active_material = $row001['MATERIAL'];

                          $st = $row001['START_DATE'];

                          $en = $row001['END_DATE'];

                          if($st == '')
                          {
                             $st = date('Y-m-01');
                          }


                          if($free == '0')
                          {
                            $active_free = '';
                          }else
                          if($free == '1')
                          {
                            $active_free = 'checked';
                          }else
                          if($free == '')
                          {
                            $active_free = '';
                          }


                          if($active_material == '0')
                          {
                            $material = '';
                          }else
                          if($active_material == '1')
                          {
                            $material = 'checked';
                          }else
                          if($active_material == '')
                          {
                            $material = '';
                          }



                          $sql002 = mysqli_query($conn,"SELECT * FROM `teacher_details` WHERE `TEACH_ID` = '$teach_id'");
                          while($row002 = mysqli_fetch_assoc($sql002))
                          {
                              $t_name = $row002['POSITION'].". ".$row002['F_NAME']." ".$row002['L_NAME'];
                          }

                          $sql003 = mysqli_query($conn,"SELECT * FROM `subject` WHERE `SUB_ID` = '$sub_id'");
                          while($row003 = mysqli_fetch_assoc($sql003))
                          {
                              $subject_name = $row003['SUBJECT_NAME'];
                              $level_id = $row003['LEVEL_ID'];
                          }

                          $sql004 = mysqli_query($conn,"SELECT * FROM `level` WHERE `LEVEL_ID` = '$level_id'");
                          while($row004 = mysqli_fetch_assoc($sql004))
                          {
                              $level_name = $row004['LEVEL_NAME'];
                          }


                          if($free == '1')
                          {
                            $icon = '<label class="label label-success">Free</label>';
                          }else
                          if($free == '0')
                          {
                            $icon = '<label class="label label-danger">Paid</label>';
                          }else
                          if($free == '')
                          {
                            $icon = '<label class="label label-danger">Paid</label>';
                          }
                                                          

                                    echo '<tr>

                                            <td>'.$level_name.'</td>

                                            <td>'.$subject_name.'</td>

                                            <td>'.$class_name.'</td>

                                            <td>'.$t_name.'</td>

                                            <td id="icon'.$class_id.'">'.$icon.'</td>

                                            <td class="text-center">

                                                <button type="button" class="btn btn-danger btn-lg" style="margin-right:10px;border-radius:80px;padding:8px 8px 8px 8px;" data-toggle="modal" data-target="#setup_free'.$class_id.'" id="setup_free_btn'.$class_id.'"><span class="fa fa-wrench" style="font-size:16px;"></span></button>

                                            </td>
                                          </tr>
                                          ';

                                          ?>

                                              <div id="setup_free<?php echo $class_id; ?>" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
                                                <div class="modal-dialog">

                                                  <!-- Modal content-->
                                                  <div class="modal-content">
                                                    <div class="modal-header">
                                                      <button type="button" class="close" id="close_free_modal<?php echo $class_id; ?>" data-dismiss="modal">&times;</button>
                                                      <h4 class="modal-title">Setup Free Class Schedule</h4>
                                                    </div>

                                                    <div class="modal-body">
                                                    <div class="form-group form-group-default">
                                                      <label>Active Free Class</label>
                                                        <div class="form-check form-check-inline switch switch-lg success" style="margin-top:8px;">
                                                          <input type="checkbox" id="free_active<?php echo $class_id; ?>" <?php echo $active_free; ?> style="float:right;">
                                                          <label for="free_active<?php echo $class_id; ?>" class="text-danger" style="text-transform: capitalize;cursor: pointer;">
                                                              </label>
                                                        </div>
                                                    </div>

                                                        <div class="form-group form-group-default">
                                                        <label>Start Date</label>
                                                          <input type="date" id="start_date<?php echo $class_id; ?>" class="form-control" value="<?php echo $st; ?>" id="start_date<?php echo $class_id; ?>"  required data-toggle="tooltip" data-title="Start Date..">
                                                        </div>

                                                        <div class="form-group form-group-default">
                                                        <label>End Date</label>
                                                        <input type="date" id="end_date<?php echo $class_id; ?>" class="form-control" value="<?php echo $en; ?>" id="end_date<?php echo $class_id; ?>" required data-toggle="tooltip" data-title="End Date..">
                                                        </div>

                                                    <div class="form-group form-group-default">
                                                      <label>Active Material</label>
                                                        <div class="form-check form-check-inline switch switch-lg success" style="margin-top:8px;"> 
                                                        
                                                          <input type="checkbox" id="material_free_active<?php echo $class_id; ?>" value="1" <?php echo $material; ?> style="float:right;" name="material">
                                                          <label for="material_free_active<?php echo $class_id; ?>" class="text-danger" style="text-transform: capitalize;cursor: pointer;">
                                                          </label>
                                                        </div>
                                                    </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                      <button type="button" id="close_free_modal<?php echo $class_id; ?>" class="btn btn-default btn-lg" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;Close</button>

                                                      <button type="button" class="btn btn-success btn-lg" name="setup_free_date" id="set_btn<?php echo $class_id; ?>" value="<?php echo $class_id; ?>"><i class="fa fa-check"></i>&nbsp;Submit</button>
                                                    </div>
                                                  </div>

                                                  </div>
                                                </div>

                              <script type="text/javascript">
                                $(document).ready(function()
                                {

                                  $('#set_btn<?php echo $class_id; ?>').click(function()
                                  {
                                      //Deactive Free Mode
                                    var free_active = $('#free_active<?php echo $class_id; ?>').is(":checked");
                                    var class_id = $('#set_btn<?php echo $class_id; ?>').val();

                                    if(free_active == true)
                                    {
                                      free_active = 1;
                                    }else
                                    if(free_active == false)
                                    {
                                      free_active = 0;
                                    }
                                    
                                    var material_free_active = $('#material_free_active<?php echo $class_id; ?>').is(":checked");

                                    if(material_free_active == true)
                                    {
                                      material_free_active = 1;
                                    }else
                                    if(material_free_active == false)
                                    {
                                      material_free_active = 0;
                                    }
                                    
                                    if(free_active == '1')
                                    {
                                      var start_date = $('#start_date<?php echo $class_id; ?>').val();
                                      var end_date = $('#end_date<?php echo $class_id; ?>').val();

                                      //alert(end_date);

                                      if(end_date == '' || start_date == '')
                                      {
                                        alert("Please set free class period!");
                                      }else
                                      {
                                        $.ajax({
                                          url:'../admin/query/update.php',
                                          method:"POST",
                                          data:{free_class_active:free_active,start_date:start_date,end_date:end_date,material:material_free_active,class_id:class_id},
                                          success:function(data)
                                          {
                                            //alert(data)
                                              const Toast = Swal.mixin({
                                              toast: true,
                                              position: 'top-end',
                                              showConfirmButton: false,
                                              timer: 1000,
                                              timerProgressBar: true,
                                              onOpen: (toast) => {
                                                toast.addEventListener('mouseenter', Swal.stopTimer)
                                                toast.addEventListener('mouseleave', Swal.resumeTimer)
                                              }
                                            })

                                            Toast.fire({
                                              icon: 'success',
                                              title: 'Free Class Activated!'
                                            })

                                            $('#icon<?php echo $class_id; ?>').html('<label class="label label-success">Free</label>');
                                            $('#close_free_modal<?php echo $class_id ?>').click();
                                          }
                                        });
                                      }
                                    }

                                    if(free_active == '0')
                                    {
                                      var start_date = $('#start_date<?php echo $class_id; ?>').val();
                                      var end_date = $('#end_date<?php echo $class_id; ?>').val();

                                      $.ajax({
                                        url:'../admin/query/update.php',
                                        method:"POST",
                                        data:{free_class_deactive:free_active,start_date:start_date,end_date:end_date,material:material_free_active,class_id:class_id},
                                        success:function(data)
                                        {
                                          //alert(data)
                                            const Toast = Swal.mixin({
                                            toast: true,
                                            position: 'top-end',
                                            showConfirmButton: false,
                                            timer: 1000,
                                            timerProgressBar: true,
                                            onOpen: (toast) => {
                                              toast.addEventListener('mouseenter', Swal.stopTimer)
                                              toast.addEventListener('mouseleave', Swal.resumeTimer)
                                            }
                                          })

                                          Toast.fire({
                                            icon: 'success',
                                            title: 'Free Class Activated!'
                                          })

                                          
                                          $('#icon<?php echo $class_id; ?>').html('<label class="label label-danger">Paid</label>');
                                          $('#close_free_modal<?php echo $class_id ?>').click();
                                        }
                                      });
                                      
                                    }
                                    
                                  });
                                });
                              </script>

                                          <?php 

                                }
                              }else
                              if($ch == '0')
                              {
                                echo '<tr><td colspan="6" class="text-danger text-center"><span class="fa fa-warning text-danger"></span> Empty Data!</td></tr>';
                              }
                                      
                                
                           ?>
                          

                          </tbody>
                          </table>
                          </div>
                          </div>


                                    


<script type="text/javascript">
  $('.save_btn').click(function(){
    
    var upload_btn0 = $('#selectedFile0').val();
    var upload_btn = $('#sel_file').val();
      
    if(empty(upload_btn0))
    {
      alert('Please select your audio file.');
    }else
    if(empty(upload_btn))
    {
      alert('Please select your document file.');
    }
   });
</script>

<script type="text/javascript">
  $('.navi').click(function(){
    $('.frm')[0].reset();
      
   });
</script>

<script>
$(document).ready(function(){
  $("#search").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#table tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

<!-- no data message/no found data show in search-->

<script type="text/javascript">
  $(document).ready(function () {

    (function ($) {

        $('#search').keyup(function () {
            var rex = new RegExp($(this).val(), 'i');
            $('#table tr').hide();
            $('#table tr').filter(function () {
                return rex.test($(this).text());
            }).show();
            $('.no-data').hide();
            if($('#table tr:visible').length == 0)
            {
                $('.no-data').show();
            }

        })

    }(jQuery));

});
</script>




</div>
</div>
</div>

<?php include('footer/footer.php'); ?>