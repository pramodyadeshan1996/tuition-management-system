
<?php

$page = "Examination";
$folder_in = '3';

  include('header/header.php'); 
 ini_set( "display_errors", 0);

 $_SESSION['new_student_marks'] = '';

 $teach_id001 = $_SESSION['TEACH_ID']; 


  ?>
  <style type="text/css">
    .btn_pulse {
  
  display: block;
  border-radius: 10%;
  cursor: pointer;
  animation: none;
  float: left;
  bottom: 5px;
  right: 5px;
  font-weight: bold;
  padding-top: 2px;
  text-align: center;
  color: white;
  font-size: 14px;
  animation: pulse 1.6s infinite;
}


@keyframes pulse {
  0% {
    -moz-box-shadow: 0 0 0 0 #7252d3;
    box-shadow: 0 0 0 0 #7252d3;
  }
  70% {
    -moz-box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
  }
  100% {
    -moz-box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
  }
}

table {
  border: 0px solid black;
}
th, td {
  border: 0px solid black;
}
td {
  padding: 5px;
}

#pagination {
  width: 100%;
  text-align: center;
}

#pagination ul li {
  display: inline;
  margin-left: 10px;
}
  </style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script src="https://pagination.js.org/dist/2.1.4/pagination.min.js"></script>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<div class="row" style="border-bottom: 1px solid #cccc;">
  <div class="col-md-9">
    <ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-bottom: 04%;margin-bottom: 0;">
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="Create Students"> Examination</li>
    </ol>
  </div>
  <div class="col-md-3">
        <div class="row">
          <div class="col-md-6"><a href="new_exam.php" data-toggle="tooltip" data-title="Create MCQ Paper" data-placement="right" class="btn btn-primary btn-lg btn-rounded btn_pulse pull-right" style="padding: 10px 10px;"><span class="fa fa-plus"></span> MCQ Paper</a></div>
          <div class="col-md-6">
            <a href="structured_paper.php" data-toggle="tooltip" data-title="Create Structured Paper" data-placement="right" class="btn btn-primary btn-lg btn-rounded btn_pulse" style="padding: 10px 10px;"><span class="fa fa-plus"></span> S & E Paper</a>
          </div>
        </div>

            

    </div>
  </div>
</div>

<?php 


          if($_REQUEST['exam_type'] == '1')
          {
            $exam_name = '(MCQ Paper)';
          }else
          if($_REQUEST['exam_type'] == '2')
          {
            $exam_name = '(Structured & Essay Paper)';
          }else
          if($_REQUEST['exam_type'] == '3')
          {
            $exam_name = '(Results)';
          }else
          if($_REQUEST['exam_type'] == '' || empty($_REQUEST['exam_type']))
          {
            $exam_name = '';
          }

         ?>


<div class="col-md-12" style="margin-top: 0;">
  <h4 style="text-transform: capitalize;"><a href="dashboard.php" data-toggle="tooltip" data-title="Back"><i class="pg-icon" style="font-size: 22px;">chevron_left</i></a> Examination <?php echo $exam_name; ?>
  <a href="view_exam_result.php" class="btn btn-success pull-right">View Exam Result</a></h4>


<div class=" container-fluid   container-fixed-lg bg-white">
<div class="card card-transparent">
<div class="card-header ">
<div class="card-title">
</div>
<div class="pull-left">
<div class="col-xs-12">



<form action="print_exam_result.php" method="POST" target="_blank" style="display: none;">
      <button type="submit" class="btn btn-success btn-lg btn-rounded" name="teacher" style="padding: 10px 12px 10px 12px;font-size: 20px;" value="<?php echo $teach_id ?>"><span class="fa fa-print"></span></button>
</form>

</div>
</div>
<div class="pull-right">
  <div class="row">
    <div class="col-md-6">

      <form action="exam.php" method="POST">
      <select class="form-control" id="exam_type" name="exam_type" onchange="change_exam();">

        <?php 

          if($_REQUEST['exam_type'] == '1' || $_SESSION['exam_type'] == '1')
          {
            echo '

              <option value="1">MCQ Paper</option>
              <option value="2">Structured & Essay Paper</option>

            ';
          }else
          if($_REQUEST['exam_type'] == '2' || $_SESSION['exam_type'] == '2')
          {
            echo '

              <option value="2">Structured & Essay Paper</option>
              <option value="1">MCQ Paper</option>

            ';
          }else
          if($_REQUEST['exam_type'] == '' || empty($_REQUEST['exam_type']))
          {
            echo '

              <option value="0">Select Exam Type</option>
              <option value="1">MCQ Paper</option>
              <option value="2">Structured & Essay Paper</option>

            ';
          }

         ?>
        
      </select>
      <button type="submit" name="change_exam_type" id="exam" style="display: none;">X</button>
      </form>

      <?php 

        if(isset($_POST['change_exam_type']))
        {
          $_SESSION['exam_type'] = $_POST['exam_type'];
        }

       ?>


      <script type="text/javascript">
        function change_exam()
        {
            document.getElementById('exam').click();
        }
      </script>
    </div>
    <div class="col-md-6">
      <input type="text" id="search" class="form-control pull-right" placeholder="Search" data-toggle="tooltip" data-title="අවශ්‍ය දත්ත සෙවීම'">
    </div>
</div>
</div>
<div class="clearfix"></div>
</div>


<script type="text/javascript">

  function check_teacher_class()
 {
      var teacher_id = document.getElementById('teacher').value;

      $.ajax({
        url:'../admin/query/check.php',
        method:"POST",
        data:{check_teacher_class:teacher_id},
        success:function(data)
        {
          //alert(data)
          document.getElementById('class_data').innerHTML = data;
          
        }
      });
  }

</script>
<!-- Check teacher's classes -->

<div class="card-body table-responsive" style="height: auto;margin-bottom: 0;">
  <label></label>
  <table class="table demo-table-search table-responsive-block text-left" id="tableWithSearch">
 

             <thead>

                <th style="width: 10%;">Create Date</th>
                <th style="width: 12%;">Teacher Name</th>
                <th style="width: 16%;">Class Name</th>
                <th  style="width: 16%;">Paper Name</th>
                <th style="width: 10%;">Start Date</th>
                <th style="width: 10%;">Finish Date</th>
                <th style="width: 3%;">Participation</th>
                <!-- 
                <th style="width: 10%;">Status</th> -->
                <th class="text-center">Action</th>

              </thead>
              <tbody id="myTable">

          
      
   <?php 



          if($_REQUEST['exam_type'] == '1' || $_SESSION['exam_type'] == '1')
          {


            $sql001 = mysqli_query($conn,"SELECT * FROM `paper_master` WHERE `PAPER_TYPE` = '1' AND `TEACH_ID` = '$teach_id001' ORDER BY `UPLOAD_TIME` DESC");
            $ch = mysqli_num_rows($sql001);
            if($ch > 0)
            {
                while($row001 = mysqli_fetch_assoc($sql001))
                {
                  $paper_id = $row001['PAPER_ID'];
                  $paper_name = $row001['PAPER_NAME'];
                  $status = $row001['STATUS'];
                  $create_date = $row001['UPLOAD_TIME'];

                  $start_d = $row001['START_DATE'];
                  $start_t01 = $row001['START_TIME'];

                  $finish_d = $row001['FINISH_DATE'];
                  $finish_t = $row001['FINISH_TIME'];

                  $file_name = $row001['PDF_NAME'];

                  $create_date = $row001['UPLOAD_TIME'];

                  $class_id = $row001['CLASS_ID'];

                  $str2 = strtotime($start_t01);
                  $start_t = date('h:i A',$str2);

                  $start_dt = $start_d." ".$start_t;

                  $str2 = strtotime($finish_t);
                  $finish_t = date('h:i A',$str2);

                  $finish_dt = $finish_d." ".$finish_t;

                   $sql002 = mysqli_query($conn,"SELECT * FROM `classes` WHERE `CLASS_ID` = '$class_id'");

                  while($row002 = mysqli_fetch_assoc($sql002))
                  {
                      $class_name = $row002['CLASS_NAME'];
                      $teach_id = $row002['TEACH_ID'];
                  
                      $sql003 = mysqli_query($conn,"SELECT * FROM `teacher_details` WHERE `TEACH_ID` = '$teach_id'");

                      while($row003 = mysqli_fetch_assoc($sql003))
                      {
                        $teacher_name = $row003['POSITION'].". ".$row003['F_NAME']." ".$row003['L_NAME'];
                      }

                  }
                  

                    //<td class="v-align-middle bold"><label class="label label-'.$alert.'"><span class="fa fa-'.$icon.'"></span> '.$status_name.'</label></td>
                    
                  $sql003 = mysqli_query($conn,"SELECT * FROM `exam_result` WHERE `PAPER_ID` = '$paper_id'");
  
                  $check_attendance = mysqli_num_rows($sql003);

                  if($check_attendance<10)
                  {
                    if($check_attendance == '0')
                    {
                      $check_attendance = '0';
                    }else
                    {
                      $check_attendance = '0'.$check_attendance;
                    }
                    
                  }

                    echo '<tr>';

                    echo '<td class="v-align-middle">'.$create_date.'</td>
                          <td class="v-align-middle bold">'.$teacher_name.'</td>
                          <td class="v-align-middle bold">'.$class_name.'</td>
                          <td class="v-align-middle bold">'.$paper_name.'</td>
                          <td class="v-align-middle">'.$start_dt.'</td>
                          <td class="v-align-middle">'.$finish_dt.'</td>
                          <td class="v-align-middle bold"><label class="badge badge-success badge-lg">'.$check_attendance.'</label></td>
                          <td class="v-align-middle text-center">

                              <a href="edit_exam.php?paper_id='.$paper_id.'&&exam_type=1" data-toggle="tooltip" data-title="Edit Student Data" class="btn btn-complete  btn-xs btn-rounded" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;margin-right:10px;margin-left:10px;color:white;margin-top:10px;"><i class="pg-icon">edit</i></a>';?>


                              <a href="../teacher/query/delete.php?delete_paper=<?php echo $paper_id; ?>&&file_name=<?php echo $file_name; ?>&&exam_type=<?php echo $_SESSION['exam_type']; ?>" onclick="return confirm('Are you sure Delete?')" data-toggle="tooltip" data-title="Delete Examination" class="btn btn-danger  btn-xs btn-rounded" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;margin-right:10px;margin-left:10px;color:white;margin-top:10px;">
                                <i class="pg-icon">trash_alt</i>
                              </a>


                              <?php



                              echo '



                              <a href="edit_mcq_answer.php?paper_id='.$paper_id.'" data-toggle="tooltip" data-title="Update MCQ Answers" class="btn btn-info  btn-xs btn-rounded" style="padding:8px 10px 8px 10px;box-shadow:0px 0px 4px 3px #cccc;margin-right:2px;margin-left:2px;color:white;margin-top:10px;background-color:#f39c12;border:none;"><span class="fa fa-paperclip" style="font-size:16px;"></span>
                              </a>

                              
                              <a href="participate_exam.php?paper_id='.$paper_id.'" data-toggle="tooltip" data-title="Participated Students" class="btn btn-success  btn-xs btn-rounded" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;margin-right:10px;margin-left:10px;color:white;margin-top:10px;"><i class="pg-icon">users</i>
                              </a>



                              <a href="add_new_marks.php?paper_id='.$paper_id.'" data-toggle="tooltip" data-title="Add New Marking" class="btn btn-info  btn-xs btn-rounded" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;margin-right:2px;margin-left:2px;color:white;margin-top:10px;"><i class="pg-icon">add_user</i>
                              </a>

                          </td>
                          </tr>
                          ';



                          
                        }
                      }
                    }else
                    if($_REQUEST['exam_type'] == '2'|| $_SESSION['exam_type'] == '2')
                    {
                      //Structured Paper

                      $sql001 = mysqli_query($conn,"SELECT * FROM `paper_master` WHERE `PAPER_TYPE` = '2' AND `TEACH_ID` = '$teach_id001' ORDER BY `UPLOAD_TIME` DESC");
                      $ch = mysqli_num_rows($sql001);
                      if($ch > 0)
                      {
                          while($row001 = mysqli_fetch_assoc($sql001))
                          {
                            $paper_id = $row001['PAPER_ID'];
                            $paper_name = $row001['PAPER_NAME'];
                            $status = $row001['STATUS'];
                            $create_date = $row001['UPLOAD_TIME'];

                            $start_d = $row001['START_DATE'];
                            $start_t01 = $row001['START_TIME'];

                            $finish_d = $row001['FINISH_DATE'];
                            $finish_t = $row001['FINISH_TIME'];

                            $file_name = $row001['PDF_NAME'];

                            $create_date = $row001['UPLOAD_TIME'];

                            $class_id = $row001['CLASS_ID'];

                            $str2 = strtotime($start_t01);
                            $start_t = date('h:i A',$str2);

                            $start_dt = $start_d." ".$start_t;

                            $str2 = strtotime($finish_t);
                            $finish_t = date('h:i A',$str2);

                            $finish_dt = $finish_d." ".$finish_t;

                             $sql002 = mysqli_query($conn,"SELECT * FROM `classes` WHERE `CLASS_ID` = '$class_id'");

                            while($row002 = mysqli_fetch_assoc($sql002))
                            {
                                $class_name = $row002['CLASS_NAME'];
                                $teach_id = $row002['TEACH_ID'];
                            
                                $sql003 = mysqli_query($conn,"SELECT * FROM `teacher_details` WHERE `TEACH_ID` = '$teach_id'");

                                while($row003 = mysqli_fetch_assoc($sql003))
                                {
                                  $teacher_name = $row003['POSITION'].". ".$row003['F_NAME']." ".$row003['L_NAME'];
                                }

                            }

                            $sql003 = mysqli_query($conn,"SELECT * FROM `exam_result` WHERE `PAPER_ID` = '$paper_id'");
  
                            $check_attendance001 = mysqli_num_rows($sql003);

                            $sql004 = mysqli_query($conn,"SELECT * FROM `student_essay_master` WHERE `PAPER_ID` = '$paper_id'");
  
                            $check_attendance002 = mysqli_num_rows($sql004);

                            $total_participate = 0;

                            $total_participate = $check_attendance001+$check_attendance002;


                            if($total_participate < 10)
                            {
                              $check_attendance = '0'.$total_participate;
                            }else
                            {
                              $check_attendance = $total_participate;
                            }
                            

                              //<td class="v-align-middle bold"><label class="label label-'.$alert.'"><span class="fa fa-'.$icon.'"></span> '.$status_name.'</label></td>
                              
                              echo '<tr id="delete_row'.$paper_id.'">';

                              echo '<td class="v-align-middle">'.$create_date.'</td>
                                    <td class="v-align-middle bold">'.$teacher_name.'</td>
                                    <td class="v-align-middle bold">'.$class_name.'</td>
                                    <td class="v-align-middle bold">'.$paper_name.'</td>
                                    <td class="v-align-middle">'.$start_dt.'</td>
                                    <td class="v-align-middle">'.$finish_dt.'</td>
                                    <td class="v-align-middle bold"><label class="badge badge-success badge-lg">'.$check_attendance.'</label></td>
                                    <td class="v-align-middle text-center">

                                        <a href="edit_st_exam.php?paper_id='.$paper_id.'&&exam_type=2" data-toggle="tooltip" data-title="Edit Student Data" class="btn btn-complete  btn-xs btn-rounded" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;margin-right:2px;margin-left:2px;color:white;margin-top:10px;"><i class="pg-icon">edit</i></a>';?>


                                        

                              <a href="../admin/query/delete.php?delete_paper=<?php echo $paper_id; ?>&&file_name=<?php echo $file_name; ?>&&exam_type=<?php echo $_SESSION['exam_type']; ?>" onclick="return confirm('Are you sure Delete?')" data-toggle="tooltip" data-title="Delete Examination" class="btn btn-danger  btn-xs btn-rounded" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;margin-right:10px;margin-left:10px;color:white;margin-top:10px;">
                                <i class="pg-icon">trash_alt</i>
                              </a>
 <!--
                                        <script type="text/javascript">
            
                                           
                                             function delete_paper(paper_id,file_name)
                                             {
                                              //alert(paper_id)
                                              
                                              if(confirm('Are you sure Delete Paper?'))
                                              {
                                                 $.ajax({
                                                  url:'../admin/query/delete.php',
                                                  method:"POST",
                                                  data:{delete_paper:paper_id,file_name:file_name},
                                                  success:function(data)
                                                  {
                                                    //alert(data)
                                                    document.getElementById('delete_row<?php echo $paper_id; ?>').style.display = 'none';
                                                    
                                                  }
                                                 })
                                              }
                                               
                                             

                                            }

                                          </script> -->


                                        <?php

                                        echo '
                                        <a href="participate_exam_str.php?paper_id='.$paper_id.'" data-toggle="tooltip" data-title="Participated Students" class="btn btn-warning  btn-xs btn-rounded" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;margin-right:2px;margin-left:2px;margin-top:10px;"><i class="pg-icon">users</i>
                                        </a>


                                        <a href="paper_marking.php?paper_id='.$paper_id.'" data-toggle="tooltip" data-title="Paper Marking" class="btn btn-success  btn-xs btn-rounded" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;margin-right:2px;margin-left:2px;color:white;margin-top:10px;"><i class="pg-icon">tick_circle</i>
                                        </a>


                                        <a href="add_new_marks.php?paper_id='.$paper_id.'" data-toggle="tooltip" data-title="Add New Marking" class="btn btn-info  btn-xs btn-rounded" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;margin-right:2px;margin-left:2px;color:white;margin-top:10px;"><i class="pg-icon">add_user</i>
                                        </a>

                                    </td>
                                    </tr>
                                    ';



                                    
                                  }
                                }
                              }
                  
 ?>
    
    
  </tbody>
  </table>
</div>


</div>

</div>
</div>

<script type="text/javascript">
  
    $(document).ready(function(){  
     $('#level_clz').change(function(){

       var level_clz = $(this).val();
       var teach_id = $('#teach_id').val();

       $.ajax({
        url:'../teacher/query/check.php',
        method:"POST",
        data:{create_clz:level_clz,teach_id:teach_id},
        success:function(data)
        {
          //alert(data)
            $('#subject').html(data);
          
        }
       })
     

    });

     
   });

  </script>

<script type="text/javascript">
  setInterval(function() {
    $('blink').fadeIn(1300).fadeOut(1500);
}, 1000);
</script>

<script>
$(document).ready(function(){
  $("#search").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $(".myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

<script type="text/javascript">
  $(document).ready(function () {

    (function ($) {

        $('#search').keyup(function () {
            var rex = new RegExp($(this).val(), 'i');
            $('#myTable tr').hide();
            $('#myTable tr').filter(function () {
                return rex.test($(this).text());
            }).show();
            $('.no-data').hide();
            if($('#myTable tr:visible').length == 0)
            {
                $('.no-data').show();
            }

        })

    }(jQuery));

});
</script>

<script>
$(document).ready(function(){

  $("#up_rec").on("change", function() {
      $('.show_image').hide();
  });
});
</script>
<script type="text/javascript">
  setInterval(function() {
    $('.blink').fadeIn(1300).fadeOut(1500);
}, 1000);
</script>

<script type="text/javascript">
  $("#show_hide_password a").on('click', function(event) {
        event.preventDefault();
        if($('#show_hide_password input').attr("type") == "text"){
            $('#show_hide_password input').attr('type', 'password');
            $('#show_hide_password i').addClass( "fa-eye-slash" );
            $('#show_hide_password i').removeClass( "fa-eye" );
        }else if($('#show_hide_password input').attr("type") == "password"){
            $('#show_hide_password input').attr('type', 'text');
            $('#show_hide_password i').removeClass( "fa-eye-slash" );
            $('#show_hide_password i').addClass( "fa-eye" );
        }
    });
</script>

<script type="text/javascript">
  let rows = []
$('table tbody tr').each(function(i, row) {
  return rows.push(row);
});

$('#pagination').pagination({
    dataSource: rows,
    pageSize: 8,
    callback: function(data, pagination) {
        $('tbody').html(data);
    }
})
</script>



<script type="text/javascript">
  $('#error').hide(); //error message hidden

  $(document).ready(function(){


    $('#error').hide(); //error message hidden



/*   validation form*/

   $('#password,#re_password').keyup(function(){

   var psw = $('#password').val();
   var re_psw = $('#re_password').val();
  
  if(psw !== '' && re_psw !== '')
  {
        if(psw !== re_psw)
       {    
            $('#error').show();

            $('#save').prop("disabled",true);
            $('#save').css('opacity','0.8');
            $('#save').css('cursor','not-allowed');

            $('#error').html('<div class="alert alert-danger col-md-12 text-center" style="color:red;font-size:16px;width:100%;"><span class="fa fa-warning text-danger"> The password you entered is incorrect.</span></div>');

       }else
       if(psw == re_psw)
       {    
            $('#error').show();

            $('#save').prop("disabled",false);
            $('#save').css('opacity','3.5');
            $('#save').css('cursor','pointer');

            $('#error').html('<div class="alert alert-success col-md-12 text-center" style="color:green;font-size:16px;width:100%;"><span class="fa fa-check-circle text-success"> The password you entered is correct.</span></div>');
       }
   }
   
   });

/*   validation form*/

});
</script>

<script>
$(document).ready(function () {

    $('#f_name,#l_name').bind('keyup blur',function()
    { 
           var thistext = $(this);
           thistext.val(thistext.val().replace(/[^a-z A-Z .]/g,'') ); 

           
    });

})
</script>

<script>
$(document).ready(function () {

    $('#address').bind('keyup blur',function()
    { 
           var thistext = $(this);
           thistext.val(thistext.val().replace(/[^a-z A-z . 0-9-/,]/g,'') ); 

           
    });

})
</script>

<script>
$(document).ready(function () {

    $('#email').bind('keyup blur',function()
    { 
           var thistext = $(this);
           thistext.val(thistext.val().replace(/[^a-zA-z0-9!#$%&'*+-/=?^_`{|,@]/g,'') ); 

           
    });

})
</script>

<script>
$(document).ready(function () {

    $('#number').bind('keyup blur',function()
    { 
           var thistext = $(this);
           thistext.val(thistext.val().replace(/[^0-9]/g,'') ); 

           
    });

})
</script>

<?php  include('footer/footer.php'); ?>


