<?php 
    session_start();
    include('../../connect/connect.php');

    /*$c = mysqli_connect('localhost','root','','bst_institute');
    $last_date = '2021-09-07';

    $sql0011 = mysqli_query($c,"SELECT * FROM `uploads` WHERE `UPL_DATE` <= '$last_date' AND `TYPE` = 'Document'");
    while($row0011 = mysqli_fetch_assoc($sql0011))
    {
      $file_nm = $row0011['FILE_NAME'];
      $upload_id = $row0011['UPLOAD_ID'];
      echo $type = $row0011['TYPE'];

      mysqli_query($c,"DELETE FROM `uploads` WHERE `UPLOAD_ID` = '$upload_id'");

      unlink('../../uploads/document/'.$file_nm.'');
        
    }*/
      ?>

    
          <!DOCTYPE html>
          <html lang="en">

          <head>

            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
            <meta name="description" content="">
            <meta name="author" content="">

            <title>Login</title>

            <!-- Custom fonts for this template-->
            <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
            <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
            <link href="https://fonts.googleapis.com/css?family=Muli&display=swap" rel="stylesheet">
            <!-- Custom styles for this template-->
            <link href="css/sb-admin-2.min.css" rel="stylesheet">
            <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

            <!-- Latest compiled and minified CSS -->
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

          <!-- jQuery library -->
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

          <!-- Latest compiled JavaScript -->
          <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
          <style type="text/css">


          button {
              --c: goldenrod;
              color: var(--c);
              font-size: 16px;
              border: 0.3em solid var(--c);
              border-radius: 0.5em;
              text-transform: uppercase;
              font-weight: bold;
              font-family: sans-serif;
              letter-spacing: 0.1em;
              text-align: center;
              position: relative;
              overflow: hidden;
              z-index: 1;
              transition: 0.5s;
              margin: 0em;
          }

          button span {
              position: absolute;
              width: 25%;
              height: 100%;
              background-color: var(--c);
              transform: translateY(150%);
              border-radius: 50%;
              left: calc((var(--n) - 1) * 25%);
              transition: 0.9s;
              transition-delay: calc((var(--n) - 1) * 0.1s);
              z-index: -1;
          }

          button:hover {
              color: black;
          }

          button:hover span {
              transform: translateY(0) scale(2);
          }

          button span:nth-child(1) {
              --n: 1;
          }

          button span:nth-child(2) {
              --n: 2;
          }

          button span:nth-child(3) {
              --n: 3;
          }

          button span:nth-child(4) {
              --n: 4;
          }

          </style>

          </head>

          <body class="bg-gradient-primary" style="font-family: 'Muli', sans-serif;background: #16BFFD;  /* fallback for old browsers */
          background: -webkit-linear-gradient(to right, #CB3066, #16BFFD);  /* Chrome 10-25, Safari 5.1-6 */
          background: linear-gradient(to right, #CB3066, #16BFFD); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
          ">

           

            <div class="container">

              <div class="col-md-12">
              <!-- Outer Row -->
              <div class="row justify-content-center">
               <div class="col-xl-4 col-lg-4 col-md-4"></div>

                <div class="col-md-4" style="border:1px solid #00000000;margin-top: 10%;background-color: #0000007d;padding: 40px 40px 40px 40px;border-radius: 4px;margin-bottom: 25px">
                           

                            <div class="text-center">
                              <h1 style="color: white;">Welcome!</h1>
                            </div>
                              <br>

                              <div class="form-group">
                                <input type="text" class="form-control form-control-user" aria-describedby="emailHelp" placeholder="Enter Username..." name="username" style="padding-left: 24px;height:50px;border-radius: 50px;" id="user" required autocomplete="off" autofocus="on">
                              </div>
                              <div class="form-group">
                                <input type="password" class="form-control form-control-user" placeholder="Enter Password" name="password" style="padding-left: 24px;height:50px;border-radius: 50px;" id="psw" required autocomplete="off">
                              </div>
                              
                              <button type="submit" onclick="this.disabled=true;" class="btn btn-info btn-active btn-user btn-block" name="submit" id="log" style="margin-top: 40px;height:50px;border-radius: 50px;font-size: 16px;background: rgb(38,158,245);background: linear-gradient(191deg, rgba(38,158,245,1) 47%, rgba(54,163,245,1) 87%);font-weight: bold;outline: none;border:rgb(218 165 32);"><span></span><span></span><span></span><span></span> 
                                Login
                              </button>

                          <div id="result" style="margin-top: 10px;"></div>
                          <div class="col-md-12" style="text-align: center;"><small style="color:white;">Admin Login Page - &copy;IBS Developer Team</small>

                            <center><a href="../../index.php"><span class="fa fa-home" style="color:#ffff;font-size: 22px;margin-top: 10px;"></span></a></center>
                          </div>

                    </div>

                <div class="col-xl-4 col-lg-4 col-md-4"></div>

              </div>
          </div>
            </div>

            <!-- Bootstrap core JavaScript-->
            <script src="vendor/jquery/jquery.min.js"></script>
            <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

            <!-- Core plugin JavaScript-->
            <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

            <!-- Custom scripts for all pages-->
            <script src="js/sb-admin-2.min.js"></script>
            <script type="text/javascript">
            
            
            /*login script*/
            $("#log").click(function(){
              var user = $('#user').val();
              var pasw = $('#psw').val();

              if(user == '')
              {
                $("#result").html("<div class='alert alert-danger alert-dismissible' id='alert'><button type='button' class='close' data-dismiss='alert'>&times;</button><span class='fa fa-warning'></span> <strong>Warning : </strong> Empty Feild.Please fill the Inputs.</div>");
                $("#alert").delay(3000).slideUp(500, function() {
                      $("#alert").slideUp(800);              
                });
              }
              else
              if( pasw == '')
              {
                $("#result").html("<div class='alert alert-danger alert-dismissible' id='alert'><button type='button' class='close' data-dismiss='alert'>&times;</button><span class='fa fa-warning'></span> <strong>Warning : </strong> Empty Feild.Please fill the Inputs.</div>");
                $("#alert").delay(3000).slideUp(500, function() {
                      $("#alert").slideUp(800);              
                });
              }
              else
              if(user !== '' && pasw !== '')
              {
                $.ajax({  
                               url:"log.php",  
                               method:"POST",  
                               data:{user:user,psw:pasw},  
                               success:function(data){ 
                               if(data == 0)
                               {
                                    Swal.fire({
                                    position: 'top-middle',
                                    type: 'error',
                                    title: 'Username Or Password Incorrect',
                                    showConfirmButton: false,
                                    timer: 1500
                                    })
                                    $("#psw").focus();
                                    $("#psw").val('');
                               }

                               //alert(data)

                               if(data == 2)
                               {
                                    $("#result").html("<div class='alert alert-danger alert-dismissible' id='alert'><button type='button' class='close' data-dismiss='alert'>&times;</button><span class='fa fa-warning'></span> <strong>Warning : </strong> Sorry, your registered account has not been verified yet.</div>");
                               }


                               if(data == 3)
                               {
                                    $("#result").html("<div class='alert alert-danger alert-dismissible' id='alert'><button type='button' class='close' data-dismiss='alert'>&times;</button><span class='fa fa-warning'></span> <strong>Warning : </strong> Sorry, Suspended your system.</div>");
                               }

                               if(data == 1)
                               {
                                  window.location.href = "OTP_verify.php";
                               }
                              }
                });
              }
            });
            /*login script*/
            

            /*Enter key press after login to page*/
            $('body').on( 'keyup', function( e ) {
                      if( e.which == 13 ) {
                        //GOTO DOWN
                        $('#log').click();
                      }
                      
            });
            /*Enter key press after login to page*/


          </script>

          </body>

         </html>
  
  <?php 

   /* $count = 0;


    $sql001 = mysqli_query($conn,"SELECT * FROM `payment_data` WHERE `CLASS_ID` = '268' AND `YEAR` = '2021' AND `MONTH` = '09'");
    while($row01 = mysqli_fetch_assoc($sql001))
    {
      $stu_id = $row01['STU_ID'];

      $sql02 = mysqli_query($conn,"SELECT * FROM `student_details` WHERE `STU_ID` = '$stu_id'");
      while($row02 = mysqli_fetch_assoc($sql02))
      {
        $name = $row02['F_NAME']." ".$row02['L_NAME'];
      }

      $sql03 = mysqli_query($conn,"SELECT * FROM `stu_login` WHERE `STU_ID` = '$stu_id'");
      while($row03 = mysqli_fetch_assoc($sql03))
      {
        $reg = $row03['REGISTER_ID'];
      }

      echo $name." - ".$reg."<br>";
      $count = $count+1;

    }

    echo "<br>Total Student - ".$count;*/

   ?>
