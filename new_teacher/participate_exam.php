
<?php

$page = "Participated Students";
$folder_in = '0';

$paper_id = $_GET['paper_id'];

  include('header/header.php'); 
 ini_set( "display_errors", 0); 


  ?>
  <style type="text/css">
    .btn_pulse {
  
  display: block;
  border-radius: 10%;
  cursor: pointer;
  animation: none;
  float: left;
  bottom: 5px;
  right: 5px;
  font-weight: bold;
  padding-top: 2px;
  text-align: center;
  color: white;
  font-size: 14px;
  animation: pulse 1.6s infinite;
}


@keyframes pulse {
  0% {
    -moz-box-shadow: 0 0 0 0 #7252d3;
    box-shadow: 0 0 0 0 #7252d3;
  }
  70% {
    -moz-box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
  }
  100% {
    -moz-box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
  }
}

table {
  border: 0px solid black;
}
th, td {
  border: 0px solid black;
}
td {
  padding: 5px;
}

#pagination {
  width: 100%;
  text-align: center;
}

#pagination ul li {
  display: inline;
  margin-left: 10px;
}
  </style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script src="https://pagination.js.org/dist/2.1.4/pagination.min.js"></script>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<div class="row" style="border-bottom: 1px solid #cccc;">
  <div class="col-md-11">
    <ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-bottom: 04%;margin-bottom: 0;">
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="Participated Students"> Participated Students</li>
    </ol>
  </div>
  <div class="col-md-1">
    </div>
  </div>
</div>




<div class="col-md-12" style="margin-top: 0;">
  <h3 style="text-transform: capitalize;"><a href="exam.php" data-toggle="tooltip" data-title="Back"><i class="pg-icon" style="font-size: 22px;">chevron_left</i></a> Participated Students
  <small class="text-muted">(MCQ Paper)</small></h3>

<div class=" container-fluid   container-fixed-lg bg-white">
<div class="card card-transparent">
<div class="card-header ">
<div class="card-title">
</div>
<div class="pull-left">
<div class="col-xs-12">

  <?php 

      $sql001 = mysqli_query($conn,"SELECT * FROM `paper_master` WHERE `PAPER_ID` = '$paper_id'");
      while($row001 = mysqli_fetch_assoc($sql001))
      {
        $class_id = $row001['CLASS_ID'];
        $paper_name = $row001['PAPER_NAME'];
        $paper_total_question = $row001['NO_OF_QUESTIONS'];
        $total_marks_answer = $row001['ANSWER_MARKS'];

         $sql002 = mysqli_query($conn,"SELECT * FROM `classes` WHERE `CLASS_ID` = '$class_id'");

        while($row002 = mysqli_fetch_assoc($sql002))
        {
            $class_name = $row002['CLASS_NAME'];
            $teach_id = $row002['TEACH_ID'];
        
            $sql003 = mysqli_query($conn,"SELECT * FROM `teacher_details` WHERE `TEACH_ID` = '$teach_id'");

            while($row003 = mysqli_fetch_assoc($sql003))
            {
              $teacher_name = $row003['POSITION'].". ".$row003['F_NAME']." ".$row003['L_NAME'];
            }

        }

        echo '<h4>'.$paper_name.' <small class="text-muted">'.$class_name.'</small><label style="font-size:14px;" class="text-muted"> ('.$teacher_name.')</label></h4>';
      }

   ?>


</div>
</div>
<div class="pull-right">
<div class="col-xs-12">
<input type="text" id="search" class="form-control pull-right" placeholder="Search" data-toggle="tooltip" data-title="අවශ්‍ය දත්ත සෙවීම'">
</div>
</div>
<div class="clearfix"></div>
</div>

<?php 
  
   $sql003 = mysqli_query($conn,"SELECT * FROM `student_mcq_master` WHERE `PAPER_ID` = '$paper_id'");
  $participated = mysqli_num_rows($sql003);

 ?>

<div class="card-body table-responsive" style="height: auto;margin-bottom: 0;">
  <table class="table demo-table-search table-responsive-block text-left" id="tableWithSearch">
  <thead>

    <th style="width: 16%;text-align: left;">Regisetered ID</th>
    <th style="width: 14%;">Student Name <label class="label label-success"><?php echo $participated; ?></label></th>
    <!-- <th style="width: 12%;">Spent Time</th>
    <th style="width: 16%;">Sitting time</th> -->
    <th style="width: 16%;text-align: center;">Submitted Time</th>
    <th style="width: 14%;text-align: left;" colspan="2">Total Marks</th><!-- 
    <th style="width: 15%;">Correct Answer</th> 
    <th class="text-center">Action</th>-->

  </thead>
  <tbody>
      
   <?php 
        
                      /*System added Marks*/
                      $sql003 = mysqli_query($conn,"SELECT * FROM `student_mcq_master` WHERE `PAPER_ID` = '$paper_id'");
  
                      $ch = mysqli_num_rows($sql003);
                      if($ch > 0)
                      {

                              while($row003 = mysqli_fetch_assoc($sql003))
                              {
                                    $mcq_total_marks = $row003['MCQ_MARK'];
                                    $submit_dt = $row003['SUBMIT_DATE_TIME'];
                                    $student_id001 = $row003['STU_ID'];
                                    $correct_answer = $row003['CORRECT_ANSWERS'];
                                    $total_question = $row003['TOTAL_QUESTION'];
                                    $question_mark = $row003['QUESTION_MARK'];

                                                                    
                                    $my_total_marks = 0;
                                    $my_final_total_marks = 0;
                                    $final_mark = 0;

                                    $sql0033 = mysqli_query($conn,"SELECT * FROM `student_mcq_transaction` WHERE `PAPER_ID` = '$paper_id' AND `STU_ID` = '$student_id001'"); //check mcq test table
                                    while($row0033 = mysqli_fetch_assoc($sql0033))
                                    {

                                      $correct_answer_no = $row0033['CORRECT_ANSWER']; //System Correct Answer
                                      $marks_answer = $row0033['MARKS']; //Mark
                                      $student_answer = $row0033['ANSWER']; //Student Answer
                                      $question = $row0033['QUESTION']; //Question

                                      $total_paper_mark = 0; //total question X Question for Mark
                                      $total_paper_mark = $question*$marks_answer;

                                      if($correct_answer_no == $student_answer)
                                      {
                                        $my_total_marks = $my_total_marks+1;
                                      }

                                    }

                                    $total_paper_mark = $paper_total_question*$total_marks_answer;
                              	
                                    $mcq_my_mark = $my_total_marks*$total_marks_answer; //Correct answer amount x per question for marks
                                      
                                    $final_mark = (50*$mcq_my_mark)/$total_paper_mark;


                                    $sql002 = mysqli_query($conn,"SELECT * FROM `student_details` WHERE `STU_ID` = '$student_id001'");

                                    while($row002 = mysqli_fetch_assoc($sql002))
                                    {
                                          $student_name = $row002['F_NAME']." ".$row002['L_NAME'];

                                           $sql00311 = mysqli_query($conn,"SELECT * FROM `stu_login` WHERE `STU_ID` = '$student_id001'");

                                            while($row00311 = mysqli_fetch_assoc($sql00311))
                                            {
                                                  $reg_id = $row00311['REGISTER_ID'];
                                            }
                                    }
                              

                                //<td class="v-align-middle bold"><label class="label label-'.$alert.'"><span class="fa fa-'.$icon.'"></span> '.$status_name.'</label></td>
                                
                                echo '<tr>';

                                echo '
                                      <td class="v-align-middle">'.$reg_id.'</td>
                                      <td class="v-align-middle">'.$student_name.'</td>
                                      <td class="v-align-middle bold text-center">'.$submit_dt.'</td>
                                      <td colspan="2" class="v-align-middle bold" style="font-size:14px"><label class="text-danger" style="text-align: center;font-weight:bold;">'.$final_mark.'/50</label>';


                                  $sql001001 = mysqli_query($conn,"SELECT * FROM `student_mcq_master` WHERE `PAPER_ID` = '$paper_id' AND `STU_ID` = '$student_id001'");
                                  while($row001001 = mysqli_fetch_assoc($sql001001))
                                  {

                                    $mcq_id = $row001001['STU_MCQ_MASTER_ID'];
                                    $question_amount = $row001001['TOTAL_QUESTION'];

                                      
                                        echo '<button type="button" data-toggle="modal" data-target="#check_answer'.$mcq_id.'" class="btn btn-link  btn-md btn-rounded" style="box-shadow:0px 0px 4px 3px #cccc;margin-right:10px;margin-left:10px;float:right;"><span class="fa fa-check-circle"></span>&nbsp; Answers</button>

                                                <div id="check_answer'.$mcq_id.'" class="modal fade" role="dialog">
                                              <div class="modal-dialog">

                                                <!-- Modal content-->
                                                <div class="modal-content modal-lg">
                                                  <div class="modal-header">
                                                    
                                                    <h4 class="modal-title">Check Answer</h4>
                                                    
                                                  </div>
                                                  <div class="modal-body">

                                                    
                                                  <div class="col-md-12" style="height: 300px;overflow: auto;margin-top:20px;">
                                                    
                                                    <div class="row" style="font-size:18px;">
                                                      <div class="col-md-3" style="text-align: left;border:1px solid #cccc;">Question No</div>
                                                      <div class="col-md-3" style="text-align: left;border:1px solid #cccc;">Student Answer</div>
                                                      <div class="col-md-3" style="text-align: left;border:1px solid #cccc;">Correct Answer</div>
                                                      <div class="col-md-3" style="text-align: left;border:1px solid #cccc;">Correction</div>
                                                    </div>
                                                    ';

                                                  

                                                  for($i=1;$i<=$question_amount;$i++)
                                                  {
                                                        $sql00111 = mysqli_query($conn,"SELECT * FROM `student_mcq_transaction` WHERE `PAPER_ID` = '$paper_id' AND `STU_ID` = '$student_id001' AND `QUESTION` = '$i'"); //Get Correct Answer
                                                        while($row0011 = mysqli_fetch_assoc($sql00111))
                                                        {
                                                            $correct_answer_no = $row0011['CORRECT_ANSWER']; //System Correct Answer
                                                            $marks_answer = $row0011['MARKS']; //Mark
                                                            $student_answer = $row0011['ANSWER']; //Student Answer

                                                            if($correct_answer_no == $student_answer)
                                                            {
                                                              $icon = '<span class="fa fa-check text-success" style="font-size: 20px;"></span>';
                                                            }else
                                                            if($correct_answer_no !== $student_answer && $student_answer !== '0')
                                                            {
                                                              $icon = '<span class="fa fa-times text-danger" style="font-size: 20px;"></span>';
                                                            }else
                                                            if($student_answer == '0')
                                                            {
                                                              $student_answer = 'N/A';
                                                              $icon = '<span class="fa fa-question-circle text-warning" style="font-size: 20px;color:#ff9f43;"></span>';
                                                            }

                                                            if($i<10)
                                                            {
                                                              $i = '0'.$i;
                                                            }
                                                          echo '

                                                                <div class="row">
                                                                  <div class="col-md-3" style="text-align: left;border:1px solid #cccc;">Question '.$i.'</div>
                                                                  <div class="col-md-3" style="text-align: left;border:1px solid #cccc;">'.$student_answer.'</div>
                                                                  <div class="col-md-3" style="text-align: left;border:1px solid #cccc;">'.$correct_answer_no.'</div>
                                                                  <div class="col-md-3" style="text-align: left;border:1px solid #cccc;">'.$icon.'</div>
                                                                </div>

                                                                ';



                                                        }

                                                  }
                                                }
                                                echo '</td>';
                                              }



                                      
                              
                          }
 ?>
    
    
  </tbody>
  </table>
</div>


</div>

</div>
</div>


<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

<?php  include('footer/footer.php'); ?>

