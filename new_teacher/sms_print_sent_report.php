<?php 

  include('../connect/connect.php');

  $start = $_POST['start_date'];
  $end = $_POST['end_date'];
  $type = $_POST['sms_type'];
 ?>
<!DOCTYPE html>
<html>
<head>
	<style type="text/css" media="print,screen">
.hideMe{
display:block;
}

.PrintClass {
display:block;

}
.NoPrintClass{
display:block;
}
table {
   width: 100%;
   border-collapse: collapse;
   table-layout: fixed;
}

.cell-breakWord {
   word-wrap: break-word;
}

</style>
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script type="text/javascript"
    src="http://jqueryjs.googlecode.com/files/jquery-1.3.1.min.js"> </script>
    <link href="https://fonts.googleapis.com/css?family=PT+Sans+Narrow&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">

<script type="text/javascript">


function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     document.body.innerHTML = originalContents;

    window.print();
    window.onafterprint = function() {
    window.close();
    };
     

}

   </script>

</head>
<body onload="printDiv('printableArea')">



<div class="col-md-12" id="printableArea" style="display:block;">
  
  <?php 

		  
      $sql0027 = mysqli_query($conn,"SELECT * FROM institute WHERE INS_ID = '1'");
      while($row0027 = mysqli_fetch_assoc($sql0027))
      {
          $ins_name = $row0027['INS_NAME'];
          $ins_tp = $row0027['INS_TP'];
          $ins_mobile = $row0027['INS_MOBILE'];
          $ins_pic = $row0027['PICTURE'];

          if($ins_tp == '0' && $ins_mobile == '0')
          {
            $institute_tp = "";
          }else
          {
            if($ins_tp == '0')
            {
              if($ins_mobile != '0')
              {
                $institute_tp = $ins_mobile;
              }
            }

            if($ins_mobile == '0')
            {
              if($ins_tp != '0')
              {
                $institute_tp = $ins_tp;
              }
            }

            if($ins_tp !== '0' && $ins_mobile !== '0')
            {
              
              
                $institute_tp = $ins_tp."/".$ins_mobile;
              
            }


          }

          
          
          $ins_address = $row0027['INS_ADDRESS'];
      }
	




   ?>

<div class="row">
    
    <div class="col-md-4"></div>
    <div class="col-md-4">
      <?php if($ins_pic !== '0'){ ?>
      <center><img src="../admin/images/institute/<?php echo $ins_pic; ?>" height="100px" width="100px" style="border-radius:80px;"></center>
    <?php } ?>

      <h1 style="text-align: center;"><?php echo $ins_name; ?></h1>
      <h5 style="text-align: center;"><?php echo $ins_address; ?> <br> <?php echo $institute_tp; ?></h5>

    </div>
    <div class="col-md-4"></div>

  </div>


  <div class="col-md-12" style="border-top: 1px solid #cccc;"></div>
  <div class="row">
    <div class="col-md-4">

      <div class="col-md-12" style="padding-top: 10px;">
        <label style="font-size: 12px;">Start Date - <?php echo $start; ?></label> <br>
        <label style="font-size: 12px;">End Date - <?php echo $end; ?></label> <br>
        <label style="font-size: 12px;">SMS Type - <?php if($type == '1'){echo "Single SMS";}else if($type == '2'){echo "Multi SMS";}  ?></label> <br>
      </div>

  </div>
    <div class="col-md-4 text-center" style="padding: 20px 0 20px 0;"><h1>Sent SMS Report</h1></div>
    <div class="col-md-4"></div>
  </div>
  
  <div class="col-md-12" style="border-top: 1px solid #cccc;">

                            <div class="table-responsive" style="margin-top: 4px;font-size: 14px;">
                            <table class="table demo-table-search table-responsive-block text-left" id="tableWithSearch2">
                            <thead>
                            <?php 
  $today = date('Y-m-d');
  if(isset($_POST['sms_type']))
  {
    $type = $_POST['sms_type'];

    $start = $_POST['start_date'];
    $end = $_POST['end_date'];


    if($type == '1')
    {//Single Type

      echo '<tr>
            <th style="width: 1%;">Sent Time</th>
            <th style="width: 1%;">Sent Date</th>
            <th style="width: 1%;">Student Name</th>
            <th style="width: 1%;">Telephone No</th>
            <th style="width: 1%;">Message Body</th>
            </tr>
            </thead>
            <tbody>';


            $sql0015 = mysqli_query($conn,"SELECT * FROM `single_sms` WHERE `SEND_DATE` BETWEEN '$start' AND '$end' ORDER BY `SEND_TIME` DESC");
            $check = mysqli_num_rows($sql0015);

                if($check>0)
                {

                    while($row0015=mysqli_fetch_assoc($sql0015))
                    {
                      $sms_body = $row0015['SMS_BODY'];
                      $tp = $row0015['TP'];
                      $st_name = $row0015['STUDENT_NAME'];

                      $add_date = $row0015['SEND_DATE'];
                      $add_time = $row0015['SEND_TIME'];
                    echo '<tr>
                            <td class="v-align-middle">'.$add_time.'</td>
                            <td class="v-align-middle">'.$add_date.'</td>
                            <td class="v-align-middle">'.$st_name.'</td>
                            <td class="v-align-middle">'.$tp.'</td>
                            <td class="v-align-middle">'.$sms_body.'</td>
                          </tr>'; 
                    }
                }else
                if($check == '0')
                {
                  echo '<tr><td colspan="5" class="text-center text-danger"><span class="fa fa-warning"></span> Not Found Data</td></tr>';
                }
      //Single Type
            }else
            if($type == '2')
            {//Multi Type
                echo '<tr>
                    <th style="width: 1%;">Sent Time</th>
                    <th style="width: 1%;">Sent Date</th>
                    <th style="width: 1%;">Register ID</th>
                    <th style="width: 1%;">Student Name</th>
                    <th style="width: 1%;">Telephone No</th>
                    </tr>
                    </thead>
                    <tbody>';
                  $sql001 = mysqli_query($conn,"SELECT * FROM `master_sms` WHERE `SENT_DATE` BETWEEN '$start' AND '$end' ORDER BY `SENT_TIME` DESC");

                  $check = mysqli_num_rows($sql001);

                  if($check>0){
                  while($row001 = mysqli_fetch_assoc($sql001))
                  {
                    $add_time = $row001['SENT_TIME'];
                    $add_date = $row001['SENT_DATE'];
                    $master_id = $row001['MASTER_SMS_ID'];

                    $sql0015 = mysqli_query($conn,"SELECT * FROM `multi_sms` WHERE `MASTER_SMS_ID` = '$master_id'");
                    while($row0015=mysqli_fetch_assoc($sql0015))
                    {
                      $sms_body = $row0015['SMS_BODY'];
                      $tp = $row0015['TP'];
                      $stu_id = $row0015['STUDENT_ID'];

                      $sql0017 = mysqli_query($conn,"SELECT * FROM `stu_login` WHERE `STU_ID` = '$stu_id'");
                      while($row0017=mysqli_fetch_assoc($sql0017))
                      {
                        $reg_id = $row0017['REGISTER_ID'];
                        
                        $sql0018 = mysqli_query($conn,"SELECT * FROM `student_details` WHERE `STU_ID` = '$stu_id'");
                        while($row0018=mysqli_fetch_assoc($sql0018))
                        {
                          $st_name = $row0018['F_NAME']." ".$row0018['L_NAME'];

                        }
                      }

                      

                   

                    echo '

                      <tr>
                        <td class="v-align-middle">'.$add_time.'</td>
                        <td class="v-align-middle">'.$add_date.'</td>
                        <td class="v-align-middle">'.$reg_id.'</td>
                        <td class="v-align-middle">'.$st_name.'</td>
                        <td class="v-align-middle">'.$tp.'</td></tr>'; 
                    
                    }   
                  }
                }else
                if($check == '0')
                {
                  echo '<tr><td colspan="5" class="text-center text-danger"><span class="fa fa-warning"></span> Not Found Data</td></tr>';
                }

              //Multi Type
            }
          }
          

         ?>
                           
                          </tbody>
                        </table>
                      </div>
</div>
        
</div> 

</body>
</html>