<?php
	$page = "Alert Management";
	$folder_in = '0';

      include('header/header.php');

        $s04 = mysqli_query($conn,"SELECT * FROM admin_login WHERE ADMIN_ID = '$admin_id'");
        while($row4 = mysqli_fetch_assoc($s04))
        {
          $name = $row4['ADMIN_NAME'];
        }

 ?>

    <ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-bottom: 04%;margin-bottom: 0;">
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="Alert Management"> <a href="alert.php">Alert Management</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="Create Alert"> Create Alert</li>
    </ol>


  <div class="col-md-12">
    
    <div class="row" style="padding-top: 10px;">
      <div class="col-md-2"></div>
      <div class="col-md-8 bg-white" style="border:1px solid #cccc;height: auto;">
      
      <div class="col-md-12" style="border-bottom: 1px solid #cccc;">
        <div class="row">

            <div class="col-md-9" style="margin-bottom: 10px;"><h3><a href="alert.php" data-toggle="tooltip" data-title="Go Back"><i class="pg-icon">chevron_left</i></a> Create Alert</h3></div>


        </div>
      </div>
          

<div class="card card-borderless">
<div class="col-md-12" style="padding-top:10px;">

<div class="tab-content">
<div class="tab-pane active" id="home">
<div class="row column-seperation">



<div class="col-lg-12">
  <form action="../admin/query/insert.php" method="POST">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group form-group-default input-group">
                <div class="form-input-group">
                  <label>Title</label>
                  <input type="text" name="title" class="form-control change_inputs" required placeholder="XXXXXXXXX">
                </div>
              </div>
            </div>

          </div>

          <div class="row">
            <div class="col-md-12">
              <div class="form-group form-group-default input-group">
                <div class="form-input-group">
                  <label>Message</label>
                    <textarea name="message" placeholder="XXXXXXXXX" style="margin-bottom: 10px;height: 200px;" class="form-control change_inputs" cols="20" data-toggle="tooltip" data-title="Please don't use (') comma symbol"></textarea>
                  </div>
                </div>
            </div>

          </div>

          <div class="row" style="display: none;">
            <div class="col-md-12">
              <div class="form-group form-group-default input-group">
                <div class="form-input-group">
                  <label>Seen Times</label>
                    <input type="text" name="times" id="customer_data" class="form-control" required style="outline: none;" placeholder="Type Seen Times.." onclick="this.select()" autocomplete="off" value="1">

                  </div>
                </div>
            </div>

          </div>


          <div class="row">
            <div class="col-md-8">
              <div class="form-group form-group-default input-group">
                <div class="form-input-group">
                  <label>Audience</label>
                    <select class="form-control" name="audience" id="aud">
                      <option value="Public">Public</option>
                      <!--<option value="Private">Private</option>
                      <option value="Teacher">Teacher-Classes</option> 
                      <option value="Level">Level</option>
                      <option value="Subject">Subject</option>
                      <option value="Classes">Classes</option> -->
                    </select>
                  </div>
                </div>
            </div>








            <div class="col-md-4">
              <div class="form-group form-group-default input-group">
                <div class="form-input-group">
                  
                  <button type="submit" class="btn btn-primary btn-block" style="height: 52px;" name="save_alert"><i class="pg-icon" style="transform: rotate(320deg);">sent</i> Publish</button>
                    
                  </div>
                </div>
            </div>

          </div>
        </form>
              
</div>
</div>
</div>
</div>

<script type="text/javascript"></script>



</div>
</div>



      </div>
      <div class="col-md-2"></div>
    </div>


  </div>

  <!-- Teacher -->

     <script type="text/javascript">
  
                $(document).ready(function(){  
                 $('#teacher_name').change(function(){

                   var teacher = $(this).val();

                   $.ajax({
                    url:'../admin/query/check.php',
                    method:"POST",
                    data:{check_teacher:teacher},
                    success:function(data)
                    {
                      //alert(data)
                      $('#subject44').html(data);
                        //$('#select_clz').html('<option value="">Select Class Name</option>');

                      
                    }
                   })
                 

                });

                 $('#subject44').change(function(){

                   var subject = $(this).val();
                   var teacher = $('#teacher_name').val();

                   $.ajax({
                    url:'../admin/query/check.php',
                    method:"POST",
                    data:{check_teacher_clz:subject,teacher:teacher},
                    success:function(data)
                    {
                      //alert(data)
                      $('#classes44').html(data);
                        //$('#select_clz').html('<option value="">Select Class Name</option>');

                      
                    }
                   })
                 

                });


                 $('#level3').change(function(){

                   var level_clz = $(this).val();

                   $.ajax({
                    url:'../admin/query/check.php',
                    method:"POST",
                    data:{check_level2:level_clz},
                    success:function(data)
                    {
                        $('#subject3').html(data);
                        //$('#select_clz').html('<option value="">Select Class Name</option>');

                      
                    }
                   })
                 

                });

                 $('#subject3').click(function(){

                   var sub_id = $(this).val();
                   $.ajax({
                    url:'../admin/query/check.php',
                    method:"POST",
                    data:{classes3:sub_id},
                    success:function(data)
                    {
                      //alert(data)
                      $('#classes3').html(data);
                      
                    }
                   })
                 

                });

                 
               });

              </script>

 <!-- Teacher -->



            <script type="text/javascript">
  
                $(document).ready(function(){  
                 $('#level2').change(function(){

                   var level_clz = $(this).val();

                   $.ajax({
                    url:'../admin/query/check.php',
                    method:"POST",
                    data:{check_level:level_clz},
                    success:function(data)
                    {
                        $('#subject2').html(data);
                        //$('#select_clz').html('<option value="">Select Class Name</option>');

                      
                    }
                   })
                 

                });


                 $('#level3').change(function(){

                   var level_clz = $(this).val();

                   $.ajax({
                    url:'../admin/query/check.php',
                    method:"POST",
                    data:{check_level2:level_clz},
                    success:function(data)
                    {
                        $('#subject3').html(data);
                        //$('#select_clz').html('<option value="">Select Class Name</option>');

                      
                    }
                   })
                 

                });

                 $('#subject3').click(function(){

                   var sub_id = $(this).val();
                   $.ajax({
                    url:'../admin/query/check.php',
                    method:"POST",
                    data:{classes3:sub_id},
                    success:function(data)
                    {
                      //alert(data)
                      $('#classes3').html(data);
                      
                    }
                   })
                 

                });

                 
               });

              </script>



<script type="text/javascript">
  $('.save_btn').click(function(){
    
    var upload_btn0 = $('#selectedFile0').val();
    var upload_btn = $('#sel_file').val();
      
    if(empty(upload_btn0))
    {
      alert('Please select your audio file.');
    }else
    if(empty(upload_btn))
    {
      alert('Please select your document file.');
    }
   });
</script>

<script type="text/javascript">
  $('.navi').click(function(){
    $('.frm')[0].reset();
      
   });
</script>

<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
<!-- search data -->

<!-- no data message/no found data show in search-->

<script type="text/javascript">
  $(document).ready(function () {
 
    (function ($) {

        $('#myInput').keyup(function () {
            var rex = new RegExp($(this).val(), 'i');
            $('#myTable tr').hide();
            $('#myTable tr').filter(function () {
                return rex.test($(this).text());
            }).show();
            $('.no-data').hide();
            if($('#myTable tr:visible').length == 0)
            {
                $('.no-data').show();
            }

        })

    }(jQuery));

});
</script>




<?php include('footer/footer.php'); ?>