
<?php 

  session_start();
    include('../../connect/connect.php');

    if(isset($_POST['change_password']))
    {

      $new_pass = md5($_POST['new_password']);
      $student_id = $_POST['change_password'];

      $sql = mysqli_query($conn,"UPDATE `stu_login` SET `PASSWORD` = '$new_pass' WHERE `STU_ID` = '$student_id'");

      echo "OK";

    }else
    if(isset($_POST['again_update_payment']))
    {
        $month = $_POST['again_month'];
        $tr_id = $_POST['again_update_payment'];
        $fees = $_POST['again_fees'];

        $class_id = $_POST['again_class_id'];
        $recent_file = $_POST['again_recent_file'];


        $sub_id = $_POST['again_sub_id'];
        $level_id = $_POST['again_level_id'];
        $stu_id = $_POST['again_stu_id'];
        
        
        $target_directory = "../../admin/images/reciption/";
        $target_file = $target_directory.basename($_FILES["file"]["name"]);   //name is to get the file name of uploaded file
        $filetype = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
        $rand = hexdec(uniqid()); //NAME CONVERT TO ENCRYPT
        $post_file = $rand.".".$filetype; //ADD IMAGE NAME TO ENCRYPTED NAME
        $newfilename = $target_directory.$post_file;

        $year = date('Y');

        $now_time = date('Y-m-d h:i:s A');

       $update003 = mysqli_query($conn,"UPDATE `payment_data` SET `FEES`= '$fees',`YEAR`='$year',`MONTH`='$month',`PAY_TIME`='$now_time',`ADMIN_SUBMIT`= '0',`FILE_NAME`='$post_file',`LAST_UPLOAD_FILE` = '$recent_file',`STU_ID` = '$stu_id' WHERE `TRA_ID`= '$tr_id' AND `CLASS_ID`= '$class_id'");
        

        //move_uploaded_file($_FILES["file"]["tmp_name"],$newfilename);   // tmp_name is the file temprory stored in the server

        //Now to check if uploaded or not

        if(move_uploaded_file($_FILES["file"]["tmp_name"],$newfilename)) echo 1;
        else echo 0;


    }else
    if(isset($_POST['update_class']))
    {

        $class_id = $_POST['update_class'];
        $class = ucwords($_POST['class']);
        $fees = $_POST['fees'];
        $link_id = $_POST['link_id'];

        $sql = mysqli_query($conn,"UPDATE `classes` SET `CLASS_NAME`='$class',`FEES`='$fees',`ZOOM_LINK`='$link_id' WHERE `CLASS_ID` = '$class_id'");

          ?>  

          <!DOCTYPE html>
          <html>
          <head>
              <title>Update Successfully</title>
              <link href="https://fonts.googleapis.com/css2?family=Ubuntu&display=swap" rel="stylesheet">
              <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
          </head>
          <body style="font-family: 'Ubuntu', sans-serif;">
                  <script type="text/javascript">

                      Swal.fire({
                            position: 'top-middle',
                            type: 'success',
                            title: 'Update Successfully',
                            showConfirmButton: false,
                            timer: 1500
                            })

                  </script>
          </body>
          </html>

          <?php

          header('refresh:2;url=../profile/create_class.php');
    }else
    if(isset($_POST['update_student_data']))
    {
      $stu_id = $_POST['update_student_data'];
      $dob = $_POST['student_dob'];
      $gen = $_POST['student_gender'];
      $email = $_POST['student_email'];
      $tp = $_POST['student_tp'];
      $address = $_POST['student_address'];

      $update001 = mysqli_query($conn,"UPDATE `student_details` SET `DOB`='$dob',`ADDRESS`='$address',`TP`='$tp',`EMAIL`='$email',`GENDER`='$gen' WHERE `STU_ID` = '$stu_id'");
      
      echo "OK";

    }else
    if(isset($_POST['upload_img']))
    {
      $stu_id = $_POST['upload_img'];

      $recent_img = $_POST['recent_img'];

      $file = $_FILES['img']['name'];

      if($recent_img !== '0')
      {
          if($recent_img !== 'girl.png' && $recent_img !== 'boy.png')
          {
            unlink('../images/profile/'.$recent_img);
          }
          
      }

      if($file !== '0')
      {
          $tmp_file = $_FILES['img']['tmp_name']; //ASSIGN DATA TO VARIABLE
          $ext = pathinfo($_FILES["img"]["name"], PATHINFO_EXTENSION); //GET EXTENTION ON IMAGE

          $base = basename($file,".".$ext);

          $sql001 = mysqli_query($conn,"SELECT * FROM student_details WHERE PICTURE LIKE '%".$base."%'");
          $check = mysqli_num_rows($sql001);

          if($check > 0)
          {
              $p1 = basename($file,".".$ext);
              $post_file = $p1."(".$check.").".$ext;
          }else
          if($check == '0')
          {
              $post_file = $file;
          }

          move_uploaded_file($tmp_file,'../images/profile/'.$post_file); //SAVE LOCATION
          
          $update001 = mysqli_query($conn,"UPDATE `student_details` SET `PICTURE`='$post_file' WHERE `STU_ID` = '$stu_id'");

      }else
      {
          $update001 = mysqli_query($conn,"UPDATE `student_details` SET `PICTURE`='0' WHERE `STU_ID` = '$stu_id'");

      }

      if($update001)
      {?> <!DOCTYPE html>
      <html>
      <head>
        <title>Update Successfully</title>
        <link href="https://fonts.googleapis.com/css2?family=Ubuntu&display=swap" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
      </head>
      <body style="font-family: 'Ubuntu', sans-serif;">
          <script type="text/javascript">


                    Swal.fire({
                            position: 'top-middle',
                            type: 'success',
                            title: 'Update Successfully',
                            showConfirmButton: false,
                            timer: 900
                            })

    
          </script>
      </body>
      </html>
        
        
      <?php
          
    header('refresh:1.2;url=../../index/profile_setting.php');
    }
  }else
    if(isset($_GET['remove_img']))
    {

        $stu_id = $_GET['remove_img'];
        $recent_img = $_GET['recent_img'];

        $sql = mysqli_query($conn,"UPDATE `student_details` SET `PICTURE`='0' WHERE `STU_ID` = '$stu_id'");

        unlink('../images/profile/'.$recent_img);


          ?>  

          <!DOCTYPE html>
          <html>
          <head>
              <title>Remove Successfully</title>
              <link href="https://fonts.googleapis.com/css2?family=Ubuntu&display=swap" rel="stylesheet">
              <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
          </head>
          <body style="font-family: 'Ubuntu', sans-serif;">
                  <script type="text/javascript">

                      Swal.fire({
                            position: 'top-middle',
                            type: 'success',
                            title: 'Remove Successfully',
                            showConfirmButton: false,
                            timer: 1500
                            })

                  </script>
          </body>
          </html>

          <?php

          header('refresh:2;url=../../index/profile_setting.php');
      }else
    if(isset($_POST['update_payment']))
    {

        $payment_id = $_POST['payment_id'];
        $last_image = $_POST['last_image'];
        
        $file = $_FILES['upload_file']['name'];

        $tmp_file = $_FILES['upload_file']['tmp_name']; //ASSIGN DATA TO VARIABLE
        $ext = pathinfo($_FILES["upload_file"]["name"], PATHINFO_EXTENSION); //GET EXTENTION ON IMAGE
        $rand = hexdec(uniqid()); //NAME CONVERT TO ENCRYPT
        $post_file = $rand.".".$ext; //ADD IMAGE NAME TO ENCRYPTED NAME
        move_uploaded_file($tmp_file,"../../admin/images/reciption/".$post_file); //SAVE LOCATION

        $sql001 = mysqli_query($conn,"SELECT * FROM `payment_data` WHERE `PAY_ID` = '$payment_id'");
        while($row001 = mysqli_fetch_assoc($sql001))
        {
          $second_upload = $row001['LAST_UPLOAD_FILE'];

          if($second_upload !== '0')
          {
            unlink('../../admin/images/reciption/'.$second_upload.'');
          }
        }


        $year = date('Y');

        $now_time = date('Y-m-d h:i:s A');

       $update003 = mysqli_query($conn,"UPDATE `payment_data` SET `PAY_TIME`='$now_time',`ADMIN_SUBMIT`= '0',`FILE_NAME`='$post_file',`LAST_UPLOAD_FILE` = '$last_image' WHERE `PAY_ID`= '$payment_id'");

        ?>  <!DOCTYPE html>
        <html>
        <head>
            <title>Sending</title>
            <link href="https://fonts.googleapis.com/css2?family=Ubuntu&display=swap" rel="stylesheet">
            <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
        </head>
        <body style="font-family: 'Ubuntu', sans-serif;">
                <script type="text/javascript">


                    Swal.fire({
                          position: 'top-middle',
                          type: 'success',
                          title: 'Sending successful',
                          icon: 'success',
                          showConfirmButton: false,
                          timer: 1500
                          })

    
                </script>
        </body>
        </html>
            
            
        <?php
            header('refresh:0.8;url=../../index/index.php');

    }else
    if(isset($_GET['change_color']))
    {
        $color = $_GET['change_color'];
        $stu_id = $_GET['stu_id'];

        $sql001 = mysqli_query($conn,"SELECT * FROM `stu_login` WHERE `STATUS` = 'Active'");
        while($row001 = mysqli_fetch_assoc($sql001))
        {
          $student_id = $row001['STU_ID'];

          $sql002 = mysqli_query($conn,"SELECT * FROM `profile_settings` WHERE `STU_ID` = '$student_id'");

          if(mysqli_num_rows($sql002) == '0')
          {
              mysqli_query($conn,"INSERT INTO `profile_settings`(`TOUR_ENABLE`, `WELCOME_MESSAGE`, `COLOR`, `DARK_MODE`, `DM_START_TIME`, `DM_END_TIME`, `STU_ID`) VALUES ('0','0','0','0','0','0', '$student_id')");
          }

        }

        mysqli_query($conn,"UPDATE `profile_settings` SET `COLOR`= '$color' WHERE `STU_ID` = '$stu_id'");

        header('location:../../index/dashboard.php');

    }else
    if(isset($_GET['dark_mode_off']))
    {
        $stu_id = $_GET['dark_mode_off']."";

        mysqli_query($conn,"UPDATE `profile_settings` SET `DARK_MODE`= '0' WHERE `STU_ID` = '$stu_id'");

        header('location:../../index/dashboard.php');

    }else
    if(isset($_GET['dark_mode_on']))
    {
        $stu_id = $_GET['dark_mode_on'];

        mysqli_query($conn,"UPDATE `profile_settings` SET `DARK_MODE`= '1' WHERE `STU_ID` = '$stu_id'");

        header('location:../../index/dashboard.php');

    }else
    if(isset($_POST['reg_grade']))
    {
        $stu_id = $_POST['student_id'];
        $grade_id = $_POST['reg_grade'];

        mysqli_query($conn,"UPDATE `stu_login` SET `GRADE_ID`= '$grade_id' WHERE `STU_ID` = '$stu_id'");


    }else
    if(isset($_POST['change_payment_tab']))
    {
        $stu_id = $_POST['change_payment_tab'];
        $type_value = $_POST['type_value'];

        mysqli_query($conn,"UPDATE `student_details` SET `PAYMENT_TAB`= '$type_value' WHERE `STU_ID` = '$stu_id'");


    }else
    if(isset($_POST['start_exam']))
    {
        $question_no = $_POST['start_exam'];
        $answer_no = $_POST['answer_no'];
        $student_id = $_POST['student_id'];
        $class_id = $_POST['class_id'];
        $paper_id = $_POST['paper_id'];
        $question_amount = $_POST['question_amount'];
        $finish_date = $_POST['finish_d_t'];

        $mcq_main_id = $_POST['mcq_main_id'];

        $today = strtotime(date('Y-m-d h:i:s A'));

        if($today <= $finish_date)
        {

              $sql001 = mysqli_query($conn,"SELECT * FROM `student_mcq_transaction` WHERE `PAPER_ID` = '$paper_id' AND `STU_ID` = '$student_id' AND `STU_MAIN_ID` = '$mcq_main_id'"); //check already available data
              $check001 = mysqli_num_rows($sql001); //Currently available given answer to this paper

              if($check001 == '0')
              {
                for($i=1;$i<=$question_amount;$i++)
                {
                   $sql002 = mysqli_query($conn,"SELECT * FROM `mcq_trans` WHERE `PAPER_ID` = '$paper_id' AND `QUESTION` = '$i'"); //Get Correct Answer
                   while($row002 = mysqli_fetch_assoc($sql002))
                   {
                      $correct_answer_no = $row002['CORRECT_ANSWER']; //Correct Answer
                      $marks_answer = $row002['MARKS_ANSWER'];
                   }

                   $now = date('Y-m-d h:i:s A');

                  mysqli_query($conn,"INSERT INTO `student_mcq_transaction`(`ANSWER_TIME`, `ANSWER`, `MARKS`, `STU_ID`, `PAPER_ID`, `QUESTION`,`CORRECT_ANSWER`,`STU_MAIN_ID`) VALUES ('$now','0','$marks_answer','$student_id','$paper_id','$i','$correct_answer_no','$mcq_main_id')"); //create all answer, all given answers are zero

                  if($question_no == $i)
                  {
                    mysqli_query($conn,"UPDATE `student_mcq_transaction` SET `ANSWER_TIME`= '$now',`ANSWER`= '$answer_no' WHERE `PAPER_ID` = '$paper_id' AND `QUESTION` = '$question_no' AND `STU_ID` = '$student_id' AND `STU_MAIN_ID` = '$mcq_main_id'"); //Get Correct Answer
                  }

                  

                }
              }else
              if($check001 > 0)
              {
                $now = date('Y-m-d h:i:s A');
                mysqli_query($conn,"UPDATE `student_mcq_transaction` SET `ANSWER_TIME`= '$now',`ANSWER`= '$answer_no' WHERE `PAPER_ID` = '$paper_id' AND `QUESTION` = '$question_no' AND `STU_ID` = '$student_id' AND `STU_MAIN_ID` = '$mcq_main_id'"); //Get Correct Answer
              }

              echo "1";
          }else
          if($today > $finish_date)
          {
              echo "0";
          }

        //mysqli_query($conn,"UPDATE `student_details` SET `PAYMENT_TAB`= '$type_value' WHERE `STU_ID` = '$stu_id'");


    }else
    if(isset($_POST['finish_exam']))
    {
      $paper_id = $_POST['finish_exam'];
      $student_id = $_POST['student_id'];
      $question_amount = $_POST['question_amount'];

      $correct = 0;
      $correct_mark = 0;

      $wrong = 0;

      $invalid = 0;

      $sql003 = mysqli_query($conn,"SELECT * FROM `paper_master` WHERE `PAPER_ID` = '$paper_id'"); //Get Correct Answer
       while($row003 = mysqli_fetch_assoc($sql003))
       {
          $start_date = $row003['START_DATE']; //Correct Answer
          $start_time = $row003['START_TIME'];

          $paper_start_date = $start_date;

          $marks_answer = $row003['ANSWER_MARKS']; //Mark

          $class_id = $row003['CLASS_ID']; //Class ID
          $exam_key = $row003['EXAM_KEY']; //Exam Key
          
          $exam_mcq_name = $row003['PAPER_NAME'];
          $teacher_id = $row003['TEACH_ID'];

          
          $paper_total_question = $row003['NO_OF_QUESTIONS'];
          $total_marks_answer = $row003['ANSWER_MARKS'];

          $strto_finish_time = strtotime($row003['FINISH_D_T']);

          $start_date_time = $start_date." ".$start_time;
       }

       $sql008 = mysqli_query($conn,"SELECT * FROM `student_mcq_main` WHERE `PAPER_ID` = '$paper_id' AND `STU_ID` = '$student_id'"); //Get Correct Answer
       while($row008 = mysqli_fetch_assoc($sql008))
       {
          $exam_started = $row008['START_TIME']; //Attend Time
       }



      $strto_today = strtotime(date('Y-m-d h:i:s A'));

      $start_date = new DateTime();
      $since_start = $start_date->diff(new DateTime($exam_started));
      $spent_hour = $since_start->h;
      $spent_minute = $since_start->i;
/*
      $previousDate = '2021-02-10 13:28:00';
      $startdate = new DateTime($previousDate);
      $endDate   = new DateTime('now');
      $interval  = $endDate->diff($startdate);
      echo$interval->format(' %h Hours  %i Minute');*/




      for($i=1;$i<=$question_amount;$i++)
      {
          $sql001 = mysqli_query($conn,"SELECT * FROM `student_mcq_transaction` WHERE `PAPER_ID` = '$paper_id' AND `STU_ID` = '$student_id' AND `QUESTION` = '$i'"); //Get Correct Answer
          while($row001 = mysqli_fetch_assoc($sql001))
          {
              $correct_answer_no = $row001['CORRECT_ANSWER']; //System Correct Answer
              $marks_answer = $row001['MARKS']; //Mark
              $student_answer = $row001['ANSWER']; //Student Answer

              if($correct_answer_no == $student_answer)
              {
                //Correct  Answer Amount
                $correct = $correct+1;
                $correct_mark = $correct_mark+$marks_answer;
              }

              if($correct_answer_no !== $student_answer && $student_answer !== '0')
              {
                //Wrong  Answer Amount

                $wrong = $wrong+1;

              }

              if($student_answer == '0')
              {
                //Invalid  Answer Amount

                $invalid = $invalid+1;
                
              }

          }

      }
/*
      if($strto_today>$strto_finish_time)
       {
        //Check Rank

         $sql004 = mysqli_query($conn,"SELECT COUNT(STU_ID) as 'count_higher_score' FROM `student_mcq_master` WHERE `PAPER_ID` = '$paper_id' AND `TOTAL_MARKS` >= '$correct_mark'"); //Get Correct Answer
         while($row004 = mysqli_fetch_assoc($sql004))
         {
            $rank = $row004['count_higher_score']; //Correct Answer
         }

       }*/



      //Write Insert query
      $today = date('Y-m-d');
      $now = date('Y-m-d h:i:s A');

      $sql005 = mysqli_query($conn,"SELECT * FROM `student_mcq_master` WHERE `PAPER_ID` = '$paper_id' AND `STU_ID` = '$student_id'"); //Check Submited data

      $check002 = mysqli_num_rows($sql005);

      if($check002 == '0')
      {

        $sql007 = mysqli_query($conn,"SELECT * FROM `student_mcq_main` WHERE `PAPER_ID` = '$paper_id' AND `STU_ID` = '$student_id'"); //Get Correct Answer
         while($row007 = mysqli_fetch_assoc($sql007))
         {
            $attend_time = $row007['ATTEND_TIME']; //Attend Time
         }

        $insert001 = mysqli_query($conn,"INSERT INTO `student_mcq_master`(`SUBMIT_DATE`, `SUBMIT_DATE_TIME`, `CORRECT_ANSWERS`, `WRONG_ANSWER`, `TOTAL_QUESTION`, `TOTAL_MARKS`, `SPEND_TIME_HOUR`, `SPEND_TIME_MIN`, `PAPER_ID`, `STU_ID`, `QUESTION_MARK`, `ATTEND_TIME`) VALUES ('$today','$now','$correct','$wrong','$question_amount','$correct_mark','$spent_hour','$spent_minute','$paper_id','$student_id','$marks_answer','$attend_time')");
      }

      $sql008 = mysqli_query($conn,"SELECT * FROM `exam_result` WHERE `STU_ID` = '$student_id' AND `CLASS_ID` = '$class_id' AND `EXAM_KEY` = '$exam_key'"); //check result
     $check003 = mysqli_num_rows($sql008);

      if($check003 > 0)
      {
        $sum = 0;
        while($row005 = mysqli_fetch_assoc($sql008))
        {
          $total_my_mark = $row005['TOTAL_MARK'];
        }
        $sum = $total_my_mark+$correct_mark; //MCQ + SE Marks


        $update = mysqli_query($conn,"UPDATE `exam_result` SET `MCQ_PAPER_NAME` = '$exam_mcq_name',`MCQ_MARK` = '$correct_mark',`TOTAL_MARK` = '$sum',`START_DATE` = '$paper_start_date' WHERE `CLASS_ID` = '$class_id' AND `STU_ID` = '$student_id' AND `EXAM_KEY` = '$exam_key'"); //Update result
      }else
      if($check003 == '0')
      {
        $insert = mysqli_query($conn,"INSERT INTO `exam_result`(`ADD_DATE`, `ADD_TIME`, `START_DATE`, `MCQ_MARK`, `SE_MARK`, `TOTAL_MARK`, `EXAM_KEY`, `STU_ID`, `CLASS_ID`, `PAPER_ID`, `MCQ_PAPER_NAME`, `SE_PAPER_NAME`) VALUES ('$today','$now','$paper_start_date','$correct_mark','0','$correct_mark','$exam_key','$student_id','$class_id','$paper_id','$exam_mcq_name','0')"); //Insert result
      }

      $sql006 = mysqli_query($conn,"SELECT * FROM `student_mcq_transaction` WHERE `PAPER_ID` = '$paper_id' AND `STU_ID` = '$student_id'"); //Get deleted given answers

     if(mysqli_num_rows($sql006)>0)
     {
        $check_delete_answer = 'show';
     }else
     if(mysqli_num_rows($sql006) == '0')
     {
        $check_delete_answer = 'none';
     }
      


      //Write Insert query

      /* Result Sheet*/

      /*
    


                      <div class="row" style="padding-bottom: 0px;">
                        <div class="col-md-8"><h3 style="font-weight: 550;color:#7f8c8d">Your Rank</h3></div>
                        <div class="col-md-4"><h3 style="text-shadow:  1px 1px #cccc;">'.$rank.'</h3></div>
                      </div>
      */

      /*Get Data From Master Table*/
      $correct = 0;
      $correct_mark = 0;
      $wrong = 0;
      $invalid = 0;
    
      $my_total_marks = 0;
      $my_final_total_marks = 0;
      $final_mark = 0;

      $sql001 = mysqli_query($conn,"SELECT * FROM `student_mcq_master` WHERE `PAPER_ID` = '$paper_id' AND `STU_ID` = '$student_id'"); //Get Correct Answer
      while($row001 = mysqli_fetch_assoc($sql001))
      {
          $correct = $row001['CORRECT_ANSWERS']; //System Correct Answer
          $wrong = $row001['WRONG_ANSWER']; //Mark
          $question_amount = $row001['TOTAL_QUESTION']; //Student Answer
          $correct_mark = $row001['TOTAL_MARKS']; //Student Answer
          $spent_hour = $row001['SPEND_TIME_HOUR']; //Student Answer
          $spent_minute = $row001['SPEND_TIME_MIN']; //Student Answer
          $marks_answer = $row001['QUESTION_MARK']; //Student Answer
          $mcq_master_id = $row001['STU_MCQ_MASTER_ID'];

          $invalid = $question_amount-($correct+$wrong);

          $my_total_marks = 0;
          $my_final_total_marks = 0;
          $final_mark = 0;

          $sql0033 = mysqli_query($conn,"SELECT * FROM `student_mcq_transaction` WHERE `PAPER_ID` = '$paper_id' AND `STU_ID` = '$student_id'"); //check mcq test table
          while($row0033 = mysqli_fetch_assoc($sql0033))
          {
          
            $correct_answer_no = $row0033['CORRECT_ANSWER']; //System Correct Answer
            $marks_answer = $row0033['MARKS']; //Mark
            $student_answer = $row0033['ANSWER']; //Student Answer
            $question = $row0033['QUESTION']; //Question

            $total_paper_mark = 0; //total question X Question for Mark
            
            if($correct_answer_no == $student_answer)
            {
            $my_total_marks = $my_total_marks+1;
            }

          }

      }

      $total_paper_mark = $paper_total_question*$total_marks_answer;
                              	
      $mcq_my_mark = $my_total_marks*$total_marks_answer; //Correct answer amount x per question for marks
      
      $final_mark = (50*$mcq_my_mark)/$total_paper_mark;


      /*Get Data from Master Table*/ 

      $sql007 = mysqli_query($conn,"SELECT * FROM `student_mcq_main` WHERE `PAPER_ID` = '$paper_id' AND `STU_ID` = '$student_id'");
      while($row007 = mysqli_fetch_assoc($sql007))
      {
        $last_attend = $row007['ATTEND_TIME'];
      }
                   

      echo '<div class="row">
                    <div class="col-md-4"><a href="../index.php" style="color: gray;text-decoration: none;">Back</a></div>
                    <div class="col-md-4">
                      
                      <div class="i_title">
                        <div class="icon" style="padding-top:20px;padding-bottom:20px;width: 120px;height: 120px;">
                          <label style="font-size: 50px;color: white;text-align:center;">'.$final_mark.' <p style="padding: 0;line-height: 0;">Marks</p></label> </div>
                      </div>

                    </div>
                    <div class="col-md-4"></div>
                  </div>

                  <div class="row">
                    <div class="col-md-2">


                    </div>
                    <div class="col-md-8">

                      <div class="row" style="padding-bottom: 0px;">
                        <div class="col-md-8"><h3 style="font-weight: 550;color:#7f8c8d"><span class="fa fa-question-circle"></span> Total Question</h3></div>
                        <div class="col-md-4"><h3 style="text-shadow:  1px 1px #cccc;">'.$question_amount.'</h3></div>
                      </div>


                      <div class="row" style="padding-bottom: 0px;">
                        <div class="col-md-8"><h3 style="font-weight: 550;color:#7f8c8d"><span class="fa fa-check-circle"></span> Correct Answers</h3></div>
                        <div class="col-md-4"><h3 style="text-shadow:  1px 1px #cccc;">'.$correct.'</h3></div>
                      </div>


                      <div class="row" style="padding-bottom: 0px;">
                        <div class="col-md-8"><h3 style="font-weight: 550;color:#7f8c8d"><span class="fa fa-times"></span> Wrong Answers</h3></div>
                        <div class="col-md-4"><h3 style="text-shadow:  1px 1px #cccc;">'.$wrong.'</h3></div>
                      </div>


                      <div class="row" style="padding-bottom: 0px;">
                        <div class="col-md-8"><h3 style="font-weight: 550;color:#7f8c8d"><span class="fa fa-ban"></span>  Invalid Answers</h3></div>
                        <div class="col-md-4"><h3 style="text-shadow:  1px 1px #cccc;">'.$invalid.'</h3></div>
                      </div>

                      <div class="row" style="padding-bottom: 0px;">
                        <div class="col-md-8"><h3 style="font-weight: 550;color:#7f8c8d"><span class="fa fa-bell"></span> Question For Mark</h3></div>
                        <div class="col-md-4"><h3 style="text-shadow:  1px 1px #cccc;"> '.$marks_answer.'</h3></div>
                      </div>

                      <div class="row" style="padding-top: 20px;border-top: 1px solid #cccc;">

                      <div class="col-md-1"></div>

                        <div class="col-md-10" style="margin-top:10px;display:'.$check_delete_answer.';">

                          <form action="show_answer.php" method="POST">

                              <input type="hidden" name="student_id" value="'.$student_id.'">
                              <input type="hidden" name="teacher_id" value="'.$teacher_id.'">
                              <input type="hidden" name="class_id" value="'.$class_id.'">
                              <input type="hidden" name="last_attend" value="'.$last_attend.'">
                              <input type="hidden" name="paper_id" value="'.$paper_id.'">
                              <input type="hidden" name="mcq_master_id" value="'.$mcq_master_id.'">
                                          
                              <button type="submit" class="btn btn-custom btn-block" id="attend_btn" style="font-weight: bold;color:white;"><i class="fa fa-search"></i>&nbsp;Check My Answer</button>

                          </form>



                        </div>
                        <div class="col-md-1"></div>
                      </div>
                    </div>
                    <div class="col-md-2"></div>
                  </div>';

      /* Result Sheet*/

    }else
    if(isset($_POST['exam_type']))
    {
      $exam = $_POST['exam_type'];
      $student_id = $_POST['student_id'];

      mysqli_query($conn,"UPDATE `student_details` SET `EXAM_TAB` = '$exam' WHERE `STU_ID` = '$student_id'");
      
    }else
    if(isset($_POST['submit_paper']))
    {
      $student_id = $_POST['submit_paper'];
      $paper_id = $_POST['paper_id'];
      
      /*if(!empty($_FILES['upload_file']['name']))
      {
        $file = $_FILES['upload_file']['name'];
        $tmp_file = $_FILES['upload_file']['tmp_name']; //ASSIGN DATA TO VARIABLE
        $ext = pathinfo($_FILES["upload_file"]["name"], PATHINFO_EXTENSION); //GET EXTENTION ON IMAGE
        $rand = hexdec(uniqid()); //NAME CONVERT TO ENCRYPT
        $post_file = $rand.".".$ext; //ADD IMAGE NAME TO ENCRYPTED NAME
        move_uploaded_file($tmp_file,"../../admin/images/str_document/".$post_file); //SAVE LOCATION

      }else
      if(empty($_FILES['upload_file']['name']))
      {
        $post_file = $recent_file;
      }*/



      $add_date = date('Y-m-d');
      $add_time = date('Y-m-d h:i:s A');

      $q_tot = 0;
      
      $sql0012 = mysqli_query($conn,"SELECT * FROM `essay_master_question` WHERE `PAPER_ID` = '$paper_id' "); //check Classes for registered teachers details
      while($row0012 = mysqli_fetch_assoc($sql0012))
      {

          $que_total_mark = $row0012['MASTER_MARK']; //Class Name
        
          $q_tot = $q_tot+$que_total_mark;

      }


      $update001 = mysqli_query($conn,"INSERT INTO `student_essay_master`(`ADD_DATE`, `ADD_TIME`, `TOTAL_MARKS`, `PAPER_FILE`, `PAPER_ID`, `COMMENT`, `STU_ID`, `STATUS`, `TOTAL_QUESTION_MARKS`) VALUES ('$add_date','$add_time','0','0','$paper_id','0','$student_id','0','$q_tot')");

      $stu_essay_id = mysqli_insert_id($conn);

      $uploadDir = '../../paper_document/';
      $allowTypes = array('jpg','png','jpeg');

      if(!empty(array_filter($_FILES['upload_file']['name']))){
          foreach($_FILES['upload_file']['name'] as $key=>$val){
              $filename0 = basename($_FILES['upload_file']['name'][$key]);

              $ext = pathinfo($filename0, PATHINFO_EXTENSION); //GET EXTENTION ON IMAGE
              $rand = hexdec(uniqid()); //NAME CONVERT TO ENCRYPT
              $filename = $rand.".".$ext; //ADD IMAGE NAME TO ENCRYPTED NAME
                
              $targetFile = $uploadDir.$filename;
              if(move_uploaded_file($_FILES["upload_file"]["tmp_name"][$key], $targetFile)){
                  $success[] = "Uploaded $filename";
                  $insertQrySplit[] = "('$filename')";
              }
              else {
                  $errors[] = "Something went wrong- File - $filename";
              }
                  
              


              $sql = mysqli_query($conn,"INSERT INTO `paper_image`(`ADD_TIME`, `ADD_DATE`, `IMAGE_NAME`, `STU_ESSAY_ID`) VALUES ('$add_time','$add_date','$filename','$stu_essay_id')");
          }

         
      }
      else {
          $errors[] = "No File Selected";
      }



      ?>  <!DOCTYPE html>
          <html>
          <head>
              <title>Success</title>
              <meta charset="utf-8">
              <meta name="viewport" content="width=device-width, initial-scale=1">
              <link href="https://fonts.googleapis.com/css2?family=Ubuntu&display=swap" rel="stylesheet">
              <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
          </head>
          <body style="font-family: 'Ubuntu', sans-serif;">
                  <script type="text/javascript">


                      Swal.fire({
                            position: 'top-middle',
                            type: 'success',
                            icon: 'success',
                            title: 'Submit Successfully!',
                            showConfirmButton: false,
                            timer: 1000
                            })

      
                  </script>
          </body>
          </html>
              
              
          <?php

          header('refresh:0.2;url=../../index/index.php#exam');
    }else
    if(isset($_POST['seen_alert']))
    {
      $student_id = $_POST['seen_alert'];

      mysqli_query($conn,"UPDATE `student_details` SET `ALERT` = '0' WHERE `STU_ID` = '$student_id'");
    }

 ?>
