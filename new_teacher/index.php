<?php

  $page = "Today Classes";
  $folder_in = '0';



  include('header/header.php'); 
//$_SESSION['alert'] = 0;
ini_set( "display_errors", 0); 
  if($_SESSION['alert'] == '0')
  {

      $sql001 = mysqli_query($conn,"SELECT * FROM alert WHERE PUBLISH = 'Yes'");
      while($row = mysqli_fetch_assoc($sql001))
      {
        $msg = $row['MESSAGE'];
        $add_date = $row['ADDED_DATE'];
        $war = $row['TITLE'];
        $publish = $row['PUBLISH'];
        $alert_id = $row['ALERT_ID'];

        $sql002 = mysqli_query($conn,"SELECT * FROM show_students WHERE ALERT_ID = '$alert_id'");
        while($row2 = mysqli_fetch_assoc($sql002))
        {
          $student_id = $row2['STU_ID'];

          if($student_id == $stu_id)
          {?>
    <script type="text/javascript">
      $(document).ready(function(){
        $('#modal_btn').click();

        $('.close,#close').click(function(){
          <?php $_SESSION['alert'] = '1'; ?>
        });
      });
    </script>

    <button type="button" class="btn btn-info btn-lg" id="modal_btn" data-toggle="modal" data-target="#myModal" style="display: none;">Open Modal</button>

    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">News Alert</h4>
      </div>
      <div class="modal-body">

          <div class="row">
            <div  class="col-md-2"></div>
            <div  class="col-md-8" style="text-align: center;"><span class="fa fa-warning text-danger" style="font-size: 100px;"></span></div>
            <div  class="col-md-2"></div>
          </div>

        <div class="text-danger" role="alert" style="margin-top: 20px;">
          
          <h4><strong><?php echo $msg; ?></strong></h4>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger btn-lg" data-dismiss="modal" id="close"><span class="fa fa-check-circle"></span> &nbsp;Close</button>
      </div>
    </div>

    </div>
    </div>


  <?php }
        }?>
  <?php }
        }
  ?>
  <style type="text/css">

.red {
  
  display: block;
  width: 100px;
  height: 25px;
  border-radius: 10%;
  cursor: pointer;
  animation: none;
  float: left;
  bottom: 5px;
  right: 5px;
  font-weight: bold;
  padding-top: 2px;
  text-align: center;
  color: white;
  font-size: 14px;
  animation: pulse 1.6s infinite;
}


@keyframes pulse {
  0% {
    -moz-box-shadow: 0 0 0 0 #ff4b4b;
    box-shadow: 0 0 0 0 #ff4b4b;
  }
  70% {
    -moz-box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
  }
  100% {
    -moz-box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
  }
}

.green {
  
  display: block;
  width: 100px;
  height: 25px;
  border-radius: 10%;
  cursor: pointer;
  animation: none;
  float: left;
  bottom: 5px;
  right: 5px;
  font-weight: bold;
  padding-top: 2px;
  text-align: center;
  color: white;
  font-size: 14px;
  animation: pulse11 1.6s infinite;
}


@keyframes pulse11 {
  0% {
    -moz-box-shadow: 0 0 0 0 #28a745;
    box-shadow: 0 0 0 0 #28a745;
  }
  70% {
    -moz-box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
  }
  100% {
    -moz-box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
  }
}

.yellow {
  
  display: block;
  width: 100px;
  height: 25px;
  border-radius: 10%;
  cursor: pointer;
  animation: none;
  float: left;
  bottom: 5px;
  right: 5px;
  font-weight: bold;
  padding-top: 2px;
  text-align: center;
  color: white;
  font-size: 14px;
  animation: pulse44 1.6s infinite;
}


@keyframes pulse44 {
  0% {
    -moz-box-shadow: 0 0 0 0 #f1b501;
    box-shadow: 0 0 0 0 #f1b501;
  }
  70% {
    -moz-box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
  }
  100% {
    -moz-box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
  }
}

.white {
  
  display: block;
  width: 100px;
  height: 25px;
  border-radius: 10%;
  cursor: pointer;
  animation: none;
  float: left;
  bottom: 5px;
  right: 5px;
  font-weight: bold;
  padding-top: 2px;
  text-align: center;
  color: white;
  font-size: 14px;
  animation: pulse33 1.6s infinite;
}


@keyframes pulse33 {
  0% {
    -moz-box-shadow: 0 0 0 0 #ffffff;
    box-shadow: 0 0 0 0 #ffffff;
  }
  70% {
    -moz-box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
  }
  100% {
    -moz-box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
  }
}
 
.cyan {
  
  display: block;
  width: 100px;
  height: 25px;
  border-radius: 10%;
  cursor: pointer;
  animation: none;
  float: left;
  bottom: 5px;
  right: 5px;
  font-weight: bold;
  padding-top: 2px;
  text-align: center;
  color: white;
  font-size: 14px;
  animation: pulse33 1.6s infinite;
}


@keyframes pulse33 {
  0% {
    -moz-box-shadow: 0 0 0 0 #007bff;
    box-shadow: 0 0 0 0 #007bff;
  }
  70% {
    -moz-box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
  }
  100% {
    -moz-box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
  }
}
  
  </style>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<div class="row" style="border-bottom: 1px solid #cccc;">
  <div class="col-md-9">
      <ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-bottom: 04%;margin-bottom: 0;">
        <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
        <li class="breadcrumb-item active" data-toggle="tooltip" data-title="Classes"> Classes</li>
      </ol>
  </div>
  <div class="col-md-3"><!-- <h4 class="text-primary"><?php echo date('Y-m-d l'); ?></h4> --></div>
</div>
<div class="col-md-12">
  <div class="row">
    
    <div class="col-md-9"><h3 style="text-transform: capitalize;margin-bottom: 30px;">Classes </h3></div>

    <div class="col-md-3" style="float: right;"><h3><input type="text" name="" class="form-control" data-toggle="tooltip" data-title="දත්ත සෙවීම" style="border-radius: 20px;padding-left: 20px;" placeholder="Search...." id="myInput"></h3></div>


  </div>
</div>




<div class="col-md-12" id="myDIV">

  <!-- if search using no found data for display message -->
            <div class="no-data col-md-12" style="margin-top: 20px;display: none;">
               
                <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6" style="margin-top: 20px;text-align:center;">
                  <img src="images/not_result.jpg" class="image-responsive-height">
                </div>
                <div class="col-md-3"></div>
                </div>

            </div>
        <!-- if search using no found data for display message -->



<?php 
  
        
          $day = date('l');
            $select001 = mysqli_query($conn,"SELECT * FROM classes WHERE DAY = '$day'");

            $count = mysqli_num_rows($select001);

            if($count>0)
            {


          for ($i=0; $i < $count ; $i++) 
          {


            $row001 = mysqli_fetch_assoc($select001);
              $class_name = $row001['CLASS_NAME'];
                      $class_id = $row001['CLASS_ID'];

                      $teach_id = $row001['TEACH_ID'];
                      $sub_id = $row001['SUB_ID'];


                      $ssql01112 = mysqli_query($conn,"SELECT * FROM classes");
                      while($row0012212 = mysqli_fetch_assoc($ssql01112))
                      { 

                        $teach_id0 = $row0012212['TEACH_ID'];
                        $class_id0 = $row0012212['CLASS_ID'];

                        copy('choose_category.php','teachers/'.$teach_id0.'/'.$class_id0.'/choose_category.php');

                                          /*copy('uploaded_tutorials.php','teachers/'.$teach_id.'/'.$class_id.'/uploaded_tutorials.php');*/

                                          copy('images/zo_logo.png','teachers/'.$teach_id0.'/'.$class_id0.'/image/zo_logo.png');

                                          copy('images/girl.png','teachers/'.$teach_id0.'/'.$class_id0.'/image/girl.png');
                                          copy('images/boy.png','teachers/'.$teach_id.'/'.$class_id.'/image/boy.png');

                                          copy('video.php','teachers/'.$teach_id0.'/'.$class_id0.'/video/video.php');

                                          copy('audio.php','teachers/'.$teach_id0.'/'.$class_id0.'/audio/audio.php');

                                          copy('document.php','teachers/'.$teach_id0.'/'.$class_id0.'/document/document.php');


                                          if (!file_exists('teachers/'.$teach_id0.'/'.$class_id0.'/video'.'')) {
                                              mkdir('teachers/'.$teach_id0.'/'.$class_id0.'/video', 0777, true);
                                          }

                                          if (!file_exists('teachers/'.$teach_id0.'/'.$class_id0.'/image'.'')) {
                                              mkdir('teachers/'.$teach_id0.'/'.$class_id0.'/image', 0777, true);
                                          }

                                          if (!file_exists('teachers/'.$teach_id0.'/'.$class_id0.'/document'.'')) {
                                              mkdir('teachers/'.$teach_id0.'/'.$class_id0.'/document', 0777, true);
                                          }

                                          if (!file_exists('teachers/'.$teach_id0.'/'.$class_id0.'/audio'.'')) {
                                              mkdir('teachers/'.$teach_id0.'/'.$class_id0.'/audio', 0777, true);
                                          }


                                          if (!file_exists('teachers/'.$teach_id0.'/'.$class_id0.'/audio/audio'.'')) {
                                              mkdir('teachers/'.$teach_id.'/'.$class_id.'/audio/audio', 0777, true);
                                          }

                                          if (!file_exists('teachers/'.$teach_id0.'/'.$class_id0.'/video/video'.'')) {
                                              mkdir('teachers/'.$teach_id.'/'.$class_id.'/video/video', 0777, true);
                                          }

                                          if (!file_exists('teachers/'.$teach_id0.'/'.$class_id0.'/document/document'.'')) {
                                              mkdir('teachers/'.$teach_id0.'/'.$class_id0.'/document/document', 0777, true);
                                          }


                                            $myfile = fopen('teachers/'.$teach_id0.'/'.$class_id0.'/'.'class_id.txt', "w") or die("Unable to open file!");
                                            $txt = $class_id;
                                            fwrite($myfile, $txt);
                                            fclose($myfile);

                      }

                       $ssql0111 = mysqli_query($conn,"SELECT * FROM subject WHERE SUB_ID = '$sub_id'");
                while($row001221 = mysqli_fetch_assoc($ssql0111))
                { 

                  $level_id = $row001221['LEVEL_ID'];
                  $subject_name = $row001221['SUBJECT_NAME'];

                }

                      $start = $row001['START_TIME'];
                      $str = strtotime($start);
                      $start_time = date('h:i A',$str);

                      $end = $row001['END_TIME'];
                      $str2 = strtotime($end);
                      $end_time = date('h:i A',$str2);


                      $fees = $row001['FEES'];
                      $link = $row001['LINK'];

                      $ssql01 = mysqli_query($conn,"SELECT * FROM teacher_details WHERE TEACH_ID = '$teach_id'");
                while($row00122 = mysqli_fetch_assoc($ssql01))
                {
                    $t_name = $row00122['F_NAME']." ".$row00122['L_NAME'];
                    $picture = $row00122['PICTURE'];
                    $gender = $row00122['GENDER'];

                    if($picture == '0')
                    {
                      if($gender == 'Male')
                      {
                        $picture = 'male_teacher1.jpg';
                      }else
                      if($gender == 'Female')
                      {
                        $picture = 'female_teacher.png';
                      }
                    }
                }


                
            $free = "Yes";      

            if(fmod($i,$count) == '0')
            {
              //echo $i;
              echo '<div class="row">
                  <div class="col-lg-3 sm-no-padding" style="margin-top:20px;" id="cc">
                    <div class="card card-transparent">
                    <div class="card-body no-padding">
                    <div id="card-advance" class="card card-default" style="">

                    <div class="card-body" style="padding: 0;border:0px solid black;">
                    <div style="position:relative;">
                  ';

                    
                        $ssql0211 = mysqli_query($conn,"SELECT * FROM transactions WHERE STU_ID = '$stu_id' AND CLASS_ID = '$class_id'");
                        $check_register_paid = mysqli_num_rows($ssql0211);
                        while($row0012311 = mysqli_fetch_assoc($ssql0211))
                        {
                            $tra_id = $row0012311['TRA_ID'];
                            
                        }
                    

                        


                        if($free !== 'Yes')
                        {
                          

                          
                            if($check_register_paid== '0')
                            {

                              //echo "Not Register(Paid)";

                              ?>
                                  <a href="../student/query/insert.php?class_id=<?php echo $class_id; ?>&&teach_id=<?php echo $teach_id; ?>&&level_id=<?php echo $level_id; ?>&&sub_id=<?php echo $sub_id; ?>&&stu_id=<?php echo $stu_id; ?>&&class_reg=class_reg&&clz=today"><span class="red"style="background: #ff4b4b;position:absolute;bottom:10px;right:10px;" data-toggle="tooltip" data-title="ඔබට නව පන්තියක ලියාපදිංචි වීමට අවශ්‍ය නම් මෙම බොත්තම ක්ලික් කරන්න." onclick="return confirm('Are you sure Register?')" ><span class="fa fa-thumbs-up"></span> Register</span></a>
                               <?php

                            }else
                            if($check_register_paid>0)
                            {
                                  $y = date('Y');
                                  $m = date('m');

                                    $ssql04 = mysqli_query($conn,"SELECT * FROM payment_data WHERE TRA_ID = '$tra_id' AND YEAR = '$y' AND MONTH = '$m'");
                                    $payments = mysqli_num_rows($ssql04);

                                    ?>

                                    <a href="../student/query/delete.php?tr_id=<?php echo $tra_id; ?>&&level_id=<?php echo $level_id; ?>&&sub_id=<?php echo $sub_id; ?>&&un_reg=un_reg&&clz=today" class="btn btn-danger btn-rounded btn-xs  btn-active" onclick="return confirm('Are you sure Unregister?')"  style="position:absolute;top:2px;right:3px;padding-top: 5px;padding-bottom: 5px;opacity: 0.6;" data-toggle="tooltip" data-title="ඔබ දැනටමත් ලියාපදිංචි පන්තිය අවලංගු කිරීම."><span class="fa fa-times"></span></a>
                                      <?php

                                    if($payments>0)
                                    {

                                    
                                        $row00120 = mysqli_fetch_assoc($ssql04);
                                        $admin_st = $row00120['ADMIN_SUBMIT'];
                                        $tr_file = $row00120['LAST_UPLOAD_FILE'];
                                        $cu_file = $row00120['FILE_NAME'];

                                        if($admin_st == '0')
                                        {

                                          //echo "Pending";
                                          echo '<span class="yellow" style="background: #f1b501;width:120px;position:absolute;bottom:10px;right:10px;" data-toggle="tooltip" data-title="ඔබ ඇතුලත් කල දත්ත පරික්ෂා කිරීමට යොමු කොට ඇත.හැකි ඉක්මනින් අප නියෝජිතයකු ඔබ වෙත ප්‍රතිචාර දක්වයි.එතෙක් රැදී සිටින්න."><span class="fa fa-bullhorn"></span> Pending</span>';


                                        }else
                                        if($admin_st == '2')
                                        {

                                          //echo "Reject";

                                          echo '<a data-toggle="modal" data-target="#again'.$class_id.'" ><span class="white"style="background: #ffffff;width:120px;color:black;position:absolute;bottom:10px;right:10px;" data-toggle="tooltip" data-title="ඔබ ඇතුලත් කල රිසිට්පතෙහි නිවැරදිබව හෝ අපැහැදිලිබවක් ඇති බැවින් නැවතත් පරික්ෂා කොට අප වෙත යොමු කරන්න."><span class="  fa fa-frown-o"></span> Again</span></a>';
                                      ?>

                                        <div id="again<?php echo $class_id; ?>" class="modal fade" role="dialog">
                                                        <div class="modal-dialog">

                                                          <!-- Modal content-->
                                                          <div class="modal-content">
                                                          <form action="../student/query/update.php" method="POST" enctype="multipart/form-data" enctype="multipart/form-data">
                                                            <div class="modal-header">
                                                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                              <h4 class="modal-title" style="margin-bottom: 20px;"><?php echo $class_name; ?></h4>
                                                            </div>
                                                            
                                                            <div class="modal-body">
                                                              <lable style="font-size: 16px;font-weight: bold;">Select Month : </lable>
                                                              <select class="form-control" style="margin-top:10px;margin-bottom:10px;" name="month" required>
                                                                <?php  
                                                                $m=date('m');
                                                                $month = date('F', mktime(0,0,0,$m));

                                                                echo '<option value='.$m.'>'.$month.'</option>';

                                                                /*$cu_month = date('m');
                                                                for ($m=1; $m<=$cu_month; $m++) 
                                                                {
                                                                   $month = date('F', mktime(0,0,0,$m));
                                                                   echo '<option value='.$m.'>'.$month.'</option>';
                                                                 }*/
                                                                ?>

                                                              </select>

                                                              <lable style="font-size: 16px;font-weight: bold;">Month Fees : </lable>
                                                              <input type="number" class="form-control" value="<?php echo $fees; ?>" style="margin-top:10px;margin-bottom:10px;" required  onfocus="this.value=''" disabled>
                                                              <input type="hidden" name="sub_id" value="<?php echo $sub_id; ?>">
                                                              <input type="hidden" name="level_id" value="<?php echo $level_id; ?>">
                                                              <input type="hidden" name="class_id" value="<?php echo $class_id; ?>">
                                                              <input type="hidden" name="recent_file" value="<?php echo $cu_file; ?>">
                                                              <input type="hidden" name="clz" value="today_clz">
                                                              <input type="hidden" name="fees" value="<?php echo $fees; ?>">
                                                              <input type="hidden" name="stu_id" value="<?php echo $stu_id; ?>">

                                                              <lable style="font-size: 16px;font-weight: bold;">Upload Payment Receipt : </lable>
                                                              <input type="file" class="form-control" name="upload_file" required  style="margin-top:10px;margin-bottom:10px;" title="Upload Here" accept="image/*" id="up_rec">
                                                              
                                                              <div class="row show_image">
                                                                <div class="col-md-4"></div>
                                                                <div class="col-md-4"><img src="../admin/images/reciption/<?php echo $cu_file; ?>" alt="Receiption Image" class="image-responsive" style="width: 100%;"></div>
                                                                <div class="col-md-4"></div>
                                                              </div>
                                                              <div class="col-md-12 text-success show_image" style="text-align: center;margin-top: 20px;"><label style="text-align: center;"><span class="fa fa-upload"></span> Recent Uploaded Image</label></div>
                                                              
                                                            </div>
                                                              <div class="modal-footer">

                                                              <button type="submit" class="btn btn-success" name="update_payment" value="<?php echo $tra_id; ?>"><span class="fa fa-check-circle"></span>&nbsp; Submit</button>

                                                              <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-times-circle"></span>&nbsp; Close</button>

                                                            </div>
                                                           </form>
                                                          </div> 

                                                        </div>
                                                      </div>


                                      <?php

                                        }else
                                        if($admin_st == '1')
                                        {

                                         // echo "Paid";
                                          echo '<a href="teachers/'.$teach_id.'/'.$class_id.'/'.'choose_category.php?clz=today"><span class="green" style="background: #28a745;width:120px;position:absolute;bottom:10px;right:10px;" data-toggle="tooltip" data-title="ඔබ නියමිත පන්ති ගාස්තු ගෙවා ඇති බැවින් ඔබට ප්‍රවේශ වීමට අවසර ඇත."><span class="fa fa-check-circle"></span> Participate</span></a>';
                                        }
                                    }else
                                    if($payments == '0')
                                    {
                                      //echo "Not Paid But Registered(Paid)";

                                      echo '<a data-toggle="modal" data-target="#add_payment'.$class_id.'"><span class="cyan" style="background: #007bff;position:absolute;bottom:10px;right:10px;" data-toggle="tooltip" data-title="ඔබ ලියාපදිංචි වු පන්තියට අදාල අය කිරීම් සිදු කිරීමට මෙම බොත්තම ක්ලික් කරන්න."><span class="fa fa-money"></span> Pay</span></a>';

                                      ?>


                                      <div id="add_payment<?php echo $class_id; ?>" class="modal fade" role="dialog">
                                                <div class="modal-dialog">
                                                    
                                                  <!-- Modal content-->
                                                  <div class="modal-content" style="padding: 15px 10px 15px 10px;">

                                                   <div class="modal-header">
                                                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                      <h4 class="modal-title" style="margin-bottom: 20px;"><?php echo $class_name; ?></h4>
                                                    </div>

                                                    <div class="col-md-12">
                                                      <div class="row">
                                                        <div class="col-md-6" style="padding: 10px 10px 10px 10px;text-align: center;cursor: pointer;" data-toggle="modal" data-target="#bank_deposit<?php echo $class_id; ?>"  data-dismiss="modal">

                                                          <div class="col-md-12" style="border:1px solid #cccc;" data-toggle="tooltip" data-title="බැංකුව හරහා මුදල් තැන්පත් කර සිදුකරන ගෙවීම් මෙහිදී සිදුකල හැක.">
                                                            
                                                            <img src="images/pay.png" class="image-responsive" style="width: 100%;">
                                                            <h5 style="text-align: center;font-weight: bold;color: gray;">Bank Deposit</h5>
                                                          
                                                          </div>
                                                          
                                                        </div>
                                                        <div class="col-md-6" style="padding: 10px 10px 10px 10px;text-align: center;cursor: pointer;" data-toggle="modal" data-target="#online_payment<?php echo $class_id; ?>"  data-dismiss="modal">

                                                          <div class="col-md-12" style="border:1px solid #cccc;" data-toggle="tooltip" data-title="අන්තර්ජාලය හරහා මුදල් ගෙවීම සිදුකල හැකිය.">
                                                            
                                                            <img src="images/online_pay.png" class="image-responsive" style="width: 100%;">
                                                            <h5 style="text-align: center;font-weight: bold;color: gray;">Online Payment</h5>
                                                          
                                                          </div>
                                                          
                                                        </div>
                                                      </div>
                                                    </div>
                                                  
                                                  </div> 

                                                </div>
                                              </div>


                                              <div id="bank_deposit<?php echo $class_id; ?>" class="modal fade" role="dialog">
                                                <div class="modal-dialog">

                                                  <!-- Modal content-->
                                                  <div class="modal-content" style="padding: 15px 10px 15px 10px;">

                                                     <form action="../student/query/insert.php" method="POST" enctype="multipart/form-data" enctype="multipart/form-data">
                                                    <div class="modal-header">
                                                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                      <h4 class="modal-title" style="margin-bottom: 20px;"><?php echo $class_name; ?></h4>
                                                    </div>
                                                    
                                                    <div class="modal-body">
                                                      <lable style="font-size: 16px;font-weight: bold;">Select Month : </lable>
                                                      <select class="form-control" style="margin-top:10px;margin-bottom:10px;" name="month" required>
                                                        <?php  
                                                        $m=date('m');
                                                        $month = date('F', mktime(0,0,0,$m));

                                                        echo '<option value='.$m.'>'.$month.'</option>';

                                                        /*$cu_month = date('m');
                                                        for ($m=1; $m<=$cu_month; $m++) 
                                                        {
                                                           $month = date('F', mktime(0,0,0,$m));
                                                           echo '<option value='.$m.'>'.$month.'</option>';
                                                         }*/
                                                        ?>

                                                      </select>

                                                      <lable style="font-size: 16px;font-weight: bold;">Month Fees : </lable>
                                                      <input type="number" class="form-control" value="<?php echo $fees; ?>" onfocus="this.value=''" style="margin-top:10px;margin-bottom:10px;" required disabled>
                                                      <input type="hidden" name="sub_id" value="<?php echo $sub_id; ?>">
                                                      <input type="hidden" name="level_id" value="<?php echo $level_id; ?>">
                                                      <input type="hidden" name="class_id" value="<?php echo $class_id; ?>">
                                                      <input type="hidden" name="fees" value="<?php echo $fees; ?>">
                                                      <input type="hidden" name="clz" value="today_clz">
                                                      <input type="hidden" name="stu_id" value="<?php echo $stu_id; ?>">

                                                      <lable style="font-size: 16px;font-weight: bold;">Upload Payment Receipt : </lable>
                                                      <input type="file" class="form-control" name="upload_file" required  style="margin-top:10px;margin-bottom:10px;" title="Upload Here" accept="image/*">
                                                      <small class="text-danger"><span class="fa fa-asterisk"></span> ඔබ බැංකුව වෙත මුදල් ගෙවු පසු බැංකුව විසින් මුදල් භාරගත් බවට දෙන රිසිට් පතෙහි පින්තූරයක් ඉහත බොත්තම ක්ලික් කර තෝරා අවසානයේ පහත 'Submit' බොත්තම ක්ලික් කරන්න.</small>
                                                      
                                                    </div>

                                                    <div class="col-md-12"><a data-toggle="modal" data-target="#add_payment<?php echo $class_id; ?>" style="float: left;cursor: pointer;" data-dismiss="modal">Back</a></div>
                                                    
                                                      <div class="modal-footer">


                                                      <button type="submit" class="btn btn-success" name="create_payment" value="<?php echo $tra_id; ?>"><span class="fa fa-check-circle"></span>&nbsp;Submit</button>

                                                      <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-times-circle"></span>&nbsp;Close</button>

                                                    </div>
                                                   </form>
                                                  
                                                  </div> 

                                                </div>
                                              </div>


                                            <div id="online_payment<?php echo $class_id; ?>" class="modal fade" role="dialog">
                                                <div class="modal-dialog">

                                                  <!-- Modal content-->
                                                  <div class="modal-content" style="padding: 15px 10px 15px 10px;">

                                                    <div class="col-md-12">
                                                    <h2>Payment Gateway</h2>
                                                    </div>
                                                    <div class="col-md-12"><a data-toggle="modal" data-target="#add_payment<?php echo $class_id; ?>" style="float: left;cursor: pointer;" data-dismiss="modal">Back</a></div>
                                                    
                                                      <div class="modal-footer">

<!-- 
                                                      <button type="submit" class="btn btn-success" name="create_payment" value="<?php echo $tra_id; ?>"><span class="fa fa-check-circle"></span>&nbsp;Submit</button> -->

                                                      <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-times-circle"></span>&nbsp;Close</button>

                                                    </div>
                                                  </div> 

                                                </div>
                                              </div>


                                              <?php


                                    }

                                        }
                          
                      }
                      else
                      if($free == 'Yes')
                      {

                        $reg_sub = mysqli_query($conn,"SELECT * FROM transactions WHERE STU_ID = '$stu_id' AND CLASS_ID = '$class_id'");

                        $check_reg = mysqli_num_rows($reg_sub);

                        $ro = mysqli_fetch_assoc($reg_sub);
                        $tra_id = $ro['TRA_ID'];

                        if($check_reg>0)
                        {

                          //echo "Registered show Participate(Free Access)";

                          ?>
                          <a href="../student/query/delete.php?tr_id=<?php echo $tra_id; ?>&&level_id=<?php echo $level_id; ?>&&sub_id=<?php echo $sub_id; ?>&&un_reg=un_reg&&clz=today" class="btn btn-danger btn-rounded btn-xs  btn-active" onclick="return confirm('Are you sure Unregister?')"  style="position:absolute;top:2px;right:3px;padding-top: 5px;padding-bottom: 5px;opacity: 0.6;" data-toggle="tooltip" data-title="ඔබ දැනටමත් ලියාපදිංචි පන්තිය අවලංගු කිරීම."><span class="fa fa-times"></span></a>
                            <?php

                           echo '<a href="teachers/'.$teach_id.'/'.$class_id.'/'.'choose_category.php?clz=today"><span class="green" style="background: #28a745;width:120px;position:absolute;bottom:10px;right:10px;" data-toggle="tooltip" data-title="ඔබ නියමිත පන්ති ගාස්තු ගෙවා ඇති බැවින් ඔබට ප්‍රවේශ වීමට අවසර ඇත."><span class="fa fa-check-circle"></span> Participate</span></a>';

                        }else
                        if($check_reg == '0')
                        { 
                           // echo "Not Registered(Free Access)";

                            ?>
                                  <a href="../student/query/insert.php?class_id=<?php echo $class_id; ?>&&teach_id=<?php echo $teach_id; ?>&&level_id=<?php echo $level_id; ?>&&sub_id=<?php echo $sub_id; ?>&&stu_id=<?php echo $stu_id; ?>&&class_reg=class_reg&&clz=today"><span class="red"style="background: #ff4b4b;position:absolute;bottom:10px;right:10px;" data-toggle="tooltip" data-title="ඔබට නව පන්තියක ලියාපදිංචි වීමට අවශ්‍ය නම් මෙම බොත්තම ක්ලික් කරන්න."><span class="fa fa-thumbs-up"></span> Register</span></a>
                               <?php
                        }



                        
                      }

                    


                    $ssql02 = mysqli_query($conn,"SELECT * FROM teacher_details WHERE TEACH_ID = '$teach_id'");
                        while($row00123 = mysqli_fetch_assoc($ssql02))
                        {
                            $t_name = $row00123['F_NAME']." ".$row00123['L_NAME'];
                            
                        }



                    echo '<img alt="Class Image" src="images/class.jpg" style="width:100%;">
                          </div>
                          <div class="row" style="border:0px solid black;padding-left:10px;padding-right:4px;">
                            <div class="col-md-10"><label style="font-size: 20px;padding-top: 10px;border:0px solid black;padding-bottom: 0;padding-left:0;">'.$class_name.'</label></div>

                            <div class="col-md-2" style="padding-top: 10px;border:0px solid black;padding-bottom: 0;text-transform:uppercase;font-size:14px;"><lable><b>'.substr($day,0,3).'</b></lable></div>
                          </div>
                          </div>
                          <div class="col-md-12" style="padding-top: 0;border:0px solid black;">
                          <lable style="margin-bottom: 20px;font-size: 12px;border:0px solid black;" class="text-danger"><b>Time : '.$start_time.' - '.$end_time.'</b></lable>

                          </div>



                          <div class="col-md-12">
                          <div class="profile-img-wrapper m-t-5 inline">
                          <img width="35" height="35" data-src-retina="../teacher/images/profile/'.$picture.'" data-src="../teacher/images/profile/'.$picture.'" alt="" src="../teacher/images/profile/'.$picture.'">
                          <div class="chat-status available">
                          </div>
                          </div>
                          <div class="inline m-l-10">
                          <p class="small hint-text m-t-5">'.$t_name.'
                          <br>'.$subject_name.'</p>
                          </div>

                          </div>

                          <div class="col-md-12" style="border-top:1px solid #cccc;">
                          <h3 style="color:gray;text-align:left;"><img src="images/money.png" width="30" height="30"> Rs '.number_format($fees,2).'</h3>
                          </div>


                          </div>
                          </div>
                          </div>
                          </div>';
            }else
            {




              echo '
                  <div class="col-lg-3 sm-no-padding" style="margin-top:20px;" id="cc">
                    <div class="card card-transparent">
                    <div class="card-body no-padding">
                    <div id="card-advance" class="card card-default" style="border:0px solid black;">

                    <div class="card-body" style="padding: 0;border:0px solid black;">
                    <div style="position:relative;">
                  ';

                    

                    $ssql0211 = mysqli_query($conn,"SELECT * FROM transactions WHERE STU_ID = '$stu_id' AND CLASS_ID = '$class_id'");
                        $check_register_paid = mysqli_num_rows($ssql0211);
                        while($row0012311 = mysqli_fetch_assoc($ssql0211))
                        {
                            $tra_id = $row0012311['TRA_ID'];
                            
                        }

                        


                        if($free !== 'Yes')
                        {
                          

                          
                            if($check_register_paid== '0')
                            {

                              //echo "Not Register(Paid)";

                              ?>
                                  <a href="../student/query/insert.php?class_id=<?php echo $class_id; ?>&&teach_id=<?php echo $teach_id; ?>&&level_id=<?php echo $level_id; ?>&&sub_id=<?php echo $sub_id; ?>&&stu_id=<?php echo $stu_id; ?>&&class_reg=class_reg&&clz=today"><span class="red"style="background: #ff4b4b;position:absolute;bottom:10px;right:10px;" data-toggle="tooltip" data-title="ඔබට නව පන්තියක ලියාපදිංචි වීමට අවශ්‍ය නම් මෙම බොත්තම ක්ලික් කරන්න." onclick="return confirm('Are you sure Register?')" ><span class="fa fa-thumbs-up"></span> Register</span></a>
                               <?php

                            }else
                            if($check_register_paid>0)
                            {
                                  $y = date('Y');
                                  $m = date('m');

                                    $ssql04 = mysqli_query($conn,"SELECT * FROM payment_data WHERE TRA_ID = '$tra_id' AND YEAR = '$y' AND MONTH = '$m'");
                                    $payments = mysqli_num_rows($ssql04);

                                    ?>

                                    <a href="../student/query/delete.php?tr_id=<?php echo $tra_id; ?>&&level_id=<?php echo $level_id; ?>&&sub_id=<?php echo $sub_id; ?>&&un_reg=un_reg&&clz=today" class="btn btn-danger btn-rounded btn-xs  btn-active" onclick="return confirm('Are you sure Unregister?')"  style="position:absolute;top:2px;right:3px;padding-top: 5px;padding-bottom: 5px;opacity: 0.6;" data-toggle="tooltip" data-title="ඔබ දැනටමත් ලියාපදිංචි පන්තිය අවලංගු කිරීම."><span class="fa fa-times"></span></a>
                                      <?php

                                    if($payments>0)
                                    {

                                    
                                        $row00120 = mysqli_fetch_assoc($ssql04);
                                        $admin_st = $row00120['ADMIN_SUBMIT'];
                                        $tr_file = $row00120['LAST_UPLOAD_FILE'];
                                        $cu_file = $row00120['FILE_NAME'];

                                        if($admin_st == '0')
                                        {

                                          //echo "Pending";
                                          echo '<span class="yellow" style="background: #f1b501;width:120px;position:absolute;bottom:10px;right:10px;" data-toggle="tooltip" data-title="ඔබ ඇතුලත් කල දත්ත පරික්ෂා කිරීමට යොමු කොට ඇත.හැකි ඉක්මනින් අප නියෝජිතයකු ඔබ වෙත ප්‍රතිචාර දක්වයි.එතෙක් රැදී සිටින්න."><span class="fa fa-bullhorn"></span> Pending</span>';


                                        }else
                                        if($admin_st == '2')
                                        {

                                          //echo "Reject";

                                          echo '<a data-toggle="modal" data-target="#again'.$class_id.'" ><span class="white"style="background: #ffffff;width:120px;color:black;position:absolute;bottom:10px;right:10px;" data-toggle="tooltip" data-title="ඔබ ඇතුලත් කල රිසිට්පතෙහි නිවැරදිබව හෝ අපැහැදිලිබවක් ඇති බැවින් නැවතත් පරික්ෂා කොට අප වෙත යොමු කරන්න."><span class="  fa fa-frown-o"></span> Again</span></a>';
                                      ?>

                                        <div id="again<?php echo $class_id; ?>" class="modal fade" role="dialog">
                                                        <div class="modal-dialog">

                                                          <!-- Modal content-->
                                                          <div class="modal-content">
                                                          <form action="../student/query/update.php" method="POST" enctype="multipart/form-data" enctype="multipart/form-data">
                                                            <div class="modal-header">
                                                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                              <h4 class="modal-title" style="margin-bottom: 20px;"><?php echo $class_name; ?></h4>
                                                            </div>
                                                            
                                                            <div class="modal-body">
                                                              <lable style="font-size: 16px;font-weight: bold;">Select Month : </lable>
                                                              <select class="form-control" style="margin-top:10px;margin-bottom:10px;" name="month" required>
                                                                <?php  
                                                                $m=date('m');
                                                                $month = date('F', mktime(0,0,0,$m));

                                                                echo '<option value='.$m.'>'.$month.'</option>';

                                                                /*$cu_month = date('m');
                                                                for ($m=1; $m<=$cu_month; $m++) 
                                                                {
                                                                   $month = date('F', mktime(0,0,0,$m));
                                                                   echo '<option value='.$m.'>'.$month.'</option>';
                                                                 }*/
                                                                ?>

                                                              </select>

                                                              <lable style="font-size: 16px;font-weight: bold;">Month Fees : </lable>
                                                              <input type="number" class="form-control" value="<?php echo $fees; ?>" style="margin-top:10px;margin-bottom:10px;" required  onfocus="this.value=''" disabled>
                                                              <input type="hidden" name="sub_id" value="<?php echo $sub_id; ?>">
                                                              <input type="hidden" name="level_id" value="<?php echo $level_id; ?>">
                                                              <input type="hidden" name="class_id" value="<?php echo $class_id; ?>">
                                                              <input type="hidden" name="recent_file" value="<?php echo $cu_file; ?>">
                                                              <input type="hidden" name="clz" value="today_clz">
                                                              <input type="hidden" name="fees" value="<?php echo $fees; ?>">
                                                              <input type="hidden" name="stu_id" value="<?php echo $stu_id; ?>">


                                                              <lable style="font-size: 16px;font-weight: bold;">Upload Payment Receipt : </lable>
                                                              <input type="file" class="form-control" name="upload_file" required  style="margin-top:10px;margin-bottom:10px;" title="Upload Here" accept="image/*" id="up_rec">
                                                              
                                                              <div class="row show_image">
                                                                <div class="col-md-4"></div>
                                                                <div class="col-md-4"><img src="../admin/images/reciption/<?php echo $cu_file; ?>" alt="Receiption Image" class="image-responsive" style="width: 100%;"></div>
                                                                <div class="col-md-4"></div>
                                                              </div>
                                                              <div class="col-md-12 text-success show_image" style="text-align: center;margin-top: 20px;"><label style="text-align: center;"><span class="fa fa-upload"></span> Recent Uploaded Image</label></div>
                                                              
                                                            </div>
                                                              <div class="modal-footer">

                                                              <button type="submit" class="btn btn-success" name="update_payment" value="<?php echo $tra_id; ?>"><span class="fa fa-check-circle"></span>&nbsp; Submit</button>

                                                              <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-times-circle"></span>&nbsp; Close</button>

                                                            </div>
                                                           </form>
                                                          </div> 

                                                        </div>
                                                      </div>


                                      <?php

                                        }else
                                        if($admin_st == '1')
                                        {

                                          //echo "Paid";
                                          echo '<a href="teachers/'.$teach_id.'/'.$class_id.'/'.'choose_category.php?clz=today"><span class="green" style="background: #28a745;width:120px;position:absolute;bottom:10px;right:10px;" data-toggle="tooltip" data-title="ඔබ නියමිත පන්ති ගාස්තු ගෙවා ඇති බැවින් ඔබට ප්‍රවේශ වීමට අවසර ඇත."><span class="fa fa-check-circle"></span> Participate</span></a>';
                                        }
                                    }else
                                    if($payments == '0')
                                    {
                                      //echo "Not Paid But Registered(Paid)";

                                      echo '<a data-toggle="modal" data-target="#add_payment'.$class_id.'"><span class="cyan" style="background: #007bff;position:absolute;bottom:10px;right:10px;" data-toggle="tooltip" data-title="ඔබ ලියාපදිංචි වු පන්තියට අදාල අය කිරීම් සිදු කිරීමට මෙම බොත්තම ක්ලික් කරන්න."><span class="fa fa-money"></span> Pay</span></a>';

                                      ?>


                                      <div id="add_payment<?php echo $class_id; ?>" class="modal fade" role="dialog">
                                                <div class="modal-dialog">
                                                    
                                                  <!-- Modal content-->
                                                  <div class="modal-content" style="padding: 15px 10px 15px 10px;">

                                                   <div class="modal-header">
                                                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                      <h4 class="modal-title" style="margin-bottom: 20px;"><?php echo $class_name; ?></h4>
                                                    </div>

                                                    <div class="col-md-12">
                                                      <div class="row">
                                                        <div class="col-md-6" style="padding: 10px 10px 10px 10px;text-align: center;cursor: pointer;" data-toggle="modal" data-target="#bank_deposit<?php echo $class_id; ?>"  data-dismiss="modal">

                                                          <div class="col-md-12" style="border:1px solid #cccc;" data-toggle="tooltip" data-title="බැංකුව හරහා මුදල් තැන්පත් කර සිදුකරන ගෙවීම් මෙහිදී සිදුකල හැක.">
                                                            
                                                            <img src="images/pay.png" class="image-responsive" style="width: 100%;">
                                                            <h5 style="text-align: center;font-weight: bold;color: gray;">Bank Deposit</h5>
                                                          
                                                          </div>
                                                          
                                                        </div>
                                                        <div class="col-md-6" style="padding: 10px 10px 10px 10px;text-align: center;cursor: pointer;" data-toggle="modal" data-target="#online_payment<?php echo $class_id; ?>"  data-dismiss="modal">

                                                          <div class="col-md-12" style="border:1px solid #cccc;" data-toggle="tooltip" data-title="අන්තර්ජාලය හරහා මුදල් ගෙවීම සිදුකල හැකිය.">
                                                            
                                                            <img src="images/online_pay.png" class="image-responsive" style="width: 100%;">
                                                            <h5 style="text-align: center;font-weight: bold;color: gray;">Online Payment</h5>
                                                          
                                                          </div>
                                                          
                                                        </div>
                                                      </div>
                                                    </div>
                                                  
                                                  </div> 

                                                </div>
                                              </div>


                                              <div id="bank_deposit<?php echo $class_id; ?>" class="modal fade" role="dialog">
                                                <div class="modal-dialog">

                                                  <!-- Modal content-->
                                                  <div class="modal-content" style="padding: 15px 10px 15px 10px;">

                                                     <form action="../student/query/insert.php" method="POST" enctype="multipart/form-data" enctype="multipart/form-data">
                                                    <div class="modal-header">
                                                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                      <h4 class="modal-title" style="margin-bottom: 20px;"><?php echo $class_name; ?></h4>
                                                    </div>
                                                    
                                                    <div class="modal-body">
                                                      <lable style="font-size: 16px;font-weight: bold;">Select Month : </lable>
                                                      <select class="form-control" style="margin-top:10px;margin-bottom:10px;" name="month" required>
                                                        <?php  
                                                        $m=date('m');
                                                        $month = date('F', mktime(0,0,0,$m));

                                                        echo '<option value='.$m.'>'.$month.'</option>';

                                                        /*$cu_month = date('m');
                                                        for ($m=1; $m<=$cu_month; $m++) 
                                                        {
                                                           $month = date('F', mktime(0,0,0,$m));
                                                           echo '<option value='.$m.'>'.$month.'</option>';
                                                         }*/
                                                        ?>

                                                      </select>

                                                      <lable style="font-size: 16px;font-weight: bold;">Month Fees : </lable>
                                                      <input type="number" class="form-control" value="<?php echo $fees; ?>" onfocus="this.value=''" style="margin-top:10px;margin-bottom:10px;" required disabled>
                                                      <input type="hidden" name="sub_id" value="<?php echo $sub_id; ?>">
                                                      <input type="hidden" name="level_id" value="<?php echo $level_id; ?>">
                                                      <input type="hidden" name="class_id" value="<?php echo $class_id; ?>">
                                                      <input type="hidden" name="fees" value="<?php echo $fees; ?>">
                                                      <input type="hidden" name="clz" value="today_clz">
                                                      <input type="hidden" name="stu_id" value="<?php echo $stu_id; ?>">

                                                      <lable style="font-size: 16px;font-weight: bold;">Upload Payment Receipt : </lable>
                                                      <input type="file" class="form-control" name="upload_file" required  style="margin-top:10px;margin-bottom:10px;" title="Upload Here" accept="image/*">
                                                      <small class="text-danger"><span class="fa fa-asterisk"></span> ඔබ බැංකුව වෙත මුදල් ගෙවු පසු බැංකුව විසින් මුදල් භාරගත් බවට දෙන රිසිට් පතෙහි පින්තූරයක් ඉහත බොත්තම ක්ලික් කර තෝරා අවසානයේ පහත 'Submit' බොත්තම ක්ලික් කරන්න.</small>
                                                      
                                                    </div>

                                                    <div class="col-md-12"><a data-toggle="modal" data-target="#add_payment<?php echo $class_id; ?>" style="float: left;cursor: pointer;" data-dismiss="modal">Back</a></div>
                                                    
                                                      <div class="modal-footer">


                                                      <button type="submit" class="btn btn-success" name="create_payment" value="<?php echo $tra_id; ?>"><span class="fa fa-check-circle"></span>&nbsp;Submit</button>

                                                      <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-times-circle"></span>&nbsp;Close</button>

                                                    </div>
                                                   </form>
                                                  
                                                  </div> 

                                                </div>
                                              </div>


                                            <div id="online_payment<?php echo $class_id; ?>" class="modal fade" role="dialog">
                                                <div class="modal-dialog">

                                                  <!-- Modal content-->
                                                  <div class="modal-content" style="padding: 15px 10px 15px 10px;">

                                                    <div class="col-md-12">
                                                    <h2>Payment Gateway</h2>
                                                    </div>
                                                    <div class="col-md-12"><a data-toggle="modal" data-target="#add_payment<?php echo $class_id; ?>" style="float: left;cursor: pointer;" data-dismiss="modal">Back</a></div>
                                                    
                                                      <div class="modal-footer">

<!-- 
                                                      <button type="submit" class="btn btn-success" name="create_payment" value="<?php echo $tra_id; ?>"><span class="fa fa-check-circle"></span>&nbsp;Submit</button> -->

                                                      <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-times-circle"></span>&nbsp;Close</button>

                                                    </div>
                                                  </div> 

                                                </div>
                                              </div>


                                              <?php


                                    }

                                        }
                          
                      }
                      else
                      if($free == 'Yes')
                      {

                        $reg_sub = mysqli_query($conn,"SELECT * FROM transactions WHERE STU_ID = '$stu_id' AND CLASS_ID = '$class_id'");

                        $check_reg = mysqli_num_rows($reg_sub);

                        $ro = mysqli_fetch_assoc($reg_sub);
                        $tra_id = $ro['TRA_ID'];

                        if($check_reg>0)
                        {

                          //echo "Registered show Participate(Free Access)";
                          ?>
                          <a href="../student/query/delete.php?tr_id=<?php echo $tra_id; ?>&&level_id=<?php echo $level_id; ?>&&sub_id=<?php echo $sub_id; ?>&&un_reg=un_reg&&clz=today" class="btn btn-danger btn-rounded btn-xs  btn-active" onclick="return confirm('Are you sure Unregister?')"  style="position:absolute;top:2px;right:3px;padding-top: 5px;padding-bottom: 5px;opacity: 0.6;" data-toggle="tooltip" data-title="ඔබ දැනටමත් ලියාපදිංචි පන්තිය අවලංගු කිරීම."><span class="fa fa-times"></span></a>
                            <?php
                           echo '<a href="teachers/'.$teach_id.'/'.$class_id.'/'.'choose_category.php?clz=today"><span class="green" style="background: #28a745;width:120px;position:absolute;bottom:10px;right:10px;" data-toggle="tooltip" data-title="ඔබ නියමිත පන්ති ගාස්තු ගෙවා ඇති බැවින් ඔබට ප්‍රවේශ වීමට අවසර ඇත."><span class="fa fa-check-circle"></span> Participate</span></a>';

                        }else
                        if($check_reg == '0')
                        { 
                            //echo "Not Registered(Free Access)";

                            ?>
                                  <a href="../student/query/insert.php?class_id=<?php echo $class_id; ?>&&teach_id=<?php echo $teach_id; ?>&&level_id=<?php echo $level_id; ?>&&sub_id=<?php echo $sub_id; ?>&&stu_id=<?php echo $stu_id; ?>&&class_reg=class_reg&&clz=today"><span class="red"style="background: #ff4b4b;position:absolute;bottom:10px;right:10px;" data-toggle="tooltip" data-title="ඔබට නව පන්තියක ලියාපදිංචි වීමට අවශ්‍ය නම් මෙම බොත්තම ක්ලික් කරන්න."><span class="fa fa-thumbs-up"></span> Register</span></a>
                               <?php
                        }



                        
                      }

                    


                    $ssql02 = mysqli_query($conn,"SELECT * FROM teacher_details WHERE TEACH_ID = '$teach_id'");
                        while($row00123 = mysqli_fetch_assoc($ssql02))
                        {
                            $t_name = $row00123['F_NAME']." ".$row00123['L_NAME'];
                            
                        }



                    echo '<img alt="Class Image" src="images/class.jpg" style="width:100%;">
                          </div>
                          <div class="row" style="border:0px solid black;padding-left:10px;padding-right:4px;">
                            <div class="col-md-10"><label style="font-size: 20px;padding-top: 10px;border:0px solid black;padding-bottom: 0;padding-left:0;">'.$class_name.'</label></div>

                            <div class="col-md-2" style="padding-top: 10px;border:0px solid black;padding-bottom: 0;text-transform:uppercase;font-size:14px;"><lable><b>'.substr($day,0,3).'</b></lable></div>
                          </div>
                          </div>
                          <div class="col-md-12" style="padding-top: 0;border:0px solid black;">
                          <lable style="margin-bottom: 20px;font-size: 12px;border:0px solid black;" class="text-danger"><b>Time : '.$start_time.' - '.$end_time.'</b></lable>

                          </div>



                          <div class="col-md-12">
                          <div class="profile-img-wrapper m-t-5 inline">
                          <img width="35" height="35" data-src-retina="../teacher/images/profile/'.$picture.'" data-src="../teacher/images/profile/'.$picture.'" alt="" src="../teacher/images/profile/'.$picture.'">
                          <div class="chat-status available">
                          </div>
                          </div>
                          <div class="inline m-l-10">
                          <p class="small hint-text m-t-5">'.$t_name.'
                          <br>'.$subject_name.'</p>
                          </div>
                          </div>

                          <div class="col-md-12" style="border-top:1px solid #cccc;">
                          <h3 style="color:gray;text-align:left;"><img src="images/money.png" width="30" height="30"> Rs '.number_format($fees,2).'</h3>
                          </div>


                          </div>
                          </div>
                          </div>
                          </div>';





            }


            }echo "</div>";

                
            }
            else
            if($count == '0')
            {
                echo '
                <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6" style="margin-top: 20px;text-align:center;">
                  <img src="images/empty.png" class="image-responsive-height">
                </div>
                <div class="col-md-3"></div>
                </div>';
              }

           ?>

</div>
</div>

<!-- search data -->

<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myDIV #cc").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>


<script>
$(document).ready(function(){
  $("#up_rec").on("change", function() {
      
      $('.show_image').hide();
  });
});
</script>

<!-- search data -->

<!-- no data message/no found data show in search-->

<script type="text/javascript">
  $(document).ready(function () {

    (function ($) {

        $('#myInput').keyup(function () {
            var rex = new RegExp($(this).val(), 'i');
            $('#myDIV #cc').hide();
            $('#myDIV #cc').filter(function () {
                return rex.test($(this).text());
            }).show();
            $('.no-data').hide();
            if($('#myDIV #cc:visible').length == 0)
            {
                $('.no-data').show();
            }

        })

    }(jQuery));

});
</script>

<script type="text/javascript">
  $('li').click(function(){
  
  $(this).addClass('active')
       .siblings()
       .removeClass('active');
    
});
</script>
<!-- no data message/no found data show in search-->

<?php  include('footer/footer.php'); ?>





