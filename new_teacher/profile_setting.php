<?php

  $page = "Profile Setting";
  $folder_in = '0';

  include('header/header.php'); 

  $teach_id = $_SESSION['TEACH_ID'];

?>
  <style type="text/css">
    .container {
  position: relative;
  width: 100%;
}


.middle {
  transition: .5s ease;
  opacity: 0;
  position: absolute;
  top: 32%;
  left: 50%;
  transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  text-align: center;
  height: 100px;
}

.container:hover .image {
  opacity: 0.3;
}

.container:hover .middle {
  opacity: 1;
}

.text {
  background-color:#0000008c;
  color: white;
  font-size: 14px;
  padding: 34px 40px;
  border-radius: 80px;
}
  </style>

  <?php 


    if(!empty($_SESSION['click_profile_tab']))
    {
      $click_tab = $_SESSION['click_profile_tab'];

      if($click_tab == 'password')
      {
        $change_password = 'active';
      }else
      if($click_tab == 'profile')
      {
        $profile_setting = 'active';
      }else
      if($click_tab == 'bank')
      {
        $bank_details = 'active';
      }

    }else
    if(empty($_SESSION['click_profile_tab']))
    {
      $change_password = 'active';

      $profile_setting = '';

      $bank_details = '';

    }

   ?>



<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<div class="gallery" style="border:0px solid black;margin-top: 0;">

<ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;border-bottom: 1px solid #cccc;margin-top: 4%;margin-bottom: 0;">
<li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
</ol>

<div class="col-md-12" style="padding-top: 20px;">

<div class="card card-transparent">

<ul class="nav nav-tabs nav-tabs-fillup" data-init-reponsive-tabs="dropdownfx">

<li class="nav-item">
<a href="#" data-toggle="tab" class="<?php echo $change_password; ?>" data-target="#slide1" onclick="profile_click_tab('password');"><span>Change Password</span></a>
</li>
<li class="nav-item">
<a href="#" data-toggle="tab" class="<?php echo $profile_setting; ?>" data-target="#slide2" onclick="profile_click_tab('profile');"><span>Profile Setting</span></a>
</li>

<li class="nav-item">
<a href="#" data-toggle="tab" class="<?php echo $bank_details; ?>" data-target="#slide3" onclick="profile_click_tab('bank');"><span>Bank Details</span></a>
</li>

<!-- <li class="nav-item">
<a href="#" data-toggle="tab" data-target="#slide3"><span>Messages</span></a>
</li> -->
</ul>


 <script type="text/javascript">
    function profile_click_tab(tab_name)
    {
          
          $.ajax({  
          url:"../teacher/query/check.php",  
          method:"POST",  
          data:{click_profile_tab:tab_name},  
          success:function(data){ 
             
             //alert(data)
           }           
         });

    }
 </script>



<div class="tab-content">
<div class="tab-pane slide-left <?php echo $change_password; ?>" id="slide1">
<div class="row column-seperation">

<div class="col-lg-3">
<h3>
  
</h3>
</div>

<div class="col-lg-6">
<form action="../teacher/query/update.php" method="POST" autocomplete="off">
<input type="hidden" id="teacher_id99" value="<?php echo $teach_id; ?>">


<h3 class="semi-bold" style="text-transform: uppercase;text-align: center;">change password</h3>

<div class="form-group" id="show_hide_password" style="position: relative;">
<h4 class="text-success">Current password</h4>
<input type="password" name="cp" placeholder="Current Password.." id="cp" class="form-control text-success" required>
<a href=""><i class="fa fa-eye-slash" aria-hidden="true" style="font-size: 18px;color: black;position: absolute;right:10px;top:50px;"></i></a>

</div>


<lable id="availability6" style="padding: 15px 10px 15px 0px;"></lable>

<div class="form-group" id="show_hide_password2" style="position: relative;">
<h4 class="text-danger">New password</h4>
<input type="password" name="npass" placeholder="New Password.." id="txtNewPassword" class="form-control text-danger" required>

<a href=""><i class="fa fa-eye-slash" aria-hidden="true" style="font-size: 18px;color: black;position: absolute;right:10px;top:50px;"></i></a>

</div>

<div class="form-group" id="show_hide_password3" style="position: relative;">
<h4 class="text-danger">Re-enter password</h4>
<input type="password" name="cpass" placeholder="Re-enter Password.." class="form-control text-danger" onkeyup="checkPasswordMatch();" id="txtConfirmPassword" required>

<a href=""><i class="fa fa-eye-slash" aria-hidden="true" style="font-size: 18px;color: black;position: absolute;right:10px;top:50px;"></i></a>
</div>

<span id="availability7" style="float: left;margin: 10px 0px 0px 0px;"></span>

<br>

<button class="btn btn-default btn-cons pull-right" style="margin-left: 10px;" type="reset"> Clear</button>
<button  type="submit"  name="submit_btn" value="<?php echo $teach_id; ?>" id="submit_btn" class="btn btn-success btn-cons pull-right"> Change</button>
</form>
</div>

<div class="col-lg-3">
<h3>
  
</h3>
</div>

</div>
</div>
<div class="tab-pane slide-left <?php echo $profile_setting; ?>" id="slide2">
  <div class="row">
<div class="col-lg-2"></div>
<div class="col-lg-8">

      

                 <div class="card">
<div class="card-header ">
<div class="card-title">


  <?php 
             $teach_id = $_SESSION['TEACH_ID'];

             $sql = mysqli_query($conn,"SELECT * FROM teacher_details WHERE TEACH_ID = '$teach_id'");
              while($row = mysqli_fetch_assoc($sql))
              {
                $name = $row['F_NAME']." ".$row['L_NAME'];
                $picture = $row['PICTURE'];
                $position = $row['POSITION'];
                $tp = $row['TP'];

                $real_pic = $picture;

                $fnm = $row['F_NAME'];

                $lnm = $row['L_NAME'];


                $email = $row['EMAIL'];
                $address = $row['ADDRESS'];

                $gender = $row['GENDER'];
                $dob = $row['DOB'];

                $tp = $row['TP'];
                $qulification = $row['QUALIFICATION'];

                
                 if($picture == '0')
                 {
                    if($gender == 'Male')
                    {
                      $picture = 'b_teacher.jpg';
                    }else
                    if($gender == 'Female')
                    {
                      $picture = 'g_teacher.jpg';
                    }
                 }
              }

               ?>




</div>
<h3 class="semi-bold" style="text-transform: uppercase;text-align: center;">profile setting</h3>

</div>
<div class="card-body">
<div class="row clearfix">
<div class="col-md-12">
  <div class="row">
  <div class="col-md-4"></div>
  <div class="col-md-4">
    
                  <form action="../teacher/query/update.php" method="POST" enctype="multipart/form-data">

                    <div class="container">
            <center><img src="../teacher/images/profile/<?php echo $picture; ?>" class="rounded-circle image-responsive" id="show_image" style="height: auto;border:1px solid #cccc;width: 68%;border: 10px solid white;">
            <div class="middle">
                <div class="text">

                    <div class="col-md-12" style="margin-bottom: 12px;"><span class="fa fa-camera" onclick ="javascript:document.getElementById('imagefile').click();" style="font-size: 40px;cursor: pointer;"></span></div>
                    <?php 
                    if($real_pic == '0')
                    {
                        $dis = '0.1';
                    }
                    else
                    if($real_pic !== '0')
                    {
                        $dis = '5;color:white;';
                    }
                      ?>
                     
                    <div class="col-md-12"><a href="../teacher/query/update.php?remove_img=<?php echo $teach_id ?>&&recent_img=<?php echo $picture; ?>" onclick="return confirm('Are you sure remove profile picture?')"><span class="fa fa-times-circle" style="font-size: 30px;cursor: pointer;opacity:  <?php echo $dis; ?>;"></span></a></div>

                  
                 
                  

                </div>
            </div></center>
          </div>
                    

                      <input type="hidden" name="recent_img" value="<?php if($picture == 'boy.jpg' || $picture == 'girl.jpg'){ echo "0"; }else{ echo $picture;} ?>">
                      <input id = "imagefile" type="file" style='visibility: hidden;' name="img" accept="image/*" />

                      <button type="button" class="btn btn-info btn-sm" style="border-radius: 80px;outline: none;display: none;" id="select_btn"><span class="fa fa-upload"></span> Upload</button>
                      <center>
                      <button type="submit" name="upload_img" id="upload" class="btn btn-success btn-sm" style="outline: none;display: none;margin-bottom: 10px;" value="<?php echo $teach_id; ?>"> <span class="fa fa-check-circle"></span>&nbsp;Set Profile</button></center>
                  </form>

</div>
  <div class="col-md-4"></div>
  </div>

</div>

<form  action="../teacher/query/update.php" method="POST" id="form-personal" role="form" autocomplete="off">

<div class="row">

  <div class="col-md-12">
<div class="form-group form-group-default input-group">
<div class="form-input-group">
<label>Position</label>

  <select class="form-control change_inputs" name="position">
    
    <?php 
                              echo '
                              <option value="'.$position.'">'.$position.'.</option>';

                              if($position == 'Mr')
                              {
                                  echo '<option value="Mrs">Mrs.</option>
                              <option value="Ms">Ms.</option>
                              <option value="Rev">Rev.</option>
                              <option value="Dr">Dr.</option>
                              <option value="Prof">Prof.</option>';
                              }else
                              if($position == 'Mrs')
                              {
                                echo '<option value="Mr">Mr.</option>
                                      <option value="Ms">Ms.</option>
                                      <option value="Rev">Rev.</option>
                                      <option value="Dr">Dr.</option>
                                      <option value="Prof">Prof.</option>';
                              }else
                              if($position == 'Ms')
                              {
                                echo '<option value="Mr">Mr.</option>
                                      <option value="Mrs">Mrs.</option>
                                      <option value="Rev">Rev.</option>
                                      <option value="Dr">Dr.</option>
                                      <option value="Prof">Prof.</option>';
                              }else
                              if($position == 'Rev')
                              {
                                echo '<option value="Mr">Mr.</option>
                                      <option value="Mrs">Mrs.</option>
                                      <option value="Ms">Ms.</option>
                                      <option value="Dr">Dr.</option>
                                      <option value="Prof">Prof.</option>';
                              }else
                              if($position == 'Dr')
                              {
                                echo '<option value="Mr">Mr.</option>
                                      <option value="Mrs">Mrs.</option>
                                      <option value="Ms">Ms.</option>
                                      <option value="Rev">Rev.</option>
                                      <option value="Prof">Prof.</option>';
                              }else
                              if($position == 'Prof')
                              {
                                echo '<option value="Mr">Mr.</option>
                                      <option value="Mrs">Mrs.</option>
                                      <option value="Ms">Ms.</option>
                                      <option value="Rev">Rev.</option>
                                      <option value="Dr">Dr.</option>';
                              }


                             ?>

  </select>

</div>
</div>
</div>
</div>


<div class="row">
<div class="col-md-6">
<div class="form-group form-group-default input-group">
<div class="form-input-group">
<label>First Name</label>
<input type="text" name="fnm" class="form-control change_inputs" value="<?php echo $fnm; ?>" pattern="[a-zA-Z. ]+"  required>
</div>
</div>
</div>

<div class="col-md-6">
<div class="form-group form-group-default input-group">
<div class="form-input-group">
<label>Last Name</label>
<input type="text" name="lnm" class="form-control change_inputs" value="<?php echo $lnm; ?>" pattern="[a-zA-Z. ]+"  required>
</div>
</div>
</div>

</div>

<div class="row">
<div class="col-md-6">
<div class="form-group form-group-default input-group">
<div class="form-input-group">
<label>Date Of Birth</label>
<input class="form-control change_inputs" type="date" name="dob" required min="<?php $d = strtotime("-60 year"); echo date('Y-m-d',$d); ?>" max="<?php $d = strtotime("-5 year"); echo date('Y-m-d',$d); ?>" style="margin-bottom: 10px;" value="<?php echo $dob; ?>">
</div>
</div>
</div>

<div class="col-md-6">
<div class="form-group form-group-default input-group">
<div class="form-input-group">
<label>Telephone No</label>
<input class="form-control change_inputs" type="telephone" pattern="[0-9]{10}" minlength="10" maxlength="10" name="number" id="number" value="<?php echo $tp; ?>" required placeholder="0xxx xxxx xx" style="margin-bottom: 10px;">
</div>
</div>
</div>

</div>

<div class="row">
<div class="col-md-12">
<div class="form-group form-group-default input-group">
<div class="form-input-group">
<label>E-mail Address</label>
<input type="email" class="form-control change_inputs" name="email" value="<?php echo $email; ?>"  style="margin-bottom: 10px;" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$">
</div>
</div>
</div>
</div>
<div class="row">
<div class="col-md-12">
<div class="form-group form-group-default">
<label>Address</label>
<textarea name="address" placeholder="xxxxxxxx" style="margin-bottom: 10px;" class="form-control change_inputs"><?php echo $address; ?></textarea>
</div>
</div>
</div>
<div class="row">
<div class="col-md-6">
<div class="form-group form-group-default">
<label>Gender</label>
<div class="row">
<?php 
    if($gender == 'Male')
    {?>



      <div class="col-md-6" style="padding-left: 6px;">

        <label><span class="fa fa-male"></span> Male : <input type="radio" checked="checked" name="gender" value="Male" class="change_inputs"></label>
      </div>
      <div class="col-md-6" style="padding-left: 6px;">

        <label style="padding-left: 10px;margin-bottom: 10px;"><span class="fa fa-female"></span> Female : <input type="radio" name="gender" value="Female" class="change_inputs"> </label>

      </div>
      <?php
    }else
    if($gender == 'Female')
    {?>



      <div class="col-md-6" style="padding-left: 6px;"><label><span class="fa fa-male"></span> Male : <input type="radio"name="gender" value="Male" class="change_inputs"></label></div>

      <div class="col-md-6" style="padding-left: 6px;"><label style="padding-left: 10px;margin-bottom: 10px;"><span class="fa fa-female"></span> Female : <input type="radio" checked="checked"  name="gender" value="Female" class="change_inputs"> </label></div>
      <?php
    }
   ?>
   </div>
</div>
</div>
<div class="col-md-6">
<div class="form-group form-group-default">
  <label>Qualification</label>
  
  <textarea name="qualification" placeholder="xxxxxxxx" style="margin-bottom: 10px;" class="form-control change_inputs"><?php echo $qulification; ?></textarea>


</div>
</div>
</div>
<div class="clearfix"></div>
<div class="row m-t-25">
<div class="col-xl-6 p-b-10">
<p class="small-text hint-text">Click the Update button to change all the data you have changed. (ඔබ වෙනස් කල සියලු දත්තයන් වෙනස් කිරීමට Update බොත්තම ඔබන්න.)</p>
</div>
<div class="col-xl-6">
<button aria-label="" id="up_btn" class="btn btn-primary pull-right btn-lg btn-block" type="submit" name="update_teacher" value="<?php echo $teach_id; ?>">Update
</button>
</div>
</div>
</form>
</div>
</div>

</div>

<div class="col-lg-2"></div>


</div>
</div>
</div>

<div class="tab-pane slide-left <?php echo $bank_details; ?>" id="slide3">

<div class="col-md-12">
  <button type="button" class="btn btn-info btn-lg" style="margin-bottom:6px;" data-toggle="modal" data-target="#add_todo"><span class="fa fa-plus"></span>&nbsp;Add Bank Details</button>
</div>
<div class="row column-seperation">


<div class="col-lg-12">


   <div class="table-responsive" style="height:400px;overflow:auto;">
                <table class="table table-hover" id="tblFruits">
                  <thead>
                      <tr>
                        <th class="text-center" style="width: 1%;">#</th>
                        <th style="width: 1%;">Bank Name</th>
                        <th style="width: 1%;">Brance Name</th>
                        <th style="width: 1%;">Holder Name</th>
                        <th style="width: 1%;">Account No</th>
                        <th style="width: 1%;text-align: center;">Action</th>
                      </tr>
                    </thead>
                    <tbody>

                      <?php 
                       
                        $teach_id = $_SESSION['TEACH_ID'];

                        
                        $page_query = "SELECT * FROM `teacher_bank_details` WHERE `TEACH_ID` = '$teach_id' ORDER BY `T_B_D_ID` DESC";

                        $j = 0;
                        ;
                        $result = mysqli_query($conn, $page_query);

                          $check = mysqli_num_rows($result);

                          if($check>0)
                          {
                          while($row001 = mysqli_fetch_assoc($result))
                          {
                            $holder = $row001['HOLDER_NAME'];
                            $bank_name = $row001['BANK_NAME'];
                            $ac_no = $row001['ACCOUNT_NO'];

                            $branch = $row001['BRANCH_NAME'];
                            $tbd_id = $row001['T_B_D_ID'];

                            $j++;

                            echo '

                              <tr>
                                <td class="v-align-middle text-center">'.$j.'</td>
                                <td class="v-align-middle">'.$bank_name.'</td>
                                <td class="v-align-middle">'.$branch.'</td>
                                <td class="v-align-middle">'.$holder.'</td>
                                <td class="v-align-middle">'.$ac_no.'</td>
                                 <td class="v-align-middle text-center">

                                <button data-toggle="modal" data-target="#edit_todo'.$tbd_id.'" class="btn btn-success btn-xs btn-rounded mt-1 mr-1" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;" data-title="Approve" onclick="show_edit_bank_data('.$tbd_id.');"><i class="pg-icon">edit</i></button>
                                ' ?>
                                <a href="../teacher/query/delete.php?delete_bd=<?php echo $tbd_id; ?>&&teach_id=<?php echo $teacher_id; ?>" onclick="return confirm('Are you sure delete?')" class="btn btn-danger mt-1 mr-1 btn-xs btn-rounded" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;" data-title="Delete" data-toggle="tooltip"><i class="pg-icon">trash_alt</i></a>

                                <?php echo '

                                </td>
                              </tr>

                            ';
                        
                          }
                        }else
                        if($check == '0')
                        {
                          echo '<tr><td colspan="7" class="text-center text-danger"><span class="fa fa-warning"></span> Not Found Data</td></tr>';
                        } 
                      

                        

                        
                         ?>


                    </tbody>
                </table>
                </div>


</div>

<div class="col-lg-3">
<h3>
  
</h3>
</div>

</div>
</div>

</div>
</div>
</div>

</div>


    <script type="text/javascript">
      $(document).ready(function(){
        var pic = $('#pic2').val();
        if(pic == 'shop_img.png')
        {
          $('#file_upload').change(function(){
            $('#ok2').click();
          });
          $('#remove_btn2').hide();
        }else
        if(pic !== 'shop_img.png')
        {
          $('#file_upload').change(function(){
            $('#ok2').click();
          });
            $('#remove_btn2').show();
        }
        
      });
    </script>

  </section>

<script type="text/javascript">
$(document).ready(function(){  
  $('#availability6').hide();
   $('#cp').blur(function(){
     var cp = $(this).val();
     var id = $("#teacher_id99").val();
     if(cp=='')
     {
      $('#availability6').hide();
     }else
     {
     $.ajax({
      url:'../teacher/query/check.php',
      method:"POST",
      data:{cp:cp,id:id},
      success:function(data)
      {
        //alert(data);
         if(data>0)
         {
          $('#availability6').show();
          $('#availability6').html('<span class="text-success"><i class="fa fa-check"></i> Match Current Password</span>');
          $('#submit_btn').attr("disabled", false);
          $('#txtConfirmPassword').attr("disabled", false);
          $('#txtNewPassword').attr("disabled", false);
         }
         else
         if(data == 0)
         {
         $('#availability6').show();
          $('#availability6').html('<span class="text-danger"><i class="fa fa-times"></i> No Match Current Password</span>');
          $('#submit_btn').attr("disabled", true);
          $('#txtConfirmPassword').attr("disabled", true);
          $('#txtNewPassword').attr("disabled", true);
         }
      }
     })
   }

  });
 });

$(document).ready(function(){  
   $('#txtConfirmPassword').keyup(function(){
     var rp = $(this).val();
     var np = document.getElementById("txtNewPassword").value;
     // alert(cate);

       if(rp == np)
       {
        $('#availability7').html('<span class="text-success"><i class="fa fa-check"></i> Match New Password</span>');
        $('#submit_btn').attr("disabled", false);

       }else
       {
        $('#availability7').html('<span class="text-danger"><i class="fa fa-times"></i> No Match New Password</span>');
        $('#submit_btn').attr("disabled", true);
       }
      

  });
 });
</script>
<script type="text/javascript">

  function readURL2(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#show_image').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $("#imagefile").change(function(){
        readURL2(this);
    });

    
</script>

<script type="text/javascript">
  
  $(document).ready(function(){
    $('#upload').hide();

    $('#imagefile').change(function(){
      $('#upload').show();
    });
  });

</script>

<script type="text/javascript">
  
  $(document).ready(function(){
    $('#up_btn').prop('disabled',true);
    $('#up_btn').css('cursor','not-allowed');

    $('.change_inputs').change(function(){
      $('#up_btn').prop('disabled',false);
      $('#up_btn').css('cursor','pointer');
    });
  });

</script>

<script type="text/javascript">
  
  $(document).ready(function(){
    $('#submit_btn').prop('disabled',true);

    $('.change_inputs').change(function(){
      $('#submit_btn').prop('disabled',false);
    });
  });

</script>


<script type="text/javascript">
   $(document).ready(function() {
    $("#show_hide_password a").on('click', function(event) {
        event.preventDefault();
        if($('#show_hide_password input').attr("type") == "text"){
            $('#show_hide_password input').attr('type', 'password');
            $('#show_hide_password i').addClass( "fa-eye-slash" );
            $('#show_hide_password i').removeClass( "fa-eye" );
        }else if($('#show_hide_password input').attr("type") == "password"){
            $('#show_hide_password input').attr('type', 'text');
            $('#show_hide_password i').removeClass( "fa-eye-slash" );
            $('#show_hide_password i').addClass( "fa-eye" );
        }
    });


    $("#show_hide_password2 a").on('click', function(event) {
        event.preventDefault();
        if($('#show_hide_password2 input').attr("type") == "text"){
            $('#show_hide_password2 input').attr('type', 'password');
            $('#show_hide_password2 i').addClass( "fa-eye-slash" );
            $('#show_hide_password2 i').removeClass( "fa-eye" );
        }else if($('#show_hide_password2 input').attr("type") == "password"){
            $('#show_hide_password2 input').attr('type', 'text');
            $('#show_hide_password2 i').removeClass( "fa-eye-slash" );
            $('#show_hide_password2 i').addClass( "fa-eye" );
        }
    });


    $("#show_hide_password3 a").on('click', function(event) {
        event.preventDefault();
        if($('#show_hide_password3 input').attr("type") == "text"){
            $('#show_hide_password3 input').attr('type', 'password');
            $('#show_hide_password3 i').addClass( "fa-eye-slash" );
            $('#show_hide_password3 i').removeClass( "fa-eye" );
        }else if($('#show_hide_password3 input').attr("type") == "password"){
            $('#show_hide_password3 input').attr('type', 'text');
            $('#show_hide_password3 i').removeClass( "fa-eye-slash" );
            $('#show_hide_password3 i').addClass( "fa-eye" );
        }
    });

});
</script>


<?php  include('footer/footer.php'); ?>

<div id="add_todo" class="modal fade" role="dialog">
                            <div class="modal-dialog">

                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title">Create Bank Details</h4>
                                </div>
                                <form action="../teacher/query/insert.php" method="POST">
                                <div class="modal-body mt-2">


                                  <label class="mt-2">Account Holder Name</label>
                                  <textarea class="form-control" placeholder="Enter Account Holder Name" name="holder_name" rows="2" style="resize:none" required></textarea>


                                  <label class="mt-2">Account Number</label>
                                  <input type="text" name="ac_no" class="form-control" placeholder="Enter Account No.." required>

                                  <label class="mt-2">Bank Name</label>
                                  <input list="bank_data" id="search-table" name="bank_name" class="form-control" placeholder="Type Bank Name.." >

                                  <datalist id="bank_data">
                                    <?php 

                                      $sql002 = mysqli_query($conn,"SELECT * FROM `teacher_bank_details`");
                                      while($row002 = mysqli_fetch_assoc($sql002))
                                      {
                                        $bank_name = $row002['BANK_NAME'];
                                        
                                          echo '<option value="'.$bank_name.'">'.$bank_name.'</option>';

                                        
                                      }

                                     ?>
                                    
                                  </datalist>


                                  <label class="mt-2">Branch Name</label>
                                  <input list="branch_data" id="search-table" name="branch" class="form-control" placeholder="Type Branch Name.." >

                                  <datalist id="branch_data">
                                    <?php 

                                      $sql002 = mysqli_query($conn,"SELECT * FROM `teacher_bank_details`");
                                      while($row002 = mysqli_fetch_assoc($sql002))
                                      {
                                        $branch_name = $row002['BRANCH_NAME'];
                                        
                                          echo '<option value="'.$branch_name.'">'.$branch_name.'</option>';

                                        
                                      }

                                     ?>
                                    
                                  </datalist>

                                  <input type="hidden" name="teacher_id" value="<?php echo $_SESSION['TEACH_ID']; ?>">

                                </div>
                                <div class="modal-footer">
                                  <button type="submit" class="btn btn-success" name="create_bank_data" value="<?php echo $page; ?>"><span class="fa fa-check-circle"></span>&nbsp;Submit</button>
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                                </form>
                              </div>

                            </div>
                          </div>


                  <script type="text/javascript">
                    function show_edit_bank_data(tbd_id)
                    {
                      $.ajax({
                       type: "POST",
                       data: {check_update_bank_details:tbd_id},
                       url: "../teacher/query/check.php",
                       success: function(msg){
                         //alert(msg)
                         document.getElementById('open_modal_btn').click();
                         document.getElementById('show_edit_bank_data').innerHTML = msg;
                       }
                    
                      });
                    }
                  </script>

                  <button type="button" style="display: none;" data-toggle="modal" data-target="#open_bank_data" id="open_modal_btn">X</button>

                  <div id="open_bank_data" class="modal fade" role="dialog">
                      <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Update Bank Details</h4>
                          </div>
                          <div id="show_edit_bank_data"></div>
                        </div>

                      </div>
                    </div>