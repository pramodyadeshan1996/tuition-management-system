
<?php

$page = "Create Administrators";
$folder_in = '0';

  include('header/header.php'); 
 ini_set( "display_errors", 0); 


  ?>
  <style type="text/css">
    .btn_pulse {
  
  display: block;
  border-radius: 10%;
  cursor: pointer;
  animation: none;
  float: left;
  bottom: 5px;
  right: 5px;
  font-weight: bold;
  padding-top: 2px;
  text-align: center;
  color: white;
  font-size: 14px;
  animation: pulse 1.6s infinite;
}


@keyframes pulse {
  0% {
    -moz-box-shadow: 0 0 0 0 #7252d3;
    box-shadow: 0 0 0 0 #7252d3;
  }
  70% {
    -moz-box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
  }
  100% {
    -moz-box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
  }
}
  </style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<div class="row" style="border-bottom: 1px solid #cccc;margin-top: 60px;">
  <div class="col-md-11">
    <ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-bottom: 04%;margin-bottom: 0;">
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="Create Administrators"> Create Administrators</li>
    </ol>
  </div>
  <div class="col-md-1">
        <div class="row">
          <divc class="col-md-5"><a data-toggle="tooltip" data-title="Create Administrators" data-placement="right"><button class="btn btn-primary btn-lg btn-rounded btn_pulse pull-right" data-toggle="modal" data-target="#create_admin" style="padding: 10px 10px;margin-top: 10px;"><i class="pg-icon">add</i></button></a></div>
          <divc class="col-md-7"></div>
        </div>



         <div class="modal fade slide-up disable-scroll" id="create_admin" tabindex="-1" role="dialog" aria-hidden="false" style="overflow: auto;">
          <div class="modal-dialog ">
          <div class="modal-content-wrapper">
          <div class="modal-content">
          <div class="modal-header clearfix text-left">
          <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
          </button>
          <h5>Create Administrators</h5>
          </div>
          <div class="modal-body">
            <form action="../admin/query/insert.php" method="POST">

            <div class="row">
            <div class="col-md-6">
            <div class="form-group form-group-default input-group">
            <div class="form-input-group" style="height: 62px;">
            <label>Full Name</label>
            <input type="text" name="full_name" class="form-control change_inputs" pattern="[a-zA-Z. ]+" placeholder="XXXXXXXX"  required>
            </div>
            </div>
            </div>

            <div class="col-md-6">
            <div class="form-group form-group-default input-group">
            <div class="form-input-group">
            <label>Gender</label>

            <div class="row">
              
              <div class="col-md-6" style="padding-left: 6px;">
                <label><span class="fa fa-male"></span> Male : <input type="radio" checked  name="gender" value="Male" class="change_inputs"></label>
              </div>

              <div class="col-md-6" style="padding-left: 6px;">
                <label style="padding-left: 10px;margin-bottom: 10px;"><span class="fa fa-female"></span> Female : <input type="radio" name="gender" value="Female" class="change_inputs"> </label>
              </div>
                  
            </div>

            </div>
            </div>
            </div>

            </div>


            <div class="row">
            <div class="col-md-6">
            <div class="form-group form-group-default input-group">
            <div class="form-input-group">
            <label>Username</label>
            <input type="text" name="username" class="form-control change_inputs" placeholder="XXXXXXXX"  required>
            </div>
            </div>
            </div>

            <div class="col-md-6">
              <div class="form-group form-group-default input-group">
              <div class="form-input-group" id="show_hide_password" style="position: relative;">
             
              <label>Password</label>
              <input type="password" name="password" placeholder="XXXXXXXX" class="form-control" required>

              <a href=""><i class="fa fa-eye-slash" aria-hidden="true" style="position: absolute;right:10px;top:25px;"></i></a>

              </div>
            </div>
            </div>

            </div>

            <button type="submit" class="btn btn-success btn-block btn-lg" name="add_admin"><i class="pg-icon">add</i> Add</button>
            </form>
            </div>
            </div>
            </div>
            </div>

            </div>
            

    </div>
  </div>
</div>




<div class="col-md-12" style="margin-top: 0;">
  <h3 style="text-transform: capitalize;"><a href="dashboard.php" data-toggle="tooltip" data-title="Back"><i class="pg-icon" style="font-size: 22px;">chevron_left</i></a> Create Administrators</h3>

<div class=" container-fluid   container-fixed-lg bg-white">
<div class="card card-transparent">
<div class="card-header ">
<div class="card-title">
</div>
<div class="pull-right">
<div class="col-xs-12">
<input type="text" id="search" class="form-control pull-right" placeholder="Search" data-toggle="tooltip" data-title="අවශ්‍ය දත්ත සෙවීම'">
</div>
</div>
<div class="clearfix"></div>
</div>

<div class="card-body table-responsive" style="height: 500px;overflow: auto;margin-bottom: 0;">
  <table class="table demo-table-search table-responsive-block text-left" id="tableWithSearch">
  <thead>

    <th>Registered Date</th>
    <th style="width: 15%;text-align: center;">Picture</th>
    <th>Administrator Name</th>
    <th>Gender</th>
    <th style="width: 10%;">Status</th>
    <th class="text-center">Action</th>

  </thead>
  <tbody id="myTable">

    <tr class="no-data alert alert-danger" style="margin-top: 20px;display: none;">
      <td colspan="6" class="text-danger"><span class="fa fa-warning"></span> Not Found Data</td>
    </tr>
      
   <?php 

          $sql001 = mysqli_query($conn,"SELECT * FROM admin_login WHERE TYPE = 'ADMIN' ORDER BY REG_DATE DESC");
          $ch = mysqli_num_rows($sql001);
          if($ch > 0)
          {
                while($row = mysqli_fetch_assoc($sql001))
                {

                      
                    $admin_id = $row['ADMIN_ID'];
                    $name = $row['ADMIN_NAME'];
                    $gender = $row['GENDER'];
                    $reg_date = $row['REG_DATE'];
                    $status = $row['STATUS'];
                    $picture = $row['PICTURE'];

                    if($picture == '0')
                   {
                      if($gender == 'Female')
                      {
                        $picture = 'female_admin.png';
                      }else
                      if($gender == 'Male')
                      {
                        $picture = 'male_admin.jpg';
                      }
                   }



                    if($status == 'Active')
                    {
                      $alert = 'success';
                      $icon = 'check-circle';
                      $btn = '<a href="../admin/query/update.php?status_admin_data='.$admin_id.'&&status=Active" class="btn btn-danger btn-rounded btn-xs" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;" data-toggle="tooltip" data-title="Deactive Administrator"><i class="pg-icon">close</i></a>';
                    }else
                    if($status == 'Deactive')
                    {
                      $alert = 'danger';
                      $icon = 'times-circle';
                      $btn = '<a href="../admin/query/update.php?status_admin_data='.$admin_id.'&&status=Deactive" class="btn btn-success btn-rounded btn-xs" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;" data-toggle="tooltip" data-title="Active Administrator"><i class="pg-icon">tick_circle</i></a>';
                    }
                    
                    
                    echo '<tr>';

                    echo '
                          <td class="v-align-middle">'.$reg_date.'</td>
                          <td class="v-align-middle"><center><img src="../admin/images/profile/'.$picture.'" class="rounded-circle image-responsive" id="show_image" style="height: auto;border:1px solid #cccc;width: 78%;border: 10px solid white;"></center></td>
                          <td class="v-align-middle bold">'.$name.'</td>
                          <td class="v-align-middle bold">'.$gender.'</td>
                          <td class="v-align-middle bold"><label class="label label-'.$alert.'"><span class="fa fa-'.$icon.'"></span> '.$status.'</label></td>
                          <td class="v-align-middle text-center">

                              '.$btn.'
                          ';


                          echo '

                                      <a data-toggle="tooltip" data-title="Edit Admin Data"><button type="button" class="btn btn-complete  btn-xs btn-rounded" data-target="#update_admin'.$admin_id.'" data-toggle="modal" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;margin-right:10px;margin-left:10px;"><i class="pg-icon">edit</i></button></a>';
                                        
                                        ?>
                                          <a href="../admin/query/delete.php?delete_admin_data=<?php echo $admin_id; ?>" onclick="return confirm('Are you sure Permanently Delete?')" class="btn btn-danger btn-rounded btn-xs" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;" data-toggle="tooltip" data-title="Delete Class"><i class="pg-icon">trash_alt</i></a>

                                          <div class="modal fade slide-up disable-scroll" id="update_admin<?php echo $admin_id; ?>" tabindex="-1" role="dialog" aria-hidden="false" style="overflow: auto;">
                                            <div class="modal-dialog ">
                                            <div class="modal-content-wrapper">
                                            <div class="modal-content">
                                            <div class="modal-header clearfix text-left">
                                            <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
                                            </button>
                                            <h5>Edit Administrator Details</h5>
                                            </div>
                                            <div class="modal-body">

                                              <form action="../admin/query/update.php" method="POST">

                                              <div class="row">
                                              <div class="col-md-6">
                                              <div class="form-group form-group-default input-group">
                                              <div class="form-input-group" style="height: 62px;">
                                              <label class="pull-left">Full Name</label>
                                              <input type="text" name="full_name" class="form-control change_inputs" pattern="[a-zA-Z. ]+" placeholder="XXXXXXXX" value="<?php echo $name; ?>" required>
                                              </div>
                                              </div>
                                              </div>

                                              <div class="col-md-6">
                                              <div class="form-group form-group-default input-group">
                                              <div class="form-input-group">
                                              <label class="pull-left">Gender</label>
                                              <br>
                                              <div class="row">
                                                <?php 

                                                  if($gender == 'Male')
                                                  {?>

                                                    <div class="col-md-6" style="padding-left: 6px;">
                                                      <label class="pull-left"><span class="fa fa-male"></span> Male : <input type="radio" checked  name="gender" value="Male" class="change_inputs"></label>
                                                    </div>

                                                    <div class="col-md-6" style="padding-left: 6px;">
                                                      <label class="pull-left" style="padding-left: 10px;margin-bottom: 10px;"><span class="fa fa-female"></span> Female : <input type="radio" name="gender" value="Female" class="change_inputs"> </label>
                                                    </div>

                                                  <?php 
                                                  }else
                                                  if($gender == 'Female')
                                                  {?>

                                                    <div class="col-md-6" style="padding-left: 6px;">
                                                      <label class="pull-left"><span class="fa fa-male"></span> Male : <input type="radio" name="gender" value="Male" class="change_inputs"></label>
                                                    </div>

                                                    <div class="col-md-6" style="padding-left: 6px;">
                                                      <label class="pull-left" style="padding-left: 10px;margin-bottom: 10px;"><span class="fa fa-female"></span> Female : <input type="radio" name="gender" value="Female" class="change_inputs" checked> </label>
                                                    </div>

                                                  <?php 
                                                  }

                                                 ?>
                                                
                                                    
                                              </div>

                                              </div>
                                              </div>
                                              </div>

                                              </div>

                                              <button type="submit" class="btn btn-success btn-block btn-lg" name="update_admin_data" value="<?php echo $admin_id; ?>"><i class="pg-icon">edit</i> Update</button>
                                              </form>


                                            </div>
                                            </div>
                                            </div>
                                            </div>

                                            </div>

                                          <?php


                          
                        }
                      }else
                      if( $ch == '0')
                      {
                          echo '<tr><td colspan="6" class="text-danger text-center"><span class="fa fa-warning text-danger"></span> Empty Data!</td></tr>';
                      }
 ?>
    
    </tr>
  </tbody>
  </table>
</div>
</div>

</div>

<script type="text/javascript">
  
    $(document).ready(function(){  
     $('#level_clz').change(function(){

       var level_clz = $(this).val();
       var teach_id = $('#teach_id').val();

       $.ajax({
        url:'../teacher/query/check.php',
        method:"POST",
        data:{create_clz:level_clz,teach_id:teach_id},
        success:function(data)
        {
          //alert(data)
            $('#subject').html(data);
          
        }
       })
     

    });

     
   });

  </script>

<script type="text/javascript">
	setInterval(function() {
    $('blink').fadeIn(1300).fadeOut(1500);
}, 1000);
</script>

<script>
$(document).ready(function(){
  $("#search").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $(".myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

<script type="text/javascript">
  $(document).ready(function () {

    (function ($) {

        $('#search').keyup(function () {
            var rex = new RegExp($(this).val(), 'i');
            $('#myTable tr').hide();
            $('#myTable tr').filter(function () {
                return rex.test($(this).text());
            }).show();
            $('.no-data').hide();
            if($('#myTable tr:visible').length == 0)
            {
                $('.no-data').show();
            }

        })

    }(jQuery));

});
</script>

<script>
$(document).ready(function(){

  $("#up_rec").on("change", function() {
      $('.show_image').hide();
  });
});
</script>
<script type="text/javascript">
  setInterval(function() {
    $('.blink').fadeIn(1300).fadeOut(1500);
}, 1000);
</script>

<script type="text/javascript">
  $("#show_hide_password a").on('click', function(event) {
        event.preventDefault();
        if($('#show_hide_password input').attr("type") == "text"){
            $('#show_hide_password input').attr('type', 'password');
            $('#show_hide_password i').addClass( "fa-eye-slash" );
            $('#show_hide_password i').removeClass( "fa-eye" );
        }else if($('#show_hide_password input').attr("type") == "password"){
            $('#show_hide_password input').attr('type', 'text');
            $('#show_hide_password i').removeClass( "fa-eye-slash" );
            $('#show_hide_password i').addClass( "fa-eye" );
        }
    });
</script>

<?php  include('footer/footer.php'); ?>


