<?php 
	$page = 'index';
	include('header.php');

 ?>
 <style type="text/css">
   .latest__item2:hover
   {
    transition: 2s;
    background-color: #2f0173;
    background-image: none;
   }


 </style>
      

<section class="hero">



<div class="hero__slider owl-carousel">

<div class="hero__item set-bg"  data-setbg="img/hero/hero-2.jpg" style="background-size: cover;height: 820px;">

<div class="container">
  
<div class="row">
<div class="col-lg-6">
<div class="hero__text">
  
    
<span>For Student & Teachers</span>
<h2>Online <br>Education</h2>

<h3 style="color: white;margin-bottom: 20px;">ඔන්ලයින් නව පන්ති ඇරඹේ!</h3>

<span style="color: white;padding: 0px;margin: 4px;padding-bottom: 10px;" class="text-danger">අලුතින් ලියාපදිංචි වීමට Register Now තෝරා ගන්න.</span>


<a href="../student/login/register.php" class="btn btn-success" style="margin-top: 0px;background-color: #e74c3c;color: white;"><i class="fa fa-check"></i> Register Now</a>

<span style="color: white;padding-top: 20px;margin: 4px;padding-bottom: 10px;" class="text-success">පංතියට සහභාහී වීමට Join Class Now තෝරා ගන්න.</span>


<a href="../student/login/index.php" class="btn btn-success" style="background-color: #2ecc71;color: white;margin-top: 0px;"><i class="fa fa-angle-double-right"></i> Join Class Room</a>

</div>
</div>

</div>
</div>
</div>
<div class="hero__item set-bg" data-setbg="img/hero/hero-2.jpg" style="background-size: cover;height: 820px;">
<div class="container">
<div class="row">
<div class="col-lg-6">
<div class="hero__text">
<span>For Students</span>
<h2>Online <br>Examination</h2>

<h3 style="color: white;margin-bottom: 20px;">ඔන්ලයින් නව පන්ති ඇරඹේ!</h3>

<span style="color: white;padding: 0px;margin: 4px;padding-bottom: 10px;" class="text-danger">අලුතින් ලියාපදිංචි වීමට Register Now තෝරා ගන්න.</span>


<a href="../student/login/register.php" class="btn btn-success" style="margin-top: 0px;background-color: #e74c3c;color: white;"><i class="fa fa-check"></i> Register Now</a>

<span style="color: white;padding-top: 20px;margin: 4px;padding-bottom: 10px;" class="text-success">පංතියට සහභාහී වීමට Join Class Now තෝරා ගන්න.</span>


<a href="../student/login/index.php" class="btn btn-success" style="background-color: #2ecc71;color: white;margin-top: 0px;"><i class="fa fa-angle-double-right"></i> Join Class Room</a>

</div>
</div>

</div>
</div>
</div>

<div class="hero__item set-bg" data-setbg="img/hero/hero-2.jpg" style="background-size: cover;height: 820px;">
<div class="container">
<div class="row">
<div class="col-lg-6">
<div class="hero__text">
<span>For Student</span>
<h2>LMS <br> System</h2>

<h3 style="color: white;margin-bottom: 20px;">ඔන්ලයින් නව පන්ති ඇරඹේ!</h3>

<span style="color: white;padding: 0px;margin: 4px;padding-bottom: 10px;" class="text-danger">අලුතින් ලියාපදිංචි වීමට Register Now තෝරා ගන්න.</span>


<a href="../student/login/register.php" class="btn btn-success" style="margin-top: 0px;background-color: #e74c3c;color: white;"><i class="fa fa-check"></i> Register Now</a>

<span style="color: white;padding-top: 20px;margin: 4px;padding-bottom: 10px;" class="text-success">පංතියට සහභාහී වීමට Join Class Now තෝරා ගන්න.</span>


<a href="../student/login/index.php" class="btn btn-success" style="background-color: #2ecc71;color: white;margin-top: 0px;"><i class="fa fa-angle-double-right"></i> Join Class Room</a>

</div>
</div>
</div>
</div>
</div>

<div class="hero__item set-bg" data-setbg="img/hero/hero-2.jpg" style="background-size: cover;height: 820px;">
<div class="container">
<div class="row">
<div class="col-lg-6">
<div class="hero__text">
<span>For Teacher</span>
<h2>Free <br>Zoom Meeting</h2>

<h3 style="color: white;margin-bottom: 20px;">ඔන්ලයින් නව පන්ති ඇරඹේ!</h3>

<span style="color: white;padding: 0px;margin: 4px;padding-bottom: 10px;" class="text-danger">අලුතින් ලියාපදිංචි වීමට Register Now තෝරා ගන්න.</span>


<a href="../student/login/register.php" class="btn btn-success" style="margin-top: 0px;background-color: #e74c3c;color: white;"><i class="fa fa-check"></i> Register Now</a>

<span style="color: white;padding-top: 20px;margin: 4px;padding-bottom: 10px;" class="text-success">පංතියට සහභාහී වීමට Join Class Now තෝරා ගන්න.</span>


<a href="../student/login/index.php" class="btn btn-success" style="background-color: #2ecc71;color: white;margin-top: 0px;"><i class="fa fa-angle-double-right"></i> Join Class Room</a>

</div>
</div>
</div>
</div>
</div>

<div class="hero__item set-bg" data-setbg="img/hero/hero-2.jpg" style="background-size: cover;height: 820px;">
<div class="container">
<div class="row">
<div class="col-lg-6">
<div class="hero__text">
<span>For Student & Teacher</span>
<h2>Free Card,<br> Class Facilities</h2>

<h3 style="color: white;margin-bottom: 20px;">ඔන්ලයින් නව පන්ති ඇරඹේ!</h3>

<span style="color: white;padding: 0px;margin: 4px;padding-bottom: 10px;" class="text-danger">අලුතින් ලියාපදිංචි වීමට Register Now තෝරා ගන්න.</span>


<a href="../student/login/register.php" class="btn btn-success" style="margin-top: 0px;background-color: #e74c3c;color: white;"><i class="fa fa-check"></i> Register Now</a>

<span style="color: white;padding-top: 20px;margin: 4px;padding-bottom: 10px;" class="text-success">පංතියට සහභාහී වීමට Join Class Now තෝරා ගන්න.</span>


<a href="../student/login/index.php" class="btn btn-success" style="background-color: #2ecc71;color: white;margin-top: 0px;"><i class="fa fa-angle-double-right"></i> Join Class Room</a>

</div>
</div>
</div>
</div>
</div>



</div>


<?php  

    $sql006 = mysqli_query($conn,"SELECT * FROM `seminar_master` WHERE `PUBLISH` = '1' ORDER BY `ADD_DATE` DESC");
    
    $check = mysqli_num_rows($sql006);

    if($check > 0)
    {
      echo '<section class="latest spad">';
    }else
    if($check == '0')
    {
      echo '<section class="latest spad" style="display:none;">';
    }
   ?>


<div class="container">
<div class="row">
<div class="col-lg-12">
<div class="section-title center-title">
<span>Our Seminar</span>
<h2>Seminar Introduction</h2>


</div>
</div>
</div>
<div class="row">
<div class="latest__slider owl-carousel">


    <?php   

      //show Query above

      while($row006 = mysqli_fetch_assoc($sql006))
      {
        $semi_id = $row006['SEMI_ID'];
        $teach_id = $row006['TEACH_ID'];
        $register_date = $row006['ADD_TIME'];
        $access_count = $row006['ATTEND_COUNT'];
        $semi_code = $row006['SEM_CODE'];
        $semi_day = $row006['DAY'];
        $due_date = $row006['DUE_DATE'];
        $seminar_name = $row006['SEM_NAME'];
        $semi_link = $row006['SEMINAR_LINK'];

        $post_name = $row006['POST_NAME'];
        $post_image = $row006['POST_IMG'];
        $wh_link = $row006['WHATSAPP_LINK'];


        $start_time = $row006['START_TIME']; //Class Start time
        $end_time = $row006['END_TIME']; //Class End time

        $str_date = strtotime($due_date);
        $sem_date = date('Y-M-d',$str_date);

        $str = strtotime($start_time);
        $class_start_time = date('h:i A',$str);

        $str2 = strtotime($end_time);
        $class_end_time = date('h:i A',$str2);

        $due_time = $class_start_time." - ".$class_end_time;


        ?>

          <div class="col-lg-4" oncontextmenu="return false">
          
          <div class="latest__item2" style="padding:4px 4px 4px 4px;border:1px solid gray;">

          <img src="post/<?php echo $post_image; ?>" style="margin-bottom:2px;width: 100%;height: auto; ">

          <div style="padding: 6px 6px 6px 6px;">
            <h4 style="color: white;margin-top:10px;text-align:left;text-transform: capitalize;"><?php echo $post_name ?></h4>
            <small style="color: white;margin-top:10px;text-align:left;font-size: 16px;"><?php echo $sem_date ?> <?php  echo $due_time; ?>
              
              <label class="badge badge-success" style="color: white;margin-top:10px;text-align:left;background-color: #e67e22;width: 40px;text-align: center;border-radius: 80px;">Free</label>

            </small>
            


          </div>

          <?php 

          $current_time = date('Y-m-d H:i:s');

          $over_time = $due_date.' '.$end_time;

          if($over_time<$current_time)
          {

           ?>

          <center>
            <label class="badge badge-danger" style="border-radius: 80px;color: white;font-weight: 600;width: 100%;margin-top: 6px;"><i class="fa fa-check-circle"></i> අප ආයතනය මගින් පැවැත්වු <br> මෙම සම්මන්ත්‍රය සාර්ථකව <br> අවසන් කර ඇත.</label>
            <br>
            <span class="fa fa-check-circle" style="color:white;font-size: 32px;margin-top: 4px;margin-bottom: 10px;"></span>
          </center>

          

          <?php 

          }else
          if($over_time>=$current_time)
          {

            ?>
          <center>
            <a href="<?php echo $wh_link; ?>" target="_blank" style="cursor: pointer;">
                  <label class="badge badge-success" style="background-color:#2ecc71;padding:7px 8px 6px 8px;border-radius: 80px;color: white;font-weight: 600;margin-top: 0px;cursor: pointer;font-size: 14px;width: 85%;"><i class="fa fa-whatsapp"></i> අපගේ ගෘප් එකට සම්බන්ධවීම</label>
                </a>

                <a href="../student/seminar_login/index.php?sem_code=<?php echo $semi_code; ?>" style="cursor: pointer;">
                  <label class="badge badge-success" style="font-size: 14px;background-color:#3498db;padding:7px 8px 6px 8px;border-radius: 80px;color: white;font-weight: 600;width: 85%;margin-top: 10px;cursor: pointer;"><span class="fa fa-lock"></span> සම්මන්ත්‍රණය සදහා ලියාපදිංචි වීම</label>
                </a>
                
                <img src="img/loading.gif" style="width:40px;height:40px;">
            </center>
          <?php 
  
          }

          ?>
                

          </div>
        
        </div>


        <?php

      }

      //Show query above

     ?>


</div>
</div>
</div>
</section>


  <section class="latest spad" style="padding-top:40px;">
  <div class="container">
  <div class="row">
  <div class="col-lg-12">
  <div class="section-title center-title">
  <span>Our Videos</span>
  <h2>Video Introduction</h2>


  </div>
  </div>
  </div>
  <div class="row">

    <div class="col-lg-4" style="margin-top:10px;">
          
      <div class="col-lg-12" style="border:1px solid #34495e;padding: 20px 15px 20px 15px;border-radius: 10px;">

            <iframe width="100%" height="250px" src="https://www.youtube.com/embed/maBGGKH13Bo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          
            <h4 style="color: white;margin-top:4px;text-align:left;"><span class="fa fa-check-circle"></span> Introduction to Our Institute</h4>

            <small style="color: #cccc;">Sipsatha Online Educational Institute</small>
       
      </div>
    </div>

    <div class="col-lg-4" style="margin-top:10px;">
          
      <div class="col-lg-12" style="border:1px solid #34495e;padding: 20px 15px 20px 15px;border-radius: 10px;">

        
          
            <iframe width="100%" height="250px" src="https://www.youtube.com/embed/gmS71hlq6Tc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          
            <h4 style="color: white;margin-top:4px;text-align:left;"><span class="fa fa-check-circle"></span> How to Register</h4>
            <small style="color: #cccc;">Sipsatha Online Educational Institute</small>
        
      </div>
    </div>
            
    
    <div class="col-lg-4" style="margin-top:10px;">
          
      <div class="col-lg-12" style="border:1px solid #34495e;padding: 20px 15px 20px 15px;border-radius: 10px;">

            <iframe width="100%" height="250px" src="https://www.youtube.com/embed/4fbtJ7Xm_jA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          
            <h4 style="color: white;margin-top:4px;text-align:left;"><span class="fa fa-check-circle"></span> How to Join a Class</h4>

            <small style="color: #cccc;">Sipsatha Online Educational Institute</small>
       
      </div>
    </div>

  </div>
  </div>
  </section>



<section class="latest spad" style="padding-top:0px;">
<div class="container">
<div class="row">
<div class="col-lg-12">
<div class="section-title center-title">
<span>Our Teachers</span>
<h2>Teacher Introduction</h2>


</div>
</div>
</div>
<div class="row">
<div class="latest__slider owl-carousel">


<?php  



    $sql001 = "SELECT * FROM `teacher_details` order by `TEACH_ID` DESC";


    $sql004 = mysqli_query($conn, $sql001);

    $check = mysqli_num_rows($sql004);

      while($row004 = mysqli_fetch_assoc($sql004))
      {
        $name = $row004['POSITION'].". ".$row004['F_NAME']." ".$row004['L_NAME'];
        $q = $row004['QUALIFICATION'];
        if($q == '')
        {
        	$q = 'Teacher';
        }

        $teach_id = $row004['TEACH_ID'];

        $picture = $row004['PICTURE'];
        $gender = $row004['GENDER'];
        if($picture == '0')
         {
            if($gender == 'Male')
            {
              $picture = 'b_teacher.jpg';
            }else
            if($gender == 'Female')
            {
              $picture = 'g_teacher.jpg';
            }
         }

        $sql005 = mysqli_query($conn,"SELECT * FROM `teacher_login` WHERE `TEACH_ID` = '$teach_id' AND `CONFIRM` = '1'");

        $check2 = mysqli_num_rows($sql005);

        if($check2 > 0)
        {
          //teacher profile image size = Width - 343px , Height - 240px

            echo '

              <div class="col-lg-4" oncontextmenu="return false">
              	<a href="teacher_profile.php?teach_id='.$teach_id.'">
					<div class="blog__item latest__item2">

					<center><div class="portfolio__item__video set-bg" data-setbg="../teacher/images/profile/'.$picture.'" style="margin-bottom:2px;"></div>
					<h4 style="color: white;margin-top:10px;text-align:center;">'.$name.'</h4>


					<p style="margin-top: 10px;text-align:center;">'.$q.'</p></center>
					</div>
				</a>
				</div>

            ';
          //teacher profile image size = Width - 343px , Height - 240px
          }
      }
?>
</div>
</div>
</div>
</section>


<!-- <section class="latest spad">
<div class="container">
<div class="row">
<div class="col-lg-12">
<div class="section-title center-title">
<span>Our Teachers</span>
<h2>Classes Update</h2>
</div>
</div>
</div>
<div class="row">
<div class="latest__slider owl-carousel">

<div class="col-lg-4">

	<img src="img/2.jpeg" style="height: auto;background-size: cover;width: 100%;height: 550px;">

</div>

<div class="col-lg-4">

	<img src="img/2.jpeg" style="height: auto;background-size: cover;width: 100%;height: 550px;">

</div>

<div class="col-lg-4">

	<img src="img/2.jpeg" style="height: auto;background-size: cover;width: 100%;height: 550px;">

</div>

</div>
</div>
</div>
</section> -->

<?php 
	
	//Student
	$sql001 = mysqli_query($conn,"SELECT * FROM `stu_login` WHERE `STATUS` = 'Active'");
	$am_student = 200+mysqli_num_rows($sql001);

	//Teachers
	$sql002 = mysqli_query($conn,"SELECT * FROM `teacher_login` WHERE `CONFIRM` = '1'");
	$am_teacher = mysqli_num_rows($sql002);

	//Classes
	$sql003 = mysqli_query($conn,"SELECT * FROM `classes`");
	$am_classes = mysqli_num_rows($sql003);


	//subject
	$sql004 = mysqli_query($conn,"SELECT * FROM `subject`");
	$am_subject = mysqli_num_rows($sql004);

 ?>

<section class="counter">
<div class="container">
<div class="counter__content">
<div class="row">
<div class="col-lg-3 col-md-6 col-sm-6">
<div class="counter__item">
<div class="counter__item__text">
<img src="img/icons/xci-1.png.pagespeed.ic.J-FtZGQORM.png" alt="">
<h2 class="counter_num"><?php echo number_format($am_subject) ?></h2>
<p>Subjects</p>
</div>
</div>
</div>
<div class="col-lg-3 col-md-6 col-sm-6">
<div class="counter__item second__item">
<div class="counter__item__text">
<img src="img/icons/xci-2.png.pagespeed.ic.gvchDg6Tkl.png" alt="">
<h2 class="counter_num"><?php echo number_format($am_student) ?></h2>
<p>Students</p>
</div>
</div>
</div>
<div class="col-lg-3 col-md-6 col-sm-6">
<div class="counter__item third__item">
<div class="counter__item__text">
<img src="img/icons/ci-3.png" alt="">
<h2 class="counter_num"><?php echo number_format($am_teacher) ?></h2>
<p>Teachers</p>
</div>
</div>
</div>
<div class="col-lg-3 col-md-6 col-sm-6">
<div class="counter__item four__item">
<div class="counter__item__text">
<img src="img/icons/ci-4.png" alt="">
<h2 class="counter_num"><?php echo number_format($am_classes) ?></h2>
<p>Classes</p>
</div>
</div>
</div>
</div>
</div>
</div>
</section>

<section class="contact-widget spad">
<div class="container">
<div class="row">
<div class="col-lg-4 col-md-6 col-md-6 col-md-3">
<div class="contact__widget__item">
<div class="contact__widget__item__icon">
<i class="fa fa-map-marker"></i>
</div>
<div class="contact__widget__item__text">
<h4>Address</h4>
<p><b style="text-transform: uppercase;font-size: 14px;">Interlink Business Solutions</b>, <br> No 06 , Kumaradasa Mawatha, Matara.</p>
</div>
</div>
</div>
<div class="col-lg-4 col-md-6 col-md-6 col-md-3">
<div class="contact__widget__item">
<div class="contact__widget__item__icon">
<i class="fa fa-phone"></i>
</div>
<div class="contact__widget__item__text">
<h4>Hotline</h4>
<p>0777 650 121 • 041 563 9635 <br>• 071 699 2994</p>
</div>
</div>
</div>
<div class="col-lg-4 col-md-6 col-md-6 col-md-3">
<div class="contact__widget__item">
<div class="contact__widget__item__icon">
<i class="fa fa-whatsapp"></i>
</div>
<div class="contact__widget__item__text">
<h4>Whatsapp</h4>
<p>0777 650 121</p>
</div>
</div>
</div>
</div>
</div>
</section>


<section class="contact spad">
<div class="container">
<div class="row">
<div class="col-lg-12 col-md-6">
<div class="contact__map">
<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15873.100180672482!2d80.5371532!3d5.9567935!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xbdb07c6809fd6eb8!2sINTERLINK%20BUSINESS%20SOLUTIONS!5e0!3m2!1sen!2slk!4v1620978775168!5m2!1sen!2slk" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
</div>
</div>
</div>
</div>
</section>

<?php 
	include('footer.php');

 ?>


<script>
  (function(){
  var site_visit = 0; // need to assign data from database
  var post_visit = 0; // need to assign data from database
  var values = [];
  var now = new Date();
  var isExpired = now.setHours(now.getHours() + 24); //one day from now
  var post_id = 0; //need to assign post id for post detail page

if (typeof Storage !== "undefined") {
  if (!localStorage.setTime) {
    localStorage.setTime = isExpired;
  }
  
  // localStorage.post_view_count;
  if(localStorage.post_view_count){
    values = localStorage.getItem('post_view_count').split(",");
  }
  
  
  // for website visit counter
  if (!localStorage.visited) {
    localStorage.setItem('siteVisitCount', site_visit + 1);
    // set update to DOM element
    //document.getElementById("result").innerHTML = 'Welcome, You visited this page '+ localStorage.siteVisitCount + 'time';
    var visit_count = 1;
     $.ajax({
          url:'../admin/query/check.php',
          method:"POST",
          data:{visit_count:visit_count},
          success:function(data)
          {
            //alert(data);
            
            
          }
         });


    // need to send updated value to database 
    localStorage.visited = true;
  }else{
    /*document.getElementById("result").innerHTML = 'You have visited here more then once within 24 hours';
    var visit_count = 1;
     $.ajax({
          url:'../admin/query/check.php',
          method:"POST",
          data:{visit_count:visit_count},
          success:function(data)
          {
            alert(data);
            
            
          }
         });*/
  }
  
  
  console.log(localStorage.siteVisitCount)

 
  // for post counter
  
if(values.length){
  for(var i =0; i< values.length; i++){
      if(values[i] != post_id){
        values.push(post_id);
        localStorage.setItem('post_view_count', values);
        // send post count update to database.......
        post_visit = post_visit + 1;
      }
      
  }
}else{
    values.push(post_id);
    localStorage.setItem('post_view_count', values);
    // send post count update to database here.......
    post_visit = post_visit + 1;

    
}


//check if expired

if (Number(localStorage.setTime) < new Date()) {
    localStorage.removeItem('visited');
  localStorage.removeItem('post_view_count');
}


  

}


})()
</script>