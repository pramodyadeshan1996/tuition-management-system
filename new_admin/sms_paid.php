<?php
	$page = "SMS Paid";
	$folder_in = '0';

      include('header/header.php');

        $s04 = mysqli_query($conn,"SELECT * FROM admin_login WHERE ADMIN_ID = '$admin_id'");
        while($row4 = mysqli_fetch_assoc($s04))
        {
          $name = $row4['ADMIN_NAME'];
        }

 ?>

 <style type="text/css">
   .count {
  
  animation: pulse 1s infinite;
}


@keyframes pulse {
  0% {
    -moz-box-shadow: 0 0 0 0 #ffd945;
    box-shadow: 0 0 0 0 #ffd945;
  }
  70% {
    -moz-box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
  }
  100% {
    -moz-box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
  }
}
 </style>

    <ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-top: 04%;margin-bottom: 0;">
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="SMS Main Menu"> <a href="sms.php">SMS Main Menu</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="SMS Paid"> SMS Paid</li>
    </ol>


  <div class="col-md-12">
    
    <div class="row" style="padding-top: 10px;">
      <div class="col-md-12 bg-white" style="border:1px solid #cccc;height: auto;">
      
      <div class="col-md-12" style="border-bottom: 1px solid #cccc;">
        <div class="row">

            <div class="col-md-9" style="margin-bottom: 10px;"><h2><a href="sms.php" style="color:black;"><span class="fa fa-angle-left"></span></a> SMS Paid</h2></div>
            <div class="col-md-3" style="float: right;"><?php 
                            $sql0021 = mysqli_query($conn,"SELECT * FROM `sms_counter`");

                            if(mysqli_num_rows($sql0021)>0)
                            { 

                              echo '<button class="btn btn-success pull-right" data-toggle="modal" data-target="#filter"  data-dismiss="modal" style="border-radius: 80px;padding:10px 10px 10px 10px;float: right;margin-top:10px;"><span class="fa fa-print" style="font-size: 19px;"></span></button>';

                            }else
                            if(mysqli_num_rows($sql0021)== '0')
                            { 
                              echo '<button class="btn btn-success disabled pull-right" style="border-radius: 80px;padding:10px 10px 10px 10px;cursor:not-allowed;float: right;margin-top:10px;"><span class="fa fa-print" style="font-size: 19px;"></span></button>';
                            }
                           ?></div>
        </div>

      </div>
          

<div class="card card-borderless">
<div class="col-md-12" style="padding-top:10px;">

<div class="tab-content">
<div class="tab-pane active" id="home">
<div class="row column-seperation">



<div class="col-lg-12">
  
  <div class="row">
    <div class="col-md-8">

      <div class="pull-left">
      </div>

    </div>
    <div class="col-md-4">
        <input type="text" id="myInput" class="form-control pull-right" style="width: 100%;" placeholder="Search" data-toggle="tooltip" data-title="අවශ්‍ය දත්ත සෙවීම'">
      
  </div>

</div>

<?php 

$sql00100 = mysqli_query($conn,"SELECT * FROM `sms_counter`");

$count = mysqli_num_rows($sql00100);

 ?>

<div class="table-responsive" style="height: 550px;overflow: auto;margin-top: 4px;">
<table class="table demo-table-search table-responsive-block text-left" id="tableWithSearch">
<thead>
<tr>
<th style="width:1%;">Pay Time</th>
<th style="width:1%;">Remaining SMS Amount <label class="label label-success"><?php echo  $count; ?></label></th>
<th style="width:1%;">Current SMS Payments (Rs)</th>
<th style="width:1%;">Last Remained SMS</th>
<th style="width:1%;">Added SMS Amount</th>
<th style="width:1%;" class="text-center">Added Payment (Rs)</th>
</tr>
</thead>
<tbody id="myTable">

<tr class="no-data alert alert-danger" style="margin-top: 20px;display: none;">
  <td colspan="6" class="text-danger"><span class="fa fa-warning"></span> Not Found Data</td>
</tr>

<?php 

  $sql001 = mysqli_query($conn,"SELECT * FROM `sms_counter` ORDER BY `SUB_SMS_C_ID` DESC");

  $check = mysqli_num_rows($sql001);

  if($check>0){
  while($row001 = mysqli_fetch_assoc($sql001))
  {
    $add_date = $row001['ADD_D_T'];
    $pay = $row001['PAYMENT'];
    $add_sms = $row001['ADD_SMS'];
    $remained_sms = $row001['REMAINED_SMS'];
    $current_sms = $row001['CURRENT_SMS'];
    $current_amount = $row001['CURRENT_AMOUNT'];


    echo '

      <tr>
        <td class="v-align-middle">'.$add_date.'</td>
        <td class="v-align-middle text-danger bold">'.number_format($current_sms).'</td>
        <td class="v-align-middle">'.number_format($current_amount,2).'</td>
        <td class="v-align-middle">'.number_format($remained_sms).'</td>
        <td class="v-align-middle">'.number_format($add_sms).'</td>
        <td class="v-align-middle text-center">'.number_format($pay,2).'</td>'; 
         ?>

                    <div class="modal fade slide-up disable-scroll" id="filter" tabindex="-1" role="dialog" aria-hidden="false">
                      <div class="modal-dialog ">
                      <div class="modal-content-wrapper">
                      <div class="modal-content">
                      <div class="modal-header clearfix text-left">
                      <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
                      </button>
                      <h5>Print SMS Payment Details</h5>
                      </div>
                      <div class="modal-body">
                          <div class="col-md-12">
                                  <form action="sms_print_payment_report.php" method="POST" target="_blank">
                                    <div class="row">
                                    <div class="col-md-12">
                                      <div class="form-group form-group-default" style="margin-top: 10px;">
                                        <label class="pull-left">Start Date</label>
                                        
                                        <input type="date" name="start_date" class="form-control" required max="<?php echo date('Y-m-d'); ?>">

                                      </div>
                                      </div>
                                    </div>
                                  
                                    <div class="row">
                                    <div class="col-md-12">
                                      <div class="form-group form-group-default" style="margin-top: 10px;">
                                        <label class="pull-left">End Date</label>
                                        
                                        <input type="date" name="end_date" class="form-control" required min="<?php echo date('Y-m-d'); ?>">

                                      </div>
                                      </div>
                                    </div> 


                      <button type="submit" class="btn btn-success btn-lg btn-block" name="filter_data" value="<?php echo $stu_id; ?>" style="width: 100%;margin: none;" id="search_btn"><i class="pg-icon">search</i> Search</button>

                      <button type="submit" class="btn btn-default btn-lg btn-block" data-dismiss="modal" data-toggle="modal" data-target="#payments<?php echo $stu_id; ?>" style="width: 100%;margin: none;"><i class="pg-icon">arrow_left</i> Back</button>


                                  </form>
                                  </div>


                        </div>
                      </div>
                      </div>
                      </div>

                      </div>
                    </div>



                    </td>
                    

                  </tr>

<?php
  }
}else
if($check == '0')
{
  echo '<tr><td colspan="7" class="text-center text-danger"><span class="fa fa-warning"></span> Not Found Data</td></tr>';
}

 ?>


</tbody>
</table>
</div>

              
</div>
</div>
</div>
</div>




</div>
</div>



      </div>
    </div>


  </div>





<script type="text/javascript">
  $('.save_btn').click(function(){
    
    var upload_btn0 = $('#selectedFile0').val();
    var upload_btn = $('#sel_file').val();
      
    if(empty(upload_btn0))
    {
      alert('Please select your audio file.');
    }else
    if(empty(upload_btn))
    {
      alert('Please select your document file.');
    }
   });
</script>

<script type="text/javascript">
  $('.navi').click(function(){
    $('.frm')[0].reset();
      
   });
</script>

<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
<!-- search data -->

<!-- no data message/no found data show in search-->

<script type="text/javascript">
  $(document).ready(function () {

    (function ($) {

        $('#myInput').keyup(function () {
            var rex = new RegExp($(this).val(), 'i');
            $('#myTable tr').hide();
            $('#myTable tr').filter(function () {
                return rex.test($(this).text());
            }).show();
            $('.no-data').hide();
            if($('#myTable tr:visible').length == 0)
            {
                $('.no-data').show();
            }

        })

    }(jQuery));

});
</script>




<?php include('footer/footer.php'); ?>