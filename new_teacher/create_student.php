
<?php

$page = "Create Student";
$folder_in = '3';

  include('header/header.php'); 
 ini_set( "display_errors", 0); 


  ?>
  <style type="text/css">
    .btn_pulse {
  
  display: block;
  border-radius: 10%;
  cursor: pointer;
  animation: none;
  float: left;
  bottom: 5px;
  right: 5px;
  font-weight: bold;
  padding-top: 2px;
  text-align: center;
  color: white;
  font-size: 14px;
  animation: pulse 1.6s infinite;
}


@keyframes pulse {
  0% {
    -moz-box-shadow: 0 0 0 0 #7252d3;
    box-shadow: 0 0 0 0 #7252d3;
  }
  70% {
    -moz-box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
  }
  100% {
    -moz-box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
  }
}

table {
  border: 0px solid black;
}
th, td {
  border: 0px solid black;
}
td {
  padding: 5px;
}

#pagination {
  width: 100%;
  text-align: center;
}

#pagination ul li {
  display: inline;
  margin-left: 10px;
}
body
{
  overflow-x: hidden;
}

  </style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script src="https://pagination.js.org/dist/2.1.4/pagination.min.js"></script>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<div class="row" style="border-bottom: 1px solid #cccc;">
  <div class="col-md-11">
    <ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-bottom: 04%;margin-bottom: 0;">
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
      <li class="breadcrumb-item" style="font-size: 50px;display: none;"> <a href="student_approved.php">Student Approved</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="Create Students"> Create Students</li>
    </ol>
  </div>
  <div class="col-md-1">
            

    </div>
  </div>
</div>

<div class="col-md-12">

  <div class="row">
    <div class="col-md-3"></div>

    <div class="col-md-6" style="margin-top: 0;">
   

  <div class=" container-fluid   container-fixed-lg bg-white">
  <div class="card card-transparent" style="padding: 10px 10px 30px 10px;">
   <h3 style="text-transform: capitalize;"><a href="dashboard.php" data-toggle="tooltip" data-title="Back" class="text-muted" style="font-size: 12px;opacity: 0.6;"> Back</a> 
    <br>
  Create Students</h3>
    <form  action="../teacher/query/insert.php" method="POST" id="form-personal" role="form" autocomplete="off">
                <?php

                  $sql99 = mysqli_query($conn,"SELECT * FROM institute WHERE INS_ID = '1'");
                  $row99 = mysqli_fetch_assoc($sql99);
                  $reg_code = $row99['REG_CODE'];

                  
                  $sql = mysqli_query($conn,"SELECT * FROM `reg_count` WHERE `REG_COUNT_ID` = '1'");
                  $row=mysqli_fetch_assoc($sql);

                  $real_number = $row['CURRENT_REG_ID']+1;

                  $number = $reg_code.sprintf('%05d',$real_number);
                  

              ?>
                         
                              <div class="row">
                              <div class="col-md-6">
                              <div class="form-group form-group-default input-group">
                              <div class="form-input-group">
                              <label class="pull-left">First Name</label>
                              <input type="text" name="fnm" id="f_name" class="form-control change_inputs" pattern="[a-zA-Z. ]+"  required placeholder="XXXXXXXX" autofocus>
                              </div>
                              </div>
                              </div>

                              <div class="col-md-6">
                              <div class="form-group form-group-default input-group">
                              <div class="form-input-group">
                              <label class="pull-left">Last Name</label>
                              <input type="text" name="lnm" id="l_name" class="form-control change_inputs" pattern="[a-zA-Z. ]+"  required placeholder="XXXXXXXX">
                              </div>
                              </div>
                              </div>

                              </div>

                               <div class="row">

                                  
                                  <div class="col-md-6">
                                  <div class="form-group form-group-default input-group">
                                  <div class="form-input-group">
                                  <label>Date Of Birth</label>

                                  <div class="row" style="padding-right: 14px;padding-left: 14px;margin-top: 6px;margin-bottom: 6px;">
                                    
                                    <div class="col-md-4">
                                      
                                      <select class="form-control change_inputs" style="border:1px solid #cccc;" id="year" onchange="document.getElementById('month').focus(); get_dob(); password_type_function();" required>

                                        <option value="">Year</option>
                                        <?php 
                                          $pre_year = date('Y',strtotime('-40 year'));
                                          $this_year = date('Y');

                                          for ($i=$pre_year; $i < $this_year; $i++) { 
                                            // code...
                                            echo '<option value="'.$i.'">'.$i.'</option>';
                                          }
                                         ?>
                                      </select>

                                    </div>
                                    <div class="col-md-4">

                                      <select class="form-control change_inputs" style="border:1px solid #cccc;" id="month" onchange="document.getElementById('day').focus(); get_dob(); password_type_function();" required>
                                        <?php 

                                          for ($i2=1; $i2 < 13; $i2++) { 
                                            
                                            $monthNum  = $i2;
                                            $dateObj   = DateTime::createFromFormat('!m', $monthNum);
                                            $monthName = $dateObj->format('F'); // March

                                            if($i2 < 10)
                                            {
                                              $mon_no = '0'.$i2;
                                            }else
                                            {
                                              $mon_no = $i2;
                                            }


                                            echo '<option value="'.$mon_no.'">'.$monthName.'</option>';
                                          }
                                         ?>
                                      </select>

                                    </div>
                                    <div class="col-md-4">

                                      <select class="form-control change_inputs" style="border:1px solid #cccc;" id="day" onchange="document.getElementById('number').focus(); get_dob(); password_type_function();" required>
                                        <?php 

                                          for ($i3=1; $i3 < 32; $i3++) 
                                          { 
                                            if($i3 < 10)
                                            {
                                              $day = '0'.$i3;
                                            }else
                                            {
                                              $day = $i3;
                                            }
                                            
                                            echo '<option value="'.$day.'">'.$day.'</option>';
                                          }
                                         ?>
                                      </select>

                                    </div>

                                  </div>

                                  <script type="text/javascript">


                                    document.getElementById('student_dob').value = dob;

                                    function get_dob()
                                    {
                                      var year = document.getElementById('year').value;
                                      var month = document.getElementById('month').value;
                                      var day = document.getElementById('day').value;

                                      var dob = '';
                                      dob = year+'-'+month+'-'+day;

                                      document.getElementById('student_dob').value = dob;

                                    }

                                  </script>
                                  </div>
                                  </div>
                                  </div>

                                  <input type="hidden" class="form-control change_inputs" name="dob" id="student_dob" onchange="password_type_function();">

                                  <div class="col-md-6">
                                  <div class="form-group form-group-default">
                                  <div class="form-input-group">
                                  <label class="pull-left">Telephone No</label>
                                  <br>
                                  <input class="form-control change_inputs" type="telephone" pattern="[0-9]{10}" minlength="10" maxlength="10" name="number" id="number" required placeholder="0xxx xxxx xx" style="margin-bottom: 10px;">
                                  </div>
                                  </div>
                                  </div>
                              </div>


                              <div class="row">
                              <div class="col-md-12">
                              <div class="form-group form-group-default">
                              <label class="pull-left">Address</label>
                              <textarea name="address" placeholder="XXXXXXXX" id="address" style="margin-bottom: 10px;" class="form-control change_inputs"></textarea>
                              </div>
                              </div>
                              </div>

                              <div class="row">
                              <div class="col-md-6">
                              <div class="form-group form-group-default">
                              <div class="form-input-group">
                              <label class="pull-left">E-mail Address</label>
                              <input type="email" class="form-control change_inputs" name="email" id="email"  style="margin-bottom: 10px;" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" placeholder="XXXXXXXX">
                              </div>
                              </div>
                              </div>

                              <div class="col-md-6">
                                <div class="form-group form-group-default">
                                  <div class="form-input-group">
                                    <label class="pull-left">Gender</label>
                                      <select class="form-control" name="gender" style="margin-bottom: 10px;">
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>
                                      </select>
                                  </div>
                                </div>
                              </div>
                                </div>

                              <div class="row">
                                
                                <div class="col-md-6">
                                  <div class="form-group form-group-default input-group">
                                  <div class="form-input-group" style="position: relative;">
                                 
                                  <label>Password Type</label>
                                  <select id="password_type" class="form-control" onchange="password_type_function();">
                                    <option value="0">Default Password</option>
                                    <option value="1">Different Password</option>
                                  </select>

                                  <script type="text/javascript">

                                    function password_type_function()
                                    {
                                      var dob = document.getElementById('student_dob').value;
                                      var password_type = document.getElementById('password_type').value;

                                      if(password_type == '0')
                                      {
                                        var datetime = new Date(dob);
                                        // format the output
                                        var month = datetime.getMonth()+1;
                                        var day = datetime.getDate();
                                        var year = datetime.getFullYear();

                                        if(month < 10)
                                        {
                                          month = '0'+month;
                                        }

                                        if(day < 10)
                                        {
                                          day = '0'+day;
                                        }

                                        var default_psw = year+''+month+''+day;

                                        document.getElementById('password').value = default_psw;

                                      }

                                      if(password_type == '1')
                                      {
                                        document.getElementById('password').value = '';
                                      }
                                    }

                                  </script>

                                  </div>
                                </div>
                                </div>

                                 <div class="col-md-6">
                                  <div class="form-group form-group-default input-group">
                                  <div class="form-input-group" id="show_hide_password" style="position: relative;">
                                 
                                  <label>Password</label>
                                  <input type="text" name="password" id="password" placeholder="XXXXXXXX" class="form-control" required>

                                  <a href=""><i class="fa fa-eye-slash" aria-hidden="true" style="position: absolute;right:10px;top:25px;"></i></a>

                                  </div>
                                </div>
                                </div>


                                </div>



                                <div class="row" style="display: none;">

                                  <div class="col-md-12">
                                  <div class="form-group form-group-default input-group">
                                  <div class="form-input-group">
                                  <label class="pull-left" style="font-size: 12px;">Are you already registered student?</label>
                                  <br>
                                  <div class="row">

                                        <div class="col-md-6" style="padding-left: 6px;">
                                          <label class="pull-left"><span class="fa fa-check-circle"></span> Yes : <input type="radio" checked  name="new" value="1" class="change_inputs"></label>
                                        </div>

                                        <div class="col-md-6" style="padding-left: 6px;">
                                          <label class="pull-left" style="margin-bottom: 10px;"><span class="fa fa-times-circle"></span> No : <input type="radio" name="new" value="0" class="change_inputs"> </label>
                                        </div>
                                    
                                        
                                  </div>

                                  </div>
                                  </div>
                                  </div>
                              </div>


                              <div class="col-md-12" id="error"></div>


                              <div class="clearfix"></div>

                              <div class="row">

                                  <div class="col-md-12">
                                  <div class="form-group form-group-default input-group">

                                  <br>
                                  <button aria-label="" class="btn btn-success btn-block btn-lg" type="submit" name="register_students" id="save"><i class="pg-icon">plus</i> Add
                              </button>
                                  </div>
                                  </div>
                              </div>
                              

                              </form>

  </div>
  </div>

  </div>
  </div>
</div>



<script type="text/javascript">
  
    $(document).ready(function(){  
     $('#level_clz').change(function(){

       var level_clz = $(this).val();
       var teach_id = $('#teach_id').val();

       $.ajax({
        url:'../teacher/query/check.php',
        method:"POST",
        data:{create_clz:level_clz,teach_id:teach_id},
        success:function(data)
        {
          //alert(data)
            $('#subject').html(data);
          
        }
       })
     

    });

     
   });

  </script>

<script type="text/javascript">
	setInterval(function() {
    $('blink').fadeIn(1300).fadeOut(1500);
}, 1000);
</script>

<script>
$(document).ready(function(){
  $("#search").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $(".myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

<script type="text/javascript">
  $(document).ready(function () {

    (function ($) {

        $('#search').keyup(function () {
            var rex = new RegExp($(this).val(), 'i');
            $('#myTable tr').hide();
            $('#myTable tr').filter(function () {
                return rex.test($(this).text());
            }).show();
            $('.no-data').hide();
            if($('#myTable tr:visible').length == 0)
            {
                $('.no-data').show();
            }

        })

    }(jQuery));

});
</script>

<script>
$(document).ready(function(){

  $("#up_rec").on("change", function() {
      $('.show_image').hide();
  });
});
</script>
<script type="text/javascript">
  setInterval(function() {
    $('.blink').fadeIn(1300).fadeOut(1500);
}, 1000);
</script>

<script type="text/javascript">
  $("#show_hide_password a").on('click', function(event) {
        event.preventDefault();
        if($('#show_hide_password input').attr("type") == "text"){
            $('#show_hide_password input').attr('type', 'password');
            $('#show_hide_password i').addClass( "fa-eye-slash" );
            $('#show_hide_password i').removeClass( "fa-eye" );
        }else if($('#show_hide_password input').attr("type") == "password"){
            $('#show_hide_password input').attr('type', 'text');
            $('#show_hide_password i').removeClass( "fa-eye-slash" );
            $('#show_hide_password i').addClass( "fa-eye" );
        }
    });
</script>

<script type="text/javascript">
  let rows = []
$('table tbody tr').each(function(i, row) {
  return rows.push(row);
});

$('#pagination').pagination({
    dataSource: rows,
    pageSize: 8,
    callback: function(data, pagination) {
        $('tbody').html(data);
    }
})
</script>



<script type="text/javascript">
  $('#error').hide(); //error message hidden

  $(document).ready(function(){


    $('#error').hide(); //error message hidden



/*   validation form*/

  /* $('#password,#re_password').keyup(function(){

   var psw = $('#password').val();
   var re_psw = $('#re_password').val();
  
  if(psw !== '' && re_psw !== '')
  {
        if(psw !== re_psw)
       {    
            $('#error').show();

            $('#save').prop("disabled",true);
            $('#save').css('opacity','0.8');
            $('#save').css('cursor','not-allowed');

            $('#error').html('<div class="alert alert-danger col-md-12 text-center" style="color:red;font-size:16px;width:100%;"><span class="fa fa-warning text-danger"> The password you entered is incorrect.</span></div>');

       }else
       if(psw == re_psw)
       {    
            $('#error').show();

            $('#save').prop("disabled",false);
            $('#save').css('opacity','3.5');
            $('#save').css('cursor','pointer');

            $('#error').html('<div class="alert alert-success col-md-12 text-center" style="color:green;font-size:16px;width:100%;"><span class="fa fa-check-circle text-success"> The password you entered is correct.</span></div>');
       }
   }
   
   });*/

/*   validation form*/

});
</script>

<script>
$(document).ready(function () {

    $('#f_name,#l_name').bind('keyup blur',function()
    { 
           var thistext = $(this);
           thistext.val(thistext.val().replace(/[^a-z A-Z .]/g,'') ); 

           
    });

})
</script>

<script>
$(document).ready(function () {

    $('#address').bind('keyup blur',function()
    { 
           var thistext = $(this);
           thistext.val(thistext.val().replace(/[^a-z A-z . 0-9-/,]/g,'') ); 

           
    });

})
</script>

<script>
$(document).ready(function () {

    $('#email').bind('keyup blur',function()
    { 
           var thistext = $(this);
           thistext.val(thistext.val().replace(/[^a-zA-z0-9!#$%&'*+-/=?^_`{|,@]/g,'') ); 

           
    });

})
</script>

<script>
$(document).ready(function () {

    $('#number').bind('keyup blur',function()
    { 
           var thistext = $(this);
           thistext.val(thistext.val().replace(/[^0-9]/g,'') ); 

           
    });

})
</script>

<?php  include('footer/footer.php'); ?>



<script type="text/javascript">
    $(document).ready(function(){

            $('#myTable').hide();
            var st = $('#st_tbl').val();

        $('#search_students').click(function(){
          
            var st = $('#st_tbl').val();
            if(st == 1)
            {
            
              $('#myTable').hide();
              $('#st_tbl').val('0');

              
            }

            if(st == '0')
            {
            
              $('#myTable').show();
              $('#st_tbl').val('1');
              
            }
            
        });
    });
  </script>
