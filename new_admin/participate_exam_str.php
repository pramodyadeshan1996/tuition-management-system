
<?php

$page = "Participated Students";
$folder_in = '3';

$paper_id = $_GET['paper_id'];

  include('header/header.php'); 
 ini_set( "display_errors", 0); 


  ?>
  <style type="text/css">
    .btn_pulse {
  
  display: block;
  border-radius: 10%;
  cursor: pointer;
  animation: none;
  float: left;
  bottom: 5px;
  right: 5px;
  font-weight: bold;
  padding-top: 2px;
  text-align: center;
  color: white;
  font-size: 14px;
  animation: pulse 1.6s infinite;
}


@keyframes pulse {
  0% {
    -moz-box-shadow: 0 0 0 0 #7252d3;
    box-shadow: 0 0 0 0 #7252d3;
  }
  70% {
    -moz-box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
  }
  100% {
    -moz-box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
  }
}

table {
  border: 0px solid black;
}
th, td {
  border: 0px solid black;
}
td {
  padding: 5px;
}

#pagination {
  width: 100%;
  text-align: center;
}

#pagination ul li {
  display: inline;
  margin-left: 10px;
}
  </style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script src="https://pagination.js.org/dist/2.1.4/pagination.min.js"></script>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<div class="row" style="border-bottom: 1px solid #cccc;">
  <div class="col-md-11">
    <ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-bottom: 04%;margin-bottom: 0;">
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="Participated Students"> Participated Students</li>
    </ol>
  </div>
  <div class="col-md-1">
    </div>
  </div>
</div>




<div class="col-md-12" style="margin-top: 0;">
  <h3 style="text-transform: capitalize;"><a href="exam.php" data-toggle="tooltip" data-title="Back"><i class="pg-icon" style="font-size: 22px;">chevron_left</i></a> Participated Students 
  <small class="text-muted">(Essay & Structured Paper)</small></h3>

<div class=" container-fluid   container-fixed-lg bg-white">
<div class="card card-transparent">
<div class="card-header ">
<div class="card-title">
</div>
<div class="pull-left">
<div class="col-xs-12">

  <?php 

      $sql001 = mysqli_query($conn,"SELECT * FROM `paper_master` WHERE `PAPER_ID` = '$paper_id'");
      while($row001 = mysqli_fetch_assoc($sql001))
      {
        $class_id = $row001['CLASS_ID'];
        $paper_name = $row001['PAPER_NAME'];

         $sql002 = mysqli_query($conn,"SELECT * FROM `classes` WHERE `CLASS_ID` = '$class_id'");

        while($row002 = mysqli_fetch_assoc($sql002))
        {
            $class_name = $row002['CLASS_NAME'];
            $teach_id = $row002['TEACH_ID'];
        
            $sql003 = mysqli_query($conn,"SELECT * FROM `teacher_details` WHERE `TEACH_ID` = '$teach_id'");

            while($row003 = mysqli_fetch_assoc($sql003))
            {
              $teacher_name = $row003['POSITION'].". ".$row003['F_NAME']." ".$row003['L_NAME'];
            }

        }

        echo '<h4>'.$paper_name.' <small class="text-muted">'.$class_name.'</small><label style="font-size:14px;" class="text-muted"> ('.$teacher_name.')</label></h4>';
      }

   ?>


</div>
</div>
<div class="pull-right">
<div class="col-xs-12">
<input type="text" id="search" class="form-control pull-right" placeholder="Search" data-toggle="tooltip" data-title="අවශ්‍ය දත්ත සෙවීම'">
</div>
</div>
<div class="clearfix"></div>
</div>

<?php 
  
  $sql003 = mysqli_query($conn,"SELECT * FROM `exam_result` WHERE `PAPER_ID` = '$paper_id'");
  
  $ch = mysqli_num_rows($sql003);

 ?>

<div class="card-body table-responsive" style="height: auto;margin-bottom: 0;">
  <table class="table demo-table-search table-responsive-block text-left" id="tableWithSearch">
  <thead>

    <th>Student Name <label class="label label-success"><?php echo $ch; ?></label></th>
    <th style="text-align: center;">Submitted Time</th>
    <th style="text-align: center;">Total Marks</th>

  </thead>
  <tbody>
      
   <?php 
          
          if($ch > 0)
          {

                  while($row003 = mysqli_fetch_assoc($sql003))
                  {
                        $se_total_marks = $row003['SE_MARK'];
                        $submit_dt = $row003['ADD_TIME'];
                        $student_id001 = $row003['STU_ID'];


                        $sql002 = mysqli_query($conn,"SELECT * FROM `student_details` WHERE `STU_ID` = '$student_id001'");

                        while($row002 = mysqli_fetch_assoc($sql002))
                        {
                              $student_name = $row002['F_NAME']." ".$row002['L_NAME'];
                        }
                  

                    //<td class="v-align-middle bold"><label class="label label-'.$alert.'"><span class="fa fa-'.$icon.'"></span> '.$status_name.'</label></td>
                    
                    echo '<tr>';

                    echo '<td class="v-align-middle">'.$student_name.'</td>
                          <td class="v-align-middle bold" style="text-align: center;">'.$submit_dt.'</td>
                          <td class="v-align-middle bold" style="font-size:14px;text-align: center;">'.$se_total_marks.'</td>
                          </tr>
                          ';



                          
                        }
              }
 ?>
    
    
  </tbody>
  </table>
</div>


</div>

</div>
</div>


<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

<?php  include('footer/footer.php'); ?>

