
<?php

require '../connect/connect.php';
$msg = '';
if (isset($_GET['remove']))
{
    /*Delete Zoom Account Setting*/
    $sql = "UPDATE zoom_accounts SET active='0' WHERE id='$_GET[remove]'"; //Update Empty Data Query
    mysqli_query($conn,$sql);
    echo "<script>alert('Deleted Successfully!')</script>";  //Update Message

    /*Delete Zoom Account Setting*/

}
   
?>


<?php
if (isset($_POST['save']))
{
    /*Save Zoom Account Settings*/

    $time = time();
    $date = date('Y-m-d h:s');

    $save = mysqli_query($conn,"INSERT INTO zoom_accounts VALUES (NULL, '$_POST[email_address]',  '$_POST[zoom_name]', '$_POST[api_key]', '$_POST[api_secret]', '$date', '$time', '1' )"); //Save Setting Query         
              
    $msg = "<div class='alert alert-success' style='width:100%'><button class='close' data-dismiss='alert'>&times;</button><strong>Zoom Accounts has been Updated!</strong></div>"; //Insert Message
    
    /*Save Zoom Account Settings*/

} 

?>
<!doctype HTML>
<html lang="en">
<head>
    <meta charset="UTF-8">

    <!-- viewport meta -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="DigiPro - Digital Products Marketplace ">
    <meta name="keywords" content="marketplace, easy digital download, digital product, digital, html5">
    <title>Zoom Settings</title>

    <link href="https://fonts.googleapis.com/css?family=Work+Sans:400,500,600" rel="stylesheet">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

    <!-- inject:css-->

    <link rel="stylesheet" href="css/plugin.min.css">

    <link rel="stylesheet" href="style.css">

    <!-- endinject -->

    <!-- Favicon Icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png">
</head>
<body class="preload">
    <section class="hero-area4 bgimage">
        <div class="hero-content content_above" style="padding: 0px 0 0 0 ">
            <div class="content-wrapper">

                <div class="container featured-products2  p-top-0 p-bottom-120" style=" margin: 0px auto; width: 90%; padding: 0px; max-width: 90%; ">
                    <div class="row">
                        <div class="col-lg-4 col-md-12"></div>
                                
                        <div class="col-lg-4 col-md-12 p-top-0">

                            <form action="" method="post" enctype="multipart/form-data">
                                <div class="cardify signup_form" style="background: white; border-radius: 10px; padding: 20px;">
                                <!-- end .login_header -->
                                    <div class="section-title">
                                        <h3 style="margin-top: 0px;text-align: center;">Manage Zoom Accounts </h3>
                                    </div>
                                    <div class="login--form">

                                        <div class="form-group">
                                            <input type="email" placeholder="Email Address" name="email_address" required="" class="form-control">
                                        </div>

                                        <div class="form-group">
                                            <input type="text" placeholder="Name" name="zoom_name" required="" class="form-control">
                                        </div>

                                        <div class="form-group">
                                            <input type="text" placeholder="API Key" name="api_key" required="" class="form-control">
                                        </div>

                                        <div class="form-group">
                                            <input type="text"  placeholder="API Secret" name="api_secret" required="" class="form-control">
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-12 mt-30">
                                                <button class="btn btn-success btn-block" name="save"><span class="fa fa-check-circle"></span> Save Now</button>
                                            </div>
                                        </div>
                                    </div><!-- end .login--form -->
                                </div><!-- end .cardify -->
                            </form><!-- Ends: .product-single -->

                            <?php echo $msg; ?>
                        </div>
                    </div>

                <div class="row">
                  

                    <div class="col-md-12">
                                            
                        <div class="statement_table table-responsive" style="background: white">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Email</th>
                                    <th>Name</th>
                                    <th>API Key</th>
                                    <th>API Secret</th>
                                </tr>
                                </thead>

                                <tbody>

                                <?php   

                                        $result_cat001 = mysqli_query($conn,"SELECT * FROM zoom_accounts where active = '1' ORDER by id ASC");
                                        while($pra_cat001 = mysqli_fetch_assoc($result_cat001))
                                        { ?>     

                                            <tr>
                                                
                                                <td><a href="?remove=<?php echo $pra_cat001['id']; ?>" onclick="return confirm('Are you sure?')">&#10060;</a></td>    
                                                
                                                <td><?php echo $pra_cat001['email_address']; ?></td>
                                                <td><?php echo $pra_cat001['zoom_name']; ?></td>
                                                <td><?php echo $pra_cat001['api_key']; ?></td>
                                                <td><?php echo $pra_cat001['api_secret']; ?></td>
                                               
                                            </tr>

                                <?php   } ?>


                                </tbody>
                            </table>

                        </div>

                    </div>

                </div>

                </div>
            </div>
        </div><!-- end hero-content -->
    </section><!-- ends: .hero-area -->

</body>
</html>