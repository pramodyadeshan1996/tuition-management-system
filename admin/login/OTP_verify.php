<?php 
    session_start();
    include('../../connect/connect.php');

    $sql1123 = mysqli_query($conn,"SELECT * FROM `institute` WHERE `ADMIN_OTP` = '1'");
      
    $check_otp_option = mysqli_num_rows($sql1123);

    if($_SESSION['TYPE'] == 'S_ADMIN')
    {
      header('location:../../new_admin/dashboard.php');
    }

    if($check_otp_option == '1')
    {

      if(isset($_POST['submit']))
      {
        $otp = $_POST['otp_code'];

        $admin_id = $_SESSION['ADMIN_ID'];

        $sql1122 = mysqli_query($conn,"SELECT * FROM `admin_login` WHERE `ADMIN_ID` = '$admin_id' AND `OTP` = '$otp'");
        
        $check_otp = mysqli_num_rows($sql1122);

        if($check_otp == '0')
        {
          $_SESSION['error'] = '<label class="text-default" style="color:#fdcb6e;"><span class="fa fa-times"></span> Wrong OTP Code. Please enter correct OTP Again.</label>';
        }else
        if($check_otp > 0)
        {
          header('location:../../new_admin/dashboard.php');
        }
      }else
      {
          $_SESSION['error'] = '';

      }

    }else
    if($check_otp_option == '0')
    {
      
      header('location:../../new_admin/dashboard.php');
        
    }


    
    
      ?>

    
          <!DOCTYPE html>
          <html lang="en">

          <head>

            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
            <meta name="description" content="">
            <meta name="author" content="">

            <title>OTP Verification</title>

            <!-- Custom fonts for this template-->
            <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
            <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
            <link href="https://fonts.googleapis.com/css?family=Muli&display=swap" rel="stylesheet">
            <!-- Custom styles for this template-->
            <link href="css/sb-admin-2.min.css" rel="stylesheet">
            <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

            <!-- Latest compiled and minified CSS -->
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

          <!-- jQuery library -->
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

          <!-- Latest compiled JavaScript -->
          <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
          <style type="text/css">


          button {
              --c: goldenrod;
              color: var(--c);
              font-size: 16px;
              border: 0.3em solid var(--c);
              border-radius: 0.5em;
              text-transform: uppercase;
              font-weight: bold;
              font-family: sans-serif;
              letter-spacing: 0.1em;
              text-align: center;
              position: relative;
              overflow: hidden;
              z-index: 1;
              transition: 0.5s;
              margin: 0em;
          }

          button span {
              position: absolute;
              width: 25%;
              height: 100%;
              background-color: var(--c);
              transform: translateY(150%);
              border-radius: 50%;
              left: calc((var(--n) - 1) * 25%);
              transition: 0.9s;
              transition-delay: calc((var(--n) - 1) * 0.1s);
              z-index: -1;
          }

          button:hover {
              color: black;
          }

          button:hover span {
              transform: translateY(0) scale(2);
          }

          button span:nth-child(1) {
              --n: 1;
          }

          button span:nth-child(2) {
              --n: 2;
          }

          button span:nth-child(3) {
              --n: 3;
          }

          button span:nth-child(4) {
              --n: 4;
          }

          </style>

          </head>

          <body class="bg-gradient-primary" style="font-family: 'Muli', sans-serif;background: #ffeaa7;">

           

            <div class="container">

              <div class="col-md-12">
              <!-- Outer Row -->
              <div class="row justify-content-center">
               <div class="col-xl-4 col-lg-4 col-md-4"></div>

                <div class="col-md-4" style="border:1px solid #00000000;margin-top: 10%;background-color: #0000007d;padding: 40px 40px 40px 40px;border-radius: 4px;margin-bottom: 25px">
                           

                            <div class="text-center">
                              <span class="fa fa-check-circle" style="font-size:60px;color:white;"></span>
                              <h1 style="color: white;">Welcome!</h1>
                            </div>
                              <br>
                              <form action="OTP_verify.php" method="POST">

                                <div class="form-group">
                                  <input type="text" class="form-control form-control-user" aria-describedby="emailHelp" placeholder="Enter OTP Code..." name="otp_code" style="padding-left: 24px;height:50px;border-radius: 50px;" id="number" required autocomplete="off" autofocus="on" maxlength="6">
                                </div>

                                <?php echo $_SESSION['error']; ?>
                                
                                <button type="submit" class="btn btn-info btn-active btn-user btn-block" name="submit" id="log" style="margin-top: 10px;height:50px;border-radius: 50px;font-size: 16px;background: rgb(38,158,245);background: linear-gradient(191deg, rgba(38,158,245,1) 47%, rgba(54,163,245,1) 87%);font-weight: bold;outline: none;border:rgb(218 165 32);"><span></span><span></span><span></span><span></span> <i class="fa fa-check"></i>
                                  Verification
                                </button>


                              </form>

                          <div class="col-md-12" style="text-align: center;margin-top: 20px;"><small style="color:white;">&copy;IBS Developer Team</small>

                            <div id="countdown" style="font-weight: bold;color:white;"></div>

                            <center><a href="index.php" id="back_link" style="color:white;font-weight: bold;text-decoration: none;"><span class="fa fa-arrow-left" style="color:#ffff;font-size: 22px;margin-top: 10px;"></span> Back</a></center>
                          </div>

                    </div>

                <div class="col-xl-4 col-lg-4 col-md-4"></div>

              </div>
          </div>
            </div>

            <!-- Bootstrap core JavaScript-->
            <script src="vendor/jquery/jquery.min.js"></script>
            <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

            <!-- Core plugin JavaScript-->
            <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

            <!-- Custom scripts for all pages-->
            <script src="js/sb-admin-2.min.js"></script>
            
            <script type="text/javascript">
            /*Enter key press after login to page*/
            $('body').on( 'keyup', function( e ) {
                      if( e.which == 13 ) {
                        //GOTO DOWN
                        $('#log').click();
                      }
                      
            });
            /*Enter key press after login to page*/


          </script>


          <script>

          //Check TP No and disable characters

          $(document).ready(function () {

              $('#number').bind('keyup blur',function()
              { 
                     var thistext = $(this);
                     thistext.val(thistext.val().replace(/[^0-9]/g,'') ); 

                     
              });

          })

          //Check TP No and disable characters
          </script>

          </body>

         </html>
