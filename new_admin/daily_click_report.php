<?php 
  session_start();
  include('../connect/connect.php');

  $start_date = $_POST['start_date'];
  $end_date = $_POST['end_date'];

  $user_name = $_SESSION['ADMIN_NAME'];
  


  $sql = mysqli_query($conn,"SELECT * FROM institute WHERE INS_ID = '1'");
  while($row = mysqli_fetch_assoc($sql))
  {
    $n = $row['INS_NAME'];
    $p = $row['PICTURE'];
    if($row['INS_MOBILE'] !== '0')
    {
      $mobile = $row['INS_MOBILE'];
    }else
    {
      $mobile = "";
    }

    if($row['INS_TP'] !== '0')
    {
      $tele = $row['INS_TP'];
    }else
    {
      $tele = "";
    }


    if(!empty($mobile) && !empty($tele))
    {
      $no = $mobile." / ".$tele;
    }

    $a = $row['INS_ADDRESS'];
  }header('Content-type: text/html; charset=UTF-8');


    $date = date('Y-m-d');
    $time = date('h:i:s A');

 ?>
<html>
<head>
<style type="text/css" media="print,screen">
.hideMe{
display:block;
}

.PrintClass {
display:block;

}
.NoPrintClass{
display:block;
}

</style>
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script type="text/javascript"
    src="http://jqueryjs.googlecode.com/files/jquery-1.3.1.min.js"> </script>
    <link href="https://fonts.googleapis.com/css?family=PT+Sans+Narrow&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<script type="text/javascript">

function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     document.body.innerHTML = originalContents;

     window.print();
    window.onafterprint = function() {
    };
     

}
function close_win() {
   window.close();
}
   </script>
</head>
  <style type="text/css">
  @media print {
  .print_btn {
    display: none;
  }
}
</style>
</head>


<body>
 <div class="col-md-12" id="printableArea" style="display:block;">

 <p style="text-align:center">
        <?php 

        if(!empty($mobile) && !empty($tele))
        {
          $no = $mobile."/".$tele;
        }else
        if(!empty($mobile) && empty($tele))
        {
          $no = $mobile;
        }else
        if(!empty($tele) && empty($mobile))
        {
          $no = $tele;
        }else
        {
          $no = '';
        }



        if($p == "0")
        {
          echo '<h3 style="text-align:center;text-transform:uppercase;">'.$n.'</h3><p style="text-align:center">'.$a.'</p><p style="text-align:center">'.$no.'</p><div style="border-top: 1px dashed black;padding:0px;"></div>';
        }else
        if($p !== "0")
        {
          echo '<h3 style="text-align:center;text-transform:uppercase;">'.$n.'</h3><p style="text-align:center">'.$a.'</p><p style="text-align:center">'.$no.'</p><div style="border-top: 1px dashed black;padding:0px;"></div>';
        }

        
        ?>
      </p>


        <div class="row">
          <div class="col-md-4">

            <h4 style="float: left;text-transform: uppercase;">Duration : <?php echo $start_date; ?> - <?php echo $end_date; ?>
          <br>

          <?php echo "User/Support : ".$user_name; ?>

            </h4>
          </div>
          <div class="col-md-4" style="text-align: center;padding-top: 15px;"><center><h1 style="text-align: center;margin-bottom: 20px;text-transform: uppercase;"><u>DAILY Access COUNT REPORT</u></h1></center>
          </div>
          <div class="col-md-4"></div>
        </div>
    
    <div style="border-top: 1px dashed black;margin-top: 10px;margin-bottom: 10px;"></div>
        <div class="table-responsive">
            
            <table class="table" style="font-size: 14px;">
              <thead>
                <th>Count Date</th>
                <th class="text-right">No Of Joined</th>
              </thead>
            <tbody>
              <?php 

                  $sql0060 = mysqli_query($conn,"SELECT * FROM `student_access_count` WHERE `COUNT_DATE` BETWEEN '$start_date' AND '$end_date' ORDER BY `COUNT_DATE` DESC");
                

                if(mysqli_num_rows($sql0060)>0)
                {
                    while($row0023 = mysqli_fetch_assoc($sql0060))
                    { 
                        $count_date = $row0023['COUNT_DATE'];
                        $amount = $row0023['AMOUNT'];

                        echo '
                            <tr>
                              <td>'.$count_date.'</td>
                              <td class="text-right">'.$amount.'</td>
                            </tr>
                          ';
                    }    
                  }else
                  if(mysqli_num_rows($sql0060) == '0')
                  {
                    echo '

                      <tr style="font-size: 14px;">
                       <th colspan="5" class="text-center text-danger"><span class="fa fa-warning"></span> Not Found Data!</th>
                     </tr>

                    ';

                  }
                  

               ?>

                </tbody>
              </table>
        </div>
      
</div>

<div class="col-md-12" style="position: fixed;top:10px;right: 10px;">

  <button class="btn btn-success btn-lg print_btn" onclick="printDiv('printableArea')" style="font-size: 20px;float: right;border-radius: 80px;padding: 10px 13px 10px 13px;margin-left: 10px;outline: none;"><span class="fa fa-print"></span></button>

  <button class="btn btn-danger btn-lg print_btn" onclick="close_win();" style="font-size: 20px;float: right;border-radius: 80px;padding: 10px 13px 10px 13px;outline: none;"><span class="fa fa-times"></span></button>


</div>
</body>

</html>



