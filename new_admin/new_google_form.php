  
<?php

$page = "Add Google Form";
$folder_in = '0';

  include('header/header.php'); 
 ini_set( "display_errors", 0); 

 $admin_id = $_SESSION['ADMIN_ID'];

  ?>
  <style type="text/css">
    .btn_pulse {
  
  display: block;
  border-radius: 10%;
  cursor: pointer;
  animation: none;
  float: left;
  bottom: 5px;
  right: 5px;
  font-weight: bold;
  padding-top: 2px;
  text-align: center;
  color: white;
  font-size: 14px;
  animation: pulse 1.6s infinite;
}


@keyframes pulse {
  0% {
    -moz-box-shadow: 0 0 0 0 #7252d3;
    box-shadow: 0 0 0 0 #7252d3;
  }
  70% {
    -moz-box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
  }
  100% {
    -moz-box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
  }
}
  </style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


<div class="col-md-12" style="border-top: 1px solid #cccc;">

  <ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-bottom: 20%;margin-bottom: 0;">
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="Add Google Form"> Add Google Form</li>
    </ol>
<div style="margin: 10px 0px 10px 0;"></div>

<div class="row" style="margin-top: 1%;margin-bottom: 3%;">
<div class="col-md-3"></div>
<div class="col-md-6">

  <div class="col-md-12 bg-white" style="padding: 20px 20px 20px 20px;">

    <a href="google_form.php" style="text-decoration: none;color: gray;margin-bottom: 10px;"><span class="fa fa-angle-left"></span> Back</a>
    <a href="dashboard.php" style="text-decoration: none;color: gray;margin-bottom: 10px;margin-left: 10px;"><span class="fa fa-home"></span> Dashboard</a>

    <div style="border-bottom: 1px solid #cccc;margin-top: 10px;">
      <label class="text-muted text-center" style="font-size: 30px;"><span class="fa fa-google" style="font-size: 30px;"></span><strong> Google Form</strong></label>
    </div>

  <form action="../admin/query/insert.php" method="POST">
    <div style="padding-top: 20px;">
      
      <div class="row">
        <div class="col-md-12">
          <div class="form-group form-group-default input-group">
              <div class="form-input-group">
                <label>Paper Name</label>
                <input type="text" name="paper_name" class="form-control" placeholder="Write.." required data-toggle="tooltip" data-title="ප්‍රශ්න පත්‍රයේ නම" style="text-transform: capitalize;">
              </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-6">
          
            <div class="form-group form-group-default input-group">
                <div class="form-input-group">
                  <label>Teacher Name</label>
                  <select class="form-control" name="teacher_id" id="teacher_id" onchange="find_class();" data-toggle="tooltip" data-title="ගුරුවරයාගේ නම">
                    <option value="">Select Teacher Name</option>
                    <?php 
                      $sql001 = mysqli_query($conn,"SELECT * FROM `teacher_details`"); //check Classes for registered teachers details
                      while($row001 = mysqli_fetch_assoc($sql001))
                      {

                          $teacher_id = $row001['TEACH_ID']; //Teacher Position
                          $teacher_f_name = $row001['F_NAME']; //Teacher First Name
                          $teacher_l_name = $row001['L_NAME']; //Teacher Last Name
                          $teacher_position = $row001['POSITION']; //Teacher Last Name

                          $teacher_full_name = $teacher_position.". ".$teacher_f_name." ".$teacher_l_name;

                          echo '<option value="'.$teacher_id.'">'.$teacher_full_name.'</option>';


                      }
                     ?>
                  </select>
                </div>
            </div>

        </div>

        <div class="col-md-6">
          
            <div style="border: 1px solid #cccc;">
                <div style="padding: 6px 6px 6px 6px;">

                <div id="add_class_button">
                  <button type="button" class="btn btn-success btn-block" style="padding: 9px 10px 9px 10px;" data-toggle="modal" data-target="#add_classes">
                  	<i class="pg-icon">plus</i> Add Classes
                  </button></div>

                  <!-- Modal -->
				<div id="add_classes" class="modal fade" role="dialog">
				  <div class="modal-dialog">

				    <!-- Modal content-->
				    <div class="modal-content">
				      <div class="modal-header">

				      	<h4 class="modal-title text-left">Add Classes</h4>
				        
				      </div>
				      <div class="modal-body">
				       
					      <div class="table-responsive" style="height: 400px;overflow: auto;">
					      <table class="table table-hover">
					      	<thead>
					      		<th>#</th>
					      		<th>Class Name</th>
					      	</thead>
					      	<tbody id="class_id001">	
					      		<tr>
			                        <td colspan="3" class="text-center text-danger"><span class="fa fa-warning"></span> Not Found Data!</td>
			                 	</tr>
					      	</tbody>
					      </table>
					      </div>
				      </div>
				      <div class="modal-footer">
				      		
				      		<div id="sub_id">
				      		<button type="button" class="btn btn-success btn-lg btn-block" onclick="close_class_modal();" style="font-size: 16px;" id="my_add_class"><i class="pg-icon" style="font-size: 20px;">tick_circle</i> Submit </button>
							</div>
				        
				      </div>
				    </div>

				  </div>
				</div>
				  <!-- Modal -->

				  <script type="text/javascript">
				  	document.getElementById('my_add_class').disabled = true;
				  	function click_all() {
				  		
					  	var array = []
						var checkboxes = document.querySelectorAll('input[type=checkbox]:checked')
					  	var clicked_amount = checkboxes.length;

					  	if(clicked_amount > 0)
					  	{
						  	document.getElementById('sub_id').disabled = false;
						  	document.getElementById('sub_id').innerHTML = '<button type="button" class="btn btn-success btn-lg btn-block" data-dismiss="modal"  style="font-size: 16px;"><i class="pg-icon" style="font-size: 20px;">tick_circle</i> Submit '+'('+clicked_amount+')</button>';

						  	
						  	document.getElementById('add_class_button').innerHTML = '<button type="button" class="btn btn-success btn-block" style="padding: 9px 10px 9px 10px;" data-toggle="modal" data-target="#add_classes" id="add_class_button"><i class="pg-icon">plus</i> Add Classes'+'('+clicked_amount+')</button>';
					  	}

					  	if(clicked_amount == '0')
					  	{
						  	document.getElementById('sub_id').disabled = true;
						  	document.getElementById('sub_id').innerHTML = '<button type="button" class="btn btn-success btn-lg btn-block" onclick="click_all();" style="font-size: 16px;"><i class="pg-icon" style="font-size: 20px;">tick_circle</i> Submit</button>';

						  	document.getElementById('add_class_button').innerHTML = '<button type="button" class="btn btn-success btn-block" style="padding: 9px 10px 9px 10px;" data-toggle="modal" data-target="#add_classes" id="add_class_button"><i class="pg-icon">plus</i> Add Classes</button>';


						  	
					  	}
				  		
				  	}

				  	function close_class_modal() {
				  		document.getElementById('close_class_modal').click();
				  	}
				  </script>


                </div>
            </div>
      
        </div>
</div>


      <div class="row">
        <div class="col-md-6">
          
            <div class="form-group form-group-default input-group">
                <div class="form-input-group">
                  <label>Time Duration(Hours)</label>
                  <select class="form-control" name="hour" data-toggle="tooltip" data-title="ලබාදෙන කාලය(පැය ගණන)">
                    <option value="">Select Hour</option>
                    <?php 
                      for ($h=0; $h <= 12; $h++) 
                      { 

                        if($h<10)
                        {
                          $h = '0'.$h;
                        }

                        echo '<option value="'.$h.'">'.$h.'</option>';

                      }
                     ?>
                  </select>
                </div>
            </div>

        </div>

        <div class="col-md-6">
          
            <div class="form-group form-group-default input-group">
                <div class="form-input-group">
                  <label>Time Duration(Minute)</label>
                  <select class="form-control" name="minute" data-toggle="tooltip" data-title="ලබාදෙන කාලය(විනාඩි ගණන)">
                    <option value="">Select Minute</option>
                    <?php 
                      for ($m=0; $m <= 60; $m++) 
                      { 

                        if($m<10)
                        {
                          $m = '0'.$m;
                        }

                        echo '<option value="'.$m.'">'.$m.'</option>';

                      }
                     ?>
                  </select>
                </div>
            </div>
      
        </div>


      </div>

      <div class="row">
          <div class="col-md-6">
            <div class="form-group form-group-default input-group">
            <div class="form-input-group">
              <label>Start Date & Time</label>
              <input type="datetime-local" name="start_date" id="start" min ='<?php echo date('Y-m-d');?>T00:00' class="form-control" required data-toggle="tooltip" data-title="ආරම්භවන දිනය හා වේලාව">
            </div>
          </div>
        </div>
        <div class="col-md-6">
          
          <div class="form-group form-group-default input-group">
              <div class="form-input-group">
                <label>Finish Date & Time</label>
                <input type="datetime-local" name="end_date" id="end" class="form-control" min ='<?php echo date('Y-m-d');?>T00:00' required data-toggle="tooltip" data-title="අවසන් වන දිනය හා වේලාව">
              </div>
          </div>
          
        </div>
      </div>

      <div class="row">
          <div class="col-md-12">
            <div class="form-group form-group-default input-group">
            <div class="form-input-group">
              <label>Google Form Link</label>
              <textarea class="form-control" name="google_form_link" placeholder="Google Form Link.."></textarea>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4"></div>
        <div class="col-md-4">
          <button class="btn btn-success btn-lg btn-block" name="add_google_link"><i class="fa fa-plus"></i>&nbsp;Add Google Link</button>
        </div>
      </div>
      
      
    </div>
  </form>
  </div>

  

</div>

<div class="col-md-3">

</div>


</div>

</form>

<script type="text/javascript">
  $(document).ready(function(){
      //$('#pay_btn').prop('disabled',true); //refresh with disable button

      setTimeout(function() {
          <?php $_SESSION['msg'] = ''; ?>
          $('#successMessage').fadeOut('slow');
      }, 3000); // <-- time in milliseconds
  });
</script>

<?php  include('footer/footer.php'); ?>


<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
<!-- search data -->

<!-- no data message/no found data show in search-->

<script type="text/javascript">
  $(document).ready(function () {

    (function ($) {

        $('#myInput').keyup(function () {
            var rex = new RegExp($(this).val(), 'i');
            $('#myTable tr').hide();
            $('#myTable tr').filter(function () {
                return rex.test($(this).text());
            }).show();
            $('.no-data').hide();
            if($('#myTable tr:visible').length == 0)
            {
                $('.no-data').show();
            }

        })

    }(jQuery));

});
</script>

<script type="text/javascript">


    function find_class()
    {
                  
        var teacher_id = document.getElementById('teacher_id').value;

         $.ajax({
          url:'../admin/query/check.php',
          method:"POST",
          data:{check_teacher_mcq:teacher_id},
          success:function(data)
          {
            //alert(data)
            document.getElementById('class_id001').innerHTML = data;
          }
        });
  }


</script>

<script type="text/javascript">


    function add_questions()
    {
      var show_question = document.getElementById('question').value;
      var answer_no = document.getElementById('answer_no').value;
      //alert(show_question)
      $.ajax({
          url:'../admin/query/check.php',
          method:"POST",
          data:{que_ans:show_question,answer_no:answer_no},
          success:function(data)
          {
            //alert(data)
            document.getElementById('que_ans').innerHTML = data;
          }
        });
  }


</script>

