

<?php
  $page = "Seminar";
  $folder_in = '3';

  include('header/header.php');

  $teacher_id001 = $_GET['teach_id'];

 ?>

    <ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-bottom: 04%;margin-bottom: 0;">
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="Seminar"> Seminar</li>
    </ol>


      <div class="col-md-12 bg-white" style="border:1px solid #cccc;height: auto;">
      
      <div class="col-md-12" style="border-bottom: 1px solid #cccc;">
        <div class="row">

            <div class="col-md-9" style="margin-bottom: 10px;"><h2><a href="dashboard.php" data-toggle="tooltip" data-title="Back"><i class="pg-icon" style="font-size: 22px;">chevron_left</i></a> Seminar</h2></div>
            <div class="col-md-1"></div>
            <div class="col-md-2" style="margin-top:12px;">
              <button class="btn btn-success btn-lg btn-block mb-2" style="display:none;" data-toggle="modal" data-target="#create_seminar"><i class="fa fa-plus"></i>&nbsp;Create Seminar</button>

            </div>

            <div class="modal fade slide-up disable-scroll" id="create_seminar" tabindex="-1" role="dialog" aria-hidden="false" style="overflow:auto;">
              <div class="modal-dialog ">
                  <div class="modal-content-wrapper">
                    <div class="modal-content modal-lg">

                      <div class="modal-header clearfix text-left">
                        <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
                        </button>
                        <h5>Create Seminar</h5>
                      </div>
                      <div class="modal-body">
                        <form action="../admin/query/insert.php" method="POST">
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group form-group-default">
                              <label>Seminar Name</label>
                              <input type="text" name="semi_name" class="form-control change_inputs" placeholder=" Enter Seminar Name" required>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group form-group-default">
                              <label>Seminar Code</label>
                              <?php $six_digit_random_number = random_int(100000, 999999); ?>
                              <input type="text" name="semi_code" readonly style="font-size: 18px;" class="form-control change_inputs text-danger" placeholder=" Enter Seminar Name" required value="<?php echo $six_digit_random_number; ?>">
                            </div>
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group form-group-default">
                              <label>Due Date</label>
                              <input type="date" name="semi_date" min="<?php echo date('Y-m-d'); ?>" class="form-control change_inputs" required>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group form-group-default">
                              <label>Teacher Name</label>

                              <input list="teacher_search" id="teacher_search2" name="semi_teacher" class="form-control" placeholder="Search Teacher Name.." required>

                              <datalist id="teacher_search">
                                <?php 


                                    $sql004 = mysqli_query($conn,"SELECT * FROM `teacher_details` WHERE `TEACH_ID` = '$teacher_id001'");
                                    while($row004 = mysqli_fetch_assoc($sql004))
                                    {
                                      $teacher_id = $row004['TEACH_ID'];

                                      $mysql = mysqli_query($conn,"SELECT * FROM `teacher_login` WHERE `TEACH_ID`='$teacher_id' AND `CONFIRM` = '1'");

                                      if(mysqli_num_rows($mysql)>0)
                                      {
                                        $teacher_name = $row004['POSITION'].". ".$row004['F_NAME']." ".$row004['L_NAME'];
                                      
                                        echo '<option value="'.$teacher_id.'">'.$teacher_name.'</option>';
                                      }
                                      
                                    }

                                 ?>
                                
                              </datalist>
                            </div>
                          </div>
                        </div>

                        
                        <div class="row">
                          <div class="col-md-6">

                            <div class="form-group form-group-default">
                              <label>Start Time</label>
                              
                              <input type="time" name="semi_start" class="form-control change_inputs" required>

                            </div>

                          </div>
                          <div class="col-md-6">

                            <div class="form-group form-group-default">
                              <label>End Time</label>
                              
                              <input type="time" name="semi_end" class="form-control change_inputs" required>

                            </div>

                          </div>
                        </div>

                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group form-group-default">
                            <label>Seminar Link</label>
                            <textarea name="semi_link" required class="form-control" style="height: 40px;font-weight: bold;" placeholder="Enter Seminar Link"></textarea>

                        </div>
                          </div>
                        </div>

                        <input type="hidden" name="page" value="<?php if(empty($_GET['page'])){echo $page = "1";}else{echo $_GET['page'];}?>">


                        <div class="clearfix"></div>
                        <div class="row m-t-25">
                          <div class="col-xl-6 p-b-10">
                            <p class="small-text hint-text">Click the Add button to save all the data you entered. <br>(ඔබ ඇතුලත් කල සියලු දත්තයන් Save කිරීමට Submit බොත්තම ඔබන්න.)</p>
                          </div>
                          <div class="col-xl-6">
                            <button class="btn btn-primary pull-right btn-lg btn-block" type="submit" name="semi_submit"><i class="fa fa-plus"></i>&nbsp; Submit
                            </button>
                          </div>
                        </div>

                      </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>



        </div>
      </div>

          
          <div class=" container-fluid container-fixed-lg bg-white" style="margin-top: 0px;">
            <div class="card card-transparent">
              <div class="card-header ">
                    <div class="clearfix"></div>
                </div>
                <div class="card-body">
                  <!--  style="height: 820px;overflow: auto;" -->
                <div class="table-responsive">
                <table class="table table-hover" id="tblFruits">
                  <thead>
                      <tr>
                        <th style="width: 1%;">Create Date</th>
                        <th style="width: 1%;">Seminar Code</th>
                        <th style="width: 1%;">Due Date</th>
                        <th style="width: 1%;">Seminar Name</th>
                        <th style="width: 1%;">Teacher Name</th>
                        <th style="width: 1%;">Access Count</th>
                        <th style="width: 1%;text-align: center;">Action</th>
                      </tr>
                    </thead>
                    <tbody>

                      <?php 
                          /*------------------ Header Pagination --------------------------------*/
                        $record_per_page = 10;

                        $nxt_five_loop = 0;

                        $page = '';

                        if(isset($_GET["page"]))
                        {
                          $page = $_GET["page"];
                        }
                        else
                        {
                         $page = 1;
                        }

                        $start_from = ($page-1)*$record_per_page; //Current page no -1 X Per Page records

                          $sql001 = "SELECT * FROM `seminar_master` WHERE `TEACH_ID` = '$teacher_id001' order by `SEMI_ID` DESC LIMIT $start_from, $record_per_page";

                        /*------------------ Header Pagination --------------------------------*/

                        
                        //LIMIT = 10-1 x 10 = 90 , 10";
                        $result = mysqli_query($conn, $sql001);
                        $page_result = mysqli_query($conn, "SELECT * FROM `seminar_master` WHERE `TEACH_ID` = '$teacher_id001' ORDER BY `SEMI_ID` DESC");

                          $check = mysqli_num_rows($result);

                          if($check>0)
                          {
                          while($row001 = mysqli_fetch_assoc($result))
                          {
                            $semi_id = $row001['SEMI_ID'];
                            $teach_id = $row001['TEACH_ID'];
                            $register_date = $row001['ADD_TIME'];
                            $access_count = $row001['ATTEND_COUNT'];
                            $semi_code = $row001['SEM_CODE'];
                            $semi_day = $row001['DAY'];
                            $due_date = $row001['DUE_DATE'];
                            $seminar_name = $row001['SEM_NAME'];
                            $semi_link = $row001['SEMINAR_LINK'];

                            $start_time = $row001['START_TIME']; //Class Start time
                            $end_time = $row001['END_TIME']; //Class End time

                            $str = strtotime($start_time);
                            $class_start_time = date('h:i A',$str);

                            $str2 = strtotime($end_time);
                            $class_end_time = date('h:i A',$str2);

                            $due_time = $class_start_time." - ".$class_end_time;

                            $sql002 = mysqli_query($conn,"SELECT * FROM `teacher_details` WHERE `TEACH_ID` = '$teach_id'");
                            while($row002 = mysqli_fetch_assoc($sql002))
                            {
                              $teacher_name = $row002['POSITION'].". ".$row002['F_NAME']." ".$row002['L_NAME'];

                            }

                            echo '

                              <tr>
                                <td class="v-align-middle">'.$register_date.'</td>
                                <td class="v-align-middle"><h4>'.$semi_code.'</h4></td>
                                <td class="v-align-middle"><label style="font-weight:600;">'.$due_date.' <br> <small>'.$due_time.'</small><br> <small class="badge badge-success">'.$semi_day.'</small></label></td>
                                <td class="v-align-middle">'.$seminar_name.'</td>
                                <td class="v-align-middle">'.$teacher_name.'</td>
                                <td class="v-align-middle">'.$access_count.'</td>
                                 <td class="v-align-middle text-center">

                                <a href="seminar_student_report.php?seminar_id='.$semi_id.'" target="_blank" class="btn btn-info  btn-xs btn-rounded mt-1 mr-1"  style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;"><i class="pg-icon">users</i></a>

                                <button type="button" data-toggle="modal" data-target="#edit_seminar'.$semi_id.'" class="btn btn-success btn-xs btn-rounded mt-1 mr-1" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;" data-title="Approve" data-toggle="tooltip"
                                "><i class="pg-icon">edit</i></button>
                                ' ?>
                                <a href="../teacher/query/delete.php?delete_seminar=<?php echo $semi_id; ?>&&page=<?php echo $page; ?>&&teach_id=<?php echo $teacher_id001; ?>" onclick="return confirm('Are you sure delete?')" class="btn btn-danger mt-1 mr-1 btn-xs btn-rounded" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;" data-title="Delete" data-toggle="tooltip"><i class="pg-icon">trash_alt</i></a>

                                <?php echo '

                                </td>
                              </tr>

                            ';
                        ?>
                        <div class="modal fade slide-up disable-scroll" id="edit_seminar<?php echo $semi_id; ?>" tabindex="-1" role="dialog" aria-hidden="false" style="overflow:auto;">
                          <div class="modal-dialog ">
                              <div class="modal-content-wrapper">
                                <div class="modal-content modal-lg">

                                  <div class="modal-header clearfix text-left">
                                    <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
                                    </button>
                                    <h5>Edit Seminar</h5>
                                  </div>
                                  <div class="modal-body">
                                    <form action="../teacher/query/update.php" method="POST">
                                    <div class="row">
                                      <div class="col-md-6">
                                        <div class="form-group form-group-default">
                                          <label>Seminar Name</label>
                                          <input type="text" name="semi_name" class="form-control change_inputs" placeholder=" Enter Seminar Name" required value="<?php echo $seminar_name; ?>">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group form-group-default">
                                          <label>Seminar Code</label>
                                          <input type="text" name="semi_code" readonly style="font-size: 18px;" class="form-control change_inputs text-danger" placeholder=" Enter Seminar Name" required value="<?php echo $semi_code; ?>">
                                        </div>
                                      </div>
                                    </div>

                                    <div class="row">
                                      <div class="col-md-12">
                                        <div class="form-group form-group-default">
                                          <label>Due Date</label>
                                          <input type="date" name="semi_date" min="<?php echo date('Y-m-d'); ?>" class="form-control change_inputs" required value="<?php echo $due_date; ?>">
                                        </div>
                                      </div>
                                      <div class="col-md-6" style="display:none;">
                                        <div class="form-group form-group-default">
                                          <label>Teacher Name</label>

                                          <input list="teacher_search" id="teacher_search2" name="semi_teacher" class="form-control" placeholder="Search Teacher Name.." required value="<?php echo $teacher_id001; ?>" onfocus="this.select();">

                                          <datalist id="teacher_search">
                                            <?php 


                                                $sql004 = mysqli_query($conn,"SELECT * FROM `teacher_details` WHERE `TEACH_ID` != '$teacher_id001'");
                                                while($row004 = mysqli_fetch_assoc($sql004))
                                                {
                                                  $teacher_id = $row004['TEACH_ID'];

                                                  $mysql = mysqli_query($conn,"SELECT * FROM `teacher_login` WHERE `TEACH_ID`='$teacher_id' AND `CONFIRM` = '1'");

                                                  if(mysqli_num_rows($mysql)>0)
                                                  {
                                                    $teacher_name = $row004['POSITION'].". ".$row004['F_NAME']." ".$row004['L_NAME'];
                                                  
                                                    echo '<option value="'.$teacher_id.'">'.$teacher_name.'</option>';
                                                  }
                                                  
                                                }

                                             ?>
                                            
                                          </datalist>
                                        </div>
                                      </div>
                                    </div>

                                    
                                    <div class="row">
                                      <div class="col-md-6">

                                        <div class="form-group form-group-default">
                                          <label>Start Time</label>
                                          
                                          <input type="time" name="semi_start" class="form-control change_inputs" required value="<?php echo $start_time; ?>">

                                        </div>

                                      </div>
                                      <div class="col-md-6">

                                        <div class="form-group form-group-default">
                                          <label>End Time</label>
                                          
                                          <input type="time" name="semi_end" class="form-control change_inputs" required value="<?php echo $end_time; ?>">

                                        </div>

                                      </div>
                                    </div>

                                    <div class="row">
                                      <div class="col-md-12">
                                        <div class="form-group form-group-default">
                                        <label>Seminar Link</label>
                                        <textarea name="semi_link" required class="form-control" style="height: 40px;font-weight: bold;" placeholder="Enter Seminar Link"><?php echo $semi_link; ?></textarea>

                                    </div>
                                      </div>
                                    </div>
                                    <input type="hidden" name="page" value="<?php echo $page ?>">

                                    <div class="clearfix"></div>
                                    <div class="row m-t-25">
                                      <div class="col-xl-6 p-b-10">
                                        <p class="small-text hint-text">Click the Update button to change all the data you have changed. (ඔබ වෙනස් කල සියලු දත්තයන් වෙනස් කිරීමට Update බොත්තම ඔබන්න.)</p>
                                      </div>
                                      <div class="col-xl-6">
                                        <button class="btn btn-primary pull-right btn-lg btn-block" type="submit" name="semi_update" value="<?php echo $semi_id; ?>"><i class="fa fa-edit"></i>&nbsp; Update
                                        </button>
                                      </div>
                                    </div>

                                  </form>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        <?php
                          }
                        }else
                        if($check == '0')
                        {
                          echo '<tr><td colspan="7" class="text-center text-danger"><span class="fa fa-warning"></span> Not Found Data</td></tr>';
                        } 
                      

                        

                        
                         ?>


                    </tbody>
                </table>
                </div>
              </div>

                <div class="col-md-12" style="width: 100%;overflow: auto;">
                  <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-8" class="php_pagination">
                      
                      <div class="col-md-12">
                    <br />
                    <?php
                    $total_records = mysqli_num_rows($page_result);
                    $total_pages = ceil($total_records/$record_per_page);
                    $start_loop = $page;
                    $difference = $total_pages - $page;
                    if($difference <= 5)
                    {
                     $start_loop = $total_pages - 5;
                    }else
                    if($difference <= 0)
                    {
                      $start_loop = '0';
                    }

                    echo '<ul class="pagination">';
                    $end_loop = $start_loop + 4;
                    $five_loop = $page-5;
                    if($five_loop <= 0)
                    {
                      $five_loop = 1;
                    }
                    if($page > 1)
                    {

                      echo '<li><a href="seminar.php?page=1">First</a></li>';
                      echo '<li><a href="seminar.php?page='.($page - 1).'"> < </a></li>';
                      echo '<li><a href="seminar.php?page='.$five_loop.'"> <<< </a></li>';

                    }
                    if($start_loop !== '1')
                    {
                      $few_no = $start_loop - 1;
                    }else
                    if($start_loop > 0)
                    {
                      $few_no = 1;
                    }
                    
                    for($i=$few_no; $i<=$end_loop+1; $i++)
                    {     
                      if($i == $page)
                      {
                        echo '<li class="active"><a href="seminar.php?page='.$i.'" style="font-weight:bold;background-color:#2980b9;color:white;">'.$i.'</a></li>';
                      }else
                      {
                        if($i == '0')
                        {
                          continue;
                        }else
                        if($i>0)
                        {
                          echo '<li><a href="seminar.php?page='.$i.'" >'.$i.'</a></li>';
                        }
                        
                      }

                    }
                    if($page <= $end_loop)
                    {
                      
                      if($page == '1')
                      {
                        $nxt_five_loop = 5;
                      }else
                      if($page >= 5)
                      {
                        $nxt_five_loop = $page+5;

                        if($total_pages < $nxt_five_loop)
                        {
                          $nxt_five_loop = $total_pages;
                        }
                      }else
                      {
                        $nxt_five_loop = $page+5;
                      }

                        echo '<li><a href="seminar.php?page='.($page + 1).'" > > </a></li>';
                        echo '<li><a href="seminar.php?page='.$nxt_five_loop.'" > >>> </a></li>';
                        echo '<li><a href="seminar.php?page='.$total_pages.'" >Last</a></li>';
                      
                    }
                    
                    echo '</ul>';
                    
                    ?>
                    </div>

                    </div>
                  </div>
                    
                </div>
            </div>
            </div>
            </div>
            </div>
            </div>
            </div>

<?php include('footer/footer.php'); ?>

<script type="text/javascript">
  function sortTable(table, order) {
    var asc   = order === 'asc',
        tbody = table.find('tbody');
    
    tbody.find('tr').sort(function(a, b) {
        if (asc) {
            return $('td:first', a).text().localeCompare($('td:first', b).text());
        } else {
            return $('td:first', b).text().localeCompare($('td:first', a).text());
        }
    }).appendTo(tbody);
}
</script>

<script type="text/javascript">
    function GetSelected() {
        //Create an Array.
        var selected = new Array();
 
        //Reference the Table.
        var tblFruits = document.getElementById("tblFruits");
 
        //Reference all the CheckBoxes in Table.
        var chks = tblFruits.getElementsByTagName("INPUT");
 
        // Loop and push the checked CheckBox value in Array.
        for (var i = 0; i < chks.length; i++) {
            if (chks[i].checked) 
            {
                var s = selected.push(chks[i].value);

            }
        }
 
        //Display the selected CheckBox values.
        if (selected.length > 0) {
          if(confirm('Are you sure delete?'))
          {

              $.ajax({
               type: "POST",
               data: {delete_multi:selected},
               url: "../admin/query/delete.php",
               success: function(msg){
                 //alert(msg)
                 location.reload();
               }
            
              });
          }
        }else{
         
         alert("Please select student!")
        
        }
    };
</script>