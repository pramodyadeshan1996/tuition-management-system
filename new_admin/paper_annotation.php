<?php

include('../connect/connect.php');
$paper_id = $_GET['paper_id'];

?>
<!DOCTYPE html>
<html>
<head>
<title>Paper Marking</title>
<!-- Latest compiled and minified CSS -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>

<div class="container-fluid" style="padding:10px 30px 10px 30px;">

					
	

	<div class="row" style="padding-bottom:100px;">
		<div class="col-md-6">

		<div style="padding-top: 20px;height: 602px;">
		<div class="row">
			<div class="col-md-12"><a href="exam.php" class="btn btn-danger"><span class="fa fa-angle-double-left"></span> Back</a></div>
		</div>

		<?php

			$sql001 = mysqli_query($conn,"SELECT * FROM `paper_master` WHERE `PAPER_ID` = '$paper_id'");
			$ch = mysqli_num_rows($sql001);
			if($ch > 0)
			{
				while($row001 = mysqli_fetch_assoc($sql001))
				{
					$paper_name = $row001['PAPER_NAME'];

					echo '<h3 class="text-danger" style="font-weight:bold;color:#e74c3c;">Paper Name - '.$paper_name.'</h3>';
				}
			}
		

	?>
		
			
				<div class="row" style="border-bottom: 1px solid #cccc;">
					<div class="col-md-12" style="">
					<form action="paper_annotation.php" method="GET">
					<div class="row">
						<div class="col-md-6">
						
						<label>Student Name</label>
						<input type="hidden" name="paper_id" value="<?php echo $_GET['paper_id']; ?>">
						<select id="student_name" name="student_id" required class="form-control">

								<?php 

									if(empty($_GET['student_id']))
									{
										$sql001001 = mysqli_query($conn,"SELECT * FROM `student_essay_master` WHERE `PAPER_ID` = '$paper_id' ORDER BY `STU_ID` DESC");
										
										echo '<option value="">Select Students</option>';
										while($row001001 = mysqli_fetch_assoc($sql001001))
										{
											$student_id001 = $row001001['STU_ID'];

											$sql002 = mysqli_query($conn,"SELECT * FROM `student_details` WHERE `STU_ID` = '$student_id001'");

											while($row002 = mysqli_fetch_assoc($sql002))
											{
												$student_name = $row002['F_NAME']." ".$row002['L_NAME'];
											}

											$sql0021 = mysqli_query($conn,"SELECT * FROM `stu_login` WHERE `STU_ID` = '$student_id001'");

											while($row0021 = mysqli_fetch_assoc($sql0021))
											{
													$stu_reg_id = $row0021['REGISTER_ID'];
											}

											echo '<option value="'.$student_id001.'">'.$student_name.' ('.$stu_reg_id.')</option>';
										}
									}else
									{
										$student_id = $_GET['student_id'];

										$sql001001 = mysqli_query($conn,"SELECT * FROM `student_essay_master` WHERE `PAPER_ID` = '$paper_id' AND `STU_ID` = '$student_id'");
										while($row001001 = mysqli_fetch_assoc($sql001001))
										{
											$student_id001 = $row001001['STU_ID'];

											$sql002 = mysqli_query($conn,"SELECT * FROM `student_details` WHERE `STU_ID` = '$student_id001'");

											while($row002 = mysqli_fetch_assoc($sql002))
											{
												$student_name = $row002['F_NAME']." ".$row002['L_NAME'];
											}

											$sql0021 = mysqli_query($conn,"SELECT * FROM `stu_login` WHERE `STU_ID` = '$student_id001'");

											while($row0021 = mysqli_fetch_assoc($sql0021))
											{
													$stu_reg_id = $row0021['REGISTER_ID'];
											}

											echo '<option value="'.$student_id001.'">'.$student_name.' ('.$stu_reg_id.')</option>';
										}

										$sql001001 = mysqli_query($conn,"SELECT * FROM `student_essay_master` WHERE `PAPER_ID` = '$paper_id' AND `STU_ID` != '$student_id001' ORDER BY `STU_ID` DESC");
										
										while($row001001 = mysqli_fetch_assoc($sql001001))
										{
											$student_id001 = $row001001['STU_ID'];

											$sql002 = mysqli_query($conn,"SELECT * FROM `student_details` WHERE `STU_ID` = '$student_id001'");

											while($row002 = mysqli_fetch_assoc($sql002))
											{
												$student_name = $row002['F_NAME']." ".$row002['L_NAME'];
											}

											$sql0021 = mysqli_query($conn,"SELECT * FROM `stu_login` WHERE `STU_ID` = '$student_id001'");

											while($row0021 = mysqli_fetch_assoc($sql0021))
											{
													$stu_reg_id = $row0021['REGISTER_ID'];
											}

											echo '<option value="'.$student_id001.'">'.$student_name.' ('.$stu_reg_id.')</option>';
										}
									}
									

								?>

							</select>

						</div>
						<div class="col-md-6" style="margin-bottom:20px;">
							<div class="row">
								<div class="col-md-4">
									<button type="submit" class="btn btn-success" name="search_student" value="0" style="margin-top:22px;"><span class="fa fa-search"></span> Search</button>
								</div>
								<div class="col-md-4">
								<?php
									$btn_disable = '';

									if(empty($_GET['student_id']))
									{
										$btn_disable = 'disabled';
									}

								?>
								<button type="submit" class="btn btn-danger" <?php echo $btn_disable; ?> name="download_student_paper" style="margin-top:22px;"><span class="fa fa-download"></span> Download</button>

								</div>
								<div class="col-md-4">
									<a href="https://smallpdf.com/edit-pdf" target="_blank" class="btn btn-warning" style="margin-top:22px;"><span class="fa fa-pencil"></span> Draw</a>
								</div>
								
							</div>
							
							

						</div>
					</div>
			</form>
						
					</div>
				</div>

		<div id="uploaded_file" style="height: 500px;overflow: auto;margin-top:10px;">
			
			<?php
			
			if(isset($_GET['search_student']))
			{
			$student_id = $_GET['student_id'];

			$sql001001 = mysqli_query($conn,"SELECT * FROM `student_essay_master` WHERE `STU_ID` = '$student_id'");

			$check = mysqli_num_rows($sql001001);

			if($check > 0)
			{
			while($row001001 = mysqli_fetch_assoc($sql001001))
			{
				$paper_name = $row001001['PAPER_FILE'];
				$stu_essay_id = $row001001['STU_ESSAY_ID'];

				$sql001002 = mysqli_query($conn,"SELECT * FROM `paper_image` WHERE `STU_ESSAY_ID` = '$stu_essay_id'");

				while($row001002 = mysqli_fetch_assoc($sql001002))
				{
					
					$img_name = $row001002['IMAGE_NAME'];

					echo '<img src="../paper_document/'.$img_name.'" class="image-responsive"style="width:100%;height:100%;" id="canvas_container2">';
				
				}


			}

			
			}else
			if($check == '0')
			{
				echo '<tr style="text-align:center;margin-top:20px;">
						<td colspan="3" class="text-center" style="text-align:center;margin-top:20px;"><div class="alert alert-danger" style="margin-top:20px;text-align:center;"><span class="fa fa-warning"></span> Not Found Data!</div></td>
					</tr>';
			}
		
		}else
		{
			echo '<tr style="text-align:center;margin-top:20px;">
					<td colspan="3" class="text-center" style="text-align:center;margin-top:20px;"><div class="alert alert-danger" style="margin-top:20px;text-align:center;"><span class="fa fa-warning"></span> Not Found Data!</div></td>
				</tr>';
		}


if(isset($_GET['download_student_paper']))
{
	$student_id = $_GET['student_id'];
	$paper_id = $_GET['paper_id'];


	$sql001001 = mysqli_query($conn,"SELECT * FROM `student_essay_master` WHERE `STU_ID` = '$student_id' AND `PAPER_ID` = '$paper_id'");

	while($row001001 = mysqli_fetch_assoc($sql001001))
	{
		$paper_name = $row001001['PAPER_FILE'];
		$stu_essay_id = $row001001['STU_ESSAY_ID'];

		$sql001002 = mysqli_query($conn,"SELECT * FROM `paper_image` WHERE `STU_ESSAY_ID` = '$stu_essay_id'");

		while($row001002 = mysqli_fetch_assoc($sql001002))
		{
			
			$postx[] = $row001002['IMAGE_NAME'];
		
		}


	}

	$sql002 = mysqli_query($conn,"SELECT * FROM `stu_login` WHERE `STU_ID` = '$student_id'");

	while($row002 = mysqli_fetch_assoc($sql002))
	{
			$reg_id = $row002['REGISTER_ID'];
	}

	$sql003 = mysqli_query($conn,"SELECT * FROM `paper_master` WHERE `PAPER_ID` = '$paper_id'");

	while($row003 = mysqli_fetch_assoc($sql003))
	{
			$paper_name = $row003['PAPER_NAME'];
	}

	/* Create ZIP */

	$error = ""; //error holder


	$file_folder = "../paper_document/"; // folder to load files
	if(extension_loaded('zip'))
	{ 

	// Checking files are selected
	$zip = new ZipArchive(); // Load zip library 
	$zip_name = $reg_id."(".$paper_name.").zip"; // Zip name
	if($zip->open($zip_name, ZIPARCHIVE::CREATE)!==TRUE)
	{ 
	// Opening zip file to load files
	$error .= "* Sorry ZIP creation failed at this time";
	}

	echo $size = sizeof($postx);

	for($i=0;$i<$size;$i++)
	{ 
	  $zip->addFile($file_folder.$postx[$i]); // Adding files into zip
	}

	$zip->close();
	if(file_exists($zip_name))
	{
	// push to download the zip
	header('Content-type: application/zip');
	header('Content-Disposition: attachment; filename="'.$zip_name.'"');
	readfile($zip_name);
	// remove zip file is exists in temp path
	unlink($zip_name);
	}


	$error .= "* Please select file to zip ";
	}

	/* Create ZIP */
	
}
		
			
			?>

	</div>
	


		</div>
		
	</div>
	
	<div class="col-md-6">

		<!-- Upload -->

		<h2 class="text-muted" style="font-weight:bold;"><span class="fa fa-upload"></span> Upload Paper</h2>
		<center>

			<output id="Filelist" style="height: 300px;overflow: auto;">

				<img src="../index/str/images/icon/upload.svg" style="width: 290px;height: 290px;" id="img_svg">

			</output>

		</center>


		<form action="../admin/query/update.php" method="POST" enctype="multipart/form-data" style="display: <?php echo $uploaded_ok; ?>">
			<input type="file" class="form-control" id="paper_file" name="upload_file[]" multiple accept="application/pdf" style="display: none;" onchange="uploaded()"/>

			<div id="open_file" onclick="document.getElementById('paper_file').click();" style="cursor: pointer;margin-top: 30px;">
				
					<button type="button" class="btn btn-block btn-sm" name="update_payment" style="padding: 32px 20px 20px 20px;border:3px dashed gray;outline: none;background-color: white;color:#545454;height: 100px;cursor: pointer;" value="Browse..."> <i class="fa fa-paperclip" style="font-size: 20px;"></i> <label style="font-weight: bold;cursor: pointer;font-size: 20px;">Upload PDF</label></button>



			</div>
			<br>
			
			<input type="hidden" name="paper_id" value="<?php echo $paper_id; ?>">

			<center><button type="button" class="btn btn-info btn-block" style="font-size: 16px;margin-top: 10px;background-color: #9c27b0;outline: none;border:none;" onclick="document.getElementById('submit_btn').click();" id="disable_btn"><span class="fa fa-check-circle"></span> Submit Paper</button></center>

			<button type="submit"  id="submit_btn" value="<?php echo $student_id; ?>" name="submit_paper" style="display: none;" data-target="#submit_modal" data-toggle="modal">X</button>


		</form>

		<!-- Upload -->

	</div>

</div>

</body>
</html>


<script type="text/javascript">
                    				
                    				document.getElementById('disable_btn').disabled = true;

                    				function clear_upload()
                    				{
                    					document.getElementById('paper_file').value = '';

                    					$("#open_file").html('<button type="button" class="btn btn-block btn-sm" name="update_payment"  style="padding: 32px 20px 20px 20px;border:3px dashed gray;outline: none;background-color: white;color:#545454;height: 100px;cursor: pointer;" value="Browse..."> <i class="fa fa-paperclip" style="font-size: 20px;"></i> <label style="font-weight: bold;cursor: pointer;font-size: 20px;">Upload Paper</label></button>');
                    				}

				                    function uploaded()
				                    {
				                        
				                        var paper_file = document.getElementById('paper_file').value;


				                        if(paper_file == '')
				                        {
				                          $("#open_file").html('<button type="button" class="btn btn-block btn-sm" name="update_payment"  style="padding: 32px 20px 20px 20px;border:3px dashed gray;outline: none;background-color: white;color:#545454;height: 100px;cursor: pointer;" value="Browse..."> <i class="fa fa-paperclip" style="font-size: 20px;"></i> <label style="font-weight: bold;cursor: pointer;font-size: 20px;">Upload Paper</label></button>');
				                        	
				                        	document.getElementById('disable_btn').disabled = true;


				                        }else
				                        if(paper_file !== '')
				                        {


				                                $("#open_file").html('<button type="button" class="btn btn-success btn-block btn-sm" name="update_payment" style="padding: 32px 20px 20px 20px;outline: none;height: 100px;cursor: pointer;background-color:#1abc9c;border:none;" value="Browse..."> <i class="fa fa-check-circle" style="font-size: 20px;"></i> <label style="font-weight: bold;cursor: pointer;font-size: 20px;">Upload Successfully</label></button>');

				                        		document.getElementById('disable_btn').disabled = false;

				                         
				                          
				                        }

				                        
				                    }

				                  </script>