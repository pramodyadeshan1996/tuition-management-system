<?php 

	session_start();
    include('../../connect/connect.php');

	$_SESSION['delete_msg'] = 'Deleted Data';

    if(isset($_GET['add_id']))
    {
    	$add_id = $_GET['add_id'];
    	$stu_id = $_GET['stu_id'];
    	$delete001 = mysqli_query($conn,"DELETE FROM add_subject WHERE ADD_SUB_ID = '$add_id'");

    	$selecte001 = mysqli_query($conn,"SELECT * FROM add_subject WHERE STU_ID = '$stu_id'");
    	$check = mysqli_num_rows($selecte001);

    	if($check == '0')
    	{
    		mysqli_query($conn,"UPDATE `student_details` SET `NEW`= '0'  WHERE `STU_ID` = '$stu_id'");
    	}

	    header('location:../profile/view_subjects.php');

	    
	}else
	if(isset($_GET['delete_lesson']))
    {
    	$file_name = $_GET['file_name'];
    	$upload_id = $_GET['upload_id'];
    	$class_id = $_GET['class_id'];
    	$teach_id = $_GET['teach_id'];
    	$type = $_GET['type'];

    	unlink("../../student/profile/teachers/".$teach_id."/".$class_id."/".$type."/".$type."/".$file_name."");
    	$delete001 = mysqli_query($conn,"DELETE FROM uploads WHERE UPLOAD_ID = '$upload_id'");

		
	    header('location:../profile/show_edit_upload.php?type='.$type.' ');

	}else
	if(isset($_GET['delete_class']))
    {
    	$class_id = $_GET['delete_class'];

    	$delete001 = mysqli_query($conn,"DELETE FROM classes WHERE CLASS_ID = '$class_id'");

		
	    header('location:../profile/create_class.php');

	    
	}else
	if(isset($_GET['delete_subject']))
    {
    	$sub_id = $_GET['delete_subject'];
    	$teach_id = $_GET['teach_id'];

    	$delete001 = mysqli_query($conn,"DELETE FROM teacher_subject WHERE SUB_ID = '$sub_id' AND TEACH_ID = '$teach_id'");

		
	    header('location:../profile/add_subject.php');

	    
	}else
	if(isset($_GET['delete_video_upload']))
    {
    	$upload_id = $_GET['delete_video_upload'];
    	$type = $_GET['type'];
    	$teach_id = $_GET['teach_id'];

    	$sql001 = mysqli_query($conn,"SELECT * FROM `uploads` WHERE `UPLOAD_ID` = '$upload_id'");
    	while($row001 = mysqli_fetch_assoc($sql001))
    	{
    		$file_name = $row001['FILE_NAME'];
    	}

    	if($type == 'audio' || $type == 'document')
    	{
    		unlink('../../uploads/'.$type.'/'.$file_name.'');
    	}

    	$delete001 = mysqli_query($conn,"DELETE FROM uploads WHERE UPLOAD_ID = '$upload_id'");

	    header('location:../../new_teacher/view_upload.php?teach_id='.$teach_id.'');

	    
	}else
	if(isset($_GET['delete_added_subject']))
    {
    	$ts_id = $_GET['delete_added_subject'];

    	$delete001 = mysqli_query($conn,"DELETE FROM teacher_subject WHERE T_S_ID = '$ts_id'");

	    header('location:../../new_teacher/add_subject.php');

	}else
	if(isset($_GET['delete_added_clz']))
    {
    	$clz_id = $_GET['delete_added_clz'];
    	$teach_id = $_GET['teach_id'];

    	$delete001 = mysqli_query($conn,"DELETE FROM classes WHERE CLASS_ID = '$clz_id'");

	    header('location:../../new_teacher/new_class.php?teach_id='.$teach_id.'');

	}else
	if(isset($_GET['delete_paper']))
    {
    	$paper_id = $_GET['delete_paper'];
    	$file_name = $_GET['file_name'];
    	$_SESSION['exam_type'] = $_GET['exam_type'];

    	//unlink('../images/paper_document/'.$file_name.'');

    	$delete001 = mysqli_query($conn,"DELETE FROM `paper_master` WHERE `PAPER_ID` = '$paper_id'");
    	header('location:../../new_teacher/exam.php');
    	
	}else
        if(isset($_GET['delete_subject2']))
        {
            $picture = $_GET['delete_subject2'];
            $sub_id = $_GET['sub_id'];

            if($picture !== 'subject.png')
            {
              unlink('../../admin/images/subject_image/'.$picture);
            }

            $sql001 = mysqli_query($conn,"SELECT * FROM `classes` WHERE `SUB_ID` = '$sub_id'");
            while($row001 = mysqli_fetch_assoc($sql001))
            {
                $class_id = $row001['CLASS_ID'];

                $delete002 = mysqli_query($conn,"DELETE FROM `payment_data` WHERE `CLASS_ID` = '$class_id'");
                $delete003 = mysqli_query($conn,"DELETE FROM `transaction` WHERE `CLASS_ID` = '$class_id'");
                $delete004 = mysqli_query($conn,"DELETE FROM `link_clicked` WHERE `CLASS_ID` = '$class_id'");
            }

            $delete001 = mysqli_query($conn,"DELETE FROM `subject` WHERE `SUB_ID` = '$sub_id'");

            $delete005 = mysqli_query($conn,"DELETE FROM `teacher_subject` WHERE `SUB_ID` = '$sub_id'");
            
            $delete006 = mysqli_query($conn,"DELETE FROM `classes` WHERE `SUB_ID` = '$sub_id'");



            header('location:../../new_teacher/add_subject.php');

        }else
		if(isset($_GET['delete_seminar']))
		{
			$seminar_id = $_GET['delete_seminar'];
			$page = $_GET['page'];
			$semi_teacher = $_GET['teach_id'];

			$delete001 = mysqli_query($conn,"DELETE FROM `seminar_master` WHERE `SEMI_ID` = '$seminar_id'");

		    header('location:../../new_teacher/seminar.php?teach_id='.$semi_teacher.'&&page='.$page.'');

		}else
		if(isset($_GET['delete_bd']))
		{
			$tbd_id = $_GET['delete_bd'];
			$teach_id = $_GET['teach_id'];

			$delete001 = mysqli_query($conn,"DELETE FROM `teacher_bank_details` WHERE `T_B_D_ID` = '$tbd_id'");

		    header('location:../../new_teacher/profile_setting.php');

		}else
          if(isset($_GET['delete_gf']))
          {
            $gf_id = $_GET['delete_gf'];
            $page = $_GET['page'];

            $delete001 = mysqli_query($conn,"DELETE FROM `g_f_master` WHERE `GF_MASTER_ID` = '$gf_id'");

            header('location:../../new_teacher/google_form.php?page='.$page.'');


      }else
			if(isset($_GET['delete_stu']))
			{
				$stu_id = $_GET['delete_stu'];
				$page = $_GET['page'];

				$delete001 = mysqli_query($conn,"DELETE FROM `stu_login` WHERE `STU_ID` = '$stu_id'");

		    	
		    $delete002 = mysqli_query($conn,"DELETE FROM `student_details` WHERE `STU_ID` = '$stu_id'");


			    header('location:../../new_teacher/student_approved.php?page='.$page.'');
			}

 ?>