

<?php
  $page = "Students Approved";
  $folder_in = '3';

      include('header/header.php');


      $sql = mysqli_query($conn,"SELECT * FROM institute WHERE INS_ID = '1'");
      while($row = mysqli_fetch_assoc($sql))
      {
        $ins_web = $row['WEB'];
      }

 ?>

    <ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-bottom: 04%;margin-bottom: 0;">
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="Student Approved"> Students Approved</li>
    </ol>


      <div class="col-md-12 bg-white" style="border:1px solid #cccc;height: auto;">
      
      <div class="col-md-12" style="border-bottom: 1px solid #cccc;">
        <div class="row">

            <div class="col-md-10" style="margin-bottom: 10px;"><h2><a href="dashboard.php" data-toggle="tooltip" data-title="Back"><i class="pg-icon" style="font-size: 22px;">chevron_left</i></a> Students Approved</h2></div>

            <div class="col-md-2" style="margin-bottom: 10px;">

              <a href="create_student.php" class="btn btn-info btn-block" style="margin-top:10px;padding: 12px 12px 12px 12px;"><i class="fa fa-plus"></i>&nbsp;Create Student</a>

            </div>

        </div>
      </div>

          
          <div class=" container-fluid container-fixed-lg bg-white" style="margin-top: 0px;">
            <div class="card card-transparent">
              <div class="card-header ">

                  <form action="student_approved.php" method="POST">
                      <div class="row">
                        <div class="col-md-2">
                          <div class="col-md-12" style="padding: 10px 10px 10px 10px;background-color: #000000b8;border-radius: 10px;" data-toggle="tooltip" data-title="Today Student Joined Amount">
                            <center><span class="fa fa-user text-white text-center pt-2" style="font-size: 18px;"></span>
                              <small class="text-white" style="font-size: 20px;">
                                <?php 

                                  $sql004 = mysqli_query($conn,"SELECT * FROM `stu_login` WHERE `STATUS` = 'Active'");
                                  $total_student = mysqli_num_rows($sql004);

                                  if($total_student < 10)
                                  {
                                    $total_student = "0".$total_student;
                                  }else
                                  {
                                    $total_student = number_format($total_student);
                                  }

                                  echo $total_student;
                                 ?>
                              </small>
                              <br>
                              <small class="text-white text-center">Total Student</small>
                            </center>
                          </div>
                        </div>
                        <div class="col-md-5"> </div>
                      <div class="col-md-3 mt-2">
                        <input list="student_search" id="search-table" name="search_student" class="form-control" placeholder="Search Register ID.." >

                        <datalist id="student_search">
                          <?php 

                            $sql002 = mysqli_query($conn,"SELECT * FROM `student_details`");
                            while($row002 = mysqli_fetch_assoc($sql002))
                            {
                              $name = $row002['F_NAME']." ".$row002['L_NAME'];
                              $student_id = $row002['STU_ID'];
                              $tp = $row002['TP'];

                              $sql003 = mysqli_query($conn,"SELECT * FROM `stu_login` WHERE `STU_ID` = '$student_id' AND `STATUS` = 'Active'");
                              while($row003 = mysqli_fetch_assoc($sql003))
                              {
                                
                                $register_id = $row003['REGISTER_ID'];
                              
                                echo '<option value="'.$register_id.'">'.$name.' | '.$tp.'</option>';
                              }

                              
                            }

                           ?>
                          
                        </datalist>
                      </div>
                      <div class="col-md-1 mt-2"> <button type="submit" class="btn btn-success btn-lg btn-block" name="search_student_btn" style="border-radius: 20px;"><i class="fa fa-search"></i> &nbsp; Search</button></div>

                      <div class="col-md-1 mt-2"> <button type="submit" class="btn btn-danger btn-lg btn-block" name="reset_student_btn" style="border-radius: 20px;"><i class="fa fa-list"></i> &nbsp; Show All</button></div>
                    </div>
                  </form>

                    <div class="clearfix"></div>
                </div>
                <div class="card-body">
                  <!--  style="height: 820px;overflow: auto;" -->
                <div class="table-responsive">
                <table class="table table-hover" id="tblFruits">
                  <thead>
                      <tr>
                        <th class="text-center" style="width: 1%;display: none;"><button class="btn btn-danger" onclick = "GetSelected()" style="border-radius: 80px;padding: 9px 10px 9px 10px;"><span class="fa fa-trash"></span></button></th>
                        <th style="width: 1%;">Registered ID</th>
                        <th style="width: 1%;">Name</th>
                        <th style="width: 1%;">DOB</th>
                        <th style="width: 1%;">Gender</th>
                        <th style="width: 1%;text-align: center;">Action</th>
                      </tr>
                    </thead>
                    <tbody>

                      <?php 
                          /*------------------ Header Pagination --------------------------------*/
                        $record_per_page = 10;

                        $nxt_five_loop = 0;

                        $page = '';

                        if(isset($_GET["page"]))
                        {
                          $page = $_GET["page"];
                        }
                        else
                        {
                         $page = 1;
                        }

                        $start_from = ($page-1)*$record_per_page; //Current page no -1 X Per Page records

                        if(isset($_POST['search_student_btn']))
                        {
                          $student_id = $_POST['search_student'];

                          $sql001 = "SELECT * FROM `stu_login` WHERE `STATUS` = 'Active' AND `REGISTER_ID` LIKE '%".$student_id."%' order by `STU_ID` DESC LIMIT $start_from, $record_per_page";


                        }else
                        {
                          $sql001 = "SELECT * FROM `stu_login` WHERE `STATUS` = 'Active'  order by `STU_ID` DESC LIMIT $start_from, $record_per_page";

                        }

                        
                          $page_query = "SELECT * FROM `stu_login` WHERE `STATUS` = 'Active'  ORDER BY `STU_ID` DESC";

                        /*------------------ Header Pagination --------------------------------*/

                        $j = 0;
                        $grade_name = 'N/A';
                        
                        //LIMIT = 10-1 x 10 = 90 , 10";
                        $result = mysqli_query($conn, $sql001);
                        $page_result = mysqli_query($conn, $page_query);

                          $check = mysqli_num_rows($result);

                          if($check>0)
                          {
                          while($row001 = mysqli_fetch_assoc($result))
                          {
                            $stu_id = $row001['STU_ID'];
                            $reg = $row001['REGISTER_ID'];
                            $register_date = $row001['REG_DATE'];
                            $password = $row001['PASSWORD'];

                            $j=$j++;

                            $sql002 = mysqli_query($conn,"SELECT * FROM `student_details` WHERE `STU_ID` = '$stu_id'");
                            while($row002 = mysqli_fetch_assoc($sql002))
                            {
                              $name = $row002['F_NAME']." ".$row002['L_NAME'];
                              $fnm = $row002['F_NAME'];
                              $lnm = $row002['L_NAME'];
                              $tp1 = $row002['TP'];
                              $tp = str_replace(" ","",$tp1);
                              $dob = $row002['DOB'];
                              $gender = $row002['GENDER'];
                              $email = $row002['EMAIL'];
                              $address = $row002['ADDRESS'];
                              $new = $row002['NEW'];
                              $grade_id = $row002['GRADE_ID'];

                              $sql1003 = mysqli_query($conn,"SELECT * FROM `grade` WHERE `GRADE_ID` = '$grade_id'");
                              while($row1003=mysqli_fetch_assoc($sql1003))
                              {
                                $grade_name = $row1003['GRADE_NAME'];
                              }

                            }

                            if(empty($_SESSION['notify_id']) || $_SESSION['notify_id'] !== $stu_id)
                            {
                              $notify = '';
                            }else
                            if($_SESSION['notify_id'] == $stu_id)
                            {
                              $notify = 'style="background-color:#dfe6e9;color:black;"';
                              ?>  

                              <script type="text/javascript">
                                setTimeout(function(){ 

                                  for(i=0;i<6;i++)
                                  {
                                    document.getElementsByClassName('recolor<?php echo $stu_id; ?>')[i].style.backgroundColor  = 'white'; //Recolor

                                    document.getElementsByClassName('recolor<?php echo $stu_id; ?>')[i].style.color  = 'black'; //Recolor
                                  }
                                 
                                  }, 4000);
                                
                              </script>

                              <?php

                              unset($_SESSION['notify_id']);
                            }

                            $sql1001 = mysqli_query($conn,"SELECT * FROM `register_stu_sub`WHERE `STU_ID` = '$stu_id'");
                            $check_data = mysqli_num_rows($sql1001);


                            echo '

                              <tr>
                                <td class="v-align-middle text-center recolor'.$stu_id.'" '.$notify.' style="display:none;">

                                    <input type="checkbox" id="chkMango" name="multi_delete[]" style="zoom:1.3;" value="'.$stu_id.'">

                                </td>
                                <td class="v-align-middle recolor'.$stu_id.'" '.$notify.'>'.$reg.'</td>
                                <td class="v-align-middle recolor'.$stu_id.'" '.$notify.'>'.$name.'<br>(<small><b>'.$grade_name.'</b>)</small></td>
                                <td class="v-align-middle recolor'.$stu_id.'" '.$notify.'>'.$dob.'</td>
                                <td class="v-align-middle recolor'.$stu_id.'" '.$notify.'>'.$gender.'</td>
                                 <td class="v-align-middle text-center recolor'.$stu_id.'" '.$notify.'>';

                                 if($check_data > 0)
                                 {
                                    echo '<a href="register_student.php?reg_id='.$reg.'&&page='.$page.'" class="btn btn-warning btn-xs btn-rounded mt-1 mr-1" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;" data-title="Select Teachers for Class" data-toggle="tooltip"><i class="pg-icon">plus</i></a>';
                                 }

                                 echo '


                                <button type="button" data-toggle="modal" data-target="#send_timetable'.$stu_id.'" class="btn btn-complete btn-xs btn-rounded mt-1 mr-1" style="padding:7px 7px;box-shadow:0px 0px 4px 3px #cccc;display:none;" data-title="View Details" data-toggle="tooltip"><i class="fa fa-send"></i></button>


                                <button type="button" data-toggle="modal" data-target="#view_student'.$stu_id.'" class="btn btn-primary btn-xs btn-rounded mt-1 mr-1" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;" data-title="View Details" data-toggle="tooltip"><i class="pg-icon">expand</i></button>

                                <button type="button" data-toggle="modal" data-target="#stu_data'.$stu_id.'" class="btn btn-success btn-xs btn-rounded mt-1 mr-1" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;display:none;" data-title="Approve" data-toggle="tooltip" id="edit1'.$stu_id.'"><i class="pg-icon">edit</i></button>
                                ' ?>
                                <a href="../teacher/query/delete.php?delete_stu=<?php echo $stu_id; ?>&&page=<?php echo $page; ?>" onclick="return confirm('Are you sure delete?')" class="btn btn-danger mt-1 mr-1 btn-xs btn-rounded" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;display:none;" data-title="Delete" data-toggle="tooltip"><i class="pg-icon">trash_alt</i></a>

                                <?php echo '

                                </td>
                              </tr>

                            ';
                        ?>

                        <!-- Modal -->
                        <div id="send_timetable<?php echo $stu_id; ?>" class="modal fade" role="dialog">
                          <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                              <div class="modal-header">
                                <h4 class="modal-title">Send SMS Timetable</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                              </div>
                              <form action="../teacher/query/insert.php" method="POST">
                              <div class="modal-body">
                                
                                <div class="row">
                                  <div class="col-md-12">
                                    <div class="form-group form-group-default">
                                      <label>Text Message</label>
                                      <textarea name="sms" required placeholder="xxxxxxxx" style="margin-bottom: 10px;height: 90px;" class="form-control">
ඔබගේ පන්ති කාලසටහන පහත Link එක මගින් ලබාගන්න.
<?php echo $ins_web; ?>/app/sms_login/index.php?reg_id=<?php echo $reg; ?>
  
</textarea>
                                    </div>
                                  </div>
                                </div>

                                <input type="hidden" name="tp" value="<?php echo $tp; ?>">
                                <input type="hidden" name="page" value="<?php echo $page; ?>">

                              </div>
                              <div class="modal-footer">
                                <button type="submit" class="btn btn-success" name="sms_timetable" value="<?php echo $reg; ?>"><i class="fa fa-send"></i>&nbsp; Send</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                              </div>
                              </form>
                            </div>

                          </div>
                        </div>



                                      <div class="modal fade slide-up disable-scroll" id="stu_data<?php echo $stu_id; ?>" tabindex="-1" role="dialog" aria-hidden="false" style="overflow: auto;">
                                        <div class="modal-dialog ">
                                          <div class="modal-content-wrapper">
                                            <div class="modal-content modal-lg">
                                              <div class="modal-header clearfix text-left">
                                                <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
                                                </button>
                                                <h5>Students Details <br><small>Registered Time - <?php echo $register_date; ?></small></h5>
                                              </div>
                                              <div class="modal-body">

                                                  <form  action="../teacher/query/update.php" method="POST" id="form-personal" role="form" autocomplete="off">


                                                        <div class="row">
                                                        <div class="col-md-6">
                                                        <div class="form-group form-group-default input-group">
                                                        <div class="form-input-group">
                                                        <label>First Name</label>
                                                        <input type="text" name="fnm" class="form-control change_inputs" value="<?php echo $fnm; ?>" pattern="[a-zA-Z. ]+"  required>
                                                        </div>
                                                        </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                        <div class="form-group form-group-default input-group">
                                                        <div class="form-input-group">
                                                        <label>Last Name</label>
                                                        <input type="text" name="lnm" class="form-control change_inputs" value="<?php echo $lnm; ?>" pattern="[a-zA-Z. ]+"  required>
                                                        </div>
                                                        </div>
                                                        </div>

                                                        </div>

                                                        <div class="row">
                                                        <div class="col-md-6">
                                                        <div class="form-group form-group-default input-group">
                                                        <div class="form-input-group">
                                                        <label>Date Of Birth</label>
                                                        <input type="date" class="form-control change_inputs" name="dob" required min="<?php $d = strtotime("-60 year"); echo date('Y-m-d',$d); ?>" max="<?php $d = strtotime("-5 year"); echo date('Y-m-d',$d); ?>" style="margin-bottom: 10px;" value="<?php echo $dob; ?>">
                                                        </div>
                                                        </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                        <div class="form-group form-group-default input-group">
                                                        <div class="form-input-group">
                                                        <label>Telephone No</label>
                                                        <input class="form-control change_inputs" type="telephone" pattern="[0-9]{10}" minlength="10" maxlength="10" name="number" id="number" value="<?php echo $tp; ?>" required placeholder="0xxx xxxx xx" style="margin-bottom: 10px;">
                                                        </div>
                                                        </div>
                                                        </div>

                                                        </div>

                                                        <div class="row">
                                                        <div class="col-md-12">
                                                        <div class="form-group form-group-default input-group">
                                                        <div class="form-input-group">
                                                        <label>E-mail Address</label>
                                                        <input type="text" class="form-control change_inputs" name="email" value="<?php echo $email; ?>"  style="margin-bottom: 10px;text-transform: lowercase;">
                                                        </div>
                                                        </div>
                                                        </div>
                                                        </div>
                                                        <div class="row">
                                                        <div class="col-md-12">
                                                        <div class="form-group form-group-default">
                                                        <label>Address</label>
                                                        <textarea name="address" required placeholder="xxxxxxxx" style="margin-bottom: 10px;" class="form-control change_inputs"><?php echo $address; ?></textarea>
                                                        </div>
                                                        </div>
                                                        </div>
                                                        <div class="row">
                                                        <div class="col-md-12">
                                                        <div class="form-group form-group-default">
                                                        <label>Gender</label>
                                                        <div class="row">
                                                        <?php 
                                                            if($gender == 'Male')
                                                            {?>



                                                              <div class="col-md-6" style="padding-left: 6px;">

                                                                <label><span class="fa fa-male"></span> Male : <input type="radio" checked="checked" name="gender" value="Male" class="change_inputs"></label>
                                                              </div>
                                                              <div class="col-md-6" style="padding-left: 6px;">

                                                                <label style="padding-left: 10px;margin-bottom: 10px;"><span class="fa fa-female"></span> Female : <input type="radio" name="gender" value="Female" class="change_inputs"> </label>

                                                              </div>
                                                              <?php
                                                            }else
                                                            if($gender == 'Female')
                                                            {?>



                                                              <div class="col-md-6" style="padding-left: 6px;"><label><span class="fa fa-male"></span> Male : <input type="radio"name="gender" value="Male" class="change_inputs"></label></div>

                                                              <div class="col-md-6" style="padding-left: 6px;"><label style="padding-left: 10px;margin-bottom: 10px;"><span class="fa fa-female"></span> Female : <input type="radio" checked="checked"  name="gender" value="Female" class="change_inputs"> </label></div>
                                                              <?php
                                                            }
                                                           ?>
                                                           </div>
                                                        </div>
                                                        </div>
                                                        

                                                        <div class="col-md-6" style="display: none;">
                                                        <div class="form-group form-group-default" style="height: 62px;">
                                                        <label>Are you already registered student?</label>
                                                        <div class="row">
                                                        <?php 
                                                            if($new == '1')
                                                            {?>



                                                              <div class="col-md-6" style="padding-left: 6px;">

                                                                <select class="form-control" name="new">
                                                                  <option value="1">Yes</option>
                                                                  <option value="0">No</option>
                                                                </select>

                                                              </div>
                                                              <?php
                                                            }

                                                            if($new == '0')
                                                            {?>

                                                              <div class="col-md-6" style="padding-left: 6px;">

                                                                <select class="form-control" name="new">
                                                                  <option value="0">No</option>
                                                                  <option value="1">Yes</option>
                                                                </select>

                                                              </div>

                                                              
                                                            <?php
                                                            }
                                                           ?>


                                                           </div>
                                                        </div>
                                                        </div>

                                                        </div>
                                                        <input type="hidden" value="<?php echo $page ?>" name="page"> <!-- Page No -->

                                                        <div class="clearfix"></div>
                                                        <div class="row m-t-25">
                                                        <div class="col-xl-6 p-b-10">
                                                        <p class="small-text hint-text">Click the Update button to change all the data you have changed. (ඔබ වෙනස් කල සියලු දත්තයන් වෙනස් කිරීමට Update බොත්තම ඔබන්න.)</p>
                                                        </div>
                                                        <div class="col-xl-6">
                                                        <button aria-label="" id="up_btn" class="btn btn-primary pull-right btn-lg btn-block" type="submit" name="update_data" value="<?php echo $stu_id; ?>">Update
                                                        </button>
                                                        </div>
                                                        </div>
                                                        </form>

                                              </div>

                                            </div>
                                          </div>
                                        </div>
                                      </div>



                                      <div class="modal fade slide-up disable-scroll" id="view_student<?php echo $stu_id; ?>" tabindex="-1" role="dialog" aria-hidden="false" style="overflow: auto;">
                                        <div class="modal-dialog ">
                                          <div class="modal-content-wrapper">
                                            <div class="modal-content modal-lg">
                                              <div class="modal-header clearfix text-left">
                                                <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
                                                </button>
                                                <h5>Students Details <br><small>Registered Time - <?php echo $register_date; ?></small></h5>
                                              </div>
                                              <div class="modal-body">

                                                  <form  action="../teacher/query/update.php" method="POST" id="form-personal" role="form" autocomplete="off">


                                                        <div class="row">

                                                          <div class="col-md-6">
                                                          <div class="form-group form-group-default">
                                                          <label>Registered ID</label>

                                                          <h5><?php echo $reg; ?></h5>

                                                          </div>
                                                          </div>
                                                        <div class="col-md-6">
                                                        <div class="form-group form-group-default input-group">
                                                        <div class="form-input-group">
                                                        <label>Full Name</label>
                                                        <div class="col-md-12" style="padding-left:10px;text-transform: capitalize;"><h5><?php echo $name; ?></h5></div>
                                                        </div>
                                                        </div>
                                                        </div>



                                                        </div>

                                                        <div class="row">

                                                        <div class="col-md-12">
                                                        <div class="form-group form-group-default input-group">
                                                        <div class="form-input-group">
                                                        <label>Date Of Birth</label>
                                                        <div class="col-md-12" style="padding-left:10px;"><h5><?php echo $dob; ?></h5></div>
                                                        </div>
                                                        </div>
                                                        </div>
                                                        <div class="col-md-6" style="display:none;">
                                                        <div class="form-group form-group-default input-group">
                                                        <div class="form-input-group">
                                                        <label>Telephone No</label>
                                                        <div class="col-md-12" style="padding-left:10px;"><h5><?php echo $tp; ?></h5></div>
                                                        </div>
                                                        </div>
                                                        </div>

                                                        </div>
                                                        <div class="row">
                                                          <div class="col-md-6">
                                                          <div class="form-group form-group-default">
                                                          <label>Address</label>

                                                          <h5><?php echo $address; ?></h5>

                                                          </div>
                                                          </div>

                                                        <div class="col-md-6">
                                                        <div class="form-group form-group-default input-group">
                                                        <div class="form-input-group">
                                                        <label>E-mail Address</label>

                                                        <div class="col-md-12" style="padding-left:10px;"><h5><?php if($email == ''){ echo 'N/A'; }else { echo $email;} ?></h5></div>
                                                        </div>
                                                        </div>
                                                        </div>

                                                        </div>  


                                                        <div class="row">
                                                          <div class="col-md-6">
                                                          <div class="form-group form-group-default">
                                                          <label>Gender</label>

                                                          <h5><?php echo $gender; ?></h5>

                                                          </div>
                                                          </div>
                                                          <div class="col-md-6">
                                                          <div class="form-group form-group-default">
                                                          <label>Grade</label>

                                                          <h5><?php 

                                                            $sql002 = mysqli_query($conn,"SELECT * FROM `grade` WHERE `GRADE_ID` = '$grade_id'");

                                                            $check_grade = mysqli_num_rows($sql002);

                                                            if($check_grade > 0)
                                                            {
                                                              while($row002 = mysqli_fetch_assoc($sql002))
                                                              {
                                                                  $grade_id =$row002['GRADE_ID'];
                                                                  echo $grade_nm =$row002['GRADE_NAME'];
                                                              }
                                                            }else
                                                            if($check_grade == '0')
                                                            {
                                                              echo 'N/A';
                                                            }
                                                            
                                                            

                                                           ?></h5>

                                                          </div>
                                                          </div>


                                                        </div>
      <div class="col-md-12" style="padding:6px 6px;height: 200px;overflow: auto;">
        <div class="row">
          <div class="col-md-5" style="border:1px solid #cccc;padding: 10px 10px 10px 10px;font-size: 16px;font-weight: bold;">Level Name</div>
          <div class="col-md-7" style="border:1px solid #cccc;padding: 10px 10px 10px 10px;font-size: 16px;font-weight: bold;">Subject Name</div>
        </div>

      <?php 

                $sql0016 = mysqli_query($conn,"SELECT * FROM `stu_login` WHERE `REGISTER_ID` = '$reg'");
                while($row0016=mysqli_fetch_assoc($sql0016))
                {
                  $stu_id = $row0016['STU_ID'];

                    $sql1001 = mysqli_query($conn,"SELECT * FROM `register_stu_sub`WHERE `STU_ID` = '$stu_id'");
                    $check_data = mysqli_num_rows($sql1001);

                    if($check_data>0)
                    {
                      while($row1001 = mysqli_fetch_assoc($sql1001))
                      {
                          $level_id = $row1001['LEVEL_ID'];
                          $sub_id = $row1001['SUB_ID'];

                          
                          $sql1003 = mysqli_query($conn,"SELECT * FROM `subject` WHERE `SUB_ID` = '$sub_id'");
                          while($row1003=mysqli_fetch_assoc($sql1003))
                          {
                            $sub_name = $row1003['SUBJECT_NAME'];
                            $sub_id = $row1003['SUB_ID'];
                            $l_id = $row1003['LEVEL_ID'];

                            $sql1002 = mysqli_query($conn,"SELECT * FROM `level` WHERE `LEVEL_ID` = '$l_id'");
                            while($row1002=mysqli_fetch_assoc($sql1002))
                            {
                              $level_name = $row1002['LEVEL_NAME'];
                              $level_id = $row1002['LEVEL_ID'];
                            }
                          }

                          echo '
                            
                            <div class="row">
                              <div class="col-md-5" style="border:1px solid #cccc;padding: 10px 10px 10px 10px;font-size: 16px;">'.$level_name.'</div>
                              <div class="col-md-7" style="border:1px solid #cccc;padding: 10px 10px 10px 10px;font-size: 16px;">'.$sub_name.'</div>
                            </div>

                          ';
                      }
                    }else
                    if($check_data == '0')
                    {
                      echo '<div class="row"><div class="col-md-12 text-center" style="border:1px solid #cccc;padding: 10px 10px 10px 10px;font-size: 16px;"><span class="fa fa-warning"></span> Not Found Data!</div></div>';
                    }


                }

              ?>
            </div>
          </form>
                                              </div>

                                            </div>
                                          </div>
                                        </div>
                                      </div>
                        <?php
                          }
                        }else
                        if($check == '0')
                        {
                          echo '<tr><td colspan="7" class="text-center text-danger"><span class="fa fa-warning"></span> Not Found Data</td></tr>';
                        } 
                      

                        

                        
                         ?>


                    </tbody>
                </table>
                </div>
              </div>

                <div class="col-md-12" style="width: 100%;overflow: auto;">
                  <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-8" class="php_pagination">
                      
                      <div class="col-md-12">
                    <br />
                    <?php
                    $total_records = mysqli_num_rows($page_result);
                    $total_pages = ceil($total_records/$record_per_page);
                    $start_loop = $page;
                    $difference = $total_pages - $page;
                    if($difference <= 5)
                    {
                     $start_loop = $total_pages - 5;
                    }else
                    if($difference <= 0)
                    {
                      $start_loop = '0';
                    }

                    echo '<ul class="pagination">';
                    $end_loop = $start_loop + 4;
                    $five_loop = $page-5;
                    if($five_loop <= 0)
                    {
                      $five_loop = 1;
                    }
                    if($page > 1)
                    {

                      echo '<li><a href="student_approved.php?page=1">First</a></li>';
                      echo '<li><a href="student_approved.php?page='.($page - 1).'"> < </a></li>';
                      echo '<li><a href="student_approved.php?page='.$five_loop.'"> <<< </a></li>';

                    }
                    if($start_loop !== '1')
                    {
                      $few_no = $start_loop - 1;
                    }else
                    if($start_loop > 0)
                    {
                      $few_no = 1;
                    }
                    
                    for($i=$few_no; $i<=$end_loop+1; $i++)
                    {     
                      if($i == $page)
                      {
                        echo '<li class="active"><a href="student_approved.php?page='.$i.'" style="font-weight:bold;background-color:#2980b9;color:white;">'.$i.'</a></li>';
                      }else
                      {
                        if($i == '0')
                        {
                          continue;
                        }else
                        if($i>0)
                        {
                          echo '<li><a href="student_approved.php?page='.$i.'" >'.$i.'</a></li>';
                        }
                        
                      }

                    }
                    if($page <= $end_loop)
                    {
                      
                      if($page == '1')
                      {
                        $nxt_five_loop = 5;
                      }else
                      if($page >= 5)
                      {
                        $nxt_five_loop = $page+5;

                        if($total_pages < $nxt_five_loop)
                        {
                          $nxt_five_loop = $total_pages;
                        }
                      }else
                      {
                        $nxt_five_loop = $page+5;
                      }

                        echo '<li><a href="student_approved.php?page='.($page + 1).'" > > </a></li>';
                        echo '<li><a href="student_approved.php?page='.$nxt_five_loop.'" > >>> </a></li>';
                        echo '<li><a href="student_approved.php?page='.$total_pages.'" >Last</a></li>';
                      
                    }
                    
                    echo '</ul>';
                    
                    ?>
                    </div>

                    </div>
                  </div>
                    
                </div>
            </div>
            </div>
            </div>
            </div>
            </div>
            </div>

<?php include('footer/footer.php'); ?>

<script type="text/javascript">
  function sortTable(table, order) {
    var asc   = order === 'asc',
        tbody = table.find('tbody');
    
    tbody.find('tr').sort(function(a, b) {
        if (asc) {
            return $('td:first', a).text().localeCompare($('td:first', b).text());
        } else {
            return $('td:first', b).text().localeCompare($('td:first', a).text());
        }
    }).appendTo(tbody);
}
</script>

<script type="text/javascript">
    function GetSelected() {
        //Create an Array.
        var selected = new Array();
 
        //Reference the Table.
        var tblFruits = document.getElementById("tblFruits");
 
        //Reference all the CheckBoxes in Table.
        var chks = tblFruits.getElementsByTagName("INPUT");
 
        // Loop and push the checked CheckBox value in Array.
        for (var i = 0; i < chks.length; i++) {
            if (chks[i].checked) 
            {
                var s = selected.push(chks[i].value);

            }
        }
 
        //Display the selected CheckBox values.
        if (selected.length > 0) {
          if(confirm('Are you sure delete?'))
          {

              $.ajax({
               type: "POST",
               data: {delete_multi:selected},
               url: "../teacher/query/delete.php",
               success: function(msg){
                 //alert(msg)
                 location.reload();
               }
            
              });
          }
        }else{
         
         alert("Please select student!")
        
        }
    };
</script>