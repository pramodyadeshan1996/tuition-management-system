<?php $id = $_GET['regid'];
    include('../connect.php');
    $qu = mysqli_query($conn,"SELECT * FROM register WHERE REG_ID = '$id'");
    while($rowww = mysqli_fetch_assoc($qu))
    {
        
        $lan = $rowww['LANGUAGE'];
        if($lan == 'English')
        {
            $here = "profile.php";
        }else
        if($lan == 'Sinhala')
        {
            $here = "profile-si.php";
        }
    }
 ?>
<!doctype html>

<html lang="en">
<head>
    <meta charset="utf-8">

    <title>Upload Images</title>
    <meta name="description" content="Ajax Multiple Image Uploader">
    <meta name="author" content="tharindulucky">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link rel="stylesheet" href="dist/css/styles.css">

    <!-- Latest compiled JavaScript -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <!-- These are the necessary files for the image uploader -->
    <script src="dist/assets/jquery-file-upload/js/vendor/jquery.ui.widget.js"></script>
    <script src="dist/assets/jquery-file-upload/js/jquery.iframe-transport.js"></script>
    <script src="dist/assets/jquery-file-upload/js/jquery.fileupload.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>

<div class="container">

<div class="row">
        <div class="col-md-6" style="border:2px solid #e8e5e5;padding: 18px 18px 18px 18px;border-radius: 10px;margin:10% 25%;">
            <div class="container1"><a href="../<?php echo $here; ?>?id=<?php echo $id; ?>" style="font-size: 20px;padding: 4px 12px 4px 12px;border-radius: 80px;" class="btn btn-default"><span class="fa fa-arrow-left"></span></a>
                <h3 style="text-align: center"><i class="fa fa-image"></i> Upload Image</h3>
               <div>
                    <form method="POST" action="server/form_process.php" enctype="multipart/form-data">
                        <div id="select_file">
                            <div class="form-group">
                                <hr>
                                <!-- The fileinput-button span is used to style the file input field as button -->
                    <center><span class="btn btn-success fileinput-button">
                                <i class="glyphicon glyphicon-plus"></i>
                                <span>Select Image...</span>
                                    <!-- The file input field used as target for the file upload widget -->
                                <input type="file" name="up" accept="image/*" required>
                            </span></center>
                                <!-- The container for the uploaded files -->
                                <input type="hidden" name="lang" value="english">
                                <input type="hidden" name="id" value="<?php echo $id; ?>">
                                <br>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary" name="submit" style="float: right;">Submit</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>