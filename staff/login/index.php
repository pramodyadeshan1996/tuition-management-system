<?php 
    session_start();
    include('../../connect/connect.php');

    $suspend_sql = mysqli_query($conn,"SELECT * FROM `institute` WHERE `INS_ID` = '1'");
    
    while($suspend_row=mysqli_fetch_assoc($suspend_sql))
    {
      $check_suspend = $suspend_row['SUSPEND'];
    }

    if($check_suspend > 0)
    {
       include('../../suspend/suspended.php');

    }else
    if($check_suspend == '0')
    {
      ?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Login</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link href="https://fonts.googleapis.com/css?family=Muli&display=swap" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

  <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>


</head>

<body class="bg-gradient-primary" style="font-family: 'Muli', sans-serif;background-image: url('../images/bg.jpeg');background-size: cover;background-repeat: no-repeat;width: 100%;">

  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">
     <div class="col-xl-4 col-lg-4 col-md-4"></div>

      <div class="col-md-4" style="border:1px solid #00000000;margin-top: 10%;background-color: #000000c9;padding: 40px 40px 40px 40px;border-radius: 4px;">

                  <div class="text-center">
                    <h1 style="color: white;">Welcome!</h1>
                  </div>
                    <br>

                    <div class="form-group">
                      <input type="text" class="form-control form-control-user" aria-describedby="emailHelp" placeholder="Enter Username..." name="username" style="padding-left: 24px;height:50px;border-radius: 50px;" id="user" required autocomplete="off" autofocus="on">
                    </div>
                    <div class="form-group">
                      <input type="password" class="form-control form-control-user" placeholder="Enter Password" name="password" style="padding-left: 24px;height:50px;border-radius: 50px;" id="psw" required autocomplete="off">
                    </div>
                    
                    <button type="submit" class="btn btn-info btn-active btn-user btn-block" name="submit" id="log" style="margin-top: 40px;height:50px;border-radius: 50px;font-size: 16px;outline: none;"> <span class="fa fa-arrow-right"></span>
                      Login
                    </button>

                <div id="result" style="margin-top: 10px;"></div>
                <div class="col-md-12" style="text-align: center;"><small style="color:white;">Staff Login Page - &copy;IBS Developer Team</small>
                  <center><a href="../../index.php"><span class="fa fa-home" style="color:#ffff;font-size: 22px;margin-top: 10px;"></span></a></center>
                </div>

          </div>

      <div class="col-xl-4 col-lg-4 col-md-4"></div>

    </div>

  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>
  <script type="text/javascript">
  
  
  /*login script*/
  $("#log").click(function(){
    var user = $('#user').val();
    var pasw = $('#psw').val();

    if(user == '')
    {
      $("#result").html("<div class='alert alert-danger alert-dismissible' id='alert'><button type='button' class='close' data-dismiss='alert'>&times;</button><span class='fa fa-warning'></span> <strong>Warning : </strong> Empty Feild.Please fill the Inputs.</div>");
      $("#alert").delay(3000).slideUp(500, function() {
            $("#alert").slideUp(800);              
      });
    }
    else
    if( pasw == '')
    {
      $("#result").html("<div class='alert alert-danger alert-dismissible' id='alert'><button type='button' class='close' data-dismiss='alert'>&times;</button><span class='fa fa-warning'></span> <strong>Warning : </strong> Empty Feild.Please fill the Inputs.</div>");
      $("#alert").delay(3000).slideUp(500, function() {
            $("#alert").slideUp(800);              
      });
    }
    else
    if(user !== '' && pasw !== '')
    {
      $.ajax({  
                     url:"log.php",  
                     method:"POST",  
                     data:{user:user,psw:pasw},  
                     success:function(data){ 

                      //alert(data)
                     if(data == 0)
                     {
                          Swal.fire({
                          position: 'top-middle',
                          type: 'error',
                          title: 'Username Or Password Incorrect',
                          showConfirmButton: false,
                          timer: 1500
                          })
                          $("#psw").focus();
                          $("#psw").val('');
                     }

                     if(data == 2)
                     {
                          $("#result").html("<div class='alert alert-danger alert-dismissible' id='alert'><button type='button' class='close' data-dismiss='alert'>&times;</button><span class='fa fa-warning'></span> <strong>Warning : </strong> Sorry, your registered account has not been verified yet.</div>");
                     }

                     if(data == 1)
                     {
                        window.location.href = "../../new_admin/dashboard.php";
                     }
                    }
      });
    }
  });
  /*login script*/
  

  /*Enter key press after login to page*/
  $('body').on( 'keyup', function( e ) {
            if( e.which == 13 ) {
              //GOTO DOWN
              $('#log').click();
            }
            
  });
  /*Enter key press after login to page*/


</script>

</body>

</html>
<?php 
  }
?>