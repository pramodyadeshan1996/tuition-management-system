<?php 

	session_start();
    include('../../connect/connect.php');

    $_SESSION['save_msg'] = 'Saved Data';

    if(isset($_POST['save_data']))
    {
    	$fnm = ucwords($_POST['first_name']);
    	$lnm = ucwords($_POST['last_name']);
    	$dob = date("Y-m-d", strtotime($_POST['dob']));
    	$gen = $_POST['gender'];
    	$email = $_POST['email'];
    	$tp = $_POST['number'];
    	$address = $_POST['address'];
        $reg_id = $_POST['username'];
        $qualifi = $_POST['qualification'];
    	$psw = md5($_POST['password']);

        $position = $_POST['position'];

        $today = date('Y-m-d h:i:s A');


    	$insert001 = mysqli_query($conn,"INSERT INTO `teacher_details`( `POSITION`, `F_NAME`, `L_NAME`, `DOB`, `ADDRESS`, `TP`, `PICTURE`, `EMAIL`, `ZOOM_LINK`, `QUALIFICATION`, `GENDER`, `INTRODUCTION`, `INTRO_VIDEO`) VALUES ('$position','$fnm','$lnm','$dob','$address','$tp','0','$email','0','$qualifi','$gen','0','0')");
    	
    	$teach_id = mysqli_insert_id($conn);

    	$insert002 = mysqli_query($conn,"INSERT INTO `teacher_login`(`USERNAME`, `PASSWORD`, `STATUS`, `TEACH_ID`,`REG_DATE`, `CONFIRM`) VALUES ('$reg_id','$psw','Deactive','$teach_id','$today','0')");

        $_SESSION['teacher_info'] = 'register';


        header('location:../login/index.php');

    	
    }else
    if(isset($_GET['add_subject']))
    {
        $stu_id = $_GET['student_id'];
        $sub_id = $_GET['subject_id'];
        $level_id = $_GET['level_id'];
        $tsid = $_GET['t_s_id'];
        $page = $_GET['page'];

        $add_time = date('Y-m-d h:i:s A');

        $insert003 = mysqli_query($conn,"INSERT INTO `add_subject`(`STU_ID`, `T_S_ID`, `ADD_DATE`) VALUES ('$stu_id','$tsid','$add_time')");
        $update003 = mysqli_query($conn,"UPDATE student_details SET NEW = '1' WHERE STU_ID = '$stu_id'");

        header('location:../profile/teachers_subjects.php?subject_id='.$sub_id.'&&level_id='.$level_id.'&&page='.$page.' ');

    }else
    if(isset($_POST['create_text_btn']))
    {
        $title = $_POST['title'];
        $description = $_POST['descip'];
        $teach_id = $_POST['teach_id'];
        $update_date = date('Y-m-d h:i:s A');
        $insert001 = mysqli_query($conn,"INSERT INTO `ads`(`DESCRIPTION`, `FILE_NAME`, `UPLOAD_DATE`, `TEACH_ID`, `TYPE`, `EXTENTION`, `TITLE`) VALUES ('$description','0','$update_date','$teach_id','TEXT','0','$title')");
        
        header('location:../profile/ads.php');
        
    }else
    if(isset($_POST['multi_add']))
    {
        $btn = $_POST['multi_add'];

        $teach_id = $_POST['teach_id1'];

        if($_POST['title'] == '')
        {
            $title = "0";
        }else
        {
            $title = $_POST['title'];
        }

        $type = $_POST['type'];

        $up_date = date('Y-m-d h:i:s A');

        $file = $_FILES['upload_file1']['name'];

        $tmp_file = $_FILES['upload_file1']['tmp_name']; //ASSIGN DATA TO VARIABLE
        $ext = pathinfo($_FILES["upload_file1"]["name"], PATHINFO_EXTENSION); //GET EXTENTION ON IMAGE
        $rand = hexdec(uniqid()); //NAME CONVERT TO ENCRYPT
        $post_file = $rand.".".$ext; //ADD IMAGE NAME TO ENCRYPTED NAME
        move_uploaded_file($tmp_file,"../../document/".$post_file); //SAVE LOCATION


        $insert001 = mysqli_query($conn,"INSERT INTO `ads`(`DESCRIPTION`, `FILE_NAME`, `UPLOAD_DATE`, `TEACH_ID`, `TYPE`, `EXTENTION`, `TITLE`) VALUES ('0','$post_file','$up_date','$teach_id','$type','$ext','$title')");

        header('location:../profile/ads.php');
        
    }else
    if(isset($_POST['create_subject']))
    {
        $subject = $_POST['subject'];
        $teach_id = $_POST['create_subject'];

        $insert003 = mysqli_query($conn,"INSERT INTO `teacher_subject`(`SUB_ID`, `TEACH_ID`) VALUES ('$subject','$teach_id')");


        header('location:../profile/add_subject.php');

    }else
    if(isset($_POST['video_upload_btn']))
    {
        $teach_id = $_POST['video_upload_btn'];
        $title = $_POST['title'];
        $descip = $_POST['descip'];
        $select_class = $_POST['clz'];
        $subject = $_POST['select_subject'];
        $type = $_POST['type'];

       $location_media_type = strtolower($type);
       $ytd= $_POST['ytd'];

       $up_date = date('Y-m-d');
      $up_time = date('h:i:s A');
      $now_time = date('Y-m-d h:i:s A');

      $insert001 = mysqli_query($conn,"INSERT INTO `uploads`(`FILE_NAME`, `TITLE`, `DESCRIPTION`, `UPL_DATE`, `UPL_TIME`, `TYPE`, `EXTENSION`, `CLASS_ID`, `YOUTUBE_LINK`, `UPLOAD_TIME`, `PUBLIC`) VALUES ('0','$title','$descip','$up_date','$up_time','$type','0','$select_class','$ytd','$now_time','checked')");

       header('location:../../new_teacher/upload.php');
        
    }else
    if(isset($_POST['audio_upload_btn']))
    {
        $teach_id = $_POST['audio_upload_btn'];
        $title = $_POST['title'];
        $descip = $_POST['descip'];
        $select_class = $_POST['clz'];
        $subject = $_POST['select_subject'];
        $type = $_POST['type'];

        $up_date = date('Y-m-d');
        $up_time = date('h:i:s A');
        $now_time = date('Y-m-d h:i:s A');

        $location_media_type = strtolower($type);

        $file = $_FILES['upload_file1']['name'];

        $tmp_file = $_FILES['upload_file1']['tmp_name']; //ASSIGN DATA TO VARIABLE
        $ext = pathinfo($_FILES["upload_file1"]["name"], PATHINFO_EXTENSION); //GET EXTENTION ON IMAGE

        $base = basename($file,".".$ext);

        $sql001 = mysqli_query($conn,"SELECT * FROM uploads WHERE FILE_NAME LIKE '%".$base."%'");
        $check = mysqli_num_rows($sql001);

        if($check > 0)
        {
            $p1 = basename($file,".".$ext);
            $post_file = $p1."(".$check.").".$ext;
        }else
        if($check == '0')
        {
            $post_file = $file;
        }

        move_uploaded_file($tmp_file,'../../uploads/audio/'.$post_file); //SAVE LOCATION



        $insert001 = mysqli_query($conn,"INSERT INTO `uploads`(`FILE_NAME`, `TITLE`, `DESCRIPTION`, `UPL_DATE`, `UPL_TIME`, `TYPE`, `EXTENSION`, `CLASS_ID`, `YOUTUBE_LINK`, `UPLOAD_TIME`, `PUBLIC`) VALUES ('$post_file','$title','$descip','$up_date','$up_time','$type','$ext','$select_class','0','$now_time','checked')");
          
       header('location:../../new_teacher/upload.php');
        
    }else
    if(isset($_POST['document_upload_btn']))
    {
        $teach_id = $_POST['document_upload_btn'];
        $title = $_POST['title'];
        $descip = $_POST['descip'];
        $select_class = $_POST['clz'];
        $subject = $_POST['select_subject'];
        $type = $_POST['type'];

        $public_share = $_POST['public_share'];

        $up_date = date('Y-m-d');
        $up_time = date('h:i:s A');
        $now_time = date('Y-m-d h:i:s A');

        $location_media_type = strtolower($type);

        $file = $_FILES['upload_file1']['name'];

        $tmp_file = $_FILES['upload_file1']['tmp_name']; //ASSIGN DATA TO VARIABLE
        $ext = pathinfo($_FILES["upload_file1"]["name"], PATHINFO_EXTENSION); //GET EXTENTION ON IMAGE

        $base = basename($file,".".$ext);

        $sql001 = mysqli_query($conn,"SELECT * FROM uploads WHERE FILE_NAME LIKE '%".$base."%'");
        $check = mysqli_num_rows($sql001);

        if($check > 0)
        {
            $p1 = basename($file,".".$ext);
            $post_file = $p1."(".$check.").".$ext;
        }else
        if($check == '0')
        {
            $post_file = $file;
        }

        move_uploaded_file($tmp_file,'../../uploads/document/'.$post_file); //SAVE LOCATION



        $insert001 = mysqli_query($conn,"INSERT INTO `uploads`(`FILE_NAME`, `TITLE`, `DESCRIPTION`, `UPL_DATE`, `UPL_TIME`, `TYPE`, `EXTENSION`, `CLASS_ID`, `YOUTUBE_LINK`, `UPLOAD_TIME`, `PUBLIC`) VALUES ('$post_file','$title','$descip','$up_date','$up_time','$type','$ext','$select_class','0','$now_time','$public_share')");
          
       header('location:../../new_teacher/upload.php');
        
    }else
    if(isset($_POST['add_subject']))
    {
        $teach_id = $_POST['add_subject'];
        $subject_id = $_POST['subject'];
        $level_id = $_POST['clz'];

        $insert003 = mysqli_query($conn,"INSERT INTO `teacher_subject`(`SUB_ID`, `TEACH_ID`, `LEVEL_ID`) VALUES ('$subject_id','$teach_id','$level_id')");


        header('location:../../new_teacher/add_subject.php');

    }else
    if(isset($_POST['add_clz']))
    {
        $teach_id = $_POST['add_clz'];
        $subject_id = $_POST['subject'];
        $level_id = $_POST['clz'];
        $clz_name = ucwords($_POST['clz_name']);
        $fees = $_POST['clz_fees'];
        $start = $_POST['start'];
        $end = $_POST['end'];
        $weekday = $_POST['weekday'];

        $insert003 = mysqli_query($conn,"INSERT INTO `classes`(`CLASS_NAME`, `FEES`, `START_TIME`, `END_TIME`, `LINK`, `TEACH_ID`, `SUB_ID`, `ZOOM_LINK`, `MS_LINK`, `JITSI_LINK`, `GOOGLE_LINK`, `WHATSAPP_LINK`, `DAY`, `FREE`, `START_DATE`, `END_DATE`,`AGE_RANGE`) VALUES ('$clz_name','$fees','$start','$end','0','$teach_id','$subject_id','0','0','0','0','0','$weekday','0','0','0','0')");

        header('location:../../new_teacher/new_class.php?teach_id='.$teach_id.'');

    }else
      if(isset($_POST['submit_paper']))
      {
        $paper_name = $_POST['paper_name'];
        $exam_key = $_POST['exam_key'];
        $class_id = $_POST['class_id'];
        $teacher_id = $_POST['teacher_id'];
        $no_question = $_POST['no_question'];
        $no_answer = $_POST['no_answer'];
        $mark = $_POST['mark'];
        $hour = $_POST['hour'];
        $minute = $_POST['minute'];
        $start_date = $_POST['start_date'];
        $end_date = $_POST['end_date'];

        $max_hour = $_POST['max_hour'];
        $max_minu = $_POST['max_minu'];


        $str1 = strtotime($start_date);
        $strdate = date('Y-m-d',$str1);
        $strtime = date('H:i:s',$str1);

        $end1 = strtotime($end_date);
        $enddate = date('Y-m-d',$end1);
        $endtime = date('H:i:s',$end1);

        $start_date = date('Y-m-d H:i:s',$str1);
        $end_date = date('Y-m-d H:i:s',$end1);


        /*Convert*/
        /*$date = new DateTime($start_date);
        $date->modify($max_time);
        $last_attend = $date->format("Y-m-d H:i:s");*/

        if($max_hour == '0' && $max_minu == '0')
        {
          $last_attend = $now;
        }else
        if($max_hour == '0' && $max_minu !== '0' || $max_hour !== '0' && $max_minu == '0' || $max_hour !== '0' && $max_minu !== '0')
        {
          $date = new DateTime($end_date);
          $date->modify('-'.$max_hour.' hour'.''.'-'.$max_minu.' minute');
          $last_attend = $date->format("Y-m-d H:i:s");
        }
        
        

        if($_FILES['upload_file']['name'] !== '0')
          {
            $file = $_FILES['upload_file']['name'];
            $tmp_file = $_FILES['upload_file']['tmp_name']; //ASSIGN DATA TO VARIABLE
            $ext = pathinfo($_FILES["upload_file"]["name"], PATHINFO_EXTENSION); //GET EXTENTION ON IMAGE
            $rand = hexdec(uniqid()); //NAME CONVERT TO ENCRYPT
            $post_file = $rand.".".$ext; //ADD IMAGE NAME TO ENCRYPTED NAME
            move_uploaded_file($tmp_file,"../../student/str_document/".$post_file); //SAVE LOCATION
          }else
          {
            $post_file = '0';
          }

          $current_time = date('Y-m-d h:i:s A');
          $current_date = date('Y-m-d');

          $class_amount = sizeof($class_id);

          for($j=0;$j<$class_amount;$j++)
          {


            $insert001 = mysqli_query($conn,"INSERT INTO `paper_master`(`PAPER_NAME`, `EXAM_KEY`, `UPLOAD_DATE`, `UPLOAD_TIME`, `NO_OF_QUESTIONS`, `NO_QUESTION_ANSWER`, `ANSWER_MARKS`, `PDF_NAME`, `UPLOAD_BY`, `TIME_DURATION_HOUR`, `TIME_DURATION_MIN`, `FINISH_DATE`, `FINISH_TIME`, `STATUS`, `CLASS_ID`, `COMMENT`, `START_DATE`, `START_TIME`, `FINISH_D_T`, `START_D_T`,`LAST_ATTENT_TIME`,`MAX_HOUR`,`MAX_MINUTE`,`PAPER_TYPE`,`TEACH_ID`) VALUES ('$paper_name','$exam_key','$current_date','$current_time','$no_question','$no_answer','$mark','$post_file','$teacher_id','$hour','$minute','$enddate','$endtime','0','$class_id[$j]','0','$strdate','$strtime','$end_date','$start_date','$last_attend','$max_hour','$max_minu','1','$teacher_id')"); //

            $paper_id = mysqli_insert_id($conn);

            $answer = $_POST['answer'];

            $size_answers = sizeof($answer);

            for ($i=0; $i < $size_answers; $i++) { 

              $que = $i+1;
              $insert002 = mysqli_query($conn,"INSERT INTO `mcq_trans`(`QUESTION`, `CORRECT_ANSWER`, `MARKS_ANSWER`, `PAPER_ID`) VALUES ('$que','$answer[$i]','$mark','$paper_id')"); //Insert Answers
              
            }
          }

          header('location:../../new_teacher/exam.php');


      }else
      if(isset($_POST['str_submit_paper']))
      {
        $paper_name = $_POST['paper_name'];
        $exam_key = $_POST['exam_key'];
        $class_id = $_POST['class_id'];
        $teacher_id = $_POST['teacher_id'];
        $no_question = $_POST['no_question'];
        $master_mark = $_POST['master_mark'];
        $hour = $_POST['hour'];
        $minute = $_POST['minute'];
        $start_date = $_POST['start_date'];
        $end_date = $_POST['end_date'];

        $sub_mark = $_POST['sub_mark'];

        $max_hour = $_POST['max_hour'];
        $max_minu = $_POST['max_minu'];



        $str1 = strtotime($start_date);
        $strdate = date('Y-m-d',$str1);
        $strtime = date('H:i:s',$str1);

        $end1 = strtotime($end_date);
        $enddate = date('Y-m-d',$end1);
        $endtime = date('H:i:s',$end1);

        $start_date = date('Y-m-d H:i:s',$str1);
        $end_date = date('Y-m-d H:i:s',$end1);


        /*Convert*/
        /*$date = new DateTime($start_date);
        $date->modify($max_time);
        $last_attend = $date->format("Y-m-d H:i:s");*/

        if($max_hour == '0' && $max_minu == '0')
        {
          $last_attend = $end_date;
        }else
        if($max_hour == '0' && $max_minu !== '0' || $max_hour !== '0' && $max_minu == '0' || $max_hour !== '0' && $max_minu !== '0')
        {
          $date = new DateTime($end_date);
          $date->modify('-'.$max_hour.' hour'.''.'-'.$max_minu.' minute');
          $last_attend = $date->format("Y-m-d H:i:s");
        }
        
        

        if($_FILES['upload_file']['name'] !== '0')
          {
            $file = $_FILES['upload_file']['name'];
            $tmp_file = $_FILES['upload_file']['tmp_name']; //ASSIGN DATA TO VARIABLE
            $ext = pathinfo($_FILES["upload_file"]["name"], PATHINFO_EXTENSION); //GET EXTENTION ON IMAGE
            $rand = hexdec(uniqid()); //NAME CONVERT TO ENCRYPT
            $post_file = $rand.".".$ext; //ADD IMAGE NAME TO ENCRYPTED NAME
            move_uploaded_file($tmp_file,"../../student/str_document/".$post_file); //SAVE LOCATION
          }else
          {
            $post_file = '0';
          }

          $current_time = date('Y-m-d h:i:s A');
          $current_date = date('Y-m-d');

          $class_amount = sizeof($class_id);

          for($j=0;$j<$class_amount;$j++)
          {


            $insert001 = mysqli_query($conn,"INSERT INTO `paper_master`(`PAPER_NAME`, `EXAM_KEY`, `UPLOAD_DATE`, `UPLOAD_TIME`, `NO_OF_QUESTIONS`, `NO_QUESTION_ANSWER`, `ANSWER_MARKS`, `PDF_NAME`, `UPLOAD_BY`, `TIME_DURATION_HOUR`, `TIME_DURATION_MIN`, `FINISH_DATE`, `FINISH_TIME`, `STATUS`, `CLASS_ID`, `COMMENT`, `START_DATE`, `START_TIME`, `FINISH_D_T`, `START_D_T`,`LAST_ATTENT_TIME`,`MAX_HOUR`,`MAX_MINUTE`,`PAPER_TYPE`,`TEACH_ID`) VALUES ('$paper_name','$exam_key','$current_date','$current_time','$no_question','$no_question','0','$post_file','$teacher_id','$hour','$minute','$enddate','$endtime','0','$class_id[$j]','0','$strdate','$strtime','$end_date','$start_date','$last_attend','$max_hour','$max_minu','2','$teacher_id')"); //

            $paper_id = mysqli_insert_id($conn);

            for($i001=0;$i001<=$no_question-1;$i001++)
            {
              $question_no = $i001+1;
              $master_q_mark = $master_mark[$i001];
              $arr_size = sizeof($sub_mark[$i001]);

              $insert002 = mysqli_query($conn,"INSERT INTO `essay_master_question`(`ADD_DATE`, `ADD_TIME`, `PAPER_ID`, `QUESTION_NO`, `MASTER_MARK`) VALUES ('$current_date','$current_time','$paper_id','$question_no','$master_q_mark')");
              
              $master_q_id = mysqli_insert_id($conn);

              for ($j001=0; $j001 < $arr_size; $j001++) 
              { 

                $sub_q_mark = $sub_mark[$i001][$j001];
                $sub_question_no = $j001+1;


                $insert002 = mysqli_query($conn,"INSERT INTO `essay_sub_question`(`MASTER_QUESTION`, `SUB_QUESTION_NO`, `SUB_MARK`, `ESSAY_TRANS_ID`) VALUES ('$question_no','$sub_question_no','$sub_q_mark','$master_q_id')");

              }
            }


          }

          header('location:../../new_teacher/exam.php');


      }else
      if(isset($_POST['paper_marking']))
      {
        $paper_id = $_POST['paper_marking'];
        $total_main_question = $_POST['total_main_question'];
        $student_id = $_POST['student_name'];
        $sub_question = $_POST['sub_question'];


        $sql003 = mysqli_query($conn,"SELECT * FROM `paper_master` WHERE `PAPER_ID` = '$paper_id'"); 
       while($row003 = mysqli_fetch_assoc($sql003))
       {
          $start_date = $row003['START_DATE']; //Correct Answer
          $start_time = $row003['START_TIME'];

          $paper_start_date = $start_date;

          $class_id = $row003['CLASS_ID']; //Class ID
          $exam_key = $row003['EXAM_KEY']; //Exam key
         
          $exam_se_name = $row003['PAPER_NAME'];
       }



        
        $total_paper_mark = 0;
          $current_time = date('Y-m-d h:i:s A');
          $current_date = date('Y-m-d');

          for($j=0;$j<$total_main_question;$j++)
          {
            $j01=$j+1;;
            $sql00144 = mysqli_query($conn,"SELECT * FROM `essay_master_question` WHERE `PAPER_ID` = '$paper_id' AND `QUESTION_NO` = '$j01'");
            while($row00144 = mysqli_fetch_assoc($sql00144))
            {
              $essay_id = $row00144['ESSAY_TRANS_ID'];

             $insert002 = mysqli_query($conn,"INSERT INTO `student_essay_question_master`(`MASTER_QUESTION_NO`, `MASTER_Q_MARK`, `PAPER_ID`) VALUES ('$j01','0','$paper_id')");

              $student_assay_q_m_id = mysqli_insert_id($conn);
            

              $sql0012 = mysqli_query($conn,"SELECT * FROM `essay_sub_question` WHERE `ESSAY_TRANS_ID` = '$essay_id'");

              $arr_size = mysqli_num_rows($sql0012);

              $total_mark= 0;
              for ($j001=0; $j001 < $arr_size; $j001++) 
              { 

                $sub_q_mark = $sub_question[$j01][$j001];

                $total_mark = $total_mark+$sub_q_mark;

                $sub_question_no = $j001+1;


                $insert002 = mysqli_query($conn,"INSERT INTO `student_essay_question_trans`(`SUB_QUESTION`, `SUB_MARK`, `STU_ESSAY_MASTER_ID`) VALUES ('$sub_question_no','$sub_q_mark','$student_assay_q_m_id')");

                if($j001 == $arr_size-1)
                {
                  $update001 = mysqli_query($conn,"UPDATE `student_essay_question_master` SET `MASTER_Q_MARK`= '$total_mark' WHERE `STU_ESSAY_MASTER_ID`= '$student_assay_q_m_id'");

                  $total_paper_mark = $total_paper_mark+$total_mark;
                }

              }

              

            }    
                
            $update002 = mysqli_query($conn,"UPDATE `student_essay_master` SET `TOTAL_MARKS`= '$total_paper_mark',`STATUS`= '1' WHERE `STU_ID`= '$student_id' AND `PAPER_ID` = '$paper_id'");





          }

            $sql008 = mysqli_query($conn,"SELECT * FROM `exam_result` WHERE `STU_ID` = '$student_id' AND `CLASS_ID` = '$class_id' AND `EXAM_KEY` = '$exam_key'"); //check result
            $check003 = mysqli_num_rows($sql008);

             if($check003 > 0)
            {
              $sum = 0;
              while($row005 = mysqli_fetch_assoc($sql008))
              {
                $total_my_mark = $row005['TOTAL_MARK'];
              }
              $sum = $total_my_mark+$total_paper_mark; //MCQ + SE Marks

              $update = mysqli_query($conn,"UPDATE `exam_result` SET `SE_PAPER_NAME` = '$exam_se_name',`SE_MARK` = '$total_paper_mark',`TOTAL_MARK` = '$sum',`START_DATE` = '$paper_start_date' WHERE `CLASS_ID` = '$class_id' AND `STU_ID` = '$student_id' AND `EXAM_KEY` = '$exam_key'"); //Update result
            }else
            if($check003 == '0')
            {
              $insert = mysqli_query($conn,"INSERT INTO `exam_result`(`ADD_DATE`, `ADD_TIME`, `START_DATE`, `MCQ_MARK`, `SE_MARK`, `TOTAL_MARK`, `EXAM_KEY`, `STU_ID`, `CLASS_ID`, `PAPER_ID`, `MCQ_PAPER_NAME`, `SE_PAPER_NAME`) VALUES ('$current_date','$current_time','$paper_start_date','0','$total_paper_mark','$total_paper_mark','$exam_key','$student_id','$class_id','$paper_id','0','$exam_se_name')"); //Insert result
            }
          

          header('location:../../new_teacher/paper_marking.php?paper_id='.$paper_id.'');


      }else
      if(isset($_POST['add_new_student_marks']))
      {
        $paper_id = $_POST['add_new_student_marks'];
        $total_marks = $_POST['total_marks'];
        $reg_id = $_POST['student_data'];

        $today = date('Y-m-d');
        $time = date('Y-m-d h:i:s A');

        $sql001 = mysqli_query($conn,"SELECT * FROM `stu_login` WHERE `REGISTER_ID` = '$reg_id'");

        while($row001 = mysqli_fetch_assoc($sql001))
        {
              $stu_id = $row001['STU_ID'];

              $sql004 = mysqli_query($conn,"SELECT * FROM `student_details` WHERE `STU_ID` = '$stu_id'");

              while($row004 = mysqli_fetch_assoc($sql004))
              {

                    $stu_name = $row004['F_NAME']." ".$row004['L_NAME'];
              }


        }

        $sql002 = mysqli_query($conn,"SELECT * FROM `paper_master` WHERE `PAPER_ID` = '$paper_id'");
        while($row002 = mysqli_fetch_assoc($sql002))
        {
              $class_id = $row002['CLASS_ID'];
              $exam_key = $row002['EXAM_KEY'];
              $start_date = $row002['START_DATE'];
              $paper_type = $row002['PAPER_TYPE'];
              $paper_name = $row002['PAPER_NAME'];
        }

        if($paper_type == '1')
        {
          $mcq = $total_marks;
          $se = '0';
          $mcq_paper_name = $paper_name;
          $se_paper_name = '0';

          $final_mark = $mcq;
        }else
        if($paper_type == '2')
        {
          $se = $total_marks;
          $mcq = '0';
          $final_mark = $se;
          $se_paper_name = $paper_name;
          $mcq_paper_name = '0';

        }


        $sql003 = mysqli_query($conn,"SELECT * FROM `exam_result` WHERE `STU_ID` = '$stu_id' AND `CLASS_ID` = '$class_id' AND `EXAM_KEY` = '$exam_key'");

        $check_paper = mysqli_num_rows($sql003);

        if($check_paper > 0)
        {

            if($paper_type == '1')
            {
              
              $update001 = mysqli_query($conn,"UPDATE `exam_result` SET `MCQ_MARK`= '$mcq',`TOTAL_MARK`= '$final_mark',`MCQ_PAPER_NAME` = '$mcq_paper_name' WHERE `STU_ID` = '$stu_id' AND `CLASS_ID` = '$class_id' AND `EXAM_KEY` = '$exam_key'");

            }else
            if($paper_type == '2')
            {
             
              $update001 = mysqli_query($conn,"UPDATE `exam_result` SET `SE_MARK`= '$se',`TOTAL_MARK`= '$final_mark',`SE_PAPER_NAME` = '$se_paper_name' WHERE `STU_ID` = '$stu_id' AND `CLASS_ID` = '$class_id' AND `EXAM_KEY` = '$exam_key'");

            }

            

            
        }else
        if($check_paper == '0')
        {
          

          $insert001 = mysqli_query($conn,"INSERT INTO `exam_result`(`ADD_DATE`, `ADD_TIME`, `START_DATE`, `MCQ_MARK`, `SE_MARK`, `TOTAL_MARK`, `EXAM_KEY`, `CLASS_ID`, `PAPER_ID`, `STU_ID`, `MCQ_PAPER_NAME`, `SE_PAPER_NAME`) VALUES ('$today','$time','$start_date','$mcq','$se','$final_mark','$exam_key','$class_id','$paper_id','$stu_id','$mcq_paper_name','$se_paper_name')");
        }

        $sql005 = mysqli_query($conn,"SELECT * FROM `exam_result` WHERE `STU_ID` = '$stu_id' AND `CLASS_ID` = '$class_id' AND `EXAM_KEY` = '$exam_key'");
        while($row005 = mysqli_fetch_assoc($sql005))
        {
          $exam_se_mark = $row005['SE_MARK']/20;
          $exam_mcq_mark = $row005['MCQ_MARK']/2;

          $exam_tota_mark001 = $exam_se_mark+$exam_mcq_mark;

          mysqli_query($conn,"UPDATE `exam_result` SET `TOTAL_MARK`= '$exam_tota_mark001' WHERE `STU_ID` = '$stu_id' AND `CLASS_ID` = '$class_id' AND `EXAM_KEY` = '$exam_key'");
        }




        $_SESSION['added_student'] = $stu_name;
        $_SESSION['added_reg_id'] = $reg_id;
        $_SESSION['new_student_marks'] = $total_marks;


        header('location:../../new_teacher/add_new_marks.php?paper_id='.$paper_id.'');
        
      }else
      if(isset($_POST['create_subject123']))
      {
          $subject = ucwords($_POST['subject']);
          $level_id = $_POST['level_id'];
          $super_subject = $_POST['super_subject'];

          if(!empty($_FILES['picture']['name']))
          {
            $file = $_FILES['picture']['name'];

            $tmp_file = $_FILES['picture']['tmp_name']; //ASSIGN DATA TO VARIABLE
            $ext = pathinfo($_FILES["picture"]["name"], PATHINFO_EXTENSION); //GET EXTENTION ON IMAGE
            $rand = hexdec(uniqid()); //NAME CONVERT TO ENCRYPT
            $post_file = $rand.".".$ext; //ADD IMAGE NAME TO ENCRYPTED NAME
            move_uploaded_file($tmp_file,"../../admin/images/subject_image/".$post_file); //SAVE LOCATION


            $insert001 = mysqli_query($conn,"INSERT INTO `subject`(`SUBJECT_NAME`, `LEVEL_ID`, `PICTURE`, `SH_LEVEL`) VALUES ('$subject','$level_id','$post_file','$super_subject')");
          }else
          if(empty($_FILES['picture']['name']))
          {
            $insert001 = mysqli_query($conn,"INSERT INTO `subject`(`SUBJECT_NAME`, `LEVEL_ID`, `PICTURE`, `SH_LEVEL`) VALUES ('$subject','$level_id','0','$super_subject')");
          }

         header('location:../../new_teacher/add_subject.php');

      }else
      if(isset($_POST['semi_submit']))
      {
        $semi_name = $_POST['semi_name'];
        $semi_code = $_POST['semi_code'];
        $semi_date = $_POST['semi_date'];
        $semi_start = $_POST['semi_start'];
        $semi_end = $_POST['semi_end'];
        $semi_teacher = $_POST['semi_teacher'];
        $semi_link = $_POST['semi_link'];

        $page = $_POST['page'];

        $str = strtotime($semi_date);
        $semi_day = date('l',$str);

        $add_date = date('Y-m-d');
        $add_time = date('Y-m-d h:i:s A');

        $sql001 = mysqli_query($conn,"INSERT INTO `seminar_master`(`ADD_DATE`, `ADD_TIME`, `SEM_NAME`, `SEM_CODE`, `TEACH_ID`, `SEMINAR_LINK`, `ATTEND_COUNT`, `DUE_DATE`, `START_TIME`, `END_TIME`, `DAY`) VALUES ('$add_date','$add_time','$semi_name','$semi_code','$semi_teacher','$semi_link','0','$semi_date','$semi_start','$semi_end','$semi_day')");

        header('location:../../new_teacher/seminar.php?teach_id='.$semi_teacher.'&&page='.$page.'');


      }else
      if(isset($_POST['create_bank_data']))
      {
        $holder = ucwords($_POST['holder_name']);
        $ac_no = $_POST['ac_no'];
        $bank_name = strtoupper($_POST['bank_name']);
        $branch = ucfirst($_POST['branch']);
        $page = $_POST['create_bank_data'];

        $teach_id = $_POST['teacher_id'];

        mysqli_query($conn,"INSERT INTO `teacher_bank_details`(`HOLDER_NAME`, `BANK_NAME`, `ACCOUNT_NO`, `BRANCH_NAME`, `TEACH_ID`) VALUES ('$holder','$bank_name','$ac_no','$branch','$teach_id')");

      
        header('location:../../new_teacher/profile_setting.php');


      }else
      if(isset($_POST['record_upload_btn']))
      {
          $teach_id = $_POST['record_upload_btn'];
          $title = $_POST['title'];
          $descip = $_POST['descip'];
          $select_class = $_POST['clz'];
          $subject = $_POST['select_subject'];
          $type = $_POST['type'];

          $teach_id = $_POST['teach_id'];
          $record= $_POST['record_link'];

          $up_date = date('Y-m-d');
          $up_time = date('h:i:s A');
          $now_time = date('Y-m-d h:i:s A');

          if(!empty($_FILES['upload_file']['name']))
          {
            $file = $_FILES['upload_file']['name'];

            $tmp_file = $_FILES['upload_file']['tmp_name']; //ASSIGN DATA TO VARIABLE
            $ext = pathinfo($_FILES["upload_file"]["name"], PATHINFO_EXTENSION); //GET EXTENTION ON IMAGE
            $rand = hexdec(uniqid()); //NAME CONVERT TO ENCRYPT
            $post_file = $rand.".".$ext; //ADD IMAGE NAME TO ENCRYPTED NAME
            move_uploaded_file($tmp_file,"../../uploads/recording/".$post_file); //SAVE LOCATION


            $insert001 = mysqli_query($conn,"INSERT INTO `uploads`(`FILE_NAME`, `TITLE`, `DESCRIPTION`, `UPL_DATE`, `UPL_TIME`, `TYPE`, `EXTENSION`, `CLASS_ID`, `YOUTUBE_LINK`, `UPLOAD_TIME`, `PUBLIC`) VALUES ('$post_file','$title','$descip','$up_date','$up_time','$type','$ext','$select_class','0','$now_time','checked')");
          }else
          if(empty($_FILES['upload_file']['name']))
          {
            $insert001 = mysqli_query($conn,"INSERT INTO `uploads`(`FILE_NAME`, `TITLE`, `DESCRIPTION`, `UPL_DATE`, `UPL_TIME`, `TYPE`, `EXTENSION`, `CLASS_ID`, `YOUTUBE_LINK`, `UPLOAD_TIME`, `PUBLIC`) VALUES ('0','$title','$descip','$up_date','$up_time','$type','0','$select_class','$record','$now_time','checked')");
          }

        header('location:../../new_teacher/upload.php?teach_id='.$teach_id.'');
          
      }else
      if(isset($_POST['add_google_link']))
      {
        $paper_name = ucwords($_POST['paper_name']);
        $class_id = $_POST['class_id'];
        $form_link = $_POST['google_form_link'];
        $teacher_id = $_POST['teacher_id'];
        $hour = $_POST['hour'];
        $minute = $_POST['minute'];
        $start_time = $_POST['start_date'];
        $end_time = $_POST['end_date'];

        $max_hour = $_POST['max_hour'];
        $max_minu = $_POST['max_minu'];

        $start_date = date('Y-m-d',strtotime($start_time));

        $end_date = date('Y-m-d',strtotime($end_time));

        $current_time = date('Y-m-d h:i:s A');
        $current_date = date('Y-m-d');

        $insert0001 = mysqli_query($conn,"INSERT INTO `g_f_master`(`ADD_DT`,`PAPER_NAME`, `FORM_LINK`, `START_DATE`, `START_TIME`, `END_DATE`, `END_TIME`, `TEACH_ID`,`HOURS`,`MINUTE`) VALUES ('$current_time','$paper_name','$form_link','$start_date','$start_time','$end_date','$end_time','$teacher_id','$max_hour','$max_minu')"); //Insert Answers

        $gf_master_id = mysqli_insert_id($conn);

        $class_amount = sizeof($class_id);

        for($j=0;$j<$class_amount;$j++)
        {
          $insert001 = mysqli_query($conn,"INSERT INTO `g_f_tranasaction`(`GF_MASTER_ID`, `CLASS_ID`) VALUES ('$gf_master_id','$class_id[$j]')");
        }

        header('location:../../new_teacher/new_google_form.php');

      }else
      if(isset($_POST['register_students']))
      {
        $j=0;
        $fnm = ucwords($_POST['fnm']);
        $lnm = ucwords($_POST['lnm']);
        $dob = $_POST['dob'];
        $gen = $_POST['gender'];
        $email = $_POST['email'];
        $tp = $_POST['number'];
        $address = $_POST['address'];
        //$reg_id = $_POST['username'];
        $new = $_POST['new']; // 0 = Already registerd,1 = Not Registered
        $psw = md5($_POST['password']);

        $today = date('Y-m-d h:i:s A');

        $sql99 = mysqli_query($conn,"SELECT * FROM institute WHERE INS_ID = '1'");
        $row99 = mysqli_fetch_assoc($sql99);
        $reg_code = $row99['REG_CODE'];

        $sql = mysqli_query($conn,"SELECT * FROM `reg_count` WHERE `REG_COUNT_ID` = '1'");
        $row=mysqli_fetch_assoc($sql);

        $real_number = $row['CURRENT_REG_ID']+1;

        $reg_id = $reg_code.sprintf('%05d',$real_number);


        $sql003 = mysqli_query($conn,"SELECT * FROM `student_details` WHERE `F_NAME` = '$fnm' AND `L_NAME` = '$lnm' AND `DOB` = '$dob' AND `ADDRESS` = '$address' AND `TP` = '$tp' AND `EMAIL` = '$email' AND `NEW` = '$new' AND `GENDER` = '$gen'"); //check already added data in student details table

        $check_query = mysqli_num_rows($sql003);


        if($check_query>0)
        {
        //true check query

          ?>  <!DOCTYPE html>
          <html>
          <head>
              <title>Create Successfully</title>
              <link href="https://fonts.googleapis.com/css2?family=Ubuntu&display=swap" rel="stylesheet">
              <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
          </head>
          <body style="font-family: 'Ubuntu', sans-serif;">
                  <script type="text/javascript">

                      Swal.fire(
                                'Sorry!',
                                'Your are already registered!',
                                'error'
                              ).then((result) => {
                                 if(result){
                                   window.location = '../../new_teacher/create_student.php'
                                 }

                              })

      
                  </script>
          </body>
          </html>
              
              
          <?php


        //true check query
          
        }else
        if($check_query == '0')
        {
        //false check query

        mysqli_query($conn,"UPDATE `reg_count` SET `CURRENT_REG_ID` = '$real_number' WHERE `REG_COUNT_ID` = '1'");
          

        $insert001 = mysqli_query($conn,"INSERT INTO `student_details`(`F_NAME`, `DOB`, `ADDRESS`, `TP`, `EMAIL`, `NEW`, `GENDER`, `L_NAME`, `PICTURE`,`PAYMENT_TAB`,`EXAM_TAB`,`ALERT`,`GRADE_ID`) VALUES ('$fnm','$dob','$address','$tp','$email','$new','$gen','$lnm','0','0','0','0','0')");
        
        $stu_id = mysqli_insert_id($conn);

        $insert002 = mysqli_query($conn,"INSERT INTO `stu_login`(`REGISTER_ID`, `PASSWORD`, `STATUS`, `STU_ID`,`LOGIN_TIME`,`LOGOUT_TIME`, `REG_DATE`,`LOGGED`) VALUES ('$reg_id','$psw','Active','$stu_id','0','0','$today','0')");

        /*==================================== Check Age rage with auto register classes =============================*/

                    $st_dob = strtotime($dob);
                    $my_dob = date('Y',$st_dob);

                    $sql002 = mysqli_query($conn,"SELECT * FROM `classes`");
                    while($row02 = mysqli_fetch_assoc($sql002))
                    {
                      $age_range = $row02['AGE_RANGE'];
                      
                      if($age_range == $my_dob)
                      {

                        $class_id = $row02['CLASS_ID'];
                        $teacher_id = $row02['TEACH_ID'];

                        $sql00123 = mysqli_query($conn,"SELECT * FROM `student_details` WHERE `STU_ID` = '$stu_id'");
                        while($row00123 = mysqli_fetch_assoc($sql00123))
                        {
                            $student_id = $row00123['STU_ID'];

                            $sql0044 = mysqli_query($conn,"SELECT * FROM `transactions` WHERE `STU_ID` = '$student_id' AND `CLASS_ID` = '$class_id'");
                            $check = mysqli_num_rows($sql0044);

                            if($check == '0')
                            {
                              $now = date('Y-m-d h:i:s A');

                              mysqli_query($conn,"INSERT INTO `transactions`(`UPDATE_TIME`, `CLASS_ID`, `TEACH_ID`, `STU_ID`, `FREE_CLASS`,`FREE_START_DATE`,`FREE_END_DATE`,`LMS_STATUS`,`new_url`) VALUES ('$now','$class_id','$teacher_id','$student_id','0','0','0','1',null)");
                            }
                          }
                        }
                      }

                /*==================================== Check Age rage with auto register classes =============================*/



        if($insert001 && $insert002)
        {

          $sql0003 = mysqli_query($conn,"SELECT * FROM `student_details` WHERE `F_NAME` = '$fnm' AND `L_NAME` = '$lnm' AND `DOB` = '$dob' AND `ADDRESS` = '$address' AND `TP` = '$tp' AND `EMAIL` = '$email' AND `GENDER` = '$gen'");
          while($row003=mysqli_fetch_assoc($sql0003))
          {
              $stu_id = $row003['STU_ID'];

              $sql0004 = mysqli_query($conn,"SELECT * FROM `stu_login` WHERE `STU_ID` = '$stu_id'");
              while($row004=mysqli_fetch_assoc($sql0004))
              {
                  $reg_code = $row004['REGISTER_ID'];
              }

          }

          


          ?>  <!DOCTYPE html>
          <html>
          <head>
              <title>Create Successfully</title>
              <link href="https://fonts.googleapis.com/css2?family=Ubuntu&display=swap" rel="stylesheet">
              <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
          </head>
          <body style="font-family: 'Ubuntu', sans-serif;">
                  <script type="text/javascript">

                      Swal.fire(
                                'Registration Successful!',
                                'Your Registration Number - <?php echo $reg_code; ?>',
                                'success'
                              ).then((result) => {
                                 if(result){
                                   window.location = '../../new_teacher/create_student.php'
                                 }

                              })

      
                  </script>
          </body>
          </html>
              
              
          <?php


          //===================================== Send SMS Message ======================================
          
          $today = date('Y-m-d');
          $now = date('Y-m-d h:i:s A');

          $sql0013 = mysqli_query($conn,"SELECT * FROM `institute` WHERE `INS_ID` = '1' AND `SINGLE_SMS` = '1'"); //SMS SERVICE ACTIVE

          if(mysqli_num_rows($sql0013)>0)
          {

            //=============================================== SMS Message Status ================================

                $sql0031 = mysqli_query($conn,"SELECT * FROM `sms_counter` ORDER BY `ADD_D_T` DESC");

                $row0031 = mysqli_fetch_assoc($sql0031);
                $current_sms = $row0031['CURRENT_SMS'];
                $current_amount = $row0031['CURRENT_AMOUNT'];
                $sms_id = $row0031['SUB_SMS_C_ID'];

                $sql99 = mysqli_query($conn,"SELECT * FROM institute WHERE INS_ID = '1'");
                $row99 = mysqli_fetch_assoc($sql99);
                $reg_code = $row99['REG_CODE'];
                $ins_name = $row99['INS_NAME'];
                $reg_without_active = $row99['REG_WITH_ACTIVE']; //Register students without active
                $web_link = $row99['WEB']; //Web Link
                $sms_footer = $row99['SMS_FOOTER'];

            if($current_sms >0)
            {
                 $msg_body = 'Registration Success! Your Registration ID- '.$reg_id.'         
'.$web_link.'/app/student/login/index.php?r='.$reg_id.'

'.$sms_footer.'';    

                    $sql0012 = mysqli_query($conn,"SELECT * FROM `single_sms_gateway` WHERE `SINGLE_SMS_ID` = '1'");
                    while($row0012 = mysqli_fetch_assoc($sql0012))
                    {
                      $username = $row0012['USERNAME'];
                      $api_key = $row0012['API_KEY'];
                      $gateway_type = $row0012['GATEWAY_TYPE'];
                      $country_code = $row0012['COUNTRY_CODE'];

                      $sms_charges001 = $row0012['SMS_CHARGE'];
                    }
                 
                  $new_sms = $current_sms-1;
                  $new_sms_amount = $current_amount-$sms_charges001;

                mysqli_query($conn,"UPDATE `sms_counter` SET `CURRENT_SMS`= '$new_sms',`CURRENT_AMOUNT`= '$new_sms_amount' WHERE `SUB_SMS_C_ID`= '$sms_id'");

                $stu_name = $fnm." ".$lnm;
                  
                mysqli_query($conn,"INSERT INTO `single_sms`(`SEND_DATE`, `SEND_TIME`, `SMS_BODY`, `STUDENT_NAME`, `TP`) VALUES ('$today','$now','$msg_body','$stu_name','$tp')");


                //=============================================== SMS Message Status ================================




        

        $tele = $_POST['number'];

        $real_tp = substr($tele,1,9);

        

        $User_name =$username; //Your Username
        $Api_key = $api_key; //Your API Key
        $Gateway_type = $gateway_type; //Define Economy Gateway
        $Country_code = $country_code; //Country Code
        $Number = $real_tp; //Mobile Number Without 0
        $message = $msg_body; //Your Message
                      
        $data = array(
        "user_name" => $User_name,
        "api_key" => $Api_key,
        "gateway_type" => $Gateway_type,
        "country_code" => $Country_code,
        "number" => $Number,
        "message" => $message
        );
        $data_string = json_encode($data);

        $ch = curl_init('https://my.ipromo.lk/api/postsendsms/');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data_string))
        );
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
                      
        //Execute Post
        $result = curl_exec($ch);
                      
        //Close Connection
        curl_close($ch);

                //SMS GATEWAY

                




        }
        }
        
        //=====================================Send SMS Message =======================================
          
        }
        //false check query
      }
      }else
      if(isset($_POST['sms_timetable']))
      {
            $msg_body = $_POST['sms'];
            $reg_id = $_POST['sms_timetable'];
            $page = $_POST['page'];

            //===================================== Send SMS Message ======================================
          
          $today = date('Y-m-d');
          $now = date('Y-m-d h:i:s A');


          $sql0013 = mysqli_query($conn,"SELECT * FROM `institute` WHERE `INS_ID` = '1' AND `SINGLE_SMS` = '1'"); //SMS SERVICE ACTIVE

          if(mysqli_num_rows($sql0013)>0)
          {

            //=============================================== SMS Message Status ================================

                $sql0031 = mysqli_query($conn,"SELECT * FROM `sms_counter` ORDER BY `ADD_D_T` DESC");

                $row0031 = mysqli_fetch_assoc($sql0031);
                $current_sms = $row0031['CURRENT_SMS'];
                $current_amount = $row0031['CURRENT_AMOUNT'];
                $sms_id = $row0031['SUB_SMS_C_ID'];

                $sql99 = mysqli_query($conn,"SELECT * FROM institute WHERE INS_ID = '1'");
                $row99 = mysqli_fetch_assoc($sql99);
                $reg_code = $row99['REG_CODE'];
                $ins_name = $row99['INS_NAME'];
                $reg_without_active = $row99['REG_WITH_ACTIVE']; //Register students without active
                $web_link = $row99['WEB']; //Web Link
                $sms_footer = $row99['SMS_FOOTER'];

            if($current_sms >0)
            {
                 $msg_body = 'Hello, you can get all the details of the registration classes by visiting this link.         
'.$web_link.'/sms_login/index.php?reg_id='.$reg_id.'

'.$sms_footer.'';   

                
                $sql002 = mysqli_query($conn,"SELECT * FROM `stu_login` WHERE `REGISTER_ID` = '$reg_id'");

                while($row002 = mysqli_fetch_assoc($sql002))
                {
                  $stu_id = $row002['STU_ID'];
                  $sql003 = mysqli_query($conn,"SELECT * FROM `student_details` WHERE `STU_ID` = '$stu_id'");

                  while($row003 = mysqli_fetch_assoc($sql003))
                  {
                    $f_name = $row003['F_NAME'];
                    $l_name = $row003['L_NAME'];
                    $tp = $row003['TP'];

                    $stu_name = $f_name." ".$l_name;
                    
                   }
                  
                 }

                




                    $sql0012 = mysqli_query($conn,"SELECT * FROM `single_sms_gateway` WHERE `SINGLE_SMS_ID` = '1'");
                    while($row0012 = mysqli_fetch_assoc($sql0012))
                    {
                      $username = $row0012['USERNAME'];
                      $api_key = $row0012['API_KEY'];
                      $gateway_type = $row0012['GATEWAY_TYPE'];
                      $country_code = $row0012['COUNTRY_CODE'];

                      $sms_charges001 = $row0012['SMS_CHARGE'];
                    }
                 
                  $new_sms = $current_sms-1;
                  $new_sms_amount = $current_amount-$sms_charges001;

                mysqli_query($conn,"UPDATE `sms_counter` SET `CURRENT_SMS`= '$new_sms',`CURRENT_AMOUNT`= '$new_sms_amount' WHERE `SUB_SMS_C_ID`= '$sms_id'");
                  
                mysqli_query($conn,"INSERT INTO `single_sms`(`SEND_DATE`, `SEND_TIME`, `SMS_BODY`, `STUDENT_NAME`, `TP`) VALUES ('$today','$now','$msg_body','$stu_name','$tp')");


                //=============================================== SMS Message Status ================================




                   echo  $real_tp = substr($tp,1,9);

                    

                    $User_name =$username; //Your Username
                    $Api_key = $api_key; //Your API Key
                    $Gateway_type = $gateway_type; //Define Economy Gateway
                    $Country_code = $country_code; //Country Code
                    $Number = $real_tp; //Mobile Number Without 0
                    $message = $msg_body; //Your Message
                                  
                    $data = array(
                    "user_name" => $User_name,
                    "api_key" => $Api_key,
                    "gateway_type" => $Gateway_type,
                    "country_code" => $Country_code,
                    "number" => $Number,
                    "message" => $message
                    );
                    $data_string = json_encode($data);

                    $ch = curl_init('https://my.ipromo.lk/api/postsendsms/');
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data_string))
                    );
                    curl_setopt($ch, CURLOPT_TIMEOUT, 5);
                    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
                                  
                    //Execute Post
                    $result = curl_exec($ch);
                                  
                    //Close Connection
                    curl_close($ch);

                            //SMS GATEWAY

                            




                    }
                    }
        
        //=====================================Send SMS Message =======================================

          $_SESSION['save_msg'] = 'Sent SMS!';


          header('location:../../new_teacher/student_approved.php?page='.$page.'');

          }else
          if(isset($_POST['getMeetingId'])) 
          { 
          /*Create Meeting*/
            $post_time = $_POST['start_time'];
            $post_date = $_POST['start_date'];

            $start_my_time = date('H:i:00',strtotime($post_time));
            $scheduled_time = $_POST['start_date']."T".$start_my_time;

            $class_id = $_POST['class_id'];
            $myemail_id = $_POST['email_id'];

            //$sql3 = "UPDATE products SET paid_date ='' WHERE lesson_id='$_GET[category_id]'";
            //mysqli_query($conn,$sql3);

            $sql3 = "UPDATE transactions SET new_url ='' WHERE CLASS_ID='$class_id'";
            mysqli_query($conn,$sql3); 

            $sql_cat = "SELECT * FROM zoom_accounts where active = '1' AND id = '$myemail_id' ";
            $result_cat = mysqli_query($conn,$sql_cat);
            $pra_cat = mysqli_fetch_assoc($result_cat);

                function new_meeting($api_key, $api_secret, $userId, $topic, $type, $startTime, $duration, $timeZone, $password,$agenda, $approvalType, $showShareButton, $allowMultipleDevices)
                {   
                    $url = 'http://phpstack-605701-1961339.cloudwaysapps.com/api/create-meeting';

                    $fields =  array(
                        "api_key" => $api_key,
                        "api_secret" => $api_secret,
                        "user_id" => $userId,  // Account email / User email
                        "topic" => $topic, // Meeting title
                        "type" => $type, // 2 - Scheduled meeting
                        "start_time" => $startTime, // yyyy-MM-dd T HH:mm:ss
                        "duration" => $duration, // minutes
                        "timezone" => $timeZone, // 'Asia/Colombo'
                        "password" => $password, // Any password
                        "agenda" => $agenda, // Meeting description
                        "approval_type" => $approvalType,  // 1 - Manually approve
                        "show_share_button" => $showShareButton, // Show/Hide share buttons on meeting 'true/false'
                        "allow_multiple_devices" => $allowMultipleDevices // Allow/Denied access meeting from multiple devices 'true/false'
                    );

                    $payload = $fields;

                    $ch = curl_init();
                    curl_setopt($ch,CURLOPT_URL, $url);
                    curl_setopt($ch,CURLOPT_POST, true);
                    curl_setopt($ch,CURLOPT_POSTFIELDS, $payload);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $response = curl_exec($ch);

                    curl_close($ch);
                    
                    return $response;
                }
                $random = rand(00000,99999);

               $new_result = $result = new_meeting (
                    $pra_cat['api_key'], 
                    $pra_cat['api_secret'], 
                    $pra_cat['email_address'], 
                    $_POST['topic'], 
                    "2", 
                    $scheduled_time,
                    $_POST['duration'],
                    "Asia/Colombo",
                    $random,
                    $_POST['topic'],
                    "1",
                    "false",
                    "false"
                );


                $data =json_decode($result);

                $meeting_id = $data->id;
                
                //$meeting_data = var_dump($data); //var_dump($data);

                $email_address = $pra_cat['email_address'];
                $api_key = $pra_cat['api_key'];
                $api_secret = $pra_cat['api_secret'];
                $class_id = $class_id;
                $id = $pra_cat['id']; //Zoom account ID
                $minute = $_POST['duration'];
                
                mysqli_query($conn,"UPDATE classes SET zoom_meeting_id='$meeting_id', zoom_meeting_data = '$new_result', zoom_email_id = '$id', zoom_email_address = '$email_address', zoom_api_key = '$api_key',  zoom_api_secret = '$api_secret',  minute = '$minute', schedule_date = '$post_date', schedule_time = '$post_time' WHERE CLASS_ID='$class_id'");

              header('location:../../new_teacher/admin_teachers_active.php?class_id='.$class_id.'');


    /*Create Meeting*/
        }

 ?>