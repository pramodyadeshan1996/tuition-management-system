

<?php
  $page = "Erase Duplicate";
  $folder_in = '3';

      include('header/header.php');

        $s04 = mysqli_query($conn,"SELECT * FROM admin_login WHERE ADMIN_ID = '$admin_id'");
        while($row4 = mysqli_fetch_assoc($s04))
        {
          $name = $row4['ADMIN_NAME'];
        }

 ?>

    <ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-bottom: 04%;margin-bottom: 0;">
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="Erase Duplicate"> Erase Duplicate</li>
    </ol>


      <div class="col-md-12 bg-white" style="border:1px solid #cccc;height: auto;">
      
      <div class="col-md-12" style="border-bottom: 1px solid #cccc;">
        <div class="row">

            <div class="col-md-9" style="margin-bottom: 10px;"><h2><a href="dashboard.php" data-toggle="tooltip" data-title="Back"><i class="pg-icon" style="font-size: 22px;">chevron_left</i></a> Erase Duplicate</h2></div>

        </div>
      </div>

          
          <div class=" container-fluid container-fixed-lg bg-white" style="margin-top: 0px;">
            <div class="card card-transparent">
              <div class="card-header ">
                    <div class="card-title">
                    </div>
                    <div>
                      <div class="row">
                        <div class="col-md-6">
                        </div>
                      <div class="col-md-2"></div>
                      <div class="col-md-4">
                      <div class="col-md-12" style="padding-top: 18px;"></div>
                      <input type="text" id="search-table" class="form-control pull-right" placeholder="Search">
                      </div>

                    </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="card-body">
                <table class="table table-hover demo-table-search table-responsive-block" id="tableWithSearch">
                  <thead>
                      <tr>
                        <th style="width: 1%;">Registered ID</th>
                        <th style="width: 1%;">Name</th>
                        <th style="width: 1%;">TP No</th>
                        <th style="width: 1%;">DOB</th>
                        <th style="width: 1%;">Gender</th>
                        <th style="width: 1%;">Status</th>
                        <th style="width: 1%;text-align: center;">Action</th>
                      </tr>
                    </thead>
                    <tbody>

                      <?php 

                          $today = date('Y-m-d h:i:s A');

                          //mysqli_query($conn,"UPDATE stu_login SET REG_DATE = '$today'");

                          $sql001 = mysqli_query($conn,"SELECT * FROM `stu_login` WHERE `STATUS` = 'Active' ORDER BY `STU_ID` DESC");

                          $check = mysqli_num_rows($sql001);

                          if($check>0){
                          while($row001 = mysqli_fetch_assoc($sql001))
                          {
                            $stu_id = $row001['STU_ID'];
                            $reg = $row001['REGISTER_ID'];
                            $register_date = $row001['REG_DATE'];
                            $password = $row001['PASSWORD'];

                            $sql002 = mysqli_query($conn,"SELECT * FROM `student_details` WHERE `STU_ID` = '$stu_id'");
                            while($row002 = mysqli_fetch_assoc($sql002))
                            {
                              $name = $row002['F_NAME']." ".$row002['L_NAME'];
                              $fnm = $row002['F_NAME'];
                              $lnm = $row002['L_NAME'];
                              $tp1 = $row002['TP'];
                              $tp = str_replace(" ","",$tp1);
                              $dob = $row002['DOB'];
                              $gender = $row002['GENDER'];
                              $email = $row002['EMAIL'];
                              $address = $row002['ADDRESS'];
                              $new = $row002['NEW'];
                            }
                            

                            echo '

                              <tr>
                                <td class="v-align-left">'.$reg.'</td>
                                <td class="v-align-left">'.$name.'</td>
                                <td class="v-align-left">'.$tp.'</td>
                                <td class="v-align-left">'.$dob.'</td>
                                <td class="v-align-left">'.$gender.'</td>
                                <td class="v-align-left" id="erase_message'.$stu_id.'"><label class="label label-success"><span class="fa fa-check-circle"></span> Registered</label></td>
                                 <td class="v-align-middle text-center">' ?>

                                <a href="../admin/query/update.php?regen_id=<?php echo $stu_id; ?>" onclick="return confirm('Are you sure re-generate register ID?')" class="btn btn-success  btn-xs btn-rounded" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;" data-title="Re-generate Register ID" data-toggle="tooltip"><i class="pg-icon">refresh</i></a>
                                  <!--  href="../admin/query/delete.php?erase_students=<?php echo $stu_id; ?>" -->
                                <a id="clear<?php echo $stu_id; ?>" onclick="delete_stu(<?php echo $stu_id; ?>);" class="btn btn-danger  btn-xs btn-rounded" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;color:white;" data-title="Permanently Delete Student" data-toggle="tooltip"><i class="pg-icon">trash_alt</i></a>

                                <script type="text/javascript">
                                  
                                  function delete_stu(para_stu_id) {

                                    if(confirm('Are you sure Permanently Delete?'))
                                    {
                                    
                                        var student_id = para_stu_id;

                                         $.ajax({
                                          url:'../admin/query/delete.php',
                                          method:"POST",
                                          data:{erase_students:student_id},
                                          success:function(data)
                                          {
                                            //alert(data)
                                              $('#erase_message'+student_id+'').html('<label class="label label-danger"><span class="fa fa-trash"></span> Deleted</label>');
                                            
                                          }
                                         })
                                    }

                                  }                               
                                </script>

                                <?php echo '

                                </td>
                              </tr>

                            ';
                          }
                        }
                      
                         ?>


                    </tbody>
                </table>
              </div>
            </div>
            </div>
            </div>
            </div>

<?php include('footer/footer.php'); ?>
