    
    <div class="row">
        <div class="col-md-12">
            <div class="section-title">
                <h5>Zoom Recordings</h5>
            </div>
        </div>
    </div>
    <div class="row ">

        <?php 
      
      $student_session_id = $_SESSION['STU_ID']; //student ID

    $sql001 = mysqli_query($conn,"SELECT * FROM `uploads` WHERE `CLASS_ID` = '$class_id' AND `TYPE` = 'Record' AND `PUBLIC` = 'checked' ORDER BY `UPLOAD_TIME` DESC");

      $sql00101 = mysqli_query($conn,"SELECT * FROM `uploads` WHERE `CLASS_ID` = '$class_id' AND `TYPE` = 'Record' ORDER BY `UPLOAD_TIME` DESC");

      $sql00102 = mysqli_query($conn,"SELECT * FROM `payment_data` WHERE `STU_ID` = '$student_session_id' AND `ADMIN_SUBMIT` = '1' AND `YEAR` = '$year' AND `MONTH` = '$month' AND `CLASS_ID` = '$class_id' "); //Check Paid for this month

      $check_paid = mysqli_num_rows($sql00102); //Check available paid?

      $check_upload = mysqli_num_rows($sql00101); //Check available uploads

      $check_public = mysqli_num_rows($sql001); //Check enabled Public

      /*
            
            Upload Table  ||  Payment Table

            Checked     AND   Paid   OK
            Checked     AND   Unpaid OK
            Unchecked   AND   Paid   OK
            UnChecked   AND   Unpaid Wrong



      */

      if($check_upload > 0)
      {
        //Now Check Enable Public show for paid and public enable students (Checked AND Paid   OK , Checked AND Unpaid OK)

        if(($check_paid > 0 && $check_public > 0) || ($check_paid > 0 && $check_public == '0') || ($check_paid == '0' & $check_public > 0) )
        {
          while($row001 = mysqli_fetch_assoc($sql001))
          {
            
            $link_name = $row001['YOUTUBE_LINK'];
            $upload_id = $row001['UPLOAD_ID'];
            $upload_title = $row001['TITLE'];
            $upload_file = $row001['FILE_NAME'];

            $video_description = $row001['DESCRIPTION'];
            $video_upload_time = $row001['UPLOAD_TIME'];
            $ext = $row001['EXTENSION'];

            $sql0012 = mysqli_query($conn,"SELECT * FROM `classes` WHERE `CLASS_ID` = '$class_id' "); //Class Details
            while($row0012 = mysqli_fetch_assoc($sql0012))
            {
                $class_name = $row0012['CLASS_NAME']; //Class Name
                $teacher_id = $row0012['TEACH_ID']; //Teacher ID
                $class_fees = $row0012['FEES']; //Teacher ID

                $class_start_time = $row0012['START_TIME']; //Class Start time
                $class_end_time = $row0012['END_TIME']; //Class End time

                $class_day = $row0012['DAY']; //Class End time
                
                $str = strtotime($class_start_time);
                $class_start_time = date('h:i A',$str);

                $str2 = strtotime($class_end_time);
                $class_end_time = date('h:i A',$str2);

            }

            $sql0013 = mysqli_query($conn,"SELECT * FROM `teacher_details` WHERE `TEACH_ID` = '$teacher_id' "); //check Classes for registered teachers details
            while($row0013 = mysqli_fetch_assoc($sql0013))
            {
                $teacher_position = $row0013['POSITION']; //Teacher Position
                $teacher_f_name = $row0013['F_NAME']; //Teacher First Name
                $teacher_l_name = $row0013['L_NAME']; //Teacher Last Name

                $teacher_picture = $row0013['PICTURE']; //Teacher Profile Picture
                $teacher_gender = $row0013['GENDER']; //Teacher Gender


                $teacher_full_name = $teacher_position.". ".$teacher_f_name." ".$teacher_l_name; //Teacher Full Name

                //If Teacher Picture zero,Check gender and gender to select picture 
                if($teacher_picture == '0')
                {
                    if($teacher_gender == 'Male')
                    {
                        $teacher_picture = 'b_teacher.jpg';
                    }else
                    if($teacher_gender == 'Female')
                    {
                        $teacher_picture = 'g_teacher.jpg';
                    }
                }
                //If Teacher Picture zero,Check gender and gender to select picture

            }



            ?>

            <?php  

                $drive = $link_name;//$link_name;
                
                $first_link = substr($drive,32);
                $sec_link = substr($drive,-17);

                $drive_id = substr($drive,32,-17);

                 ?>

                <!-- Register Class Box -->

                <div class="col-lg-6">
                    <div class="pricing" style="border: 1px solid #cccc;padding: 10px 10px;" id="remove_box<?php echo $class_id; ?>">
                    

                        <h5 class="text-dark mt-10 col-md-12"><?php echo $upload_title; ?> <br> <small class="text-center"><?php echo $class_name; ?></small></h5>

                        <?php 

                        if($upload_file !== '0')
                        {

                         ?>
                        <video controls oncontextmenu="return false" style="width:100%;height: 300px;" controlsList="nodownload">
                            <source src="../../uploads/recording/<?php echo $upload_file; ?>" type="video/<?php echo $ext; ?>">
                            Oops. HTML 5 video not supported.
                        </video>

                        <?php 
                        }else
                        if($upload_file == '0')
                        {?>

                            <div class="col-md-12 row mt-4">
                                <div class="col-md-3"></div>
                                <div class="col-md-6">
                                        <center>
                                            <img src="../assets/img/icon/video.svg" style="width:100%;height: 210px;">
                                        </center>
                                </div>
                                <div class="col-md-3"></div>
                            </div>


                            <div class="col-md-12">
                            <a href="<?php echo $link_name; ?>" target="_blank" style="color:white;" class="btn-custom" onclick="clicked_video('<?php echo $upload_id; ?>','<?php echo $student_session_id; ?>');">
                                <span><i class="fas fa-play"></i></span>
                                <span>View</span>
                            </a>
                            <br>
                            <small class="btn btn-link" style="color:#2BA908;font-size: 14px;font-weight: 500px;" data-toggle="modal" data-target="#record_info" onclick="rec_info('<?php echo $upload_title; ?>','<?php echo $video_description; ?>','<?php echo $video_upload_time; ?>')">Recording Info</small>
                          </div>

                       <?php 
                        }
                         ?>
                          

                          <!-- Link Click with inform database -->

                            <script type="text/javascript">

                                  function clicked_video(upload_id,student_id)
                                  {
                                      var type = 'Video';
                                      
                                      $.ajax({
                                        type:"POST",
                                        url:"../../student/query/insert.php",
                                        data : {click_lms:upload_id,student_id:student_id,type:type},
                                        success: function (data) {

                                        }
                                      });

                                    }
                            </script>

                            <!-- Link Click with inform database -->


                    </div>
                </div>
                

                <!-- Register Class Box -->

            <?php

          }
        //Now Check Enable Public show for paid and public enable students (Checked AND Paid   OK , Checked AND Unpaid OK)

          }else
            if(($check_paid == '0' & $check_public == '0') )
            {
                
                ?>


                <div class="col-md-12 row mt-20" style="padding: 10px 10px 10px 10px;">
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                            <center>
                                <img src="../assets/img/icon/unavailable.svg" style="width: 250px;height: 250px;">
                                <h5 class="mt-20 text-muted"><span class="fas fa-search"></span> Recordings Unavailable</h5>
                            </center>
                    </div>
                    <div class="col-md-3"></div>
                </div>


                <?php

                
            }



        }else
        if($check_upload == '0')
        {
                ?>
                <div class="col-md-12 row mt-20" style="padding: 10px 10px 10px 10px;">
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                            <center>
                                <img src="../assets/img/icon/unavailable.svg" style="width: 250px;height: 250px;">
                                <h5 class="mt-20 text-muted"><span class="fas fa-search"></span> Recordings Unavailable</h5>
                            </center>
                    </div>
                    <div class="col-md-3"></div>
                </div>


                <?php

            /*Empty Payments*/
        }

       ?>


    </div>