  <?php 

    session_start();
    include('../../connect/connect.php');
    $_SESSION['update_msg'] = 'Updated Data';

      if(isset($_POST['submit_btn']))
      {

        $new_pass = md5($_POST['npass']);
        $id = $_POST['submit_btn'];

        $sql = mysqli_query($conn,"UPDATE admin_login SET PASSWORD = '$new_pass' WHERE ADMIN_ID = '$id'");

        header('location:../../new_admin/profile_setting.php');

      }else
      if(isset($_POST['edit_video_btn']))
      {
          $upload_id = $_POST['edit_video_btn'];
          $title = ucwords($_POST['title']);
          $descip = $_POST['descip'];
          $select_class = $_POST['clz'];
          $subject = $_POST['select_subject'];
          $type = $_POST['type'];
          $teach_id = $_POST['teach_id'];


          $up_date = date('Y-m-d');
          $up_time = date('h:i:s A');


          $ytd = $_POST['ytd'];

          $s = mysqli_query($conn,"SELECT * FROM `uploads` WHERE `YOUTUBE_LINK` = '$ytd' AND `CLASS_ID` = '$select_class'");
          if(mysqli_num_rows($s)>0)
          {
            
              $update001 = mysqli_query($conn,"UPDATE `uploads` SET `TITLE`='$title',`DESCRIPTION`= '$descip',`UPL_DATE`='$up_date',`UPL_TIME`='$up_time',`TYPE`='Video',`CLASS_ID`='$select_class',`YOUTUBE_LINK`='$ytd' WHERE `UPLOAD_ID` = '$upload_id'");
          }else
          if(mysqli_num_rows($s)=='0')
          {
              mysqli_query($conn,"DELETE FROM uploads_clicked WHERE UPLOAD_ID = '$upload_id'");

              $update001 = mysqli_query($conn,"UPDATE `uploads` SET `TITLE`='$title',`DESCRIPTION`= '$descip',`UPL_DATE`='$up_date',`UPL_TIME`='$up_time',`TYPE`='Video',`CLASS_ID`='$select_class',`YOUTUBE_LINK`='$ytd' WHERE `UPLOAD_ID` = '$upload_id'");
          }
          
         header('location:../../new_admin/view_upload.php?teach_id='.$teach_id.' ');
          
      }else
      if(isset($_POST['edit_record_btn']))
      {
          $upload_id = $_POST['edit_record_btn'];
          $title = ucwords($_POST['title']);
          $descip = $_POST['descip'];
          $select_class = $_POST['clz'];
          $subject = $_POST['select_subject'];
          $type = $_POST['type'];
          $teach_id = $_POST['teach_id'];

          $current_file = $_POST['current_file'];

          $up_date = date('Y-m-d');
          $up_time = date('h:i:s A');

          if(!empty($_FILES['upload_file']['name']))
          {
            $file = $_FILES['upload_file']['name'];

            $tmp_file = $_FILES['upload_file']['tmp_name']; //ASSIGN DATA TO VARIABLE
            $ext = pathinfo($_FILES["upload_file"]["name"], PATHINFO_EXTENSION); //GET EXTENTION ON IMAGE
            $rand = hexdec(uniqid()); //NAME CONVERT TO ENCRYPT
            $post_file = $rand.".".$ext; //ADD IMAGE NAME TO ENCRYPTED NAME
            move_uploaded_file($tmp_file,"../../uploads/recording/".$post_file); //SAVE LOCATION


            $insert001 = mysqli_query($conn,"UPDATE `uploads` SET `TITLE`='$title',`DESCRIPTION`= '$descip',`UPL_DATE`='$up_date',`UPL_TIME`='$up_time',`TYPE`='Record',`CLASS_ID`='$select_class',`YOUTUBE_LINK`='0',`EXTENSION`='$ext',`FILE_NAME`='$post_file' WHERE `UPLOAD_ID` = '$upload_id'");
          }else
          if(empty($_FILES['upload_file']['name']))
          {
            $record_link = $_POST['record_link'];

            unlink("../../uploads/recording/".$current_file); //SAVE LOCATION
            
            $insert001 = mysqli_query($conn,"UPDATE `uploads` SET `TITLE`='$title',`DESCRIPTION`= '$descip',`UPL_DATE`='$up_date',`UPL_TIME`='$up_time',`TYPE`='Record',`CLASS_ID`='$select_class',`YOUTUBE_LINK`='$record_link',`FILE_NAME`='0',`EXTENSION`='0' WHERE `UPLOAD_ID` = '$upload_id'");
          }
          
         header('location:../../new_admin/view_upload.php?teach_id='.$teach_id.' ');
          
      }else
      if(isset($_POST['save_link']))
      {

          $teach_id = $_POST['save_link'];
          $link = $_POST['link'];

          $sql = mysqli_query($conn,"UPDATE teacher_details SET ZOOM_LINK = '$link' WHERE TEACH_ID = '$teach_id'");
          mysqli_query($conn,"DELETE FROM link_clicked");

          header('location:../profile/new_upload.php');

        }else
      if(isset($_POST['update_class']))
      {

          $class_id = $_POST['update_class'];
          $class = ucwords($_POST['class']);
          $fees = $_POST['fees'];
          $link_id = $_POST['link_id'];
          $start = $_POST['start'];
         $end = $_POST['end'];
          $day = $_POST['day'];

          if($link_id == '')
          {
              $link_id = "0";
          }

          $sql = mysqli_query($conn,"UPDATE `classes` SET `CLASS_NAME`='$class',`FEES`='$fees',`ZOOM_LINK`='$link_id',`START_TIME`='$start',`END_TIME`='$end',`DAY`='$day' WHERE `CLASS_ID` = '$class_id'");

          header('location:../profile/create_class.php');

        }else
        if(isset($_POST['update_admin']))
        {
          $admin_id = $_POST['update_admin'];

          $name = ucwords($_POST['name']);
          $gen = $_POST['gender'];
          $staff_free_timer = $_POST['staff_free_timer'];

          $timer_minute = 0;
          $timer_minute = $staff_free_timer*1000*60;

          $update001 = mysqli_query($conn,"UPDATE `admin_login` SET `ADMIN_NAME`='$name',`GENDER`='$gen' WHERE `ADMIN_ID` = '$admin_id'");
          $update002 = mysqli_query($conn,"UPDATE `institute` SET `STAFF_TIMER`= '$timer_minute' WHERE `INS_ID` = '1'");
              
          header('location:../../new_admin/profile_setting.php');

          
        }else
        if(isset($_POST['upload_img']))
        {
          $admin_id = $_POST['upload_img'];

          $recent_img = $_POST['recent_img'];

          

          if($recent_img !== '0')
          {
            if($recent_img !== 'female_admin.png' || $recent_img !== 'male_admin.png')
            {
              unlink('../images/profile/'.$recent_img);
            }
          }

          if(!empty($_FILES['img']['name']))
          {   
              $file = $_FILES['img']['name'];
              $tmp_file = $_FILES['img']['tmp_name']; //ASSIGN DATA TO VARIABLE
              $ext = pathinfo($_FILES["img"]["name"], PATHINFO_EXTENSION); //GET EXTENTION ON IMAGE
              $rand = hexdec(uniqid()); //NAME CONVERT TO ENCRYPT
              $post_file = $rand.".".$ext; //ADD IMAGE NAME TO ENCRYPTED NAME

              move_uploaded_file($tmp_file,'../images/profile/'.$post_file); //SAVE LOCATION
              
              $update001 = mysqli_query($conn,"UPDATE `admin_login` SET `PICTURE`='$post_file' WHERE `ADMIN_ID` = '$admin_id'");

          }else
          if(empty($_FILES['img']['name']))
          {
              $update001 = mysqli_query($conn,"UPDATE `admin_login` SET `PICTURE`='0' WHERE `ADMIN_ID` = '$admin_id'");

          }

              
          header('location:../../new_admin/profile_setting.php');
        
      }else
      if(isset($_GET['remove_img']))
      {

          $admin_id = $_GET['remove_img'];
          $recent_img = $_GET['recent_img'];

          $sql = mysqli_query($conn,"UPDATE `admin_login` SET `PICTURE`='0' WHERE `ADMIN_ID` = '$admin_id'");

          unlink('../images/profile/'.$recent_img);


          header('location:../../new_admin/profile_setting.php');

        }else
      if(isset($_POST['edit_audio_btn']))
      {
          $upload_id = $_POST['edit_audio_btn'];
          $title = ucwords($_POST['title']);
          $descip = $_POST['descip'];
          $select_class = $_POST['clz'];
          $subject = $_POST['select_subject'];
          $type = $_POST['type'];
          $recent_file = $_POST['recent_file'];
          $teach_id = $_POST['teach_id'];

          unlink('../../uploads/audio/'.$recent_file);

          $up_date = date('Y-m-d');
          $up_time = date('h:i:s A');

          $upload_time = date('Y-m-d h:i:s A');

          $file = $_FILES["upload_file1"]["name"];

            $tmp_file = $_FILES['upload_file1']['tmp_name']; //ASSIGN DATA TO VARIABLE
            $ext = pathinfo($_FILES["upload_file1"]["name"], PATHINFO_EXTENSION); //GET EXTENTION ON IMAGE

            
            $base = basename($file,".".$ext);

            $sql001 = mysqli_query($conn,"SELECT * FROM uploads WHERE FILE_NAME LIKE '%".$base."%'");
            $check = mysqli_num_rows($sql001);

            if($check > 0)
            {
                $p1 = basename($file,".".$ext);
                $post_file = $p1."(".$check.").".$ext;
            }else
            if($check == '0')
            {
                $post_file = $file;
            }


            move_uploaded_file($tmp_file,'../../uploads/audio/'.$post_file); //SAVE LOCATION


            $s = mysqli_query($conn,"SELECT * FROM `uploads` WHERE `FILE_NAME` = '$post_file' AND `CLASS_ID` = '$select_class'");
          if(mysqli_num_rows($s)>0)
          {
            
              $update001 = mysqli_query($conn,"UPDATE `uploads` SET `TITLE`='$title',`DESCRIPTION`= '$descip',`UPL_DATE`='$up_date',`UPL_TIME`='$up_time',`TYPE`='Audio',`CLASS_ID`='$select_class',`YOUTUBE_LINK`='0',`UPLOAD_TIME`='$upload_time',`FILE_NAME`='$post_file' WHERE `UPLOAD_ID` = '$upload_id'");

          }else
          if(mysqli_num_rows($s)=='0')
          {
              mysqli_query($conn,"DELETE FROM uploads_clicked WHERE UPLOAD_ID = '$upload_id'");

              $update001 = mysqli_query($conn,"UPDATE `uploads` SET `TITLE`='$title',`DESCRIPTION`= '$descip',`UPL_DATE`='$up_date',`UPL_TIME`='$up_time',`TYPE`='Audio',`CLASS_ID`='$select_class',`YOUTUBE_LINK`='0',`UPLOAD_TIME`='$upload_time',`FILE_NAME`='$post_file' WHERE `UPLOAD_ID` = '$upload_id'");
          }

          header('location:../../new_admin/view_upload.php?teach_id='.$teach_id.' ');
          
      }else
      if(isset($_POST['edit_document_btn']))
      {
          $upload_id = $_POST['edit_document_btn'];
          $title = ucwords($_POST['title']);
          $descip = $_POST['descip'];
          $select_class = $_POST['clz'];
          $subject = $_POST['select_subject'];
          $type = $_POST['type'];
          $recent_file = $_POST['recent_file'];
          $teach_id = $_POST['teach_id'];

          unlink('../../uploads/document/'.$recent_file.'');

          $up_date = date('Y-m-d');
          $up_time = date('h:i:s A');

          $upload_time = date('Y-m-d h:i:s A');

          $file = $_FILES["upload_file1"]["name"];

            $tmp_file = $_FILES['upload_file1']['tmp_name']; //ASSIGN DATA TO VARIABLE
            $ext = pathinfo($_FILES["upload_file1"]["name"], PATHINFO_EXTENSION); //GET EXTENTION ON IMAGE

            
            $base = basename($file,".".$ext);

            $sql001 = mysqli_query($conn,"SELECT * FROM uploads WHERE FILE_NAME LIKE '%".$base."%'");
            $check = mysqli_num_rows($sql001);

            if($check > 0)
            {
                $p1 = basename($file,".".$ext);
                $post_file = $p1."(".$check.").".$ext;
            }else
            if($check == '0')
            {
                $post_file = $file;
            }


            move_uploaded_file($tmp_file,'../../uploads/document/'.$post_file); //SAVE LOCATION

            $s = mysqli_query($conn,"SELECT * FROM `uploads` WHERE `FILE_NAME` = '$post_file' AND `CLASS_ID` = '$select_class'");
          if(mysqli_num_rows($s)>0)
          {
            
              $update001 = mysqli_query($conn,"UPDATE `uploads` SET `TITLE`='$title',`DESCRIPTION`= '$descip',`UPL_DATE`='$up_date',`UPL_TIME`='$up_time',`TYPE`='Document',`CLASS_ID`='$select_class',`YOUTUBE_LINK`='0',`UPLOAD_TIME`='$upload_time',`FILE_NAME`='$post_file' WHERE `UPLOAD_ID` = '$upload_id'");

          }else
          if(mysqli_num_rows($s)=='0')
          {
              mysqli_query($conn,"DELETE FROM uploads_clicked WHERE UPLOAD_ID = '$upload_id'");

              $update001 = mysqli_query($conn,"UPDATE `uploads` SET `TITLE`='$title',`DESCRIPTION`= '$descip',`UPL_DATE`='$up_date',`UPL_TIME`='$up_time',`TYPE`='Document',`CLASS_ID`='$select_class',`YOUTUBE_LINK`='0',`UPLOAD_TIME`='$upload_time',`FILE_NAME`='$post_file' WHERE `UPLOAD_ID` = '$upload_id'");
          }


          header('location:../../new_admin/view_upload.php?teach_id='.$teach_id.' ');
          
      }else
      if(isset($_POST['update_zoom_link']))
      {

          $class_id = $_POST['update_zoom_link'];
          $link = $_POST['zoom_link'];
          $teach_id = $_POST['teach_id'];

          if($link == '')
          {
            $link = '0';
          }

          mysqli_query($conn,"DELETE FROM link_clicked WHERE CLASS_ID = '$class_id'");
          $sql = mysqli_query($conn,"UPDATE `classes` SET `ZOOM_LINK`='$link' WHERE `CLASS_ID` = '$class_id'");

          header('location:../../new_admin/setup_link.php');

        }else
      if(isset($_POST['update_google_link']))
      {

          $class_id = $_POST['update_google_link'];
          $link = $_POST['google_link'];
          $teach_id = $_POST['teach_id'];

          if($link == '')
          {
            $link = '0';
          }
          mysqli_query($conn,"DELETE FROM link_clicked WHERE CLASS_ID = '$class_id'");

          $sql = mysqli_query($conn,"UPDATE `classes` SET `GOOGLE_LINK`='$link' WHERE `CLASS_ID` = '$class_id'");

          header('location:../../new_admin/setup_link.php');

        }else
      if(isset($_POST['update_jitsi_link']))
      {

          $class_id = $_POST['update_jitsi_link'];
          $link = $_POST['jitsi_link'];
          $teach_id = $_POST['teach_id'];

          if($link == '')
          {
            $link = '0';
          }
          mysqli_query($conn,"DELETE FROM link_clicked WHERE CLASS_ID = '$class_id'");

          $sql = mysqli_query($conn,"UPDATE `classes` SET `JITSI_LINK`='$link' WHERE `CLASS_ID` = '$class_id'");

          header('location:../../new_admin/setup_link.php');

        }else
      if(isset($_POST['update_ms_link']))
      {

          $class_id = $_POST['update_ms_link'];
          $link = $_POST['ms_link'];
          $teach_id = $_POST['teach_id'];

          if($link == '')
          {
            $link = '0';
          }
          mysqli_query($conn,"DELETE FROM link_clicked WHERE CLASS_ID = '$class_id'");

          $sql = mysqli_query($conn,"UPDATE `classes` SET `MS_LINK`='$link' WHERE `CLASS_ID` = '$class_id'");

          header('location:../../new_admin/setup_link.php');
          
        }else
      if(isset($_POST['update_wa_link']))
      {

          $class_id = $_POST['update_wa_link'];
          $link = $_POST['wa_link'];
          $teach_id = $_POST['teach_id'];

          if($link == '')
          {
            $link = '0';
          }
          mysqli_query($conn,"DELETE FROM link_clicked WHERE CLASS_ID = '$class_id'");

          $sql = mysqli_query($conn,"UPDATE `classes` SET `WHATSAPP_LINK`='$link' WHERE `CLASS_ID` = '$class_id'");


          header('location:../../new_admin/setup_link.php');
        }else
      if(isset($_POST['update_tg_link']))
      {

          $class_id = $_POST['update_tg_link'];
          $link = $_POST['tg_link'];
          $teach_id = $_POST['teach_id'];

          if($link == '')
          {
            $link = '0';
          }

          $sql = mysqli_query($conn,"UPDATE `classes` SET `TELEGRAM_LINK`='$link' WHERE `CLASS_ID` = '$class_id'");


          header('location:../../new_admin/setup_link.php');
        }else
      if(isset($_POST['edit_clz']))
      {
          $class_id = $_POST['edit_clz'];
          $subject_id = $_POST['subject'];
          $level_id = $_POST['clz'];
          $clz_name = ucwords($_POST['clz_name']);
          $fees = $_POST['clz_fees'];
          $start = $_POST['start'];
          $end = $_POST['end'];
          $weekday = $_POST['weekday'];
          $teach_id = $_POST['teach_id'];
          $age_range = $_POST['age_range'];
          $recent_age = $_POST['recent_age'];
          $contact_no = $_POST['contact_no'];

          if(empty($_POST['hide_tg_btn']))
          {
            $hide_tg_btn = '0';
          }else
          if(!empty($_POST['hide_tg_btn']))
          {
            $hide_tg_btn = '1';
          }

          if($age_range == '')
          {
            $age_range = '0';
          }


          $update = mysqli_query($conn,"UPDATE `classes` SET `HIDE_TG_BTN`= '$hide_tg_btn',`CLASS_NAME`= '$clz_name',`FEES`= '$fees',`START_TIME`= '$start',`END_TIME`='$end',`SUB_ID`='$subject_id',`DAY`= '$weekday',`AGE_RANGE`= '$age_range',`CONTACT_NO`= '$contact_no' WHERE `CLASS_ID`= '$class_id'");


          if($age_range !== $recent_age)
          {


            /*==================================== Check Age rage with auto register classes =============================*/

                if($age_range !== '0')
                {
                  $sql002 = mysqli_query($conn,"SELECT * FROM `classes` WHERE `CLASS_ID` = '$class_id'");
                  while($row02 = mysqli_fetch_assoc($sql002))
                  {
                    $age_range = $row02['AGE_RANGE'];
                    
                      $class_id = $row02['CLASS_ID'];
                      $teacher_id = $row02['TEACH_ID'];

                      $sql00123 = mysqli_query($conn,"SELECT * FROM `student_details`");
                      while($row00123 = mysqli_fetch_assoc($sql00123))
                      {
                          $student_id = $row00123['STU_ID'];
                          $dob = $row00123['DOB'];


                          $st_dob = strtotime($dob);
                          $my_dob = date('Y',$st_dob);

                          if($age_range == $my_dob)
                          {

                              $sql0044 = mysqli_query($conn,"SELECT * FROM `transactions` WHERE `STU_ID` = '$student_id' AND `CLASS_ID` = '$class_id'");
                              $check = mysqli_num_rows($sql0044);

                              if($check == '0')
                              {
                                $now = date('Y-m-d h:i:s A');

                                mysqli_query($conn,"INSERT INTO `transactions`(`UPDATE_TIME`, `CLASS_ID`, `TEACH_ID`, `STU_ID`, `FREE_CLASS`,`FREE_START_DATE`,`FREE_END_DATE`,`LMS_STATUS`,`new_url`) VALUES ('$now','$class_id','$teacher_id','$student_id','0','0','0','1',null)");
                              }
                          }
                      }
                  }
                }

              /*==================================== Check Age rage with auto register classes =============================*/



            /*==================================== Delete recent auto registered class =============================*/

                if($age_range !== '0')
                {
                  $sql002 = mysqli_query($conn,"SELECT * FROM `classes` WHERE `CLASS_ID` = '$class_id'");
                  while($row02 = mysqli_fetch_assoc($sql002))
                  {
                    $age_range = $row02['AGE_RANGE'];
                    
                      $class_id = $row02['CLASS_ID'];
                      $teacher_id = $row02['TEACH_ID'];

                      $sql00123 = mysqli_query($conn,"SELECT * FROM `student_details`");
                      while($row00123 = mysqli_fetch_assoc($sql00123))
                      {
                          $student_id = $row00123['STU_ID'];
                          $dob = $row00123['DOB'];


                          $st_dob = strtotime($dob);
                          $my_dob = date('Y',$st_dob);

                          if($recent_age == $my_dob) //delete recent age and my birthday not equel
                          {
                              $sql0044 = mysqli_query($conn,"DELETE FROM `transactions` WHERE `CLASS_ID` = '$class_id' AND `STU_ID` = '$student_id'");
                              
                          }
                      }
                  }
                }

            /*==================================== Delete recent auto registered class =============================*/
    
          }

          header('location:../../new_admin/new_class.php?teach_id='.$teach_id.' ');

      }else
      if(isset($_POST['edit_clz']))
      {
          $class_id = $_POST['edit_clz'];
          $subject_id = $_POST['subject'];
          $level_id = $_POST['clz'];
          $clz_name = ucwords($_POST['clz_name']);
          $fees = $_POST['clz_fees'];
          $start = $_POST['start'];
          $end = $_POST['end'];
          $weekday = $_POST['weekday'];

          $update = mysqli_query($conn,"UPDATE `classes` SET `CLASS_NAME`= '$clz_name',`FEES`= '$fees',`START_TIME`= '$start',`END_TIME`='$end',`SUB_ID`='$subject_id',`DAY`= '$weekday' WHERE `CLASS_ID`= '$class_id'");

          header('location:../../new_teacher/new_class.php');

      }else
      if(isset($_GET['approve_teach'])) //Approve OK****
      {
          $teach_id = $_GET['approve_teach'];
          $approve = $_GET['approve'];
          
          $sql002 = mysqli_query($conn,"SELECT * FROM `teacher_details` WHERE `TEACH_ID` = '$teach_id'");
          while($row002 = mysqli_fetch_assoc($sql002))
          {
            $name = $row002['POSITION'].". ".$row002['F_NAME']." ".$row002['L_NAME'];
            $tp = $row002['TP'];
          }

          if($approve == 'Approve')
          {

            $update = mysqli_query($conn,"UPDATE `teacher_login` SET `STATUS`= 'Active' WHERE `TEACH_ID`= '$teach_id'");

          }else
          if($approve == 'Disapprove')
          {
            $update = mysqli_query($conn,"UPDATE `teacher_login` SET `STATUS`= 'Delete' WHERE `TEACH_ID`= '$teach_id'");
          }
         //===================================== Send SMS Message ======================================
          
          $today = date('Y-m-d');
          $now = date('Y-m-d h:i:s A');

          $sql0013 = mysqli_query($conn,"SELECT * FROM `institute` WHERE `INS_ID` = '1' AND `SINGLE_SMS` = '1'"); //SMS SERVICE ACTIVE

          if(mysqli_num_rows($sql0013)>0)
          {

            //=============================================== SMS Message Status ================================

                $sql0031 = mysqli_query($conn,"SELECT * FROM `sms_counter` ORDER BY `ADD_D_T` DESC");

                $row0031 = mysqli_fetch_assoc($sql0031);
                $current_sms = $row0031['CURRENT_SMS'];
                $current_amount = $row0031['CURRENT_AMOUNT'];
                $sms_id = $row0031['SUB_SMS_C_ID'];

                $sql99 = mysqli_query($conn,"SELECT * FROM institute WHERE INS_ID = '1'");
                $row99 = mysqli_fetch_assoc($sql99);
                $reg_code = $row99['REG_CODE'];
                $ins_name = $row99['INS_NAME'];
                $reg_without_active = $row99['REG_WITH_ACTIVE']; //Register students without active
                $web_link = $row99['WEB']; //Web Link
                $sms_footer = $row99['SMS_FOOTER'];

            if($current_sms >0)
            {
                 $msg_body = 'Your Registration has been Successfully! You can login now!
'.$web_link.'/app/teacher/login/index.php

'.$sms_footer.'';


                    $sql0012 = mysqli_query($conn,"SELECT * FROM `single_sms_gateway` WHERE `SINGLE_SMS_ID` = '1'");
                    while($row0012 = mysqli_fetch_assoc($sql0012))
                    {
                      $username = $row0012['USERNAME'];
                      $api_key = $row0012['API_KEY'];
                      $gateway_type = $row0012['GATEWAY_TYPE'];
                      $country_code = $row0012['COUNTRY_CODE'];

                      $sms_charges001 = $row0012['SMS_CHARGE'];
                    }
                 
                  $new_sms = $current_sms-1;
                  $new_sms_amount = $current_amount-$sms_charges001;

                mysqli_query($conn,"UPDATE `sms_counter` SET `CURRENT_SMS`= '$new_sms',`CURRENT_AMOUNT`= '$new_sms_amount' WHERE `SUB_SMS_C_ID`= '$sms_id'");
                  
                mysqli_query($conn,"INSERT INTO `single_sms`(`SEND_DATE`, `SEND_TIME`, `SMS_BODY`, `STUDENT_NAME`, `TP`) VALUES ('$today','$now','$msg_body','$name','$tp')");


                //=============================================== SMS Message Status ================================


        $real_tp = substr($tp,1,9);

        

        $User_name =$username; //Your Username
        $Api_key = $api_key; //Your API Key
        $Gateway_type = $gateway_type; //Define Economy Gateway
        $Country_code = $country_code; //Country Code
        $Number = $real_tp; //Mobile Number Without 0
        $message = $msg_body; //Your Message
                      
        $data = array(
        "user_name" => $User_name,
        "api_key" => $Api_key,
        "gateway_type" => $Gateway_type,
        "country_code" => $Country_code,
        "number" => $Number,
        "message" => $message
        );
        $data_string = json_encode($data);

        $ch = curl_init('https://my.ipromo.lk/api/postsendsms/');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data_string))
        );
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
                      
        //Execute Post
        $result = curl_exec($ch);
                      
        //Close Connection
        curl_close($ch);

                //SMS GATEWAY

                




        }
        }
        
        //=====================================Send SMS Message =======================================




        header('location:../../new_admin/teacher_pending.php');

      }else
      if(isset($_GET['approved_stu'])) //APPROVED variable OK**
      {
          $stu_id = $_GET['approved_stu'];
          $approve = $_GET['approve'];

          if($approve == 'Disapprove')
          {
            $update = mysqli_query($conn,"UPDATE `stu_login` SET `STATUS`= 'Delete' WHERE `STU_ID`= '$stu_id'");

          }

          header('location:../../new_admin/student_approved.php');

      }else
      if(isset($_POST['update_data']))
      {
        $stu_id = $_POST['update_data'];
        $page = $_POST['page'];
        $fnm = ucwords($_POST['fnm']);
        $lnm = ucwords($_POST['lnm']);
        $dob = date("Y-m-d", strtotime($_POST['dob']));
        $gen = $_POST['gender'];
        $email = $_POST['email'];
        $tp = $_POST['number'];
        $address = $_POST['address'];
        $new = $_POST['new']; // 0 = Already registerd,1 = Not Registered


          $today = date('Y-m-d h:i:s A');
          

        $insert001 = mysqli_query($conn,"UPDATE `student_details` SET `F_NAME`= '$fnm',`L_NAME`= '$lnm',`DOB`= '$dob',`EMAIL`= '$email',`ADDRESS`= '$address',`TP`= '$tp',`NEW`= '$new',`GENDER`= '$gen' WHERE `STU_ID`= '$stu_id'");

        $_SESSION['notify_id'] = $stu_id;

        header('location:../../new_admin/student_approved.php?page='.$page.'');
        
      }else
      if(isset($_POST['update_teacher']))
      {
        $teach_id = $_POST['update_teacher'];
        $fnm = ucwords($_POST['fnm']);
        $lnm = ucwords($_POST['lnm']);
        $dob = date("Y-m-d", strtotime($_POST['dob']));
        $gen = $_POST['gender'];
        $email = $_POST['email'];
        $tp = $_POST['number'];
        $address = $_POST['address'];
        $position = $_POST['position']; // 0 = Already registerd,1 = Not Registered
        $qualification = $_POST['qualification'];
        $about = $_POST['about'];
        $video_link = $_POST['video_link'];
        $recent_picture = $_POST['recent_picture'];
        $teacher_title = $_POST['teacher_title'];


        if($about == '')
        {
          $about = '0';
        }

        
        if($video_link == '')
        {
          $video_link = '0';
        }
        
          if(!empty($_FILES['picture']['name']))
          {
            $file = $_FILES['picture']['name'];
            $tmp_file = $_FILES['picture']['tmp_name']; //ASSIGN DATA TO VARIABLE
            $ext = pathinfo($_FILES["picture"]["name"], PATHINFO_EXTENSION); //GET EXTENTION ON IMAGE
            $rand = hexdec(uniqid()); //NAME CONVERT TO ENCRYPT
            $post_file = $rand.".".$ext; //ADD IMAGE NAME TO ENCRYPTED NAME
            move_uploaded_file($tmp_file,"../../teacher/images/profile/".$post_file); //SAVE LOCATION
          }else
          {
            $post_file = $recent_picture;
          }


          $today = date('Y-m-d h:i:s A');
          

        $insert001 = mysqli_query($conn,"UPDATE `teacher_details` SET `F_NAME`= '$fnm',`L_NAME`= '$lnm',`DOB`= '$dob',`EMAIL`= '$email',`ADDRESS`= '$address',`TP`= '$tp',`POSITION`= '$position',`GENDER`= '$gen',`QUALIFICATION`= '$qualification',`INTRODUCTION`= '$about',`PICTURE`= '$post_file',`INTRO_VIDEO`= '$video_link',`CLASS_TITLE` = '$teacher_title' WHERE `TEACH_ID`= '$teach_id'");

          header('location:../../new_admin/teacher_approved.php');
        
      }else
      if(isset($_GET['approved_teach'])) //APPROVED variable OK**
      {
          $teach_id = $_GET['approved_teach'];
          $approve = $_GET['approve'];

          if($approve == 'Disapprove')
          {
            $update = mysqli_query($conn,"UPDATE `teacher_login` SET `STATUS`= 'Delete' WHERE `TEACH_ID`= '$teach_id'");

          }

          header('location:../../new_admin/teacher_approved.php');

      }else
      if(isset($_GET['reactive_teach'])) //APPROVED variable OK**
      {
          $teach_id = $_GET['reactive_teach'];

            $update = mysqli_query($conn,"UPDATE `teacher_login` SET `STATUS`= 'Active' WHERE `TEACH_ID`= '$teach_id'");


          header('location:../../new_admin/teacher_reactive.php');

      }else
      if(isset($_POST['update_admin_data']))
      {
          $name = ucwords($_POST['full_name']);
          $gender = $_POST['gender'];
          $admin_id = $_POST['update_admin_data'];

          $insert003 = mysqli_query($conn,"UPDATE `admin_login` SET `ADMIN_NAME`= '$name',`GENDER`= '$gender' WHERE `ADMIN_ID`= '$admin_id'");

          header('location:../../new_admin/create_admin.php');

      }else
      if(isset($_GET['status_admin_data']))
      {
          $status = $_GET['status'];
          $admin_id = $_GET['status_admin_data'];

          if($status == 'Active')
          {
            $insert003 = mysqli_query($conn,"UPDATE `admin_login` SET `STATUS`= 'Deactive' WHERE `ADMIN_ID`= '$admin_id'");
          }else
          if($status == 'Deactive')
          {
            $insert003 = mysqli_query($conn,"UPDATE `admin_login` SET `STATUS`= 'Active' WHERE `ADMIN_ID`= '$admin_id'");
          }

          header('location:../../new_admin/create_admin.php');

      }else
      if(isset($_GET['status_staff_data']))
      {
          $status = $_GET['status'];
          $staff_id = $_GET['status_staff_data'];

          if($status == 'Active')
          {
            $insert003 = mysqli_query($conn,"UPDATE `admin_login` SET `STATUS`= 'Deactive' WHERE `ADMIN_ID`= '$staff_id'");
          }else
          if($status == 'Deactive')
          {
            $insert003 = mysqli_query($conn,"UPDATE `admin_login` SET `STATUS`= 'Active' WHERE `ADMIN_ID`= '$staff_id'");
          }


          header('location:../../new_admin/create_staff.php');

      }else
      if(isset($_POST['update_staff_data']))
      {
          $name = ucwords($_POST['full_name']);
          $gender = $_POST['gender'];
          $staff_id = $_POST['update_staff_data'];

          $insert003 = mysqli_query($conn,"UPDATE `admin_login` SET `ADMIN_NAME`= '$name',`GENDER`= '$gender' WHERE `ADMIN_ID`= '$staff_id'");

          header('location:../../new_admin/create_staff.php');

      }else
      if(isset($_POST['update_new_teacher']))
      {
        $teach_id = $_POST['update_new_teacher'];
        $fnm = ucwords($_POST['fnm']);
        $lnm = ucwords($_POST['lnm']);
        $dob = date("Y-m-d", strtotime($_POST['dob']));
        $gen = $_POST['gender'];
        $email = $_POST['email'];
        $tp = $_POST['number'];
        $address = $_POST['address'];
        $position = $_POST['position']; // 0 = Already registerd,1 = Not Registered
        $qualification = $_POST['qualification'];

        

        $up_date = date('Y-m-d');
        $up_time = date('h:i:s A');

        $upload_time = date('Y-m-d h:i:s A');

        if($_FILES["picture"]["name"] !== '0')
        {
          $recent_file = $_POST['recent_file'];

          unlink('../../teacher/images/profile/'.$recent_file);
          
          $tmp_file = $_FILES['picture']['tmp_name']; //ASSIGN DATA TO VARIABLE
          $ext = pathinfo($_FILES["picture"]["name"], PATHINFO_EXTENSION); //GET EXTENTION ON IMAGE
          $rand = hexdec(uniqid()); //NAME CONVERT TO ENCRYPT
          $post_file = $rand.".".$ext; //ADD IMAGE NAME TO ENCRYPTED NAME

          move_uploaded_file($tmp_file,'../../teacher/images/profile/'.$post_file); //SAVE LOCATION


        }else
        {
          $post_file = '0';
        }

          $today = date('Y-m-d h:i:s A');
          

        $insert001 = mysqli_query($conn,"UPDATE `teacher_details` SET `F_NAME`= '$fnm',`L_NAME`= '$lnm',`DOB`= '$dob',`EMAIL`= '$email',`ADDRESS`= '$address',`TP`= '$tp',`POSITION`= '$position',`GENDER`= '$gen',`QUALIFICATION`= '$qualification',`PICTURE`= '$post_file' WHERE `TEACH_ID`= '$teach_id'");

        header('location:../../new_admin/create_teacher.php');
        
      }else
      if(isset($_GET['status_teacher_data']))
      {
          $status = $_GET['status'];
          $teach_id = $_GET['status_teacher_data'];

          if($status == 'Active')
          {
            $insert003 = mysqli_query($conn,"UPDATE `teacher_login` SET `STATUS`= 'Deactive' WHERE `TEACH_ID`= '$teach_id'");
          }else
          if($status == 'Deactive')
          {
            $insert003 = mysqli_query($conn,"UPDATE `teacher_login` SET `STATUS`= 'Active' WHERE `TEACH_ID`= '$teach_id'");
          }

          header('location:../../new_admin/create_teacher.php');

      }else
      if(isset($_POST['edit_subject123']))
      {
          $subject_id = $_POST['edit_subject123'];

          $level_id = $_POST['level_id'];
          $recent_file = $_POST['recent_file'];

          $subject = ucwords($_POST['subject']);
          $level_id = $_POST['level_id'];
          $super_subject = $_POST['super_subject'];

          if(!empty($_FILES['picture']['name']))
          {
            if($recent_file !== 'subject.png')
            {
              unlink('../images/subject_image/'.$recent_file);
            }
            
            $file = $_FILES['picture']['name'];

            $tmp_file = $_FILES['picture']['tmp_name']; //ASSIGN DATA TO VARIABLE
            $ext = pathinfo($_FILES["picture"]["name"], PATHINFO_EXTENSION); //GET EXTENTION ON IMAGE
            $rand = hexdec(uniqid()); //NAME CONVERT TO ENCRYPT
            $post_file = $rand.".".$ext; //ADD IMAGE NAME TO ENCRYPTED NAME
            move_uploaded_file($tmp_file,"../images/subject_image/".$post_file); //SAVE LOCATION


            $insert001 = mysqli_query($conn,"UPDATE `subject` SET `SUBJECT_NAME`='$subject',`LEVEL_ID`='$level_id',`PICTURE`='$post_file',`SH_LEVEL`='$super_subject' WHERE `SUB_ID` = '$subject_id'");
          }else
          if(empty($_FILES['picture']['name']))
          {
            if($recent_file !== 'subject.png')
            {
              unlink('../images/subject_image/'.$recent_file);
            }
            
            $insert001 = mysqli_query($conn,"UPDATE `subject` SET `SUBJECT_NAME`='$subject',`LEVEL_ID`='$level_id',`PICTURE`='0',`SH_LEVEL`='$super_subject' WHERE `SUB_ID` = '$subject_id'");
          }

         header('location:../../new_admin/add_subject.php');

      }else
      if(isset($_POST['edit_level123']))
      {
          $level_id = $_POST['edit_level123'];

          $recent_file = $_POST['recent_file'];

          $level = ucwords($_POST['level']);

          $sn = $_POST['sn'];

          if(!empty($_FILES['picture']['name']))
          {
            if($recent_file !== 'subject.png')
            {
              unlink('../images/level_image/'.$recent_file);
            }
            
            $file = $_FILES['picture']['name'];

            $tmp_file = $_FILES['picture']['tmp_name']; //ASSIGN DATA TO VARIABLE
            $ext = pathinfo($_FILES["picture"]["name"], PATHINFO_EXTENSION); //GET EXTENTION ON IMAGE
            $rand = hexdec(uniqid()); //NAME CONVERT TO ENCRYPT
            $post_file = $rand.".".$ext; //ADD IMAGE NAME TO ENCRYPTED NAME
            move_uploaded_file($tmp_file,"../images/level_image/".$post_file); //SAVE LOCATION


            $insert001 = mysqli_query($conn,"UPDATE `level` SET `LEVEL_NAME`='$level',`SHORT_NAME`='$sn',`PICTURE`='$post_file' WHERE `LEVEL_ID` = '$level_id'");
          }else
          if(empty($_FILES['picture']['name']))
          {
            if($recent_file !== 'subject.png')
            {
              unlink('../images/level_image/'.$recent_file);
            }
            
            $insert001 = mysqli_query($conn,"UPDATE `level` SET `LEVEL_NAME`='$level',`SHORT_NAME`='$sn',`PICTURE`='0' WHERE `LEVEL_ID` = '$level_id'");
          }

         header('location:../../new_admin/add_level.php');

      }else
      if(isset($_GET['admin_approve']))
      {
          $pay_id = $_GET['admin_approve'];
          $status = $_GET['status'];
          $fees = $_GET['fees'];

          $now = date('Y-m-d h:i:s A');
          $today = date('Y-m-d');

          if($status == 'Approve')
          {
              mysqli_query($conn,"UPDATE `payment_data` SET `ADMIN_SUBMIT`= '1' WHERE `PAY_ID`= '$pay_id'");
              mysqli_query($conn,"INSERT INTO `income`(`PAYMENT`, `ADD_DATE`, `ADD_D_T`, `PAY_ID`) VALUES ('$fees','$today','$now','$pay_id')");
              echo "Approved";
          }else
          if($status == 'Disapprove')
          {
              mysqli_query($conn,"UPDATE `payment_data` SET `ADMIN_SUBMIT`= '2' WHERE `PAY_ID`= '$pay_id'");
              mysqli_query($conn,"DELETE FROM `income`WHERE `PAY_ID`= '$pay_id'");
              echo "Disapproved";
          }

      }else
      if(isset($_GET['remove_img2']))
      {

          $admin_id = $_GET['remove_img2'];
          $recent_img = $_GET['recent_img2'];

          $sql = mysqli_query($conn,"UPDATE `institute` SET `PICTURE`='0' WHERE `INS_ID` = '1'");

          if($recent_img !== 'ins.png')
          {
            unlink('../images/institute/'.$recent_img);
          }

            header('location:../../new_admin/profile_setting.php');

        }else
      if(isset($_POST['upload_img2']))
      {
        $admin_id = $_POST['upload_img2'];

        $recent_img = $_POST['recent_img2'];

        

        if($recent_img !== '0')
        {
          if($recent_img !== 'female_admin.png' || $recent_img !== 'male_admin.png')
          {
            unlink('../images/profile/'.$recent_img);
          }
        }

        if(!empty($_FILES['img2']['name']))
        {   
            $file = $_FILES['img2']['name'];
            $tmp_file = $_FILES['img2']['tmp_name']; //ASSIGN DATA TO VARIABLE
            $ext = pathinfo($_FILES["img2"]["name"], PATHINFO_EXTENSION); //GET EXTENTION ON IMAGE
            $rand = hexdec(uniqid()); //NAME CONVERT TO ENCRYPT
            $post_file = $rand.".".$ext; //ADD IMAGE NAME TO ENCRYPTED NAME

            move_uploaded_file($tmp_file,'../images/institute/'.$post_file); //SAVE LOCATION
            
            $update001 = mysqli_query($conn,"UPDATE `institute` SET `PICTURE`='$post_file' WHERE `INS_ID` = '1'");

        }else
        if(empty($_FILES['img2']['name']))
        {
            $update001 = mysqli_query($conn,"UPDATE `institute` SET `PICTURE`='0' WHERE `INS_ID` = '1'");

        }

            
          header('location:../../new_admin/profile_setting.php');
      
    }else
      if(isset($_POST['update_institute']))
      {
        $name = ucwords($_POST['name']);

        $tp = $_POST['tp'];

        $mobile = $_POST['mobile'];

        $address = $_POST['address'];

        $web_address = $_POST['web'];

        $reg_code = $_POST['reg_code'];

        $sms_footer = $_POST['sms_footer'];

        $student_profile_color = $_POST['student_profile_color'];
        

        if(empty($_POST['tp']))
        {
          $tp = '0';
        }

        if(empty($_POST['mobile']))
        {
          $mobile = '0';
        }


          $today = date('Y-m-d h:i:s A');
          

        $insert001 = mysqli_query($conn,"UPDATE `institute` SET `INS_NAME`= '$name',`INS_TP`= '$tp',`INS_ADDRESS`= '$address',`INS_MOBILE`= '$mobile',`REG_CODE`= '$reg_code',`WEB`= '$web_address',`BG_COLOR`= '$student_profile_color',`SMS_FOOTER`= '$sms_footer' WHERE `INS_ID`= '1'");

          header('location:../../new_admin/profile_setting.php');
        
      }else
      if(isset($_POST['switch_free']))
      {
        $msg = $_POST['switch_free'];

        if($msg == 'Yes')
        {
            mysqli_query($conn,"UPDATE `institute` SET `FREE`= 'Yes' WHERE `INS_ID`= '1'");
        }else
        if($msg == 'No')
        {
            mysqli_query($conn,"UPDATE `institute` SET `FREE`= 'No' WHERE `INS_ID`= '1'");
        }

        echo $msg;
      }else
      if(isset($_GET['status_student_data2']))
      {
          $status = $_GET['status'];
          $stu_id = $_GET['status_student_data2'];

          if($status == 'Active')
          {
            $insert003 = mysqli_query($conn,"UPDATE `stu_login` SET `STATUS`= 'Deactive' WHERE `STU_ID`= '$stu_id'");
          }else
          if($status == 'Deactive')
          {
            $insert003 = mysqli_query($conn,"UPDATE `stu_login` SET `STATUS`= 'Active' WHERE `STU_ID`= '$stu_id'");
          }

          header('location:../../new_admin/create_student.php');

      }else
      if(isset($_POST['update_student']))
      {
        $stu_id = $_POST['update_student'];
        $fnm = ucwords($_POST['fnm']);
        $lnm = ucwords($_POST['lnm']);
        $dob = $_POST['year']."-".$_POST['month']."-".$_POST['day'];
        $gen = $_POST['gender'];
        $email = $_POST['email'];
        $tp = $_POST['number'];
        $address = $_POST['address'];
        $new = '1';//$_POST['new']; // 0 = Already registerd,1 = Not Registered


          $today = date('Y-m-d h:i:s A');
          

        $insert001 = mysqli_query($conn,"UPDATE `student_details` SET `F_NAME`= '$fnm',`L_NAME`= '$lnm',`DOB`= '$dob',`EMAIL`= '$email',`ADDRESS`= '$address',`TP`= '$tp',`NEW`= '$new',`GENDER`= '$gen' WHERE `STU_ID`= '$stu_id'");

          header('location:../../new_admin/create_student.php');
        
      }else
      if(isset($_POST['update_search_student']))
      {
        $stu_id = $_POST['update_search_student'];
        $fnm = ucwords($_POST['fnm']);
        $lnm = ucwords($_POST['lnm']);
        $dob = $_POST['year']."-".$_POST['month']."-".$_POST['day'];
        $gen = $_POST['gender'];
        $email = $_POST['email'];
        $tp = $_POST['number'];
        $address = $_POST['address'];
        $new = '1';//$_POST['new']; // 0 = Already registerd,1 = Not Registered


          $today = date('Y-m-d h:i:s A');
          

        $insert001 = mysqli_query($conn,"UPDATE `student_details` SET `F_NAME`= '$fnm',`L_NAME`= '$lnm',`DOB`= '$dob',`EMAIL`= '$email',`ADDRESS`= '$address',`TP`= '$tp',`NEW`= '$new',`GENDER`= '$gen' WHERE `STU_ID`= '$stu_id'");

        header('location:../../new_admin/student_search.php');
        
      }else
      if(isset($_POST['edit_alert']))
      {
        $title = $_POST['title'];
        $msg123 = $_POST['message001'];
        $alert_id = $_POST['edit_alert'];
        $audience = $_POST['audience'];
        $times = '1';//$_POST['times'];

        $pub_date = date('Y-m-d h:i:s A');
        $today = date('Y-m-d');

        mysqli_query($conn,"UPDATE `alert` SET `MESSAGE`= '$msg123',`AUDIENCE`= '$audience',`ADDED_DATE`= '$today',`TITLE`= '$title',`PUBLISH`='Yes',`PUB_DATE`= '$pub_date',`TIMES`= '$times' WHERE `ALERT_ID`= '$alert_id'");


        //mysqli_query($conn,"UPDATE `student_details` SET `ALERT` = '1'");


        if($audience == 'Public')
        {

         header('location:../../new_admin/edit_alert.php?alert_id='.$alert_id.' ');

        }else
        if($audience == 'Private')
        {
          

          if(!empty($_POST['check_stu']))
          {

              $check = $_POST['check_stu'];
              $size = sizeof($check);

              mysqli_query($conn,"DELETE FROM custom_student WHERE ALERT_ID = '$alert_id'");

              for ($i=0; $i < $size; $i++)
              { 

                $sql0012 = mysqli_query($conn,"SELECT * FROM custom_student WHERE STU_ID = '$check[$i]'");
                $row12 = mysqli_fetch_assoc($sql0012);
                $stu = $row12['STU_ID'];

                
                    
                mysqli_query($conn,"INSERT INTO `custom_student`(`STATUS`, `ALERT_ID`, `STU_ID`) VALUES ('OK','$alert_id','$check[$i]')");

                  
              }
          }

         header('location:../../new_admin/edit_alert.php?alert_id='.$alert_id.' ');
      
        }else
        if($audience == 'Teacher')
        {
          $teach_id = $_POST['teacher'];
          $sub_id = $_POST['subject'];
          $class_id = $_POST['class_id'];

          mysqli_query($conn,"DELETE FROM custom_student WHERE ALERT_ID = '$alert_id'");

          $sql002233 = mysqli_query($conn,"SELECT * FROM `transactions` WHERE `CLASS_ID`= '$class_id' AND `TEACH_ID` = '$teach_id'");
          //echo mysqli_num_rows($sql002233);
          while($row002233 = mysqli_fetch_assoc($sql002233))
          {
              $student_ids = $row002233['STU_ID'];

              mysqli_query($conn,"INSERT INTO `custom_student`(`STATUS`, `ALERT_ID`, `STU_ID`, `SHOW_TIMES`) VALUES ('OK','$alert_id','$student_ids','$times')");
          }
              
         header('location:../../new_admin/edit_alert.php?alert_id='.$alert_id.' ');
          
        }

      }else
      if(isset($_GET['publish']))
      {

          $alert_id = $_GET['publish'];
          $approve = $_GET['approve'];

          if($approve == 'OK')
          {
            mysqli_query($conn,"UPDATE `alert` SET `PUBLISH`='No' WHERE `ALERT_ID` = '$alert_id'");

            mysqli_query($conn,"UPDATE `student_details` SET `ALERT` = '0'");

          }else
          if($approve == 'NO')
          {
            mysqli_query($conn,"UPDATE `alert` SET `PUBLISH`='Yes' WHERE `ALERT_ID` = '$alert_id'");

            mysqli_query($conn,"UPDATE `student_details` SET `ALERT` = '1'");

          }

          header('location:../../new_admin/alert.php ');

        }else
        if(isset($_POST['edit_expenditure']))
        {

          $reason = ucwords($_POST['reason']);
          $reason_id = $_POST['edit_expenditure'];

          $now = date('Y-m-d h:i:s A');
          $today = date('Y-m-d');

          $sql002 = mysqli_query($conn,"UPDATE `reason` SET `REASON`= '$reason',`ADD_DATE`= '$today',`ADD_D_T`= '$now' WHERE `REASON_ID`= '$reason_id'");

         header('location:../../new_admin/reason.php');


        }else
        if(isset($_POST['active_msg']))
        {
          $sms = $_POST['active_msg'];

          $sql002 = mysqli_query($conn,"UPDATE `institute` SET `SMS_ACTIVE`= '$sms' WHERE `INS_ID`= '1'");

          echo "OK";

        }else
        if(isset($_POST['public_share']))
        {
          $p_s = $_POST['public_share'];
          $upload_id = $_POST['upload_id'];

          $sql002 = mysqli_query($conn,"UPDATE `uploads` SET `PUBLIC`= '$p_s' WHERE `UPLOAD_ID`= '$upload_id'");

          echo "OK";

        }else
        if(isset($_POST['active_single_msg']))
        {
          $sms = $_POST['active_single_msg'];

          $sql002 = mysqli_query($conn,"UPDATE `institute` SET `SINGLE_SMS`= '$sms' WHERE `INS_ID`= '1'");

          echo "OK";

        }else
        if(isset($_POST['reset_psw']))
        {
          $stu_reg_code = $_POST['reset_psw'];
          $password = md5($_POST['password']);

          $sql001 = mysqli_query($conn,"SELECT * FROM `stu_login` WHERE `REGISTER_ID` = '$stu_reg_code'");
          while($row001=mysqli_fetch_assoc($sql001))
          {
            $stu_id = $row001['STU_ID'];

          }

          $sql002 = mysqli_query($conn,"UPDATE `stu_login` SET `PASSWORD`= '$password' WHERE `REGISTER_ID`= '$stu_reg_code' AND `STU_ID`= '$stu_id'");

          echo "OK";

        }else
        if(isset($_GET['regen_id']))
        {
          $stu_id = $_GET['regen_id'];

          $sql001 = mysqli_query($conn,"SELECT * FROM `reg_count` WHERE `REG_COUNT_ID` = '1'");
          while($row001=mysqli_fetch_assoc($sql001))
          {
            $current_id = $row001['CURRENT_REG_ID'];
          }

          $sql002 = mysqli_query($conn,"SELECT * FROM `institute` WHERE `INS_ID` = '1'");
          while($row002=mysqli_fetch_assoc($sql002))
          {
            $reg_code = $row002['REG_CODE'];
          }

          $reg_id = $reg_code.sprintf('%05d',$current_id);

          $new_cu_id = $current_id+1;


          $sql003 = mysqli_query($conn,"UPDATE `stu_login` SET `REGISTER_ID`= '$reg_id' WHERE `STU_ID`= '$stu_id'");

          $sql004 = mysqli_query($conn,"UPDATE `reg_count` SET `CURRENT_REG_ID`= '$new_cu_id' WHERE `REG_COUNT_ID`= '1'");

          header('location:../../new_admin/erase_duplicate.php');

        }else
        if(isset($_POST['free_class_active']))
        {
          $class_id = $_POST['class_id'];

          $start_date = $_POST['start_date'];

          $end_date = $_POST['end_date'];

          $material = $_POST['material'];

          $sql004 = mysqli_query($conn,"UPDATE `classes` SET `FREE`= '1',`START_DATE`='$start_date',`END_DATE` = '$end_date',`MATERIAL` = '$material' WHERE `CLASS_ID`= '$class_id'");

          echo "OK";

        }else
        if(isset($_POST['free_class_deactive']))
        {
          $material = $_POST['material'];

          $class_id = $_POST['class_id'];
          
          $start_date = $_POST['start_date'];

          $end_date = $_POST['end_date'];

          $sql004 = mysqli_query($conn,"UPDATE `classes` SET `FREE`= '0',`START_DATE`='$start_date',`END_DATE` = '$end_date',`MATERIAL` = '$material' WHERE `CLASS_ID`= '$class_id'");

          echo "OK";

        }else
        if(isset($_POST['reg_stu_without_check']))
        {
          $without_active = $_POST['reg_stu_without_check'];

          $sql002 = mysqli_query($conn,"UPDATE `institute` SET `REG_WITH_ACTIVE`= '$without_active' WHERE `INS_ID`= '1'");

          echo "OK";

        }else
        if(isset($_POST['active_exam']))
        {
          $active_exam = $_POST['active_exam'];

          $sql002 = mysqli_query($conn,"UPDATE `institute` SET `EXAM_TAB`= '$active_exam' WHERE `INS_ID`= '1'");

          echo "OK";

        }else
        if(isset($_POST['search_free_card_students']))
        {
          $class_id = $_POST['search_free_card_students'];
          $teach_id = $_POST['teach_id'];

          $sql002 = mysqli_query($conn,"SELECT * FROM `transactions` WHERE `TEACH_ID` = '$teach_id' AND `CLASS_ID` = '$class_id'");
          if(mysqli_num_rows($sql002)>0)
          {

              while($row003 = mysqli_fetch_assoc($sql002))
              {
                  
                  $tra_id = $row003['TRA_ID'];
                  $stu_id = $row003['STU_ID'];
                  $free_class = $row003['FREE_CLASS'];

                  if($free_class == '0')
                  {
                    $icon = '<label class="label label-danger">Payment</label>';
                    $btn = '<button id="btn_status'.$tra_id.'" value="0" style="display:none;"></button><button type="button" class="btn btn-success rounded-circle" style="box-shadow:0px 1px 10px 2px #cccc;font-size:10px;" id="add_free_card'.$tra_id.'" value="'.$tra_id.'"><span class="fa fa-plus"></span></button>';
                  }else
                  if($free_class == '1')
                  {
                    $icon = '<label class="label label-success">Free Card</label>';
                    $btn = '<button id="btn_status'.$tra_id.'" value="1" style="display:none;"></button>
                    <button type="button" class="btn btn-danger rounded-circle" style="box-shadow:0px 1px 10px 2px #cccc;font-size:10px;" id="add_free_card'.$tra_id.'" value="'.$tra_id.'"><span class="fa fa-times"></span></button>';
                  }

                  $sql00004 = mysqli_query($conn,"SELECT * FROM `student_details` WHERE `STU_ID` = '$stu_id'");
                  while($row00004 = mysqli_fetch_assoc($sql00004))
                  {
                    $student_name = $row00004['F_NAME']." ".$row00004['L_NAME'];
                  }

                  $sql00005 = mysqli_query($conn,"SELECT * FROM `stu_login` WHERE `STU_ID` = '$stu_id'");
                  while($row00005 = mysqli_fetch_assoc($sql00005))
                  {
                    $reg = $row00005['REGISTER_ID'];
                  }

                  echo '
                    <tr>
                      <td style="display:none;">'.$reg.'</td>
                      <td>'.$student_name.'<br> <small>'.$reg.'</small></td>
                      <td id="free_card_status'.$tra_id.'">'.$icon.'</td>
                      <td class="text-center" id="free_card_btn'.$tra_id.'">'.$btn.'</td>
                    </tr>

                  ';

                  ?>

                  <script type="text/javascript">
  

                      $('#add_free_card<?php echo $tra_id; ?>').bind('click',function(){
                                    
                          var tra_id = $('#add_free_card<?php echo $tra_id; ?>').val();
                          var status = $('#btn_status<?php echo $tra_id; ?>').val();

                          //alert(status)

                           $.ajax({
                            url:'../admin/query/check.php',
                            method:"POST",
                            data:{add_free_cards:tra_id,status:status},
                            success:function(data)
                            {
                              
                              if(data == 1)
                              {
                                $('#free_card_status<?php echo $tra_id; ?>').html('<label class="label label-success">Free Card</label>');

                                $('#free_card_btn<?php echo $tra_id; ?>').html('<button id="btn_status<?php echo $tra_id; ?>" value="1" style="display:none;"></button><button type="button" class="btn btn-danger rounded-circle" style="box-shadow:0px 1px 10px 2px #cccc;font-size:10px;" id="add_free_card<?php echo $tra_id; ?>" value="<?php echo $tra_id; ?>"><span class="fa fa-times"></span></button>');
                              }

                              if(data == 0)
                              {
                                $('#free_card_status<?php echo $tra_id; ?>').html('<label class="label label-danger">Payment</label>');

                                $('#free_card_btn<?php echo $tra_id; ?>').html('<button id="btn_status<?php echo $tra_id; ?>" value="0" style="display:none;"></button><button type="button" class="btn btn-success rounded-circle" style="box-shadow:0px 1px 10px 2px #cccc;font-size:10px;" id="add_free_card<?php echo $tra_id; ?>" value="<?php echo $tra_id; ?>"><span class="fa fa-plus"></span></button>');
                              }

                            }
                          });
                    });
                  </script>

                  <?php
                  
              }
             
          }else
          if(mysqli_num_rows($sql002) == '0')
          {
              echo '<tr><td colspan="3" class="text-center text-danger"><span class="fa fa-warning"></span> Not Found Data</td></tr>';
          }

        }else
      if(isset($_POST['edit_grade123']))
      {
          $grade_id = $_POST['edit_grade123'];
          $grade_name = ucwords($_POST['grade_name']);
          $grade_no = $_POST['grade_no'];

          
            $update001 = mysqli_query($conn,"UPDATE `grade` SET `GRADE_NAME`= '$grade_name',`GRADE_NO`= '$grade_no' WHERE `GRADE_ID`= '$grade_id'");
          

         header('location:../../new_admin/add_grade.php');

      }else
      if(isset($_POST['clear_multi_login']))
      {
          $stu_id = $_POST['clear_multi_login'];

          mysqli_query($conn,"UPDATE `stu_login` SET `LOGGED`= '0' WHERE `STU_ID`= '$stu_id'");
          
          echo "OK";
      }else
      if(isset($_POST['update_paper']))
      {

        $paper_name = $_POST['paper_name'];
        $paper_id = $_POST['update_paper'];
        $exam_key = $_POST['exam_key'];
        $class_id = $_POST['class_id'];
        $teacher_id = $_POST['teacher_id'];
        $hour = $_POST['hour'];
        $minute = $_POST['minute'];
        $start_date = $_POST['start_date'];
        $end_date = $_POST['end_date'];

        $exam_type = '1';

        if($exam_type == '1')
        {
          $mark = $_POST['mark'];
        }else
        if($exam_type == '2')
        {
          $mark = '0';
        }

        if($hour == '')
        {
          $hour = '0';
        }else
        if($minute == '')
        {
          $minute = '0';
        }


        $max_hour = $_POST['max_hour'];
        $max_minu = $_POST['max_minu'];

        $recent_file = $_POST['recent_file'];

        $str1 = strtotime($start_date);
        $strdate = date('Y-m-d',$str1);
        $strtime = date('H:i:s',$str1);

        $end1 = strtotime($end_date);
        $enddate = date('Y-m-d',$end1);
        $endtime = date('H:i:s',$end1);

        $start_date = date('Y-m-d H:i:s',$str1);
        $end_date = date('Y-m-d H:i:s',$end1);

        if(!empty($_FILES['upload_file']['name']))
          {
            unlink('../images/paper_document/'.$recent_file.'');
            $file = $_FILES['upload_file']['name'];
            $tmp_file = $_FILES['upload_file']['tmp_name']; //ASSIGN DATA TO VARIABLE
            $ext = pathinfo($_FILES["upload_file"]["name"], PATHINFO_EXTENSION); //GET EXTENTION ON IMAGE
            $rand = hexdec(uniqid()); //NAME CONVERT TO ENCRYPT
            $post_file = $rand.".".$ext; //ADD IMAGE NAME TO ENCRYPTED NAME
            move_uploaded_file($tmp_file,"../images/paper_document/".$post_file); //SAVE LOCATION
          }else
          if(empty($_FILES['upload_file']['name']))
          {
            $post_file = $recent_file;
          }

        
        if($max_hour == '0' && $max_minu !== '0' || $max_hour !== '0' && $max_minu == '0' || $max_hour !== '0' && $max_minu !== '0')
        {
          $date = new DateTime($end_date);
          $date->modify('-'.$max_hour.' hour'.''.'-'.$max_minu.' minute');
          $last_attend = $date->format("Y-m-d H:i:s");

        }else
        if($max_hour == '0' && $max_minu == '0')
        {
          $last_attend = $end_date;
        }

        mysqli_query($conn,"UPDATE `paper_master` SET `LAST_ATTENT_TIME`= '$last_attend' WHERE `PAPER_ID`= '$paper_id'");
            
          

          $current_time = date('Y-m-d h:i:s A');
          $current_date = date('Y-m-d');

          //MCQ Paper = 1
          //Structured Paper = 2

          $update001 = mysqli_query($conn,"UPDATE `paper_master` SET `PAPER_NAME`= '$paper_name',`EXAM_KEY`= '$exam_key',`ANSWER_MARKS`= '$mark',`PDF_NAME`= '$post_file',`UPLOAD_BY`= '$teacher_id',`TIME_DURATION_HOUR`= '$hour',`TIME_DURATION_MIN`= '$minute',`FINISH_DATE`='$enddate',`FINISH_TIME`= '$endtime',`CLASS_ID`= '$class_id',`START_DATE`= '$strdate',`START_TIME`= '$strtime',`FINISH_D_T`='$end_date',`START_D_T`= '$start_date',`MAX_HOUR`= '$max_hour',`MAX_MINUTE`= '$max_minu',`PAPER_TYPE` = '$exam_type' WHERE `PAPER_ID`= '$paper_id'");

          header('location:../../new_admin/exam.php');


      }else
      if(isset($_POST['update_st_paper']))
      {


        $paper_name = $_POST['paper_name'];
        $paper_id = $_POST['update_st_paper'];
        $exam_key = $_POST['exam_key'];
        $class_id = $_POST['class_id'];
        $teacher_id = $_POST['teacher_id'];
        $hour = $_POST['hour'];
        $minute = $_POST['minute'];
        $start_date = $_POST['start_date'];
        $end_date = $_POST['end_date'];

        $exam_type = '2';

        if($exam_type == '1')
        {
          $mark = $_POST['mark'];
        }else
        if($exam_type == '2')
        {
          $mark = '0';
        }

        if($hour == '')
        {
          $hour = '0';
        }else
        if($minute == '')
        {
          $minute = '0';
        }


        $max_hour = $_POST['max_hour'];
        $max_minu = $_POST['max_minu'];

        $recent_file = $_POST['recent_file'];

        $str1 = strtotime($start_date);
        $strdate = date('Y-m-d',$str1);
        $strtime = date('H:i:s',$str1);

        $end1 = strtotime($end_date);
        $enddate = date('Y-m-d',$end1);
        $endtime = date('H:i:s',$end1);

        $start_date = date('Y-m-d H:i:s',$str1);
        $end_date = date('Y-m-d H:i:s',$end1);

        if(!empty($_FILES['upload_file']['name']))
          {

            unlink('../images/paper_document/'.$recent_file.'');
            $file = $_FILES['upload_file']['name'];
            $tmp_file = $_FILES['upload_file']['tmp_name']; //ASSIGN DATA TO VARIABLE
            $ext = pathinfo($_FILES["upload_file"]["name"], PATHINFO_EXTENSION); //GET EXTENTION ON IMAGE
            $rand = hexdec(uniqid()); //NAME CONVERT TO ENCRYPT
            $post_file = $rand.".".$ext; //ADD IMAGE NAME TO ENCRYPTED NAME
            move_uploaded_file($tmp_file,"../images/str_document/".$post_file); //SAVE LOCATION

          }else
          if(empty($_FILES['upload_file']['name']))
          {
            $post_file = $recent_file;
          }
          
          if($max_hour == '0' && $max_minu !== '0' || $max_hour !== '0' && $max_minu == '0' || $max_hour !== '0' && $max_minu !== '0')
          {
            $date = new DateTime($end_date);
            $date->modify('-'.$max_hour.' hour'.''.'-'.$max_minu.' minute');
            $last_attend = $date->format("Y-m-d H:i:s");

          }else
          if($max_hour == '0' && $max_minu == '0')
          {
            $last_attend = $end_date;
          }

          mysqli_query($conn,"UPDATE `paper_master` SET `LAST_ATTENT_TIME`= '$last_attend' WHERE `PAPER_ID`= '$paper_id'");

          $current_time = date('Y-m-d h:i:s A');
          $current_date = date('Y-m-d');

          //MCQ Paper = 1
          //Structured Paper = 2

          $update001 = mysqli_query($conn,"UPDATE `paper_master` SET `PAPER_NAME`= '$paper_name',`EXAM_KEY`= '$exam_key',`ANSWER_MARKS`= '0',`PDF_NAME`= '$post_file',`UPLOAD_BY`= '$teacher_id',`TIME_DURATION_HOUR`= '$hour',`TIME_DURATION_MIN`= '$minute',`FINISH_DATE`='$enddate',`FINISH_TIME`= '$endtime',`CLASS_ID`= '$class_id',`START_DATE`= '$strdate',`START_TIME`= '$strtime',`FINISH_D_T`='$end_date',`START_D_T`= '$start_date',`MAX_HOUR`= '$max_hour',`MAX_MINUTE`= '$max_minu',`PAPER_TYPE` = '$exam_type' WHERE `PAPER_ID`= '$paper_id'");

          header('location:=../../new_admin/exam.php');


      }else
      if(isset($_POST['upload_file_name'])) 
      {

          $up_date = date('Y-m-d');
          $up_time = date('h:i:s A');
          $now_time = date('Y-m-d h:i:s A');

          $file = $_FILES['upload_file']['name'];

          $tmp_file = $_FILES['upload_file']['tmp_name']; //ASSIGN DATA TO VARIABLE
          $ext = pathinfo($_FILES["upload_file"]["name"], PATHINFO_EXTENSION); //GET EXTENTION ON IMAGE

          $base = basename($file,".".$ext);

          $sql001 = mysqli_query($conn,"SELECT * FROM `institute` WHERE `TIMETABLE` LIKE '%".$base."%'");
          $check = mysqli_num_rows($sql001);

          if($check > 0)
          {
              $p1 = basename($file,".".$ext);
              $post_file = $p1."(".$check.").".$ext;
          }else
          if($check == '0')
          {
              $post_file = $file;
          }

          move_uploaded_file($tmp_file,'../images/timetable/'.$post_file); //SAVE LOCATION

          $update = mysqli_query($conn,"UPDATE `institute` SET `TIMETABLE`='$post_file' WHERE `INS_ID` = '1'");
            
           
         header('location:../../new_admin/profile_setting.php');
          

      }else
      if(isset($_POST['clear_timetable'])) 
      {

          $file_name = $_POST['clear_timetable'];
          unlink('../images/timetable/'.$file_name);

          $update = mysqli_query($conn,"UPDATE `institute` SET `TIMETABLE`='0' WHERE `INS_ID` = '1'");
            

      }else
        if(isset($_POST['active_suspend']))
        {
          $active_suspend = $_POST['active_suspend'];

          $sql002 = mysqli_query($conn,"UPDATE `institute` SET `SUSPEND`= '$active_suspend' WHERE `INS_ID`= '1'");

          echo "OK";

        }else
      if(isset($_POST['switch_reg_btn_hide']))
      {
        $reg_btn = $_POST['switch_reg_btn_hide'];

        if($reg_btn == '0')
        {
            mysqli_query($conn,"UPDATE `institute` SET `ENABLE_REGISTER_BTN`= '1' WHERE `INS_ID`= '1'");
        }else
        if($reg_btn == '1')
        {
            mysqli_query($conn,"UPDATE `institute` SET `ENABLE_REGISTER_BTN`= '0' WHERE `INS_ID`= '1'");
        }

        echo $reg_btn;
        
      }else
      if(isset($_POST['active_limit']))
      {
        $limit_btn = $_POST['active_limit'];

        if($limit_btn == '0')
        {
            mysqli_query($conn,"UPDATE `institute` SET `CLZ_LIMIT`= '1' WHERE `INS_ID`= '1'");
        }else
        if($limit_btn == '1')
        {
            mysqli_query($conn,"UPDATE `institute` SET `CLZ_LIMIT`= '0' WHERE `INS_ID`= '1'");
        }

        echo $limit_btn;
      }else
      if(isset($_POST['setup_limit']))
      {
        $setup_limit = $_POST['setup_limit'];
        $today = date('Y-m-d');

        mysqli_query($conn,"UPDATE `institute` SET `NO_LIMIT`= '$setup_limit' WHERE `INS_ID`= '1'");

        mysqli_query($conn,"UPDATE `classes` SET `LIMIT_STUDENT`= '$setup_limit',`LAST_UPDATED`= '$today'");


       
        echo $limit_btn;
      }else
      if(isset($_GET['approved_teacher_deactive']))
      {
        $teach_id = $_GET['approved_teacher_deactive'];
        $page = $_GET['page'];

        mysqli_query($conn,"UPDATE `teacher_login` SET `STATUS`= 'Deactive' WHERE `TEACH_ID`= '$teach_id'");

         header('location:../../new_admin/teacher_approved.php?page='.$page.'');

      }else
      if(isset($_GET['approved_teacher_dis_confirm']))
      {
        $teach_id = $_GET['approved_teacher_dis_confirm'];
        $page = $_GET['page'];
        $confirm = $_GET['confirm'];

        mysqli_query($conn,"UPDATE `teacher_login` SET `CONFIRM`= '$confirm' WHERE `TEACH_ID`= '$teach_id'");

         header('location:../../new_admin/teacher_approved.php?page='.$page.'');

      }else
      if(isset($_GET['hold_teacher']))
      {
        $teach_id = $_GET['hold_teacher'];

        mysqli_query($conn,"UPDATE `teacher_login` SET `STATUS`= 'Hold' WHERE `TEACH_ID`= '$teach_id'");

         header('location:../../new_admin/teacher_pending.php');

      }else
      if(isset($_POST['active_free_days']))
      {
        $active_free_days = $_POST['active_free_days'];

        if($active_free_days == '0')
        {
            mysqli_query($conn,"UPDATE `institute` SET `FREE_WEEK`= '1' WHERE `INS_ID`= '1'");
        }else
        if($active_free_days == '1')
        {
            mysqli_query($conn,"UPDATE `institute` SET `FREE_WEEK`= '0' WHERE `INS_ID`= '1'");
        }

        echo "OK";
      }else
      if(isset($_POST['setup_free_days']))
      {
        $setup_free_days = $_POST['setup_free_days'];
        $today = date('Y-m-d');

        mysqli_query($conn,"UPDATE `institute` SET `NO_FREE_WEEK`= '$setup_free_days' WHERE `INS_ID`= '1'");
       
        echo 'OK';
      }else
      if(isset($_POST['update_todo']))
      {
        $reg_id = $_POST['student_name'];
        $comment = $_POST['comment'];
        $todo_id = $_POST['update_todo'];

        $sql001 = mysqli_query($conn,"SELECT * FROM `stu_login` WHERE `REGISTER_ID` = '$reg_id'");
        $row001 = mysqli_fetch_assoc($sql001);
        $stu_id = $row001['STU_ID'];

        $today = date('Y-m-d');
        $time = date('Y-m-d h:i:s A');

        $sql001 = mysqli_query($conn,"UPDATE `todo_list` SET `ADD_DATE`='$today',`ADD_DT`='$time',`COMMENT`='$comment',`STU_ID`='$stu_id',`REGISTER_ID`='$reg_id' WHERE `TODO_ID`='$todo_id'");

        header('location:../../new_admin/todo_list.php');
      }else
      if(isset($_POST['edit_grade']))
      {
        $level_id = $_POST['edit_grade'];
        $grade_name = $_POST['grade'];
        $grade_id = $_POST['grade_id'];

        $sql001 = mysqli_query($conn,"UPDATE `grade` SET `GRADE_NAME`='$grade_name',`LEVEL_ID`='$level_id' WHERE `GRADE_ID`='$grade_id'");

        header('location:../../new_admin/create_grade.php?level_id='.$level_id.'');


      }else
      if(isset($_POST['semi_update']))
      {
        $semi_id = $_POST['semi_update'];
        $semi_name = $_POST['semi_name'];
        $semi_date = $_POST['semi_date'];
        $semi_start = $_POST['semi_start'];
        $semi_end = $_POST['semi_end'];
        $semi_teacher = $_POST['semi_teacher'];
        $semi_link = $_POST['semi_link'];

        $whatsapp_link = $_POST['whatsapp_link'];
        $post_name = $_POST['post_name'];

        $current_image = $_POST['current_image'];

        if(empty($_POST['publish']))
        {
          $publish = '0';
        }else
        if(!empty($_POST['publish']))
        {
          $publish = $_POST['publish'];
        }

        $page = $_POST['page'];

        $str = strtotime($semi_date);
        $semi_day = date('l',$str);

        $add_date = date('Y-m-d');
        $add_time = date('Y-m-d h:i:s A');

        if(!empty($_FILES['post_image']['name']))
        {
          unlink('../../web/post/'.$current_image);

          $tmp_file = $_FILES['post_image']['tmp_name']; //ASSIGN DATA TO VARIABLE
          $ext = pathinfo($_FILES["post_image"]["name"], PATHINFO_EXTENSION); //GET EXTENTION ON IMAGE
          $rand = hexdec(uniqid()); //NAME CONVERT TO ENCRYPT
          $post_file = $rand.".".$ext; //ADD IMAGE NAME TO ENCRYPTED NAME
          move_uploaded_file($tmp_file,"../../web/post/".$post_file); //SAVE LOCATION
        }else
        if(empty($_FILES['post_image']['name']))
        {
          $post_file = $current_image;
        }

        /*if(empty($semi_link))
        {
          $semi_link = '0';
        }

        if(empty($whatsapp_link))
        {
          $whatsapp_link = '0';
        }*/

        /*$sql001 = mysqli_query($conn,"SELECT * FROM `seminar_master` WHERE `SEMI_ID` = '$semi_id'");
        while($row001 = mysqli_fetch_assoc($sql001))
        {
          $update_publish = $row001['PUBLISH'];
        }*/
          

          /*if($publish == '1' && empty($_FILES['post_image']['name']))
          {
            $publish = '0';
          }
        */


        $page = $_POST['page'];

        $str = strtotime($semi_date);
        $semi_day = date('l',$str);

        $add_date = date('Y-m-d');
        $add_time = date('Y-m-d h:i:s A');

        $insert001 = mysqli_query($conn,"UPDATE `seminar_master` SET `SEM_NAME`='$semi_name',`TEACH_ID`='$semi_teacher',`SEMINAR_LINK`='$semi_link',`DUE_DATE`='$semi_date',`START_TIME`='$semi_start',`END_TIME`='$semi_end',`DAY`='$semi_day',`POST_NAME`='$post_name',`POST_IMG`='$post_file',`WHATSAPP_LINK`='$whatsapp_link',`PUBLISH`='$publish' WHERE `SEMI_ID`='$semi_id'");

        header('location:../../new_admin/seminar.php?page='.$page.'');


      }else
      if(isset($_POST['update_bank_data']))
      {
        $holder = ucwords($_POST['holder_name']);
        $ac_no = $_POST['ac_no'];
        $bank_name = strtoupper($_POST['bank_name']);
        $branch = ucfirst($_POST['branch']);
        $page = $_POST['update_bank_data'];
        $last_pagination = $_POST['last_pagination'];


        $teach_id = $_POST['teacher_id'];
        $tbd_id = $_POST['tbd_id'];

        mysqli_query($conn,"UPDATE `teacher_bank_details` SET `HOLDER_NAME`='$holder',`BANK_NAME`='$bank_name',`ACCOUNT_NO`='$ac_no',`BRANCH_NAME`='$branch',`TEACH_ID`='$teach_id' WHERE `T_B_D_ID`= '$tbd_id'");

      
        header('location:../../new_admin/bank_details.php?page='.$page.'&&teacher_id='.$teach_id.'&&last_pagination='.$last_pagination.'');


      }else
      if(isset($_POST['update_mcq_answer']))
      {
        $paper_id = $_POST['update_mcq_answer'];
        $mcq_tr_id = $_POST['mcq_tr_id'];
        $answer = $_POST['answer'];
        $mark = $_POST['mark'];
        $question = $_POST['question_no'];


        $sql003 = mysqli_query($conn,"SELECT * FROM `paper_master` WHERE `PAPER_ID` = '$paper_id'"); 
       while($row003 = mysqli_fetch_assoc($sql003))
       {
          $total_question = $row003['NO_OF_QUESTIONS']; //Exam key
         
          $answer_mark = $row003['ANSWER_MARKS'];
       }

          $current_time = date('Y-m-d h:i:s A');
          $current_date = date('Y-m-d');

          for($i=0;$i<$total_question;$i++)
          {
            $update001 = mysqli_query($conn,"UPDATE `mcq_trans` SET `CORRECT_ANSWER`= '$answer[$i]',`MARKS_ANSWER`='$mark[$i]' WHERE `MCQ_TRAN_ID` = '$mcq_tr_id[$i]'");


            $update002 = mysqli_query($conn,"UPDATE `student_mcq_transaction` SET `CORRECT_ANSWER`= '$answer[$i]',`MARKS`='$mark[$i]' WHERE `PAPER_ID` = '$paper_id' AND `QUESTION` = '$question[$i]'");
          }

          $sql0031 = mysqli_query($conn,"SELECT * FROM `student_mcq_master` WHERE `PAPER_ID` = '$paper_id'"); 
          while($row0031 = mysqli_fetch_assoc($sql0031))
          {
            $total_marks001 = $row0031['TOTAL_MARKS'];
            $correct001 = $row0031['CORRECT_ANSWERS'];
            $wrong001 = $row0031['WRONG_ANSWER'];
            $stu_mcq_master_id = $row0031['STU_MCQ_MASTER_ID'];
            $stu_id = $row0031['STU_ID'];


            $new_correct_marks = 0;
            $new_wrong_marks = 0;

            $new_correct_count = 0;
            $new_wrong_count = 0;
            $total_marks = 0;


            $sql0032 = mysqli_query($conn,"SELECT * FROM `student_mcq_transaction` WHERE `PAPER_ID` = '$paper_id' AND `STU_ID` = '$stu_id'"); 
             while($row0032 = mysqli_fetch_assoc($sql0032))
             {
                $student_answer = $row0032['ANSWER'];
                $orginal_answer = $row0032['CORRECT_ANSWER'];
                $real_marks = $row0032['MARKS'];

                if($student_answer == $orginal_answer)
                {
                  $new_correct_marks = $new_correct_marks+$real_marks;
                  $new_correct_count = $new_correct_count+1;
                }else
                if($student_answer !== $orginal_answer)
                {
                  $new_wrong_count = $new_wrong_count+1;
                }

             }

            $update003 = mysqli_query($conn,"UPDATE `student_mcq_master` SET `CORRECT_ANSWERS`= '$new_correct_count',`WRONG_ANSWER`='$new_wrong_count',`TOTAL_MARKS`='$new_correct_marks' WHERE `PAPER_ID` = '$paper_id' AND `STU_ID` = '$stu_id'");

            $update004 = mysqli_query($conn,"UPDATE `exam_result` SET `MCQ_MARK`='$new_correct_marks' WHERE `PAPER_ID` = '$paper_id' AND `STU_ID` = '$stu_id'");

            $sql0033 = mysqli_query($conn,"SELECT * FROM `exam_result` WHERE `PAPER_ID` = '$paper_id' AND `STU_ID` = '$stu_id'"); 
             while($row0033 = mysqli_fetch_assoc($sql0033))
             {
                $total_exam_mcq_marks = $row0033['MCQ_MARK'];
                $total_exam_se_marks = $row0033['SE_MARK'];

                $total_marks = ($total_exam_mcq_marks/2)+($total_exam_se_marks/20);


             }

            $update004 = mysqli_query($conn,"UPDATE `exam_result` SET `TOTAL_MARK`='$total_marks' WHERE `PAPER_ID` = '$paper_id' AND `STU_ID` = '$stu_id'");


            }
          

          header('location:../../new_admin/edit_mcq_answer.php?paper_id='.$paper_id.'');


      }else
      if(isset($_POST['update_google_form']))
      {
        $paper_name = ucwords($_POST['paper_name']);
        $class_id = $_POST['class_id'];
        $form_link = $_POST['google_form_link'];
        $teacher_id = $_POST['teacher_id'];
        $hour = $_POST['hour'];
        $minute = $_POST['minute'];
        $start_time = $_POST['start_date'];
        $end_time = $_POST['end_date'];

        $page = $_POST['page'];

        $gf_master_id = $_POST['update_google_form'];

        $max_hour = $_POST['max_hour'];
        $max_minu = $_POST['max_minu'];

        $start_date = date('Y-m-d',strtotime($start_time));

        $end_date = date('Y-m-d',strtotime($end_time));

        $current_time = date('Y-m-d h:i:s A');
        $current_date = date('Y-m-d');

        $insert0001 = mysqli_query($conn,"UPDATE `g_f_master` SET `ADD_DT` = '$current_time',`PAPER_NAME`='$paper_name',`FORM_LINK`='$form_link',`START_DATE`='$start_date',`START_TIME`='$start_time',`END_DATE`='$end_date',`HOURS`='$max_hour',`MINUTE`='$max_minu',`END_TIME`='$end_time',`TEACH_ID`='$teacher_id' WHERE `GF_MASTER_ID`='$gf_master_id'"); //Insert Answers


        header('location:../../new_admin/google_form.php?page='.$page.'');

      }else
      if(isset($_POST['update_google_form_classes']))
      {
        $gf_master_id = $_POST['update_google_form_classes'];
        $page = $_POST['page'];
        $class_id = $_POST['class_id'];
        $teacher_id = $_POST['teacher_id'];

        $class_amount = sizeof($class_id);

        for($j=0;$j<$class_amount;$j++)
        {

          $sql = mysqli_query($conn,"SELECT * FROM `g_f_tranasaction` WHERE `CLASS_ID` = '$class_id[$j]' AND `GF_MASTER_ID` = '$gf_master_id'");

          if(mysqli_num_rows($sql) == '0')
          {

            $insert001 = mysqli_query($conn,"INSERT INTO `g_f_tranasaction`(`GF_MASTER_ID`, `CLASS_ID`) VALUES ('$gf_master_id','$class_id[$j]')");

          }else
          if(mysqli_num_rows($sql) > 0)
          {

              mysqli_query($conn,"DELETE FROM `g_f_tranasaction` WHERE `GF_MASTER_ID` = '$gf_master_id' AND `CLASS_ID` != '$class_id[$j]'");


          }

        }


        
        
        header('location:../../new_admin/google_form.php?page='.$page.'');

      }else
      if(isset($_POST['update_password']))
      {
        $page = $_POST['page'];

        $teach_id = $_POST['update_password'];
        $username = $_POST['username'];

        $reset_password = md5($_POST['reset_password']);

        mysqli_query($conn,"UPDATE `teacher_login` SET `PASSWORD`='$reset_password',`USERNAME`='$username' WHERE `TEACH_ID`='$teach_id'");

      
        header('location:../../new_admin/teacher_approved.php?page='.$page.'');


      }else
      if(isset($_POST['free_material_option']))
      {
        $msg = $_POST['free_material_option'];

        if($msg == '1')
        {
            mysqli_query($conn,"UPDATE `institute` SET `FREE_MODE_MATERIAL`= '1' WHERE `INS_ID`= '1'");

            mysqli_query($conn,"UPDATE `transactions` SET `LMS_STATUS` = '1' WHERE `FREE_CLASS` != '1'"); //Update Transactions table
            

        }else
        if($msg == '0')
        {
            mysqli_query($conn,"UPDATE `institute` SET `FREE_MODE_MATERIAL`= '0' WHERE `INS_ID`= '1'");

            mysqli_query($conn,"UPDATE `transactions` SET `LMS_STATUS` = '0' WHERE `FREE_CLASS` != '1'"); //Update Transactions table
        }

        echo $msg;
      }else
      if(isset($_POST['seminar_option_status']))
      {
        $msg = $_POST['seminar_option_status'];

        if($msg == '1')
        {
            mysqli_query($conn,"UPDATE `institute` SET `SEMINAR_BTN`= '1' WHERE `INS_ID`= '1'");

        }else
        if($msg == '0')
        {
            mysqli_query($conn,"UPDATE `institute` SET `SEMINAR_BTN`= '0' WHERE `INS_ID`= '1'");

        }

        echo $msg;
      }else
      if(isset($_POST['update_free_card']))
      {
        $page = $_POST['page'];
        $tra_id = $_POST['update_free_card'];

        $lms_active = $_POST['lms_active'];
        $start_date = $_POST['start_date'];
        $end_date = $_POST['end_date'];

        $class_id = $_POST['class_id'];
        $teach_id = $_POST['teach_id'];

        if(empty($start_date))
        {
          $start_date = '0';
        }

        if(empty($end_date))
        {
          $end_date = '0';
        }

        $current_time = date('Y-m-d h:i:s A');

        mysqli_query($conn,"UPDATE `transactions` SET `FREE_CLASS` = '1',`FREE_START_DATE`='$start_date',`FREE_END_DATE`='$end_date',`LMS_STATUS`='$lms_active',`UPDATE_TIME`='$current_time' WHERE `TRA_ID`='$tra_id'");


        if(!empty($_POST['stu_id']))
        {
          $stu_id = $_POST['stu_id'];
          header('location:../../new_admin/free_card.php?page='.$page.'&&teach_id='.$teach_id.'&&class_id='.$class_id.'&&stu_id='.$stu_id.'');
          
        }else
        if(empty($_POST['stu_id']))
        {
          header('location:../../new_admin/free_card.php?page='.$page.'&&teach_id='.$teach_id.'&&class_id='.$class_id.'');
        }


      }else
      if(isset($_POST['cancel_free_card']))
      {
        $page = $_POST['page'];
        $tra_id = $_POST['cancel_free_card'];

        $class_id = $_POST['class_id'];
        $teach_id = $_POST['teach_id'];

        mysqli_query($conn,"UPDATE `transactions` SET `FREE_CLASS` = '0' WHERE `TRA_ID`='$tra_id'");

        if(!empty($_POST['stu_id']))
        {
          $stu_id = $_POST['stu_id'];
          header('location:../../new_admin/free_card.php?page='.$page.'&&teach_id='.$teach_id.'&&class_id='.$class_id.'&&stu_id='.$stu_id.'');

        }else
        if(empty($_POST['stu_id']))
        {
          header('location:../../new_admin/free_card.php?page='.$page.'&&teach_id='.$teach_id.'&&class_id='.$class_id.'');
        }
        


      }else
      if(isset($_POST['free_material_option']))
      {
        $msg = $_POST['free_material_option'];

        if($msg == '1')
        {
            mysqli_query($conn,"UPDATE `institute` SET `FREE_MODE_MATERIAL`= '1' WHERE `INS_ID`= '1'");

            mysqli_query($conn,"UPDATE `transactions` SET `LMS_STATUS` = '1' WHERE FREE_CLASS != '1'"); //Update Transactions table
            

        }else
        if($msg == '0')
        {
            mysqli_query($conn,"UPDATE `institute` SET `FREE_MODE_MATERIAL`= '0' WHERE `INS_ID`= '1'");

            mysqli_query($conn,"UPDATE `transactions` SET `LMS_STATUS` = '0' WHERE FREE_CLASS != '1'"); //Update Transactions table
        }

        echo $msg;
      }else
      if(isset($_POST['update_mcq_answer']))
      {
        $paper_id = $_POST['update_mcq_answer'];
        $mcq_tr_id = $_POST['mcq_tr_id'];
        $answer = $_POST['answer'];
        $mark = $_POST['mark'];
        $question = $_POST['question_no'];


        $sql003 = mysqli_query($conn,"SELECT * FROM `paper_master` WHERE `PAPER_ID` = '$paper_id'"); 
       while($row003 = mysqli_fetch_assoc($sql003))
       {
          $total_question = $row003['NO_OF_QUESTIONS']; //Exam key
         
          $answer_mark = $row003['ANSWER_MARKS'];
       }

          $current_time = date('Y-m-d h:i:s A');
          $current_date = date('Y-m-d');

          for($i=0;$i<$total_question;$i++)
          {
            $update001 = mysqli_query($conn,"UPDATE `mcq_trans` SET `CORRECT_ANSWER`= '$answer[$i]',`MARKS_ANSWER`='$mark[$i]' WHERE `MCQ_TRAN_ID` = '$mcq_tr_id[$i]'");


            $update002 = mysqli_query($conn,"UPDATE `student_mcq_transaction` SET `CORRECT_ANSWER`= '$answer[$i]',`MARKS`='$mark[$i]' WHERE `PAPER_ID` = '$paper_id' AND `QUESTION` = '$question[$i]'");
          }

          $sql0031 = mysqli_query($conn,"SELECT * FROM `student_mcq_master` WHERE `PAPER_ID` = '$paper_id'"); 
          while($row0031 = mysqli_fetch_assoc($sql0031))
          {
            $total_marks001 = $row0031['TOTAL_MARKS'];
            $correct001 = $row0031['CORRECT_ANSWERS'];
            $wrong001 = $row0031['WRONG_ANSWER'];
            $stu_mcq_master_id = $row0031['STU_MCQ_MASTER_ID'];
            $stu_id = $row0031['STU_ID'];


            $new_correct_marks = 0;
            $new_wrong_marks = 0;

            $new_correct_count = 0;
            $new_wrong_count = 0;
            $total_marks = 0;


            $sql0032 = mysqli_query($conn,"SELECT * FROM `student_mcq_transaction` WHERE `PAPER_ID` = '$paper_id' AND `STU_ID` = '$stu_id'"); 
             while($row0032 = mysqli_fetch_assoc($sql0032))
             {
                $student_answer = $row0032['ANSWER'];
                $orginal_answer = $row0032['CORRECT_ANSWER'];
                $real_marks = $row0032['MARKS'];

                if($student_answer == $orginal_answer)
                {
                  $new_correct_marks = $new_correct_marks+$real_marks;
                  $new_correct_count = $new_correct_count+1;
                }else
                if($student_answer !== $orginal_answer)
                {
                  $new_wrong_count = $new_wrong_count+1;
                }

             }

            $update003 = mysqli_query($conn,"UPDATE `student_mcq_master` SET `CORRECT_ANSWERS`= '$new_correct_count',`WRONG_ANSWER`='$new_wrong_count',`TOTAL_MARKS`='$new_correct_marks' WHERE `PAPER_ID` = '$paper_id' AND `STU_ID` = '$stu_id'");

            $update004 = mysqli_query($conn,"UPDATE `exam_result` SET `MCQ_MARK`='$new_correct_marks' WHERE `PAPER_ID` = '$paper_id' AND `STU_ID` = '$stu_id'");

            $sql0033 = mysqli_query($conn,"SELECT * FROM `exam_result` WHERE `PAPER_ID` = '$paper_id' AND `STU_ID` = '$stu_id'"); 
             while($row0033 = mysqli_fetch_assoc($sql0033))
             {
                $total_exam_mcq_marks = $row0033['MCQ_MARK'];
                $total_exam_se_marks = $row0033['SE_MARK'];

                $total_marks = ($total_exam_mcq_marks/2)+($total_exam_se_marks/20);


             }

            $update004 = mysqli_query($conn,"UPDATE `exam_result` SET `TOTAL_MARK`='$total_marks' WHERE `PAPER_ID` = '$paper_id' AND `STU_ID` = '$stu_id'");


            }
          

          header('location:../../new_admin/edit_mcq_answer.php?paper_id='.$paper_id.'');


      }else
      if(isset($_POST['admin_otp_option']))
      {
        $msg = $_POST['admin_otp_option'];

        if($msg == '1')
        {
            mysqli_query($conn,"UPDATE `institute` SET `ADMIN_OTP`= '1' WHERE `INS_ID`= '1'");

        }else
        if($msg == '0')
        {
            mysqli_query($conn,"UPDATE `institute` SET `ADMIN_OTP`= '0' WHERE `INS_ID`= '1'");

        }

        echo $msg;
        
        $_SESSION['update_msg'] = '';
      }else
      if(isset($_POST['update_staff_worktime']))
      {

        $staff_id = $_POST['update_staff_worktime'];

        $sql001 = mysqli_query($conn,"SELECT * FROM `online_staff_status` WHERE `ADMIN_ID` = '$staff_id'");

        $check = mysqli_num_rows($sql001);

        $log_date = date('Y-m-d');
        $log_time = date('Y-m-d h:i:s A');


        $sql002701 = mysqli_query($conn,"SELECT * FROM `institute` WHERE `INS_ID` = '1'");
        while($row002701 = mysqli_fetch_assoc($sql002701))
        {
            $staff_timer = $row002701['STAFF_TIMER'];
        }
            
        $institute_setup_time = 0;
        $institute_setup_time = $staff_timer/(1000*60);

        $max_log_time = date('Y-m-d h:i:s A',strtotime('+'.$institute_setup_time.' minute'));

        if($check > 0)
        {
            mysqli_query($conn,"UPDATE `online_staff_status` SET `LAST_LOG_DATE` = '$log_date', `LAST_LOG_TIME` = '$log_time',`ONLINE` = '1',`MAX_LOG_TIME` = '$max_log_time' WHERE `ADMIN_ID` = '$staff_id'");
        }else
        if($check == '0')
        {
            mysqli_query($conn,"INSERT INTO `online_staff_status`(`LAST_LOG_DATE`, `LAST_LOG_TIME`, `ADMIN_ID`,`ONLINE`,`MAX_LOG_TIME`) VALUES ('$log_date','$log_time','$staff_id','1','$max_log_time')");
        }

        echo "OK";

        $_SESSION['update_msg'] = '';

      }else
      if(isset($_POST['update_offline']))
      {

        $staff_id = $_POST['update_offline'];

        mysqli_query($conn,"UPDATE `online_staff_status` SET `ONLINE` = '0' WHERE `ADMIN_ID` = '$staff_id'");
        

        echo "OK";

        $_SESSION['update_msg'] = '';

      }else
      if(isset($_POST['check_staff_active_status']))
      {

        $current_time = date('Y-m-d h:i:s A');

        mysqli_query($conn,"UPDATE `online_staff_status` SET `ONLINE` = '0' WHERE `MAX_LOG_TIME` < '$current_time'");
        

        echo "OK";

        $_SESSION['update_msg'] = '';

      }else
      if(isset($_POST['active_safe_link']))
      {
        $msg = $_POST['active_safe_link'];

        if($msg == '1')
        {
            mysqli_query($conn,"UPDATE `institute` SET `SAFETY_LINK`= '1' WHERE `INS_ID`= '1'");

        }else
        if($msg == '0')
        {
            mysqli_query($conn,"UPDATE `institute` SET `SAFETY_LINK`= '0' WHERE `INS_ID`= '1'");

        }

        echo $msg;

        $_SESSION['update_msg'] = '';


      }else
      if(isset($_POST['check_safe_link']))
      {
        $class_id = $_POST['check_safe_link'];
        $status = $_POST['status'];

        if($status == '1')
        {
            mysqli_query($conn,"UPDATE `classes` SET `ACTIVE_SAFE_LINK`= '1' WHERE `CLASS_ID`= '$class_id'");

        }else
        if($status == '0')
        {
            mysqli_query($conn,"UPDATE `classes` SET `ACTIVE_SAFE_LINK`= '0' WHERE `CLASS_ID`= '$class_id'");

        }

        echo $status;

        $_SESSION['update_msg'] = '';


      }else
      if(isset($_POST['check_safe_link']))
      {
        $class_id = $_POST['check_safe_link'];
        $status = $_POST['status'];

        if($status == '1')
        {
            mysqli_query($conn,"UPDATE `classes` SET `ACTIVE_SAFE_LINK`= '1' WHERE `CLASS_ID`= '$class_id'");

        }else
        if($status == '0')
        {
            mysqli_query($conn,"UPDATE `classes` SET `ACTIVE_SAFE_LINK`= '0' WHERE `CLASS_ID`= '$class_id'");

        }

        echo $status;

        $_SESSION['update_msg'] = '';


      }else
      if(isset($_GET['approve_my_stu'])) //APPROVED variable OK**
      {
          $stu_id = $_GET['approve_my_stu'];

          $update = mysqli_query($conn,"UPDATE `stu_login` SET `STATUS`= 'Active' WHERE `STU_ID`= '$stu_id'");

          header('location:../../new_admin/student_pending.php');

      }else
      if(isset($_POST['submit_paper']))
      {
        $student_id = $_POST['submit_paper'];
        $paper_id = $_POST['paper_id'];
        
        $add_date = date('Y-m-d');
        $add_time = date('Y-m-d h:i:s A');
  
        $q_tot = 0;
        
  
        $uploadDir = '../../paper_marker/';
        $allowTypes = array('jpg','png','jpeg');
  
        if(!empty(array_filter($_FILES['upload_file']['name']))){
            foreach($_FILES['upload_file']['name'] as $key=>$val){
                $filename0 = basename($_FILES['upload_file']['name'][$key]);
  
                $ext = pathinfo($filename0, PATHINFO_EXTENSION); //GET EXTENTION ON IMAGE
                $rand = hexdec(uniqid()); //NAME CONVERT TO ENCRYPT
                $filename = $rand.".".$ext; //ADD IMAGE NAME TO ENCRYPTED NAME
                  
                $targetFile = $uploadDir.$filename;
                if(move_uploaded_file($_FILES["upload_file"]["tmp_name"][$key], $targetFile)){
                    $success[] = "Uploaded $filename";
                    $insertQrySplit[] = "('$filename')";
                }
                else {
                    $errors[] = "Something went wrong- File - $filename";
                }
                    
                
  
  
                $update001 = mysqli_query($conn,"INSERT INTO `paper_mark`(`ADD_DATE`, `ADD_TIME`, `FILE_NAME`, `PAPER_ID`, `STU_ID`) VALUES ('$add_date','$add_time','$filename','$paper_id','$student_id')");
            }
  
           
        }
        else {
            $errors[] = "No File Selected";
        }
  
  
  
        ?>  <!DOCTYPE html>
            <html>
            <head>
                <title>Success</title>
                <meta charset="utf-8">
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <link href="https://fonts.googleapis.com/css2?family=Ubuntu&display=swap" rel="stylesheet">
                <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
            </head>
            <body style="font-family: 'Ubuntu', sans-serif;">
                    <script type="text/javascript">
  
  
                        Swal.fire({
                              position: 'top-middle',
                              type: 'success',
                              icon: 'success',
                              title: 'Submit Successfully!',
                              showConfirmButton: false,
                              timer: 1000
                              })
  
        
                    </script>
            </body>
            </html>
                
                
            <?php
  
            header('refresh:0.2;url=../../new_admin/paper_annotation.php?paper_id='.$paper_id.'&&student_id='.$student_id.'');
      }else
      if(isset($_POST['show_teacher_list'])) //APPROVED variable OK**
      {
          $order = $_POST['list_order'];
          $level_id = $_POST['level_name'];
          $teach_id = $_POST['teacher_id'];

          $size = sizeof($order);

          for($i=0;$i<$size;$i++)
          {
            $update = mysqli_query($conn,"UPDATE `teacher_details` SET `LEVEL_ID`= '$level_id[$i]',`TEACHER_ORDER` = '$order[$i]' WHERE `TEACH_ID`= '$teach_id[$i]'");
          }

          

          header('location:../../new_admin/show_teacher_login.php');

      }else
      if(isset($_POST['update_slider']))
      {
        $slid_id = $_POST['update_slider'];
  
        $recent_img = $_POST['recent_img'];
  
        echo $order = $_POST['myOrder'];
  
        $file = $_FILES['img']['name'];
  
        if(!empty($_FILES['img']['name'][$slid_id]))
        {
            unlink('../../student/slider_images/'.$recent_img[$slid_id]);  
          
            $tmp_file = $_FILES['img']['tmp_name'][$slid_id]; //ASSIGN DATA TO VARIABLE
            $ext = pathinfo($_FILES["img"]["name"][$slid_id], PATHINFO_EXTENSION); //GET EXTENTION ON IMAGE
  
            $rand = hexdec(uniqid()); //NAME CONVERT TO ENCRYPT
            $post_file = $rand.".".$ext; //ADD IMAGE NAME TO ENCRYPTED NAME
  
  
            move_uploaded_file($tmp_file,'../../student/slider_images/'.$post_file); //SAVE LOCATION
            
            $update001 = mysqli_query($conn,"UPDATE `slide` SET `IMG_NAME`='$post_file',`ORDER` = '$order[$slid_id]' WHERE `SLID_ID` = '$slid_id'");
  
        }else
        {
          $update001 = mysqli_query($conn,"UPDATE `slide` SET `ORDER` = '$order[$slid_id]' WHERE `SLID_ID` = '$slid_id'");
  
        }
            
      header('location:../../new_admin/slider.php');
      
      
    }else
    if(isset($_POST['edit_post']))
    {
      
      $creator = $_POST['user_id'];
      $title = mysqli_real_escape_string($conn,$_POST['title']);
      $desc = mysqli_real_escape_string($conn,$_POST['desc']);
      $view_post = $_POST['view_post'];
      $post_id = $_POST['edit_post'];
      $last_image = $_POST['last_img'];
      $file = $_FILES['img']['name'];

      if(!empty($_FILES['img']['name']))
      {
        if($img !== 'no_img.jpg')
				{
					unlink('../../student/blog_post/'.$last_image);
				}
        
        $tmp_file = $_FILES['img']['tmp_name']; //ASSIGN DATA TO VARIABLE
        $ext = pathinfo($_FILES["img"]["name"], PATHINFO_EXTENSION); //GET EXTENTION ON IMAGE
        $rand = hexdec(uniqid()); //NAME CONVERT TO ENCRYPT
        $post_file = $rand.".".$ext; //ADD IMAGE NAME TO ENCRYPTED NAME
        move_uploaded_file($tmp_file,"../../student/blog_post/".$post_file); //SAVE LOCATION
      }else
      if(empty($_FILES['img']['name']))
      {
        $post_file = '0';
      
      }

      $add_date = date('Y-m-d');
      $add_time = date('Y-m-d h:i:s A');
      
      $update1 = mysqli_query($conn,"UPDATE `post` SET `TITLE`= '$title',`DESCRIPTION`= '$desc',`POST_VIEW`= '$view_post' WHERE `POST_ID`= '$post_id'");

      if(!empty($_FILES['img']['name']))
      {
        $sql1113 = mysqli_query($conn,"SELECT * FROM `post_img` WHERE `POST_ID` = '$post_id'");
        $check = mysqli_num_rows($sql1113);

        if($check == '0')
        {
          $insert2 = mysqli_query($conn,"INSERT INTO `post_img`(`IMG`, `POST_ID`) VALUES ('$post_file','$post_id')");

        }else
        if($check > 0)
        {
          $update2 = mysqli_query($conn,"UPDATE `post_img` SET `IMG`= '$post_file' WHERE `POST_ID`= '$post_id'");
        }

        
      
      }
      
    
      header('location:../../new_admin/blog_post.php');
    
  }else
  if(isset($_POST['show_telegram_btn']))
  {
    $status = $_POST['show_telegram_btn'];

    if($status == '1')
    {
        mysqli_query($conn,"UPDATE `institute` SET `SHOW_TG`= '1' WHERE `INS_ID`= '1'");

    }else
    if($status == '0')
    {
        mysqli_query($conn,"UPDATE `institute` SET `SHOW_TG`= '0' WHERE `INS_ID`= '1'");

    }

    echo $status;

    $_SESSION['update_msg'] = '';


  }

   ?>