

<?php
  $page = "Teacher Bank Details";
  $folder_in = '3';

  include('header/header.php');

  $s04 = mysqli_query($conn,"SELECT * FROM admin_login WHERE ADMIN_ID = '$admin_id'");
  while($row4 = mysqli_fetch_assoc($s04))
  {
    $name = $row4['ADMIN_NAME'];
  }


  $show_button = 'show';
  if($position == 'STAFF')
  {
    $show_button = 'none';
  }

 ?>

    <ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-bottom: 04%;margin-bottom: 0;">
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="Todo List"> Teacher Bank Details</li>
    </ol>


      <div class="col-md-12 bg-white" style="border:1px solid #cccc;height: auto;">
      
      <div class="col-md-12" style="border-bottom: 1px solid #cccc;">
        <div class="row">

            <div class="col-md-9" style="margin-bottom: 10px;">

              <h2><a href="dashboard.php" data-toggle="tooltip" data-title="Back"><i class="pg-icon" style="font-size: 22px;">chevron_left</i></a> Teacher Bank Details</h2>

            </div>

        </div>
      </div>

          
          <div class=" container-fluid container-fixed-lg bg-white" style="margin-top: 0px;">
            <div class="card card-transparent">
              <div class="card-header ">
                  <div class="col-md-12">
                      <div class="row">
                        <div class="col-md-4">

                          <a href="create_bank_data.php" class="btn btn-info btn-lg" style="display: <?php echo $show_button; ?>"><span class="fa fa-plus"></span>&nbsp;Create Bank Details</a>

                          <a href="create_bank.php" class="btn btn-danger btn-lg" style="display: <?php echo $show_button; ?>"><span class="fa fa-plus"></span>&nbsp;Create Bank</a>

                        </div>
                        <div class="col-md-8"> </div>
                      </div>
                    </div>
                <div class="card-body">
                  <!--  style="height: 820px;overflow: auto;" -->
                <div class="table-responsive">
                <table class="table table-hover" id="tblFruits">
                  <thead>
                      <tr>
                        <th style="width: 1%;">Add Time</th>
                        <th style="width: 1%;">Teacher Name</th>
                        <th style="width: 1%;">Account No</th>
                        <th style="width: 1%;">Account Name</th>
                        <th style="width: 1%;">Branch Name</th>
                        <th style="width: 1%;">Bank Name</th>
                        <th style="width: 1%;text-align: center;">Action</th>
                      </tr>
                    </thead>
                    <tbody>

                      <?php 
                          /*------------------ Header Pagination --------------------------------*/
                        $record_per_page = 10;

                        $nxt_five_loop = 0;

                        $page = '';

                        if(isset($_GET["page"]))
                        {
                          $page = $_GET["page"];
                        }
                        else
                        {
                         $page = 1;
                        }

                        $start_from = ($page-1)*$record_per_page; //Current page no -1 X Per Page records

                       
                          $sql001 = "SELECT * FROM `class_teacher_bank_data`  order by `C_T_B_D_ID` DESC LIMIT $start_from, $record_per_page";

                        
                          $page_query = "SELECT * FROM `class_teacher_bank_data` ORDER BY `C_T_B_D_ID` DESC";

                        /*------------------ Header Pagination --------------------------------*/

                        $j = 0;
                        
                        //LIMIT = 10-1 x 10 = 90 , 10";
                        $result = mysqli_query($conn, $sql001);
                        $page_result = mysqli_query($conn, $page_query);

                          $check = mysqli_num_rows($result);

                          if($check>0)
                          {
                            while($row001 = mysqli_fetch_assoc($result))
                            {
                              $account_name = $row001['ACCOUNT_NAME'];
                              $account_no = $row001['ACCOUNT_NO'];
                              $branch = $row001['BRANCH'];

                              $bank_id = $row001['BANK_ID'];
                              $teach_id = $row001['TEACH_ID'];
                              $add_time = $row001['ADD_TIME'];

                              $ctbd_id = $row001['C_T_B_D_ID'];

                              $sql002 = mysqli_query($conn,"SELECT * FROM `teacher_details` WHERE `TEACH_ID` = '$teach_id'");
                              while($row002 = mysqli_fetch_assoc($sql002))
                              {
                                $teacher_name = $row002['POSITION'].". ". $row002['F_NAME']." ".$row002['L_NAME'];
                                
                              }

                              $sql001 = mysqli_query($conn,"SELECT * FROM `bank` WHERE `BANK_ID` = '$bank_id'");
                              while($row001 = mysqli_fetch_assoc($sql001))
                              {
                                $bank_name = $row001['BANK_NAME'];
                              }


                            echo '

                              <tr>

                                <td class="v-align-left">'.$add_time.'</td>
                                <td class="v-align-left">'.$teacher_name.'</td>
                                <td class="v-align-left">'.$account_no.'</td>
                                <td class="v-align-left">'.$account_name.'</td>
                                <td class="v-align-left">'.$branch.'</td>
                                <td class="v-align-left">'.$bank_name .'</td>
                                 <td class="v-align-middle text-center">

                                <a href="create_bank_data.php?ctbd_id='.$ctbd_id.'&&page='.$page.'" class="btn btn-success mt-1 mr-1 btn-xs btn-rounded" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;display:'.$show_button.'" data-title="Delete" data-toggle="tooltip"><i class="pg-icon">edit</i></a>
                                ' ?>
                                <a href="../admin/query/delete.php?delete_ctbd_id=<?php echo $ctbd_id; ?>&&page=<?php echo $page; ?>" onclick="return confirm('Are you sure delete?')" class="btn btn-danger mt-1 mr-1 btn-xs btn-rounded" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;display: <?php echo $show_button; ?>;" data-title="Delete" data-toggle="tooltip"><i class="pg-icon">trash_alt</i></a>

                                <?php echo '

                                </td>
                              </tr>

                            ';
                        
                          }
                        }else
                        if($check == '0')
                        {
                          echo '<tr><td colspan="7" class="text-center text-danger"><span class="fa fa-warning"></span> Not Found Data</td></tr>';
                        } 
                      

                        

                        
                         ?>


                    </tbody>
                </table>
                </div>
              </div>

                <div class="col-md-12" style="width: 100%;overflow: auto;">
                  <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-8" class="php_pagination">
                      
                      <div class="col-md-12">
                    <br />
                    <?php
                    $total_records = mysqli_num_rows($page_result);
                    $total_pages = ceil($total_records/$record_per_page);
                    $start_loop = $page;
                    $difference = $total_pages - $page;
                    if($difference <= 5)
                    {
                     $start_loop = $total_pages - 5;
                    }else
                    if($difference <= 0)
                    {
                      $start_loop = '0';
                    }

                    echo '<ul class="pagination">';
                    $end_loop = $start_loop + 4;
                    $five_loop = $page-5;
                    if($five_loop <= 0)
                    {
                      $five_loop = 1;
                    }
                    if($page > 1)
                    {

                      echo '<li><a href="class_teacher_bank_data.php?page=1">First</a></li>';
                      echo '<li><a href="class_teacher_bank_data.php?page='.($page - 1).'"> < </a></li>';
                      echo '<li><a href="class_teacher_bank_data.php?page='.$five_loop.'"> <<< </a></li>';

                    }
                    if($start_loop !== '1')
                    {
                      $few_no = $start_loop - 1;
                    }else
                    if($start_loop > 0)
                    {
                      $few_no = 1;
                    }
                    
                    for($i=$few_no; $i<=$end_loop+1; $i++)
                    {     
                      if($i == $page)
                      {
                        echo '<li class="active"><a href="class_teacher_bank_data.php?page='.$i.'" style="font-weight:bold;background-color:#2980b9;color:white;">'.$i.'</a></li>';
                      }else
                      {
                        if($i == '0')
                        {
                          continue;
                        }else
                        if($i>0)
                        {
                          echo '<li><a href="class_teacher_bank_data.php?page='.$i.'" >'.$i.'</a></li>';
                        }
                        
                      }

                    }
                    if($page <= $end_loop)
                    {
                      
                      if($page == '1')
                      {
                        $nxt_five_loop = 5;
                      }else
                      if($page >= 5)
                      {
                        $nxt_five_loop = $page+5;

                        if($total_pages < $nxt_five_loop)
                        {
                          $nxt_five_loop = $total_pages;
                        }
                      }else
                      {
                        $nxt_five_loop = $page+5;
                      }

                        echo '<li><a href="class_teacher_bank_data.php?page='.($page + 1).'" > > </a></li>';
                        echo '<li><a href="class_teacher_bank_data.php?page='.$nxt_five_loop.'" > >>> </a></li>';
                        echo '<li><a href="class_teacher_bank_data.php?page='.$total_pages.'" >Last</a></li>';
                      
                    }
                    
                    echo '</ul>';
                    
                    ?>
                    </div>

                    </div>
                  </div>
                    
                </div>
            </div>
            </div>
            </div>
            </div>
            </div>
            </div>
            </div>



<?php include('footer/footer.php'); ?>

<script type="text/javascript">
  function sortTable(table, order) {
    var asc   = order === 'asc',
        tbody = table.find('tbody');
    
    tbody.find('tr').sort(function(a, b) {
        if (asc) {
            return $('td:first', a).text().localeCompare($('td:first', b).text());
        } else {
            return $('td:first', b).text().localeCompare($('td:first', a).text());
        }
    }).appendTo(tbody);
}
</script>

<script type="text/javascript">
    function GetSelected() {
        //Create an Array.
        var selected = new Array();
 
        //Reference the Table.
        var tblFruits = document.getElementById("tblFruits");
 
        //Reference all the CheckBoxes in Table.
        var chks = tblFruits.getElementsByTagName("INPUT");
 
        // Loop and push the checked CheckBox value in Array.
        for (var i = 0; i < chks.length; i++) {
            if (chks[i].checked) 
            {
                var s = selected.push(chks[i].value);

            }
        }
 
        //Display the selected CheckBox values.
        if (selected.length > 0) {
          if(confirm('Are you sure delete?'))
          {

              $.ajax({
               type: "POST",
               data: {delete_multi_todo:selected},
               url: "../admin/query/delete.php",
               success: function(msg){
                 //alert(msg)
                 location.reload();
               }
            
              });
          }
        }else{
         
         alert("Please select todo list!")
        
        }
    };
</script>