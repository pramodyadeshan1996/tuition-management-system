

<?php
  $page = "Bank Details";
  $folder_in = '3';

      include('header/header.php');

        $s04 = mysqli_query($conn,"SELECT * FROM admin_login WHERE ADMIN_ID = '$admin_id'");
        while($row4 = mysqli_fetch_assoc($s04))
        {
          $name = $row4['ADMIN_NAME'];
        }

        if(!empty($_GET['last_pagination']))
        {
          $last_pagination = $_GET['last_pagination'];
        }
  

  $teacher_id = $_GET['teacher_id'];

  $sql0021 = mysqli_query($conn,"SELECT * FROM teacher_details WHERE TEACH_ID = '$teacher_id'");
  while($row0021 = mysqli_fetch_assoc($sql0021))
  {
    $teach_name = $row0021['POSITION'].". ".$row0021['F_NAME']." ".$row0021['L_NAME'];
  }

 ?>

    <ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-bottom: 04%;margin-bottom: 0;">
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="teacher_approved.php?page=<?php echo $last_pagination; ?>">Teacher Approved</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="Bank Details"> Bank Details</li>
    </ol>


      <div class="col-md-12 bg-white" style="border:1px solid #cccc;height: auto;">
      
      <div class="col-md-12" style="border-bottom: 1px solid #cccc;">
        <div class="row">

            <div class="col-md-9" style="margin-bottom: 10px;"><h2><a href="teacher_approved.php?page=<?php echo $last_pagination; ?>" data-toggle="tooltip" data-title="Back"><i class="pg-icon" style="font-size: 22px;">chevron_left</i></a> Bank Details <small style="font-size: 16px;">(<?php echo $teach_name; ?>)</small>
            </h2></div>

        </div>
      </div>

          
          <div class=" container-fluid container-fixed-lg bg-white" style="margin-top: 0px;">
            <div class="card card-transparent">
              <div class="card-header ">

                  <form action="bank_details.php?page=<?php echo $page; ?>&&teacher_id=<?php echo $teacher_id; ?>" method="POST">
                      <div class="row">
                        <div class="col-md-2">

                          <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#add_todo"><span class="fa fa-plus"></span>&nbsp;Add Bank Details</button>

                        </div>
                        <div class="col-md-5"> </div>
                      <div class="col-md-3 mt-2">
                        <!-- <input list="student_search" id="search-table" name="bank_data1" class="form-control" placeholder="Search Bank Details.." autofocus>

                        <datalist id="student_search">
                          <?php 
                          /*
                            $sql002 = mysqli_query($conn,"SELECT * FROM `teacher_bank_details`");
                            while($row002 = mysqli_fetch_assoc($sql002))
                            {
                              $bank_name = $row002['BANK_NAME'];
                              $br_name = $row002['BRANCH_NAME'];
                              $b_ac_no = $row002['ACCOUNT_NO'];
                              $tbd_id1 = $row002['T_B_D_ID'];
                              
                                echo '<option value="'.$tbd_id1.'">'.$bank_name.' | '.$br_name.' | '.$b_ac_no.'</option>';

                              
                            }*/

                           ?>
                          
                        </datalist> -->
                      </div>
                      <div class="col-md-1 mt-2"> <!-- <button type="submit" class="btn btn-success btn-lg btn-block" name="search_student_btn" style="border-radius: 20px;"><i class="fa fa-search"></i> &nbsp; Search</button> --></div>

                      <div class="col-md-1 mt-2"> <!-- <button type="submit" class="btn btn-danger btn-lg btn-block" name="reset_student_btn" style="border-radius: 20px;display: none;"><i class="fa fa-refresh"></i> &nbsp; Reset</button> --></div>
                    </div>
                  </form>



                    <div class="clearfix"></div>
                </div>
                <div class="card-body">
                  <!--  style="height: 820px;overflow: auto;" -->
                <div class="table-responsive">
                <table class="table table-hover" id="tblFruits">
                  <thead>
                      <tr>
                        <th class="text-center" style="width: 1%;">#</th>
                        <th style="width: 1%;">Bank Name</th>
                        <th style="width: 1%;">Brance Name</th>
                        <th style="width: 1%;">Holder Name</th>
                        <th style="width: 1%;">Account No</th>
                        <th style="width: 1%;text-align: center;">Action</th>
                      </tr>
                    </thead>
                    <tbody>

                      <?php 
                          /*------------------ Header Pagination --------------------------------*/
                        $record_per_page = 10;

                        $nxt_five_loop = 0;

                        if(isset($_GET["page"]))
                        {
                          $page = $_GET["page"];
                        }
                        else
                        {
                         $page = 1;
                        }

                        $start_from = ($page-1)*$record_per_page; //Current page no -1 X Per Page records

                        /*------------------ Header Pagination --------------------------------*/

                        $j = 0;
                        
                        //LIMIT = 10-1 x 10 = 90 , 10";
                        $result22 = mysqli_query($conn, "SELECT * FROM `teacher_bank_details` WHERE `TEACH_ID` = '$teacher_id' LIMIT $start_from, $record_per_page");
                        $page_result = mysqli_query($conn,"SELECT * FROM `teacher_bank_details` WHERE `TEACH_ID` = '$teacher_id'");

                         $check = mysqli_num_rows($result22);

                          if($check>0)
                          {
                          while($row001 = mysqli_fetch_assoc($result22))
                          {
                            $holder = $row001['HOLDER_NAME'];
                            $bank_name = $row001['BANK_NAME'];
                            $ac_no = $row001['ACCOUNT_NO'];

                            $branch = $row001['BRANCH_NAME'];
                            $tbd_id = $row001['T_B_D_ID'];

                            $j++;

                            echo '

                              <tr>
                                <td class="v-align-middle text-center">'.$j.'</td>
                                <td class="v-align-middle">'.$bank_name.'</td>
                                <td class="v-align-middle">'.$branch.'</td>
                                <td class="v-align-middle">'.$holder.'</td>
                                <td class="v-align-middle">'.$ac_no.'</td>
                                 <td class="v-align-middle text-center">

                                <button data-toggle="modal" data-target="#edit_todo'.$tbd_id.'" class="btn btn-success btn-xs btn-rounded mt-1 mr-1" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;" data-title="Approve" data-toggle="tooltip" id="edit1'.$tbd_id.'"><i class="pg-icon">edit</i></button>
                                ' ?>
                                <a href="../admin/query/delete.php?delete_bd=<?php echo $tbd_id; ?>&&page=<?php echo $page; ?>&&teach_id=<?php echo $teacher_id; ?>&&last_pagination=<?php echo $last_pagination; ?>" onclick="return confirm('Are you sure delete?')" class="btn btn-danger mt-1 mr-1 btn-xs btn-rounded" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;" data-title="Delete" data-toggle="tooltip"><i class="pg-icon">trash_alt</i></a>

                                <div id="edit_todo<?php echo $tbd_id; ?>" class="modal fade" role="dialog">
                                  <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Update Bank Details</h4>
                                      </div>
                                      <form action="../admin/query/update.php" method="POST">
                                        <div class="modal-body mt-2 text-left">


                                          <label class="mt-2" style="text-align:left;">Account Holder Name</label>
                                          <textarea class="form-control" placeholder="Enter Account Holder Name" name="holder_name" rows="2" style="resize:none" required><?php echo $holder; ?></textarea>


                                          <label class="mt-2" style="text-align:left;">Account Number</label>
                                          <input type="text" name="ac_no" class="form-control" placeholder="Enter Account No.." required value="<?php echo $ac_no; ?>">

                                          <label class="mt-2" style="text-align:left;">Bank Name</label>
                                          <input list="bank_data" id="search-table" name="bank_name" class="form-control" placeholder="Type Bank Name.." value="<?php echo $bank_name; ?>">

                                          <datalist id="bank_data">
                                            <?php 

                                              $sql002 = mysqli_query($conn,"SELECT * FROM `teacher_bank_details`");
                                              while($row002 = mysqli_fetch_assoc($sql002))
                                              {
                                                $bank_name = $row002['BANK_NAME'];
                                                
                                                  echo '<option value="'.$bank_name.'">'.$bank_name.'</option>';

                                                
                                              }

                                             ?>
                                            
                                          </datalist>


                                          <label class="mt-2" style="text-align:left;">Branch Name</label>
                                          <input list="branch_data" id="search-table" name="branch" class="form-control" placeholder="Type Branch Name.."  value="<?php echo $branch; ?>">

                                          <datalist id="branch_data">
                                            <?php 

                                              $sql002 = mysqli_query($conn,"SELECT * FROM `teacher_bank_details`");
                                              while($row002 = mysqli_fetch_assoc($sql002))
                                              {
                                                $branch_name = $row002['BRANCH_NAME'];
                                                
                                                  echo '<option value="'.$branch_name.'">'.$branch_name.'</option>';

                                                
                                              }

                                             ?>
                                            
                                          </datalist>

                                          <input type="hidden" name="teacher_id" value="<?php echo $teacher_id; ?>">
                                          <input type="hidden" name="tbd_id" value="<?php echo $tbd_id; ?>">
                                          <input type="hidden" name="last_pagination" value="<?php echo $last_pagination; ?>">

                                        </div>
                                        <div class="modal-footer">
                                          <button type="submit" class="btn btn-success" name="update_bank_data" value="<?php echo $page; ?>"><span class="fa fa-check-circle"></span>&nbsp;Submit</button>
                                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                        </form>
                                    </div>

                                  </div>
                                </div>

                                <?php echo '

                                </td>
                              </tr>

                            ';
                        
                          }
                        }else
                        if($check == '0')
                        {
                          echo '<tr><td colspan="7" class="text-center text-danger"><span class="fa fa-warning"></span> Not Found Data</td></tr>';
                        } 
                      

                        

                        
                         ?>


                    </tbody>
                </table>
                </div>
              </div>

                <div class="col-md-12" style="width: 100%;overflow: auto;">
                  <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-8" class="php_pagination">
                      
                      <div class="col-md-12">
                    <br />
                    <?php
                    $total_records = mysqli_num_rows($page_result);
                    $total_pages = ceil($total_records/$record_per_page);
                    $start_loop = $page;
                    $difference = $total_pages - $page;
                    if($difference <= 5)
                    {
                     $start_loop = $total_pages - 5;
                    }else
                    if($difference <= 0)
                    {
                      $start_loop = '0';
                    }

                    echo '<ul class="pagination">';
                    $end_loop = $start_loop + 4;
                    $five_loop = $page-5;
                    if($five_loop <= 0)
                    {
                      $five_loop = 1;
                    }
                    if($page > 1)
                    {

                      echo '<li><a href="bank_details.php?page=1">First</a></li>';
                      echo '<li><a href="bank_details.php?page='.($page - 1).'"> < </a></li>';
                      echo '<li><a href="bank_details.php?page='.$five_loop.'"> <<< </a></li>';

                    }
                    if($start_loop !== '1')
                    {
                      $few_no = $start_loop - 1;
                    }else
                    if($start_loop > 0)
                    {
                      $few_no = 1;
                    }
                    
                    for($i=$few_no; $i<=$end_loop+1; $i++)
                    {     
                      if($i == $page)
                      {
                        echo '<li class="active"><a href="bank_details.php?page='.$i.'" style="font-weight:bold;background-color:#2980b9;color:white;">'.$i.'</a></li>';
                      }else
                      {
                        if($i == '0')
                        {
                          continue;
                        }else
                        if($i>0)
                        {
                          echo '<li><a href="bank_details.php?page='.$i.'" >'.$i.'</a></li>';
                        }
                        
                      }

                    }
                    if($page <= $end_loop)
                    {
                      
                      if($page == '1')
                      {
                        $nxt_five_loop = 5;
                      }else
                      if($page >= 5)
                      {
                        $nxt_five_loop = $page+5;

                        if($total_pages < $nxt_five_loop)
                        {
                          $nxt_five_loop = $total_pages;
                        }
                      }else
                      {
                        $nxt_five_loop = $page+5;
                      }

                        echo '<li><a href="bank_details.php?page='.($page + 1).'" > > </a></li>';
                        echo '<li><a href="bank_details.php?page='.$nxt_five_loop.'" > >>> </a></li>';
                        echo '<li><a href="bank_details.php?page='.$total_pages.'" >Last</a></li>';
                      
                    }
                    
                    echo '</ul>';
                    
                    ?>
                    </div>

                    </div>
                  </div>
                    
                </div>
            </div>
            </div>
            </div>
            </div>
            </div>
            </div>



<?php include('footer/footer.php'); ?>

<script type="text/javascript">
  function sortTable(table, order) {
    var asc   = order === 'asc',
        tbody = table.find('tbody');
    
    tbody.find('tr').sort(function(a, b) {
        if (asc) {
            return $('td:first', a).text().localeCompare($('td:first', b).text());
        } else {
            return $('td:first', b).text().localeCompare($('td:first', a).text());
        }
    }).appendTo(tbody);
}
</script>


<!-- Modal -->
                          <div id="add_todo" class="modal fade" role="dialog">
                            <div class="modal-dialog">

                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title">Create Bank Details</h4>
                                </div>
                                <form action="../admin/query/insert.php" method="POST">
                                <div class="modal-body mt-2">


                                  <label class="mt-2">Account Holder Name</label>
                                  <textarea class="form-control" placeholder="Enter Account Holder Name" name="holder_name" rows="2" style="resize:none" required></textarea>


                                  <label class="mt-2">Account Number</label>
                                  <input type="text" name="ac_no" class="form-control" placeholder="Enter Account No.." required>

                                  <label class="mt-2">Bank Name</label>
                                  <input list="bank_data" id="search-table" name="bank_name" class="form-control" placeholder="Type Bank Name.." >

                                  <datalist id="bank_data">
                                    <?php 

                                      $sql002 = mysqli_query($conn,"SELECT * FROM `teacher_bank_details`");
                                      while($row002 = mysqli_fetch_assoc($sql002))
                                      {
                                        $bank_name = $row002['BANK_NAME'];
                                        
                                          echo '<option value="'.$bank_name.'">'.$bank_name.'</option>';

                                        
                                      }

                                     ?>
                                    
                                  </datalist>


                                  <label class="mt-2">Branch Name</label>
                                  <input list="branch_data" id="search-table" name="branch" class="form-control" placeholder="Type Branch Name.." >

                                  <datalist id="branch_data">
                                    <?php 

                                      $sql002 = mysqli_query($conn,"SELECT * FROM `teacher_bank_details`");
                                      while($row002 = mysqli_fetch_assoc($sql002))
                                      {
                                        $branch_name = $row002['BRANCH_NAME'];
                                        
                                          echo '<option value="'.$branch_name.'">'.$branch_name.'</option>';

                                        
                                      }

                                     ?>
                                    
                                  </datalist>

                                  <input type="hidden" name="teacher_id" value="<?php echo $_GET['teacher_id']; ?>">
                                  <input type="hidden" name="last_pagination" value="<?php echo $last_pagination; ?>">

                                </div>
                                <div class="modal-footer">
                                  <button type="submit" class="btn btn-success" name="create_bank_data" value="<?php echo $page; ?>"><span class="fa fa-check-circle"></span>&nbsp;Submit</button>
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                                </form>
                              </div>

                            </div>
                          </div>