
<?php

$page = "Class Registeration";
$folder_in = '3';

  include('header/header.php'); 
 ini_set( "display_errors", 0); 

 $admin_id = $_SESSION['ADMIN_ID'];

  ?>
  <style type="text/css">
    .btn_pulse {
  
  display: block;
  border-radius: 10%;
  cursor: pointer;
  animation: none;
  float: left;
  bottom: 5px;
  right: 5px;
  font-weight: bold;
  padding-top: 2px;
  text-align: center;
  color: white;
  font-size: 14px;
  animation: pulse 1.6s infinite;
}


@keyframes pulse {
  0% {
    -moz-box-shadow: 0 0 0 0 #7252d3;
    box-shadow: 0 0 0 0 #7252d3;
  }
  70% {
    -moz-box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
  }
  100% {
    -moz-box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
  }
}
  </style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


    <ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-bottom: 04%;margin-bottom: 0;">
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="Class Registeration"> Class Registeration</li>
    </ol>




<div class="col-md-12" style="border-top: 1px solid #cccc;">
  
<div class="row" style="margin-top: 3%;margin-bottom: 3%;">
<div class="col-md-12">
<div class=" container-fluid   container-fixed-lg bg-white" style="padding: 2px 20px 4px 20px;">
<div class="card card-transparent">
<div class="card-header ">
<div class="card-title">


  <h3 style="text-transform: capitalize;"><a href="dashboard.php" data-toggle="tooltip" data-title="Back" style="text-align: center;color: black;"><span class="fa fa-angle-left" style="color: black;"></span></a> &nbsp;<span class="fa fa-users"></span> Class Registeration</h3>


</div>
<div class="clearfix"></div>
</div>

<?php 

    if(empty($_SESSION['student_id']))
    {
      $st = '';
    }else
    if(!empty($_SESSION['student_id']))
    {
      $st = $_SESSION['student_id'];
    }

 ?>

            <form action="" method="POST">

              <div class="row">
                <div class="col-md-3">
                    <div class="form-group form-group-default" style="margin-top: 0px;">
                      <label>Student ID</label>
                      <input list="search_student" name="student" id="student" required class="form-control" placeholder="Type Student ID" onclick="this.value=''" value="<?php echo $st; ?>" onchange="change_btn();" onclick="change_btn();" onblur="change_btn();">

                      <datalist id="search_student">
                        <?php 

                              $sql0015 = mysqli_query($conn,"SELECT * FROM `student_details`");
                              while($row0015=mysqli_fetch_assoc($sql0015))
                              {
                                $f_name = $row0015['F_NAME'];
                                $l_name = $row0015['L_NAME'];
                                $s_id = $row0015['STU_ID'];

                                $sql0016 = mysqli_query($conn,"SELECT * FROM `stu_login` WHERE `STU_ID` = '$s_id'");
                                while($row0016=mysqli_fetch_assoc($sql0016))
                                {
                                  $reg_code = $row0016['REGISTER_ID'];

                                  echo '<option value='.$reg_code.'>'.$f_name.' '.$l_name.'</option>';

                                }

                              }

                               ?>
                      </datalist>

                    </div>

                </div>
                <div class="col-md-3">
                    
                    <div class="form-group form-group-default">
                      <label>Teacher Name</label>
                      
                      <select class="form-control"  id="teacher" onchange="changed_teacher(); change_btn();" name="teacher_id" style="cursor: pointer;" onclick="change_btn();" onblur="change_btn();">
                        <option value="">Select Teacher Name</option>
                        <?php 

                          $sql0018 = mysqli_query($conn,"SELECT * FROM `teacher_login` WHERE `STATUS` = 'Active'");
                          while($row0018=mysqli_fetch_assoc($sql0018))
                          {
                            $teachers_id = $row0018['TEACH_ID'];

                            $sql0010 = mysqli_query($conn,"SELECT * FROM `teacher_details` WHERE `TEACH_ID` = '$teachers_id'");
                            while($row0010 = mysqli_fetch_assoc($sql0010))
                            {
                                    $teacher_name123 = $row0010['POSITION'].". ".$row0010['F_NAME']." ".$row0010['L_NAME'];
                            }

                            echo '<option value="'.$teachers_id.'">'.$teacher_name123.'</option>';

                          }


                         ?>
                      </select>

                    </div>

                </div>
                <div class="col-md-3">
                  <div class="form-group form-group-default">
                      <label>Class Name</label>
                      
                      <input type="hidden" id="admin_id" value="<?php echo $admin_id; ?>">

                      <select class="form-control"  id="clz"  name="class_id" style="cursor: pointer;" onchange="change_btn();" onclick="change_btn();" onblur="change_btn();">
                        <option value="">Select Class Name</option>
                      </select>

                    </div>

                </div>
                <div class="col-md-3" id="s_btn">
                 <button type="submit" class="btn btn-primary btn-block btn-lg" name="search_btn" id="search_btn"  style="margin-bottom: 10px;padding: 10px 20px 10px 20px;font-size: 20px;"><i class="pg-icon" style="font-size: 25px;margin-top: 4px;">search</i> Search</button>

               </div>

               <div class="col-md-3" id="a_btn">
                 <button type="button" class="btn btn-success btn-block btn-lg" name="add_btn" id="add_btn" onclick="reg_class();" style="margin-bottom: 10px;padding: 10px 20px 10px 20px;font-size: 20px;"><i class="pg-icon" style="font-size: 25px;margin-top: 4px;">plus</i> Add</button>
               </div>
              </div>

            </form>
            </div>

            <script type="text/javascript">
              $(document).ready(function(){

                document.getElementById("a_btn").style.display='none';
                document.getElementById("s_btn").style.display='block';

                $('#student,#teacher,#clz').bind('keyup change blue',function(){
                 
                  
                  var student_id = document.getElementById("student").value;
                  var teacher_id = document.getElementById("teacher").value;
                  var clz_id = document.getElementById("clz").value;

                    if(teacher_id !== '' || clz_id !== '')
                    {
                        document.getElementById("s_btn").style.display='none';
                        document.getElementById("a_btn").style.display='block';

                    }else
                    if(teacher_id == '' || clz_id == '')
                    {
                        document.getElementById("s_btn").style.display='block';
                        document.getElementById("a_btn").style.display='none';
                    }


                });
                 




              }); 
            </script>

</div>

</div>

</div>
<div class=" container-fluid container-fixed-lg bg-white" style="margin-top: 0px;">
            <div class="card card-transparent">
              <div class="card-header ">
                    <div class="card-title">
                    </div>
                    <div>
                      <div class="row">
                        <div class="col-md-6">
                        </div>
                      <div class="col-md-2"></div>
                      <div class="col-md-4">
                      <div class="col-md-12" style="padding-top: 18px;"></div>
                      <input type="text" id="search-table" class="form-control pull-right" placeholder="Search">
                      </div>

                    </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="card-body">
                <table class="table table-hover demo-table-search table-responsive-block" id="tableWithSearch">
                  <thead>
                      <tr>
                        <th style="width: 12%;" class="text-danger">Added Date</th>
                        <th style="width: 1%;">Category</th>
                        <th style="width: 1%;">Subject</th>
                        <th style="width: 1%;">Teacher Name</th>
                        <th style="width: 1%;">Class Name</th>
                        <th style="width: 1%;text-align: center;">Action</th>
                      </tr>
                    </thead>

                      <?php 
                      if(isset($_POST['search_btn']))
                      {
                          $student_id = $_POST['student'];
                          $sql005 = mysqli_query($conn,"SELECT * FROM `stu_login` WHERE `REGISTER_ID` = '$student_id'");
                          while($row005 = mysqli_fetch_assoc($sql005))
                          {
                              $stu_id = $row005['STU_ID'];
                          }

                          $_SESSION['student_id'] = $student_id;

                          $sql002 = mysqli_query($conn,"SELECT * FROM `transactions` WHERE `STU_ID` = '$stu_id' ORDER BY `UPDATE_TIME` DESC");
                          if(mysqli_num_rows($sql002)>0)
                          {
                              while($row003 = mysqli_fetch_assoc($sql002))
                              {
                                  
                                  $clz_id = $row003['CLASS_ID'];
                                  $teach_id = $row003['TEACH_ID'];
                                  $added_date = $row003['UPDATE_TIME'];

                                  $sql004 = mysqli_query($conn,"SELECT * FROM `classes` WHERE `CLASS_ID` = '$clz_id' AND `TEACH_ID` = '$teach_id'");
                                  while($row004 = mysqli_fetch_assoc($sql004))
                                  {
                                          $class_id = $row004['CLASS_ID'];
                                          $clz_name = $row004['CLASS_NAME'];
                                          $sub_id = $row004['SUB_ID'];
                                  }

                                  $sql005 = mysqli_query($conn,"SELECT * FROM `subject` WHERE `SUB_ID` = '$sub_id'");
                                  while($row005 = mysqli_fetch_assoc($sql005))
                                  {
                                          $sub_name = $row005['SUBJECT_NAME'];
                                          $level_id = $row005['LEVEL_ID'];
                                  }

                                  $sql006 = mysqli_query($conn,"SELECT * FROM `level` WHERE `LEVEL_ID` = '$level_id'");
                                  while($row006 = mysqli_fetch_assoc($sql006))
                                  {
                                          $level_name = $row006['LEVEL_NAME'];
                                  }

                                  $sql007 = mysqli_query($conn,"SELECT * FROM `teacher_details` WHERE `TEACH_ID` = '$teach_id'");
                                  while($row007 = mysqli_fetch_assoc($sql007))
                                  {
                                          $teacher_name = $row007['POSITION'].". ".$row007['F_NAME']." ".$row007['L_NAME'];
                                  }

                                  echo '<tr>
                                      <td class="v-align-middle">'.$added_date.'</td>
                                      <td class="v-align-middle">'.$level_name.'</td>
                                      <td class="v-align-middle">'.$sub_name.'</td>
                                      <td class="v-align-middle">'.$teacher_name.'</td>
                                      <td class="v-align-middle">'.$clz_name.'</td>';
                                      ?>
                                      <td class="v-align-middle text-center"><a href="../admin/query/delete.php?delete_reg_class=<?php echo $class_id; ?>&&stu_id=<?php echo $stu_id; ?>&&teach_id=<?php echo $teach_id; ?>&&student_regcode=<?php echo $student_id; ?>" onclick="return confirm('Are you sure remove subject?')" class="btn btn-danger"><i class="pg-icon">close</i> Unregister</a></td>
                                      <?php echo '
                                  </tr>';

                                  
                              }
                          }
                      }
                     ?>
                    
                    <tbody>
                    </tbody>
                </table>
              </div>
            </div>
            </div>
</div>
</div>
</div>
</div>

<script type="text/javascript">

    function changed_teacher(){
                  
        var student_id = document.getElementById("student").value;
        var teacher_id = document.getElementById("teacher").value;
        var clz_id = document.getElementById("clz").value;

         $.ajax({
          url:'../admin/query/check.php',
          method:"POST",
          data:{search_regiser:student_id,teach_id:teacher_id},
          success:function(data)
          {
            //alert(data)
            //$('#clz').html(data);
            document.getElementById("clz").innerHTML = data;
          }
        });
  }
</script>

<script type="text/javascript">
  
    function reg_class(){
                  
        var student_id = document.getElementById("student").value;
        var teacher_id = document.getElementById("teacher").value;
        var clz_id = document.getElementById("clz").value;

         $.ajax({
          url:'../admin/query/insert.php',
          method:"POST",
          data:{reg_student_subject:student_id,teach_id:teacher_id,clz_id:clz_id},
          success:function(data)
          {
            if(data == 0)
            {
              Swal.fire({
                position: 'top-middle',
                type: 'error',
                icon: 'error',
                title: 'Already Registered Subjects!',
                showConfirmButton: false,
                timer: 1000
              })
            }

            if(data>0)
            {
              Swal.fire({
                position: 'top-middle',
                type: 'success',
                icon: 'success',
                title: 'Registered Successfully!',
                showConfirmButton: false,
                timer: 1000
              }).then(function(){
                location.reload();
              });
            }
          }
        });
  }
</script>



<?php  include('footer/footer.php'); ?>


