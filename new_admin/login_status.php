
<?php

$page = "Login Status";
$folder_in = '0';

  include('header/header.php'); 
 ini_set( "display_errors", 0); 

 $teach_id = $_SESSION['TEACH_ID'];


  ?>
  <style type="text/css">
    .btn_pulse {
  
  display: block;
  border-radius: 10%;
  cursor: pointer;
  animation: none;
  float: left;
  bottom: 5px;
  right: 5px;
  font-weight: bold;
  padding-top: 2px;
  text-align: center;
  color: white;
  font-size: 14px;
  animation: pulse 1.6s infinite;
}


@keyframes pulse {
  0% {
    -moz-box-shadow: 0 0 0 0 #7252d3;
    box-shadow: 0 0 0 0 #7252d3;
  }
  70% {
    -moz-box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
  }
  100% {
    -moz-box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
  }
}
  </style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


    <ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-top: 04%;margin-bottom: 0;">
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="Login Status"> Login Status (Search)</li>
    </ol>




<div class="col-md-12" style="border-top: 1px solid #cccc;">
  
<div class="row" style="margin-top: 3%;">
<div class="col-md-4"></div>
<div class="col-md-4">
<div class=" container-fluid   container-fixed-lg bg-white" style="padding: 6px 20px 4px 20px;">
<div class="card card-transparent">
<div class="card-header ">
<div class="card-title">


  <h3 style="text-transform: capitalize;"> <span class="fa fa-search"></span> Search</h3>


</div>
<div class="clearfix"></div>
</div>

            <form action="login_report.php" method="POST" target="_blank">

              
            <div class="col-md-12" style="padding-left: 8px;display: none">
              <div class="col-md-12" style="margin-top: 10px;">

                <input type="hidden" value="<?php echo $teach_id; ?>" name="teacher_id" id="teacher_id">
              </div>
              </div>


            <div class="form-group form-group-default" style="margin-top: 10px;">
              <label>Student Name</label>
                <input type="text" id="browser" class="form-control"  name="student_id" required value="All" onclick="this.value = '';">

                <!-- list="browsers" <datalist id="browsers">
                  <option value="All">

                    <?php 

                      /* $sql001 = mysqli_query($conn,"SELECT * FROM `stu_login`");
                      while($row001 = mysqli_fetch_assoc($sql001))
                      {
                        $stu_id = $row001['STU_ID'];
                        $reg = $row001['REGISTER_ID'];

                        $sql002 = mysqli_query($conn,"SELECT * FROM `student_details` WHERE `STU_ID` = '$stu_id'");
                        while($row002 = mysqli_fetch_assoc($sql002))
                        {
                          $name = $row002['F_NAME']." ".$row002['L_NAME'];
                          $tp = $row002['TP'];
                        }

                        echo '<option value="'.$reg.'">'.$name.' | '.$tp.'</option>';
                      } */

                     ?>
                </datalist> -->

            </div>

            <div class="form-group form-group-default" style="margin-top: 10px;">
              <label>Start Date</label>
              <input type="date" value="<?php echo date('Y-m-01') ?>" name="start_date" class="form-control">
            </div>

            <div class="form-group form-group-default" style="margin-top: 10px;">
              <label>End Date</label>
              <input type="date" value="<?php echo date('Y-m-01') ?>" name="end_date" class="form-control">
            </div>

            <button type="submit" class="btn btn-success btn-block btn-lg" name="search_student" value="<?php echo $teach_id; ?>" style="margin-bottom: 10px;"><i class="pg-icon">search</i> Search</button>
            <center><a href="dashboard.php" data-toggle="tooltip" data-title="Back" style="text-align: center;">Back</a></center> 
            </form>
            </div>

</div>
<div class="col-md-4"></div>

</div>
</div>


<script type="text/javascript">
	setInterval(function() {
    $('blink').fadeIn(1300).fadeOut(1500);
}, 1000);
</script>

<script>
$(document).ready(function(){
  $("#search-table").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $(".myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

<script>
$(document).ready(function(){

  $("#up_rec").on("change", function() {
      $('.show_image').hide();
  });
});
</script>
<script type="text/javascript">
  setInterval(function() {
    $('.blink').fadeIn(1300).fadeOut(1500);
}, 1000);
</script>

<?php  include('footer/footer.php'); ?>


<script type="text/javascript">
  $(document).ready(function(){
    $('#teacher_id123').change(function(){

                   var teach_id = $('#teacher_id123').val();

                   $.ajax({
                    url:'../admin/query/check.php',
                    method:"POST",
                    data:{check_classes123:teach_id},
                    success:function(data)
                    {
                      //alert(data)
                      $('#level_clz').html(data);

                      
                    }
                   })
                 

                });
  })
</script>