
<?php

$page = "Chat Page";
$folder_in = '0';


  include('header/header.php'); 

 //session_destroy();

  ?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<style type="text/css">
 
  body
  {
    background-color: #fdfdfd !important;
  }

  .btn1 {
    background: linear-gradient(144deg, #490a87, #6a12bf);
    background-size: 400% 400%;
    border-radius: 20px;
    box-shadow: 1px 1px 8px 6px #cccc;
    
    -webkit-animation: AnimationName 23s ease infinite;
    -moz-animation: AnimationName 23s ease infinite;
    animation: AnimationName 23s ease infinite;
}

@-webkit-keyframes AnimationName {
    0%{background-position:0% 2%}
    50%{background-position:100% 99%}
    100%{background-position:0% 2%}
}
@-moz-keyframes AnimationName {
    0%{background-position:0% 2%}
    50%{background-position:100% 99%}
    100%{background-position:0% 2%}
}
@keyframes AnimationName {
    0%{background-position:0% 2%}
    50%{background-position:100% 99%}
    100%{background-position:0% 2%}
}

  .count {
  
  display: block;
  cursor: pointer;
  animation: none;
  float: left;
  bottom: 5px;
  right: 5px;
  font-weight: bold;
  padding-top: 2px;
  text-align: center;
  color: white;
  font-size: 14px;
  animation: pulse 1.6s infinite;
}


@keyframes pulse {
  0% {
    -moz-box-shadow: 0 0 0 0 white;
    box-shadow: 0 0 0 0 white;
  }
  70% {
    -moz-box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
  }
  100% {
    -moz-box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
  }
}



</style>

<?php $admin_type = $_SESSION['TYPE'];
      

      if($admin_type == 'ADMIN')
      {
          $hide0 = "none";
          $user_name001 = 'Administrator';
      }else
      if($admin_type == 'S_ADMIN')
      {
          $user_name001 = 'Super Administrator';
      }else
      if($admin_type == 'STAFF')
      {
          $user_name001 = 'Staff member';
      }


 ?>



<ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-top: 2%;margin-bottom: 0;">
<li class="breadcrumb-item"></li>

</ol>


<div class="row" style="padding-bottom: 10px;border-bottom: 1px solid #cccc;">
  <div class="col-md-8">
    
    <div class="col-md-12" style="margin-top: 0;">

      <h3 style="text-transform: capitalize;">Chat Page <br><label style="font-weight: bold;line-height: 0.2;font-size: 12px;text-transform: capitalize;" class="text-muted">(<?php echo $user_name001; ?>)</label></h3>

      
    </div>

  </div>

<script type="text/javascript">

  setInterval(function()
  { 

    var check_it = '1';


//check online status staff
  
  var user_id = document.getElementById('user_id').value;

  //alert(user_id);

    $.ajax({
        url:'../admin/query/check.php',
        method:"POST",
        data:{check_live_staff:user_id},
        success:function(data)
        {
         // alert(data)
          document.getElementById('show_active_status').innerHTML = data;
          
        }
      });

//check online status staff




  }, 1000);


//class for yesterday joined student count

  var yesterday_count = '1';

    $.ajax({
        url:'../admin/query/check.php',
        method:"POST",
        data:{yesterday_count:yesterday_count},
        success:function(data)
        {
         // alert(data)
          document.getElementById('yester_day_count').innerHTML = data;
          
        }
      });

//class for yesterday joined student count



</script>
</div>

  <div class="col-md-12" id="show_active_status">
    
  </div>

  <div class="container">

    <div class="col-md-12" style="padding:20px 20px 20px 20px;">


    <div class="row">
      <div class="col-md-1"></div>
      <div class="col-md-10" style="padding:10px 10px 10px 10px;background-color: #1dd1a1;">
        <div class="row">
          <div class="col-md-12">
            <h5 style="color:white;">

              <a href="dashboard.php" style="padding:10px 10px 10px 10px;color: white;font-size: 30px"><span class="fa fa-angle-left"></span></a>

              <span class="fa fa-comment-o" style="font-size:30px;"></span> Chat App

              <span class="fa fa-power-off" style="font-size:20px;color: white;float: right;margin-top: 8px;margin-right: 12px;"></span>
            </h5>

          </div>

        </div>

      </div>
      <div class="col-md-1"></div>
    </div>

      <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10" style="border:2px solid #cccc;padding: 0px;height:500px;overflow: auto;">
          

          <div class="col-md-12" style="padding:20px 20px 20px 20px;">
            
            <div class="row" style="margin-top:10px;">
              <div class="col-md-6">
                
                <div class="col-md-12" style="padding:10px 20px 4px 20px;background-color: #ffffff;border-radius: 10px;box-shadow: 1px 1px 3px 1px #cccc;font-size: 16px;">
                  
                  <label style="font-weight: bold;color: #38ce98;font-size: 16px;">Sender 01</label>
                  <p style="font-weight:500;font-size: 16px;">
                    Message 0001
                  </p>


                  <div class="row">
                    <div class="col-md-12" style="text-align: right;"><small style="text-align: right;opacity: 0.7;" class="text-muted">2021-10-26 08:30 PM</small></div>
                  </div>

                </div>

              </div>
            </div>

            <div class="row" style="margin-top:10px;">

              <div class="col-md-6"></div>
              
              <div class="col-md-6">
                
                <div class="col-md-12" style="padding:10px 20px 4px 20px;background-color: #dcf8c6;border-radius: 10px;box-shadow: 1px 1px 3px 1px #cccc;font-size: 16px;">
                  
                  <p style="font-weight:500;font-size: 16px;">
                    Message 0002
                  </p>

                  <div class="row">
                    <div class="col-md-12" style="text-align: right;"><small style="text-align: right;opacity: 0.7;" class="text-muted">2021-10-26 08:30 PM</small></div>
                  </div>
                  
                  
                </div>

              </div>

            </div>

          </div>


        </div>
        <div class="col-md-1"></div>
      </div>

        <div class="row">
          <div class="col-md-1"></div>
          <div class="col-md-10" style="padding:10px 10px 10px 10px;background-color: #dedede;">

            <div class="d-flex flex-row">
              <div class="p-2" style="width: 95%;">

                <textarea class="form-control" placeholder="Type Message.." style="border-radius: 50px;resize: none;padding-left: 16px;padding-top: 12px;height: 45px;font-size: 16px;font-weight: 500;"></textarea>

              </div>
              <div class="p-2">
                
                <button class="btn" style="padding:12px 12px 12px 12px;border-radius: 80px;border:1px solid #cccc;background-color: gray;"><span class="fa fa-paper-plane" style="font-size: 20px;color: white;"></span></button>

              </div>
            </div>

          </div>
          <div class="col-md-1"></div>
        </div>



    </div>
  </div>

<?php  include('footer/footer.php'); ?>


<script>
        $(function(){
            $("a.hidelink").each(function (index, element){
                var href = $(this).attr("href");
                $(this).attr("hiddenhref", href);
                $(this).removeAttr("href");
            });
            $("a.hidelink").click(function(){
                url = $(this).attr("hiddenhref");
                window.open(url, '_blank');
            })
        });
    </script>




<script type="text/javascript">
  $(document).ready(function(){
    $('#teacher_id123').change(function(){

                   var teach_id = $('#teacher_id123').val();

                   $.ajax({
                    url:'../admin/query/check.php',
                    method:"POST",
                    data:{get_classes_data:teach_id},
                    success:function(data)
                    {
                      //alert(data)
                      $('#level_clz').html(data);

                      
                    }
                   });
                 

                });
  })




</script>
