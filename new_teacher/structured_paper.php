  
<?php

$page = "Create Examination";
$folder_in = '0';

  include('header/header.php'); 
 ini_set( "display_errors", 0); 

 $teacher_id = $_SESSION['TEACH_ID'];

  ?>
  <style type="text/css">
    .btn_pulse {
  
  display: block;
  border-radius: 10%;
  cursor: pointer;
  animation: none;
  float: left;
  bottom: 5px;
  right: 5px;
  font-weight: bold;
  padding-top: 2px;
  text-align: center;
  color: white;
  font-size: 14px;
  animation: pulse 1.6s infinite;
}


@keyframes pulse {
  0% {
    -moz-box-shadow: 0 0 0 0 #7252d3;
    box-shadow: 0 0 0 0 #7252d3;
  }
  70% {
    -moz-box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
  }
  100% {
    -moz-box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
  }
}
  </style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


<div class="col-md-12" style="border-top: 1px solid #cccc;">

  <ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-bottom: 20%;margin-bottom: 0;">
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="Create Examination"> Create Examination</li>
    </ol>
<div style="margin: 10px 0px 10px 0;"><a href="exam.php" style="text-decoration: none;color: gray;"><span class="fa fa-angle-left"></span> Back</a></div>

<form action="../teacher/query/insert.php" method="POST" enctype="multipart/form-data">
<div class="row" style="margin-top: 1%;margin-bottom: 3%;">
<div class="col-md-6">

  <div class="col-md-12 bg-white" style="padding: 20px 20px 20px 20px;">
    <div style="border-bottom: 1px solid #cccc;">
      <label class="text-muted text-center" style="font-size: 30px;"><span class="fa fa-file" style="font-size: 30px;"></span><strong> Paper Details</strong></label>
    </div>

    <?php 

      /*$sql002 = mysqli_query($conn,"SELECT MAX(PAPER_ID) as 'last_exam_id' FROM `paper_master` WHERE `STATUS` = '0'");

      $check_next_paper_id = mysqli_num_rows($sql002);

      if($check_next_paper_id > 0)
      {
        while($row002 = mysqli_fetch_assoc($sql002))
        {
            echo $last_exam_id = $row002['last_exam_id'];
        }
      }else
      if($check_next_paper_id == '0')
      {
        $insert001 = mysqli_query($conn,"INSERT INTO `paper_master`(`PAPER_ID`, `PAPER_NAME`, `EXAM_KEY`, `UPLOAD_DATE`, `UPLOAD_TIME`, `NO_OF_QUESTIONS`, `NO_QUESTION_ANSWER`, `ANSWER_MARKS`, `PDF_NAME`, `UPLOAD_BY`, `TIME_DURATION_HOUR`, `TIME_DURATION_MIN`, `FINISH_DATE`, `FINISH_TIME`, `STATUS`, `CLASS_ID`, `COMMENT`, `START_DATE`, `START_TIME`, `FINISH_D_T`, `START_D_T`, `LAST_ATTENT_TIME`, `MAX_HOUR`, `MAX_MINUTE`, `PAPER_TYPE`) VALUES ([value-1],[value-2],[value-3],[value-4],[value-5],[value-6],[value-7],[value-8],[value-9],[value-10],[value-11],[value-12],[value-13],[value-14],[value-15],[value-16],[value-17],[value-18],[value-19],[value-20],[value-21],[value-22],[value-23],[value-24],[value-25])")
      }*/


     ?>


    <div style="padding-top: 20px;">
      
      <div class="row">
        <div class="col-md-12">
          <div class="form-group form-group-default input-group">
              <div class="form-input-group">
                <label>Paper Name</label>
                <input type="text" name="paper_name" class="form-control" placeholder="Write.." required data-toggle="tooltip" data-title="ප්‍රශ්න පත්‍රයේ නම" style="text-transform: capitalize;">
              </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="form-group form-group-default input-group">
              <div class="form-input-group">
                <label>Exam Key</label>
                <input type="text" list="exam_key" name="exam_key" class="form-control" placeholder="Write.." required data-toggle="tooltip" data-title="බහුවරණ ප්‍රශ්න පත්‍ර්‍ර්‍රය හා රචනා ප්‍රශ්න පත්‍රය සමග සම්බන්ධ කිරීමේදී පොදු අංකයක් භාවිතා කල යුතුය.">
                <datalist id="exam_key">
                    <?php 

                        $sql003 = mysqli_query($conn,"SELECT * FROM `paper_master`");
                        while($row003 = mysqli_fetch_assoc($sql003))
                        {
                            $exam_key = $row003['EXAM_KEY'];

                            echo '<option value="'.$exam_key.'"></option>';
                        }

                     ?>

                </datalist>
              </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-6">
          
            <div class="form-group form-group-default input-group">
                <div class="form-input-group">
                  <label>Teacher Name</label>
                  <select class="form-control" name="teacher_id" id="teacher_id" onchange="find_class();" data-toggle="tooltip" data-title="ගුරුවරයාගේ නම">
                    <?php 
                      $sql001 = mysqli_query($conn,"SELECT * FROM `teacher_details` WHERE `TEACH_ID` = '$teacher_id'"); //check Classes for registered teachers details
                      while($row001 = mysqli_fetch_assoc($sql001))
                      {

                          $teacher_id = $row001['TEACH_ID']; //Teacher Position
                          $teacher_f_name = $row001['F_NAME']; //Teacher First Name
                          $teacher_l_name = $row001['L_NAME']; //Teacher Last Name
                          $teacher_position = $row001['POSITION']; //Teacher Last Name

                          $teacher_full_name = $teacher_position.". ".$teacher_f_name." ".$teacher_l_name;

                          echo '<option value="'.$teacher_id.'">'.$teacher_full_name.'</option>';


                      }
                     ?>
                  </select>
                </div>
            </div>

        </div>

        <div class="col-md-6">
          
            <div style="border: 1px solid #cccc;">
                <div style="padding: 6px 6px 6px 6px;">

                <div id="add_class_button">
                  <button type="button" class="btn btn-success btn-block" style="padding: 9px 10px 9px 10px;" data-toggle="modal" data-target="#add_classes">
                    <i class="pg-icon">plus</i> Add Classes
                  </button></div>

                  <!-- Modal -->
        <div id="add_classes" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">

                <h4 class="modal-title text-left">Add Classes</h4>
                
              </div>
              <div class="modal-body">
               
                <div class="table-responsive" style="height: 400px;overflow: auto;">
                <table class="table table-hover">
                  <thead>
                    <th>#</th>
                    <th>Class Name</th>
                  </thead>
                  <tbody id="class_id001">  
                    <tr>
                              <td colspan="3" class="text-center text-danger"><span class="fa fa-warning"></span> Not Found Data!</td>
                        </tr>
                  </tbody>
                </table>
                </div>
              </div>
              <div class="modal-footer">
                  
                  <div id="sub_id">
                  <button type="button" class="btn btn-success btn-lg btn-block" onclick="close_class_modal();" style="font-size: 16px;" id="my_add_class"><i class="pg-icon" style="font-size: 20px;">tick_circle</i> Submit </button>
              </div>
                
              </div>
            </div>

          </div>
        </div>
          <!-- Modal -->

          <script type="text/javascript">
            document.getElementById('my_add_class').disabled = true;
            function click_all() {
              
              var array = []
            var checkboxes = document.querySelectorAll('input[type=checkbox]:checked')
              var clicked_amount = checkboxes.length;

              if(clicked_amount > 0)
              {
                document.getElementById('sub_id').disabled = false;
                document.getElementById('sub_id').innerHTML = '<button type="button" class="btn btn-success btn-lg btn-block" data-dismiss="modal"  style="font-size: 16px;"><i class="pg-icon" style="font-size: 20px;">tick_circle</i> Submit '+'('+clicked_amount+')</button>';

                
                document.getElementById('add_class_button').innerHTML = '<button type="button" class="btn btn-success btn-block" style="padding: 9px 10px 9px 10px;" data-toggle="modal" data-target="#add_classes" id="add_class_button"><i class="pg-icon">plus</i> Add Classes'+'('+clicked_amount+')</button>';
              }

              if(clicked_amount == '0')
              {
                document.getElementById('sub_id').disabled = true;
                document.getElementById('sub_id').innerHTML = '<button type="button" class="btn btn-success btn-lg btn-block" onclick="click_all();" style="font-size: 16px;"><i class="pg-icon" style="font-size: 20px;">tick_circle</i> Submit</button>';

                document.getElementById('add_class_button').innerHTML = '<button type="button" class="btn btn-success btn-block" style="padding: 9px 10px 9px 10px;" data-toggle="modal" data-target="#add_classes" id="add_class_button"><i class="pg-icon">plus</i> Add Classes</button>';


                
              }
              
            }

            function close_class_modal() {
              document.getElementById('close_class_modal').click();
            }
          </script>


                </div>
            </div>
      
        </div>
</div>


      <div class="row">
          <div class="col-md-12">
            <div class="form-group form-group-default input-group">
            <div class="form-input-group">
              <label>No Of Questions</label>
              <input type="number" name="no_question" id="question" class="form-control" placeholder="Write.." value="0" min="0" onclick="this.select();" required onchange="add_questions();" onfocus="add_questions();" data-toggle="tooltip" data-title="ප්‍රශ්න සංඛ්‍යාව">
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-6">
          
            <div class="form-group form-group-default input-group">
                <div class="form-input-group">
                  <label>Time Duration(Hours)</label>
                  <select class="form-control" name="hour" data-toggle="tooltip" data-title="ලබාදෙන කාලය(පැය ගණන)">
                    <option value="">Select Hour</option>
                    <?php 
                      for ($h=0; $h <= 12; $h++) 
                      { 

                        if($h<10)
                        {
                          $h = '0'.$h;
                        }

                        echo '<option value="'.$h.'">'.$h.'</option>';

                      }
                     ?>
                  </select>
                </div>
            </div>

        </div>

        <div class="col-md-6">
          
            <div class="form-group form-group-default input-group">
                <div class="form-input-group">
                  <label>Time Duration(Minute)</label>
                  <select class="form-control" name="minute" data-toggle="tooltip" data-title="ලබාදෙන කාලය(විනාඩි ගණන)">
                    <option value="">Select Minute</option>
                    <?php 
                      for ($m=0; $m <= 60; $m++) 
                      { 

                        if($m<10)
                        {
                          $m = '0'.$m;
                        }

                        echo '<option value="'.$m.'">'.$m.'</option>';

                      }
                     ?>
                  </select>
                </div>
            </div>
      
        </div>


      </div>

      <div class="row">
          <div class="col-md-6">
            <div class="form-group form-group-default input-group">
            <div class="form-input-group">
              <label>Start Date & Time</label>
              <input type="datetime-local" name="start_date" id="start" min ='<?php echo date('Y-m-d');?>T00:00' class="form-control" required data-toggle="tooltip" data-title="විභාගය ආරම්භවන දිනය හා වේලාව">
            </div>
          </div>
        </div>
        <div class="col-md-6">
          
          <div class="form-group form-group-default input-group">
              <div class="form-input-group">
                <label>Finish Date & Time</label>
                <input type="datetime-local" name="end_date" id="end" class="form-control" min ='<?php echo date('Y-m-d');?>T00:00' required data-toggle="tooltip" data-title="විභාගය අවසන් වන දිනය හා වේලාව">
              </div>
          </div>
          
        </div>
      </div>

      <div class="row">
          <div class="col-md-6">
            <div class="form-group form-group-default input-group">
            <div class="form-input-group">
              <label>Maximum Attendance Hours</label>

              <input type="number" name="max_hour" class="form-control" value="0" data-toggle="tooltip" data-title="සහභාගීවීමේ අවසන් කාලය(පැය)" min="0">

            </div>
          </div>
        </div>

          <div class="col-md-6">
            <div class="form-group form-group-default input-group">
            <div class="form-input-group">
              <label>Maximum Attendance Minute</label>

              <input type="number" name="max_minu" class="form-control" value="0" data-toggle="tooltip" data-title="සහභාගීවීමේ අවසන් කාලය(විනාඩි)" min="0">

            </div>
          </div>
        </div>
      </div>

    <div class="row">
      <div class="col-md-12">

      <div class="form-group form-group-default input-group" style="outline: none;">
          <div class="form-input-group" style="outline: none;padding: 10px 6px 10px 6px;">
            <label style="padding-bottom: 4px;">Upload Paper File <strong class="text-danger">(PDF Only)</strong></label>
             <input type="file" id="upload_pdf" name="upload_file" accept="application/pdf" style="display: none;" onchange="uploaded_rejected()"/>

             <a onclick="document.getElementById('upload_pdf').click();" style="cursor: pointer;" id="td_btn" data-toggle="tooltip" data-title="ප්‍රශ්නපත්‍රය අප්ලෝඩ් කිරීම.(PDF File පමණි.)">
                  <button type="button" class="btn btn-block btn-sm" name="update_payment" id="show_upload_file" style="padding: 20px 20px 20px 20px;border:3px dashed gray;font-size: 18px;outline: none;background-color: white;color:#545454;" value="Browse..."> <i class="fa fa-plus"></i> <label style="font-weight: bold;cursor: pointer;">Upload Paper File</label></button>
              </a>
          </div>

          <script type="text/javascript">
                    

                    function uploaded_rejected()
                    {
                        
                        var uploaded = document.getElementById('upload_pdf').value;

                        if(uploaded == '')
                        {
                          $("#td_btn").html('<button type="button" class="btn btn-block btn-sm" name="update_payment" id="reject_upload_btn" style="padding: 20px 20px 20px 20px;border:3px dashed gray;font-size: 18px;outline: none;background-color: white;" value="Browse..."> <i class="fa fa-plus"></i> <label style="font-weight: 1000px;">Upload Bank Slip</label></button>');
                          $('#td_btn').prop('disabled',true);

                        }else
                        if(uploaded !== '')
                        {

                            $("#td_btn").html('<button type="button" class="btn btn-block btn-sm" id="upload_btn" style="padding: 20px 20px 26px 20px;font-size: 18px;outline: none;background-color: #ff6500;color:white;" value="Browse..." > <i class="fa fa-spinner fa-pulse"></i> <label style="font-weight: 1000px;color:white;">Changing..</label></button>');

                            setTimeout(function() { 

                                $("#td_btn").html('<button type="button" class="btn btn-block btn-sm" name="update_payment" id="upload_btn" style="padding: 20px 20px 26px 20px;font-size: 18px;outline: none;background-color: #28a745;color:white;" value="Browse..." > <i class="fa fa-check-circle"></i> <label style="font-weight: 1000px;color:white;">Upload successfully</label></button>');
                                $('#show_upload_file').prop('disabled',false);

                            }, 1000);
                          

                          
                        }

                        
                    }

                  </script>
      </div>
    </div>
  </div>

      
    </div>
  </div>

  

</div>

<div class="col-md-6">

  <div class="col-md-12 bg-white" style="padding: 20px 20px 20px 20px;">

    <div style="border-bottom: 1px solid #cccc;">
      <label class="text-muted text-center" style="font-size: 30px;"><span class="fa fa-check-circle"></span> <strong> Answer Sheet</strong></label>
    </div>

  
  


    <script type="text/javascript">
      
      function moreItems(button,master) {
        
    var maxItems = Number(button.attributes.cnt.value);
    maxItems += 1;
    button.setAttribute("cnt", maxItems);
    if (maxItems < 20) {
      var label = document.createElement("label");
      label.id = "ItemLabel" + maxItems;
      label.innerHTML = "Question " + (maxItems + 1) + " : &nbsp;";
      var input = document.createElement("input");
      input.type = 'text';
      var count = maxItems-1;
      var sup_master = master-1;
      input.name = 'sub_mark['+sup_master+'][]';
      //alert(sup_master)

      $($(label)).insertBefore($(button));
      $($(input)).insertBefore($(button));
      $('<br/>').insertBefore($(button));
    }
  }

    </script>


    <div class="table-responsive" style="height: 540px;overflow: auto;padding-bottom: 6px;">
      <table class="table table-hover">
        <thead>
          <th>Question No</th>
          <th>Marks</th>
        </thead>

        <tbody id="que_ans">

          <tr>
            <td colspan="2" class="text-center"><span class="fa fa-warning"></span> Not Founded Question!</td>
          </tr>
          
        </tbody>
      </table>
    </div>

    <div style="padding-top: 10px;border-top: 1px solid #cccc;">

      <button type="submit" class="btn btn-success btn-lg btn-block" data-toggle="tooltip" data-title="තහවුරු කිරීම" style="padding: 10px 10px 10px 10px;font-size: 20px;" name="str_submit_paper" ><i class="pg-icon" style="font-size: 30px;">tick_circle</i> Submit Paper</button>
    </div>
  </div>
</div>


</div>

</form>

<script type="text/javascript">
  $(document).ready(function(){
      //$('#pay_btn').prop('disabled',true); //refresh with disable button

      setTimeout(function() {
          <?php $_SESSION['msg'] = ''; ?>
          $('#successMessage').fadeOut('slow');
      }, 3000); // <-- time in milliseconds
  });
</script>

<?php  include('footer/footer.php'); ?>


<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
<!-- search data -->

<!-- no data message/no found data show in search-->

<script type="text/javascript">
  $(document).ready(function () {

    (function ($) {

        $('#myInput').keyup(function () {
            var rex = new RegExp($(this).val(), 'i');
            $('#myTable tr').hide();
            $('#myTable tr').filter(function () {
                return rex.test($(this).text());
            }).show();
            $('.no-data').hide();
            if($('#myTable tr:visible').length == 0)
            {
                $('.no-data').show();
            }

        })

    }(jQuery));

});
</script>

<script type="text/javascript">


                  
        var teacher_id = document.getElementById('teacher_id').value;

         $.ajax({
          url:'../teacher/query/check.php',
          method:"POST",
          data:{check_teacher_mcq:teacher_id},
          success:function(data)
          {
            //alert(data)
            document.getElementById('class_id001').innerHTML = data;
          }
        });
  


</script>

<script type="text/javascript">


    function add_questions()
    {
      var show_question = document.getElementById('question').value;
      //alert(show_question)
      $.ajax({
          url:'../teacher/query/check.php',
          method:"POST",
          data:{str_que_ans:show_question},
          success:function(data)
          {
            //alert(data)
            document.getElementById('que_ans').innerHTML = data;
          }
        });
  }


</script>

