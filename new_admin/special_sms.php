
<?php

$page = "Custom SMS";
$folder_in = '0';

  include('header/header.php'); 
 ini_set( "display_errors", 0); 

 $today = date('Y-m-d'); //today

 $admin_id = $_SESSION['ADMIN_ID'];

  ?>
  <style type="text/css">
     .no-data {
    display: none;
    text-align: center;
}
    .btn_pulse {
  
  display: block;
  border-radius: 10%;
  cursor: pointer;
  animation: none;
  float: left;
  bottom: 5px;
  right: 5px;
  font-weight: bold;
  padding-top: 2px;
  text-align: center;
  color: white;
  font-size: 14px;
  animation: pulse 1.6s infinite;
}


@keyframes pulse {
  0% {
    -moz-box-shadow: 0 0 0 0 #7252d3;
    box-shadow: 0 0 0 0 #7252d3;
  }
  70% {
    -moz-box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
  }
  100% {
    -moz-box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
  }
}
  </style>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


    <ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-top: 04%;margin-bottom: 0;">
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="SMS Main Menu"> <a href="sms.php">SMS Main Menu</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="New Classes"> Custom SMS</li>
    </ol>

<?php 

      $sql001 = mysqli_query($conn,"SELECT * FROM `sms_counter` ORDER BY `SUB_SMS_C_ID` DESC");
      $qu001 = mysqli_fetch_assoc($sql001);
      $current_sms = $qu001['CURRENT_SMS'];

      if($current_sms<9)
      {
        $color = 'danger';
      }
      else
      if($current_sms>9)
      {
        $color = 'success';
      }
     ?>


<div class="col-md-12" style="border-top: 1px solid #cccc;">
  
<div class="row" style="margin-top: 0%;">
<div class="col-md-1"></div>
<div class="col-md-5" style="margin-top:10px;">
<div class=" container-fluid   container-fixed-lg bg-white" style="padding: 2px 20px;">
<div class="card card-transparent">
<div class="card-header ">
<div class="card-title col-md-12">

  <div class="row">
    <div class="col-md-9"><h3 style="text-transform: capitalize;"> <span class="fa fa-envelope"></span> Custom SMS</h3></div>
    <div class="col-md-3">
        
        <h5 class="label label-<?php echo $color; ?> text-center" style="font-size: 25px;box-shadow: 1px 1px 8px 1px #cccc;">

        <?php 

            if($current_sms<9)
            {
              echo '0'.$current_sms;
            }
            else
            if($current_sms>9)
            {
                echo number_format($current_sms);
            }

         ?> 
         <br>
         <small style="font-size: 12px;text-align: center;">Current SMS</small></h5>

    </div>
  </div>
</div>
<div class="clearfix"></div>
</div>
    
              

            <div class="form-group form-group-default" style="margin-top: 10px;">
              <label>Message</label>

              <?php 

                $session_sms_body = ''; //session_sms_body variable define

                if(!empty($_SESSION['SMS_BODY'])) //check session empty????
                {
                  $session_sms_body = $_SESSION['SMS_BODY']; //session_sms_body varible to assign session has data
                }

               ?>
              
              <textarea class="form-control" id="msg_body"  name="msg_body" required placeholder="Enter Your Message" onclick="this.select();" style="height: 80px;resize: none" maxlength="124"><?php echo $session_sms_body; ?></textarea>
              <div style="text-align: right;"><i>Characters <small id="character-count">0</small></i></div>

              <script type="text/javascript">
                $(document).ready(function(){


                  var input = $('textarea#input');
                  $('small#character-count').text(input.val().length);

                  input.bind('keydown', function() {
                      $('small#character-count').text(input.val().length);
                  });

                });
              </script>

            </div>

                <div class="form-group form-group-default" style="margin-top: 10px;">
                <label>Student Name</label>
                <input type="text" name="st_name" class="form-control" placeholder="Student Name.." autofocus id="student_name">
                </div>

              <div class="form-group form-group-default" style="margin-top: 10px;">
                <label>Telephone No</label>
                <input type="telephone" name="tp" class="form-control" placeholder="Telephone No (Ex - 777XXXXXX)" maxlength="9" required id="tp">
                </div>
  
            
              <div class="col-md-12" style="border-top:1px solid #cccc;padding-bottom:6px;"></div>

              <?php 
                

                if($current_sms == '0')
                {
                    echo '<button type="submit" class="btn btn-success btn-block btn-lg "disabled style="cursor:not-allowed;" id="single_sms_submit"><i class="pg-icon">send</i>&nbsp;Sent</button>
                      <span class="text-danger text-center" style="padding-top:4px;"><span class="fa fa-warning"></span> Your SMS package is over.Please recharge your sms package.</span>
                    ';
                }else
                if($current_sms>0)
                {
                    echo '<button type="submit" class="btn btn-success btn-block btn-lg" name="single_sms_submit" id="single_sms_submit"><i class="pg-icon">send</i>&nbsp;Sent</button>';
                }
               ?>
            
            <br><center><a href="sms.php">Back</a></center>

            </div>

</div>

</div>
<div class="col-md-5">
  <div class="col-md-12" style="background-color: white;padding: 20px 20px 20px 20px;margin-top:10px;">

    <?php 
      $sql0020 = mysqli_query($conn,"SELECT * FROM `single_sms` WHERE `SEND_DATE` BETWEEN '$today' AND '$today'"); //check today sent sms
      $sms_count0 = mysqli_num_rows($sql0020); //Today sent all SMS Count
      if($sms_count0<9)
      {
        $color = 'danger';
      }
      else
      if($sms_count0>9)
      {
        $color = 'success';
      }


     ?>

    <div class="row"> 
        <div class="col-md-10"><h4><span class="fa fa-send"></span> Sent Details </h4> </div>
        <div class="col-md-2" style="text-align: center;"><h5 class="label label-<?php echo $color; ?>" style="font-size: 25px;box-shadow: 1px 1px 8px 1px #cccc;">

        <?php 

            $sql002 = mysqli_query($conn,"SELECT * FROM `single_sms` WHERE `SEND_DATE` BETWEEN '$today' AND '$today'"); //check today sent sms
            $sms_count = mysqli_num_rows($sql002); //Today sent all SMS Count

            if($sms_count<9)
            {
              echo $sms_count = '0'.$sms_count;
            }
            else
            if($sms_count>9)
            {
                echo number_format($sms_count);
            }

         ?> 
         <br>
         <small style="font-size: 12px;">SMS</small></h5></div>
    </div>
  

  <input class="form-control" id="search" type="text" placeholder="Search..">

  <br>  
  <div class="table-responsive" style="height: 332px;overflow: auto;border:0px solid black;">
  <table class="table table-bordered table-striped" style="border-top: 1px solid #cccc;">
    <thead>
      <tr>
        <th style="width: 30%;">Sent Time</th>
        <th>Telephone</th>
        <th>Student Name</th>
        <th>Status</th>
      </tr>
    </thead>
    <tbody id="myTable">

    <tr class="no-data alert alert-danger">
          <td colspan="4" class="text-danger"><span class="fa fa-warning"></span> Not Found Data</td>
    </tr>

    <?php 
      

      $sql001 = mysqli_query($conn,"SELECT * FROM `single_sms` WHERE `SEND_DATE` BETWEEN '$today' AND '$today'"); //check today sent sms

      if(mysqli_num_rows($sql001) > 0) //check available data??
      {

            while($row001 = mysqli_fetch_assoc($sql001))
            {
              $tp = $row001['TP']; //telephone no
              $sent_time = $row001['SEND_TIME']; //sent time
              $student_name = $row001['STUDENT_NAME']; //student Name

              echo '<tr>
                      <td>'.$sent_time.'</td>
                      <td>'.$tp.'</td>
                      <td>'.$student_name.'</td>
                      <td><label class="label label-success">Sent</label></td>
                    </tr>';
            }

      }else
      if(mysqli_num_rows($sql001) == '0') {

        echo '<tr><td colspan="4" class="text-center text-danger"><span class="fa fa-warning"></span> Empty Data</td></tr>';
      }
     ?>

      

    </tbody>
  </table>
 </div>
 </div>
</div>
<div class="col-md-1"></div>
</div>


  <script type="text/javascript">

    $(document).ready(function(){ 
    $('#clz').bind('keyup change',function(){

      var clz_id = $('#clz').val();

      

      $.ajax({
        url:'../admin/query/check.php',
        method:"POST",
        data:{select_students:clz_id},
        success:function(data)
        {
          //alert(data)
          $('#mydatatable').html(data);
          
        }
      })
    

    });
  });
  </script>

   <script type="text/javascript">

    $(document).ready(function(){ 
    $('#clz').bind('keyup change',function(){

      var clz_id = $('#clz').val();

      $.ajax({
        url:'../admin/query/check.php',
        method:"POST",
        data:{student_count:clz_id},
        success:function(data)
        {
          //alert(data)
          $('#student_count').html(data);
          
        }
      })
    

    });
  });
  </script>




<script>
$(document).ready(function(){
  $("#search").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
<!-- search data -->

<!-- no data message/no found data show in search-->

<script type="text/javascript">
  $(document).ready(function () {
 
    (function ($) {

        $('#search').keyup(function () {
            var rex = new RegExp($(this).val(), 'i');
            $('#myTable tr').hide();
            $('#myTable tr').filter(function () {
                return rex.test($(this).text());
            }).show();
            $('.no-data').hide();
            if($('#myTable tr:visible').length == 0)
            {
                $('.no-data').show();
            }

        })

    }(jQuery));

});
</script>

 <script type="text/javascript">

    $(document).ready(function(){ 
    $('#single_sms_submit').click(function(){

      var student_name = $('#student_name').val();
      var msg_body = $('#msg_body').val();
      var tp = $('#tp').val();
      var single_sms_submit = 'single_sms_submit';

      if(student_name == '' || msg_body == '' || tp == '')
      {
        //alert("asda");
        Swal.fire(
          'Wrong!',
          'Some input is empty! Please fill them out.',
          'error'
        )

      }else
      {
          $('#single_sms_submit').prop('disabled',true);

            $.ajax({
              url:'../admin/query/insert.php',
              method:"POST",
              data:{single_sms_submit:single_sms_submit,st_name:student_name,msg_body:msg_body,tp:tp},
              success:function(data)
              {
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 500,
                    timerProgressBar: true,
                    onOpen: (toast) => {
                      toast.addEventListener('mouseenter', Swal.stopTimer)
                      toast.addEventListener('mouseleave', Swal.resumeTimer)
                    }
                  })

                  Toast.fire({
                    icon: 'success',
                    title: 'Sent Message!'
                  }).then(function(){
                      
                      $('#student_name').val('');
                      $('#msg_body').val('');
                      $('#tp').val('');

                      $('#student_name').focus();

                      location.reload();


                  });
                  
                
              }
            })
        }

    });
  });
  </script>

  <script type="text/javascript">
    $(document).ready(function(){

      $('#msg_body').on( 'keyup', function( e ) {
            if( e.which == 13 ) {
              //GOTO DOWN
              $('#student_name').focus();
            }
            
        });

      $('#student_name').on( 'keyup', function( e ) {
            if( e.which == 13 ) {
              //GOTO DOWN
              $('#tp').focus();
            }
            
        });

       $('#tp').on( 'keyup', function( e ) {
            if( e.which == 13 ) {
              //GOTO DOWN
              $('#single_sms_submit').click();
            }
            
        });
    });
  </script>

  <script>
$(document).ready(function () {

    $('#student_name').bind('keyup blur',function()
    { 
           var thistext = $(this);
           thistext.val(thistext.val().replace(/[^a-z A-Z .]/g,'') ); 

           
    });

})
</script>

<script>
$(document).ready(function () {

    $('#tp').bind('keyup blur',function()
    { 
           var thistext = $(this);
           thistext.val(thistext.val().replace(/[^0-9]/g,'') ); 

           
    });

})
</script>

<?php  include('footer/footer.php'); ?>


