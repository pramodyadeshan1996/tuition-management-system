

<?php
  $page = "Blog Post";
  $folder_in = '3';
  

      include('header/header.php');

        $s04 = mysqli_query($conn,"SELECT * FROM admin_login WHERE ADMIN_ID = '$admin_id'");
        while($row4 = mysqli_fetch_assoc($s04))
        {
          $name = $row4['ADMIN_NAME'];
        }


      $sql = mysqli_query($conn,"SELECT * FROM institute WHERE INS_ID = '1'");
      while($row = mysqli_fetch_assoc($sql))
      {
        $ins_web = $row['WEB'];
      }


 ?>
 <script src="https://code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>


    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>

 <style>
   div.searchable {
    width: 300px;
    float: left;
    margin: 0 15px;
}

.searchable input {
    width: 100%;
    height: 50px;
    font-size: 18px;
    padding: 10px;
    -webkit-box-sizing: border-box; /* Safari/Chrome, other WebKit */
    -moz-box-sizing: border-box; /* Firefox, other Gecko */
    box-sizing: border-box; /* Opera/IE 8+ */
    display: block;
    font-weight: 400;
    line-height: 1.6;
    color: #495057;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid #ced4da;
    border-radius: .25rem;
    transition: border-color .15s ease-in-out, box-shadow .15s ease-in-out;
    background: url("data:image/svg+xml;charset=utf-8,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 4 5'%3E%3Cpath fill='%23343a40' d='M2 0L0 2h4zm0 5L0 3h4z'/%3E%3C/svg%3E") no-repeat right .75rem center/8px 10px;
}

.searchable ul {
    display: none;
    list-style-type: none;
    background-color: #fff;
    border-radius: 0 0 5px 5px;
    border: 1px solid #add8e6;
    border-top: none;
    max-height: 180px;
    margin: 0;
    overflow-y: scroll;
    overflow-x: hidden;
    padding: 0;
}

.searchable ul li {
    padding: 7px 9px;
    border-bottom: 1px solid #e1e1e1;
    cursor: pointer;
    color: #6e6e6e;
}

.searchable ul li.selected {
    background-color: #e8e8e8;
    color: #333;
}

 </style>

    <ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-bottom: 04%;margin-bottom: 0;">
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="Blog Post"> Blog Post</li>
    </ol>


      <div class="col-md-12 bg-white" style="border:1px solid #cccc;height: auto;">
      
      <div class="col-md-12" style="border-bottom: 1px solid #cccc;">
        <div class="row">

            <div class="col-md-10" style="margin-bottom: 10px;"><h2><a href="dashboard.php" data-toggle="tooltip" data-title="Back"><i class="pg-icon" style="font-size: 22px;">chevron_left</i></a> Blog Post</h2></div>

            <div class="col-md-2" style="margin-bottom: 10px;">

              <button type="button" class="btn btn-info btn-block" data-toggle="modal" data-target="#create_post" style="margin-top:10px;padding: 12px 12px 12px 12px;"><i class="fa fa-plus"></i>&nbsp;Create Post</button>

              <div id="create_post" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg" style="width:100%;">

                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">Create Post</h4>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <form action="../admin/query/insert.php" method="POST" enctype="multipart/form-data">
                    <div class="modal-body">
                      
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group form-group-default">
                            <label>Post Title</label>
                            <input type="text" class="form-control" name="title" placeholder="Enter Title.." style="font-weight:bold;">
                          </div>

                          <div class="form-group form-group-default">
                            <label>Post Description</label>
                            <textarea id="summernote" name="desc"></textarea>
                            <script>
                              $('#summernote').summernote({
                                placeholder: 'Enter Description..',
                                tabsize: 2,
                                height: 250,
                                toolbar: [
                                  ['fontsize', ['fontsize']],
                                  ['fontname', ['fontname']],
                                  ['style', ['style']],
                                  ['font', ['bold', 'underline', 'clear']],
                                  ['color', ['forecolor']],
                                  ['color', ['backcolor']],
                                  ['para', ['ul', 'paragraph']],
                                  ['insert', ['link']]
                                ],
                                fontNames: [
                            'Serif', 'Sans', 'Arial', 'Arial Black', 'Courier',
                            'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande',
                            'Sacramento'
                                ]
                              });
                            </script>
                          </div>

                          <input type="hidden" name="user_id" value="<?php echo $_SESSION['ADMIN_ID']; ?>">
                          
                          <div class="form-group form-group-default">
                            <label>Post Image</label>
                            <input type="file" class="form-control" name="img" style="margin-top:10px;" accept="image/*">
                            
                          </div>

                        </div>
                      </div>

                    </div>
                    <div class="modal-footer">
                      <button type="submit" class="btn btn-success btn-lg" name="create_post" value=""><i class="fa fa-check"></i>&nbsp; Submit</button>
                      <button type="button" class="btn btn-default btn-lg" data-dismiss="modal">Close</button>
                    </div>
                    </form>
                  </div>

                </div>
              </div>

            </div>

        </div>
      </div>

          
          <div class=" container-fluid container-fixed-lg bg-white" style="margin-top: 0px;">
            <div class="card card-transparent">
              <div class="card-header ">

                 

                    <div class="clearfix"></div>
                </div>
                <div class="card-body">
                  <!--  style="height: 820px;overflow: auto;" -->
                <div class="table-responsive">
                <table class="table">
                  <thead>
                      <tr>
                        <th style="width: 30%;" colspan="2">Post</th>
                        <th style="width: 60%;">Description</th>
                        <th style="width: 10%;text-align: center;">Action</th>
                      </tr>
                    </thead>
                    <tbody>

                      <?php 
                          /*------------------ Header Pagination --------------------------------*/
                        $record_per_page = 10;

                        $nxt_five_loop = 0;

                        $page = '';

                        if(isset($_GET["page"]))
                        {
                          $page = $_GET["page"];
                        }
                        else
                        {
                         $page = 1;
                        }

                        $start_from = ($page-1)*$record_per_page; //Current page no -1 X Per Page records

                        $sql001 = "SELECT * FROM `post` order by `POST_ID` DESC LIMIT $start_from, $record_per_page";

                        

                        
                        $page_query = "SELECT * FROM `post` ORDER BY `POST_ID` DESC";

                        /*------------------ Header Pagination --------------------------------*/

                        
                        //LIMIT = 10-1 x 10 = 90 , 10";
                        $result = mysqli_query($conn, $sql001);
                        $page_result = mysqli_query($conn, $page_query);

                          $check = mysqli_num_rows($result);

                          if($check>0)
                          {
                          while($row001 = mysqli_fetch_assoc($result))
                          {
                            $add_time = $row001['ADD_TIME'];
                            $add_date = $row001['ADD_DATE'];
                            $title = $row001['TITLE'];
                            $description = $row001['DESCRIPTION'];
                            $post_id = $row001['POST_ID'];
                            $creator = $row001['ADMIN_ID'];
                            $post_view = $row001['POST_VIEW'];

                            if($post_view == '0')
                            {
                              $post_status = '';
                            }else
                            if($post_view == '1')
                            {
                              $post_status = 'checked';
                            }

                            $sql2 = mysqli_query($conn,"SELECT * FROM `admin_login` WHERE `ADMIN_ID` = '$creator'");
                            while($row2 = mysqli_fetch_assoc($sql2))
                            { 

                              $admin_name = $row2['ADMIN_NAME'];

                            }

                            $sql1113 = mysqli_query($conn,"SELECT * FROM `post_img` WHERE `POST_ID` = '$post_id'");
                            $ch = mysqli_num_rows($sql1113);

                            if($ch > 0)
                            {
                              while($row1113 = mysqli_fetch_assoc($sql1113))
                              { 

                                $post_img = $row1113['IMG'];
                                $disabled = 'pointer-events: auto;';

                              }
                            }else
                            if($ch == '0')
                            {
                              $post_img = 'no_img.jpg';
                              $disabled = 'pointer-events: none;';
                            }
                            


                            echo '
                                <td colspan="2">
                                  <div class="col-md-12" style="padding-bottom:20px;padding-top:20px;">
                                    <div>
                                      <p style="font-size:22px;font-weight:bold;">'.$title.'</p>
                                      <label style="margin-bottom:20px;opacity:0.7;font-size:12px;">Posted on '.date('jS F, Y',strtotime($add_date)).'</label>

                                    </div>
                                    <div class="row">
                                      <div class="col-md-4">
                                        <a href="../student/blog_post/'.$post_img.'" target="_blank"><img width="250px" height="auto" class="blog_img" src="../student/blog_post/'.$post_img.'"></a></td>

                                      </div>
                                    </div>
                                    </div>
                                </td>
                                <td>
                                	<div style="height:200px;overflow:auto;margin-top:60px;font-weight:500;line-height:2;">
                                          '.$description.'
                                     </div>
                                </td>
                                 <td class="text-center" style="padding-top:60px;">

                                <button type="button" data-toggle="modal" data-target="#edit_post'.$post_id.'" class="btn btn-success btn-xs btn-rounded mt-1 mr-1" style="padding:6px 8px;box-shadow:0px 0px 4px 3px #cccc;" data-title="Approve" data-toggle="tooltip" id="edit1'.$post_id.'"><i class="pg-icon">edit</i></button>
                                ' ?>
                                <a href="../admin/query/delete.php?delete_post=<?php echo $post_id; ?>&&page=<?php echo $page; ?>&&img=<?php echo $post_img; ?>" onclick="return confirm('Are you sure delete?')" class="btn btn-danger mt-1 mr-1 btn-xs btn-rounded" style="padding:6px 8px;box-shadow:0px 0px 4px 3px #cccc;" data-title="Delete" data-toggle="tooltip"><i class="pg-icon">trash_alt</i></a>

                                <?php echo '

                                </td>
                              </tr>

                            ';
                        ?>

                        <!-- Modal -->
                        <div id="edit_post<?php echo $post_id; ?>" class="modal fade" role="dialog">
                          <div class="modal-dialog modal-lg" style="width:100%;">

                            <!-- Modal content-->
                            <div class="modal-content">
                              <div class="modal-header">
                                <h4 class="modal-title">Edit Post</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                              </div>
                              <form action="../admin/query/update.php" method="POST" enctype="multipart/form-data">
                              <div class="modal-body">
                                
                              <div class="row">
                                <div class="col-md-12">
                                  <div class="form-group form-group-default">
                                    <label>Post Title</label>
                                    <input type="text" class="form-control" name="title" placeholder="Enter Title.." value="<?php echo $title; ?>" style="font-weight:bold;">
                                  </div>

                                  <div class="form-group form-group-default">
                                    <label class="mb-2">Post Description</label>
                                    <textarea id="summernote2" name="desc"><?php echo $description; ?></textarea>
                                      <script>
                                        $('#summernote2').summernote({
                                          placeholder: 'Enter Description..',
                                          tabsize: 2,
                                          height: 250,
                                          toolbar: [
                                            ['fontsize', ['fontsize']],
                                            ['fontname', ['fontname']],
                                            ['style', ['style']],
                                            ['font', ['bold', 'underline', 'clear']],
                                            ['color', ['forecolor']],
                                            ['color', ['backcolor']],
                                            ['para', ['ul', 'paragraph']],
                                            ['insert', ['link']]
                                          ],
                                          fontNames: [
                                      'Serif', 'Sans', 'Arial', 'Arial Black', 'Courier',
                                      'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande',
                                      'Sacramento'
                                          ]
                                        });
                                      </script>
                                   </div>

                                  <input type="hidden" name="last_img" value="<?php echo $post_img; ?>">
                                  
                                  <div class="form-group form-group-default">
                                    <label>Post Image</label>
                                    <input type="file" class="form-control" name="img" style="margin-top:10px;" accept="image/*">
                                    
                                  </div>

                                <div class="form-group form-group-default">
                                  <label>View Post for student</label>
                                  <div class="form-check form-check-inline switch switch-lg success" style="width:100%;">

                                    <div class="form-check form-check-inline switch switch-lg success" style="width:100%;">
                                    <input type="checkbox" id="view_post<?php echo $post_id; ?>" value="1" name="view_post" <?php echo $post_status; ?>>
                                    <label for="view_post<?php echo $post_id; ?>" class="text-danger" style="text-transform: capitalize;cursor: pointer;"></label>

                                  </div>
                                </div>
                                </div>
                              </div>

                              </div>
                              <div class="modal-footer">
                                <button type="submit" class="btn btn-success btn-lg" name="edit_post" value="<?php echo $post_id; ?>"><i class="fa fa-edit"></i>&nbsp; Update</button>
                                <button type="button" class="btn btn-default btn-lg" data-dismiss="modal">Close</button>
                              </div>
                              </form>
                            </div>

                          </div>
                        </div>


                                     
                        <?php
                          }
                        }else
                        if($check == '0')
                        {
                          echo '<tr><td colspan="6" class="text-center text-danger"><span class="fa fa-warning"></span> Not Found Data</td></tr>';
                        } 
                      

                        

                        
                         ?>


                    </tbody>
                </table>
                </div>
              </div>

                <div class="col-md-12" style="width: 100%;overflow: auto;">
                  <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-8" class="php_pagination">
                      
                      <div class="col-md-12">
                    <br />
                    <?php
                    $total_records = mysqli_num_rows($page_result);
                    $total_pages = ceil($total_records/$record_per_page);
                    $start_loop = $page;
                    $difference = $total_pages - $page;
                    if($difference <= 5)
                    {
                     $start_loop = $total_pages - 5;
                    }else
                    if($difference <= 0)
                    {
                      $start_loop = '0';
                    }

                    echo '<ul class="pagination">';
                    $end_loop = $start_loop + 4;
                    $five_loop = $page-5;
                    if($five_loop <= 0)
                    {
                      $five_loop = 1;
                    }
                    if($page > 1)
                    {

                      echo '<li><a href="blog_post.php?page=1">First</a></li>';
                      echo '<li><a href="blog_post.php?page='.($page - 1).'"> < </a></li>';
                      echo '<li><a href="blog_post.php?page='.$five_loop.'"> <<< </a></li>';

                    }
                    if($start_loop !== '1')
                    {
                      $few_no = $start_loop - 1;
                    }else
                    if($start_loop > 0)
                    {
                      $few_no = 1;
                    }
                    
                    for($i=$few_no; $i<=$end_loop+1; $i++)
                    {     
                      if($i == $page)
                      {
                        echo '<li class="active"><a href="blog_post.php?page='.$i.'" style="font-weight:bold;background-color:#2980b9;color:white;">'.$i.'</a></li>';
                      }else
                      {
                        if($i == '0')
                        {
                          continue;
                        }else
                        if($i>0)
                        {
                          echo '<li><a href="blog_post.php?page='.$i.'" >'.$i.'</a></li>';
                        }
                        
                      }

                    }
                    if($page <= $end_loop)
                    {
                      
                      if($page == '1')
                      {
                        $nxt_five_loop = 5;
                      }else
                      if($page >= 5)
                      {
                        $nxt_five_loop = $page+5;

                        if($total_pages < $nxt_five_loop)
                        {
                          $nxt_five_loop = $total_pages;
                        }
                      }else
                      {
                        $nxt_five_loop = $page+5;
                      }

                        echo '<li><a href="blog_post.php?page='.($page + 1).'" > > </a></li>';
                        echo '<li><a href="blog_post.php?page='.$nxt_five_loop.'" > >>> </a></li>';
                        echo '<li><a href="blog_post.php?page='.$total_pages.'" >Last</a></li>';
                      
                    }
                    
                    echo '</ul>';
                    
                    ?>
                    </div>

                    </div>
                  </div>
                    
                </div>
            </div>
            </div>
            </div>
            </div>
            </div>
            </div>

<?php include('footer/footer.php'); ?>

<script type="text/javascript">
  function sortTable(table, order) {
    var asc   = order === 'asc',
        tbody = table.find('tbody');
    
    tbody.find('tr').sort(function(a, b) {
        if (asc) {
            return $('td:first', a).text().localeCompare($('td:first', b).text());
        } else {
            return $('td:first', b).text().localeCompare($('td:first', a).text());
        }
    }).appendTo(tbody);
}
</script>

<script type="text/javascript">
    function GetSelected() {
        //Create an Array.
        var selected = new Array();
 
        //Reference the Table.
        var tblFruits = document.getElementById("tblFruits");
 
        //Reference all the CheckBoxes in Table.
        var chks = tblFruits.getElementsByTagName("INPUT");
 
        // Loop and push the checked CheckBox value in Array.
        for (var i = 0; i < chks.length; i++) {
            if (chks[i].checked) 
            {
                var s = selected.push(chks[i].value);

            }
        }
 
        //Display the selected CheckBox values.
        if (selected.length > 0) {
          if(confirm('Are you sure delete?'))
          {

              $.ajax({
               type: "POST",
               data: {delete_multi:selected},
               url: "../admin/query/delete.php",
               success: function(msg){
                 //alert(msg)
                 location.reload();
               }
            
              });
          }
        }else{
         
         alert("Please select student!")
        
        }
    };
</script>