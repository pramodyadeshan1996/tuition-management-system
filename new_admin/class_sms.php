<?php
  $page = "Class SMS";
  $folder_in = '0';

      include('header/header.php');

        $s04 = mysqli_query($conn,"SELECT * FROM admin_login WHERE ADMIN_ID = '$admin_id'");
        while($row4 = mysqli_fetch_assoc($s04))
        {
          $name = $row4['ADMIN_NAME'];
        }


$sql00100 = mysqli_query($conn,"SELECT * FROM classes");

$count = mysqli_num_rows($sql00100);

 ?>

    <ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-top: 04%;margin-bottom: 0;">
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="Class SMS"> Class SMS</li>
    </ol>


  <div class="col-md-12">
    
    <div class="row" style="padding-top: 10px;">
      
      <div class="col-md-12 bg-white" style="border:1px solid #cccc;height: auto;">
      
      <div class="col-md-12" style="border-bottom: 1px solid #cccc;">
        <div class="row">

            <div class="col-md-10" style="margin-bottom: 10px;">
              <h6>
                <a href="dashboard.php" data-toggle="tooltip" data-title="Back" class="text-muted">
                  <i class="fa fa-angle-left"></i> Back</a>
                </h6>
              <h2><span class="fa fa-comment"></span> Class SMS</h2>
            </div>
            <div class="col-md-2 mb-3">
              <div class="col-md-12" id="count_clz" style="margin-top: 20px"></div>
            </div>
        </div>
      </div>
    


<div class="col-lg-12">
 
  <div class="row" style="padding-bottom: 20px;border-bottom: 1px solid #cccc;padding-top: 20px;">
    <div class="col-md-2 mt-2">
      
        <select class="form-control" id="search_day">
          <option value="0">Today</option>
          <option value="1">Monday</option>
          <option value="2">Tuesday</option>
          <option value="3">Wednesday</option>
          <option value="4">Thursday</option>
          <option value="5">Friday</option>
          <option value="6">Saturday</option>
          <option value="7">Sunday</option>
        </select>

    </div>
    <div class="col-md-3 mt-2">
      
        <select class="form-control" id="status">
          <option value="1">Student Registered Teacher</option>
          <option value="0">Student Not Registered Teacher</option>
          <option value="2">All Teacher</option>
        </select>

    </div>

    <div class="col-md-1 mt-2">
        
        <button class="btn btn-success btn-block btn-lg" onclick="check_today_class();"><span class="fa fa-search"></span>&nbsp;Search</button>

    </div>
    <div class="col-md-3 mt-2">
      
    </div>
    <div class="col-md-3 mt-2">
        <input type="text" id="myInput" class="form-control pull-right" style="width: 100%;padding: 10px 15px 10px 15px;font-size: 15px;margin-bottom: 10px;border-radius: 20px;" placeholder="Search" data-toggle="tooltip" autofocus data-title="සෙවීම..">

    </div>


</div>



<script type="text/javascript">
  
  function check_today_class()
  {

   var search_day = document.getElementById('search_day').value;
   var status = document.getElementById('status').value;
   //alert(status);

   $.ajax({
    url:'../admin/query/check.php',
    method:"POST",
    data:{search_day:search_day,status:status},
    success:function(data)
    {
        //alert(data)
        document.getElementById('show_classes').innerHTML = data;
        
        var total_clz = document.getElementById('total_clz').value;
  //alert(total_clz)
  document.getElementById('count_clz').innerHTML = '<div style="width:200px;background-color:#000000b8;text-align:center;color:white;padding:12px 12px 12px 12px;font-size:16px;border-radius:20px;"><span class="fa fa-check"></span> Total Class - <label style="font-size:20px;font-weight:bold;">'+total_clz+'</label></div>';


    }
   });
 }


</script>


<div class="col-md-12" id="show_classes">
  
  <div class="table-responsive" style="height: 600px;overflow: auto;">
        <table class="table table-hover" style="margin: 0px;padding: 0px;">
          <thead>
            <th class="text-center">Picture</th>
            <th>Teacher Name</th>
            <th>Subject Name</th>
            <th>Class Name</th>
            <th>Time</th>
            <th class="text-center">Action</th>
          </thead>
          <tbody id="myTable">

            <tr class="no-data alert alert-danger" style="margin-top: 20px;display: none;">
              <td colspan="7" class="text-danger"><span class="fa fa-warning"></span> Not Found Data</td>
            </tr>

          <?php 

        $day = date('l');
        
 
        $i=0;
        
        $sql002 = mysqli_query($conn,"SELECT * FROM `classes` WHERE `DAY` LIKE '%".$day."%' ORDER BY `START_TIME` ASC");

        $c1 = mysqli_num_rows($sql002);

        if($c1 < 10)
        {
            echo '<input type="hidden" id="total_clz" value="0'.$c1.'">';
        }else
        if($c1 >= 10)
        {
            echo '<input type="hidden" id="total_clz" value="'.$c1.'">';
        }
        

        if($c1 > 0)
        {

                while($row002 = mysqli_fetch_assoc($sql002))
                {
                    $teach_id =$row002['TEACH_ID'];
                    $subject_id =$row002['SUB_ID'];
                    $class_name = $row002['CLASS_NAME'];

                    $class_id = $row002['CLASS_ID'];
                    $class_fees = $row002['FEES'];
                    $sub_id = $row002['SUB_ID'];
                    $class_start_time = $row002['START_TIME']; //Class Start time
                    $class_end_time = $row002['END_TIME']; //Class End time

                    $end_time = date('Y-m-d')." ".$class_end_time; //H:i:s Time Format

                    $str = strtotime($class_start_time);
                    $class_start_time = date('h:i A',$str);

                    $str2 = strtotime($class_end_time);
                    $class_end_time = date('h:i A',$str2);

                    $sql007 = mysqli_query($conn,"SELECT * FROM `subject` WHERE `SUB_ID` = '$subject_id'");
                    while($row007=mysqli_fetch_assoc($sql007))
                    {
                        $subject_name = $row007['SUBJECT_NAME'];
                        $level_id = $row007['LEVEL_ID'];

                        $sql008 = mysqli_query($conn,"SELECT * FROM `level` WHERE `LEVEL_ID` = '$level_id'");
                        while($row008=mysqli_fetch_assoc($sql008))
                        {
                            $level_name = $row008['LEVEL_NAME'];

                        }

                    }



                    $sql0035 = mysqli_query($conn,"SELECT * FROM `teacher_details` WHERE `TEACH_ID` = '$teach_id'");
                    while($row0035 = mysqli_fetch_assoc($sql0035))
                    {
                      $teacher_name = $row0035['POSITION'].". ".$row0035['F_NAME']." ".$row0035['L_NAME'];
                      $gender = $row0035['GENDER'];
                      $picture = $row0035['PICTURE'];
                      $tp = $row0035['TP'];
                      if($picture == '0')
                       {
                          if($gender == 'Male')
                          {
                            $picture = 'b_teacher.jpg';
                          }else
                          if($gender == 'Female')
                          {
                            $picture = 'g_teacher.jpg';
                          }
                       }


                    }

                    $sql0036 = mysqli_query($conn,"SELECT * FROM `classes` WHERE `TEACH_ID` = '$teach_id'");

                    $total_class = mysqli_num_rows($sql0036);

                    $sql009 = mysqli_query($conn,"SELECT * FROM `transactions` WHERE `TEACH_ID` = '$teach_id'");

                    $total_student = mysqli_num_rows($sql009);

                    if($i == '0')
                    {
                        $show = 'show';
                        $i++;
                    }else
                    {
                        $show = '';
                    }
           

                    $sql010 = mysqli_query($conn,"SELECT * FROM `transactions` WHERE `TEACH_ID` = '$teach_id' AND `CLASS_ID` = '$class_id'");

                    $total_class_student = mysqli_num_rows($sql010);

                    $current_time = date('Y-m-d H:i:s');

                    if($end_time > $current_time)
                    {

                      $disable_btn = '"';

                      if($total_class_student == '0')
                      {
                        $disable_btn = 'style="pointer-events:none;opacity:0.4;"';
                      }

                    echo '

                      <tr>
                          <td class="v-align-middle">

                          <center><img src="../teacher/images/profile/'.$picture.'" style="width:100px;height:100px;max-width:100px;max-height:100px;border-radius:80px;"></center>

                          </td>
                          <td class="v-align-middle"><h6>'.$teacher_name.'<br> <small>'.$tp.'</small></h6></td>
                          <td class="v-align-middle"><h6>'.$subject_name.'</h6></td>
                          <td class="v-align-middle"><h6>'.$class_name.' <br> <span class="label label-success">'.$total_class_student.' Students</span></h6> </td>
                          <td class="v-align-middle"><h6>'.$class_start_time.'-'.$class_end_time.' <br>
                          <center><span class="label label-success" style="font-size:14px;margin-top:6px;">'.$day.'</span></center></h6></td>
                          <td class="v-align-middle">'; ?>

                            <a href="../admin/query/insert.php?send_class_sms=<?php echo $class_id; ?>" <?php echo $disable_btn; ?> onclick="return confirm('Are you sure send SMS?');" class="btn btn-danger"><span class="fa fa-send"></span>&nbsp; Send SMS</a>

                            <?php 
                            echo '
                          </td>
                          
                      </tr>

                    ';
                  }
                                        
                } 
        }else
        if($c1 == '0')
        {
            echo '
            <tr>
              <td class="text-muted" colspan="6"><span class="fa fa-warning"></span> Not found data!</td>
            </tr>
              ';
        }

    ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
</div>
<!-- no data message/no found data show in search-->

<script>


var total_clz = document.getElementById('total_clz').value;
//alert(total_clz)
document.getElementById('count_clz').innerHTML = '<div style="width:200px;background-color:#000000b8;text-align:center;color:white;padding:12px 12px 12px 12px;font-size:16px;border-radius:20px;"><span class="fa fa-check"></span> Total Class - <label style="font-size:20px;font-weight:bold;">'+total_clz+'</label></div>';


$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });


    (function ($) {

        $('#myInput').keyup(function () {
            var rex = new RegExp($(this).val(), 'i');
            $('#myTable tr').hide();
            $('#myTable tr').filter(function () {
                return rex.test($(this).text());
            }).show();
            $('.no-data').hide();
            if($('#myTable tr:visible').length == 0)
            {
                $('.no-data').show();
            }

        })

    }(jQuery));

});
</script>




<?php include('footer/footer.php'); ?>