
<?php

$page = "Create Teacher";
$folder_in = '0';

  include('header/header.php'); 
 ini_set( "display_errors", 0); 


  ?>
  <style type="text/css">
    .btn_pulse {
  
  display: block;
  border-radius: 10%;
  cursor: pointer;
  animation: none;
  float: left;
  bottom: 5px;
  right: 5px;
  font-weight: bold;
  padding-top: 2px;
  text-align: center;
  color: white;
  font-size: 14px;
  animation: pulse 1.6s infinite;
}


@keyframes pulse {
  0% {
    -moz-box-shadow: 0 0 0 0 #7252d3;
    box-shadow: 0 0 0 0 #7252d3;
  }
  70% {
    -moz-box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
  }
  100% {
    -moz-box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
  }
}
  </style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<div class="row" style="border-bottom: 1px solid #cccc;padding-left: 8px;font-weight: bold;margin-top: 04%;margin-bottom: 0;">
  <div class="col-md-11">
    <ol class="breadcrumb" style="">
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="Create Administrators"> Create Teacher</li>
    </ol>
  </div>
  <div class="col-md-1">
            

  </div>
  </div>
</div>




<div class="col-md-12" style="margin-top: 0;">
  

<div class=" container-fluid   container-fixed-lg ">
<div class="row">
  
  <div class="col-md-3"></div>

  <div class="col-md-6 bg-white" style="padding: 10px 20px 20px 20px;">
        <h3 style="text-transform: capitalize;"><a href="dashboard.php" data-toggle="tooltip" data-title="Back"><i class="pg-icon" style="font-size: 22px;">chevron_left</i></a> Create Teacher</h3>
            <form  action="../admin/query/insert.php" method="POST" id="form-personal" role="form" autocomplete="off" enctype="multipart/form-data">

                            <div class="row">

                            <div class="col-md-12">
                          <div class="form-group form-group-default input-group">
                          <div class="form-input-group">
                          <label class="pull-left">Salutation</label>

                            <select class="form-control change_inputs" name="position">
                                <option value="Mr">Mr.</option>
                                <option value="Mrs">Mrs.</option>
                                <option value="Ms">Ms.</option>
                                <option value="Rev">Rev.</option>
                                <option value="Dr">Dr.</option>
                                <option value="Prof">Prof.</option>

                              </select>

                            </div>
                            </div>
                            </div>
                            </div>

                            <div class="row">
                            <div class="col-md-6">
                            <div class="form-group form-group-default input-group">
                            <div class="form-input-group">
                            <label class="pull-left">First Name</label>
                            <input type="text" name="fnm" class="form-control change_inputs" pattern="[a-zA-Z. ]+"  required placeholder="XXXXXXXX">
                            </div>
                            </div>
                            </div>

                            <div class="col-md-6">
                            <div class="form-group form-group-default input-group">
                            <div class="form-input-group">
                            <label class="pull-left">Last Name</label>
                            <input type="text" name="lnm" class="form-control change_inputs" pattern="[a-zA-Z. ]+"  required placeholder="XXXXXXXX">
                            </div>
                            </div>
                            </div>

                            </div>

                            <div class="row">
                            <div class="col-md-6">
                            <div class="form-group form-group-default input-group">
                            <div class="form-input-group">
                            <label class="pull-left">Date Of Birth</label>
                            <input class="form-control change_inputs" type="date" name="dob" required min="<?php $d = strtotime("-60 year"); echo date('Y-m-d',$d); ?>" max="<?php $d = strtotime("-5 year"); echo date('Y-m-d',$d); ?>" style="margin-bottom: 10px;">
                            </div>
                            </div>
                            </div>

                            <div class="col-md-6">
                            <div class="form-group form-group-default input-group">
                            <div class="form-input-group">
                            <label class="pull-left">Telephone No</label>
                            <input class="form-control change_inputs" type="telephone" pattern="[0-9]{10}" minlength="10" maxlength="10" name="number" id="number" required placeholder="0xxx xxxx xx" style="margin-bottom: 10px;">
                            </div>
                            </div>
                            </div>

                            </div>

                            <div class="row">
                            <div class="col-md-6">
                            <div class="form-group form-group-default input-group">
                            <div class="form-input-group">
                            <label class="pull-left">E-mail Address</label>
                            <input type="email" class="form-control change_inputs" name="email"  style="margin-bottom: 10px;" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" placeholder="XXXXXXXX">
                            </div>
                            </div>
                            </div>

                            <div class="col-md-6">
                            <div class="form-group form-group-default">
                            <label class="pull-left">Address</label>
                            <textarea name="address" required placeholder="XXXXXXXX" style="margin-bottom: 10px;" class="form-control change_inputs"></textarea>
                            </div>
                            </div>
                            </div>
                            <div class="row">
                            <div class="col-md-6">
                                              <div class="form-group form-group-default input-group">
                                              <div class="form-input-group">
                                              <label class="pull-left">Gender</label>
                                              <br>
                                              <div class="row">

                                                    <div class="col-md-6" style="padding-left: 6px;">
                                                      <label class="pull-left"><span class="fa fa-male"></span> Male : <input type="radio" checked  name="gender" value="Male"></label>
                                                    </div>

                                                    <div class="col-md-6" style="padding-left: 6px;">
                                                      <label class="pull-left" style="padding-left: 10px;margin-bottom: 10px;"><span class="fa fa-female"></span> Female : <input type="radio" name="gender" value="Female"> </label>
                                                    </div>
                                                
                                                    
                                              </div>

                                              </div>
                                              </div>
                                              </div>
                            <div class="col-md-6">
                            <div class="form-group form-group-default">
                              <label class="pull-left">Qualification</label>
                              
                              <textarea name="qualification" placeholder="XXXXXXXX" style="margin-bottom: 10px;" class="form-control change_inputs"><?php echo $qualification; ?></textarea>


                            </div>
                            </div>
                            </div>

                            <div class="row">
                              <div class="col-md-6">
                              <div class="form-group form-group-default input-group">
                              <div class="form-input-group">
                              <label>Username</label>
                              <input type="text" name="username" class="form-control change_inputs" placeholder="XXXXXXXX"  required>
                              </div>
                              </div>
                              </div>

                              <div class="col-md-6">
                                <div class="form-group form-group-default input-group">
                                <div class="form-input-group" id="show_hide_password" style="position: relative;">
                               
                                <label>Password</label>
                                <input type="password" name="password" placeholder="XXXXXXXX" class="form-control" required>

                                <a href=""><i class="fa fa-eye-slash" aria-hidden="true" style="position: absolute;right:10px;top:25px;"></i></a>

                                </div>
                              </div>
                              </div>

                              </div>

                            <div class="row">
                            <div class="col-md-6">
                            <div class="form-group form-group-default text-left">
                            <label style="padding-bottom: 10px;">Picture</label>
                              <input type="file" name="picture" class="form-control" accept="image/*" value="0">
                            </div>
                            </div>

                              <div class="col-md-6">
                                <div class="form-group form-group-default">
                                  <label>About Me</label>
                                  
                                  <textarea name="about" placeholder="xxxxxxxx" style="margin-bottom: 10px;" class="form-control change_inputs"></textarea>


                                </div>
                              </div>
                              </div>

                              <div class="row">
                                 <div class="col-md-12">
                                    <div class="form-group form-group-default">
                                      <label>Sample Video</label>
                                      
                                      <textarea name="video_link" placeholder="xxxxxxxx" style="margin-bottom: 10px;" class="form-control change_inputs"><?php echo $video_link; ?></textarea>


                                    </div>
                                    </div>
                              </div>


                              <div class="row">
                                 <div class="col-md-12">
                                    <div class="form-group form-group-default">
                                      <label>Student Profile Teacher Title</label>
                                      
                                      <textarea name="teacher_title" placeholder="xxxxxxxx" style="margin-bottom: 10px;" class="form-control change_inputs"></textarea>


                                    </div>
                                    </div>
                              </div>
                              
                              <div class="row">
                                <div class="col-md-12">
                                  <button aria-label="" id="up_btn" class="btn btn-success btn-block pull-right btn-lg btn-block" type="submit" name="reg_teacher"><i class="pg-icon">plus</i> Add
                            </button>
                                </div>
                              </div>
                            

                            </form>
            </div>
            <div class="col-md-3"></div>

</div>

</div>

<script type="text/javascript">
  
    $(document).ready(function(){  
     $('#level_clz').change(function(){

       var level_clz = $(this).val();
       var teach_id = $('#teach_id').val();

       $.ajax({
        url:'../teacher/query/check.php',
        method:"POST",
        data:{create_clz:level_clz,teach_id:teach_id},
        success:function(data)
        {
          //alert(data)
            $('#subject').html(data);
          
        }
       })
     

    });

     
   });

  </script>

<script type="text/javascript">
	setInterval(function() {
    $('blink').fadeIn(1300).fadeOut(1500);
}, 1000);
</script>

<script>
$(document).ready(function(){
  $("#search").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $(".myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

<script type="text/javascript">
  $(document).ready(function () {

    (function ($) {

        $('#search').keyup(function () {
            var rex = new RegExp($(this).val(), 'i');
            $('#myTable tr').hide();
            $('#myTable tr').filter(function () {
                return rex.test($(this).text());
            }).show();
            $('.no-data').hide();
            if($('#myTable tr:visible').length == 0)
            {
                $('.no-data').show();
            }

        })

    }(jQuery));

});
</script>

<script>
$(document).ready(function(){

  $("#up_rec").on("change", function() {
      $('.show_image').hide();
  });
});
</script>
<script type="text/javascript">
  setInterval(function() {
    $('.blink').fadeIn(1300).fadeOut(1500);
}, 1000);
</script>

<script type="text/javascript">
  $("#show_hide_password a").on('click', function(event) {
        event.preventDefault();
        if($('#show_hide_password input').attr("type") == "text"){
            $('#show_hide_password input').attr('type', 'password');
            $('#show_hide_password i').addClass( "fa-eye-slash" );
            $('#show_hide_password i').removeClass( "fa-eye" );
        }else if($('#show_hide_password input').attr("type") == "password"){
            $('#show_hide_password input').attr('type', 'text');
            $('#show_hide_password i').removeClass( "fa-eye-slash" );
            $('#show_hide_password i').addClass( "fa-eye" );
        }
    });
</script>

<?php  include('footer/footer.php'); ?>


