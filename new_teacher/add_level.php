
<?php

$page = "Create Level";
$folder_in = '0';

  include('header/header.php'); 
 ini_set( "display_errors", 0); 


  ?>
  <style type="text/css">
    .btn_pulse {
  
  display: block;
  border-radius: 10%;
  cursor: pointer;
  animation: none;
  float: left;
  bottom: 5px;
  right: 5px;
  font-weight: bold;
  padding-top: 2px;
  text-align: center;
  color: white;
  font-size: 14px;
  animation: pulse 1.6s infinite;
}


@keyframes pulse {
  0% {
    -moz-box-shadow: 0 0 0 0 #7252d3;
    box-shadow: 0 0 0 0 #7252d3;
  }
  70% {
    -moz-box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
  }
  100% {
    -moz-box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
  }
}
  </style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<div class="row" style="border-bottom: 1px solid #cccc;margin-top: 60px;">
  <div class="col-md-11">
    <ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-bottom: 04%;margin-bottom: 0;">
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="Create Level"> Create Level</li>
    </ol>
  </div>
  <div class="col-md-1">
        <div class="row">
          <div class="col-md-5"><a data-toggle="tooltip" data-title="Create a subject" data-placement="right"><button class="btn btn-primary btn-lg btn-rounded btn_pulse pull-right" data-toggle="modal" data-target="#create_level" style="padding: 10px 10px;"><i class="pg-icon">add</i></button></a></div>
          <div class="col-md-7">
          </div>
        </div>



         <div class="modal fade slide-up disable-scroll" id="create_level" tabindex="-1" role="dialog" aria-hidden="false">
          <div class="modal-dialog ">
          <div class="modal-content-wrapper">
          <div class="modal-content">
          <div class="modal-header clearfix text-left">
          <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
          </button>
          <h5>Create Level</h5>
          </div>
          <div class="modal-body">
            <form action="../admin/query/insert.php" method="POST" enctype="multipart/form-data">
            
            <div class="form-group form-group-default input-group">
            <div class="form-input-group">
            <label>Level Name</label>
            <input type="text" name="level" class="form-control change_inputs" placeholder="XXXXXXXXXXXX" required>
            </div>
            </div>

            <div class="form-group form-group-default input-group">
            <div class="form-input-group">
            <label>Short Name</label>
            <input type="text" name="sn" class="form-control change_inputs" placeholder="XXXXXXXXXXXX" required>
            </div>
            </div>

            <div class="form-group form-group-default input-group">
            <div class="form-input-group">
            <label>Picture</label>
            <input type="file" name="picture" class="form-control" accept="image/*" value="0">
            </div>
            </div>

            <button type="submit" class="btn btn-success btn-block btn-lg" name="create_level123"><i class="pg-icon">add</i> Add</button>
            </form>
            </div>
            </div>
            </div>
            </div>

            </div>
            
</div>
</div>





<div class="col-md-12" style="margin-top: 0;">
  <h3 style="text-transform: capitalize;"><a href="dashboard.php" data-toggle="tooltip" data-title="Back"><i class="pg-icon" style="font-size: 22px;">chevron_left</i></a> Create Level</h3>

<div class=" container-fluid   container-fixed-lg bg-white">
<div class="card card-transparent">
<div class="card-header ">
<div class="card-title">Details and removal of all the level you have entered can be done here.
</div>
<div class="pull-right">
<div class="col-xs-12">
<input type="text" id="search" class="form-control pull-right" placeholder="Search" data-toggle="tooltip" data-title="අවශ්‍ය දත්ත සෙවීම'">
</div>
</div>
<div class="clearfix"></div>
</div>
<?php 

$sql00100 =  mysqli_query($conn,"SELECT * FROM level");

$count = mysqli_num_rows($sql00100);

 ?>
<div class="card-body table-responsive" style="height: 500px;overflow: auto;margin-bottom: 0;">
  <table class="table demo-table-search table-responsive-block text-left table-striped" id="tableWithSearch">
  <thead>

    <th style="width: 9%;text-align: center;">Picture</th>
    <th>Level Name <label class="label label-success"><?php echo  $count; ?></th>
    <th>Short Name</th>
    <th style="text-align: center;">Action</th>

  </thead>
  <tbody id="myTable">
<tr class="no-data alert alert-danger" style="margin-top: 20px;display: none;">
  <td colspan="4" class="text-danger"><span class="fa fa-warning"></span> Not Found Data</td>
</tr>
  <?php 
          $sql0015 = mysqli_query($conn,"SELECT * FROM level ORDER BY LEVEL_ID ASC");

          if(mysqli_num_rows($sql0015)>0)
          {
              while($row0015=mysqli_fetch_assoc($sql0015))
              {
                  $level_name = $row0015['LEVEL_NAME'];
                  $level_id = $row0015['LEVEL_ID'];
                  $sn = $row0015['SHORT_NAME'];
                  $pic = $row0015['PICTURE'];

                  if($pic == '0')
                  {
                    $pic = 'subject.png';
                  }

                echo '<tr>
                          <td class="v-align-middle">
                          
                          <center><img src="../admin/images/level_image/'.$pic.'" class="rounded-circle image-responsive" id="show_image" style="height: 100px;width: 100%;border: 0px solid white;"></center></td>
                          <td class="v-align-middle">'.$level_name.'</td>
                          <td class="v-align-middle">'.$sn.'</td>
                          <td class="v-align-middle" style="text-align:center;">

                          <button data-toggle="modal" data-target="#edit_level'.$level_id.'" class="btn btn-success  btn-xs btn-rounded" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;" data-title="Approve" data-toggle="tooltip" id="edit1'.$stu_id.'"><i class="pg-icon">edit</i></button>
                          ';?>

                        <button type="button" data-target="#delete_level<?php echo $level_id; ?>" data-toggle="modal" class="btn btn-danger btn-rounded btn-xs" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;"><i class="pg-icon">trash_alt</i></button>

                        <div id="delete_level<?php echo $level_id; ?>" class="modal fade" role="dialog">
                          <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                              <div>
                                <div class="row" style="padding-left: 20px;padding-bottom: 20px;">
                                  <div class="col-md-8 text-left"><h4 class="modal-title">Delete Level</h4></div>
                                  <div class="col-md-4"><button type="button" class="close" data-dismiss="modal">&times;</button></div>
                                </div>
                              </div>
                              <div class="modal-body">

                                <div class="text-left" style="padding-bottom: 10px;"><i class="pg-icon text-success">bulb</i> විකල්පය - අදාල කාණ්ඩයේ නම වෙනස් කරන්න. එවිට අනිකුත් විශයන් පිලබදව ද සැලකිලිමත්වන්න.</div>

                                <div class="text-left" style="padding-bottom: 10px;"><span class="fa fa-warning text-danger"></span> අවවාදය! - ඔබ මෙම කාණ්ඩය යටතේ අඩංගු කර ඇති සියලුම විශයන් හා පන්ති ඉවත් වේ.</div>

                                  
                                <div class="text-left"><i class="pg-icon">cursor</i> විධානය - ඒ සදහා ඔබ එකග වේ නම් දත්ත මකා දමන්නද?</div>

                              </div>
                              <div class="modal-footer">
                                <a href="../admin/query/delete.php?delete_level2=<?php echo $pic; ?>&&level_id=<?php echo $level_id; ?>" class="btn btn-success"><i class="pg-icon">tick_circle</i>&nbsp;Yes</a>
                                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="pg-icon">close</i>&nbsp;No</button>
                              </div>
                            </div>

                          </div>
                        </div>

                        <?php
                        echo '
                          </td>
                        </tr>';

                        ?>

                        <div class="modal fade slide-up disable-scroll" id="edit_level<?php echo $level_id; ?>" tabindex="-1" role="dialog" aria-hidden="false">
                          <div class="modal-dialog ">
                          <div class="modal-content-wrapper">
                          <div class="modal-content">
                          <div class="modal-header clearfix text-left">
                          <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
                          </button>
                          <h5>Edit Subjects</h5>
                          </div>
                          <div class="modal-body">
                            <form action="../admin/query/update.php" method="POST" enctype="multipart/form-data">
                              
                              <div class="form-group form-group-default input-group">
                              <div class="form-input-group">
                              <label>Level Name</label>
                              <input type="text" name="level" class="form-control change_inputs" value="<?php echo $level_name; ?>" placeholder="XXXXXXXXXXXX" required>
                              </div>
                              </div>

                              <div class="form-group form-group-default input-group">
                              <div class="form-input-group">
                              <label>Short Name</label>
                              <input type="text" name="sn" class="form-control change_inputs" placeholder="XXXXXXXXXXXX" required value="<?php echo $sn; ?>" >
                              </div>
                              </div>

                              <div class="form-group form-group-default input-group">
                              <div class="form-input-group">
                              <label>Picture</label>
                              <input type="file" name="picture" class="form-control" accept="image/*" value="0">
                              </div>
                              </div>

                              <input type="hidden" name="recent_file" value="<?php echo $pic; ?>">
                              <button type="submit" class="btn btn-primary btn-block btn-lg" name="edit_level123" value="<?php echo $level_id; ?>"><i class="pg-icon">edit</i> Edit</button>
                              </form>
                              </div>
                              </div>
                              </div>
                              </div>
                            </div>
<?php

              } 
            }else
            if(mysqli_num_rows($sql0015)== '0')
            {
                echo '<tr><td colspan="4" class="text-danger text-center"><span class="fa fa-warning text-danger"></span> Empty Data!</td></tr>';
            }

 ?>
  

  </tbody>
  </table>
</div>
</div>

</div>
</div>

<script type="text/javascript">
	setInterval(function() {
    $('blink').fadeIn(1300).fadeOut(1500);
}, 1000);
</script>

<script>
$(document).ready(function(){
  $("#search").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

<script type="text/javascript">
  $(document).ready(function () {
 
    (function ($) {

        $('#search').keyup(function () {
            var rex = new RegExp($(this).val(), 'i');
            $('#myTable tr').hide();
            $('#myTable tr').filter(function () {
                return rex.test($(this).text());
            }).show();
            $('.no-data').hide();
            if($('#myTable tr:visible').length == 0)
            {
                $('.no-data').show();
            }

        })

    }(jQuery));

});
</script>

<script>
$(document).ready(function(){

  $("#up_rec").on("change", function() {
      $('.show_image').hide();
  });
});
</script>
<script type="text/javascript">
  setInterval(function() {
    $('.blink').fadeIn(1300).fadeOut(1500);
}, 1000);
</script>

<?php  include('footer/footer.php'); ?>


