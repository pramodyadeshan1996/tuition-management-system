

<?php
  $page = "Students Approved";
  $folder_in = '3';

      include('header/header.php');

        $s04 = mysqli_query($conn,"SELECT * FROM admin_login WHERE ADMIN_ID = '$admin_id'");
        while($row4 = mysqli_fetch_assoc($s04))
        {
          $name = $row4['ADMIN_NAME'];
        }

 ?>

    <ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-bottom: 04%;margin-bottom: 0;">
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="Student Approved"> Students Approved</li>
    </ol>


      <div class="col-md-12 bg-white" style="border:1px solid #cccc;height: auto;">
      
      <div class="col-md-12" style="border-bottom: 1px solid #cccc;">
        <div class="row">

            <div class="col-md-9" style="margin-bottom: 10px;"><h2><a href="dashboard.php" data-toggle="tooltip" data-title="Back"><i class="pg-icon" style="font-size: 22px;">chevron_left</i></a> Students Approved</h2></div>

        </div>
      </div>

          
          <div class=" container-fluid container-fixed-lg bg-white" style="margin-top: 0px;">
            <div class="card card-transparent">
              <div class="card-header ">
                    <div class="card-title">
                    </div>
                    <div>
                      <div class="row">
                        <div class="col-md-6">
                          <form action="" method="POST">
                          <?php 

                            if(!isset($_POST['show_students']))
                            {?>

                              <button class="btn btn-success btn-sm pull-left" name="show_students" data-toggle="tooltip" data-title="Show all students"><span class="fa fa-check-circle"></span>&nbsp; Show All Student</button>

                            </form>
                            <?php 
                            }else
                            if(isset($_POST['show_students']))
                            {?>

                              <button class="btn btn-success btn-sm pull-left" name="hide_students" data-toggle="tooltip" data-title="Show all students"><span class="fa fa-check-circle"></span>&nbsp; Hide All Student</button>

                            </form>
                            <?php 
                            }

                           ?>
                          
                            
                          </form>


                        </div>
                      <div class="col-md-2">
                          
                        </div>
                      <div class="col-md-4">
                      <div class="col-md-12" style="padding-top: 0px;"></div>
                      <input type="text" id="search-table" class="form-control pull-right" placeholder="Search">
                      </div>

                    </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="card-body">
                <table class="table table-hover demo-table-search table-responsive-block" id="tableWithSearch">
                  <thead>
                      <tr>
                        <th style="width: 1%;">Registered ID</th>
                        <th style="width: 1%;">Name</th>
                        <th style="width: 1%;">TP No</th>
                        <th style="width: 1%;">DOB</th>
                        <th style="width: 1%;">Gender</th>
                        <th style="width: 1%;text-align: center;">Action</th>
                      </tr>
                    </thead>
                    <tbody>

                      <?php 
                        if(isset($_POST['show_students']))
                        {
                          $today = date('Y-m-d h:i:s A');

                          //mysqli_query($conn,"UPDATE stu_login SET REG_DATE = '$today'");

                          $sql001 = mysqli_query($conn,"SELECT * FROM `stu_login` WHERE `STATUS` = 'Active' ORDER BY `STU_ID` DESC");

                          $check = mysqli_num_rows($sql001);

                          if($check>0){
                          while($row001 = mysqli_fetch_assoc($sql001))
                          {
                            $stu_id = $row001['STU_ID'];
                            $reg = $row001['REGISTER_ID'];
                            $register_date = $row001['REG_DATE'];
                            $password = $row001['PASSWORD'];

                            $sql002 = mysqli_query($conn,"SELECT * FROM `student_details` WHERE `STU_ID` = '$stu_id'");
                            while($row002 = mysqli_fetch_assoc($sql002))
                            {
                              $name = $row002['F_NAME']." ".$row002['L_NAME'];
                              $fnm = $row002['F_NAME'];
                              $lnm = $row002['L_NAME'];
                              $tp1 = $row002['TP'];
                              $tp = str_replace(" ","",$tp1);
                              $dob = $row002['DOB'];
                              $gender = $row002['GENDER'];
                              $email = $row002['EMAIL'];
                              $address = $row002['ADDRESS'];
                              $new = $row002['NEW'];
                              
                            }

                            echo '

                              <tr>
                                <td class="v-align-left">'.$reg.'</td>
                                <td class="v-align-left">'.$name.'</td>
                                <td class="v-align-left">'.$tp.'</td>
                                <td class="v-align-left">'.$dob.'</td>
                                <td class="v-align-left">'.$gender.'</td>
                                 <td class="v-align-middle text-center">

                                <button data-toggle="modal" data-target="#stu_data'.$stu_id.'" class="btn btn-success  btn-xs btn-rounded" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;" data-title="Approve" data-toggle="tooltip" id="edit1'.$stu_id.'"><i class="pg-icon">edit</i></button>
                                ' ?>
                                <a href="../admin/query/update.php?approved_stu=<?php echo $stu_id; ?>&&approve=Disapprove" onclick="return confirm('Are you sure disapprove?')" class="btn btn-danger  btn-xs btn-rounded" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;" data-title="Disapprove" data-toggle="tooltip"><i class="pg-icon">trash_alt</i></a>

                                <?php echo '

                                </td>
                              </tr>

                            ';
                        ?>
                                      <div class="modal fade slide-up disable-scroll" id="stu_data<?php echo $stu_id; ?>" tabindex="-1" role="dialog" aria-hidden="false">
                                        <div class="modal-dialog ">
                                          <div class="modal-content-wrapper">
                                            <div class="modal-content modal-lg">
                                              <div class="modal-header clearfix text-left">
                                                <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
                                                </button>
                                                <h5>Students Details</h5>
                                              </div>
                                              <div class="modal-body">

                                                  <form  action="../admin/query/update.php" method="POST" id="form-personal" role="form" autocomplete="off">


                                                        <div class="row">
                                                        <div class="col-md-6">
                                                        <div class="form-group form-group-default input-group">
                                                        <div class="form-input-group">
                                                        <label>First Name</label>
                                                        <input type="text" name="fnm" class="form-control change_inputs" value="<?php echo $fnm; ?>" pattern="[a-zA-Z. ]+"  required>
                                                        </div>
                                                        </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                        <div class="form-group form-group-default input-group">
                                                        <div class="form-input-group">
                                                        <label>Last Name</label>
                                                        <input type="text" name="lnm" class="form-control change_inputs" value="<?php echo $lnm; ?>" pattern="[a-zA-Z. ]+"  required>
                                                        </div>
                                                        </div>
                                                        </div>

                                                        </div>

                                                        <div class="row">
                                                        <div class="col-md-6">
                                                        <div class="form-group form-group-default input-group">
                                                        <div class="form-input-group">
                                                        <label>Date Of Birth</label>
                                                        <input class="form-control change_inputs" type="date" name="dob" required min="<?php $d = strtotime("-60 year"); echo date('Y-m-d',$d); ?>" max="<?php $d = strtotime("-5 year"); echo date('Y-m-d',$d); ?>" style="margin-bottom: 10px;" value="<?php echo $dob; ?>">
                                                        </div>
                                                        </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                        <div class="form-group form-group-default input-group">
                                                        <div class="form-input-group">
                                                        <label>Telephone No</label>
                                                        <input class="form-control change_inputs" type="telephone" pattern="[0-9]{10}" minlength="10" maxlength="10" name="number" id="number" value="<?php echo $tp; ?>" required placeholder="0xxx xxxx xx" style="margin-bottom: 10px;">
                                                        </div>
                                                        </div>
                                                        </div>

                                                        </div>

                                                        <div class="row">
                                                        <div class="col-md-12">
                                                        <div class="form-group form-group-default input-group">
                                                        <div class="form-input-group">
                                                        <label>E-mail Address</label>
                                                        <input type="email" class="form-control change_inputs" name="email" value="<?php echo $email; ?>" required  style="margin-bottom: 10px;" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$">
                                                        </div>
                                                        </div>
                                                        </div>
                                                        </div>
                                                        <div class="row">
                                                        <div class="col-md-12">
                                                        <div class="form-group form-group-default">
                                                        <label>Address</label>
                                                        <textarea name="address" required placeholder="xxxxxxxx" style="margin-bottom: 10px;" class="form-control change_inputs"><?php echo $address; ?></textarea>
                                                        </div>
                                                        </div>
                                                        </div>
                                                        <div class="row">
                                                        <div class="col-md-12">
                                                        <div class="form-group form-group-default">
                                                        <label>Gender</label>
                                                        <div class="row">
                                                        <?php 
                                                            if($gender == 'Male')
                                                            {?>



                                                              <div class="col-md-6" style="padding-left: 6px;">

                                                                <label><span class="fa fa-male"></span> Male : <input type="radio" checked="checked" name="gender" value="Male" class="change_inputs"></label>
                                                              </div>
                                                              <div class="col-md-6" style="padding-left: 6px;">

                                                                <label style="padding-left: 10px;margin-bottom: 10px;"><span class="fa fa-female"></span> Female : <input type="radio" name="gender" value="Female" class="change_inputs"> </label>

                                                              </div>
                                                              <?php
                                                            }else
                                                            if($gender == 'Female')
                                                            {?>



                                                              <div class="col-md-6" style="padding-left: 6px;"><label><span class="fa fa-male"></span> Male : <input type="radio"name="gender" value="Male" class="change_inputs"></label></div>

                                                              <div class="col-md-6" style="padding-left: 6px;"><label style="padding-left: 10px;margin-bottom: 10px;"><span class="fa fa-female"></span> Female : <input type="radio" checked="checked"  name="gender" value="Female" class="change_inputs"> </label></div>
                                                              <?php
                                                            }
                                                           ?>
                                                           </div>
                                                        </div>
                                                        </div>
                                                        

                                                        <div class="col-md-6" style="display: none;">
                                                        <div class="form-group form-group-default" style="height: 62px;">
                                                        <label>Are you already registered student?</label>
                                                        <div class="row">
                                                        <?php 
                                                            if($new == '1')
                                                            {?>



                                                              <div class="col-md-6" style="padding-left: 6px;">

                                                                <select class="form-control" name="new">
                                                                  <option value="1">Yes</option>
                                                                  <option value="0">No</option>
                                                                </select>

                                                              </div>
                                                              <?php
                                                            }

                                                            if($new == '0')
                                                            {?>

                                                              <div class="col-md-6" style="padding-left: 6px;">

                                                                <select class="form-control" name="new">
                                                                  <option value="0">No</option>
                                                                  <option value="1">Yes</option>
                                                                </select>

                                                              </div>

                                                              
                                                            <?php
                                                            }
                                                           ?>


                                                           </div>
                                                        </div>
                                                        </div>

                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="row m-t-25">
                                                        <div class="col-xl-6 p-b-10">
                                                        <p class="small-text hint-text">Click the Update button to change all the data you have changed. (ඔබ වෙනස් කල සියලු දත්තයන් වෙනස් කිරීමට Update බොත්තම ඔබන්න.)</p>
                                                        </div>
                                                        <div class="col-xl-6">
                                                        <button aria-label="" id="up_btn" class="btn btn-primary pull-right btn-lg btn-block" type="submit" name="update_data" value="<?php echo $stu_id; ?>">Update
                                                        </button>
                                                        </div>
                                                        </div>
                                                        </form>

                                              </div>

                                            </div>
                                          </div>
                                        </div>
                                      </div>
                        <?php
                          }
                        } 
                      }else
                      if(isset($_POST['hide_students']))
                      {
                        
                      }

                        

                        
                         ?>


                    </tbody>
                </table>
              </div>
            </div>
            </div>
            </div>
            </div>
            </div>
            </div>

<?php include('footer/footer.php'); ?>

<script type="text/javascript">
  function sortTable(table, order) {
    var asc   = order === 'asc',
        tbody = table.find('tbody');
    
    tbody.find('tr').sort(function(a, b) {
        if (asc) {
            return $('td:first', a).text().localeCompare($('td:first', b).text());
        } else {
            return $('td:first', b).text().localeCompare($('td:first', a).text());
        }
    }).appendTo(tbody);
}
</script>