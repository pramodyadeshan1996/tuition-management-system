

<div id="result_div">
    <div class="row">
        <div class="col-md-12">
            <div class="section-title">
                <h5 class="mb-2">ලියාපදිංචි වු පන්ති අවලංගු කිරීම</h5>
            </div>
        </div>
    </div>
    
   <!--Testimonials-->
    <div class="row mb-10">
        <div class="col-md-12">
            <div class="section-sub-title mb-10">
                <h6>Available Classes</h6>
            </div>
        </div>
    </div> 

    <div class="row">

        <?php 

            $student_session_id = $_SESSION['STU_ID']; //Get Student id using session

            //$class_day = date('l'); //Day

            $today_date = date('Y-m-d'); //Today Date

            $today__date_time = date('Y-m-d h:i:s A'); //Today Date & Time

            $new_check_transaction = 0;

            $this_year = date('Y'); //This Year

            $this_month = date('m'); //This Month

            $i = 0;
            $j = 0;

            $whatsapp_status = ''; //Whatsapp Status
            $tg_status = ''; //Telegram Status

            $zoom_status = ''; //Zoom Status


        //-------------------------------------------------------------------------------------------------------------------------------------
        /*
            Agribst System


           $subject_name = $_SESSION['SUBJECT'];

            $min = '0';
            $max = '18';

            $sql00101 = mysqli_query($conn,"SELECT * FROM `subject` WHERE `SH_LEVEL` = '$subject_name'"); //`LEVEL_ID` = '$sub_array[$i]'

            $ch = mysqli_num_rows($sql00101);

            if($ch > 0)
            {

            $_SESSION['not_registered_students'] = '1'; //Available data

            $j = $j+1;
            while($row00101 = mysqli_fetch_assoc($sql00101))
            {
                $subject00101 = $row00101['SUB_ID'];

                



                //`SUB_ID` = '$subject00101' AND  <=== Add For below Query to (`DAY`)
            */
        //-------------------------------------------------------------------------------------------------------------------------------------



            // WHERE `DAY` LIKE '%".$class_day."%'
            
            $sql0010 = mysqli_query($conn,"SELECT * FROM `classes`"); //check Today available classes

            $check_today_available_class = mysqli_num_rows($sql0010);

            if($check_today_available_class > 0)
            {

                while($row0010 = mysqli_fetch_assoc($sql0010))
                {
                   $class_id = $row0010['CLASS_ID']; //Class ID
                     $class_name = $row0010['CLASS_NAME']; //Class Name
                    $class_fees = $row0010['FEES']; //Class Fees
                    $class_start_time = $row0010['START_TIME']; //Class Start time
                    $class_end_time = $row0010['END_TIME']; //Class End time
                    $subject_id = $row0010['SUB_ID']; //Subject ID
                    $class_day = $row0010['DAY']; //Subject ID

                    $free_class = $row0010['FREE']; //Free Class
                    $limited_student = $row0010['LIMIT_STUDENT']; //Limit Student

                    $link_msg = ''; //message

                    $zoom_link = $row0010['ZOOM_LINK']; //Zoom Link
                    $whatsapp_link = $row0010['WHATSAPP_LINK']; //Whatsapp Link
                    $tg_link = $row0010['TELEGRAM_LINK']; //Telegram Link


                    $str = strtotime($class_start_time);
                    $class_start_time = date('h:i A',$str);

                    $str2 = strtotime($class_end_time);
                    $class_end_time = date('h:i A',$str2);




                    $class_teach_id = $row0010['TEACH_ID']; //Class Teach ID
                    $class_subject_id = $row0010['SUB_ID']; //Class Subject ID
                  
                  /*Bank Details*/

                    $ctbd_id = 0;

                    $result = mysqli_query($conn,"SELECT * FROM `class_teacher_bank_data` WHERE `TEACH_ID` = '$class_teach_id'");

                      while($row001 = mysqli_fetch_assoc($result))
                      {

                        $ctbd_id = $row001['C_T_B_D_ID'];
                      }



                    /*Bank Details*/

                    $sql0011 = mysqli_query($conn,"SELECT * FROM `teacher_details` WHERE `TEACH_ID` = '$class_teach_id' "); //check Classes for registered teachers details
                    while($row0011 = mysqli_fetch_assoc($sql0011))
                    {

                        $teacher_position = $row0011['POSITION']; //Teacher Position
                        $teacher_f_name = $row0011['F_NAME']; //Teacher First Name
                        $teacher_l_name = $row0011['L_NAME']; //Teacher Last Name
                        $teacher_gender = $row0011['GENDER']; //Teacher Gender
                        $teacher_qualification = $row0011['QUALIFICATION']; //Teacher Qualification

                        $teacher_picture = $row0011['PICTURE']; //Teacher Qualification

                        $teacher_full_name = $teacher_position.". ".$teacher_f_name." ".$teacher_l_name;


                        //If Teacher Picture zero,Check gender and gender to select picture 
                        if($teacher_picture == '0')
                        {
                            if($teacher_gender == 'Male')
                            {
                                $teacher_picture = 'b_teacher.jpg';
                            }else
                            if($teacher_gender == 'Female')
                            {
                                $teacher_picture = 'g_teacher.jpg';
                            }
                        }
                        //If Teacher Picture zero,Check gender and gender to select picture 


                    }

                    $sql0012 = mysqli_query($conn,"SELECT * FROM `institute` WHERE `INS_ID` = '1' "); //Institute Data assign to variables
                    while($row0012 = mysqli_fetch_assoc($sql0012))
                    {

                        $institute_name = $row0012['INS_NAME']; //Institute Name
                        $institute_tp = $row0012['INS_TP']; //Institute Tp
                        $institute_address = $row0012['INS_ADDRESS']; //Institute Address
                        $institute_mobile = $row0012['INS_MOBILE']; //Institute Mobile No
                        $institute_picture = $row0012['PICTURE']; //Institute Picture

                        $institute_free_period = $row0012['FREE']; // **Institute Free Period**

                        $class_limit = $row0012['CLZ_LIMIT']; // **Institute class limit**

                        $active_free_days = $row0012['FREE_WEEK']; // **Institute Free days active for last month paid student**
                        $no_free_days = $row0012['NO_FREE_WEEK']; // **Institute Setup No Free days for last month paid student**


                    }


                    $sql0013 = mysqli_query($conn,"SELECT * FROM `transactions` WHERE `CLASS_ID` = '$class_id' AND `TEACH_ID` = '$class_teach_id' AND `STU_ID` = '$student_session_id'"); //check Classes for registered teachers details

                    $check_transaction = mysqli_num_rows($sql0013);

                    $new_check_transaction = $new_check_transaction+$check_transaction;

                    //Check Available registered Classes?

                    

                    //Check Available registered Classes?


                    while($row0013 = mysqli_fetch_assoc($sql0013))
                    {
                        
                        $_SESSION['not_registered_students'] = $new_check_transaction; //Not registered student create a session and add value
                        

                        $student_free_card = $row0013['FREE_CLASS']; //Student Free Card

                        $student_transaction_id = $row0013['TRA_ID']; //Student Transaction ID  
                        $student_class_id = $row0013['CLASS_ID']; //Student Class ID

                        /* Check Payament Already to this month*/

                        $sql0014 = mysqli_query($conn,"SELECT * FROM `payment_data` WHERE `MONTH` = '$this_month' AND `YEAR` = '$this_year' AND `STU_ID` = '$student_session_id' AND `CLASS_ID` = '$student_class_id'"); //Check Student Paid?

                        $payment_status = mysqli_num_rows($sql0014); //Check Already Paid?

                        if($payment_status > 0)
                        {
                            while($row0014 = mysqli_fetch_assoc($sql0014))
                            {
                                $payment_id = $row0014['PAY_ID'];//Payment ID
                                $payment_time = $row0014['PAY_TIME']; //Paid Time
                                $payment_admin_decision = $row0014['ADMIN_SUBMIT']; //Administrator Decision
                                $payment_method = $row0014['PAY_METHOD']; //Payment Method
                                $payment_first_uploaded_bank_slip = $row0014['FILE_NAME']; //1st time uploaded bank slip
                                $payment_second_uploaded_bank_slip = $row0014['LAST_UPLOAD_FILE']; //2nd time uploaded bank slip

                            }  
                        }
                        
                        $sql0016 = mysqli_query($conn,"SELECT * FROM `classes` WHERE `CLASS_ID` = '$class_id'"); //check Classes for registered teachers details
                        while($row0016 = mysqli_fetch_assoc($sql0016))
                        {
                            $student_subject_id002 = $row0016['SUB_ID'];
                        }

                        /* Subject Details */

                        $sql0015 = mysqli_query($conn,"SELECT * FROM `subject` WHERE `SUB_ID` = '$student_subject_id002' "); //Institute Data assign to variables
                        while($row0015 = mysqli_fetch_assoc($sql0015))
                        {
                            $subject_name = $row0015['SUBJECT_NAME']; //Subject Name

                        }

                        /* Subject Details */


                /* ============================================ Check Show Status Message, Button =====================================*/


                        if($institute_free_period == 'Yes')
                        {
                            /* Enabled Free Period */

                            $status_message = 'Free'; //Status message
                            $status_icon = 'fas fa-gift'; //Status icon

                            $show_button = "00"; //Show Buttons (Free Period)

                            /* Enabled Free Period */

                        }else
                        if($institute_free_period == 'No')
                        {
                            /*Check Enabled Free Class*/

                            if($free_class > 0)
                            {

                                /* Enabled Free Class */

                                    $status_message = 'Free'; //Status message
                                    $status_icon = 'fas fa-gift'; //Status icon

                                    $show_button = "14"; //Show Buttons (Free Class)

                                /* Enabled Free Class */



                            }else
                            if($free_class == '0')
                            {
                                /*Check Enabled Free Class*/

                            /* Not Enabled Free Period but Enabled Free Card? */


                                if($student_free_card > 0)
                                {

                                    /* Enabled Free Card? */

                                    $status_message = 'Free'; //Status message
                                    $status_icon = 'fas fa-gift'; //Status icon

                                    $show_button = "01"; //Show Buttons (Free Card)

                                    /* Enabled Free Card? */

                                }else
                                if($student_free_card == '0')
                                {
                                    /* Check Paid Already */


                                    if($payment_status == '0')
                                    {

                                        /* Already Not Paid*/

                                        $status_message = 'Pay'; //Status message
                                        $status_icon = 'fas fa-coins'; //Status icon

                                        $show_button = "10"; //Show Buttons (Not Paid Yet)

                                        /* Already Not Paid*/

                                    }else
                                    if($payment_status > 0)
                                    { /* Already Paid*/

                                        if($payment_admin_decision == '0')
                                        {
                                            /* Admin Decided Pending to Bank Slip */

                                            $status_message = 'Pay'; //Status message
                                            $status_icon = 'fas fa-coins'; //Status icon
                                            $show_button = "11"; //Show Buttons (Not Paid Yet)

                                            /* Admin Decided Pending to Bank Slip */

                                        }else
                                        if($payment_admin_decision == '1')
                                        {
                                            /* Admin Decided Approved to Bank Slip */

                                            $status_message = 'Join'; //Status message
                                            $status_icon = 'fas fa-check-circle'; //Status icon
                                            $show_button = "12"; //Show Buttons (Not Paid Yet)


                                            /* Admin Decided Approved to Bank Slip */

                                        }if($payment_admin_decision == '2')
                                        {
                                            /* Admin Decided Reject to Bank Slip */

                                            $status_message = 'Pay'; //Status message
                                            $status_icon = 'fas fa-coins'; //Status icon
                                            $show_button = "13"; //Show Buttons (Not Paid Yet)

                                            /* Admin Decided Reject to Bank Slip */

                                        }

                                        

                                    }/* Already Paid*/
                                    
                                }/* Check Paid Already */
                            }//Free Class Enabled
                            
                        }/* Not Enabled Free Period but Enabled Free Card? */


            /* ============================================ Check Show Status Message, Button =====================================*/



/* ================================================== CODE BODY =================================================================*/
                    
                    ?>


                    <!--Payment Section-->
                    <div class="col-lg-6 col-md-12">
                        <div class="item">
                          <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                              <div class="image pt-15">
                                <center><img src="../teacher/images/profile/<?php echo $teacher_picture; ?>" alt="" style="border-radius: 80px;height: 150px;width: 150px;"></center>
                              </div>
                            </div>
                            <div class="col-md-3"></div>
                          </div>
                            
                            <div class="content">
                                <div class="date">
                                    <a style="cursor: pointer;background-color: <?php echo $ins_bg_color; ?>;">
                                      <i class="<?php echo $status_icon; ?> pt-10 pb-10"></i>
                                        <span class="mb-0"><?php echo $status_message; ?></span> <!-- Status Message -->
                                    </a>
                                </div>

                                <h5 class="mt-10" style="font-size: 20px;font-weight: bold;">
                                    <?php echo $class_name ?> 
                                </h5>

                                <h5 style="color: #757373;font-size: 17px;"><?php echo $teacher_full_name; ?> </h5>
                            
                                <h6 style="color: #757373;"><?php echo $class_start_time; ?> - <?php echo $class_end_time; ?> <span class="badge badge-default" style="background-color: <?php echo $ins_bg_color; ?>;color:white;"><?php echo $class_day; ?></span></h6>

                                    <h3 style="font-size: 30px;color:<?php echo $ins_bg_color; ?>;font-weight: bold;"><small style="font-size: 14px;font-weight: 600;">LKR </small> <?php echo number_format($class_fees); ?>/=</h3>
                                
                                <hr>

                                <button onclick="unregister_subject('<?php echo $student_session_id ?>','<?php echo $class_id; ?>');" class="btn btn-danger btn-block" data-toggle="tooltip" title="මෙම පන්තිය ලියාපදිංචියෙන් ඉවත්කර ගැනීම." style="font-weight: bold;">

                                    <i class="fas fa-times" style="color:white;"></i> Unregister
                                </button>

                            </div>
                        </div>
                    </div>
                                <?php 

                                    }
                                }

                            }

                                 ?>
        

      </div>
    </div>
      <script>
        $(function(){
            $("a.hidelink").each(function (index, element){
                var href = $(this).attr("href");
                $(this).attr("hiddenhref", href);
                $(this).removeAttr("href");
            });
            $("a.hidelink").click(function(){
                url = $(this).attr("hiddenhref");
                window.open(url, '_blank');
            })
        });
    </script>



    <script type="text/javascript">

        function goto_lms(tab,class_id)
        {
            $.ajax({
                type:"POST",
                url:"../student/query/check.php",
                data : {lms_tab_active:tab,class_id:class_id},
                success: function (data) {
                 // alert(data);
                    location.href = 'lms/lms.php?class_id='+data+''; 
                }
              });
        }

      </script>
<script type="text/javascript">
        function unregister_subject(student_id,class_id)
        {
            if(confirm('Are you sure unregister this class?'))
            {
                $.ajax({
                type:"POST",
                url:"../student/query/delete.php",
                data : {unregister_subject:student_id,class_id:class_id},
                success: function (data) {
                  
                    Swal.fire(
                      'Unregistered!',
                      'Registered Class has beed unregistered!',
                      'success'
                      
                    ).then((result) => {
                      if (result.isConfirmed) {

                            document.getElementById('remove_box'+class_id+'').style.visibility = "hidden";
                            location.reload();
                         
                      }
                    });
                    
                }
              });
            }
            

        }

    </script>