
<?php

$page = "Attendence Application";
$folder_in = '0';

  include('../header/header.php'); 
 ini_set( "display_errors", 0); 

 $admin_id = $_SESSION['ADMIN_ID'];
 

  if(isset($_POST['set_class']))
  {
      $teacher_id = $_POST['teacher_name'];
      $clz_id = $_POST['class_name'];

      $_SESSION['teach_id'] = $teacher_id;
      $_SESSION['clz_id'] = $clz_id;

  }

           ?>
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Crimson+Pro:wght@500&family=Lobster&display=swap" rel="stylesheet">

<div class="row" style="padding-top: 10px;">
 <div class="col-md-3"></div>

  <div class="col-md-6">
      <div class="col-md-12 bg-white" style="padding: 10px 20px 20px 20px;">
          
        <h2 class="text-center" style="font-family: 'Crimson Pro', serif;font-family: 'Lobster', cursive;"><span class="fa fa-search"></span> Search</h2>

          <div class="col-md-12" style="border-top: 1px solid #cccc;margin-bottom: 20px;"></div>

          <form action="" method="POST">
            <div class="row" style="padding-left: 4px;padding-right: 4px;">
              <div class="col-md-5">
                <div class="form-group form-group-default input-group">
                  <div class="form-input-group">
                    <label>Please Select Teacher Name</label>
                    <select class="form-control" name="teacher_name" id="teacher_name" required value="17">
                      
                      <?php 

                        if(empty($_SESSION['teach_id']))
                        { 
                          echo '<option value="">Select Teacher Name</option>';
                            $sql001 = mysqli_query($conn,"SELECT * FROM `teacher_details` ORDER BY `TEACH_ID` ASC");
                            while($row001 = mysqli_fetch_assoc($sql001))
                            {
                              $teach_id = $row001['TEACH_ID'];
                              $name = $row001['POSITION'].". ".$row001['F_NAME']." ".$row001['L_NAME'];
       
                              $sql002 = mysqli_query($conn,"SELECT * FROM `teacher_details` WHERE `TEACH_ID` = '$teach_id'");
                              while($row002 = mysqli_fetch_assoc($sql002))
                              {
                                $teach_id = $row002['TEACH_ID'];

                                echo '<option value="'.$teach_id.'">'.$name.'</option>';
                                
                              }

                            }
                        }else
                        if(!empty($_SESSION['teach_id']))
                        { 
                            $ses_teach = $_SESSION['teach_id'];

                            $sql001 = mysqli_query($conn,"SELECT * FROM `teacher_details` WHERE `TEACH_ID` = '$ses_teach'");
                            while($row001 = mysqli_fetch_assoc($sql001))
                            {
                              $teach_id = $row001['TEACH_ID'];
                              $name = $row001['POSITION'].". ".$row001['F_NAME']." ".$row001['L_NAME'];
       
                              $sql002 = mysqli_query($conn,"SELECT * FROM `teacher_details` WHERE `TEACH_ID` = '$teach_id'");
                              while($row002 = mysqli_fetch_assoc($sql002))
                              {
                                $teach_id = $row002['TEACH_ID'];

                                echo '<option value="'.$teach_id.'">'.$name.'</option>';
                                
                              }

                            }

                            $sql003 = mysqli_query($conn,"SELECT * FROM `teacher_details` WHERE `TEACH_ID` != '$ses_teach'");
                            while($row003 = mysqli_fetch_assoc($sql003))
                            {
                              $teach_id2 = $row003['TEACH_ID'];
                              $name2 = $row003['POSITION'].". ".$row003['F_NAME']." ".$row003['L_NAME'];
       
                              $sql004 = mysqli_query($conn,"SELECT * FROM `teacher_details` WHERE `TEACH_ID` = '$teach_id2'");
                              while($row004 = mysqli_fetch_assoc($sql004))
                              {
                                $teach_id2 = $row004['TEACH_ID'];

                                echo '<option value="'.$teach_id2.'">'.$name2.'</option>';
                                
                              }

                            }



                        }
                       ?>
                    </select>
                  </div>
                </div>
              </div>

              <div class="col-md-5">
                <div class="form-group form-group-default input-group">
                  <div class="form-input-group">
                    <label>Please Select Class Name</label>

                      <select class="form-control" name="class_name" id="class_name">
                        <?php 

                        if(empty($_SESSION['clz_id']))
                        { 
                          echo '<option value="">Select Class Name</option>';
                        }else
                        if(!empty($_SESSION['clz_id']))
                        { 
                            $ses_class = $_SESSION['clz_id'];
                            $ses_teach = $_SESSION['teach_id'];
       
                              $sql0022 = mysqli_query($conn,"SELECT * FROM `classes` WHERE `TEACH_ID` = '$ses_teach' AND `CLASS_ID` = '$ses_class'");
                              while($row0022 = mysqli_fetch_assoc($sql0022))
                              {
                                $clz_id22 = $row0022['CLASS_ID'];
                                $clz_name22 = $row0022['CLASS_NAME'];

                                echo '<option value="'.$clz_id22.'">'.$clz_name22.'</option>';
                                
                              }

                             $sql0023 = mysqli_query($conn,"SELECT * FROM `classes` WHERE `TEACH_ID` = '$ses_teach' AND `CLASS_ID` != '$ses_class'");
                              while($row0023 = mysqli_fetch_assoc($sql0023))
                              {
                                $clz_id23 = $row0023['CLASS_ID'];
                                $clz_name23 = $row0023['CLASS_NAME'];

                                echo '<option value="'.$clz_id23.'">'.$clz_name23.'</option>';
                                
                              }



                        }
                       ?>


                      </select>

                  </div>
                </div>
              </div>

              <div class="col-md-2">
                <button type="submit" class="btn btn-success btn-lg btn-block" name="set_class" style="font-size: 26px;padding: 11px 20px 11px 20px;"><i class="pg-icon" style="font-size: 30px;">tick_circle</i> Set</button>
              </div>

            </div>
          </form>

          <?php 

            if(empty($_SESSION['clz_id']) && empty($_SESSION['teach_id']))
            {
              $btn_disable = 'disabled';
              $alert = '<label class="text-danger"><span class="fa fa-warning"></span> Please select teacher and class names.</label>';
            }else
            if(!empty($_SESSION['clz_id']) && !empty($_SESSION['teach_id']))
            {
              $btn_disable = '';
              $alert = '';
            }
           ?>
                <div class="form-group form-group-default input-group">
                  <div class="form-input-group">
                    <label>Please Enter Barcode Scanner / Manual Input</label>
                    <input type="text" class="form-control"<?php echo $btn_disable; ?> id="student" placeholder="Registered ID / Student ID" autofocus style="padding: 20px 10px 20px 10px;font-size: 30px;">

                  </div>
                </div>
              <?php echo $alert; ?>

          <div id="results" style="border-top: 1px solid #cccc;">

            <div class="row bg-white">
              <div class="col-md-6">
                
                <div class="row">
                  <div class="col-md-12" style="padding-top: 10px;padding-bottom: 10px;">
                    <div class="col-md-12">
                      <img src="../../../admin/images/admin.png" height="100" width="100">
                    </div>
                    <h3>Pramodya Deshan</h3>
                    <table style="font-size: 16px;color: gray">
                      <tr>
                        <td>Register ID</td>
                        <td style="padding-right: 12px;padding-left: 12px;">-</td>
                        <td>PD000011</td>
                      </tr>
                      <tr>
                        <td>Telephone No</td>
                        <td style="padding-right: 12px;padding-left: 12px;">-</td>
                        <td>0779707981</td>
                      </tr>
                      <tr>
                        <td>Gender</td>
                        <td style="padding-right: 12px;padding-left: 12px;">-</td>
                        <td>Male</td>
                      </tr>
                      <tr>
                        <td>Attendence </td>
                        <td style="padding-right: 12px;padding-left: 12px;">-</td>
                        <td><small style="font-size: 20px;font-weight: bold;">5</small> Day(s)</td>
                      </tr>
                      <tr>
                        <td>Payment </td>
                        <td style="padding-right: 12px;padding-left: 12px;">-</td>
                        <td><label class="label label-success" style="font-size: 14px;">Yes</label></td>
                      </tr>
                    </table>
                  </div>

                  <div class="col-md-12">
                  </div>
                </div>  

                  
              </div>
              <div class="col-md-6" style="border-left: 1px solid #cccc;margin-top: 10px;margin-bottom: 10px;">
                <div class="col-md-12">
                  <h3 style="color: gray;"><u>Last Attendence</u></h3>
                  <div class="label label-success" style="font-size: 20px;margin-top: 4px;"><span class="fa fa-check-circle"></span> 2020-11-01 10:20:00 PM</div>
                  <div class="label label-danger" style="font-size: 20px;margin-top: 4px;"><span class="fa fa-times-circle"></span> 2020-11-08 10:20:00 PM</div>
                  <div class="label label-warning text-center" style="font-size: 20px;margin-top: 4px;">N/A</div>
                  <div class="label label-success" style="font-size: 20px;margin-top: 4px;"><span class="fa fa-check-circle"></span> 2020-11-22 10:20:00 PM</div>
                </div>
              </div>
            </div>
            <div class="text-center text-success" style="padding-top: 10px;border-top: 1px solid #cccc;">

              <label class="label label-success btn-block" style="padding: 6px 6px 6px 6px;font-size: 20px;">
                <span class="fa fa-check-circle"></span> Attendenced
              </label>
            </div>

            <div class="spinner" style="margin-top: 20%;display: none;">
              <div class="rect1"></div>
              <div class="rect2"></div>
              <div class="rect3"></div>
              <div class="rect4"></div>
              <div class="rect5"></div>
            </div>
          </div>
    </div>

  </div>

 <div class="col-md-3"></div>
</div>



  


<?php  include('../footer/footer.php'); ?>


<script type="text/javascript">
  
  $(document).ready(function(){

    $('#student').bind('change keyup blur',function(){
                  
        var student_id = $('#student').val();

         $.ajax({
          url:'../../../admin/query/check.php',
          method:"POST",
          data:{search_attendence_data:student_id},
          success:function(data)
          {
            //alert(data)
            $('#result').html(data);
          }
        });
  });

  });
</script>

<script type="text/javascript">
  
  $(document).ready(function(){

    $('#teacher_name').bind('change blur',function(){
                  
        var teacher_id = $('#teacher_name').val();

         $.ajax({
          url:'../../../admin/query/check.php',
          method:"POST",
          data:{check_teacher_att:teacher_id},
          success:function(data)
          {
            //alert(data)
            $('#class_name').html(data);
          }
        });
  });

  });
</script>

<script type="text/javascript">
  var timeDisplay = document.getElementById("time");


  function refreshTime() {
    var dateString = new Date().toLocaleString("en-US", {timeZone: "asia/colombo"});
    var formattedString = dateString.replace(", ", " - ");
    timeDisplay.innerHTML = formattedString;
  }

  setInterval(refreshTime, 1000);
  </script>