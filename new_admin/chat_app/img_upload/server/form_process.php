<?php

include('../../connect.php');
date_default_timezone_set("asia/colombo");
if(isset($_POST['submit'])){

 $lang = $_POST['lang'];
    $id = $_POST['id'];
    $date = date('Y-m-d');
    $time = date('h:i:s A');
    $act = date('Y-m-d H:i:s');
    
    $vi = $_FILES['up']['name'];

    mysqli_query($conn,"INSERT INTO `message`(`MESSAGE`, `REG_ID`,`SEND_DATE`,`SEND_TIME`,`ACTUAL_TIME`,`TYPE`) VALUES ('$vi','$id','$date','$time','$act','1')");
    move_uploaded_file($_FILES['up']['tmp_name'],"../../images/".$_FILES['up']['name']);        
    if($lang == 'sinhala')
    {?>
        <!DOCTYPE html>
            <html>
            <head>
                <title>සම්පූර්ණයි!</title>
                <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
                <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
                <link href="https://fonts.googleapis.com/css?family=Blinker&display=swap" rel="stylesheet">
            </head>
            <body style="font-family: 'Blinker', sans-serif;">
                <script type="text/javascript">
                    Swal.fire({
                      position: 'top-middle',
                      type: 'success',
                      title: 'පින්තූර උඩුගත කිරීම සම්පූර්ණයි!!',
                      showConfirmButton: false,
                      timer: 1400
                    })
                </script>
            </body>
            </html>

    <?php
       header('refresh:1.2;url=../../profile-si.php?id='.$id.' ');
    }else
    if($lang == 'english')
    {?>
        <!DOCTYPE html>
            <html>
            <head>
                <title>Success</title>
                <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
                <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
                <link href="https://fonts.googleapis.com/css?family=Blinker&display=swap" rel="stylesheet">
            </head>
            <body style="font-family: 'Blinker', sans-serif;">
                <script type="text/javascript">
                    Swal.fire({
                      position: 'top-middle',
                      type: 'success',
                      title: 'Images Upload Successfully',
                      showConfirmButton: false,
                      timer: 1400
                    })
                </script>
            </body>
            </html>

    <?php
     header('refresh:1.2;url=../../profile.php?id='.$id.' ');
    }
    

}