  <?php
 $stu_id = $_SESSION['STU_ID'];

 $s01 = mysqli_query($conn,"SELECT * FROM student_details WHERE STU_ID = '$stu_id'");
      while($row = mysqli_fetch_assoc($s01))
      {
        $st_name = $row['F_NAME']." ".$row['L_NAME']; //student name

        $dob = $row['DOB'];  //student DOB
        $email = $row['EMAIL'];  //student Email
        $address = $row['ADDRESS'];  //student Address
        $picture = $row['PICTURE']; // //student Picture
        $gender = $row['GENDER']; // //student Gender
        $tp = $row['TP']; // //student Telephone No

        $stt = strtotime($dob); //COnvert DOB
        $m = date('m',$stt); //Convert after DOB
        $d = date('d',$stt); //Convert after DOB
        $y = date('Y',$stt); //Convert after DOB

        $con_dob = $y."-".$m."-".$d;

         if($picture == '0')
         {
            if($gender == 'Male')
            {
              $picture = 'boy202015.png';
            }else
            if($gender == 'Female')
            {
              $picture = 'girl202015.png';
            }
         }

        

        $tp = $row['TP'];
        $have_sub = $row['NEW'];

        $ssql01 = mysqli_query($conn,"SELECT * FROM stu_login WHERE STU_ID = '$stu_id'");
        while($row001 = mysqli_fetch_assoc($ssql01))
        {
            $last_time = $row001['LOGIN_TIME'];
            $reg_id = $row001['REGISTER_ID'];

            $password = $row001['PASSWORD'];
        }

      }


    ?>

  <div class="row">

        <div class="col-lg-6">
          <div class="profile d-flex align-items-center">
              <div class="d-flex align-items-center">
                
                <div class="row">
                  <div class="col-md-2"></div>
                  <div class="col-md-8"><center><img src="../student/images/profile/<?php echo $picture; ?>" alt="" height="200px" width="200px"></center></div>
                  <div class="col-md-2"></div>
                </div>
              </div>

               
          </div>

          <div class="section-title mt-25">
                <h5> Security</h5>
          </div>

          <div class="col-md-12 mb-15" style="margin: 0;padding: 0;">


              <input type="hidden" id="student_id" value="<?php echo $stu_id; ?>"> <!-- Student ID -->


                <div style="padding-bottom: 8px;">
                    <div>
                        <p class="text-dark font-w-700 mb-0 text-muted"><i class="fas fa-asterisk base-color"> </i> Current Password: </p>

                        <label class="text-dark mb-0" id="show_hide_password" style="position: relative;width: 100%;margin-top: 8px;">
                          <input type="password" style="margin-left: 0;" placeholder="Current Password.." id="current_password" class="form-control" onkeyup="check_current_password();">

                          <a href=""><i class="fa fa-eye-slash" aria-hidden="true" style="font-size: 18px;color: black;position: absolute;right:12px;top:10px;"></i></a>


                          <small id="check_cu_psw" style="font-size: 20px;margin-top: 20px;"></small>

                        </label>

                    </div>
                </div>

                <div style="padding-bottom: 8px;">
                    <div id="show_hide_password2" style="position: relative;">
                        <p class="text-dark font-w-700 mb-0 text-muted"><i class="fas fa-asterisk base-color"> </i> New Password: </p>
                        <label class="text-dark mb-0" style="width: 100%;margin-top: 8px;">
                          <input type="password" style="margin-left: 0;" placeholder="New Password.." class="form-control" id="new_password" onkeyup="check_new_password();">

                          <a href=""><i class="fa fa-eye-slash" aria-hidden="true" style="font-size: 18px;color: black;position: absolute;right:12px;top:40px;"></i></a>


                        </label>
                    </div>
                </div>



                <div style="padding-bottom: 8px;">
                    <div id="show_hide_password3" style="position: relative;">
                        <p class="text-dark font-w-700 mb-0 text-muted"><i class="fas fa-asterisk base-color"> </i> Re-Password: </p>
                        <label class="text-dark mb-0" style="width: 100%;margin-top: 8px;">
                          <input type="password" style="margin-left: 0;" placeholder="Re-Password.." class="form-control" id="re_password" onkeyup="check_new_password();">

                          <a href=""><i class="fa fa-eye-slash" aria-hidden="true" style="font-size: 18px;color: black;position: absolute;right:12px;top:40px;"></i></a>


                        </label>

                        <small id="check_cu_psw" style="font-size: 20px;margin-top: 20px;"></small>

                    </div>
                </div>

                <small id="result" style="font-size: 20px;margin-top: 20px;"></small>


                <button type="submit" name="submit" class="btn btn-custom btn-block mt-2" onclick="change_password();" id="change_password_btn">
                        <i class="fas fa-user-edit"></i> Change Password
                </button>

                <center><a class="text-muted" onclick="clear_data();" style="text-align: center;font-size: 14px;cursor: pointer;"> Reset</a></center>
              </div>

              <script type="text/javascript">
                    
                    document.getElementById('change_password_btn').disabled = true;


                    document.getElementById('change_password_btn').style.cursor = 'not-allowed';

                    function check_current_password() {
                      
                      var current_password = document.getElementById('current_password').value;
                      var student_id = document.getElementById('student_id').value;


                      if(current_password !== '')
                      {
                          $.ajax({  //AJAX USING SENT INPUT DATA
                            url:"../student/query/check.php",  //check username in section/insert/insert.php
                            method:"POST",  
                            data:{check_current_password:current_password,student_id:student_id}, 
                            success:function(data)
                            {  
                              //alert(data)
                              if(data == 0)
                              {

                                  document.getElementById('check_cu_psw').innerHTML = '<label style="font-size: 14px;margin-top: 8px;" class="text-danger"><span class="fa fa-times-circle" ></span> Doesn&lsquo;t Match Your Current Password!</label>';


                                  document.getElementById('change_password_btn').disabled = true;

                                  document.getElementById('change_password_btn').style.cursor = 'not-allowed';
                              }

                              if(data > 0)
                              {

                                  document.getElementById('check_cu_psw').innerHTML = '<label style="font-size: 14px;margin-top: 8px;" class="text-success"><span class="fa fa-check-circle" ></span> Match Your Current Password!</label>';

                                  document.getElementById('change_password_btn').disabled = true;

                                  document.getElementById('change_password_btn').style.cursor = 'not-allowed';
                              }
                            }
                        });
                      }

                      if(current_password == '')
                      {
                          document.getElementById('check_cu_psw').innerHTML = '';
                      }

                    }

                    function check_new_password()
                    {
                        var new_password = document.getElementById('new_password').value;
                        var re_password = document.getElementById('re_password').value;

                        if(new_password == '' && re_password == '')
                        {
                            document.getElementById('result').innerHTML = '<label style="font-size: 14px;margin-top: 8px;" class="text-danger"><span class="fa fa-times-circle" ></span> Please Enter New and Re-Password!</label>';
                        }

                        if(new_password == re_password)
                        {
                            document.getElementById('result').innerHTML = '<label style="font-size: 14px;margin-top: 8px;" class="text-success"><span class="fa fa-check-circle" ></span> Match Your New Password!</label>';
                            
                            document.getElementById('change_password_btn').disabled = false;

                            document.getElementById('change_password_btn').style.cursor = 'pointer';

                        }else
                        if(new_password !== re_password)
                        {

                            document.getElementById('result').innerHTML = '<label style="font-size: 14px;margin-top: 8px;" class="text-danger"><span class="fa fa-times-circle" ></span> Doesn&lsquo;t Match Your New Password!</label>';

                            document.getElementById('change_password_btn').disabled = true;
                            
                            document.getElementById('change_password_btn').style.cursor = 'not-allowed';
                        }
                        
                    }

                    function change_password(){

                      var new_password = document.getElementById('new_password').value;

                      var student_id = document.getElementById('student_id').value;

                      $.ajax({  //AJAX USING SENT INPUT DATA
                          url:"../student/query/update.php",  //check username in section/insert/insert.php
                          method:"POST",  
                          data:{change_password:student_id,new_password:new_password}, 
                          success:function(data)
                          {  
                            Swal.fire(
                              'Changed Successfully!',
                              'Your Password has beed Changed!',
                              'success'
                              
                            );

                            document.getElementById('current_password').value = '';
                            document.getElementById('new_password').value = '';
                            document.getElementById('re_password').value = '';

                            document.getElementById('result').innerHTML = "";
                            document.getElementById('check_cu_psw').innerHTML = "";

                          }
                      });
                    }

                    function clear_data() 
                    {
                        document.getElementById('current_password').value = '';
                        document.getElementById('new_password').value = '';
                        document.getElementById('re_password').value = '';

                        document.getElementById('result').innerHTML = "";
                        document.getElementById('check_cu_psw').innerHTML = "";
                    }

                </script>


        </div>
        <div class="col-lg-6 pb-10" style="margin-top: 4px;">
          <form>
          <div class="section-title">
            <input type="hidden" id="student_id" value="<?php echo $stu_id; ?>">

            
            <h3 class="text-dark">
                  
                  <?php echo $st_name; ?>

            </h3>

          </div>
            <div class="contact-info">
                <div class="item" style="margin-bottom: 2px;">
                    <div class="details">
                        <p class="text-dark font-w-700 mb-0 text-muted"><i class="fa fa-phone base-color"></i> Phone: </p>
                        <p class="text-dark mb-0" style="margin-top: 8px;">
                          <input type="text" value="<?php echo $tp; ?>" class="form-control" id="tp" required maxlength="10" minlength="10" placeholder="Enter Your Telephone No.." pattern="[0-9]{10}" id="student_tp" onchange="change_input()" onkeyup="change_input()" onkeypress="return isNumberKey(event)">
                        </p>
                    </div>
                </div>
                <label id="error_tp"></label>
                <div class="item">
                    <div>
                        <p class="text-dark font-w-700 mb-0 text-muted"><i class="fas fa-envelope base-color"> </i> Email: </p>
                        <p class="mb-0" style="margin-top: 8px;">
                            <input type="email" style="margin-left: 0;" value="<?php echo $email; ?>" class="form-control"placeholder="Enter Your Email Address.." id="student_email" onchange="change_input()" onkeyup="change_input()">
                        </p>
                    </div>
                </div>

                <label></label>

                <div class="item">
                    <div>
                        <p class="text-dark font-w-700 mb-0 text-muted"><i class="fas fa-map-marker-alt base-color"> </i> Address: </p>
                        <p class="text-dark mb-0" style="margin-top: 8px;">

                          <textarea style="margin-left: 0;" class="form-control" placeholder="Enter Your Address.." id="student_address" onchange="change_input()" onkeyup="change_input()"><?php echo $address; ?></textarea>

                        </p>
                    </div>
                </div>
                
                <label></label>

                <div class="item">
                    <div>
                        <p class="text-dark font-w-700 mb-1 text-muted"><i class="fas fa-calendar base-color"> </i> Date Of Birth: </p>
                        <p class="text-dark mb-0" style="margin-top: 8px;">
                          <input type="date" style="margin-left: 0;" value="<?php echo $con_dob; ?>" class="form-control" placeholder="Enter Your Date Of Birth.." id="student_dob" onchange="change_input()" onkeyup="change_input()">
                        </p>
                    </div>
                </div>

                <label></label>

                <div class="item mb-10">
                    <div>
                        <p class="text-dark font-w-700 mb-0 text-muted"><i class="fas fa-map-marker-alt base-color"> </i> Gender: </p>
                        <p class="text-dark mb-0" style="margin-top: 8px;">
                          <select class="form-control" id="student_gender" onchange="change_input()" onkeyup="change_input()">
                            <?php

                              if($gender == 'Male')
                              {
                                echo '<option value="'.$gender.'">'.$gender.'</option>';
                                echo '<option value="Female">Female</option>';
                              }else
                              if($gender == 'Female')
                              {
                                echo '<option value="'.$gender.'">'.$gender.'</option>';
                                echo '<option value="Male">Male</option>';
                              }


                             ?>
                          </select>
                        </p>
                    </div>
                </div>
                <label style="display: none;" id="status_msg"></label>
                <button type="button" name="submit" class="btn btn-custom btn-block" onclick="update_profile();" id="change_profile_btn">
                        <i class="fas fa-check-circle"></i> Update Profile
                </button>
            </div>
          </form>
        </div>
    </div>



    <!-- Show Password -->
    <script type="text/javascript">
   $(document).ready(function() {
    $("#show_hide_password a").on('click', function(event) {
        event.preventDefault();
        if($('#show_hide_password input').attr("type") == "text"){
            $('#show_hide_password input').attr('type', 'password');
            $('#show_hide_password i').addClass( "fa-eye-slash" );
            $('#show_hide_password i').removeClass( "fa-eye" );
        }else if($('#show_hide_password input').attr("type") == "password"){
            $('#show_hide_password input').attr('type', 'text');
            $('#show_hide_password i').removeClass( "fa-eye-slash" );
            $('#show_hide_password i').addClass( "fa-eye" );
        }
    });


    $("#show_hide_password2 a").on('click', function(event) {
        event.preventDefault();
        if($('#show_hide_password2 input').attr("type") == "text"){
            $('#show_hide_password2 input').attr('type', 'password');
            $('#show_hide_password2 i .fa-eye-slash').addClass( "fa-eye-slash" );
            $('#show_hide_password2 i .fa-eye-slash').removeClass( "fa-eye" );
        }else if($('#show_hide_password2 input').attr("type") == "password"){
            $('#show_hide_password2 input').attr('type', 'text');
            $('#show_hide_password2 i .fa-eye-slash').removeClass( "fa-eye-slash" );
            $('#show_hide_password2 i .fa-eye-slash').addClass( "fa-eye" );
        }
    });


    $("#show_hide_password3 a").on('click', function(event) {
        event.preventDefault();
        if($('#show_hide_password3 input').attr("type") == "text"){
            $('#show_hide_password3 input').attr('type', 'password');
            $('#show_hide_password3 i .fa-eye-slash').addClass( "fa-eye-slash" );
            $('#show_hide_password3 i .fa-eye-slash').removeClass( "fa-eye" );
        }else if($('#show_hide_password3 input').attr("type") == "password"){
            $('#show_hide_password3 input').attr('type', 'text');
            $('#show_hide_password3 i .fa-eye-slash').removeClass( "fa-eye-slash" );
            $('#show_hide_password3 i .fa-eye-slash').addClass( "fa-eye" );
        }
    });

});
</script>
<!-- Show Password -->

<!-- Update Profile Details -->

<script type="text/javascript">

    document.getElementById('change_profile_btn').disabled = true;
    document.getElementById('change_profile_btn').style.cursor = 'not-allowed';

    function change_input() {

      document.getElementById('change_profile_btn').disabled = false;
      document.getElementById('change_profile_btn').style.cursor = 'pointer';

    }

    function update_profile()
    {

      var student_id = document.getElementById('student_id').value;
      var student_tp = document.getElementById('tp').value;
      var student_email = document.getElementById('student_email').value;
      var student_address = document.getElementById('student_address').value;
      var student_dob = document.getElementById('student_dob').value;
      var student_gender = document.getElementById('student_gender').value;

      if(student_tp.length < 10)
      {
      
        document.getElementById('error_tp').innerHTML = '<label class="text-danger"><span class="fa fa-times-circle"></span> Please check your Telephone No..</label>';

      }else
      {

        $.ajax({
          url:'../student/query/update.php',
          method:"POST",
          data:{update_student_data:student_id,student_dob:student_dob,student_email:student_email,student_tp:student_tp,student_address:student_address,student_gender:student_gender},
          success:function(data)
          {
            Swal.fire(
              'Changed!',
              'Your profile details have been updated!',
              'success'
            )

            document.getElementById('error_tp').innerHTML = '';
            
          }
        });
      }
    }
  </script>

<!-- Update Profile Details -->

<script language=Javascript>
      <!--
      function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
      }
      //-->
   </script>