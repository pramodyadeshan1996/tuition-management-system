<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/png" sizes="16x16" href="../images/logo.png">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

	<title>403 ERROR</title>

	<!-- Google font -->
	<script src='https://kit.fontawesome.com/a076d05399.js'></script>
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Nunito:wght@600&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
	<!-- Custom stlylesheet -->

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

</head>

<body style="font-family: 'Nunito', sans-serif;">

	<div id="notfound">
		<div class="notfound" style="text-align: center;padding-top: 6%;">
			<div class="notfound-404">
				<h1 style="font-size: 100px;">
				<span class="fas fa-frown"></span> <br>
				Oops!</h1>
			</div>
			<h2>403 - Access to the requested resource is forbidden</h2>
			<p>We do not provide direct access to any web page. We apologize for any inconvenience.</p>
			<p>(අපි කිසිදු වෙබ් පිටුවකට සෘජු ප්‍රවේශයක් සපයන්නේ නැත.)</p>
		</div>
	</div>

</body><!-- This templates was made by Colorlib (https://colorlib.com) -->

</html>
