<?php
  $page = "Payment Status";
  $folder_in = '0';

      include('header/header.php');

      $teach_id = $_SESSION['TEACH_ID'];
      
?>




  <div class="col-md-12" style="margin-top: 8%;" onload="change_teacher();">
    <div class="row" style="padding-top: 10px;">
      <div class="col-md-4"></div>
      <div class="col-md-4 bg-white" style="border:1px solid #cccc;height: auto;">
      
        <div class="col-md-12" style="border-bottom: 1px solid #cccc;">
          <div class="row">

              <div class="col-md-9" style="margin-bottom: 10px;"><h2>Payment Status</h2></div>
              <div class="col-md-3" style="padding-top: 20px;">
              </div>

          </div>
        </div>
                <div class="col-md-12" style="padding: 10px 10px 10px 10px;">
                  <form action="clz_fees_report.php" method="POST" target="_blank">
                    <div class="row">

                      <div class="col-md-12" style="padding-left: 8px;display: none">
                        <div class="col-md-12" style="margin-top: 10px;">

                          <input type="hidden" value="<?php echo $teach_id; ?>" name="teacher_id" id="teacher_id">
                        </div>
                        </div>

                        <div class="col-md-12" style="margin-top: 10px;">
                          <label style="line-height: 0.2;">Class Name</label>
                          <select class="form-control" name="class_id" required id="class_id">
                            
                            <?php 

                              $sql0035 = mysqli_query($conn,"SELECT * FROM `classes` WHERE `TEACH_ID` = '$teach_id'");

                                echo '<option value="all">All Class</option>';

                                if(mysqli_num_rows($sql0035)>0)
                                {
                                   
                                    while($row0035 = mysqli_fetch_assoc($sql0035))
                                    {
                                        $class_id = $row0035['CLASS_ID'];
                                        $class_name = $row0035['CLASS_NAME'];

                                      echo '<option value="'.$class_id.'">'.$class_name.'</option>';
                                    }

                                }else
                                if(mysqli_num_rows($sql0035) == '0')
                                {
                                      echo '<option value="">Not Added Classes</option>';

                                }

                             ?>
                          </select>
                        </div>

                        <div class="col-md-12" style="margin-top: 10px;">

                          <label style="line-height: 0.2;">Year</label>
                          <select class="form-control" name="year" required id="year">
                              <?php 
                                    
                                    $year = date('Y');
                                    $few = strtotime("-6 years");
                                    $few_5year = date('Y',$few);
                                    for($year;$year>$few_5year;$year--)
                                    {
                                      echo '<option value="'.$year.'">'.$year.'</value>';
                                    }

                                  ?>
                            </select>
                          </div>

                        <div class="col-md-12" style="margin-top: 10px;">
                          <label style="line-height: 0.2;">Month</label>
                          <select class="form-control" name="month" id="month" required>
                            <?php 
                                  
                                  for($m=1;$m<13;$m++)
                                  {
                                     $month = $m;
                                     $mon_name = date('F', mktime(0, 0, 0, $month, 10)); // March
                                    echo '<option value="'.$m.'">'.$mon_name.'</value>';
                                  }

                                ?>
                          </select>
                        </div>

                      <div class="col-md-12" style="margin-top: 10px;">
                          <label style="line-height: 0.2;">Payment Type</label>
                        <select class="form-control" name="pay_type" required id="pay_type" onchange="click_search_btn();">
                          <option value="">Payment Type</option>
                          <option value="1">Paid Students</option>
                          <option value="0">Not Paid Students</option>
                        </select>
                      </div>
                       
                      <div class="col-md-12" style="margin-top: 10px;font-size: 18px;">
                        <button type="submit" class="btn btn-success btn-block" style="border-radius: 80px;padding: 10px 10px 10px 10px;" name="print_class_fees"><i class="fa fa-print"></i> &nbsp;Print Report</button>

                        <center><a href="dashboard.php" class="text-center" style="font-size: 14px;">Back</a></center>
                      </div>
                    </div>
                    </form>
                  </div>
          </div>
        </div>
        <div class="col-md-4"></div>
        </div>





<script type="text/javascript">
  $('.save_btn').click(function(){
    
    var upload_btn0 = $('#selectedFile0').val();
    var upload_btn = $('#sel_file').val();
      
    if(empty(upload_btn0))
    {
      alert('Please select your audio file.');
    }else
    if(empty(upload_btn))
    {
      alert('Please select your document file.');
    }
   });
</script>

<script type="text/javascript">
  $('.navi').click(function(){
    $('.frm')[0].reset();
      
   });
</script>

<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
<script>
$(document).ready(function(){
  $("#search-table").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

<script>
$(document).ready(function(){
  $("#search-table33").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable33").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

<!-- search data -->

<!-- no data message/no found data show in search-->

<script type="text/javascript">
  $(document).ready(function () {

    (function ($) {

        $('##search-table2').keyup(function () {
            var rex = new RegExp($(this).val(), 'i');
            $('#myTable2').hide();
            $('#myTable2').filter(function () {
                return rex.test($(this).text());
            }).show();
            $('.no-data').hide();
            if($('#myTable2:visible').length == 0)
            {
                $('.no-data').show();
            }

        })

    }(jQuery));

});
</script>



<script type="text/javascript">
  function click_search_btn() 
  {
    var teacher_id = document.getElementById('teacher_id').value;
    var class_id = document.getElementById('class_id').value;
    var year = document.getElementById('year').value;
    var month = document.getElementById('month').value;
    var pay_type = document.getElementById('pay_type').value;

     $.ajax({
      url:'../admin/query/check.php',
      method:"POST",
      data:{search_class_students:class_id,teacher_id:teacher_id,year:year,month:month,pay_type:pay_type},
      success:function(data)
      {
        //alert(data);
        document.getElementById('myTable').innerHTML = data;

      }
    });
  }
</script>



<?php include('footer/footer.php'); ?>


