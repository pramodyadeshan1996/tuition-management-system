
  <div class="row" style="margin-top: 4px;margin-bottom: 8px;">
      <div class="col-md-4">
          <div class="section-title">
              <h5>ගෙවීම්</h5>
          </div>


            <div class="section-sub-title mb-10">
                <h6 id="sub_text"><?php echo $tab_name; ?></h6>
            </div>

      </div>


      <style type="text/css">
        .switch {
    display: inline-block;
    position: relative;
    width: 50px;
    height: 30px;
    border-radius: 20px;
    background: #dfd9ea;
    transition: background 0.28s cubic-bezier(0.4, 0, 0.2, 1);
    vertical-align: middle;
    cursor: pointer;
}
.switch::before {
    content: '';
    position: absolute;
    top: 2px;
    left: 2px;
    width: 22px;
    height: 25px;
    background: #fafafa;
    border-radius: 50%;
    transition: left 0.28s cubic-bezier(0.4, 0, 0.2, 1), background 0.28s cubic-bezier(0.4, 0, 0.2, 1), box-shadow 0.28s cubic-bezier(0.4, 0, 0.2, 1);
}
.switch:active::before {
    box-shadow: 0 2px 8px rgba(0,0,0,0.28), 0 0 0 20px rgba(128,128,128,0.1);
}
input:checked + .switch {
    background: <?php echo $ins_bg_color; ?>;
}
input:checked + .switch::before {
    left: 27px;
    background: #fff;
}
input:checked + .switch:active::before {
    box-shadow: 0 2px 8px rgba(0,0,0,0.28), 0 0 0 20px rgba(0,150,136,0.2);
}
      </style>
      <div class="col-md-8">
        <div class="row">
          <div class="col-md-4" style="margin-top: 9px;">

            <input type="hidden" id="student_id" value="<?php echo $student_session_id; ?>">

            <select class="form-control" id="year">
              <option value="">Select Year</option>
              <?php 
                  $str = strtotime('-5 year');
                  $few_year = date('Y',$str);

                  $str2 = strtotime('5 year');
                  $last_five_year = date('Y',$str2);

                  for($last_five_year;$last_five_year>$few_year;$last_five_year--)
                  {
                      echo '<option value="'.$last_five_year.'">'.$last_five_year.'</option>';
                  }
               ?>
            </select>
          </div>
          <div class="col-md-4" style="margin-top: 9px;">
            <select class="form-control" id="month">
              <option value="">Select Month</option>
               <?php 

                  $month_num = date('m');
                  $months001 = array (1=>'January',2=>'February',3=>'March',4=>'April',5=>'May',6=>'June',7=>'July',8=>'August',9=>'September',10=>'October',11=>'November',12=>'December');
                    $month_name001 = $months001[(int)$month_num];

                  for ($i=1; $i < 13 ; $i++) { 

                      $months = array (1=>'January',2=>'February',3=>'March',4=>'April',5=>'May',6=>'June',7=>'July',8=>'August',9=>'September',10=>'October',11=>'November',12=>'December');
                      $month_name = $months[(int)$i];

                      if($i<10)
                      {
                        $i = "0".$i;
                      }

                      echo '<option value="'.$i.'">'.$month_name.'</option>';
                    
                      
                  }
                  
               ?>
            </select>
          </div>
          <div class="col-md-3">
            <center>
              <div class="row">
                <div class="col-md-6 pull-right;" style="padding-top: 8px;"> 
                      <button class="btn btn-default btn-block" id="search_btn" onclick="search_payment();" style="outline: none;border: 1px solid #cccc;padding: 12px 10px 12px 10px;border-radius: 80px;"><span class="fas fa-search"></span></button>
                  </div> 
                <div class="col-md-6" style="padding-top: 8px;">
                  
                    <input type="checkbox" hidden="hidden" id="select_pay_type" onchange="change_pay_type(<?php echo $student_session_id; ?>);" <?php echo $switch; ?>>
                    <label class="switch" style="margin-top: 6px;" for="select_pay_type"></label>
                </div> 
              </div>



            </center>
          </div>
        </div>


      </div>
  </div>

<!-- Paid classes -->

<div  id="search_result" <?php echo $tab01; ?>>
  <div class="row mb-20">
      <!-- item 1-->

      <?php 
      
      $student_session_id = $_SESSION['STU_ID']; //student ID

      $current_year = date('Y');

      $current_month = date('m');

      $sql001 = mysqli_query($conn,"SELECT * FROM `payment_data` WHERE `YEAR` >= '$current_year' AND `MONTH` >= '$current_month' AND `STU_ID` = '$student_session_id'");

      $check001 = mysqli_num_rows($sql001); //Check Available Payments

      if($check001 > 0)
      {


          while($row001 = mysqli_fetch_assoc($sql001))
          {
            $payment_id = $row001['PAY_ID'];
            $payment_fees = $row001['FEES'];
            $class_id = $row001['CLASS_ID'];
            $payment_method = $row001['PAY_METHOD'];
            $payment_time = $row001['PAY_TIME'];

            $payment_first_uploaded_bank_slip = $row001['FILE_NAME'];

            $payment_year = $row001['YEAR'];
            $payment_month = $row001['MONTH'];

            $months001 = array (1=>'ජනවාරි',2=>'පෙබරවාරි',3=>'මාර්තු',4=>'අප්‍රේල්',5=>'මැයි',6=>'ජුනි',7=>'ජුලි',8=>'අගෝස්තු',9=>'සැප්තැම්බර්',10=>'ඔක්තෝම්බර්',11=>'නොවැම්බර්',12=>'දෙසැම්බර්');
            $month_name002 = $months001[(int)$payment_month];


            $payment_admin_decision = $row001['ADMIN_SUBMIT']; //Administrator Decision

            $sql0012 = mysqli_query($conn,"SELECT * FROM `classes` WHERE `CLASS_ID` = '$class_id' "); //Class Details
            while($row0012 = mysqli_fetch_assoc($sql0012))
            {
                $class_name = $row0012['CLASS_NAME']; //Class Name
                $teacher_id = $row0012['TEACH_ID']; //Class Name

            }

            $sql0013 = mysqli_query($conn,"SELECT * FROM `teacher_details` WHERE `TEACH_ID` = '$teacher_id' "); //check Classes for registered teachers details
            while($row0013 = mysqli_fetch_assoc($sql0013))
            {
                $teacher_position = $row0013['POSITION']; //Teacher Position
                $teacher_f_name = $row0013['F_NAME']; //Teacher First Name
                $teacher_l_name = $row0013['L_NAME']; //Teacher Last Name

                $teacher_full_name = $teacher_position.". ".$teacher_f_name." ".$teacher_l_name; //Teacher Full Name

            }

            $sql0014 = mysqli_query($conn,"SELECT * FROM `institute` WHERE `INS_ID` = '1' "); //Institute Data assign to variables
            while($row0014 = mysqli_fetch_assoc($sql0014))
            {

                $institute_free_period = $row0014['FREE']; // **Institute Free Period**

            }



            $sql0015 = mysqli_query($conn,"SELECT * FROM `transactions` WHERE `CLASS_ID` = '$class_id' AND `TEACH_ID` = '$class_teach_id' AND `STU_ID` = '$student_session_id'"); //check Classes for registered teachers details

            while($row0015 = mysqli_fetch_assoc($sql0015))
            {

                $student_free_card = $row0015['FREE_CLASS']; //Student Free Card

                $student_transaction_id = $row0015['TRA_ID']; //Student Transaction ID  
                $student_class_id = $row0015['CLASS_ID']; //Student Class ID

              }


            /* ============================================ Check Show Status Message, Button =====================================*/


                        if($institute_free_period == 'Yes')
                        {
                            /* Enabled Free Period */

                            $status_message = 'Free'; //Status message
                            $status_icon = 'fas fa-gift'; //Status icon

                            $show_button = "00"; //Show Buttons (Free Period)

                            /* Enabled Free Period */

                        }else
                        if($institute_free_period == 'No')
                        {
                            /* Not Enabled Free Period but Enabled Free Card? */


                            if($student_free_card > 0)
                            {

                                /* Enabled Free Card? */

                                $status_message = 'Free'; //Status message
                                $status_icon = 'fas fa-gift'; //Status icon

                                $show_button = "01"; //Show Buttons (Free Card)

                                /* Enabled Free Card? */

                            }else
                            if($student_free_card == '0')
                            {
                                /* Check Paid Already */


                                if($check001 == '0') //Payment data table this student paid status
                                {

                                    /* Already Not Paid*/

                                    $status_message = 'Pay'; //Status message
                                    $status_icon = 'fas fa-coins'; //Status icon

                                    $show_button = "10"; //Show Buttons (Not Paid Yet)

                                    /* Already Not Paid*/

                                }else
                                if($check001 > 0) //Payment data table this student paid status
                                {

                                    /* Already Paid*/

                                    if($payment_admin_decision == '0')
                                    {
                                        /* Admin Decided Pending to Bank Slip */

                                        $status_message = 'Pay'; //Status message
                                        $status_icon = 'fas fa-coins'; //Status icon
                                        $show_button = "11"; //Show Buttons (Not Paid Yet)

                                        /* Admin Decided Pending to Bank Slip */

                                    }else
                                    if($payment_admin_decision == '1')
                                    {
                                        /* Admin Decided Approved to Bank Slip */

                                        $status_message = 'Join'; //Status message
                                        $status_icon = 'fas fa-check-circle'; //Status icon
                                        $show_button = "12"; //Show Buttons (Not Paid Yet)


                                        /* Admin Decided Approved to Bank Slip */

                                    }if($payment_admin_decision == '2')
                                    {
                                        /* Admin Decided Reject to Bank Slip */

                                        $status_message = 'Pay'; //Status message
                                        $status_icon = 'fas fa-coins'; //Status icon
                                        $show_button = "13"; //Show Buttons (Not Paid Yet)

                                        /* Admin Decided Reject to Bank Slip */

                                    }

                                    /* Already Paid*/

                                }


                                /* Check Paid Already */
                            }

                            /* Not Enabled Free Period but Enabled Free Card? */
                        }


            /* ============================================ Check Show Status Message, Button =====================================*/






            ?>
                <!-- Payment Box -->

                <div class="col-lg-6" style="cursor: pointer;margin-top: 15px;">

                  <div class="item d-flex align-items-center">
                    <div class="circle">
                          <span><h2 class="base-color" style="padding-top: 9px;text-transform: uppercase;"><?php echo substr($class_name, 0,1) ?></h2></span>
                      </div>
                      <div class="content">

                          <h5 class="text-dark" style="text-transform: capitalize;"><?php echo $class_name; ?></h5>
                          <label style="text-transform: capitalize;"><?php echo $teacher_full_name; ?></label>
                          <p class="text-dark mb-0">
                            <b><?php echo $payment_time; ?></b>
                            
                            <br>

                            <h6 style="margin-top:10px;color:#e74c3c;font-weight: bold;"><?php echo $payment_year; ?> - <?php echo $month_name002; ?></h6>
                            <label class="badge badge-success mt-10" style="text-shadow: uppercase;"><?php echo $payment_method; ?></label>
                            <p style="font-weight: bold;font-size: 22px;"><span class="fa fa-check-circle"></span> LKR <?php echo number_format($payment_fees); ?>/=</p>

                            <?php 

                                    if($show_button == '11')
                                    {
                                        //Pending
                                        ?>
                                        <label class="text-danger" style="font-size:13px;margin-bottom:14px;font-weight: bold;"><i class="fas fa-clock"></i>&nbsp;ඔබේ ඇතුලත් කිරීම නිවැරැදි දැයි පරීක්ෂා කිරීමට කාර්යාලයීය පැය 4 ක් ගතවනු ඇත</label>

                                        <button type="submit" class="btn btn-block" style="background-color: #f21e4e;color: white;font-weight: bold"><i class="fas fa-circle-notch fa-spin"></i>&nbsp;Checking</button>

                                        <?php
                                    }else
                                    if($show_button == '13')
                                    {
                                        //Rejected
                                        ?>
                                        <label class="text-danger" style="font-size:13px;margin-bottom:14px;font-weight: bold;"><i class="fas fa-clock"></i>&nbsp;ඔබ එවන ලද රිසිට්පතෙහි යම් දෝෂයක් පවතී.නැවත පරික්ෂා කර Upload කරන්න.</label>

                                        <button type="submit" class="btn btn-block" onclick="rejected_upload('<?php echo $payment_first_uploaded_bank_slip; ?>','<?php echo $payment_id; ?>')" style="background-color: #f21e4e;color: white;font-weight: bold" data-toggle="tooltip" title="ඔබ එවන ලද රිසිට්පතෙහි යම් දෝෂයක් පවතී.නැවත පරික්ෂා කර Upload කරන්න."><i class="fas fa-plus"></i>&nbsp;Upload Again</button>

                                        <?php
                                    }else
                                    if($show_button == '12')
                                    {
                                        //Paid
                                        ?>

                                        <button type="submit" class="btn btn-block" style="background-color: #1abc9c;color: white;font-weight: bold"><i class="fas fa-check-circle"></i>&nbsp;Payment Successfully</button>

                                        <?php
                                    }

                             ?>

                          </p>
                           
                      </div>
                    </div>
                </div>
                <!-- Payment Box -->

            <?php

          }
        }else
        if($check001 == '0')
        {
          /*Empty Payments*/

            ?>
                <div class="col-md-12 pull-center">
                        <center><img src="assets/img/icon/unavailable.svg" style="width: 250px;height: 250px;"></center>
                            <h6 class="mt-20 text-muted text-center"><span class="fas fa-search"></span> Payments Not Found!</h6>
                        
                </div></center>


            <?php

            /*Empty Payments*/
        }

       ?>

  </div>
</div>
  <!-- Paid classes -->


  <!-- Payment Classes -->
<div id="show_payment" <?php echo $tab02; ?>>
  <div class="row ">

        <?php 
      
      $student_session_id = $_SESSION['STU_ID']; //student ID

      $sql001 = mysqli_query($conn,"SELECT * FROM `transactions` WHERE `STU_ID` = '$student_session_id'");

      $check001 = mysqli_num_rows($sql001); //Check Available Payments

      if($check001 > 0)
      {
          while($row001 = mysqli_fetch_assoc($sql001))
          {
            $class_id = $row001['CLASS_ID'];
            $class_free_card = $row001['FREE_CLASS']; //Free Card Yes = 1,No = 0

            $sql0012 = mysqli_query($conn,"SELECT * FROM `classes` WHERE `CLASS_ID` = '$class_id' "); //Class Details
            while($row0012 = mysqli_fetch_assoc($sql0012))
            {
                $class_name = $row0012['CLASS_NAME']; //Class Name
                $teacher_id = $row0012['TEACH_ID']; //Teacher ID
                $class_fees = $row0012['FEES']; //Teacher ID

                $class_start_time = $row0012['START_TIME']; //Class Start time
                $class_end_time = $row0012['END_TIME']; //Class End time

                $class_day = $row0012['DAY']; //Class End time
                
                $str = strtotime($class_start_time);
                $class_start_time = date('h:i A',$str);

                $str2 = strtotime($class_end_time);
                $class_end_time = date('h:i A',$str2);

            }

            $sql0013 = mysqli_query($conn,"SELECT * FROM `teacher_details` WHERE `TEACH_ID` = '$teacher_id' "); //check Classes for registered teachers details
            while($row0013 = mysqli_fetch_assoc($sql0013))
            {
                $teacher_position = $row0013['POSITION']; //Teacher Position
                $teacher_f_name = $row0013['F_NAME']; //Teacher First Name
                $teacher_l_name = $row0013['L_NAME']; //Teacher Last Name

                $teacher_picture = $row0013['PICTURE']; //Teacher Profile Picture
                $teacher_gender = $row0013['GENDER']; //Teacher Gender


                $teacher_full_name = $teacher_position.". ".$teacher_f_name." ".$teacher_l_name; //Teacher Full Name

                //If Teacher Picture zero,Check gender and gender to select picture 
                if($teacher_picture == '0')
                {
                    if($teacher_gender == 'Male')
                    {
                        $teacher_picture = 'b_teacher.jpg';
                    }else
                    if($teacher_gender == 'Female')
                    {
                        $teacher_picture = 'g_teacher.jpg';
                    }
                }
                //If Teacher Picture zero,Check gender and gender to select picture

            }
            $sql0012 = mysqli_query($conn,"SELECT * FROM `institute` WHERE `INS_ID` = '1' "); //Institute Data assign to variables
            while($row0012 = mysqli_fetch_assoc($sql0012))
            {

                $institute_name = $row0012['INS_NAME']; //Institute Name
                $institute_tp = $row0012['INS_TP']; //Institute Tp
                $institute_address = $row0012['INS_ADDRESS']; //Institute Address
                $institute_mobile = $row0012['INS_MOBILE']; //Institute Mobile No
                $institute_picture = $row0012['PICTURE']; //Institute Picture

                $institute_free_period = $row0012['FREE']; // **Institute Free Period**

            }



            //Check Free

            if($institute_free_period == 'Yes')
            {

                $price_status = '<h3 class="base-color" style="color:#41e266"><span class="fa fa-gift"></span> Free <small style="font-size:12px;font-weight: bold;">Period</small></h3>';

            }else
            if($institute_free_period == 'No')
            {
                if($class_free_card == '0')
                {
                    $price_status = '<h3 class="base-color">'.number_format($class_fees).'<span style="font-size: 10px;"> LKR/month</span></h3>';
                }else
                if($class_free_card>0)
                {
                    $price_status = '<h3 class="base-color" style="color:#41e266"><span class="fa fa-gift"></span> Free <small style="font-size:12px;font-weight: bold;">Card</small></h3>';
                }
            }

            $year001 = date('Y');
            $month001 = date('m');

            $sql00201 = mysqli_query($conn,"SELECT * FROM `payment_data` WHERE `STU_ID` = '$student_session_id' AND `YEAR` = '$year001' AND `MONTH` = '$month001' AND `CLASS_ID` = '$class_id'");

            $check_payment = mysqli_num_rows($sql00201);

            while($row001996 = mysqli_fetch_assoc($sql00201))
            {

                $payment_year = $row001996['YEAR'];
                $payment_month = $row001996['MONTH'];

                $months001 = array (1=>'ජනවාරි',2=>'පෙබරවාරි',3=>'මාර්තු',4=>'අප්‍රේල්',5=>'මැයි',6=>'ජුනි',7=>'ජුලි',8=>'අගෝස්තු',9=>'සැප්තැම්බර්',10=>'ඔක්තෝම්බර්',11=>'නොවැම්බර්',12=>'දෙසැම්බර්');
                $month_name002 = $months001[(int)$payment_month];
            }

            //Check Free

            ?>
                <!-- Register Class Box -->

                <div class="col-lg-4">
                    <div class="pricing" style="border: 1px solid #cccc;" id="remove_box<?php echo $class_id; ?>">
                        <div class="content">

                              <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-8 image"><center><img src="../teacher/images/profile/<?php echo $teacher_picture; ?>" alt="" style="border-radius: 80px;width: 100px;height: 100px;background-size: cover;"></center></div>
                                <div class="col-md-2"></div>
                              </div>


                        <h5 class="text-dark mt-10 col-md-12"><?php echo $class_name; ?></h5>
                        <label class="text-center"><?php echo $teacher_full_name; ?></label>
                        <br>
                        <label class="col-md-12 text-center" style="font-weight: bold;font-size: 12px;color: <?php echo $ins_bg_color; ?>;"><?php $val = preg_replace('/( )+/', ' ', $class_day); $val_arr = str_getcsv($val); $result = implode(', ', $val_arr); echo $result; ?></label>
                        <br>
                        <label class="text-center text-muted" style="font-weight: bold;font-size: 12px;"><?php echo $class_start_time; ?> - <?php echo $class_end_time; ?></label>
                        
                        <br>

                        <h6 style="margin-top:10px;color:#e74c3c;font-weight: bold;"><?php echo $payment_year; ?> - <?php echo $month_name002; ?></h6>
                        
                        <?php echo $price_status; //Price Status ?>
                        
                          <div class="col-md-12">

                            <?php 

                            if($check_payment > 0)
                            {

                              while($row00201 = mysqli_fetch_assoc($sql00201))
                              {
                                $admin_submit = $row00201['ADMIN_SUBMIT'];
                                $payment_first_uploaded_bank_slip = $row00201['FILE_NAME'];

                                if($admin_submit == '0')
                                {?>

                                        <button type="submit" class="btn btn-block" style="background-color: #f21e4e;color: white;font-weight: bold"><i class="fas fa-circle-notch fa-spin"></i>&nbsp;Checking</button>

                                <?php
                                }else
                                if($admin_submit == '1')
                                {?>

                                    <a style="color:white;pointer-events: none;opacity: 0.5;" onclick="open_modal('<?php echo $class_name; ?>','<?php echo $class_fees; ?>','<?php echo $class_id; ?>','<?php echo $student_transaction_id; ?>')" class="btn-custom" data-toggle="tooltip" title="මෙම පන්තිය ලියාපදිංචියෙන් ඉවත්කර ගැනීම.">
                                        <span><i class="fas fa-check"></i></span>
                                        <span>Paid</span>
                                    </a>


                                <?php 
                                }else
                                if($admin_submit == '2')
                                {?>

                                        <button type="submit" class="btn btn-block" onclick="rejected_upload('<?php echo $payment_first_uploaded_bank_slip; ?>','<?php echo $payment_id; ?>')" style="background-color: #f21e4e;color: white;font-weight: bold" data-toggle="tooltip" title="ඔබ එවන ලද රිසිට්පතෙහි යම් දෝෂයක් පවතී.නැවත පරික්ෂා කර Upload කරන්න."><i class="fas fa-plus"></i>&nbsp;Rejected</button>

                                <?php
                                }
                              }
                            }else
                            if($check_payment == '0')
                            {?>

                                    <a style="color:white;" onclick="open_modal('<?php echo $class_name; ?>','<?php echo $class_fees; ?>','<?php echo $class_id; ?>','<?php echo $student_transaction_id; ?>')" class="btn-custom" data-toggle="tooltip" title="මෙම පන්තිය ලියාපදිංචියෙන් ඉවත්කර ගැනීම.">
                                        <span><i class="fas fa-dollar-sign"></i></span>
                                        <span>Payment</span>
                                    </a>


                                <?php
                            }


                             ?>
                            <br>

                          </div>
                            
                        </div>
                    </div>
                </div>
                

                <!-- Register Class Box -->

            <?php


          }
        }else
        if($check001 == '0')
        {
          /*Empty Payments*/

            ?>


            <div class="row mt-20" style="padding: 10px 10px 10px 10px;">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                        <center>
                            <img src="assets/img/icon/unavailable.svg" style="width: 250px;height: 250px;">
                            <h6 class="mt-20 text-muted"><span class="fas fa-search"></span> Payment Not Found!</h6>
                        </center>
                </div>
                <div class="col-md-3"></div>
            </div>


            <?php

            /*Empty Payments*/
        }

       ?>

    </div>
</div>


    <!-- Payment Classes -->

  <script type="text/javascript">
    function search_payment() {

      var year = document.getElementById('year').value;

      var month = document.getElementById('month').value;

      var student_id = document.getElementById('student_id').value;
      
      if(year !== '' && month !== '')
      {
        $.ajax({  //AJAX USING SENT INPUT DATA
                url:"../student/query/check.php",  //check username in section/insert/insert.php
                method:"POST",  
                data:{search_payments:student_id,year:year,month:month}, 
                success:function(data)
                {  
                  //alert(data)
                    document.getElementById('search_result').innerHTML = data;
                    document.getElementById('show_payment').innerHTML = data;
                }
            });
      }else
      if(year == '' || month == '')
      {
        Swal.fire({
          position: 'top-middle',
          type: 'error',
          icon: 'error',
          title: '<h4><b>ඔබ ගෙවීම් සිදුකල අදාල වර්ෂය හෝ මාසය තෝරන්න.</b></h4>',
          });
      }




      // body...
    }
  </script>


              <script type="text/javascript">
                function change_pay_type(student_id)
                {
                    var pay_type = document.getElementById('select_pay_type').checked;
                    var student_id = document.getElementById('student_id').value;

                    location.reload();

                    if(pay_type == true)
                    {
                      //document.getElementById('search_result').style.display='none';
                      //document.getElementById('show_payment').style.display='inline-flex';

                      //document.getElementById('sub_text').innerHTML='Payment Classes';

                      type_value = '1'; //Active

                      $.ajax({  //AJAX USING SENT INPUT DATA
                        url:"../student/query/update.php",  //check username in section/insert/insert.php
                        method:"POST",  
                        data:{change_payment_tab:student_id,type_value:type_value}, 
                        success:function(data)
                        {  

                        }
                      });

                    }

                    if(pay_type == false)
                    {
                      //document.getElementById('search_result').style.display='inline-flex';
                      //document.getElementById('show_payment').style.display='none';

                      //document.getElementById('sub_text').innerHTML='Paid Classes'; 


                      type_value = '0'; //Active

                      $.ajax({  //AJAX USING SENT INPUT DATA
                        url:"../student/query/update.php",  //check username in section/insert/insert.php
                        method:"POST",  
                        data:{change_payment_tab:student_id,type_value:type_value}, 
                        success:function(data)
                        {  
                          
                        }
                      });
                      
                    }
                }
              </script>

