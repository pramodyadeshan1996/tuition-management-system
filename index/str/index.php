<?php 
	include('../../connect/connect.php');
	session_start();
	$last_attend = '';
	if($_POST['status'] == '1')
	{
		
		$mcq_main_id = $_POST['mcq_main_id'];
		$paper_id = $_POST['paper_id'];
		$student_id = $_POST['student_id'];
		$teacher_id = $_POST['teacher_id'];
		$class_id = $_POST['class_id'];

		$sql007 = mysqli_query($conn,"SELECT * FROM `student_mcq_main` WHERE `PAPER_ID` = '$paper_id' AND `STU_ID` = '$student_id'");
		while($row007 = mysqli_fetch_assoc($sql007))
		{
			$last_attend = $row007['ATTEND_TIME'];
		}

	}else
	if($_POST['status'] == '0')
	{
		
		$student_id = $_POST['student_id'];
		$teacher_id = $_POST['teacher_id'];
		$class_id = $_POST['class_id'];

	}



	$s01 = mysqli_query($conn,"SELECT * FROM `student_details` WHERE `STU_ID` = '$student_id'");
	while($row = mysqli_fetch_assoc($s01))
	{
	    $st_name = $row['F_NAME']." ".$row['L_NAME']; //student name
	    $dob = $row['DOB'];  //student DOB
	    $email = $row['EMAIL'];  //student Email
	    $address = $row['ADDRESS'];  //student Address
	    $picture = $row['PICTURE']; // //student Picture
	    $gender = $row['GENDER']; // //student Gender

	     if($picture == '0')
	     {
	        if($gender == 'Male')
	        {
	          $picture = 'boy202015.png';
	        }else
	        if($gender == 'Female')
	        {
	          $picture = 'girl202015.png';
	        }
	     }

	     $tab_status = $row['PAYMENT_TAB'];

	     if($tab_status == '0')
	     {
	        $tab01 = 'style="display:show"';
	        $tab02 = 'style="display:none"';

	        $switch = '';
	     }else
	     if($tab_status == '1')
	     {
	        $tab01 = 'style="display:none"';
	        $tab02 = 'style="display:show"';

	        $switch = 'checked';
	     }

	    

	    $tp = $row['TP'];
	    $have_sub = $row['NEW'];

	    $ssql01 = mysqli_query($conn,"SELECT * FROM stu_login WHERE STU_ID = '$student_id'");
	    while($row001 = mysqli_fetch_assoc($ssql01))
	    {
	        $last_time = $row001['LOGIN_TIME'];
	        $reg_id = $row001['REGISTER_ID'];
	    }

	}
	
 ?>


<!DOCTYPE html>
<html lang="en">

<head>
	
	<!-- Basic -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title> Student Profile | <?php echo $st_name; ?> | <?php echo $reg_id; ?></title>
	<meta name="description" content="">
	<meta name="keywords" content="">
	
	<!-- Mobile Specific Metas -->
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<!-- Load Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic" rel="stylesheet">

	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
	
	<!-- CSS -->
	<link rel="stylesheet" href="css/basic.css" />
	<link rel="stylesheet" href="css/layout.css" />
	<link rel="stylesheet" href="css/ionicons.css" />
	<link rel="stylesheet" href="css/owl.carousel.css" />
	<link rel="stylesheet" href="css/magnific-popup.css" />
	<link rel="stylesheet" href="css/animate.css" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/pdf.js/2.0.943/pdf.min.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=PT+Sans&display=swap" rel="stylesheet">
	
	<!--[if lt IE 9]>
	<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
	<!-- Favicon -->
	<link rel="shortcut icon" href="images/favicons/favicon.ico">

	<style type="text/css">
		#canvas_container {
          width: auto;
          height: 567px;
          overflow: auto;
      }
 
      #canvas_container {
        background: #ccc;
        text-align: center;
      }

      #canvas_container2 {
          width: auto;
          height: 500px;
          overflow: auto;
      }
 
      #canvas_container2 {
        background: #ccc;
        text-align: center;
      }

      /* width */
	::-webkit-scrollbar {
	  width: 10px;
	}

	/* Track */
	::-webkit-scrollbar-track {
	  background: #f1f1f1;
	}

	/* Handle */
	::-webkit-scrollbar-thumb {
	  background: #888;
	}

	/* Handle on hover */
	::-webkit-scrollbar-thumb:hover {
	  background: #555;
	}
#show_paper
{
	opacity: 0;
	pointer-events: none;
}

#back
{
	opacity: 0.5;
	pointer-events: visible;
	outline: none;
	border-radius: 90px;
	width: 60px;
	font-size: 22px;
	padding: 8px 14px 50px 14px;
}
#back:hover
{
	opacity: 5;
	pointer-events: visible;
	outline: none;
	border-radius: 90px;
	width: 60px;
	font-size: 22px;
	padding: 8px 14px 50px 14px;
	
}

@media only screen and (max-width: 600px) {
#exam {
	display: none;
}
#show_paper
{
	opacity: 5;
	pointer-events: visible;
	outline: none;
	border-radius: 90px;
	width: 60px;
	padding: 12px 14px 46px 14px;
}
}

#ctrl_btn
{
	opacity: 0.25;
	transition: 0.7s;
	position: fixed;
	bottom: 40px;
	right: 50px;
	padding: 20px 10px 20px 0px;
	width: 100%;
}

#ctrl_btn:hover
{
	opacity: 5;
}

/* This is copied from https://github.com/blueimp/jQuery-File-Upload/blob/master/css/jquery.fileupload.css */
.fileinput-button {
  position: relative;
  overflow: hidden;
}

.fileinput-button input {
  position: absolute;
  top: 0;
  right: 0;
  margin: 0;
  opacity: 0;
  -ms-filter: "alpha(opacity=0)";
  font-size: 200px;
  direction: ltr;
  cursor: pointer;
}

.thumb {
  height: 80px;
  width: 100px;
  border: 1px solid #000;
}

ul.thumb-Images li {
  width: 120px;
  float: left;
  display: inline-block;
  vertical-align: top;
  height: 120px;
}

.img-wrap {
  position: relative;
  display: inline-block;
  font-size: 0;
}

.img-wrap .close {
  position: absolute;
  top: 2px;
  right: 2px;
  z-index: 100;
  background-color: #e74c3c;
  padding: 5px 2px 2px;
  color: #ffff;
  font-weight: bolder;
  cursor: pointer;
  opacity: 1;
  font-size: 20px;
  line-height: 10px;
  border-radius: 50%;
  padding: 5px 5px;
}

.img-wrap:hover .close {
  opacity: 1;
  background-color: #ff0000;
}

.FileNameCaptionStyle {
  font-size: 12px;
}


	</style>
	
</head>

<body style="font-family: 'PT Sans', sans-serif;">
	
	<!-- Page -->
	<div class="page" id="home-section">


		<!-- Started Background -->
		<div class="started-bg">
			<div id="particles-bg" class="slide" style="background-image: url(images/particles-bg.jpg);"></div>
		</div>

		<!-- Header -->
		
		<!-- Container -->
		<div class="container-fluid" style="background-color: #eeeeee;position: relative;">

			<?php 


			$sql001 = mysqli_query($conn,"SELECT * FROM `teacher_details` WHERE `TEACH_ID` = '$teacher_id' "); //check Classes for registered teachers details
            while($row001 = mysqli_fetch_assoc($sql001))
            {

                $teacher_position = $row001['POSITION']; //Teacher Position
                $teacher_f_name = $row001['F_NAME']; //Teacher First Name
                $teacher_l_name = $row001['L_NAME']; //Teacher Last Name
                $teacher_gender = $row001['GENDER']; //Teacher Gender
                $teacher_qualification = $row001['QUALIFICATION']; //Teacher Qualification

                $teacher_picture = $row001['PICTURE']; //Teacher Qualification

                $teacher_full_name = $teacher_position.". ".$teacher_f_name." ".$teacher_l_name;


                //If Teacher Picture zero,Check gender and gender to select picture 
                if($teacher_picture == '0')
                {
                    if($teacher_gender == 'Male')
                    {
                        $teacher_picture = 'b_teacher.jpg';
                    }else
                    if($teacher_gender == 'Female')
                    {
                        $teacher_picture = 'g_teacher.jpg';
                    }
                }
                //If Teacher Picture zero,Check gender and gender to select picture 


            }

            $sql002 = mysqli_query($conn,"SELECT * FROM `classes` WHERE `CLASS_ID` = '$class_id'"); //check Classes for registered teachers details
	        while($row002 = mysqli_fetch_assoc($sql002))
	        {
	            $class_name = $row002['CLASS_NAME'];
	        }

	        $sql003 = mysqli_query($conn,"SELECT * FROM `paper_master` WHERE `CLASS_ID` = '$class_id' AND `PAPER_TYPE` = '2'"); //check mcq test table
	        while($row003 = mysqli_fetch_assoc($sql003))
	        {
	            $finish_time = $row003['FINISH_TIME']; //MCQ Test Hold Time
	            $finish_date = $row003['FINISH_DATE']; //MCQ Test Hold Date

	            $finish_d_t = strtotime($finish_date." ".$finish_time);

	            $during_hours = $row003['TIME_DURATION_HOUR']; //Time duration hour
	            $during_min = $row003['TIME_DURATION_MIN']; //Time duration minute
	            $pdf_name = $row003['PDF_NAME']; //Paper Name
	            $answer_amount = $row003['NO_QUESTION_ANSWER'];
	            $question_amount = $row003['NO_OF_QUESTIONS'];

	            $paper_id = $row003['PAPER_ID']; //Paper ID
	        }

	       
	        /*Attend date and time using get time*/

	        $today = strtotime($last_attend);
	        $month = date('F',$today);
	        $day = date('d',$today);
	        $year = date('Y',$today);

	        $h_m = date('H:i:s',$today); //Hours and min

	        /*Attend date and time using get time*/

			 ?>

			<!-- Started -->
			<div class="section started" style="padding:0;margin: 0;">
				<div class="st-box" style="padding-bottom: 0;margin-bottom: 0;">
					<div class="st-image"><img src="../../teacher/images/profile/<?php echo $teacher_picture; ?>" alt="" /></div>
					<div class="st-title" style="font-weight: 500;text-transform: capitalize;"><?php echo $teacher_full_name; ?></div>
					<small><?php echo $teacher_qualification; ?></small>
					<div class="st-subtitle" style="text-transform: capitalize;"><?php echo $class_name; ?> Structured Paper</div>

					<small class="st-subtitle" style="color: #dd20fdeb;">(<?php echo $during_hours ?> Hour <?php echo $during_min ?> Minute)</small>
					<div class="st-soc">
						<a class="btn_animated" style="width: 200px;padding-bottom: 45px;">
 							<label id="count_time" style="color: white;font-size: 24px;font-weight: 400;"></label>
						</a>

						<script>

				          // The data/time we want to countdown to
				          var countDownDate = new Date("<?php echo $month; ?> <?php echo $day; ?>, <?php echo $year; ?> <?php echo $h_m; ?>").getTime();

				          // Run myfunc every second
				          var myfunc = setInterval(function() {

				          var now = new Date().getTime();
				          var timeleft = countDownDate - now;

				          //alert(timeleft)
				              
				          // Calculating the days, hours, minutes and seconds left
				          var days = Math.floor(timeleft / (1000 * 60 * 60 * 24));
				          var hours = Math.floor((timeleft % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
				          var minutes = Math.floor((timeleft % (1000 * 60 * 60)) / (1000 * 60));
				          var seconds = Math.floor((timeleft % (1000 * 60)) / 1000);
				              
				          // Result is output to the specific element
				          document.getElementById("count_time").innerHTML = hours + "h : " + minutes + "m  : " + seconds + 's'

				          /*============================================================*/
				          

        				//document.getElementById("high_score_button").disabled = true; //Disable Answer Button until time out

				          /*============================================================*/


				          document.getElementById("paper_sheet").style.display = 'inline-block'; //Active Answer Button
				          document.getElementById("over_exam_message").style.display = 'none'; //Active Answer Button
				          // Display the message when countdown is over
				          if (timeleft < 0) 
				          {

				            clearInterval(myfunc);
				          	document.getElementById("count_time").innerHTML = hours + "h : " + minutes + "m  : " + seconds + 's'
				            document.getElementById("count_time").innerHTML = "TIME OUT!!";

				            //document.getElementById('submit_btn').click();

				            document.getElementById("over_exam_message").style.display = 'inline-block'; //Active Answer Button
				          	document.getElementById("paper_sheet").style.display = 'none'; //Active Answer Button

				          }


				          if (hours == '1' && minutes == '00' && seconds == '0') 
				          {

				            const Toast = Swal.mixin({
							  toast: true,
							  position: 'top-end',
							  showConfirmButton: false,
							  timer: 5000,
							  timerProgressBar: true,
							  didOpen: (toast) => {
							    toast.addEventListener('mouseenter', Swal.stopTimer)
							    toast.addEventListener('mouseleave', Swal.resumeTimer)
							  }
							})

							Toast.fire({
							  icon: 'success',
							  title: 'ඔබට තවත් පැයක කාලයක් ඉතිරි වී ඇත.'
							})


				          }

				          if (minutes == '30' && seconds == '0') 
				          {

				            const Toast = Swal.mixin({
							  toast: true,
							  position: 'top-end',
							  showConfirmButton: false,
							  timer: 5000,
							  timerProgressBar: true,
							  didOpen: (toast) => {
							    toast.addEventListener('mouseenter', Swal.stopTimer)
							    toast.addEventListener('mouseleave', Swal.resumeTimer)
							  }
							})

							Toast.fire({
							  icon: 'success',
							  title: 'ඔබට තවත් විනාඩි 30ක කාලයක් ඉතිරි වී ඇත.'
							})


				          }

				          if (minutes == '5' && seconds == '0') 
				          {

				            const Toast = Swal.mixin({
							  toast: true,
							  position: 'top-end',
							  showConfirmButton: false,
							  timer: 5000,
							  timerProgressBar: true,
							  didOpen: (toast) => {
							    toast.addEventListener('mouseenter', Swal.stopTimer)
							    toast.addEventListener('mouseleave', Swal.resumeTimer)
							  }
							})

							Toast.fire({
							  icon: 'success',
							  title: 'ඔබට තවත් විනාඩි 05ක කාලයක් ඉතිරි වී ඇත.'
							})


				          }



				          }, 1000);
				          </script>
					</div>
				</div>
			</div>


			<hr style="border: 1px solid #cccc;">
			<!-- Wrapper -->
			<?php 
				$sql005 = mysqli_query($conn,"SELECT * FROM `student_mcq_master` WHERE `PAPER_ID` = '$paper_id' AND `STU_ID` = '$student_id'");

				$check001 = mysqli_num_rows($sql005); //Check Submited?

				if($check001 > 0)
				{
					$result_div = 'show';
					$paper_div = 'none';
				}else
				if($check001 == '0')
				{
					$result_div = 'none';
					$paper_div = 'show';
				}

			 ?>

			<div class="wrapper" style="padding-top: 25px;display: <?php echo $paper_div; ?>;" id="paper">

				<!-- Section Skills -->
				<div class="section skills" id="skills-section" style="padding-top: 0;">
					<div class="title" style="font-weight: 400;"></div>
					<div class="i_title" style="padding-bottom: 20px;">
						<div class="icon"><i class="ion-ios-lightbulb" style="font-size: 40px;color: white;"></i> </div>
						<h2 style="padding-top: 0;">Examination </h2>
					</div>

					<center>
					<div class="col-md-12 col-m-12 col-t-7 col-d-7" id="over_exam_message" style="display: none;margin-bottom: 20px;">
						<div class="content-box animated">

							
								<img src="images/icon/timeover.svg" style="width:300px;height: 300px;">
								<h4 class="text-success text-center" style="text-align: center;color: #9C27B0;font-size: 24px;"><span class="fa fa-check-circle"></span> Given time has been expired.</h4>

								<a href="../index.php" style="text-decoration: none;font-weight: bold;opacity: 0.6;color: #9C27B0;"><span class="fa fa-angle-left"></span> Go Back</a>

						</div>
					</center>
					</div>

					<center>
					<div class="row" id="paper_sheet" style="display: none;margin-bottom: 10px;">
						<div class="col col-m-12 col-t-7 col-d-7" id="exam">
							<div class="content-box animated">
								

									<h4 class="name text-center text-bold" style="color: #8e44ad;"><i class="fa fa-file"></i> Paper Sheet</h4>

								<div class="col-m-12">

									<div id="my_pdf_viewer" oncontextmenu="return false;">
								        <div id="canvas_container" style="position: relative;">
								            <canvas id="pdf_renderer"></canvas>
								        </div>

								        <div class="row" id="ctrl_btn">
								        	
								        		<div class="col-md-3" >

												</div>
												<div class="col-md-6" style="padding-top: 60px;">

													<center><input id="current_page" readonly class="form-control" value="1" type="text" style="text-align: center;border: none;outline: none;width:60px;background-color: #ffffff00;font-weight: 700;" /></center>
												</div>
												<div class="col-md-3"> 

													<div id="navigation_controls" style="float: right;">
										        		<button class="btn btn-primary" id="go_previous" style="font-size: 15px;margin-top: 4px;margin-right: 8px;background-color: #3498db;outline: none;border-radius: 80px;padding-left: 14px;padding-right: 14px;border: 1px solid #3498db;"><span class="fa fa-arrow-left"></span></button>
										            	
										            	<button class="btn btn-primary" id="go_next" style="font-size: 15px;margin-top: 4px;background-color: #3498db;outline: none;border-radius: 80px;padding-left: 14px;padding-right: 14px;border: 1px solid #3498db;"><span class="fa fa-arrow-right"></span>
										            	</button>
													</div>


													<div id="zoom_controls" style="float: right;">
										            	<button class="btn btn-danger" id="zoom_out" style="font-size: 15px;margin-top: 4px;outline: none;margin-right: 9px;background-color: #e74c3c;border-radius: 80px;padding-left: 15px;padding-right: 15px;border: none;"><span class="fa fa-minus"></span></button>

										            	<button class="btn btn-success" id="zoom_in" style="font-size: 15px;margin-top: 4px;outline: none;background-color: #27ae60;border-radius: 80px;padding-left: 15px;padding-right: 15px;border: none;"><span class="fa fa-plus"></span></button>
										        	</div>
								        		</div> 
						        			
									        
								    	</div>
					        		</div>
 
										    <script>
										        var myState = {
										            pdf: null,
										            currentPage: 1,
										            zoom: 1
										        }
										      
										        pdfjsLib.getDocument('../../admin/images/str_document/<?php echo $pdf_name; ?>').then((pdf) => {
										      
										            myState.pdf = pdf;
										            render();
										 
										        });
										 
										        function render() {
										            myState.pdf.getPage(myState.currentPage).then((page) => {
										          
										                var canvas = document.getElementById("pdf_renderer");
										                var ctx = canvas.getContext('2d');
										      
										                var viewport = page.getViewport(myState.zoom);
										 
										                canvas.width = viewport.width;
										                canvas.height = viewport.height;
										          
										                page.render({
										                    canvasContext: ctx,
										                    viewport: viewport
										                });
										            });
										        }
										 
										        document.getElementById('go_previous').addEventListener('click', (e) => {
										            if(myState.pdf == null || myState.currentPage == 1) 
										              return;
										            myState.currentPage -= 1;
										            document.getElementById("current_page").value = myState.currentPage;
										            render();
										        });
										 
										        document.getElementById('go_next').addEventListener('click', (e) => {
										            if(myState.pdf == null || myState.currentPage > myState.pdf._pdfInfo.numPages) 
										               return;
										            myState.currentPage += 1;
										            document.getElementById("current_page").value = myState.currentPage;
										            render();
										        });
										 
										        document.getElementById('current_page').addEventListener('keypress', (e) => {
										            if(myState.pdf == null) return;
										          
										            // Get key code
										            var code = (e.keyCode ? e.keyCode : e.which);
										          
										            // If key code matches that of the Enter key
										            if(code == 13) {
										                var desiredPage = 
										                document.getElementById('current_page').valueAsNumber;
										                                  
										                if(desiredPage >= 1 && desiredPage <= myState.pdf._pdfInfo.numPages) {
										                    myState.currentPage = desiredPage;
										                    document.getElementById("current_page").value = desiredPage;
										                    render();
										                }
										            }
										        });
										 
										        document.getElementById('zoom_in').addEventListener('click', (e) => {
										            if(myState.pdf == null) return;
										            myState.zoom += 0.1;
										            render();
										        });
										 
										        document.getElementById('zoom_out').addEventListener('click', (e) => {
										            if(myState.pdf == null) return;
										            myState.zoom -= 0.1;
										            render();
										        });
										    </script>



								</div>
							</div>
						</div>

						<div class="col col-m-12 col-t-5 col-d-5">
							<div class="content-box animated">

								<h4 class="text-center text-bold" style="color: #8e44ad;"><i class="fa fa-upload"></i> Upload Paper</h4>


								<div class="skills">
									<ul style="height: 568px;">

										<div class="row">
											<div class="col-md-1"></div>
											<div class="col-md-10">
												
												<center>

													<output id="Filelist" style="height: 300px;overflow: auto;">

														<img src="images/icon/upload.svg" style="width: 290px;height: 290px;" id="img_svg">

													</output>

												</center>

												<?php 

													$sql008 = mysqli_query($conn,"SELECT * FROM `student_essay_master` WHERE `PAPER_ID` = '$paper_id' AND `STU_ID` = '$student_id'");

													if(mysqli_num_rows($sql008)>0)
													{
														$uploaded_ok = 'none'; //Show Form
														$upload_msg = 'show';//Show upload message
													}else
													if(mysqli_num_rows($sql008)=='0')
													{
														$uploaded_ok = 'show'; //Show Form
														$upload_msg = 'none';//Show upload message

													}

												 ?>
												 <div style="display: <?php echo $upload_msg; ?>">
												 <hr>
												 <center><span class="fa fa-check-circle text-success" style="font-size: 120px;margin-top:16px;color: #1abc9c;"></span>
												 	<h2 style="color: #16a085;font-weight: 500;">Upload Successfully!</h2>
												 </center>
												</div>

											<form action="../../student/query/update.php" method="POST" enctype="multipart/form-data" style="display: <?php echo $uploaded_ok; ?>">
												<input type="file" class="form-control" id="paper_file" name="upload_file[]" multiple accept="image/jpeg,image/jpg,image/png" style="display: none;" onchange="uploaded()"/>

												<div id="open_file" onclick="document.getElementById('paper_file').click();" style="cursor: pointer;margin-top: 30px;">
													
									                  <button type="button" class="btn btn-block btn-sm" name="update_payment" style="padding: 32px 20px 20px 20px;border:3px dashed gray;outline: none;background-color: white;color:#545454;height: 100px;cursor: pointer;" value="Browse..."> <i class="fa fa-paperclip" style="font-size: 20px;"></i> <label style="font-weight: bold;cursor: pointer;font-size: 20px;">Upload Paper</label></button>



												</div>
												<br>
												<strong class="text-danger" style="padding-top: 4px;"><span class="fa fa-asterisk"></span> Please upload only .jpeg,.jpg,.png file type images.</strong>

												<input type="hidden" name="paper_id" value="<?php echo $paper_id; ?>">

												<center><button type="button" class="btn btn-info btn-block" style="font-size: 16px;margin-top: 35px;background-color: #9c27b0;outline: none;border:none;" onclick="var des = confirm('Are you sure submit your paper?'); if(des == true){ document.getElementById('submit_btn').click(); }" id="disable_btn"><span class="fa fa-check-circle"></span> Submit Paper</button></center>

												<button type="submit"  id="submit_btn" value="<?php echo $student_id; ?>" name="submit_paper" style="display: none;" data-target="#submit_modal" data-toggle="modal">X</button>

												<center><button type="reset" class="btn btn-link text-muted" style="text-decoration: none;color: gray;outline: none;" onclick="location.reload();">Reset</button></center>

											</form>

											</div>
											<div class="col-md-1"></div>
										</div>

									</ul>
								</div>


								 <script type="text/javascript">
                    				
                    				document.getElementById('disable_btn').disabled = true;

                    				function clear_upload()
                    				{
                    					document.getElementById('paper_file').value = '';

                    					$("#open_file").html('<button type="button" class="btn btn-block btn-sm" name="update_payment"  style="padding: 32px 20px 20px 20px;border:3px dashed gray;outline: none;background-color: white;color:#545454;height: 100px;cursor: pointer;" value="Browse..."> <i class="fa fa-paperclip" style="font-size: 20px;"></i> <label style="font-weight: bold;cursor: pointer;font-size: 20px;">Upload Paper</label></button>');
                    				}

				                    function uploaded()
				                    {
				                        
				                        var paper_file = document.getElementById('paper_file').value;


				                        if(paper_file == '')
				                        {
				                          $("#open_file").html('<button type="button" class="btn btn-block btn-sm" name="update_payment"  style="padding: 32px 20px 20px 20px;border:3px dashed gray;outline: none;background-color: white;color:#545454;height: 100px;cursor: pointer;" value="Browse..."> <i class="fa fa-paperclip" style="font-size: 20px;"></i> <label style="font-weight: bold;cursor: pointer;font-size: 20px;">Upload Paper</label></button>');
				                        	
				                        	document.getElementById('disable_btn').disabled = true;


				                        }else
				                        if(paper_file !== '')
				                        {


				                                $("#open_file").html('<button type="button" class="btn btn-success btn-block btn-sm" name="update_payment" style="padding: 32px 20px 20px 20px;outline: none;height: 100px;cursor: pointer;background-color:#1abc9c;border:none;" value="Browse..."> <i class="fa fa-check-circle" style="font-size: 20px;"></i> <label style="font-weight: bold;cursor: pointer;font-size: 20px;">Upload Successfully</label></button>');

				                        		document.getElementById('disable_btn').disabled = false;

				                         
				                          
				                        }

				                        
				                    }

				                  </script>


							</div>
						</div>

					



					<button class="btn btn-success btn-lg" data-toggle="modal" data-target="#show_my_paper" id="show_paper" style="position: fixed;bottom: 110px;right: 40px;background-color: #16a085;"><i class="icon ion ion-document-text" style="font-size: 35px;"></i></button>


					</div>
					</center>

					<a href="../index.php" class="btn btn-danger btn-lg" id="back" style="position: fixed;bottom: 40px;right: 40px;background-color: #ee5253;border:1px solid #ee5253;"><span class="fa fa-angle-left"></span></a>
				</div>

			</div>
			


								<div id="submit_modal" class="modal fade" role="dialog" style="margin-top: 100px;" data-backdrop="static" data-keyboard="false">
								  <div class="modal-dialog modal-sm">

								    <!-- Modal content-->
								    <div class="modal-content">
								      <div class="modal-body">
								        	
								        <img src="../../student/images/uploading.gif" width="100%" height="200px">
								        <h4 class="text-muted" style="position: fixed;bottom: 40px;right: 100px;font-weight: bold;">Uploading..</h4>

								        <center><small class="text-center" style="text-align: center;position: fixed;bottom: 30px;left: 55px;">Please Don't close or reload this page.</small></center>

								      </div>
								    </div>

								  </div>
								</div>
			
		</div>

	</div>
	
	<!-- jQuery Scripts -->
	<script src="js/jquery.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/jquery.validate.js"></script>
	<script src="js/magnific-popup.js"></script>
	<script src="js/masonry.pkgd.js"></script>
	<script src="js/imagesloaded.pkgd.js"></script>
	<script src="js/masonry-filter.js"></script>
	<script src="js/scrollreveal.js"></script>
	<script src="js/jquery.mb.YTPlayer.js"></script>
	<script src="js/particles.js"></script>
	<script src="js/particles-setting.js"></script>

	<!-- Google map api -->
	<script src="https://maps.google.com/maps/api/js?sensor=false"></script>
	
	<!-- Main Scripts -->
	<script src="js/main.js"></script>
	
</body>

<!-- Mirrored from beshley.com/mcard/theme_colors/purple/index-1-particles.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 26 Jan 2021 18:39:47 GMT -->
</html>


								<div id="show_my_paper" class="modal fade" role="dialog">
								  <div class="modal-dialog">

								    <!-- Modal content-->
								    <div class="modal-content">
								      <div class="modal-body">

								      	<div id="my_pdf_viewer2" oncontextmenu="return false;">
									        <div id="canvas_container2">
									            <canvas id="pdf_renderer2"></canvas>
									        </div>

									        		<div class="row">
									        			<div class="col-md-12 col-sm-12 col-xs-12" style="padding-left: 16%;">
									        				
									        				<div class="row" style="padding-top: 10px;">
											        			<div class="col-md-2 col-sm-2 col-xs-2">

											        				<button class="btn btn-primary btn-sm btn-block" id="go_previous2" style="margin-top: 4px;margin-right: 8px;background-color: #2980b9;outline: none;width: 100%;border-radius: 80px;padding: 10px 25px 10px 16px;text-align: center;"><span class="fa fa-arrow-left"></span></button>

											        			</div>
											        			<div class="col-md-2 col-sm-2 col-xs-2">

											        				<input id="current_page2" value="1" type="hidden"/>
														    		<button class="btn btn-primary btn-block" id="go_next2" style="margin-top: 4px;background-color: #2980b9;outline: none;width: 100%;border-radius: 80px;padding: 10px 25px 10px 16px;text-align: center;"><span class="fa fa-arrow-right"></span></button>

											        			</div>

											        			<div class="col-md-2 col-sm-2 col-xs-2">

											        				<button class="btn btn-danger btn-sm" id="zoom_out2" style="margin-top: 4px;outline: none;background-color: #e74c3c;width: 100%;border-radius: 80px;padding: 10px 25px 10px 16px;text-align: center;"><span class="fa fa-minus"></span></button>

											        			</div>
											        			<div class="col-md-2 col-sm-2 col-xs-2">

											        				<button class="btn btn-success btn-sm" id="zoom_in2" style="margin-top: 4px;outline: none;margin-right: 8px;background-color: #27ae60;width: 100%;border-radius: 80px;padding: 10px 25px 10px 16px;text-align: center;"><span class="fa fa-plus"></span></button>

											        			</div>
											        			<div class="col-md-2 col-sm-2 col-xs-2">

											        				<button class="btn btn-default btn-sm btn-block" data-dismiss="modal" style="outline: none;margin-top: 4px;width: 100%;padding: 10px 25px 10px 16px;border-radius: 80px;text-align: center;"><span class="fa fa-times"></span></button>

											        			</div>
											        		</div>

									        			</div>
										        		
									        		</div>

						        			</div>
						 
									    <script>
									        var myState = {
									            pdf: null,
									            currentPage: 1,
									            zoom: 1.2
									        }
									      
									        pdfjsLib.getDocument('../../admin/images/str_document/<?php echo $pdf_name; ?>').then((pdf) => {
									      
									            myState.pdf = pdf;
									            render2();
									 
									        });
									 
									        function render2() {
									            myState.pdf.getPage(myState.currentPage).then((page) => {
									          
									                var canvas = document.getElementById("pdf_renderer2");
									                var ctx = canvas.getContext('2d');
									      
									                var viewport = page.getViewport(myState.zoom);
									 
									                canvas.width = viewport.width;
									                canvas.height = viewport.height;
									          
									                page.render({
									                    canvasContext: ctx,
									                    viewport: viewport
									                });
									            });
									        }
									 
									        document.getElementById('go_previous2').addEventListener('click', (e) => {
									            if(myState.pdf == null || myState.currentPage == 1) 
									              return;
									            myState.currentPage -= 1;
									            document.getElementById("current_page2").value = myState.currentPage;
									            render2();
									        });
									 
									        document.getElementById('go_next2').addEventListener('click', (e) => {
									            if(myState.pdf == null || myState.currentPage > myState.pdf._pdfInfo.numPages) 
									               return;
									            myState.currentPage += 1;
									            document.getElementById("current_page2").value = myState.currentPage;
									            render2();
									        });
									 
									        document.getElementById('current_page2').addEventListener('keypress', (e) => {
									            if(myState.pdf == null) return;
									          
									            // Get key code
									            var code = (e.keyCode ? e.keyCode : e.which);
									          
									            // If key code matches that of the Enter key
									            if(code == 13) {
									                var desiredPage = 
									                document.getElementById('current_page2').valueAsNumber;
									                                  
									                if(desiredPage >= 1 && desiredPage <= myState.pdf._pdfInfo.numPages) {
									                    myState.currentPage = desiredPage;
									                    document.getElementById("current_page2").value = desiredPage;
									                    render2();
									                }
									            }
									        });
									 
									        document.getElementById('zoom_in2').addEventListener('click', (e) => {
									            if(myState.pdf == null) return;
									            myState.zoom += 0.1;
									            render2();
									        });
									 
									        document.getElementById('zoom_out2').addEventListener('click', (e) => {
									            if(myState.pdf == null) return;
									            myState.zoom -= 0.1;
									            render2();
									        });
									    </script>

								      </div>
								    </div>

								  </div>
								</div>




			<script>
			$(document).ready(function(){
			  $("#myInput").on("keyup", function() {
			    var value = $(this).val().toLowerCase();
			    $("#myTable tr").filter(function() {
			      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
			    });
			  });
			});
			</script>



			<script type="text/javascript">
				

				function printDiv(divName) {
				     var printContents = document.getElementById(divName).innerHTML;
				     var originalContents = document.body.innerHTML;

				     document.body.innerHTML = printContents;

				     window.print();

				     document.body.innerHTML = originalContents;
				}

			</script>

			<!-- Confirm submit Modal -->

			<div id="confirm_submit" class="modal fade" role="dialog">
			  <div class="modal-dialog" style="margin-top: 100px;">

			    <!-- Modal content-->
			    <div class="modal-content modal-sm">
			      <div class="modal-header">
			      	
			      	<h4 class="modal-title">Confirmation</h4>
			        
			      </div>
			      <div class="modal-body">

			      <div class="row" style="padding-top: 0px;padding-bottom: 0px;">
			      	<!--<div class="col-md-3"> <a href="#" class="btn btn-success" onclick="printDiv('printableArea')" style="border-radius: 80px;"><span class="fa fa-print"></span></a></div>
			      	<div class="col-md-3"></div> -->
			      	<div class="col-md-12" style="padding-bottom: 0px;padding-top: 0px;">
			      		
			      		<center>
			      			<span class="fa fa-check-circle" style="font-size: 80px;color: #10ac84;"></span>
			      			<h2>Are you sure submit your answers?</h2>

			      		</center>
			      		<br>
			      		<center>
			      			<button type="button" class="btn btn-success btn-block" style="background-color: #10ac84;border: 1px solid #10ac84;" onclick="document.getElementById('submit_btn').click();" data-dismiss="modal"><span class="fa fa-check-circle"></span> Yes</button>

			        		<button type="button" class="btn btn-default btn-block" data-dismiss="modal"><span class="fa fa-times-circle"></span> No</button>

			      		</center>

			      	</div>
			      </div>

				  </div>
			      <div class="modal-footer">
			        
			      </div>
			    </div>
			</div>


			<!-- Confirm submit Modal -->

			<script type="text/javascript">
				//JS

				//I added event handler for the file upload control to access the files properties.
				document.addEventListener("DOMContentLoaded", init, false);

				//To save an array of attachments
				var AttachmentArray = [];

				//counter for attachment array
				var arrCounter = 0;

				//to make sure the error message for number of files will be shown only one time.
				var filesCounterAlertStatus = false;

				//un ordered list to keep attachments thumbnails
				var ul = document.createElement("ul");
				ul.className = "thumb-Images";
				ul.id = "imgList";

				function init() {
				  //add javascript handlers for the file upload event
				  document
				    .querySelector("#paper_file")
				    .addEventListener("change", handleFileSelect, false);
				}

				//the handler for file upload event
				function handleFileSelect(e) {
				  //to make sure the user select file/files
				  if (!e.target.files) return;

				  //To obtaine a File reference
				  var files = e.target.files;

				  // Loop through the FileList and then to render image files as thumbnails.
				  for (var i = 0, f; (f = files[i]); i++) {
				    //instantiate a FileReader object to read its contents into memory
				    var fileReader = new FileReader();

				    // Closure to capture the file information and apply validation.
				    fileReader.onload = (function(readerEvt) {
				      return function(e) {
				        //Apply the validation rules for attachments upload
				        ApplyFileValidationRules(readerEvt);

				        //Render attachments thumbnails.
				        RenderThumbnail(e, readerEvt);

				        //Fill the array of attachment
				        FillAttachmentArray(e, readerEvt);
				      };
				    })(f);

				    // Read in the image file as a data URL.
				    // readAsDataURL: The result property will contain the file/blob's data encoded as a data URL.
				    // More info about Data URI scheme https://en.wikipedia.org/wiki/Data_URI_scheme
				    fileReader.readAsDataURL(f);
				  }
				  document
				    .getElementById("paper_file")
				    .addEventListener("change", handleFileSelect, false);
				}

				//To remove attachment once user click on x button
				jQuery(function($) {
				  $("div").on("click", ".img-wrap .close", function() {
				    var id = $(this)
				      .closest(".img-wrap")
				      .find("img")
				      .data("id");

				    //to remove the deleted item from array
				    var elementPos = AttachmentArray.map(function(x) {
				      return x.FileName;
				    }).indexOf(id);
				    if (elementPos !== -1) {
				      AttachmentArray.splice(elementPos, 1);
				    }

				    if(elementPos == '0')
				    {
				  
				  		document.getElementById('img_svg').style.display = 'inline-block'; //Hidden SVG image

				  		document.getElementById('paper_file').value = '';

                    	$("#open_file").html('<button type="button" class="btn btn-block btn-sm" name="update_payment"  style="padding: 32px 20px 20px 20px;border:3px dashed gray;outline: none;background-color: white;color:#545454;height: 100px;cursor: pointer;" value="Browse..."> <i class="fa fa-paperclip" style="font-size: 20px;"></i> <label style="font-weight: bold;cursor: pointer;font-size: 20px;">Upload Paper</label></button>');

				    }

				    //to remove image tag
				    $(this)
				      .parent()
				      .find("img")
				      .not()
				      .remove();

				    //to remove div tag that contain the image
				    $(this)
				      .parent()
				      .find("div")
				      .not()
				      .remove();

				    //to remove div tag that contain caption name
				    $(this)
				      .parent()
				      .parent()
				      .find("div")
				      .not()
				      .remove();

				    //to remove li tag
				    var lis = document.querySelectorAll("#imgList li");
				    for (var i = 0; (li = lis[i]); i++) {
				      if (li.innerHTML == "") {
				        li.parentNode.removeChild(li);
				      }
				    }
				  });
				});

				//Apply the validation rules for attachments upload
				function ApplyFileValidationRules(readerEvt) {
				  //To check file type according to upload conditions
				  if (CheckFileType(readerEvt.type) == false) {
				    alert(
				      "The file (" +
				        readerEvt.name +
				        ") does not match the upload conditions, You can only upload jpeg/jpg/png files"
				    );
				    e.preventDefault();
				    return;
				  }

				  //To check file Size according to upload conditions
				  if (CheckFileSize(readerEvt.size) == false) {
				    alert(
				      "The file (" +
				        readerEvt.name +
				        ") does not match the upload conditions, The maximum file size for uploads should not exceed 300 KB"
				    );
				    e.preventDefault();
				    return;
				  }

				  //To check files count according to upload conditions
				  if (CheckFilesCount(AttachmentArray) == false) {
				    if (!filesCounterAlertStatus) {
				      filesCounterAlertStatus = true;
				      alert(
				        "You have added more than 100 files. According to upload conditions you can upload 10 files maximum"
				      );
				    }
				    e.preventDefault();
				    return;
				  }
				}

				//To check file type according to upload conditions
				function CheckFileType(fileType) {
				  if (fileType == "image/jpeg") {
				    return true;
				  } else if (fileType == "image/png") {
				    return true;
				  } else if (fileType == "image/jpg") {
				    return true;
				  } else {
				    return false;
				  }
				  return true;
				}

				//To check file Size according to upload conditions
				function CheckFileSize(fileSize) {
				  if (fileSize < 10000000) {
				    return true;
				  } else {
				    return false;
				  }
				  return true;
				}

				//To check files count according to upload conditions
				function CheckFilesCount(AttachmentArray) {
				  //Since AttachmentArray.length return the next available index in the array,
				  //I have used the loop to get the real length
				  var len = 0;
				  for (var i = 0; i < AttachmentArray.length; i++) {
				    if (AttachmentArray[i] !== undefined) {
				      len++;
				    }
				  }
				  //To check the length does not exceed 100 files maximum
				  if (len > 100) {
				    return false;
				  } else {
				    return true;
				  }
				}

				//Render attachments thumbnails.
				function RenderThumbnail(e, readerEvt) {
				  var li = document.createElement("li");

				  document.getElementById('img_svg').style.display = 'none'; //Hidden SVG image

				  ul.appendChild(li);
				  li.innerHTML = [
				    '<div class="img-wrap"> <span class="close">&times;</span>' +
				      '<img class="thumb" src="',
				    e.target.result,
				    '" title="',
				    escape(readerEvt.name),
				    '" data-id="',
				    readerEvt.name,
				    '"/>' + "</div>"
				  ].join("");

				  var div = document.createElement("div");
				  div.className = "FileNameCaptionStyle";
				  li.appendChild(div);
				  div.innerHTML = [readerEvt.name].join("");
				  document.getElementById("Filelist").insertBefore(ul, null);
				}

				//Fill the array of attachment
				function FillAttachmentArray(e, readerEvt) {
				  AttachmentArray[arrCounter] = {
				    AttachmentType: 1,
				    ObjectType: 1,
				    FileName: readerEvt.name,
				    FileDescription: "Attachment",
				    NoteText: "",
				    MimeType: readerEvt.type,
				    Content: e.target.result.split("base64,")[1],
				    FileSizeInBytes: readerEvt.size
				  };
				  arrCounter = arrCounter + 1;
				}
			</script>