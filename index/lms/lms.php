<?php
include('../../connect/connect.php');
session_start();
 $stu_id = $_SESSION['STU_ID'];


/*check session with active tab*/


$active_pdf_tab = '';

$active_rec_tab = '';

$active_ytd_tab = '';


 if(empty($_SESSION['lms_tab_active']))
 {
    $activated_tab = "pdf";
 }else
 if(!empty($_SESSION['lms_tab_active']))
 {
    $activated_tab = $_SESSION['lms_tab_active'];
 }

 if($activated_tab == 'pdf')
 {
    $active_pdf_tab = 'active';
 }else
 if($activated_tab == 'rec')
 {
    $active_rec_tab = 'active';
 }else
 if($activated_tab == 'ytd')
 {
    $active_ytd_tab = 'active';
 }

/*check session with active tab*/


 $class_id = $_GET['class_id'];

 $url = '';

  if ($_SERVER['HTTP_REFERER'] == $url) {
    header('Location: ../../direct_access.php'); //redirect to some other page
    exit();
  }

  $sql003 = mysqli_query($conn,"SELECT * FROM `institute` WHERE `INS_ID` = '1'");
  while($row003 = mysqli_fetch_assoc($sql003))
  {
      $ins_name = $row003['INS_NAME'];
      $ins_bg_color = $row003['BG_COLOR'];
  }


 $s01 = mysqli_query($conn,"SELECT * FROM student_details WHERE STU_ID = '$stu_id'");
      while($row = mysqli_fetch_assoc($s01))
      {
        $st_name = $row['F_NAME']." ".$row['L_NAME']; //student name
        $dob = $row['DOB'];  //student DOB
        $email = $row['EMAIL'];  //student Email
        $address = $row['ADDRESS'];  //student Address
        $picture = $row['PICTURE']; // //student Picture
        $gender = $row['GENDER']; // //student Gender

         if($picture == '0')
         {
            if($gender == 'Male')
            {
              $picture = 'boy202015.png';
            }else
            if($gender == 'Female')
            {
              $picture = 'girl202015.png';
            }
         }

        

        $tp = $row['TP'];
        $have_sub = $row['NEW'];

        $ssql01 = mysqli_query($conn,"SELECT * FROM stu_login WHERE STU_ID = '$stu_id'");
        while($row001 = mysqli_fetch_assoc($ssql01))
        {
            $last_time = $row001['LOGIN_TIME'];
            $reg_id = $row001['REGISTER_ID'];
        }

      }

      $sql00199 = mysqli_query($conn,"SELECT * FROM `classes` WHERE `CLASS_ID` = '$class_id'");
        while($row00199 = mysqli_fetch_assoc($sql00199))
        {
            $teach_id = $row00199['TEACH_ID'];
            $my_class_name = $row00199['CLASS_NAME'];

            $sql0011 = mysqli_query($conn,"SELECT * FROM `teacher_details` WHERE `TEACH_ID` = '$teach_id' "); //check Classes for registered teachers details
            while($row0011 = mysqli_fetch_assoc($sql0011))
            {

                $teacher_position = $row0011['POSITION']; //Teacher Position
                $teacher_f_name = $row0011['F_NAME']; //Teacher First Name
                $teacher_l_name = $row0011['L_NAME']; //Teacher Last Name
                $teacher_gender = $row0011['GENDER']; //Teacher Gender
                $teacher_qualification = $row0011['QUALIFICATION']; //Teacher Qualification

                if($teacher_qualification == '0' || $teacher_qualification == '')
                {
                    $teacher_qualification = '';
                }else
                {
                    $teacher_qualification = '('.$teacher_qualification.')';
                }

                $teacher_picture = $row0011['PICTURE']; //Teacher Qualification

                $teacher_full_name = $teacher_position.". ".$teacher_f_name." ".$teacher_l_name;


                //If Teacher Picture zero,Check gender and gender to select picture 
                if($teacher_picture == '0')
                {
                    if($teacher_gender == 'Male')
                    {
                        $teacher_picture = 'b_teacher.jpg';
                    }else
                    if($teacher_gender == 'Female')
                    {
                        $teacher_picture = 'g_teacher.jpg';
                    }
                }
                //If Teacher Picture zero,Check gender and gender to select picture 


            }
        }


    ?>


<html lang="en">

<!-- Mirrored from dongo.netlify.app/dongo/index-2.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 24 Dec 2020 18:01:18 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
    <!-- Meta -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- favicon -->
    <link rel="icon" sizes="16x16" href="../assets/img/favicon.png">

    <!-- Title -->
    <title style="text-transform: capitalize;"> Student Profile | <?php echo $st_name; ?> | <?php echo $reg_id; ?> </title>

    <!-- CSS Plugins -->
    <link rel="stylesheet" href="../assets/css/all.css">
    <link rel="stylesheet" href="../assets/css/elegant-font-icons.css">
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/magnific-popup.css">
    <link rel="stylesheet" href="../assets/css/owl.carousel.css">

    <!-- Font Google -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Muli:300,400,500,600,700,800,900&amp;display=swap" rel="stylesheet">

    <!-- main style -->
    <link rel="stylesheet" href="../assets/css/style.css">
    <link rel="stylesheet" href="../assets/css/colors/color-1.php">

    <link rel="stylesheet" href="../assets/bootstrap_4//css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="../assets/bootstrap_4/js/bootstrap.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

  <style type="text/css">
      @media print {
  body * {
    visibility: hidden;
  }
  #section-to-print, #section-to-print * {
    visibility: visible;
  }
  #section-to-print {
    position: absolute;
    left: 0;
    top: 0;
  }
}


.circular-menu {
  position: fixed;
  bottom: 1em;
  right: 1em;
}

.circular-menu .floating-btn {
  display: block;
  width: 3.5em;
  height: 3.5em;
  border-radius: 50%;
  background-color: <?php echo $ins_bg_color; ?>;
  border:1px solid <?php echo $ins_bg_color; ?>;
  box-shadow: 0 2px 5px 0 hsla(0, 0%, 0%, .26);  
  color: hsl(0, 0%, 100%);
  text-align: center;
  line-height: 3.9;
  cursor: pointer;
  outline: none;
}

.circular-menu.active .floating-btn {
  box-shadow: inset 0 0 3px hsla(0, 0%, 0%, .3);
  border:4px solid white;
}

.circular-menu .floating-btn:active {
  box-shadow: 0 4px 8px 0 hsla(0, 0%, 0%, .4);
}

.circular-menu .floating-btn i {
  font-size: 1.2em;
  transition: transform .2s;  
}

.circular-menu.active .floating-btn i {
  transform: rotate(-45deg);
}

.circular-menu:after {
  display: block;
  content: ' ';
  width: 2.5em;
  height: 2.5em;
  border-radius: 80%;
  position: absolute;
  top: 0;
  right: 0px;
  z-index: -2;
  background-color: <?php echo $ins_bg_color; ?>;
  transition: all 0.6s ease-out;
}

.circular-menu.active:after {
  transform: scale3d(5.5, 5.5, 1);
  transition-timing-function: cubic-bezier(.68, 1.55, .265, 1);
}

.circular-menu .items-wrapper {
  padding: 0;
  margin: 0;
}

.circular-menu .menu-item {
  position: absolute;
  top: .2em;
  right: .2em;
  z-index: -1;
  display: block;
  text-decoration: none;
  color: hsl(0, 0%, 100%);
  font-size: 1em;
  width: 2.3em;
  height: 2.3em;
  padding-top: 10px;
  border-radius: 50%;
  text-align: center;
  line-height: 3;
  background-color: hsla(0,0%,0%,.1);
  transition: transform .3s ease, background .2s ease;
  outline: none;
  border:1px solid <?php echo $ins_bg_color; ?>;
}

.circular-menu .menu-item:hover {
  background-color: hsla(0,0%,0%,.3);
}

.circular-menu.active .menu-item {
  transition-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1.275);
}

.circular-menu.active .menu-item:nth-child(1) {
  transform: translate3d(0.8em,-5em,0);
}

.circular-menu.active .menu-item:nth-child(2) {
  transform: translate3d(-1.8em,-4.6em,0);
}

.circular-menu.active .menu-item:nth-child(3) {
  transform: translate3d(-3.8em,-3em,0);
}

.circular-menu.active .menu-item:nth-child(4) {
  transform: translate3d(-4.8em,-0.6em,0);
}

.circular-menu.active .menu-item:nth-child(5) {
  transform: translate3d(-4.2em,2em,0);
}

/**
 * The other theme for this menu
 */

.circular-menu.circular-menu-left {
  right: auto; 
  left: 1em;
}

.circular-menu.circular-menu-left .floating-btn {
  background-color: hsl(217, 89%, 61%);
}

.circular-menu.circular-menu-left:after {
  background-color: hsl(217, 89%, 61%);
}

.circular-menu.circular-menu-left.active .floating-btn i {
  transform: rotate(90deg);
}

.circular-menu.circular-menu-left.active .menu-item:nth-child(1) {
  transform: translate3d(-1em,-7em,0);
}

.circular-menu.circular-menu-left.active .menu-item:nth-child(2) {
  transform: translate3d(3.5em,-6.3em,0);
}

.circular-menu.circular-menu-left.active .menu-item:nth-child(3) {
  transform: translate3d(6.5em,-3.2em,0);
}

.circular-menu.circular-menu-left.active .menu-item:nth-child(4) {
  transform: translate3d(7em,1em,0);
}
  </style>
  <?php 

    if(empty($_SESSION['STU_ID']))
     {
          echo 
          "<script>
            let timerInterval
            Swal.fire({
              title: 'Auto Logout From System!',
              html: 'I will close in <b></b> milliseconds.',
              timer: 2000,
              timerProgressBar: true,
              didOpen: () => {
                Swal.showLoading()
                timerInterval = setInterval(() => {
                  const content = Swal.getContent()
                  if (content) {
                    const b = content.querySelector('b')
                    if (b) {
                      b.textContent = Swal.getTimerLeft()
                    }
                  }
                }, 100)
              },
              willClose: () => {
                clearInterval(timerInterval)
              }
            }).then((result) => {
              /* Read more about handling dismissals below */
              if (result.dismiss === Swal.DismissReason.timer) {

                window.location.href='logout.php';

              }
            })
          </script>";
    }

   ?>

</head>

<body data-spy="scroll" data-target="#scrollspy" data-offset="">


    <div class="page">
        <!--background -->
        <div class="bg">
            <ul class="bg-circles">
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
            </ul>
        </div>





        <div class="info-tab-area">
            <div class="container">


                <div class="info-tab-section">
                    <!--header-->
                    <div class="header d-flex align-items-center">
                        <div class="container">
                            <div class="row">
                                <div class="col-12">
                                    <div class="profile d-flex align-items-center">
                                        <div class="d-flex align-items-center">
                                            <img src="../../student/images/profile/<?php echo $picture; ?>" alt="">
                                            <a class="desc">
                                                <h5 class="text-white mb-0" style="text-transform: capitalize;"><?php echo $st_name ?> <span class="fa fa-check-circle"></span></h5>
                                                <small class="text-white mb-0" style="font-weight: bold;font-size: 18px;"> <?php echo $reg_id; ?></small>
                                            </a>
                                        </div>

                                        <!--social-links-->
                                        <div class="social-links ml-auto">
                                            <a href="#" class="text-white" data-toggle="modal" data-target="#logout" style="font-size: 20px;"><i class="fa fa-power-off"></i></a>
                                            
                                            </ul>
                                        </div>

                                        <!--btn-toggle-->
                                        <div class="btn-toggler ml-auto" id="btnToggle">
                                            <span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--info-tab-->
                    <div class="info-tab">
                        <!--navigation-->
                        <ul class="nav nav-tabs" id="myTab" role="tablist">

                            <li class="nav-item">
                                <a class="nav-link" href="../index.php" role="tab">
                                    <span style="padding-top: 6px;"><i class="fas fa-arrow-left"></i></span>
                                    <span>Back</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link <?php echo $active_pdf_tab; ?>" id="about-tab" data-toggle="tab" href="#about" role="tab" aria-controls="about" aria-selected="false">
                                    <span style="padding-top: 6px;"><i class="fas fa-file"></i></span>
                                    <span> PDF Tute ලබාගැනීම</span>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link <?php echo $active_ytd_tab; ?>" id="resume-tab" data-toggle="tab" href="#resume" role="tab" aria-controls="resume" aria-selected="false">
                                    <span style="padding-top: 6px;"><i class="fas fa-video"></i></span>
                                    <span>Youtube video</span>
                                </a>
                            </li>
                           
                            <li class="nav-item" style="display:none;">
                                <a class="nav-link" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="false">
                                    <span style="padding-top: 6px;"><i class="fas fa-volume-up"></i></span>
                                    <span>Audio</span>
                                </a>
                            </li>
                            
                            <li class="nav-item">
                                <a class="nav-link <?php echo $active_rec_tab; ?>" id="record-tab" data-toggle="tab" href="#record" role="tab" aria-controls="record" aria-selected="false">
                                    <span style="padding-top: 6px;"><i class="fas fa-circle text-danger"></i></span>
                                    <span>Recording</span>
                                </a>
                            </li>

                            <li class="nav-item" data-toggle="modal" data-target="#logout" style="cursor: pointer;">
                                <a class="nav-link" id="contact-tab" data-toggle="tab" role="tab" aria-controls="contact" aria-selected="false">
                                    <span style="padding-top: 4px;"><i class="fas fa-power-off"></i></span>
                                    <span>Logout</span>
                                </a>
                            </li>
                        </ul>


                        <!--tab-content-->
                        <div class="tab-content" id="myTabContent">
                            <!--Home-->



                            <!--about-->
                            <div class="tab-pane fade show <?php echo $active_pdf_tab; ?>" id="about" role="tabpanel" aria-labelledby="about-tab">
                                <div class="about section">
                                    
                                    <!-- Document Page  -->
                                    <div class="col-md-12" style="padding-left: 0;">
                                        <h5 class="text-muted"><span class="fas fa-user-tie"></span> <?php echo $teacher_full_name; ?>
                                        <br>
                                        <small style="font-size: 10px;padding-left: 24px;"><?php echo $teacher_qualification; ?></small>
                                    </h5>
                                    <h6 class="text-muted" style="text-transform"><span class="fa fa-check-circle"></span> <?php echo $my_class_name; ?></h6>
                                </div>
                                    
                                    <?php 

                                        include('document.php'); 

                                    ?>

                                    <!-- Document Page  -->

                                    <!-- Company Detals -->
                                    <div class="col-md-12" style="border-top:1px solid #cccc;padding-top: 10px;">
                                    <a data-target="#company_details" data-toggle="modal" class="text-center" style="cursor: pointer;"><center><small class="col-md-12 text-center"><span class="fa fa-paw"></span> 2015-<?php echo date('Y'); ?> All Rights Reserved. ©IBS Developer Team</small></center></a></div>
                                    <!-- Company Detals -->
                                </div>
                            </div>



                           <div class="tab-pane fade" id="home" role="tabpanel" aria-labelledby="contact-tab">
                                <div class="home section">

                                    <!-- Audio Page  -->

                                    <div class="col-md-12" style="padding-left: 0;"><h5 class="text-muted"><span class="fas fa-user-tie"></span> <?php echo $teacher_full_name; ?>
                                        <br>
                                        <small style="font-size: 10px;padding-left: 24px;"><?php echo $teacher_qualification; ?></small>
                                    </h5>

                                    <h6 class="text-muted" style="text-transform"><span class="fa fa-check-circle"></span> <?php echo $my_class_name; ?></h6>
                                    </div>

                                    <?php 

                                        include('audio.php');


                                     ?>

                                    <!-- Audio Page  -->


                                    <!-- Company Detals -->
                                    <div class="col-md-12" style="border-top:1px solid #cccc;padding-top: 10px;">
                                    <a data-target="#company_details" data-toggle="modal" class="text-center" style="cursor: pointer;"><center><small class="col-md-12 text-center"><span class="fa fa-paw"></span> 2015-<?php echo date('Y'); ?> All Rights Reserved. ©IBS Developer Team</small></center></a></div>
                                    <!-- Company Detals -->
    
                                </div>
                            </div>


                            <!--Resume-->
                            <div class="tab-pane fade show <?php echo $active_ytd_tab; ?>" id="resume" role="tabpanel" aria-labelledby="resume-tab">
                                <div class="services section">

                                    <!-- Video Page  -->


                                    <div class="col-md-12" style="padding-left: 0;"><h5 class="text-muted"><span class="fas fa-user-tie"></span> <?php echo $teacher_full_name; ?>
                                        <br>
                                        <small style="font-size: 10px;padding-left: 24px;"><?php echo $teacher_qualification; ?></small>
                                    </h5>

                                    <h6 class="text-muted" style="text-transform"><span class="fa fa-check-circle"></span> <?php echo $my_class_name; ?></h6>
                                    </div>

                                    <?php 


                                        include('video.php'); 


                                    ?>

                                    <!-- Video Page  -->

                                    <!-- Company Detals -->
                                    <div class="col-md-12" style="border-top:1px solid #cccc;padding-top: 10px;">
                                    <a data-target="#company_details" data-toggle="modal" class="text-center" style="cursor: pointer;"><center><small class="col-md-12 text-center"><span class="fa fa-paw"></span> 2015-<?php echo date('Y'); ?> All Rights Reserved. ©IBS Developer Team</small></center></a></div>
                                    <!-- Company Detals -->
                                </div>
                            </div>


                            <!--Recording-->
                            <div class="tab-pane fade show <?php echo $active_rec_tab; ?>" id="record" role="tabpanel" aria-labelledby="record-tab">
                                <div class="services section">

                                    <!-- Video Page  -->


                                    <div class="col-md-12" style="padding-left: 0;"><h5 class="text-muted"><span class="fas fa-user-tie"></span> <?php echo $teacher_full_name; ?>
                                        <br>
                                        <small style="font-size: 10px;padding-left: 24px;"><?php echo $teacher_qualification; ?></small>
                                    </h5>
                                    
                                    <h6 class="text-muted" style="text-transform"><span class="fa fa-check-circle"></span> <?php echo $my_class_name; ?></h6>
                                    </div>

                                    <?php 


                                        include('zoom_record.php'); 


                                    ?>

                                    <!-- Video Page  -->

                                    <!-- Company Detals -->
                                    <div class="col-md-12" style="border-top:1px solid #cccc;padding-top: 10px;">
                                    <a data-target="#company_details" data-toggle="modal" class="text-center" style="cursor: pointer;"><center><small class="col-md-12 text-center"><span class="fa fa-paw"></span> 2015-<?php echo date('Y'); ?> All Rights Reserved. ©IBS Developer Team</small></center></a></div>
                                    <!-- Company Detals -->
                                </div>
                            </div>
   

                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>



<div id="logout" class="modal fade" role="dialog" style="margin-top: 120px;">
  <div class="modal-dialog"  style="padding: 10px 10px 10px 10px;">

    <!-- Modal content-->
    <div class="modal-content" style="padding: 10px 10px 10px 10px;">
      <div class="modal-header" style="border:none;">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
          <center><span class="far fa-power-off base-color" style="font-size: 60px;margin-bottom: 10px; "></span></center>

        <h3><center>පිටවීම</center></h3>

        <h5> 
            <center><label style="color:#565656;">ඔබගේ ගිණුමෙන් පිටවීමට අවශ්‍යද?</label></center>
        </h5>
      </div>
          <div class="row pb-10">
              <div class="col-md-12">
                  <a href="../logout.php" class="btn btn-custom btn-block" style="border-radius: 0px;"><i class="fa fa-check-circle"></i> ඔව්</a>
              </div>

              <div class="col-md-12 mt-10">
                  <button class="btn btn-default btn-block" data-dismiss="modal" style="border-radius: 0px;background-color: #8e8386;color: white"><span class="fa fa-times-circle"></span> නැත</button>
              </div>
          </div>
    </div>

  </div>
</div>
    <!--loading -->
    <div class="loading">
        <div class="circle"></div>
    </div>



    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="../assets/js/jquery-3.5.0.min.js"></script>
    <script src="../assets/js/popper.min.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>

    <!-- JS Plugins  -->
    <script src="../assets/js/jquery.magnific-popup.min.js"></script>
    <script src="../assets/js/ajax-contact.js"></script>
    <script src="../assets/js/owl.carousel.min.js"></script>
    <script src="../assets/js/main.js"></script>

</body>


<!-- Mirrored from dongo.netlify.app/dongo/index-2.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 24 Dec 2020 18:01:31 GMT -->
</html>

<!-- Document Info Modal -->
<div id="document_info" class="modal fade" role="dialog" style="margin-top:60px;">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Document Info</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">

        <script type="text/javascript">
            function document_info(doc_title,doc_desc,doc_up_time) 
            {
                document.getElementById('doc_title').innerHTML = '<h6>'+doc_title+'</h6>';
                document.getElementById('doc_desc').innerHTML = '<h6>'+doc_desc+'</h6>';
                document.getElementById('doc_up_time').innerHTML = '<h6>'+doc_up_time+'</h6>';
            }
        </script>

        <table class="table">
            <tr>
                <td colspan="2" style="border-top:1px solid white;">
                    <div class="section-title">
                        <h5>Title</h5>
                    </div>
                <h6 id="doc_title"></h6></td>
            </tr>

            <tr>
                <td colspan="2" style="border-top:1px solid white;">
                    <div class="section-title">
                        <h5>Description</h5>
                    </div>
                <div id="doc_desc" style="height: 80px;overflow: auto;"></div></td>
            </tr>


            <tr>
                <td colspan="2" style="border-top:1px solid white;">
                    <div class="section-title">
                        <h5>Upload Time</h5>
                    </div>
                <h6 id="doc_up_time"></h6></td>
            </tr>

        </table>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<!-- Document Info Modal -->

<!-- Audio Info Modal -->
<div id="audio_info" class="modal fade" role="dialog" style="margin-top:60px;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Audio Info</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">

        <script type="text/javascript">
            function audio_info(audio_title,audio_desc,audio_up_time) 
            {
                document.getElementById('audio_title').innerHTML = '<h6>'+audio_title+'</h6>';
                document.getElementById('audio_desc').innerHTML = '<h6>'+audio_desc+'</h6>';
                document.getElementById('audio_up_time').innerHTML = '<h6>'+audio_up_time+'</h6>';
            }
        </script>

        <table class="table">
            <tr>
                <td colspan="2" style="border-top:1px solid white;">
                    <div class="section-title">
                        <h5>Title</h5>
                    </div>
                <h6 id="audio_title"></h6></td>
            </tr>

            <tr>
                <td colspan="2" style="border-top:1px solid white;">
                    <div class="section-title">
                        <h5>Description</h5>
                    </div>
                <div id="audio_desc" style="height: 80px;overflow: auto;"></div>
            </tr>


            <tr>
                <td colspan="2" style="border-top:1px solid white;">
                    <div class="section-title">
                        <h5>Upload Time</h5>
                    </div>
                <h6 id="audio_up_time"></h6></td>
            </tr>

        </table>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<!-- Audio Info Modal -->



<!-- Video Info Modal -->
<div id="video_info" class="modal fade" role="dialog" style="margin-top:60px;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Video Info</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">

        <script type="text/javascript">
            function video_info(video_title,video_desc,video_up_time) 
            {
                document.getElementById('video_title').innerHTML = '<h6>'+video_title+'</h6>';
                document.getElementById('video_desc').innerHTML = '<h6>'+video_desc+'</h6>';
                document.getElementById('video_up_time').innerHTML = '<h6>'+video_up_time+'</h6>';
            }
        </script>

        <table class="table">
            <tr>
                <td colspan="2" style="border-top:1px solid white;">
                    <div class="section-title">
                        <h5>Title</h5>
                    </div>
                <h6 id="video_title"></h6></td>
            </tr>

            <tr>
                <td colspan="2" style="border-top:1px solid white;">
                    <div class="section-title">
                        <h5>Description</h5>
                    </div>
                <div id="video_desc" style="height: 80px;overflow: auto;"></div>
                </td>
            </tr>


            <tr>
                <td colspan="2" style="border-top:1px solid white;">
                    <div class="section-title">
                        <h5>Upload Time</h5>
                    </div>
                <h6 id="video_up_time"></h6></td>
            </tr>

        </table>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<!-- Video Info Modal -->


<div id="company_details" class="modal fade" role="dialog" style="padding: 10px 10px 10px 10px;margin-top: 80px;">
      <div class="modal-dialog">

        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Roboto+Slab&display=swap" rel="stylesheet">


        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header" style="border:none;">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body" style="height: auto;font-family: 'Roboto Slab', serif;">

            <div class="row" style="padding-top: 0px;">
                <div class="col-md-4">
                </div>
                <div class="col-md-4">
                            <center> 
                                <img src="../assets/img/ibs.png" id="logo" style="height: auto;width: 100%;border-radius: 80px;">
                            </center>
                        
                </div>
                <div class="col-md-4">

                </div>
            </div>
        
            <center><small class="text-muted">Developed By</small></center>
            <center>
                <h4>Interlink Business Solutions</h4></center>

            <center><label class="text-muted" style="color:#565656;font-size: 16px;font-weight: initial;"><span class="fa fa-map-marker"></span> No 06, Kumaradasa Road, Matara.</label></center>
            <center>
                <label class="text-muted"><span class="fa fa-phone" style="-ms-transform: rotate(90deg); /* IE 9 */
  transform: rotate(90deg); /* Standard syntax */"></span> +94 777 650 120</label>
                <br>
                <label class="text-muted"><a href="https://www.ibslanka.com/" target="_blank"><span class="fa fa-globe"></span> https://www.ibslanka.com</a></label>
                
            </center>
          </div>
        </div>

      </div>
    </div>


    <div id="circularMenu" class="circular-menu base-color">

  <a class="floating-btn base-color" onclick="document.getElementById('circularMenu').classList.toggle('active');" style="padding-top: 16px;">
    <i class="fas fa-plus" style="color: white;"></i>
  </a>

  <menu class="items-wrapper">
    <a href="#" class="menu-item base-color" data-title="Document" data-toggle="tooltip" onclick="click_file();"><i class="fas fa-file"></i></a>
    <a href="#" class="menu-item base-color" data-title="Video" data-toggle="tooltip" onclick="click_video();"><i class="fas fa-video"></i></a>
    <a href="../logout.php" onclick="return confirm('Are you sure logout?');" class="menu-item base-color" data-title="Logout" data-toggle="tooltip"><i class="fas fa-power-off"></i></a>
    <a href="../index.php" class="menu-item base-color" data-title="Back" data-toggle="tooltip"><i class="fas fa-arrow-left"></i></a>
  </menu>

</div>

<script type="text/javascript">
    function click_file() {
        document.getElementById('about-tab').click();
    }

    function click_video() {
        document.getElementById('resume-tab').click();
    }

    function click_audio() {
        document.getElementById('home-tab').click();
    }
</script>