 
  <div class="row" style="margin-top: 4px;margin-bottom: 8px;">
      <div class="col-md-8">
          <div class="section-title">
              <h5>Examination</h5>
          </div>




          <div class="section-sub-title mb-10">
              <h6 id="exam_name">
          <?php 

          if($exam_tab == '0')
          {
            $exam_tab = '1';
          }

          if($exam_tab == '1')
          {
             /* $select_option = '

                <select class="form-control" onchange="exam_type('.$_SESSION['STU_ID'].');" id="exam_type">
                  <option value="1">MCQ Paper</option>
                  <option value="2">Structured & Essay Paper</option>
                  <option value="3">Results</option>
                </select> 

              ';*/

              echo 'MCQ Paper';

              $mcq_tab = 'display:show';
              $str_tab = 'display:none';
              $result_tab = 'display:none';
          }

          if($exam_tab == '2')
          {
             /* $select_option = '

                <select class="form-control" onchange="exam_type('.$_SESSION['STU_ID'].');" id="exam_type">
                  <option value="2">Structured & Essay Paper</option>
                  <option value="1">MCQ Paper</option>
                  <option value="3">Results</option>
                </select> 

              ';*/

              echo 'Structured & Essay Paper';

              $mcq_tab = 'display:none';
              $str_tab = 'display:show';
              $result_tab = 'display:none';

          }


          if($exam_tab == '3')
          {
              /*$select_option = '

                <select class="form-control" onchange="exam_type('.$_SESSION['STU_ID'].');" id="exam_type">
                  <option value="3">Results</option>
                  <option value="2">Structured & Essay Paper</option>
                  <option value="1">MCQ Paper</option>
                </select> 

              ';*/

              echo 'Results Paper';


              $mcq_tab = 'display:none';
              $str_tab = 'display:none';
              $result_tab = 'display:show';
          }

           ?>
            </h6>
          </div>
      </div>
      <div class="col-md-4">

        <?php 

          //echo $select_option;

         ?>

      </div>
  </div>
  <div class="row mb-20" id="mcq" style="<?php echo $mcq_tab; ?>">
      <!-- item 1-->

      <?php
            


                          $hour = '2';
                          $minute = '30';

                          $mcq_marks001 = '<form action="#" method="POST">
                                                
                                                <button type="submit" class="btn btn-custom btn-block" id="attend_btn" style="font-weight: bold;"><i class="fas fa-arrow-right"></i>&nbsp;Sit Examination</button>

                                                </form>';

                            echo '

                              <div class="col-md-6" style="margin-top:10px;margin-bottom:10px;">
                                <div class="col-md-12" style="border:1px solid #cccc;padding:30px 20px 20px 40px;border-top-right-radius: 20px;border-bottom-left-radius: 20px;height:auto;">
                            
                                          <h5 class="text-dark" style="text-transform: capitalize;">Test Paper</h5>
                                          <small class="base-color" style="font-weight:500;">Sample Class</small> <br>
                                          <label style="text-transform: capitalize;">Mr.ABC</label>
                                          <p class="text-dark mb-0">
                                            <label style="font-weight:500;">2021-07-07 <label style="font-weight:700;">08.00 -  10.00 PM</label>
                                            <br>
                                            <label style="font-weight:500;">('.$hour.' Hour '.$minute.' Minute)</label> 

                                              <div style="padding-top:0;" class="clockdiv" data-date2="07 07, 2021 09:15 PM">
                                                  
                                                  <span class="days" style="font-weight:600;color:gray;">0</span>
                                                  <label class="smalltext" style="font-weight:600;color:gray;">Days </label>
                                                
                                                  <span class="hours" style="font-weight:600;color:gray;">0</span>
                                                  <label class="smalltext" style="font-weight:600;color:gray;">Hour</label>
                                                
                                                  <span class="minutes" style="font-weight:600;color:gray;">0</span>
                                                  <label class="smalltext" style="font-weight:600;color:gray;">Minute</label>
                                                
                                                  <span class="seconds" style="font-weight:600;color:gray;">0</span>
                                                  <label class="smalltext" style="font-weight:600;color:gray;">Second</label>
                                                  <br>
                                                  <label id="wait_time" style="text-shadow: uppercase;color: gray;"><img src="mcq/images/animation/loading2.svg"> Wait</label>

                                                  <button type="submit" class="btn btn-custom btn-block" disabled id="attend_btn" style="font-weight: bold;cursor:not-allowed"><i class="fas fa-arrow-right"></i>&nbsp;Sit Examination</button>
                                              </div>
                                            </label>
                                                  
                                          </p>
                                           
                                      </div>  
                              </div>

                            ';

                            echo '

                              <div class="col-md-6" style="margin-top:10px;margin-bottom:10px;">
                                <div class="col-md-12" style="border:1px solid #cccc;padding:30px 20px 20px 40px;border-top-right-radius: 20px;border-bottom-left-radius: 20px;height:auto;">
                            
                                          <h5 class="text-dark" style="text-transform: capitalize;">Test Paper</h5>
                                          <small class="base-color" style="font-weight:500;">Sample Class</small> <br>
                                          <label style="text-transform: capitalize;">Mr.ABC</label>
                                          <p class="text-dark mb-0">
                                            <label style="font-weight:500;">2021-07-07 <label style="font-weight:700;">08.00 -  10.00 PM</label>
                                            <br>
                                            <label style="font-weight:500;">('.$hour.' Hour'.$minute.')</label> 

                                              <div style="padding-top:0;" class="clockdiv" data-date2="07 07, 2021 09:15 PM">
                                                  
                                                  <span class="days" style="font-weight:600;color:gray;">0</span>
                                                  <label class="smalltext" style="font-weight:600;color:gray;">Days </label>
                                                
                                                  <span class="hours" style="font-weight:600;color:gray;">0</span>
                                                  <label class="smalltext" style="font-weight:600;color:gray;">Hour</label>
                                                
                                                  <span class="minutes" style="font-weight:600;color:gray;">0</span>
                                                  <label class="smalltext" style="font-weight:600;color:gray;">Minute</label>
                                                
                                                  <span class="seconds" style="font-weight:600;color:gray;">0</span>
                                                  <label class="smalltext" style="font-weight:600;color:gray;">Second</label>

                                                  <br>
                                                  <label id="start_time" class="text-success" style="text-shadow: uppercase;font-weight:600;padding-bottom:2px;">
                                                  <span class="fa fa-play"></span> Started</label>

                                                  '.$mcq_marks001.'
                                                

                                              </div>
                                            </label>
                                                  
                                          </p>
                                           
                                      </div>  
                              </div>

                            ';


                            echo '

                              <div class="col-md-6" style="margin-top:10px;margin-bottom:10px;">
                                <div class="col-md-12" style="border:1px solid #cccc;padding:30px 20px 20px 40px;border-top-right-radius: 20px;border-bottom-left-radius: 20px;height:auto;">
                            
                                          <h5 class="text-dark" style="text-transform: capitalize;">Test Paper</h5>
                                          <small class="base-color" style="font-weight:500;">Sample Class</small> <br>
                                          <label style="text-transform: capitalize;">Mr.ABC</label>
                                          <p class="text-dark mb-0">
                                            <label style="font-weight:500;">2021-07-07 <label style="font-weight:700;">08.00 -  10.00 PM</label>
                                            <br>
                                            <label style="font-weight:500;">('.$hour.' Hour'.$minute.')</label> 

                                              <div style="padding-top:0;" class="clockdiv2" data-date2="07 07, 2021 09:15 PM">
                                                  
                                                  <span class="days" style="font-weight:600;color:gray;">0</span>
                                                  <label class="smalltext" style="font-weight:600;color:gray;">Days </label>
                                                
                                                  <span class="hours" style="font-weight:600;color:gray;">0</span>
                                                  <label class="smalltext" style="font-weight:600;color:gray;">Hour</label>
                                                
                                                  <span class="minutes" style="font-weight:600;color:gray;">0</span>
                                                  <label class="smalltext" style="font-weight:600;color:gray;">Minute</label>
                                                
                                                  <span class="seconds" style="font-weight:600;color:gray;">0</span>
                                                  <label class="smalltext" style="font-weight:600;color:gray;">Second</label>

                                                  <br>

                                                  <label id="end_time" class="base-color" style="text-shadow: uppercase;font-weight:600;padding-bottom:2px;">
                                                  <span class="fa fa-check-circle"></span> Finished </label>
                                                

                                              </div>
                                            </label>
                                                  
                                          </p>
                                           
                                      </div>  
                              </div>

                            ';
                            

                            ?>

                              <script>

                              document.addEventListener('readystatechange', event => {
                                if (event.target.readyState === "complete") {
                                    var clockdiv = document.getElementsByClassName("clockdiv");

                                  var countDownDate = new Array();
                                    for (var i = 0; i < clockdiv.length; i++) {
                                        countDownDate[i] = new Array();
                                        countDownDate[i]['el'] = clockdiv[i];
                                        countDownDate[i]['time'] = new Date(clockdiv[i].getAttribute('data-date2')).getTime();

                                        countDownDate[i]['name'] = clockdiv[i].getAttribute('data-date3');
                                        countDownDate[i]['days'] = 0;
                                        countDownDate[i]['hours'] = 0;
                                        countDownDate[i]['seconds'] = 0;
                                        countDownDate[i]['minutes'] = 0;

                                        //alert(countDownDate[i]['name'])

                                    }

                                  var countdownfunction = setInterval(function() {
                                  for (var i = 0; i < countDownDate.length; i++) {
                                                  var now = new Date().getTime();
                                                  var distance = countDownDate[i]['time'] - now;

                                                   countDownDate[i]['days'] = Math.floor(distance / (1000 * 60 * 60 * 24));
                                                   countDownDate[i]['hours'] = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                                                   countDownDate[i]['minutes'] = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                                                   countDownDate[i]['seconds'] = Math.floor((distance % (1000 * 60)) / 1000);

                                                   

                                                   if (distance<0) 
                                                   {
      
                                                      countDownDate[i]['el'].querySelector('.days').innerHTML = 0;
                                                      countDownDate[i]['el'].querySelector('.hours').innerHTML = 0;
                                                      countDownDate[i]['el'].querySelector('.minutes').innerHTML = 0;
                                                      countDownDate[i]['el'].querySelector('.seconds').innerHTML = 0;

                                                  
                                                   }else
                                                   {
                                                        countDownDate[i]['el'].querySelector('.days').innerHTML = countDownDate[i]['days'];
                                                        countDownDate[i]['el'].querySelector('.hours').innerHTML = countDownDate[i]['hours'];
                                                        countDownDate[i]['el'].querySelector('.minutes').innerHTML = countDownDate[i]['minutes'];
                                                        countDownDate[i]['el'].querySelector('.seconds').innerHTML = countDownDate[i]['seconds'];

                                                        countDownDate[i]['el'].querySelector('#attend_btn').disabled = true;
                                                        countDownDate[i]['el'].querySelector('#wait_time').style.display = 'inline-block';
                                                        countDownDate[i]['el'].querySelector('#start_time').style.display = 'none';
                                                        countDownDate[i]['el'].querySelector('#end_time').style.display = 'none';

                                                  }

                                                  if(countDownDate[i]['seconds'] == '0' && countDownDate[i]['minutes'] == '0')
                                                  {
                                                    countDownDate[i]['el'].querySelector('#attend_btn').disabled = false;
                                                    countDownDate[i]['el'].querySelector('#wait_time').style.display = 'none';
                                                    countDownDate[i]['el'].querySelector('#start_time').style.display = 'inline-block';
                                                    countDownDate[i]['el'].querySelector('#end_time').style.display = 'none';

                                                    
                                                  }
                                    
                                       }
                                              }, 1000);
                                      }
                                  });

                            </script>

          
           
           

  </div>

  <div class="row mb-20" id="structured" style="<?php echo $str_tab; ?>">
      <!-- item 1-->


      <?php 

            $student_id = $_SESSION['STU_ID'];//Student ID

            $strto = strtotime('+3 Days');
            $threedays = date('Y-m-d',$strto); //Today from 3 days

            $today = date('Y-m-d'); //Today

            $now = date('Y-m-d h:i:s A'); //Now

            $i=0;
            

            

                  $sql001 = mysqli_query($conn,"SELECT * FROM `paper_master` WHERE `FINISH_DATE` BETWEEN '$today' AND '$threedays' AND `PAPER_TYPE` = '2' ORDER BY `FINISH_DATE` ASC"); //Check paper held dates
                  $check_paper = mysqli_num_rows($sql001);

                  if($check_paper > 0)
                  {
                    while($row001 = mysqli_fetch_assoc($sql001))
                    {
                        $paper_id = $row001['PAPER_ID'];
                        $class_id001 = $row001['CLASS_ID'];
                        $hour = $row001['TIME_DURATION_HOUR'];
                        $minute = $row001['TIME_DURATION_MIN'];
                        $teach_id = $row001['UPLOAD_BY'];
                        $paper_name = $row001['PAPER_NAME'];

                        /*Class Table*/

                         $sql0012 = mysqli_query($conn,"SELECT * FROM `classes` WHERE `CLASS_ID` = '$class_id001' "); //check Classes for registered teachers details
                          while($row0012 = mysqli_fetch_assoc($sql0012))
                          {

                              $class_name = $row0012['CLASS_NAME']; //Class Name
       
                          }

                        /*Class Table*/


                        $currentDate = strtotime(date('Y-m-d H:i:s'));
                        $last_attend = $row001['LAST_ATTENT_TIME'];
                        //$userLastActivity = strtotime($last_attend);
                        //$rest_min = round(abs($currentDate - $userLastActivity) / 60); //Find Two date using minute



                        $finish_date = $row001['FINISH_DATE'];
                        $finish_time = $row001['FINISH_TIME'];
                        $start_date = $row001['START_DATE'];
                        $start_time = $row001['START_TIME'];
                        $finish_dt = $row001['FINISH_D_T'];
                        $start_dt = $row001['START_D_T'];

                        $exam_last_finish_time_str = strtotime($last_attend);
                        $exam_last_finish_time = date('h:i A',$exam_last_finish_time_str);

                        

                        $exam_last_start_time_str = strtotime($start_time);
                        $exam_last_start_time = date('h:i A',$exam_last_start_time_str);

                        $exam_status = $row001['STATUS'];

                        if($minute == '00' || $minute == '0')
                        {
                          $minute = '';
                        }else
                        {
                          $minute = " ".$minute.' Min';
                        }

                        $check_date = strtotime($start_date);
                        $month = date('F',$check_date);
                        $day = date('d',$check_date);
                        $year = date('Y',$check_date);

                        $current_time = date('H:i:s');
                        $current_date = date('Y-m-d');

                        $finish_date_time = strtotime($finish_dt);
                        $today_date001 = strtotime($now);

                        //Update Finished Exam
                        if($finish_date_time<=$today_date001)
                        {
                            //mysqli_query($conn,"UPDATE `paper_master` SET `STATUS` = '1' WHERE `PAPER_ID`='$paper_id'");
                            $start_btn = 'display:none;';
                            $wait_btn = 'display:none;';
                            $end_btn = 'display:show;';
                            $exam_btn = 'display:none;';
                        }else
                        if($finish_date_time>$today_date001)
                        {
                          $start_btn = 'display:show;';
                          $wait_btn = 'display:none;';
                          $end_btn = 'display:none;';
                          $exam_btn = 'display:show;';
                        }

                        $sql002 = mysqli_query($conn,"SELECT * FROM `transactions` WHERE `STU_ID` = '$student_id' AND `CLASS_ID` = '$class_id001'"); //Institute Data assign to variables
                        $check_empty = mysqli_num_rows($sql002);

                        if($check_empty>0)
                        {

                            while($row002 = mysqli_fetch_assoc($sql002))
                            { 

                              $class_id = $row002['CLASS_ID'];



                        $sql0011 = mysqli_query($conn,"SELECT * FROM `teacher_details` WHERE `TEACH_ID` = '$teach_id' "); //check Classes for registered teachers details
                            while($row0011 = mysqli_fetch_assoc($sql0011))
                            {

                                $teacher_position = $row0011['POSITION']; //Teacher Position
                                $teacher_f_name = $row0011['F_NAME']; //Teacher First Name
                                $teacher_l_name = $row0011['L_NAME']; //Teacher Last Name
                                $teacher_picture = $row0011['PICTURE']; //Teacher Picture
                                $teacher_gender = $row0011['GENDER']; //Teacher Picture

                                //If Teacher Picture zero,Check gender and gender to select picture 
                                if($teacher_picture == '0')
                                {
                                    if($teacher_gender == 'Male')
                                    {
                                        $teacher_picture = 'b_teacher.jpg';
                                    }else
                                    if($teacher_gender == 'Female')
                                    {
                                        $teacher_picture = 'g_teacher.jpg';
                                    }
                                }
                                //If Teacher Picture zero,Check gender and gender to select picture

                        

                                $teacher_full_name = $teacher_position.". ".$teacher_f_name." ".$teacher_l_name;
         

                            }

                            $str = strtotime($start_time);
                            $mcq_start_time = date('h:i A',$str);

                            $disabled_attend = '';
                            $disabled_style = 'pointer-events:cursor;';
                            $student_mark = '';
                            $display = 'display:show';


                          $i = $i+1;

                          $sql0013 = mysqli_query($conn,"SELECT * FROM `student_essay_master` WHERE `STU_ID` = '$student_id' AND `PAPER_ID` = '$paper_id' AND `STATUS` = '1'"); //check Classes for registered teachers details
                          if(mysqli_num_rows($sql0013)>0)
                          {
                            while($row0013 = mysqli_fetch_assoc($sql0013))
                            {
                                $que_total = 1000/20;

                                $structured_marks = "<label style='font-size:20px;font-weight:bold;opacity:0.6999;'>Total Marks -&nbsp;</label><label class='text-success' style='font-size:24px;font-weight:bold;'> ".$row0013['TOTAL_MARKS']." /<label style='font-size:18px;'>".$que_total."</label></label>"; //Class Name
         
                            }
                          }else
                          if(mysqli_num_rows($sql0013) == '0')
                          {
                            $structured_marks = '';
                          }

                          

                            echo '

                              <div class="col-md-6" style="margin-top:10px;margin-bottom:10px;">
                                <div class="col-md-12" style="border:1px solid #cccc;padding:30px 20px 20px 40px;border-top-right-radius: 20px;border-bottom-left-radius: 20px;height:auto;">

                                <div class="right-divider hidden-sm hidden-xs">
                              <img src="assets/img/exam_bg.png" style="opacity: 0.04;width: 130px;border-radius: 0px;padding-right: 0px;position: absolute;right:0;top:0;">
                            </div>
                            

                                          <h5 class="text-dark" style="text-transform: capitalize;">'.$paper_name.'</h5>
                                          <small class="base-color" style="font-weight:500;">'.$class_name.'</small> <br>
                                          <label style="text-transform: capitalize;">'.$teacher_full_name.'</label>
                                          <p class="text-dark mb-0">
                                            <label style="font-weight:500;">'.$start_date.' <label style="font-weight:700;">'.$exam_last_start_time.' - '.$exam_last_finish_time.'</label> <br>
                                            <label style="font-weight:500;">('.$hour.' Hour'.$minute.')</label> 

                                              <div style="padding-top:0;" class="clockdiv" data-date2="'.$month.' '.$day.', '.$year.' '.$start_time.'" data-date3="'.$paper_id.'">
                                                  
                                                  <span class="days" style="font-weight:600;color:gray;">0</span>
                                                  <label class="smalltext" style="font-weight:600;color:gray;">Days </label>
                                                
                                                  <span class="hours" style="font-weight:600;color:gray;">0</span>
                                                  <label class="smalltext" style="font-weight:600;color:gray;">Hour</label>
                                                
                                                  <span class="minutes" style="font-weight:600;color:gray;">0</span>
                                                  <label class="smalltext" style="font-weight:600;color:gray;">Minute</label>
                                                
                                                  <span class="seconds" style="font-weight:600;color:gray;">0</span>
                                                  <label class="smalltext" style="font-weight:600;color:gray;">Second</label>

                                                  <br>
                                                  <label id="start_time" class="text-success" style="text-shadow: uppercase;font-weight:600;padding-bottom:2px;'.$start_btn.'">
                                                  <span class="fa fa-play"></span> Started</label>

                                                  <label id="end_time" class="base-color" style="text-shadow: uppercase;font-weight:600;padding-bottom:2px;'.$end_btn.'">
                                                  <span class="fa fa-check-circle"></span> Finished </label>
                                                  <br>
                                                  '.$structured_marks.'

                                                  <label id="wait_time" style="text-shadow: uppercase;color: gray;'.$wait_btn.'"><img src="str/images/animation/loading2.svg"> Wait</label>

                                                  <form action="str/check_data.php" method="POST">

                                                  <input type="hidden" name="student_id" value="'.$student_id.'">
                                                  <input type="hidden" name="teacher_id" value="'.$teach_id.'">
                                                  <input type="hidden" name="class_id" value="'.$class_id.'">
                                                  <input type="hidden" name="last_attend" value="'.$last_attend.'">
                                                  <input type="hidden" name="paper_id" value="'.$paper_id.'">
                                                
                                                <button type="submit" class="btn btn-custom btn-block '.$disabled_attend.'" id="attend_btn" style="font-weight: bold;'.$disabled_style.';'.$exam_btn.'"><i class="fas fa-arrow-right"></i>&nbsp;Examination</button>

                                                </form>
                                                

                                              </div>
                                            </label>
                                                  
                                          </p>
                                           
                                      </div>  
                              </div>

                            ';

                            ?>

                              <script>

                              document.addEventListener('readystatechange', event => {
                                if (event.target.readyState === "complete") {
                                    var clockdiv = document.getElementsByClassName("clockdiv");

                                  var countDownDate = new Array();
                                    for (var i = 0; i < clockdiv.length; i++) {
                                        countDownDate[i] = new Array();
                                        countDownDate[i]['el'] = clockdiv[i];
                                        countDownDate[i]['time'] = new Date(clockdiv[i].getAttribute('data-date2')).getTime();

                                        countDownDate[i]['name'] = clockdiv[i].getAttribute('data-date3');
                                        countDownDate[i]['days'] = 0;
                                        countDownDate[i]['hours'] = 0;
                                        countDownDate[i]['seconds'] = 0;
                                        countDownDate[i]['minutes'] = 0;

                                        //alert(countDownDate[i]['name'])

                                    }

                                  var countdownfunction = setInterval(function() {
                                  for (var i = 0; i < countDownDate.length; i++) {
                                                  var now = new Date().getTime();
                                                  var distance = countDownDate[i]['time'] - now;

                                                   countDownDate[i]['days'] = Math.floor(distance / (1000 * 60 * 60 * 24));
                                                   countDownDate[i]['hours'] = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                                                   countDownDate[i]['minutes'] = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                                                   countDownDate[i]['seconds'] = Math.floor((distance % (1000 * 60)) / 1000);

                                                   

                                                   if (distance<0) 
                                                   {
      
                                                      countDownDate[i]['el'].querySelector('.days').innerHTML = 0;
                                                      countDownDate[i]['el'].querySelector('.hours').innerHTML = 0;
                                                      countDownDate[i]['el'].querySelector('.minutes').innerHTML = 0;
                                                      countDownDate[i]['el'].querySelector('.seconds').innerHTML = 0;

                                                  
                                                   }else
                                                   {
                                                        countDownDate[i]['el'].querySelector('.days').innerHTML = countDownDate[i]['days'];
                                                        countDownDate[i]['el'].querySelector('.hours').innerHTML = countDownDate[i]['hours'];
                                                        countDownDate[i]['el'].querySelector('.minutes').innerHTML = countDownDate[i]['minutes'];
                                                        countDownDate[i]['el'].querySelector('.seconds').innerHTML = countDownDate[i]['seconds'];

                                                        countDownDate[i]['el'].querySelector('#attend_btn').disabled = true;
                                                        countDownDate[i]['el'].querySelector('#wait_time').style.display = 'inline-block';
                                                        countDownDate[i]['el'].querySelector('#start_time').style.display = 'none';
                                                        countDownDate[i]['el'].querySelector('#end_time').style.display = 'none';

                                                  }

                                                  if(countDownDate[i]['seconds'] == '0' && countDownDate[i]['minutes'] == '0')
                                                  {
                                                    countDownDate[i]['el'].querySelector('#attend_btn').disabled = false;
                                                    countDownDate[i]['el'].querySelector('#wait_time').style.display = 'none';
                                                    countDownDate[i]['el'].querySelector('#start_time').style.display = 'inline-block';
                                                    countDownDate[i]['el'].querySelector('#end_time').style.display = 'none';

                                                    if(countDownDate[i]['name'] == <?php echo $paper_id ?>)
                                                    {
                                                      
                                                      setTimeout(function(){ notifyMe(); }, 500);
                                                    
    
                                                        document.addEventListener('DOMContentLoaded', function () {
                                                          if (Notification.permission !== "granted")
                                                            Notification.requestPermission();
                                                        });

                                                        function notifyMe() {
                                                          if (!Notification) {
                                                            //alert('Desktop notifications not available in your browser.'); 
                                                            return;
                                                          }

                                                          if (Notification.permission !== "granted")
                                                            Notification.requestPermission();
                                                          else {
                                                            var notification = new Notification('<?php echo $teacher_full_name; ?>(<?php echo $class_name; ?>)', {
                                                              icon: '../teacher/images/profile/<?php echo $teacher_picture; ?>',
                                                              body: "<?php echo $paper_name; ?> has been started. Please sit for it.",
                                                            });

                                                            notification.onclick = function () {
                                                                
                                                                countDownDate[i]['el'].querySelector('#attend_btn').click;
                                                            };

                                                          }

                                                        }
                                                    }

                                                    
                                                  }
                                    
                                       }
                                              }, 1000);
                                      }
                                  });

                            </script>
         
                          <?php

                        }

                      }
                        
                        /*$student_id = $_SESSION['STU_ID'];
                        
                        $sql00211 = mysqli_query($conn,"SELECT * FROM `transactions` WHERE `STU_ID` = '$student_id' AND `CLASS_ID` = '$class_id001'"); //Institute Data assign to variables
                        $check_ava_class = mysqli_num_rows($sql00211);
                        
                        if($check_ava_class == '0')
                        {
                        ?>


                            <div class="col-md-12 row" style="padding: 10px 10px 10px 10px;">
                                <div class="col-md-3"></div>
                                <div class="col-md-6">
                                        <center>
                                            <img src="assets/img/icon/unavailable.svg" style="width: 250px;height: 250px;">
                                            <h5 class="mt-20 text-muted"><span class="fas fa-search"></span> Examination Not Found!</h5>
                                        </center>
                                </div>
                                <div class="col-md-3"></div>
                            </div>


                            <?php
                        }*/
                        
                  }
              
            }else
              if($check_paper == '0')
              {
                ?>


                    <div class="col-md-12 row" style="padding: 10px 10px 10px 10px;">
                        <div class="col-md-3"></div>
                        <div class="col-md-6">
                                <center>
                                    <img src="assets/img/icon/unavailable.svg" style="width: 250px;height: 250px;">
                                    <h5 class="mt-20 text-muted"><span class="fas fa-search"></span> Examination Not Found!</h5>
                                </center>
                        </div>
                        <div class="col-md-3"></div>
                    </div>


                    <?php
                
              }
      ?>

          
           
           

  </div>

<div class="row mb-20" id="result123" style="<?php echo $result_tab; ?>">
      <!-- item 1-->
   <div class="col-md-12">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  <!-- <script type="text/javascript">

      function printDiv(divName) {
           var printContents = document.getElementById(divName).innerHTML;
           var originalContents = document.body.innerHTML;

           document.body.innerHTML = printContents;

           document.body.innerHTML = originalContents;

           window.print();
          window.onafterprint = function() {

          };
           

      }
 </script> 
    <button class="btn btn-success btn-sm mb-10 pull-right" style="float: right;"  onclick="printDiv('printableArea123')"><span class="fa fa-print"></span> Print Result</button>  -->
  

   <div class="table-responsive" style="overflow: auto;height: 300px;font-size: 8px;">
     <table class="table">
       <thead class="text-muted">
         <th class="text-left" style="width: 32%;">Exam Date</th><!-- 
         <th class="text-center">Subject Name</th> -->
         <th class="text-left">Paper Name</th>
         <th class="text-center">Mark</th>
       </thead>
       <tbody class="text-muted">

        <?php 
          $student_id = $_SESSION['STU_ID'];
          $sql003 = mysqli_query($conn,"SELECT * FROM `exam_result` WHERE `STU_ID` = '$student_id' ORDER BY `ADD_TIME` ASC");
          $check001 = mysqli_num_rows($sql003);

          if($check001 > 0)
          {
            while($row003 = mysqli_fetch_assoc($sql003))
            {
              $ex_id = $row003['EXAM_RESULT_ID'];

              $exam_date = $row003['START_DATE'];
              $mcq_mark = $row003['MCQ_MARK'];
              $se_mark = $row003['SE_MARK'];
              $total_mark = $row003['TOTAL_MARK'];

              $exam_mcq_paper_name = $row003['MCQ_PAPER_NAME'];
              $exam_se_paper_name = $row003['SE_PAPER_NAME'];

              $exam_paper_name = 'N/A';

              if($exam_mcq_paper_name !== '0' && $exam_se_paper_name == '0')
              {
                $exam_paper_name = $exam_mcq_paper_name;
              }else
              if($exam_mcq_paper_name == '0' && $exam_se_paper_name !== '0')
              {
                $exam_paper_name = $exam_se_paper_name;
              }else
              if($exam_mcq_paper_name !== '0' && $exam_se_paper_name !== '0')
              {
                $exam_paper_name = $exam_mcq_paper_name." <b>+</b> ".$exam_se_paper_name;
              }


              $add_time = $row003['ADD_TIME'];

              $class_id = $row003['CLASS_ID'];

              $sql004 = mysqli_query($conn,"SELECT * FROM `classes` WHERE `CLASS_ID` = '$class_id'");
              while($row004 = mysqli_fetch_assoc($sql004))
              {
                $class_name001 = $row004['CLASS_NAME'];
                $sub_id = $row004['SUB_ID'];
                
                $sql005 = mysqli_query($conn,"SELECT * FROM `subject` WHERE `SUB_ID` = '$sub_id'");
                while($row005 = mysqli_fetch_assoc($sql005))
                {
                  $subject_name001 = $row005['SUBJECT_NAME'];
                }

              }


              if($mcq_mark > 0 && $se_mark > 0)
              {
                $total_mark = $total_mark;
              }


              echo '

                <tr>
                 <td class="text-left" style="font-size: 15px;line-height:1.6;"><b>'.$exam_date.'</b></td>
                 <td class="text-left" style="font-size: 15px;line-height:1.6;"><b>'.$exam_paper_name.'</b></td>
                 <td class="text-center" style="cursor:pointer;"><h3 class="badge badge-danger" style="font-size:22px;border-radius:10px;" id="tglr'.$ex_id.'"  data-placement="top" class="popover-toggle" title="Examination Marks">'.$total_mark.'
                 </h3>

                  <div id="customdiv'.$ex_id.'" style="display: none;">
                  ';


                    if($mcq_mark > 0)
                    {
                      $mcq_mark = $mcq_mark/2;

                      if($mcq_mark < 10)
                      {
                        $mcq_mark = '0'.$mcq_mark;
                      }

                      echo '<div style="text-align:left;">MCQ Paper Marks - <b style="text-align:right;">'.$mcq_mark.'</b></div>';
                    }

                    if($se_mark > 0)
                    {
                      $se_mark = $se_mark/20;

                      if($se_mark < 10)
                      {
                        $se_mark = '0'.$se_mark;
                      }
                      
                      echo '<div style="text-align:left;">S & E Paper Marks - <b style="text-align:right;">'.$se_mark.'</b></div>';
                    }

                    if($se_mark == '0' && $mcq_mark == '0')
                    {
                      
                      echo '<div style="text-align:center;" class="text-danger"><b style="text-align:center;">Not Available</b></div>';
                    }



                echo '</div>

                 </td>

               </tr>';
              ?>
              <script type="text/javascript">
                $(document).ready(function(){
                    $('#tglr<?php echo $ex_id; ?>').popover({
                      html : true, 
                      content: function() {
                        return $('#customdiv<?php echo $ex_id; ?>').html();
                      } 
                  });  
                });
                </script>

              <?php

              //<td class="text-center" style="font-size: 15px;line-height:1.6;"><b>'.$subject_name001.'</b></td>
            }


          }else
          if($check001 == '0')
          {
            echo '
              <tr>
                <td colspan="6" class="text-center text-danger"><span class="fa fa-warning"></span> Not Found Data!</td>
              </tr>
            ';
          }

         ?>
         
       </tbody>
     </table> 
   </div> 

   </div>
  </div>

  <script type="text/javascript">
    function search_payment() {

      var year = document.getElementById('year').value;

      var month = document.getElementById('month').value;

      var student_id = document.getElementById('student_id').value;
      
      if(year !== '' && month !== '')
      {
        $.ajax({  //AJAX USING SENT INPUT DATA
                url:"../student/query/check.php",  //check username in section/insert/insert.php
                method:"POST",  
                data:{search_payments:student_id,year:year,month:month}, 
                success:function(data)
                {  
                    document.getElementById('search_result').innerHTML = data;
                }
            });
      }else
      if(year == '' || month == '')
      {
        Swal.fire({
          position: 'top-middle',
          type: 'error',
          icon: 'error',
          title: '<h4><b>ඔබ ගෙවීම් සිදුකල අදාල වර්ෂය හෝ මාසය තෝරන්න.</b></h4>',
          });
      }




      // body...
    }
  </script>

  <script type="text/javascript">
    
    function exam_type(student_id)
    {
      var exam = document.getElementById('exam_type').value;

      //alert(exam)

      if(exam == '1')
      {

        $('#mcq').show(600);
        $('#structured').hide(600);
        $('#result123').hide(600);

        document.getElementById('exam_name').innerHTML = 'MCQ Paper';



      }
      if(exam == '2')
      {
        $('#mcq').hide(600);
        $('#structured').show(600);
        $('#result123').hide(600);
        document.getElementById('exam_name').innerHTML = 'Structured & Essay Paper';



      }
      if(exam == '3')
      {

        $('#result123').show(600);
        $('#mcq').hide(600);
        $('#structured').hide(600);
        document.getElementById('exam_name').innerHTML = 'Results';

      }


        $.ajax({  //AJAX USING SENT INPUT DATA
            url:"../student/query/update.php",  //check username in section/insert/insert.php
            method:"POST",  
            data:{exam_type:exam,student_id:student_id}, 
            success:function(data)
            {  

            }
        });
    }
  </script>


  