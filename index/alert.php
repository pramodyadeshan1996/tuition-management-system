    
    <div class="row">
        <div class="col-md-12">
            <div class="section-title">
                <h5>දැනුම් දීම්</h5>
            </div>
        </div>
    </div>
    <div class="row">
        <!--item-->
        <?php 

            $hide = '';

            $sql001 = mysqli_query($conn,"SELECT * FROM `alert` WHERE `AUDIENCE` = 'Public' AND `PUBLISH` = 'Yes' ORDER BY `ALERT_ID` DESC LIMIT 8");
            $check_alert = mysqli_num_rows($sql001);
            if($check_alert > 0)
            {
                while($row001 = mysqli_fetch_assoc($sql001))
                {
                    $title = $row001['TITLE'];
                    $msg = $row001['MESSAGE'];
                    $add_date = $row001['ADDED_DATE'];
                    $add_time = $row001['PUB_DATE'];

                    if($msg == '' || $msg == '0')
                    {
                        $hide = 'display:none;';
                    }

                    $strtime = strtotime($add_date);
                    $month = substr(date('F',$strtime),0,3);
                    $day = date('d',$strtime);

                    echo '

                        <div class="col-lg-6 col-md-12">
                            <div class="item" style="border:1px solid #ccc;">
                                <div class="image">
                                    <center><img src="assets/img/icon/alert.svg" alt="" style="width: 200px;height: 200px;"></center>
                                </div>
                                <div class="content">
                                    <div class="date">
                                        <a>'.$day.'
                                            <span class="mb-0 text-white">'.$month.'</span>
                                        </a>
                                    </div>

                                    <h5 class="title mt-20 mb-10">
                                        <label class="text-dark" style="font-size:18px;"><strong>'.$title.'</strong></label>
                                        <br><label style="font-size:12px;" class="base-color">'.$add_time.'</label>
                                    </h5>';


                                        ?>

                                        <div class="row">
                                            <div class="col-md-6" style="padding-left: 0;">
                                                <button type="button" class="btn btn-link text-muted" onclick="alert_modal('<?php echo $msg ?>','<?php echo $title ?>','<?php echo $add_time; ?>');"  data-toggle="modal" data-target="#alert_modal" style="cursor: pointer;outline: none;border:none;color:black;<?php echo $hide; ?>">Read Message..</button>
                                            </div>
                                            <div class="col-md-6"></div>
                                        </div>
                                        <?php
                                    echo '
                                </div>
                            </div>
                        </div>
                    ';
                }
            }else
            {
                /*Empty Alert*/
                    ?>


                    <div class="col-md-12 row" style="padding: 10px 10px 10px 10px;">
                        <div class="col-md-3"></div>
                        <div class="col-md-6">
                                <center>
                                    <img src="assets/img/icon/unavailable.svg" style="width: 250px;height: 250px;">
                                    <h6 class="mt-20 text-muted"><span class="fas fa-search"></span> Alert Not Found! </h6>
                                </center>
                        </div>
                        <div class="col-md-3"></div>
                    </div>


                    <?php

                /*Empty Alert*/
            }

         ?>
        
    </div><!-- 
    <div class="row mb-20">
        <div class="col-12">
            <div class="pagination-nav">
                <ul class="pagination">
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#"><i class="fas fa-angle-double-right"></i></a></li>
                </ul>
            </div>
        </div>
    </div> -->