

<?php
  $page = "Google Form";
  $folder_in = '3';

  include('header/header.php');

  $teach_id001 = $_SESSION['TEACH_ID']; 

 ?>

    <ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-bottom: 04%;margin-bottom: 0;">
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="Google Form"> Google Form</li>
    </ol>


      <div class="col-md-12 bg-white" style="border:1px solid #cccc;height: auto;">
      
      <div class="col-md-12" style="border-bottom: 1px solid #cccc;">
        <div class="row">

            <div class="col-md-9" style="margin-bottom: 10px;"><h2><a href="dashboard.php" data-toggle="tooltip" data-title="Back"><i class="pg-icon" style="font-size: 22px;">chevron_left</i></a> Google Form</h2></div>

        </div>
      </div>

          
          <div class=" container-fluid container-fixed-lg bg-white" style="margin-top: 0px;">
            <div class="card card-transparent">
              <div class="card-header ">

                  
                      <div class="row">
                        <div class="col-md-2">

                          <a href="new_google_form.php" class="btn btn-info btn-lg"><span class="fa fa-plus"></span>&nbsp;Add Google Form</a>

                        </div>
                        <div class="col-md-10"> </div>
                    </div>
                 



                    <div class="clearfix"></div>
                </div>
                <div class="card-body">
                  <!--  style="height: 820px;overflow: auto;" -->
                <div class="table-responsive">
                <table class="table table-hover" id="tblFruits">
                  <thead>
                      <tr>
                        <th style="width: 1%;">Add Time</th>
                        <th style="width: 1%;">Teacher Name</th>
                        <th style="width: 1%;">Total Class</th>
                        <th style="width: 1%;">Start Time</th>
                        <th style="width: 1%;">End Time</th>
                        <th style="width: 1%;">Preview Link</th>
                        <th style="width: 1%;text-align: center;">Action</th>
                      </tr>
                    </thead>
                    <tbody>

                      <?php 
                          /*------------------ Header Pagination --------------------------------*/
                        $record_per_page = 10;

                        $nxt_five_loop = 0;

                        $page = '';

                        if(isset($_GET["page"]))
                        {
                          $page = $_GET["page"];
                        }
                        else
                        {
                         $page = 1;
                        }

                        $start_from = ($page-1)*$record_per_page; //Current page no -1 X Per Page records

                        $sql001 = "SELECT * FROM `g_f_master` WHERE `TEACH_ID` = '$teach_id001' order by `GF_MASTER_ID` DESC LIMIT $start_from, $record_per_page";
                        
                        $page_query = "SELECT * FROM `g_f_master` WHERE `TEACH_ID` = '$teach_id001' ORDER BY `GF_MASTER_ID` DESC";

                        /*------------------ Header Pagination --------------------------------*/

                        $j = 0;
                        
                        //LIMIT = 10-1 x 10 = 90 , 10";
                        $result = mysqli_query($conn, $sql001);
                        $page_result = mysqli_query($conn, $page_query);

                          $check = mysqli_num_rows($result);

                          if($check>0)
                          {
                          while($row001 = mysqli_fetch_assoc($result))
                          {
                            $add_date = $row001['ADD_DT'];
                            $gf_master_id = $row001['GF_MASTER_ID'];
                            $teacher_id = $row001['TEACH_ID'];
                            $form_link = $row001['FORM_LINK'];
                            $paper_name = $row001['PAPER_NAME'];

                            $hour = $row001['HOURS'];
                            $min = $row001['MINUTE'];

                            $paper_name = $row001['PAPER_NAME'];


                            $form_start_time = $row001['START_TIME'];
                            $form_end_time = $row001['END_TIME'];

                            $start_time = date('Y-m-d h:i:s A',strtotime($row001['START_TIME']));
                            $end_time = date('Y-m-d h:i:s A',strtotime($row001['END_TIME']));

                            $j=$j++;
                            $count = 0;

                            $sql002 = mysqli_query($conn,"SELECT * FROM `teacher_details` WHERE `TEACH_ID` = '$teacher_id'");
                            while($row002 = mysqli_fetch_assoc($sql002))
                            {
                              $teacher_name = $row002['POSITION'].". ".$row002['F_NAME']." ".$row002['L_NAME'];
                              
                            }

                            $sql003 = mysqli_query($conn,"SELECT * FROM `g_f_tranasaction` WHERE `GF_MASTER_ID` = '$gf_master_id'");
                            while($row003 = mysqli_fetch_assoc($sql003))
                            {
                              $gf_class_id = $row003['CLASS_ID'];
                              $count = $count+1;
                              
                            }


                            echo '

                              <tr>
                                <td class="v-align-left">'.$add_date.'</td>
                                <td class="v-align-left">'.$teacher_name.'</td>
                                <td class="v-align-left"><label class="badge badge-success">'.$count.'</label></td>
                                <td class="v-align-left">'.$start_time.'</td>
                                <td class="v-align-left">'.$end_time.'</td>
                                <td class="v-align-left"><span class="fa fa-link"></span> <a href="'.$form_link.'" target="_blank">'.$form_link.'</a></td>
                                 <td class="v-align-middle text-center">

                                <button data-toggle="modal" data-target="#edit_gf'.$gf_master_id.'" class="btn btn-success btn-xs btn-rounded mt-1 mr-1" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;" data-title="Approve" data-toggle="tooltip" id="edit1'.$gf_master_id.'"><i class="pg-icon">edit</i></button>
                                '; ?>

                                <button class="btn btn-warning mt-1 mr-1 btn-xs btn-rounded" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;" data-target="#add_class<?php echo $gf_master_id; ?>" data-toggle="modal"><i class="pg-icon">plus</i></button>

                                <div id="add_class<?php echo $gf_master_id; ?>" class="modal fade" role="dialog">
                                  <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Add Class</h4>
                                      </div>
                                      <form action="../teacher/query/update.php" method="POST">
                                      <div class="modal-body mt-2" style="text-align:left;">

                                        <div class="row">
                                          <div class="col-md-12">

                                            <div class="table-responsive" style="height: 400px;overflow: auto;">
                                              <table class="table table-hover">
                                                <thead>
                                                  <th>#</th>
                                                  <th>Class Name</th>
                                                </thead>
                                                <tbody>  
                                                  
                                                  <?php 

                                                    $sql0022 = mysqli_query($conn,"SELECT * FROM `classes` WHERE `TEACH_ID` = '$teacher_id'");
                                                    if(mysqli_num_rows($sql0022)>0)
                                                    {

                                                        while($row0032 = mysqli_fetch_assoc($sql0022))
                                                        {
                                                            
                                                            $clz_id = $row0032['CLASS_ID'];
                                                            $clz_name = $row0032['CLASS_NAME'];

                                                            $check_input = '';


                                                            $sql0033 = mysqli_query($conn,"SELECT * FROM `g_f_tranasaction` WHERE `CLASS_ID` = '$clz_id'");
                                                            if(mysqli_num_rows($sql0033)>0)
                                                            {
                                                              $check_input = 'checked';
                                                            }

                                                            echo '

                                                                <tr>
                                                                    <label>
                                                                    <td style="padding-left: 12px;"><input type="checkbox" name="class_id[]" onclick="click_all()" value="'.$clz_id.'" '.$check_input.' style="zoom:1.5;margin-top: 4px;"></td>
                                                                    <td style="padding-left: 0;"><label style="font-size: 14px;padding-top:6px;">'.$clz_name.'</label></td>
                                                                    </label>
                                                                </tr>

                                                            ';
                                                            
                                                        }
                                                       
                                                    }else
                                                    if(mysqli_num_rows($sql0022) == '0')
                                                    {
                                                        echo '<tr>
                                                                    <td colspan="3" class="text-center text-danger"><span class="fa fa-warning"></span> Not Found Data!</td>
                                                              </tr>';
                                                    }

                                                   ?>

                                                </tbody>
                                              </table>
                                              </div>
                                          </div>
                                        </div>
                                       
                                        <input type="hidden" name="page" value="<?php echo $page; ?>">
                                        <input type="hidden" name="gf_master_id" value="<?php echo $gf_master_id; ?>">
                                        <input type="hidden" name="teacher_id" value="<?php echo $teacher_id; ?>">

                                      </div>
                                      <div class="modal-footer">
                                        <button type="submit" class="btn btn-success btn-lg" name="update_google_form_classes" value="<?php echo $gf_master_id; ?>" id="class_id_btn<?php echo $gf_master_id; ?>"><span class="fa fa-edit"></span>&nbsp;update</button>
                                        <button type="button" class="btn btn-default btn-lg" data-dismiss="modal">Close</button>
                                      </div>
                                      </form>
                                    </div>

                                  </div>
                                </div>

                                <script type="text/javascript">
                                  document.getElementById('my_add_class').disabled = true;
                                  function click_all() 
                                  {
                                    
                                    var array = []
                                    var checkboxes = document.querySelectorAll('input[type=checkbox]:checked');
                                    var clicked_amount = checkboxes.length;

                                    if(clicked_amount == 0)
                                    {
                                      document.getElementById('class_id_btn<?php echo $gf_master_id; ?>').disabled = true;
                                      
                                    }else
                                    if(clicked_amount > 0)
                                    {
                                      document.getElementById('class_id_btn<?php echo $gf_master_id; ?>').disabled = false;
                                      
                                    }
                                  }
                                </script>




                                <a href="../teacher/query/delete.php?delete_gf=<?php echo $gf_master_id; ?>&&page=<?php echo $page; ?>" onclick="return confirm('Are you sure delete?')" class="btn btn-danger mt-1 mr-1 btn-xs btn-rounded" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;" data-title="Delete" data-toggle="tooltip"><i class="pg-icon">trash_alt</i></a>

                                <div id="edit_gf<?php echo $gf_master_id; ?>" class="modal fade" role="dialog">
                                  <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Update Google Form</h4>
                                      </div>
                                      <form action="../teacher/query/update.php" method="POST">
                                      <div class="modal-body mt-2" style="text-align:left;">

                                        <div class="row">
                                          <div class="col-md-12">
                                            <div class="form-group form-group-default input-group">
                                                <div class="form-input-group">
                                                  <label>Paper Name</label>
                                                  <input type="text" name="paper_name" class="form-control" placeholder="Write.." required data-toggle="tooltip" data-title="ප්‍රශ්න පත්‍රයේ නම" style="text-transform: capitalize;" value="<?php echo $paper_name; ?>">
                                                </div>
                                            </div>
                                          </div>
                                        </div>

                                        <div class="row">
                                          <div class="col-md-12">
                                            
                                              <div class="form-group form-group-default input-group">
                                                  <div class="form-input-group">
                                                    <label>Teacher Name</label>
                                                    <select class="form-control" name="teacher_id" id="teacher_id" onchange="find_class();" data-toggle="tooltip" data-title="ගුරුවරයාගේ නම">
                                                      <option value="<?php echo $teacher_id; ?>"><?php echo $teacher_name ?></option>
                                                    </select>
                                                  </div>
                                              </div>

                                          </div>
                                  </div>


                                        <div class="row">
                                          <div class="col-md-6">
                                            
                                              <div class="form-group form-group-default input-group">
                                                  <div class="form-input-group">
                                                    <label>Time Duration(Hours)</label>
                                                    <select class="form-control" name="hour" data-toggle="tooltip" data-title="ලබාදෙන කාලය(පැය ගණන)">
                                                      <option value="<?php echo $hour; ?>"><?php echo $hour; ?></option>
                                                      <?php 
                                                        for ($h=0; $h <= 12; $h++) 
                                                        { 

                                                          if($h<10)
                                                          {
                                                            $h = '0'.$h;
                                                          }

                                                          echo '<option value="'.$h.'">'.$h.'</option>';

                                                        }
                                                       ?>
                                                    </select>
                                                  </div>
                                              </div>

                                          </div>

                                          <div class="col-md-6">
                                            
                                              <div class="form-group form-group-default input-group">
                                                  <div class="form-input-group">
                                                    <label>Time Duration(Minute)</label>
                                                    <select class="form-control" name="minute" data-toggle="tooltip" data-title="ලබාදෙන කාලය(විනාඩි ගණන)">
                                                      <option value="<?php echo $min; ?>"><?php echo $min; ?></option>
                                                      <?php 
                                                        for ($m=0; $m <= 60; $m++) 
                                                        { 

                                                          if($m<10)
                                                          {
                                                            $m = '0'.$m;
                                                          }

                                                          echo '<option value="'.$m.'">'.$m.'</option>';

                                                        }
                                                       ?>
                                                    </select>
                                                  </div>
                                              </div>
                                        
                                          </div>


                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                              <div class="form-group form-group-default input-group">
                                              <div class="form-input-group">
                                                <label>Start Date & Time</label>
                                                <input type="datetime-local" name="start_date" id="start" min ="<?php echo date('Y-m-d');?>T00:00" class="form-control" required data-toggle="tooltip" data-title="ආරම්භවන දිනය හා වේලාව" value="<?php echo $form_start_time; ?>">
                                              </div>
                                            </div>
                                          </div>
                                          <div class="col-md-6">
                                            
                                            <div class="form-group form-group-default input-group">
                                                <div class="form-input-group">
                                                  <label>Finish Date & Time</label>
                                                  <input type="datetime-local" name="end_date" id="end" class="form-control" min ='<?php echo date('Y-m-d');?>T00:00' required data-toggle="tooltip" data-title="අවසන් වන දිනය හා වේලාව" value="<?php echo $form_end_time; ?>">
                                                </div>
                                            </div>
                                            
                                          </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                              <div class="form-group form-group-default input-group">
                                              <div class="form-input-group">
                                                <label>Google Form Link</label>
                                                <textarea class="form-control" name="google_form_link" placeholder="Google Form Link.."><?php echo $form_link; ?></textarea>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                       
                                        <input type="hidden" name="page" value="<?php echo $page; ?>">

                                      </div>
                                      <div class="modal-footer">
                                        <button type="submit" class="btn btn-success btn-lg" name="update_google_form" value="<?php echo $gf_master_id; ?>"><span class="fa fa-edit"></span>&nbsp;update</button>
                                        <button type="button" class="btn btn-default btn-lg" data-dismiss="modal">Close</button>
                                      </div>
                                      </form>
                                    </div>

                                  </div>
                                </div>

                                <?php echo '

                                </td>
                              </tr>

                            ';
                        
                          }
                        }else
                        if($check == '0')
                        {
                          echo '<tr><td colspan="7" class="text-center text-danger"><span class="fa fa-warning"></span> Not Found Data</td></tr>';
                        } 
                      

                        

                        
                         ?>


                    </tbody>
                </table>
                </div>
              </div>

                <div class="col-md-12" style="width: 100%;overflow: auto;">
                  <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-8" class="php_pagination">
                      
                      <div class="col-md-12">
                    <br />
                    <?php
                    $total_records = mysqli_num_rows($page_result);
                    $total_pages = ceil($total_records/$record_per_page);
                    $start_loop = $page;
                    $difference = $total_pages - $page;
                    if($difference <= 5)
                    {
                     $start_loop = $total_pages - 5;
                    }else
                    if($difference <= 0)
                    {
                      $start_loop = '0';
                    }

                    echo '<ul class="pagination">';
                    $end_loop = $start_loop + 4;
                    $five_loop = $page-5;
                    if($five_loop <= 0)
                    {
                      $five_loop = 1;
                    }
                    if($page > 1)
                    {

                      echo '<li><a href="google_form.php?page=1">First</a></li>';
                      echo '<li><a href="google_form.php?page='.($page - 1).'"> < </a></li>';
                      echo '<li><a href="google_form.php?page='.$five_loop.'"> <<< </a></li>';

                    }
                    if($start_loop !== '1')
                    {
                      $few_no = $start_loop - 1;
                    }else
                    if($start_loop > 0)
                    {
                      $few_no = 1;
                    }
                    
                    for($i=$few_no; $i<=$end_loop+1; $i++)
                    {     
                      if($i == $page)
                      {
                        echo '<li class="active"><a href="google_form.php?page='.$i.'" style="font-weight:bold;background-color:#2980b9;color:white;">'.$i.'</a></li>';
                      }else
                      {
                        if($i == '0')
                        {
                          continue;
                        }else
                        if($i>0)
                        {
                          echo '<li><a href="google_form.php?page='.$i.'" >'.$i.'</a></li>';
                        }
                        
                      }

                    }
                    if($page <= $end_loop)
                    {
                      
                      if($page == '1')
                      {
                        $nxt_five_loop = 5;
                      }else
                      if($page >= 5)
                      {
                        $nxt_five_loop = $page+5;

                        if($total_pages < $nxt_five_loop)
                        {
                          $nxt_five_loop = $total_pages;
                        }
                      }else
                      {
                        $nxt_five_loop = $page+5;
                      }

                        echo '<li><a href="google_form.php?page='.($page + 1).'" > > </a></li>';
                        echo '<li><a href="google_form.php?page='.$nxt_five_loop.'" > >>> </a></li>';
                        echo '<li><a href="google_form.php?page='.$total_pages.'" >Last</a></li>';
                      
                    }
                    
                    echo '</ul>';
                    
                    ?>
                    </div>

                    </div>
                  </div>
                    
                </div>
            </div>
            </div>
            </div>
            </div>
            </div>
            </div>



<?php include('footer/footer.php'); ?>

<script type="text/javascript">
  function sortTable(table, order) {
    var asc   = order === 'asc',
        tbody = table.find('tbody');
    
    tbody.find('tr').sort(function(a, b) {
        if (asc) {
            return $('td:first', a).text().localeCompare($('td:first', b).text());
        } else {
            return $('td:first', b).text().localeCompare($('td:first', a).text());
        }
    }).appendTo(tbody);
}
</script>

<script type="text/javascript">
    function GetSelected() {
        //Create an Array.
        var selected = new Array();
 
        //Reference the Table.
        var tblFruits = document.getElementById("tblFruits");
 
        //Reference all the CheckBoxes in Table.
        var chks = tblFruits.getElementsByTagName("INPUT");
 
        // Loop and push the checked CheckBox value in Array.
        for (var i = 0; i < chks.length; i++) {
            if (chks[i].checked) 
            {
                var s = selected.push(chks[i].value);

            }
        }
 
        //Display the selected CheckBox values.
        if (selected.length > 0) {
          if(confirm('Are you sure delete?'))
          {

              $.ajax({
               type: "POST",
               data: {delete_multi_todo:selected},
               url: "../admin/query/delete.php",
               success: function(msg){
                 //alert(msg)
                 location.reload();
               }
            
              });
          }
        }else{
         
         alert("Please select todo list!")
        
        }
    };
</script>

<!-- Modal -->
                          <div id="add_todo" class="modal fade" role="dialog">
                            <div class="modal-dialog">

                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title">Create Todo List</h4>
                                </div>
                                <form action="../admin/query/insert.php" method="POST">
                                <div class="modal-body mt-2">

                                  <label class="mt-2">Student Name</label>
                                  <input list="student_list" id="search-table" name="student_name" class="form-control" placeholder="Search Register ID.." >

                                  <datalist id="student_list">
                                    <?php 

                                      $sql002 = mysqli_query($conn,"SELECT * FROM `student_details`");
                                      while($row002 = mysqli_fetch_assoc($sql002))
                                      {
                                        $name = $row002['F_NAME']." ".$row002['L_NAME'];
                                        $student_id = $row002['STU_ID'];
                                        $tp = $row002['TP'];

                                        $sql003 = mysqli_query($conn,"SELECT * FROM `stu_login` WHERE `STU_ID` = '$student_id' AND `STATUS` = 'Active'");
                                        while($row003 = mysqli_fetch_assoc($sql003))
                                        {
                                          
                                          $register_id = $row003['REGISTER_ID'];
                                        
                                          echo '<option value="'.$register_id.'">'.$name.' | '.$tp.'</option>';
                                        }

                                        
                                      }

                                     ?>
                                    
                                  </datalist>

                                  <label class="mt-2">Comment</label>
                                  <textarea class="form-control" placeholder="Enter comment" name="comment" rows="6"></textarea>
                                </div>
                                <div class="modal-footer">
                                  <button type="submit" class="btn btn-success" name="create_todo"><span class="fa fa-check-circle"></span>&nbsp;Submit</button>
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                                </form>
                              </div>

                            </div>
                          </div>