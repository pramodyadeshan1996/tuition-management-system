<?php 
include('../connect/connect.php');
 
if(isset($_POST['sql_clean_now']))
{

    mysqli_query($conn,"UPDATE transactions SET new_url ='' WHERE CLASS_ID='$_POST[class_id]'"); //Clear Link URL

    mysqli_query($conn,"UPDATE classes SET zoom_meeting_id ='' WHERE CLASS_ID='$_POST[class_id]'"); //Clear Class Zoom Meeting ID
/*
    echo "<script>alert('Zoom Links Cleared!')</script>"; //Message */
}  

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Create Safe Link</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200&display=swap" rel="stylesheet">
</head>
<body style="font-family: 'Poppins', sans-serif;">





    <span style="display:none;">
    <?php

    


    ?>
    </span>

    <div class="container-fluid">
        
                <div class="row">
                    
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                    <div class="col-md-12" style="padding:20px 20px 20px 20px;border:1px solid #cccc;margin-top: 60px;margin-bottom: 50px;">
                    <center><strong><img src="https://crowdpurr.zendesk.com/hc/article_attachments/360078529571/zoom_badge.png" style="width: 120px;height: 45px;"></strong> </center>

                    <h1 style="font-weight: bold;opacity: 0.7;">Create Safe Link</h1>
                    <h5>

                        <!-- Create Topic -->

                            <?php 

                                $class_id001 = $_GET['class_id'];

                                $sql001 = mysqli_query($conn,"SELECT * FROM `classes` WHERE `CLASS_ID` = '$class_id001'");

                                while($row001 = mysqli_fetch_assoc($sql001))
                                {
                                  $class_name = $row001['CLASS_NAME'];
                                  $teach_id = $row001['TEACH_ID'];
                                    
                                  $scheduled_minute = '0';
                                  $scheduled_date = date('Y-m-d');
                                  $scheduled_time = date('h:i:00 A');

                                  if(!empty($row001['minute']))
                                  {
                                    $scheduled_minute = $row001['minute'];
                                  }
                                    
                                  if(!empty($row001['schedule_date']))
                                  {
                                    $scheduled_date = $row001['schedule_date'];
                                  } 

                                  if(!empty($row001['schedule_time']))
                                  {
                                    $scheduled_time = $row001['schedule_time'];
                                  }
                                  

                                  $sql003 = mysqli_query($conn,"SELECT * FROM `teacher_details` WHERE `TEACH_ID` = '$teach_id'");
                                  while($row003 = mysqli_fetch_assoc($sql003))
                                  {
                                    $teacher_name = $row003['POSITION'].". ".$row003['F_NAME']." ".$row003['L_NAME'];

                                  }

                                }

                                $topic = $class_name." - ".$teacher_name;
                             ?>

                        <!-- Create Topic -->


                        <label><span class="fa fa-user-circle"></span> Teacher Name - <?php echo $teacher_name; ?></label>
                        <br>
                        <label><span class="fa fa-check-circle"></span> Class Name - <?php echo $class_name; ?></label>
                    </h5>

                    <form action="../admin/query/insert.php" method="post" style="margin-top: 10px;border-radius: 10px;" >

                        <div class="row">
                            <div class="col-md-12">

                                <!-- Minute Input -->
                                <label class="text-dark" style="margin-top:6px;opacity: 0.6;">Setup Minute</label>
                                <input type="text" name="duration" class="form-control" placeholder="Minutes" onkeypress="return isNumberKey(event)" value="<?php echo $scheduled_minute; ?>" style="font-size: large;font-weight: bold;"  required>
                                <!-- Minute Input -->

                            </div>
                            <div class="col-md-12">

                                <!-- Start Time -->

                                <label class="text-dark" style="margin-top:6px;opacity: 0.6;">Schedule Time</label>

                                 
                                <!-- <input type="text" class="form-control" name="start_date" value="<?php //echo date('Y-m-d'); ?>T<?php //echo date("h:i"); ?>:00" style="font-size: large;font-weight: bold;" required>  -->
                                <div class="row">
                                    <div class="col-md-6" style="margin-top:10px;">
                                        <input type="text" class="form-control" name="start_date" value="<?php echo $scheduled_date; ?>" style="font-size: large;font-weight: bold;" required min="<?php echo date('Y-m-d'); ?>">
                                    </div>

                                    <div class="col-md-6" style="margin-top:10px;">
                                        <input type="text" class="form-control" name="start_time" value="<?php echo $scheduled_time; ?>" style="font-size: large;font-weight: bold;" required>
                                    </div>
                                </div>
                                
                                <!-- Start Time -->

                            </div>
                            <div class="col-md-12">
                                <label class="text-dark" style="margin-top:6px;opacity: 0.6;">Zoom Account</label>
                                <select name="email_id" class="form-control" style="font-weight: bold;font-size: 14px;"  required>
                                    <?php  

                                        if(empty($id))
                                        {

                                            $sql_cat2 = "SELECT * FROM zoom_accounts  where active = '1' ORDER by id ASC";
                                            $result_cat2 = mysqli_query($conn,$sql_cat2);
                                            
                                            $ch = mysqli_num_rows($result_cat2);

                                            if($ch > 0)
                                            {
                                                while($pra_cat2 = mysqli_fetch_assoc($result_cat2)) 
                                                { 

                                                ?>        

                                                <option value="<?php echo $pra_cat2['id']; ?>"><?php echo $pra_cat2['zoom_name']; echo ' - '; echo $pra_cat2['email_address']; ?> </option>

                                                <?php 

                                                }
                                            }else
                                            if($ch == '0')
                                            {
                                                ?>        

                                                <option value="">Not Found Zoom Account</option>

                                                <?php
                                            }
                                        }else
                                        if(!empty($id))
                                        {
                                            
                                            $sql_cat2 = "SELECT * FROM zoom_accounts  where id = '$id' AND active = '1' ORDER by id ASC";
                                                $result_cat2 = mysqli_query($conn,$sql_cat2);

                                            while($pra_cat2 = mysqli_fetch_assoc($result_cat2)) 
                                            { 

                                            ?>        

                                            <option value="<?php echo $pra_cat2['id']; ?>"><?php echo $pra_cat2['zoom_name']; echo ' - '; echo $pra_cat2['email_address']; ?> </option>

                                            <?php 

                                            }

                                            $sql_cat3 = "SELECT * FROM zoom_accounts  where id != '$id' AND active = '1' ORDER by id ASC";
                                                $result_cat3 = mysqli_query($conn,$sql_cat3);

                                            while($pra_cat3 = mysqli_fetch_assoc($result_cat3)) 
                                            { 

                                            ?>        

                                            <option value="<?php echo $pra_cat3['id']; ?>"><?php echo $pra_cat3['zoom_name']; echo ' - '; echo $pra_cat3['email_address']; ?> </option>

                                            <?php 

                                            }


                                        }

                                    ?>

                                </select>


                            </div>
                            <div class="col-md-12">
                                
                                <!-- Create Meeting Button -->

                                <input type="hidden" name="class_id" value="<?php echo $_GET['class_id']; ?>">

                                <input type="hidden" name="topic" value="<?php echo $topic; ?>">
                                
                                <button class="btn btn-success btn-block" type="submit" name="getMeetingId" style="font-weight: bold;margin-top: 10px;background-color: #1abc9c;border:1px solid #1abc9c;padding: 10px 10px 10px 10px;font-size:18px;margin-bottom: 8px;"><span class="fa fa-check-circle"></span> Generate Zoom ID</button>

                                <!-- Create Meeting Button -->

                                <!-- Back Button -->
                                <center><a href="setup_link.php" class="btn btn-default btn-block" style="font-size:16px;font-weight: bold;"><i class="fa fa-angle-left"></i> Back</a></center>

                                <!-- Back Button -->


                                <!-- Message -->

                            </div>
                        </div>
                        
                    </form>
                                

                        <?php 

                            $sql0044 = mysqli_query($conn,"SELECT * FROM classes WHERE CLASS_ID = '$_GET[class_id]' ");
                            $pra2 = mysqli_fetch_assoc($sql0044);

                            if($pra2['zoom_meeting_id'] != '')
                            { 

                            ?>
                                <div style="margin-top: 2px;padding: 10px;border-radius: 10px; margin-bottom: 10px">
                                    <div class="row" style="margin-top:0px;">
                                        <div class="col-md-12">

                                            <h2 style="font-weight:bold;opacity: 0.7;padding-bottom: 10px;">Your Zoom Meeting ID</h2>
                                            <!-- Show Zoom Meeting ID -->
                                            <span class="btn-common ds-md-none mr-0 hide_in_mobile_x my_page btn-warning btn-block text-center" style="font-size: 30px;font-weight: bold;padding: 10px 10px 10px 10px;"><?php echo $pra2['zoom_meeting_id']; ?></span>
                                            <!-- Show Zoom Meeting ID -->

                                            <form action="" method="post" onsubmit="return confirm('Are you Sure to Clear?');">
                                        
                                                <input type="hidden" name="class_id" value="<?php echo $_GET['class_id']; ?>">

                                                <br>
                                                <p class="text-danger" style="font-weight: bold;"><span class="fa fa-warning"></span> අලුත් Zoom Meeting එකක් Create කිරීමේදී පහත Button ක්ලික් කර පෙර සකස් කරන ලද Meeting එක ඉවත් කරන්න. <br> (When creating a zoom meeting again, click the button below to delete the previously created meeting.)</p>
                                                
                                                <button type="submit" name="sql_clean_now" style="background: #f4516c;color: white;border-radius: 20px;font-weight: 100;font-weight: bold;" class="btn btn-danger btn-block"><span class="fa fa-times"></span> Clear Zoom Meeting ID</button>

                                            </form>
                                        </div>
                                    </div>
                                    
                                </div>
                            
                        <?php 

                            } 

                        ?>
                                    


                                <!-- Message -->

                            
                </div> 
                <!-- col-md-12 Tag end -->

            </div>
            <div class="col-md-3"></div>

        </div>
    
</div>

</body>
</html>