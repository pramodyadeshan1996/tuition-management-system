
<?php

$page = "SMS Payment";
$folder_in = '0';

  include('header/header.php'); 

 $admin_id = $_SESSION['ADMIN_ID'];

  ?>
  <style type="text/css">
    .btn_pulse {
  
  display: block;
  border-radius: 10%;
  cursor: pointer;
  animation: none;
  float: left;
  bottom: 5px;
  right: 5px;
  font-weight: bold;
  padding-top: 2px;
  text-align: center;
  color: white;
  font-size: 14px;
  animation: pulse 1.6s infinite;
}


@keyframes pulse {
  0% {
    -moz-box-shadow: 0 0 0 0 #7252d3;
    box-shadow: 0 0 0 0 #7252d3;
  }
  70% {
    -moz-box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
  }
  100% {
    -moz-box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
  }
}
  </style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


    <ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-bottom: 04%;margin-bottom: 0;">
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="New Classes"> SMS Payment</li>
    </ol>




<div class="col-md-12" style="border-top: 1px solid #cccc;">
  
<div class="row" style="margin-top: 3%;">
<div class="col-md-4"></div>
<div class="col-md-4">
<div class=" container-fluid   container-fixed-lg bg-white" style="padding: 2px 20px;">
<div class="card card-transparent">
<div class="card-header ">
<div class="card-title">


  <h3 style="text-transform: capitalize;"> <span class="fa fa-money"></span> SMS Payment</h3>


</div>
<div class="clearfix"></div>
</div>

            <form action="../admin/query/insert.php" method="POST">
            

            <div class="form-group form-group-default" style="margin-top: 10px;">
              <label>Payment (Rs)</label>
              
              <input type="text" class="form-control"  id="payment"  name="payment" required placeholder="Enter Payment(Rs)" onclick="this.select();">

            </div>
            
            <div class="form-group form-group-default" style="margin-top: 10px;">
              <label>SMS Amount</label>
              
              <input type="text" class="form-control"  id="sms_amount"  name="sms_amount" required placeholder="Enter SMS Amount" onclick="this.select();">

            </div>

            <div id="result" style="font-weight:bold;">

              <?php
              
              $sql = mysqli_query($conn,"SELECT * FROM `sms_counter` ORDER BY `SUB_SMS_C_ID` DESC");
              $row = mysqli_fetch_assoc($sql);
              $current_sms = $row['CURRENT_SMS'];
              $current_amount = $row['CURRENT_AMOUNT'];
  
              $new_sms = 0;
              $new_payment = 0;
  
              $sms_amount = 0;
              $payment = 0;
              $current_sms = (int)$current_sms;
              $current_amount = (int)$current_amount;
  
              $new_sms = $current_sms+$sms_amount;  //SMS with total sms
              $new_payment = $current_amount+$payment; //payment with total sms payments
        
              if(mysqli_num_rows($sql)>0)
              {
                  echo '
                      <div class="col-md-12" style="padding:0;font-weight:bold;">
                      <div class="row text-danger" style="padding:2px 8px 2px 8px;">
                      <div class="col-md-6" style="font-weight:bold;"> <span class="fa fa-envelope"></span> <label>Available SMS Amount </label></div>
                      <div class="col-md-6">: '.$current_sms.' SMS</div>
                      </div>
  
                      <div class="row text-danger" style="padding:2px 8px 2px 8px;font-weight:bold;">
                      <div class="col-md-6" style="font-weight:bold;"><span class="fa fa-money"></span> <label>Available SMS Payment </label></div>
                      <div class="col-md-6">: LKR '.number_format($current_amount,2).'</div>
                      </div>
                  </div>
                  
                  ';
              }else
              if(mysqli_num_rows($sql)== '0')
              {
                  echo '
                      <div class="col-md-12" style="padding:0;font-weight:bold;">
                      <div class="row text-danger" style="padding:2px 8px 2px 8px;">
                      <div class="col-md-6" style="font-weight:bold;"> <span class="fa fa-envelope"></span> <label>Current SMS Amount </label></div>
                      <div class="col-md-6">: 0 SMS</div>
                      </div>
  
                      <div class="row text-danger" style="padding:2px 8px 2px 8px;font-weight:bold;">
                      <div class="col-md-6" style="font-weight:bold;"><span class="fa fa-money"></span> <label>Current SMS Payment </label></div>
                      <div class="col-md-6">: LKR '.number_format(0,2).'</div>
                      </div>
  
                      <div class="row text-success" style="padding:2px 8px 2px 8px;font-weight:bold;">
                      <div class="col-md-6" style="font-weight:bold;"><span class="fa fa-envelope"></span> <label>New SMS Amount</label></div>
                      <div class="col-md-6">: 0 SMS</div>
                      </div>
  
                      <div class="row text-success" style="padding:2px 8px 2px 8px;font-weight:bold;">
                      <div class="col-md-6" style="font-weight:bold;"><span class="fa fa-money"></span> <label>New SMS Payment </label></div>
                      <div class="col-md-6">: LKR '.number_format(0,2).'</div>
                      </div>
                  </div>
                  
                  ';
              }

              ?>
              
            </div>
              <div class="col-md-12" style="border-top:1px solid #cccc;padding-bottom:10px;"></div>
            <button type="submit" class="btn btn-success btn-block btn-lg" name="sms_payment_submit"><i class="pg-icon">tick</i> Submit</button>
            <br>
            <br><center><a href="sms.php">Back</a></center>
            </form>
            </div>

</div>
<div class="col-md-4">
 
</div>

</div>
</div>



<script type="text/javascript">

    $(document).ready(function(){ 
    $('#payment,#sms_amount').bind('keyup change',function(){

      var payment = $('#payment').val();
      var sms_amount = $('#sms_amount').val();

      

      $.ajax({
        url:'../admin/query/check.php',
        method:"POST",
        data:{add_sms_amount:sms_amount,payment:payment},
        success:function(data)
        {
          //alert(data)
          $('#result').html(data);
          
        }
      })
    

    });
  });
  </script>

<?php  include('footer/footer.php'); ?>


