<?php 
  $page = 'teachers';
  include('header.php');

 ?>


<div class="breadcrumb-option spad set-bg" data-setbg="img/breadcrumb-bg.jpg">
<div class="container">
<div class="row">
<div class="col-lg-12 text-center">
<div class="breadcrumb__text" style="margin-top: 60px;">
<h2>Teachers</h2>
<div class="breadcrumb__links">
<a href="index.php">Home</a>
<span>Teachers</span>
</div>
</div>
</div>
</div>
</div>
</div>


<section class="portfolio spad">
<div class="container">
<div class="row portfolio__gallery">

<?php  

  $record_per_page = 8;

    $nxt_five_loop = 0;

    $page = '';

    if(isset($_GET["page"]))
    {
      $page = $_GET["page"];
    }
    else
    {
     $page = 1;
    }

    $start_from = ($page-1)*$record_per_page; //Current page no -1 X Per Page records

    

    /*------------------ Header Pagination --------------------------------*/

        $sql001 = mysqli_query($conn, "SELECT * FROM `teacher_login` WHERE `CONFIRM` = '1' order by `TEACH_ID` DESC LIMIT $start_from, $record_per_page");

        $page_result = mysqli_query($conn, "SELECT * FROM `teacher_login` WHERE `CONFIRM` = '1'");

        while($row001 = mysqli_fetch_assoc($sql001))
        {
          $teach_id = $row001['TEACH_ID'];
          $confirm = $row001['CONFIRM'];

            
            $sql004 = mysqli_query($conn,"SELECT * FROM `teacher_details` WHERE `TEACH_ID` = '$teach_id'");

            while($row004 = mysqli_fetch_assoc($sql004))
            {
              $name = $row004['POSITION'].". ".$row004['F_NAME']." ".$row004['L_NAME'];
              $qualification = $row004['QUALIFICATION'];

              $picture = $row004['PICTURE'];
              $gender = $row004['GENDER'];
              $teach_id = $row004['TEACH_ID'];

              if($picture == '0')
               {
                  if($gender == 'Male')
                  {
                    $picture = 'b_teacher.jpg';
                  }else
                  if($gender == 'Female')
                  {
                    $picture = 'g_teacher.jpg';
                  }
               }

              echo '

                <div class="col-lg-3 col-md-6 col-sm-6 mix branding" oncontextmenu="return false">
                  <a href="teacher_profile.php?teach_id='.$teach_id.'">
                    <div class="portfolio__item">
                              <div class="portfolio__item__video set-bg" data-setbg="../teacher/images/profile/'.$picture.'"></div>
                      
                      <div class="portfolio__item__text">
                      <h4>'.$name.'</h4>
                      <ul>
                      <li>'.$qualification.'</li>
                      </ul>
                      </div>
                    </div>
                  </a>
                </div>

              ';
          }
      }
?>



</div>
<div class="row">
<div class="col-lg-12">
<div class="pagination__option">
  <?php
                    $total_records = mysqli_num_rows($page_result);
                    $total_pages = ceil($total_records/$record_per_page);
                    $start_loop = $page;
                    $difference = $total_pages - $page;
                    if($difference <= 5)
                    {
                     $start_loop = $total_pages - 5;
                    }else
                    if($difference <= 0)
                    {
                      $start_loop = '0';
                    }

                    echo '<ul class="pagination">';
                    $end_loop = $start_loop + 4;
                    $five_loop = $page-5;
                    if($five_loop <= 0)
                    {
                      $five_loop = 1;
                    }
                    if($page > 1)
                    {

                      echo '<a href="teachers.php?page='.($page - 1).'" class="arrow__pagination left__arrow"><span class="arrow_left"></span> Prev</a>';

                    }
                    if($start_loop !== '1')
                    {
                      $few_no = $start_loop - 1;
                    }else
                    if($start_loop > 0)
                    {
                      $few_no = 1;
                    }
                    
                    for($i=$few_no; $i<=$end_loop+1; $i++)
                    {     
                      if($i == $page)
                      {
                        echo '<a href="teachers.php?page='.$i.'" class="number__pagination" style="background-color:white;color:black;">'.$i.'</a>';
                      }else
                      {
                        if($i == '0')
                        {
                          continue;
                        }else
                        if($i>0)
                        {
                          echo '<a href="teachers.php?page='.$i.'" class="number__pagination">'.$i.'</a>';
                        }
                        
                      }

                    }
                    if($page <= $end_loop)
                    {
                      
                      if($page == '1')
                      {
                        $nxt_five_loop = 5;
                      }else
                      if($page >= 5)
                      {
                        $nxt_five_loop = $page+5;

                        if($total_pages < $nxt_five_loop)
                        {
                          $nxt_five_loop = $total_pages;
                        }
                      }else
                      {
                        $nxt_five_loop = $page+5;
                      }

                        echo '<a href="teachers.php?page='.($page + 1).'" class="arrow__pagination right__arrow">Next <span class="arrow_right"></span></a>';
                      
                    }
                    
                    
                    ?>

        </div>
      </div>
    </div>
  </div>
</section>

<?php 
  include('footer.php');

 ?>