<?php 
  session_start();
  include('../connect/connect.php');
  $class_id = $_POST['class_id'];

  $year = $_POST['year'];
  $month = $_POST['month'];

  if($month<10)
  {
    $month = '0'.$month;
  }
  $pay_type = $_POST['pay_type'];



  $user_name = $_SESSION['POSITION'].". ".$_SESSION['F_NAME']." ".$_SESSION['L_NAME'];

  $sql = mysqli_query($conn,"SELECT * FROM institute WHERE INS_ID = '1'");
  while($row = mysqli_fetch_assoc($sql))
  {
    $n = $row['INS_NAME'];
    $p = $row['PICTURE'];
    if($row['INS_MOBILE'] !== '0')
    {
      $mobile = $row['INS_MOBILE'];
    }else
    {
      $mobile = "";
    }

    if($row['INS_TP'] !== '0')
    {
      $tele = $row['INS_TP'];
    }else
    {
      $tele = "";
    }


    if(!empty($mobile) && !empty($tele))
    {
      $no = $mobile." / ".$tele;
    }

    $a = $row['INS_ADDRESS'];
  }header('Content-type: text/html; charset=UTF-8');


    $date = date('Y-m-d');
    $time = date('h:i:s A');

 ?>
<html>
<head>
<style type="text/css" media="print,screen">
.hideMe{
display:block;
}

.PrintClass {
display:block;

}
.NoPrintClass{
display:block;
}

</style>
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script type="text/javascript"
    src="http://jqueryjs.googlecode.com/files/jquery-1.3.1.min.js"> </script>
    <link href="https://fonts.googleapis.com/css?family=PT+Sans+Narrow&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<script type="text/javascript">

function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     document.body.innerHTML = originalContents;

     window.print();
    window.onafterprint = function() {
    };
     

}
function close_win() {
   window.close();
}
   </script>
</head>
  <style type="text/css">
  @media print {
  .print_btn {
    display: none;
  }
}
</style>
</head>


<body>
 <div class="col-md-12" id="printableArea" style="display:block;">

 <p style="text-align:center">
        <?php 

        if(!empty($mobile) && !empty($tele))
        {
          $no = $mobile."/".$tele;
        }else
        if(!empty($mobile) && empty($tele))
        {
          $no = $mobile;
        }else
        if(!empty($tele) && empty($mobile))
        {
          $no = $tele;
        }else
        {
          $no = '';
        }

echo '<center><img src="../admin/images/institute/'.$p.'" style="width:80px;height:80px;margin-bottom:10px;"></center>';

        if($p == "0")
        {
          echo '<h3 style="text-align:center;text-transform:uppercase;">'.$n.'</h3><p style="text-align:center">'.$a.'</p><p style="text-align:center">'.$no.'</p><div style="border-top: 1px dashed black;padding:0px;"></div>';
        }else
        if($p !== "0")
        {
          echo '<h3 style="text-align:center;text-transform:uppercase;">'.$n.'</h3><p style="text-align:center">'.$a.'</p><p style="text-align:center">'.$no.'</p><div style="border-top: 1px dashed black;padding:0px;"></div>';
        }
        
        ?>
      </p>


        <h4 style="float: left;text-transform: uppercase;">Date & Time : <?php echo $date; ?> <?php echo $time; ?>
          <br>

          <?php echo "Teacher Name : ".$user_name; ?> <br>

          <?php 

            if($class_id == 'all')
            {
              echo "Class Name : All Classes <br>";
              

            }else
            if($class_id !== 'all')
            {
              $sql001 = mysqli_query($conn,"SELECT * FROM `classes` WHERE `CLASS_ID` = '$class_id'");
              while($row001 = mysqli_fetch_assoc($sql001))
              {
                $clz_name = $row001['CLASS_NAME'];
                $clz_fees = $row001['FEES'];
              }

              ?>

                <?php echo "Class Name : ".$clz_name; ?> <br>
                <?php echo "Class Fees : LKR ".number_format($clz_fees,2); ?> <br>

              <?php
            }
            

           ?>
          
          <?php echo "Month : ".$year." - ".date('F', mktime(0, 0, 0, $month, 10)); // March ?> <br>
        </h4>
        <br>
        <br>
        <br>
        <br>
        <br>

        <?php 
          if($pay_type == '1') //Paid Students
          {
         ?>

        <div class="row">
          <div class="col-md-2"></div>
          <div class="col-md-5" style="text-align: center;"><center><h1 style="text-align: center;margin-bottom: 20px;"><u>PAID REPORT</u></h1></center></div>
          <div class="col-md-5"></div>
        </div>
   
        <div class="table-responsive">
            
            <table class="table" style="font-size: 14px;">
              <thead>
                <th>Paid Time</th>
                <th>Student Name</th>
                <th>Register ID</th>
                <th>Contact No</th>
                <th>Month</th>
                <th style="text-align: center;">Payment Method</th>
                <th style="text-align: right;">Fees(LKR)</th>
              </thead>
            <tbody>
              <?php 
                $total_fees = 0;
                $count_stu = 0;

                if($class_id == 'all')
                {
                  $sql006 = mysqli_query($conn,"SELECT * FROM `payment_data` WHERE `YEAR` = '$year' AND `MONTH` = '$month' AND `ADMIN_SUBMIT` = '1'");
                }else
                if($class_id !== 'all')
                {
                  $sql006 = mysqli_query($conn,"SELECT * FROM `payment_data` WHERE `CLASS_ID` = '$class_id' AND `YEAR` = '$year' AND `MONTH` = '$month' AND `ADMIN_SUBMIT` = '1'");
                }
                

                  if(mysqli_num_rows($sql006)>0)
                  {
                   
                      while($row006 = mysqli_fetch_assoc($sql006))
                      {
                        $tra_id = $row006['TRA_ID'];
                        $stu_id = $row006['STU_ID'];
                        $pay_method = $row006['PAY_METHOD'];
                        $p_time = $row006['PAY_TIME'];
                        $y = $row006['YEAR'];
                        $m = $row006['MONTH'];
                        $clz_feeses = $row006['FEES'];

                        $total_fees = $total_fees+$clz_feeses;

                        $count_stu = $count_stu+1;

                        $sql007 = mysqli_query($conn,"SELECT * FROM `student_details` WHERE `STU_ID` = '$stu_id'");
                        while($row007 = mysqli_fetch_assoc($sql007))
                        {
                          $stu_name = $row007['F_NAME']." ".$row007['L_NAME'];
                          $stu_tp = $row007['TP'];
                        }

                        $sql008 = mysqli_query($conn,"SELECT * FROM `stu_login` WHERE `STU_ID` = '$stu_id'");
                        while($row008 = mysqli_fetch_assoc($sql008))
                        {
                          $reg_id = $row008['REGISTER_ID'];
                        }

                        $mon_name = date('F', mktime(0, 0, 0, $m, 10)); // March

                        echo '
                            <tr>
                              <td>'.$p_time.'</td>
                              <td>'.$stu_name.'</td>
                              <td>'.$reg_id.'</td>
                              <td>'.$stu_tp.'</td>
                              <td>'.$y.' - '.$mon_name.'</td>
                              <td style="text-align: center;">'.$pay_method.'</td>
                              <td style="text-align: right;">'.number_format($clz_feeses,2).'</td>
                            </tr>
                          ';
                        
                      }
                    }else
                    if(mysqli_num_rows($sql006) == '0')
                    {
                      echo '<tr>
                            <td colspan="6" style="text-align: center;"><span class="fa fa-warning"></span> Not Found Data!</td>
                         </tr>';
                    }
                  
                
                  

               ?>
               <tr style="font-size: 14px;">
                 <th>
                   Total Class Fees(LKR)
                 </th>
                 <th colspan="5"></th>
                 <th colspan="1" style="text-align: right;"><?php echo number_format($total_fees,2); ?></th>
               </tr>
               <tr style="font-size: 14px;">
                 <th>
                   No Of Students
                 </th>
                 <th colspan="5"></th>
                 <th colspan="1" style="text-align: right;"><?php echo $count_stu; ?></th>
               </tr>

                </tbody>
              </table>
        </div>
      <?php //paid students 
      }else
      if($pay_type == '0') //Not Paid Students
      {?>


          <div class="row">
          <div class="col-md-2"></div>
          <div class="col-md-5" style="text-align: center;"><center><h1 style="text-align: center;margin-bottom: 20px;"><u>UNPAID REPORT</u></h1></center></div>
          <div class="col-md-5"></div>
        </div>
   
        <div class="table-responsive">
            
            <table class="table" style="font-size: 14px;">
              <thead>
                <th style="width: 35%;">Student Name</th>
                <th>Register ID</th>
                <th>Contact No</th>
                <th>Gender</th>
                <th>Month</th>
                <th class="text-right" style="width: 10%;">Class Fees</th>

              </thead>
            <tbody>
              <?php 

              $count_stu = 0;

              if($class_id !== 'all')
              {

                $sql0060 = mysqli_query($conn,"SELECT * FROM `transactions` WHERE `CLASS_ID` = '$class_id'");

              }else
              if($class_id == 'all')
              {
                  $sql0060 = mysqli_query($conn,"SELECT * FROM `transactions`");
              }

                if(mysqli_num_rows($sql0060)>0)
                {

                      while($row0060 = mysqli_fetch_assoc($sql0060))
                      {
                        $stu_id = $row0060['STU_ID'];
                        $classes_id = $row0060['CLASS_ID']; //transaction table class id
                        $tra_id = $row0060['TRA_ID'];

                        $sql006 = mysqli_query($conn,"SELECT * FROM `payment_data` WHERE `CLASS_ID` = '$classes_id' AND `STU_ID` = '$stu_id' AND  `YEAR` = '$year' AND `MONTH` = '$month' AND `ADMIN_SUBMIT` = '1'");

                        if(mysqli_num_rows($sql006) == '0')
                        {
                              $count_stu = $count_stu+1;


                              $sql007 = mysqli_query($conn,"SELECT * FROM `student_details` WHERE `STU_ID` = '$stu_id'");
                              while($row007 = mysqli_fetch_assoc($sql007))
                              {
                                $stu_name = $row007['F_NAME']." ".$row007['L_NAME'];
                                $stu_tp = $row007['TP'];
                                $stu_gender = $row007['GENDER'];
                              

                                $sql008 = mysqli_query($conn,"SELECT * FROM `stu_login` WHERE `STU_ID` = '$stu_id'");
                                while($row008 = mysqli_fetch_assoc($sql008))
                                {
                                  $reg_id = $row008['REGISTER_ID'];
                                }

                                $sql009 = mysqli_query($conn,"SELECT * FROM `classes` WHERE `CLASS_ID` = '$classes_id'");
                                while($row009 = mysqli_fetch_assoc($sql009))
                                {
                                  $clz_fees = $row009['FEES'];
                                }

                                $mon_name = date('F', mktime(0, 0, 0, $month, 10)); // March

                                echo '
                                    <tr>
                                      <td>'.$stu_name.'</td>
                                      <td>'.$reg_id.'</td>
                                      <td>'.$stu_tp.'</td>
                                      <td>'.$stu_gender.'</td>
                                      <td>'.$year.' - '.$mon_name.'</td>
                                      <td class="text-right">'.$clz_fees.'</td>
                                    </tr>
                                  ';
                              }
                      }
                    }
                  }else
                  if(mysqli_num_rows($sql0060) == '0')
                  {
                    echo '<tr>
                            <td colspan="6" style="text-align: center;"><span class="fa fa-warning"></span> Not Found Data!</td>
                         </tr>';
                  }


               ?>


               <tr style="font-size: 14px;">
                 <th>
                   Expected amount
                 </th>
                 <th colspan="3"></th>
                 <th colspan="2" style="text-align: right;">LKR <?php echo number_format($count_stu*$clz_fees,2); ?></th>
               </tr>
               <tr style="font-size: 14px;">
                 <th>
                   No Of Students
                 </th>
                 <th colspan="3"></th>
                 <th colspan="2" style="text-align: right;"><?php echo $count_stu; ?></th>
               </tr>

                </tbody>
              </table>
        </div>


     <?php  } //Not Paid Students ?>


</div>

<div class="col-md-12" style="position: fixed;top:10px;right: 10px;">

  <button class="btn btn-success btn-lg print_btn" onclick="printDiv('printableArea')" style="font-size: 20px;float: right;border-radius: 80px;padding: 10px 13px 10px 13px;margin-left: 10px;outline: none;"><span class="fa fa-print"></span></button>

  <button class="btn btn-danger btn-lg print_btn" onclick="close_win();" style="font-size: 20px;float: right;border-radius: 80px;padding: 10px 13px 10px 13px;outline: none;"><span class="fa fa-times"></span></button>


</div>
</body>

</html>


