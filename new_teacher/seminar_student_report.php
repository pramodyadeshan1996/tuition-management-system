<?php 
  session_start();
  include('../connect/connect.php');

  $seminar_id = $_GET['seminar_id'];


  $sql9901 = mysqli_query($conn, "SELECT * FROM `seminar_master` WHERE `SEMI_ID` = '$seminar_id'");
  while($row9901 = mysqli_fetch_assoc($sql9901))
  {
    $semi_id = $row9901['SEMI_ID'];
    $teach_id = $row9901['TEACH_ID'];
    $register_date = $row9901['ADD_TIME'];
    $access_count = $row9901['ATTEND_COUNT'];
    $semi_code = $row9901['SEM_CODE'];
    $semi_day = $row9901['DAY'];
    $due_date = $row9901['DUE_DATE'];
    $seminar_name = $row9901['SEM_NAME'];
    $semi_link = $row9901['SEMINAR_LINK'];

    $start_time = $row9901['START_TIME']; //Class Start time
    $end_time = $row9901['END_TIME']; //Class End time

    $str = strtotime($start_time);
    $class_start_time = date('h:i A',$str);

    $str2 = strtotime($end_time);
    $class_end_time = date('h:i A',$str2);

    $due_time = $class_start_time." - ".$class_end_time;

    $sql9902 = mysqli_query($conn,"SELECT * FROM `teacher_details` WHERE `TEACH_ID` = '$teach_id'");
    while($row9902 = mysqli_fetch_assoc($sql9902))
    {
      $teacher_name = $row9902['POSITION'].". ".$row9902['F_NAME']." ".$row9902['L_NAME'];

    }

  }

  
  $sql = mysqli_query($conn,"SELECT * FROM institute WHERE INS_ID = '1'");
  while($row = mysqli_fetch_assoc($sql))
  {
    $n = $row['INS_NAME'];
    $p = $row['PICTURE'];
    if($row['INS_MOBILE'] !== '0')
    {
      $mobile = $row['INS_MOBILE'];
    }else
    {
      $mobile = "";
    }

    if($row['INS_TP'] !== '0')
    {
      $tele = $row['INS_TP'];
    }else
    {
      $tele = "";
    }


    if(!empty($mobile) && !empty($tele))
    {
      $no = $mobile." / ".$tele;
    }

    $a = $row['INS_ADDRESS'];
  }header('Content-type: text/html; charset=UTF-8');


    $date = date('Y-m-d');
    $time = date('h:i:s A');

 ?>
<html>
<head>
<style type="text/css" media="print,screen">
.hideMe{
display:block;
}

.PrintClass {
display:block;

}
.NoPrintClass{
display:block;
}

</style>
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script type="text/javascript"
    src="http://jqueryjs.googlecode.com/files/jquery-1.3.1.min.js"> </script>
    <link href="https://fonts.googleapis.com/css?family=PT+Sans+Narrow&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<script type="text/javascript">

function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     document.body.innerHTML = originalContents;

     window.print();
    window.onafterprint = function() {
    };
     

}
function close_win() {
   window.close();
}
   </script>
</head>
  <style type="text/css">
  @media print {
  .print_btn {
    display: none;
  }
}
</style>
</head>


<body>
 <div class="col-md-12" id="printableArea" style="display:block;">

 <p style="text-align:center">
        <?php 

        if(!empty($mobile) && !empty($tele))
        {
          $no = $mobile."/".$tele;
        }else
        if(!empty($mobile) && empty($tele))
        {
          $no = $mobile;
        }else
        if(!empty($tele) && empty($mobile))
        {
          $no = $tele;
        }else
        {
          $no = '';
        }


        echo '<center><img src="../admin/images/institute/'.$p.'" style="width:80px;height:80px;margin-bottom:10px;"></center>';

        if($p == "0")
        {
          echo '<h3 style="text-align:center;text-transform:uppercase;">'.$n.'</h3><p style="text-align:center">'.$a.'</p><p style="text-align:center">'.$no.'</p><div style="border-top: 1px dashed black;padding:0px;"></div>';
        }else
        if($p !== "0")
        {
          echo '<h3 style="text-align:center;text-transform:uppercase;">'.$n.'</h3><p style="text-align:center">'.$a.'</p><p style="text-align:center">'.$no.'</p><div style="border-top: 1px dashed black;padding:0px;"></div>';
        }

        
        ?>
      </p>


        <div class="row">
          <div class="col-md-4">

            <h4 style="float: left;text-transform: uppercase;">
          <?php echo "Date & Time : ".date('Y-m-d h:i:s A'); ?><br>
          <?php echo "Seminar Name : ".$seminar_name;  ?><br>
          <?php echo "Seminar Code : ".$semi_code;  ?><br>
          <?php echo "Teacher Name : ".$teacher_name;  ?><br>
          <?php echo "Held Date : ".$due_date;  ?><br>
          <?php echo "Held Time : ".$due_time;  ?><br><br>
          Total Attendance - <?php echo $access_count; ?>
            </h4>
            
            
          </div>
          <div class="col-md-4" style="text-align: center;padding-top: 15px;"><center><h1 style="text-align: center;margin-bottom: 20px;text-transform: uppercase;"><u>Seminar Report</u></h1></center>
          </div>
          <div class="col-md-4"></div>
        </div>
    
    <div style="border-top: 1px dashed black;margin-top: 10px;margin-bottom: 10px;"></div>
        <div class="table-responsive">
            
            <table class="table" style="font-size: 14px;">
              <thead>
                <th>Access Time</th>
                <th>Student Name</th>
                <th>Contact No</th>
              </thead>
            <tbody>
              <?php 

                $sql0033 = mysqli_query($conn,"SELECT * FROM `seminar_transaction` WHERE `SEMI_ID` = '$seminar_id'");

                $check_data = mysqli_num_rows($sql0033);

                if($check_data > 0)
                {

                  $count = 0;
                  while($row0033=mysqli_fetch_assoc($sql0033))
                  {
                    $name = $row0033['NAME'];
                    $reg_time = $row0033['REG_DT'];

                    $tp = $row0033['TP'];

                    echo '
                        <tr>
                          <td>'.$reg_time.'</td>
                          <td >'.$name.'</td>
                          <td >'.$tp.'</td>
                        </tr>
                      ';

                  }

                }else
                if($check_data == '0')
                {
                  echo '

                      <tr style="font-size: 14px;">
                       <th colspan="3" class="text-center text-danger"><span class="fa fa-warning"></span> Not Found Data!</th>
                     </tr>

                    ';
                }
                 


              ?>

               <tr style="font-size: 20px;font-weight: bold;">
                  <td>Total Attendance : </td>
                 <td colspan="3" class="text-right">
                   <?php echo number_format($access_count); ?>
                 </td>
               </tr>

                </tbody>
              </table>
        </div>
      
</div>

<div class="col-md-12" style="position: fixed;top:10px;right: 10px;">

  <button class="btn btn-success btn-lg print_btn" onclick="printDiv('printableArea')" style="font-size: 20px;float: right;border-radius: 80px;padding: 10px 13px 10px 13px;margin-left: 10px;outline: none;"><span class="fa fa-print"></span></button>

  <button class="btn btn-danger btn-lg print_btn" onclick="close_win();" style="font-size: 20px;float: right;border-radius: 80px;padding: 10px 13px 10px 13px;outline: none;"><span class="fa fa-times"></span></button>


</div>
</body>

</html>



