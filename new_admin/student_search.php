<?php
  $page = "Students Search";
  $folder_in = '0';

      include('header/header.php');

        $s04 = mysqli_query($conn,"SELECT * FROM admin_login WHERE ADMIN_ID = '$admin_id'");
        while($row4 = mysqli_fetch_assoc($s04))
        {
          $name = $row4['ADMIN_NAME'];
        }

 ?>

    <ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-top: 04%;margin-bottom: 0;">
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="Student Pending"> Students Search</li>
    </ol>


  <div class="col-md-12">
    
    <div class="row" style="padding-top: 10px;">
      <div class="col-md-1"></div>
      <div class="col-md-10 bg-white" style="border:1px solid #cccc;height: auto;">
      
      <div class="col-md-12" style="border-bottom: 1px solid #cccc;">
        <div class="row">

            <div class="col-md-9" style="margin-bottom: 10px;"><h2><a href="dashboard.php" data-toggle="tooltip" data-title="Back"><i class="pg-icon" style="font-size: 22px;">chevron_left</i></a> Students Search</h2></div>

        </div>
      </div>
          

<div class="card card-borderless">
<div class="col-md-12" style="padding-top:10px;">

<div class="tab-content">
<div class="tab-pane active" id="home">
<div class="row column-seperation">

 <?php 
    if(empty($_GET['stu_search']))
    { 
      $get_user_reg_id = '';
    }else
    if(!empty($_GET['stu_search']))
    {  
      $get_user_reg_id = $_GET['stu_search'];

    } ?>

<div class="col-lg-12">
 
  <div class="row" style="padding-bottom: 20px;border-bottom: 1px solid #cccc;padding-top: 20px;">
    <div class="col-md-9">
        <input type="text" id="myInput" class="form-control pull-right" style="width: 100%;padding: 20px 20px 20px 20px;font-size: 20px;margin-bottom: 10px;border-radius: 20px;" placeholder="Search" data-toggle="tooltip" autofocus data-title="අවශ්‍ය දත්ත සෙවීම'" value="<?php echo $get_user_reg_id; ?>" name="browser" onfocus="this.select();">

     <!--    <datalist id="browsers">
          <?php 
/* 
            $sql003 = mysqli_query($conn,"SELECT * FROM `stu_login` WHERE `STATUS` = 'Active'");
            while($row003 = mysqli_fetch_assoc($sql003))
            {
              $register_id = $row003['REGISTER_ID'];
              $stu_id = $row003['STU_ID'];

              $sql004 = mysqli_query($conn,"SELECT * FROM `student_details` WHERE `STU_ID` = '$stu_id'");
              while($row004 = mysqli_fetch_assoc($sql004))
              {
                $name = $row004['F_NAME']." ".$row004['L_NAME'];
                $tp = $row004['TP'];
              }

              echo '<option value="'.$register_id.'">'.$name.' | '.$tp.'</option>';
            } */

           ?>
         
        </datalist> -->


       
    </div>

    <div class="col-md-3">
        <button type="button" class="btn btn-success btn-block btn-lg" id="search_real_stu" style="padding-top:6px;padding-bottom:6px;font-size: 22px;border-radius:80px;"><i class="pg-icon" style="font-size: 30px;">search</i> Search</button></div>
    </div>
  

</div>



<div class="table-responsive" style="height: 550px;overflow: auto;margin-top: 4px;margin-top: 20px;">
<table class="table demo-table-search table-responsive-block text-left" id="tableWithSearch" style="display: none;">
<thead>
<tr>
<th style="width: 14%;">Register ID</th>
<th>Student Name</th>
<th style="width: 14%;">TP No</th>
<th>DOB</th>
<th style="width: 10%;">Gender</th>
<th>Status</th>
<th colspan="2" class="text-center">Action</th>
</tr>
</thead>
<tbody id="myTable">


</tbody>
</table>

<div class="col-md-12 text-center text-danger" id="not_msg"><span class="fa fa-warning"></span> Not Found Data!</div>
</div>

              
</div>
</div>
</div>
</div>




</div>
</div>



      </div>
      <div class="col-md-1"></div>
    </div>


  </div>




<script type="text/javascript">
  $('.save_btn').click(function(){
    
    var upload_btn0 = $('#selectedFile0').val();
    var upload_btn = $('#sel_file').val();
      
    if(empty(upload_btn0))
    {
      alert('Please select your audio file.');
    }else
    if(empty(upload_btn))
    {
      alert('Please select your document file.');
    }
   });
</script>

<script type="text/javascript">
  $('.navi').click(function(){
    $('.frm')[0].reset();
      
   });
</script>

<script>
$(document).ready(function(){
  $('#tableWithSearch').hide();


  $("#myInput").on("blur", function() {
    var value = $(this).val().toLowerCase();
    //alert(value);
    if(value !== '')
    {
      $('#tableWithSearch').show();
        $("#myTable tr").filter(function() {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
      });
    }

    if(value == '')
    {
      $('#tableWithSearch').hide();
    }
    
  });
});
</script>
<!-- search data -->

<!-- no data message/no found data show in search-->

<script type="text/javascript">
  $(document).ready(function () {

    (function ($) {

        $('#myInput').blur(function () {
            var rex = new RegExp($(this).val(), 'i');
            $('#myTable tr').hide();
            $('#myTable tr').filter(function () {
                return rex.test($(this).text());
            }).show();
            $('.no-data').hide();
            $('#not_msg').hide();

            if($('#myTable tr:visible').length == 0)
            {
                $('.no-data').show();

                $('#not_msg').hide();
            }

        })

    }(jQuery));

});
</script>




<?php include('footer/footer.php'); ?>
