<?php 

	include('../../connect/connect.php');
	session_start();
	$student_id = $_POST['student_id'];
	$teacher_id = $_POST['teacher_id'];
	$class_id = $_POST['class_id'];
	$last_attend = $_POST['last_attend']; //Last time of permission for students
	$paper_id = $_POST['paper_id'];
	$current_time = date('Y-m-d H:i:s');

	$str_last_attend = strtotime($last_attend);
	$str_current_time = strtotime($current_time);

	//Check student mcq transaction table available data

	$sql001 = mysqli_query($conn,"SELECT * FROM `student_mcq_main` WHERE `STU_ID` = '$student_id' AND `PAPER_ID` = '$paper_id'");

	$check_data = mysqli_num_rows($sql001); //Check availble data in student mcq transaction table

	if($check_data > 0)
	{
		//echo "OK";

		while($row001 = mysqli_fetch_assoc($sql001))
		{
			$mcq_main_id = $row001['STU_MAIN_ID'];
		}
	?>
		<!DOCTYPE html>
				<html>
				<head>
					<title></title>
				</head>
				<body onload="submit_data();">
					<script type="text/javascript">
						
						function submit_data() 
						{
							document.getElementById('submit_btn').click();
						}

					</script>
					<form action="index.php" method="POST">
						<input type="hidden" name="student_id" value="<?php echo $student_id; ?>">
				        <input type="hidden" name="teacher_id" value="<?php echo $teacher_id; ?>">
				        <input type="hidden" name="class_id" value="<?php echo $class_id; ?>">
				        <input type="hidden" name="mcq_main_id" value="<?php echo $mcq_main_id; ?>">
				        <input type="hidden" name="paper_id" value="<?php echo $paper_id; ?>">

				        <input type="hidden" name="status" value="1"> <!-- Exam held -->
						<input type="submit" value="OK" id="submit_btn" style="display:none;">
					</form>
				</body>
				</html>
		

	<?php
	}else
	if($check_data == '0')
	{

		if($str_current_time > $str_last_attend)
		{
			
			?>
				<!DOCTYPE html>
				<html>
				<head>
					<title>Time Out</title>
					<meta name="viewport" content="width=device-width, initial-scale=1.0">
					<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

					<!-- jQuery library -->
					<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

					<!-- Popper JS -->
					<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
					<script src='https://kit.fontawesome.com/a076d05399.js'></script>
					<!-- Latest compiled JavaScript -->
					<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
					
				</head>
				<body>
					
					<div class="container-fluid">
						<div class="row">
							
							<div class="col-md-4"></div>
							<div class="col-md-4" style="padding-top: 12%;">
								<center>


									<img src="images/icon/timeout.svg" class="image-reponsive" style="width: 250px;height: 250px;">



								</center>
							</div>
							<div class="col-md-4"></div>

						</div>
						<div class="row">
							<div class="col-md-3"></div>
							<div class="col-md-6">

								<center><h4 style="font-weight: 500;line-height: 1.5;">සමාවෙන්න! ඔබට විභාගය සදහා පෙනී සිටීමට ලබාදුන් කාලය අවසන් වී ඇත. </h4>

								<a href="../index.php" class="btn btn-danger" style="border-radius: 20px;box-shadow: 1px 1px 10px 3px #cccc;"><span class="fas fa-angle-double-left"></span> Go Back</a>

								</center>

							</div>
							<div class="col-md-3"></div>
						</div>
					</div>

				</body>
				</html>
				
		

			<?php




		}else
		{

			$sql004 = mysqli_query($conn,"SELECT * FROM `paper_master` WHERE `PAPER_ID` = '$paper_id'"); //check mcq test table
	        while($row004 = mysqli_fetch_assoc($sql004))
	        {

	            $h = $row004['TIME_DURATION_HOUR']; //MCQ Duration Hour
	            $m = $row004['TIME_DURATION_MIN']; //MCQ Duration Minuite

	        }

	        /*attend time convert to finish exam time*/

	        	$set_h_m = strtotime('+'.$h.' hours '.$m.' minute');
	        	$last_atten_time = date('Y-m-d H:i:s',$set_h_m);

	        /*attend time convert to finish exam time*/

			$insert001 = mysqli_query($conn,"INSERT INTO `student_mcq_main`(`ATTEND_TIME`, `PAPER_ID`, `STU_ID`,`START_TIME`) VALUES ('$last_atten_time','$paper_id','$student_id','$current_time')"); //save exam faced student records
			$mcq_main_id =mysqli_insert_id($conn);
			?>
				<!DOCTYPE html>
				<html>
				<head>
					<title></title>
				</head>
				<body onload="submit_data();">
					<script type="text/javascript">
						
						function submit_data() 
						{
							document.getElementById('submit_btn').click();
						}

					</script>
					<form action="index.php" method="POST">
						<input type="hidden" name="student_id" value="<?php echo $student_id; ?>">
				        <input type="hidden" name="teacher_id" value="<?php echo $teacher_id; ?>">
				        <input type="hidden" name="class_id" value="<?php echo $class_id; ?>">
				        <input type="hidden" name="mcq_main_id" value="<?php echo $mcq_main_id; ?>">
				        <input type="hidden" name="paper_id" value="<?php echo $paper_id; ?>">
				        <input type="hidden" name="status" value="1"> <!-- Exam held -->
						<input type="submit" value="OK" id="submit_btn" style="display:none;">
					</form>
				</body>
				</html>
				
		

			<?php


		}
		
	}

 ?>