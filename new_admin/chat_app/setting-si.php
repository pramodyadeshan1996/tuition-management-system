<?php 
include('connect.php');
$id = $_GET['id'];
 ?>
<!DOCTYPE html>
<html>
<head>
	<title>ගිණුම සැකසීම</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
  <style type="text/css">
    @media (max-width: 767px) {
  .table-wrap table,
  .table-wrap thead,
  .table-wrap tbody,
  .table-wrap th,
  .table-wrap td,
  .table-wrap tr {
    display: block;
  }
  .table-wrap thead tr {
    position: absolute;
    top: -9999px;
    left: -9999px;
  }
  .table-wrap td {
    border: none;
    border-bottom: 1px solid #eee;
    position: relative;
    padding-left: 50%!important;
    white-space: normal;
    text-align: left;
  }
  .control{
    width: 20px;
    height: 20px;
    font-size:10px;
    padding: 2px 2px 2px 2px;
  }
  .control thead tr {
    position: absolute;
    top: -9999px;
    left: -9999px;
  }
  .control td {
    border: none;
    border-bottom: 1px solid #eee;
    position: relative;
    padding-left: 50%!important;
    white-space: normal;
    text-align: left;
  }


  .table-wrap td:before {
    position: absolute;
    top: 8px;
    left: 15px;
    width: 45%;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
    text-align: left;
    font-weight: bold;
  }
  .table-wrap td:first-child {
    padding-top: 17px;
  }
  .table-wrap td:last-child {
    padding-bottom: 16px;
  }
  .table-wrap td:first-child:before {
    top: 17px;
  }
  .table-wrap td:before {
    content: attr(data-title);
  }
  .table tbody tr {
    border-top: 1px solid #ddd;
  }
}

@media (min-width: 768px) {
  .table thead tr th,
  .table tbody tr td {
    padding: 5px 8px;
  }
}
  </style>
</head>
<body>
<?php 
	$sql = mysqli_query($conn,"SELECT * FROM register WHERE REG_ID = '$id' ");
	while($row = mysqli_fetch_assoc($sql))
	{
		$f = $row['F_NAME'];
		$l = $row['L_NAME'];
		$p = $row['PIC'];
    $posi = $row['POSITION'];
    $cl = $row['COLOR'];
		if($p == '0')
		{
			$p = "user.png";
		}
		$t = $row['TP'];
		$ps = $row['PASSWORD'];
    $la = $row['LANGUAGE'];

	}
 ?>
<div class="container-fluid">


	<div class=" col-md-6" style="margin: 10% 22%;">
    <a href="profile-si.php?id=<?php echo $id; ?>" style="font-size: 20px;padding: 4px 12px 4px 12px;border-radius: 80px;margin-bottom: 12px;" class="btn btn-default"><span class="fa fa-arrow-left"></span></a>

    <a href="logout.php?id=<?php echo $id; ?>" style="font-size: 20px;padding: 4px 12px 4px 12px;border-radius: 80px;float: right;margin-bottom: 12px;" class="btn btn-default"><span class="fa fa-power-off"></span></a>


    
	<div class="panel-group" id="accordion">
  <div class="panel panel-default" style="padding: 10px 10px;">
    <div class="panel-heading">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
      <h4 class="panel-title">
        ගිණුම වෙනස් කිරීම
      </h4></a>
    </div>
    <div id="collapse1" class="panel-collapse collapse">
        <form action="action2.php" method="POST" enctype="multipart/form-data">
      <div class="panel-body">
          <input type="hidden" name="lan" value="sinhala">
      		<center><p><img src="images/<?php echo $p; ?>" style="width:10vw;height: 10vw;border-radius: 80px;"></p></center>
      		<div class="col-md-12" style="padding: 5px 0 5px 0;">මුල් නම : </div>
      		<div class="col-md-12" style="padding: 5px 0 5px 0;"><input type="text" name="fname1" value="<?php echo $f; ?>" class="form-control" pattern="[a-zA-Z. ]+" required></div>
      		<div class="col-md-12" style="padding: 5px 0 5px 0;">අවසාන නම : </div>
      		<div class="col-md-12" style="padding: 5px 0 5px 0;"><input type="text" name="lname1" value="<?php echo $l; ?>" class="form-control" pattern="[a-zA-Z. ]+" required></div>
      		<div class="col-md-12" style="padding: 5px 0 5px 0;">දු.ක අංකය : </div>
      		<div class="col-md-12" style="padding: 5px 0 5px 0;"><input type="text" name="tp1" value="0<?php echo $t; ?>" class="form-control" pattern="[0-9]{10}" required maxlength="10"></div>
      		<div class="col-md-12" style="padding: 5px 0 5px 0;">පින්තූර : </div>
      		<div class="col-md-12" style="padding: 5px 0 5px 0;"><input type="file" value="<?php echo $p; ?>" name="pic1" class="form-control"></div>
        </div>
        <div class="panel-footer"><button type="submit" name="myacupdate" class="btn btn-success" value="<?php echo $id; ?>">යාවත්කාලීන කිරීම</button></div>
      		
      </div>
    </form>
    </div>
  </div>


  <div class="panel panel-default" style="padding: 10px 10px;">
    <div class="panel-heading">
      <a data-toggle="collapse" data-parent="#accordion" href="#collapse4"><h4 class="panel-title">
        
        මුරපදය වෙනස් කිරීම
      </h4></a>
    </div>
    <div id="collapse4" class="panel-collapse collapse">
      <form action="action2.php" method="POST">
      <div class="panel-body">
         <input type="hidden" name="lan" value="sinhala">
        <div class="col-md-12" style="padding: 5px 0 5px 0;">දැනට භාවිත මුරපදය : </div>
          <div class="col-md-12" style="padding: 5px 0 5px 0;"><input type="password" id="curps" placeholder="දැනට භාවිත මුරපදය.." class="form-control" required name="current_psw">
           <div id="result" style="margin-top: 5px"></div> </div>

          <div class="col-md-12" style="padding: 5px 0 5px 0;">නව මුරපදය : </div>
          <div class="col-md-12" style="padding: 5px 0 5px 0;"><input type="password" id="newps" placeholder="නව මුරපදය.." class="form-control" required name="new_psw"></div>
          <div id="result2"></div>
        </div>
          <div class="panel-footer"><button class="btn btn-success" id="up" name="myupdate" value="<?php echo $id; ?>">යාවත්කාලීන කිරීම</button></div>
      
    </form>
    </div>
  </div>

  <div class="panel panel-default" style="padding: 10px 10px;">
    <div class="panel-heading">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
      <h4 class="panel-title">
        වර්ණය වෙනස් කිරීම <button class="btn" style="background-color:<?php echo $cl; ?>;padding: 5px 5px 5px 5px;border-radius: 80px;"></button>
      </h4></a>
    </div>
    <div id="collapse2" class="panel-collapse collapse">
      <div class="panel-body">
      	<div class="container">
      	<div class="row">
          <input type="hidden" value="<?php echo $id; ?>" id="id">
           <input type="hidden" name="lan" value="english">
        <button class="btn btn-danger avatar-icon color" style="border-radius: 80px;padding: 26px 30px 29px 30px;outline: none;" id="danger" value="#d9534f"><?php if($cl == '#d9534f'){echo '<span class="fa fa-check"></span>';} ?></button>
        <button class="btn btn-info avatar-icon color" style="border-radius: 80px;padding: 26px 30px 29px 30px;outline: none;" id="info" value="#5bc0de"><?php if($cl == '#5bc0de'){echo '<span class="fa fa-check"></span>';} ?></button>
        <button class="btn btn-primary avatar-icon color" style="border-radius: 80px;padding: 26px 30px 29px 30px;outline: none;" id="primary" value="#337ab7"><?php if($cl == '#337ab7'){echo '<span class="fa fa-check"></span>';} ?></button>
        <button class="btn avatar-icon color" style="border-radius: 80px;padding: 26px 30px 29px 30px;background-color: #00bfa5;outline: none;" id="whtspp" value="#00bfa5"><?php if($cl == '#00bfa5'){echo '<span class="fa fa-check" style="color:white;"></span>';} ?></button>

        <button class="btn avatar-icon  color" style="border-radius: 80px;padding: 26px 30px 29px 30px;background-color: purple;outline: none;" id="purple" value="purple"><?php if($cl == 'purple'){echo '<span class="fa fa-check" style="color:white;"></span>';} ?></button>
        <button class="btn avatar-icon  color" style="border-radius: 80px;padding: 26px 30px 29px 30px;background-color: orange;outline: none;" id="orange" value="orange"><?php if($cl == 'orange'){echo '<span class="fa fa-check" style="color:white;"></span>';} ?></button>
        <button class="btn avatar-icon  color" style="border-radius: 80px;padding: 26px 30px 29px 30px;background-color: #5f5a5f;outline: none;" id="gray" value="#5f5a5f"><?php if($cl == '#5f5a5f'){echo '<span class="fa fa-check" style="color:white;"></span>';} ?></button>
      </div>
  	</div>
      </div>
    </div>
  </div>

  <script type="text/javascript">
    $(document).ready(function(){
      $('.color').click(function(){
        var color = $(this).val();
        var id = $('#id').val();
        $.ajax({
           url:'action2.php',
           type: 'POST',
           data: {id:id,color:color},
           success: function (data) {
            const Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 3000,size:100
            })

            Toast.fire({
              type: 'success',
              title: 'වර්ණය වෙනස් කලා'
            })
            setTimeout(function(){location.reload(); }, 1500);
           }

      });
      });
    });
  </script>
  
  <div class="panel panel-default" style="padding: 10px 10px;">
    <div class="panel-heading">
      <a data-toggle="collapse" data-parent="#accordion" href="#collapse3"><h4 class="panel-title">
        භාෂාව වෙනස් කිරීම
      </h4></a>
    </div>
    <div id="collapse3" class="panel-collapse collapse">
      <form action="action2.php" method="POST">
      <div class="panel-body">
         <input type="hidden" name="lan" value="sinhala">
      	<select class="form-control" name="language">
          <?php 
            
            if($la == 'English')
            {
              echo '<option value='.$la.'>'.$la.'</option>';
              echo '<option value="Sinhala">සිංහල</option>';
            }else
            if($la == 'Sinhala')
            {
              echo '<option value='.$la.'>සිංහල</option>';
              echo '<option value="English">English</option>';
            }
           ?>
      	</select>
      </div>
      	<div class="panel-footer">
          <button type="submit" class="btn btn-success" name="submit_lan" value="<?php echo $id; ?>">යාවත්කාලීන කිරීම</button></div>
      
      </form>
    </div>
  </div>

  <?php 
    if($posi == 'Admin')
    {
   ?>
  <div class="panel panel-default" style="padding: 10px 10px;">
    <div class="panel-heading">
      <a data-toggle="collapse" data-parent="#accordion" href="#collapse5"><h4 class="panel-title">
        පාලන කටයුතු
      </h4></a>
    </div>
    <div id="collapse5" class="panel-collapse collapse">
      <div class="panel-body">
        <div class="col-md-12"><button class="btn btn-info" style="border-radius: 80px;float: right;" data-toggle="modal" data-target="#add"><span class="fa fa-plus"></span></button></div>
        <div class="col-md-12"><hr></div>

<div id="add" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">නව පරිශීලක ලියාපදිංචිය</h4>
      </div>
        <form action="register.php" method="POST" enctype="multipart/form-data">
      <div class="modal-body">
          <input type="hidden" name="lan" value="sinhala">
          <div class="col-md-12" style="padding: 5px 0 5px 0;">මුල් නම : </div>
          <div class="col-md-12" style="padding: 5px 0 5px 0;"><input type="text" name="fname5" placeholder="මුල් නම ඇතුලත් කරන්න.." class="form-control" pattern="[a-zA-Z. ]+" required></div>
          <div class="col-md-12" style="padding: 5px 0 5px 0;">අවසාන නම : </div>
          <div class="col-md-12" style="padding: 5px 0 5px 0;"><input type="text" name="lname5" placeholder="අවසාන නම ඇතුලත් කරන්න.." class="form-control" pattern="[a-zA-Z. ]+" required></div>
          <div class="col-md-12" style="padding: 5px 0 5px 0;">දු.ක අංකය : </div>
          <div class="col-md-12" style="padding: 5px 0 5px 0;"><input type="text" name="tp5" placeholder="දු.ක අංකය ඇතුලත් කරන්න.." class="form-control" pattern="[0-9]{10}" required maxlength="10"></div>
          <div class="col-md-12" style="padding: 5px 0 5px 0;">පරිශීලක නම : </div>
          <div class="col-md-12" style="padding: 5px 0 5px 0;"><input type="text" name="usernm5" id="username2" placeholder="පරිශීලක නම ඇතුලත් කරන්න.." class="form-control" required></div>

          <div id="res1"></div>

          <div class="col-md-12" style="padding: 5px 0 5px 0;">මුර පදය : </div>
          <div class="col-md-12" style="padding: 5px 0 5px 0;"><input type="text" id="newps22" name="pass5" placeholder="මුරපදය පදය ඇතුලත් කරන්න.." class="form-control" required ></div>
          <div class="col-md-12" style="padding: 5px 0 5px 0;">නැවත මුරපදය : </div>
          <div class="col-md-12" style="padding: 5px 0 5px 0;"><input type="text" id="resps22" name="repass5" placeholder="නැවත මුරපදය ඇතුලත් කරන්න.." class="form-control" required></div>
          <div id="res2"></div>
          <div class="col-md-12" style="padding: 5px 0 5px 0;">පින්තූර : </div>
          <div class="col-md-12" style="padding: 5px 0 5px 0;"><input type="file" value="<?php echo $p; ?>" name="pic5" class="form-control"></div>
       
      </div>
      <div class="modal-footer" style="border:none;">
        <button type="submit" class="btn btn-success" name="add" id="add" value="<?php echo $id; ?>">යාවත්කාලීන කිරීම</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">අවලංගු කිරීම</button>
      </div>
       </form>
    </div>

  </div>
</div>

        <div class="table-wrap">
          <table class="table">
          <thead>
            <tr>
              <th>නම</th>
              <th>දු.ක අංකය</th>
              <th>තනතුර</th>
              <th colspan="2">
              ක්‍රියාකාරකම්
              </th>
            </tr>
          </thead>
          <tbody style="font-size: 1.2vw;">
            <?php 
              $sql5 = mysqli_query($conn,"SELECT * FROM register WHERE POSITION != 'Admin'");
              $empty = mysqli_num_rows($sql5);
              if($empty>0)
              {
              while($row5 = mysqli_fetch_assoc($sql5))
              {
                $regid = $row5['REG_ID'];
                $fn = $row5['F_NAME'];
                $ln = $row5['L_NAME'];
                $tp5 = $row5['TP'];
                $position5 = $row5['POSITION'];

             ?>
            <tr>
              <td><?php echo $fn." ".$ln; ?></td>
              <td>0<?php echo $tp5; ?></td>
              <td><?php echo $position5; ?></td>
              <td colspan="2">
                <button class="btn btn-info control" style="border-radius: 80px;outline: none;" data-toggle="modal" data-target="#edit<?php echo $regid; ?>"><span class="fa fa-edit"></span></button>
                <button class="btn btn-danger control" style="border-radius: 80px;outline: none;" data-toggle="modal" data-target="#delete<?php echo $regid; ?>"><span class="fa fa-trash"></span></button>
              </td>
            </tr>

            
            <div id="edit<?php echo $regid; ?>" class="modal fade" role="dialog">
              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">වෙනස්කම් කිරීම</h4>
                  </div>
                   <form action="action2.php" method="POST">
                     <input type="hidden" name="lan" value="sinhala">
                  <div class="modal-body">
                    <input type="hidden" name="member_id" value="<?php echo $id; ?>">
                    <div class="col-md-12" style="padding: 5px 0 5px 0;">මුල් නම : </div>
                    <div class="col-md-12" style="padding: 5px 0 5px 0;"><input type="text" name="fname" value="<?php echo $fn; ?>" class="form-control" pattern="[a-zA-Z. ]+" required></div>
                    <div class="col-md-12" style="padding: 5px 0 5px 0;">අවසාන නම : </div>
                    <div class="col-md-12" style="padding: 5px 0 5px 0;"><input type="text" name="lname" value="<?php echo $ln; ?>" class="form-control" pattern="[a-zA-Z. ]+" required></div>
                    <div class="col-md-12" style="padding: 5px 0 5px 0;">දු.ක අංකය : </div>
                    <div class="col-md-12" style="padding: 5px 0 5px 0;"><input type="text" name="tp" value="<?php echo $tp5; ?>" class="form-control" pattern="[0-9]{10}" required maxlength="10"></div>
                  </div>
                  <div class="modal-footer" style="border:none;">
                      <button class="btn btn-success" type="submit" value="<?php echo $regid; ?>" name="update_memeber">යාවත්කාලීන කිරීම</button>
                      <button type="button" class="btn btn-danger" data-dismiss="modal">අවලංගු කිරීම</button>
                  </div>
                </form>
                </div>

              </div>
            </div>



            <div id="delete<?php echo $regid; ?>" class="modal fade" role="dialog">
              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">ඉවත් කිරීම</h4>
                  </div>
                  <div class="modal-body">
                    මෙම පරිශීලකයා ඉවත්කිරීමට අවසර දෙනවාද?
                  </div>
                  <div class="modal-footer">
                    <form action="action2.php" method="POST">
                       <input type="hidden" name="lan" value="sinhala">
                      <input type="hidden" name="member_id" value="<?php echo $id; ?>">
                      <button type="submit" value="<?php echo $regid; ?>" class="btn btn-info" name="delete_member">ඔව්</button>
                      <button type="button" class="btn btn-danger" data-dismiss="modal">නැත</button>
                    </form>
                  </div>
                </div>

              </div>
            </div>


          <?php }
          }
          elseif ($empty == "0") {
             echo '<tr><td colspan="4" style="text-align:center;"><span class="fa fa-warning"></span> කිසිදු දත්තයක් නොමැත.</td></tr>';
           } ?>
           </div>
          </tbody>
        </table>
    </div>
  </div>
  <?php 
    }
   ?>
</div>
	</div>
	 <?php 

  if($posi == 'Admin')
    {?>
       <div class="panel panel-default" style="padding: 10px 10px;">
    <div class="panel-heading">
      <a data-toggle="collapse" data-parent="#accordion" href="#collapse6"><h4 class="panel-title">
        ලියාපදිංචි ඉල්ලීම්

        <?php 
          $sql22 = mysqli_query($conn,"SELECT * FROM register WHERE ACCEPT = '0'");
          $check = mysqli_num_rows($sql22);
          if($check > 0)
          {
            echo '<span class="badge">'.$check.'</span>';
          }else
          if($check == '0')
          {
            echo '<span class="badge">'.$check.'</span>';
          }else

         ?>
      </h4></a>
    </div>
    <div id="collapse6" class="panel-collapse collapse">
      <form action="action.php" method="POST">
      <div class="panel-body">
         <input type="hidden" name="lan" value="english">
          <table class="table">
            <tr>
              <th>මුල් නම</th>
              <th>අවසන් නම</th>
              <th>දු.ක අංකය</th>
              <th>දිනය</th>
              <th style="text-align: center">එකගයි</th>
            </tr>
            <?php 
              $sql234 = mysqli_query($conn,"SELECT * FROM register WHERE ACCEPT = '0'");
              $empty = mysqli_num_rows($sql234);
              if($empty>0)
              {
                  while($roo = mysqli_fetch_assoc($sql234))
                  {
                    $id = $roo['REG_ID'];
                    $f = $roo['F_NAME'];
                    $l = $roo['L_NAME'];
                    $t = $roo['TP'];
                    $ac = $roo['ACCEPT'];
                    $da = $roo['LOG_TIME'];

                    echo '<tr>
                      <td>'.$f.'</td>
                      <td>'.$l.'</td>
                      <td>0'.$t.'</td>
                      <td>'.$da.'</td>
                      <td style="text-align:center;">
                      <form action="action.php" method="POST">
                        <input type="hidden" name="lan" value="sinhala">
                        <button type="submit" value='.$id.' name="accept" class="btn btn-success">ඔව්</button>
                      </form></td>
                    </tr>';
                  }
              }elseif ($empty == "0") {
             echo '<tr><td colspan="4" style="text-align:center;"><span class="fa fa-warning"></span> කිසිදු දත්තයක් නොමැත.</td></tr>';
           }
             ?>
            
            
          </table>
      </div>
      
      </form>
    </div>
  </div>
  <?php
  } ?>
</div>
</div>
</div>

</body>
</html>
<script type="text/javascript">
  $(function() {
  $(".table-wrap").each(function() {
    var nmtTable = $(this);
    var nmtHeadRow = nmtTable.find("thead tr");
    nmtTable.find("tbody tr").each(function() {
      var curRow = $(this);
      for (var i = 0; i < curRow.find("td").length; i++) {
        var rowSelector = "td:eq(" + i + ")";
        var headSelector = "th:eq(" + i + ")";
        curRow.find(rowSelector).attr('data-title', nmtHeadRow.find(headSelector).text());
      }
    });
  });
});
</script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#curps').keyup(function(){
      var cpass = $(this).val();
      if(cpass=='')
      {
        $('#result').html('');
      }else
      {
        $.ajax({
           url:'action2.php',
           type: 'POST',
           data: {cupass:cpass},
           success: function (data) {
            if(data>0)
            {
              $('#result').html('<div class="alert alert-success"><span class="fa fa-check"></span> ඔබ ඇතුලත් කල මුරපදය නිවැරදියි!</div>');
               $('#up').prop("disabled",false);
            }else
            if(data==0)
            {
              $('#result').html('<div class="alert alert-danger"><span class="fa fa-times"></span> ඔබ ඇතුලත් කල මුරපදය වැරදියි!</div>');
               $('#up').prop("disabled",true);
            }
           
           }

      });
      }
      
    });

    $('#newps').keyup(function(){
      var cpass = $(this).val().length;
      var nps = $('#curps').val().length;
      if(nps == 0)
      {
        $('#up').prop("disabled",true);
      }else
      {
        if(cpass<0)
      {
        $('#result2').html('');
         $('#up').prop("disabled",true);

      }
      if(cpass<2)
      {
        $('#result2').html('<div class="progress-bar  progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:10%;background-color:purple;">10%</div>');
         $('#up').prop("disabled",true);
      }else
      if(cpass<3)
      {
        $('#result2').html('<div class="progress-bar progress-bar-danger  progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:30%">30%</div>');
         $('#up').prop("disabled",true);
      }else
      if(cpass<4)
      {
        $('#result2').html('<div class="progress-bar progress-bar-info  progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:50%">50%</div>');
         $('#up').prop("disabled",true);
      }
      else
      if(cpass<5)
      {
        $('#result2').html('<div class="progress-bar progress-bar-primary  progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:70%">70%</div>');
         $('#up').prop("disabled",false);
      }
      else
      if(cpass<6)
      {
        $('#result2').html('<div class="progress-bar progress-bar-success  progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:90%">90%</div>');
        $('#up').prop("disabled",false);

      }
      else
      if(cpass<7)
      {
        $('#result2').html('<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:100%;background-color:#431dde">100%</div>');
         $('#up').prop("disabled",false);
      }
      }
    });


  });
</script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#username2').keyup(function(){
    var user2 = $('#username2').val();
    var u = $('#username2').val().length;
    if(u == '') 
    {
      $('#res1').html('');
      $('#add').prop("disabled",true);
    }else
    {
      $.ajax({
        url:'action2.php',
        type: 'POST',
        data: {user:user2},
        success: function (data) {

          if(data > 0)
          {
            $('#res1').html('<div style=color:red>දැනටමත් පරිශීලක නම ඇතුලත් කර ඇත.</div>');
            $('#add').prop("disabled",false);
          }
          if(data == 0)
          {
            $('#res1').html(' ');
            $('#add').prop("disabled",true);
          }
          
          }
       });
    }
    
    });
    
  });
</script>

<script type="text/javascript">
  $(document).ready(function(){
    $('#resps22').keyup(function(){
      var np2 = $('#newps22').val();
      var rp2 = $(this).val();
      if(rp2 == '')
      {
        $('#res2').html(' ');
      }else
      {
        if(np2 == rp2)
        {
          $('#res2').html('<div style=color:red>නැවත ඇතුලත් කල මුරපදය නිවැරදියි.</div>');
        }else
        {
          $('#res2').html('<div style=color:red>නැවත ඇතුලත් කල මුරපදය වැරදියි.</div>');
        }
      }
      
    });

  });
</script>