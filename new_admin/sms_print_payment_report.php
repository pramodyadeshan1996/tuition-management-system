<?php 

  include('../connect/connect.php');

  $start_date = $_POST['start_date'];
  $end_date = $_POST['end_date'];


 ?>
<!DOCTYPE html>
<html>
<head>
	<style type="text/css" media="print,screen">
.hideMe{
display:block;
}

.PrintClass {
display:block;

}
.NoPrintClass{
display:block;
}
table {
   width: 100%;
   border-collapse: collapse;
   table-layout: fixed;
}

.cell-breakWord {
   word-wrap: break-word;
}

</style>
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script type="text/javascript"
    src="http://jqueryjs.googlecode.com/files/jquery-1.3.1.min.js"> </script>
    <link href="https://fonts.googleapis.com/css?family=PT+Sans+Narrow&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">

<script type="text/javascript">


function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     document.body.innerHTML = originalContents;

     window.print();
    window.onafterprint = function() {
    window.close();
    };
     

}

   </script>

</head>
<body onload="printDiv('printableArea')">



<div class="col-md-12" id="printableArea" style="display:block;">
  
  <?php 

		  
      $sql0027 = mysqli_query($conn,"SELECT * FROM institute WHERE INS_ID = '1'");
      while($row0027 = mysqli_fetch_assoc($sql0027))
      {
          $ins_name = $row0027['INS_NAME'];
          $ins_tp = $row0027['INS_TP'];
          $ins_mobile = $row0027['INS_MOBILE'];
          $ins_pic = $row0027['PICTURE'];

          if($ins_tp == '0' && $ins_mobile == '0')
          {
            $institute_tp = "";
          }else
          {
            if($ins_tp == '0')
            {
              if($ins_mobile != '0')
              {
                $institute_tp = $ins_mobile;
              }
            }

            if($ins_mobile == '0')
            {
              if($ins_tp != '0')
              {
                $institute_tp = $ins_tp;
              }
            }

            if($ins_tp !== '0' && $ins_mobile !== '0')
            {
              
              
                $institute_tp = $ins_tp."/".$ins_mobile;
              
            }


          }

          
          
          $ins_address = $row0027['INS_ADDRESS'];
      }
	




   ?>

<div class="row">
    
    <div class="col-md-4"></div>
    <div class="col-md-4">
      <?php if($ins_pic !== '0'){ ?>
      <center><img src="../admin/images/institute/<?php echo $ins_pic; ?>" height="100px" width="100px" style="border-radius:80px;"></center>
    <?php } ?>

      <h1 style="text-align: center;"><?php echo $ins_name; ?></h1>
      <h5 style="text-align: center;"><?php echo $ins_address; ?> <br> <?php echo $institute_tp; ?></h5>

    </div>
    <div class="col-md-4"></div>

  </div>


  <div class="col-md-12" style="border-top: 1px solid #cccc;"></div>
  <div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-4 text-center" style="padding: 20px 0 20px 0;"><h1>SMS Payments Report</h1></div>
    <div class="col-md-4"></div>
  </div>
  <div class="col-md-12">

                            <div class="table-responsive" style="margin-top: 4px;font-size: 14px;">
                            <table class="table demo-table-search table-responsive-block text-left" id="tableWithSearch2">
                            <thead>
                            <tr>
                            <th>Pay Time</th>
                            <th>Remained SMS</th>
                            <th>Remaining SMS</th>
                            <th>Current Payments</th>
                            <th style="width: 8%;">Added SMS</th>
                            <th>Paid Amount</th>
                            </tr>
                            </thead>
                            <tbody id="myTable2">

                            <tr class="no-data2 col-md-12 alert alert-danger" style="margin-top: 20px;display: none;">
                              <td><span class="fa fa-warning"></span> Not Found Data</td>
                            </tr>

                            <?php 
                        $sql0055 = mysqli_query($conn,"SELECT * FROM `sms_counter` WHERE `ADD_DATE` BETWEEN '$start_date' AND '$end_date' ORDER BY ADD_D_T DESC");
                            
                              if(mysqli_num_rows($sql0055)>0)
                              {
                                while($row0055=mysqli_fetch_assoc($sql0055))
                                {
                                  $pay = $row0055['ADD_D_T'];
                                  $payment = $row0055['PAYMENT'];
                                  $add_sms = $row0055['ADD_SMS'];
                                  $remained_sms = $row0055['REMAINED_SMS'];
                                  $current_sms = $row0055['CURRENT_SMS'];
                                  $current_amount = $row0055['CURRENT_AMOUNT'];
                                
                                  echo '<tr>
                                    <td class="v-align-middle">'.$pay.'</td>
                                    <td class="v-align-middle">'.number_format($remained_sms).'</td>
                                    <td class="v-align-middle">'.number_format($current_sms).'</td>
                                    <td class="v-align-middle">'.number_format($current_amount,2).'</td>
                                    <td class="v-align-middle">'.number_format($add_sms).'</td>
                                    <td class="v-align-middle">'.number_format($payment,2).'</td>
                                  </tr>';
                                }
                              }else
                              if(mysqli_num_rows($sql0055)=='0')
                              {
                                echo '<tr><td colspan="8" class="text-danger text-center"><span class="fa fa-warning text-danger"></span> Empty Data!</td></tr>';
                              }
                              

                             ?>
                           
                          </tbody>
                        </table>
                      </div>
</div>
        
</div> 

</body>
</html>