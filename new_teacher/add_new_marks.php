  
<?php

$page = "External Paper Marking";
$folder_in = '0';

$paper_id = $_GET['paper_id'];

  



  include('header/header.php'); 
 ini_set( "display_errors", 0); 

 $teacher_id001 = $_SESSION['TEACH_ID'];


  $sql004 = mysqli_query($conn,"SELECT * FROM `paper_master` WHERE `PAPER_ID` = '$paper_id'");

  while($row004 = mysqli_fetch_assoc($sql004))
  {
        $paper_name = $row004['PAPER_NAME'];
        $start_date = $row004['START_DATE'];
  }

  ?>
  <style type="text/css">

    #canvas_container {
          width: auto;
          height: 500px;
          overflow: auto;
      }
 
      #canvas_container {
        background: #ccc;
        text-align: center;
        width: 100%;
      }

      #canvas_container2 {
        margin-top: 6px;
        margin-bottom: 6px;
          width: auto;
          height: 500px;
          overflow: auto;
        width: 100%;
      }
 
      #canvas_container2 {
        background: #ccc;
        text-align: center;
      }

    .btn_pulse {
  
  display: block;
  border-radius: 10%;
  cursor: pointer;
  animation: none;
  float: left;
  bottom: 5px;
  right: 5px;
  font-weight: bold;
  padding-top: 2px;
  text-align: center;
  color: white;
  font-size: 14px;
  animation: pulse 1.6s infinite;
}


@keyframes pulse {
  0% {
    -moz-box-shadow: 0 0 0 0 #7252d3;
    box-shadow: 0 0 0 0 #7252d3;
  }
  70% {
    -moz-box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
  }
  100% {
    -moz-box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
  }
}
  </style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<br>
<br>

<div class="col-md-12" style="border-top: 1px solid #cccc;margin-top: 10px;">

<div style="margin: 10px 0px 10px 0;"><a href="exam.php" style="text-decoration: none;color: gray;"><span class="fa fa-angle-left"></span> Back</a>


</div>

<form action="../teacher/query/insert.php" method="POST">
<div class="row" style="margin-top: 1%;margin-bottom: 3%;">
<div class="col-md-5">

  <div class="col-md-12 bg-white" style="padding: 20px 20px 20px 20px;">
    <div style="border-bottom: 1px solid #cccc;">
      <label class="text-muted text-center" style="font-size: 30px;"><span class="fa fa-search" style="font-size: 30px;"></span><strong> Search Student</strong></label>
    </div>

  <h3 class="text-muted text-center"><?php echo $paper_name; ?> <p style="font-size: 14px;">(<?php echo $start_date; ?>)</p></h3> 

    <div class="col-md-12" style="padding-top: 10px;height: 200px;">
      
      <form action="../teacher/query/insert.php" method="POST">
      <div class="row">
        <div class="col-md-12">

          <div class="form-group form-group-default input-group">
              <div class="form-input-group">
                <label>Student Name</label>

                  <input list="student_data" name="student_data" id="student_data2" class="form-control" placeholder="Student Name | Registered ID" required>

                  <datalist id="student_data">
                    <?php 
                          $sql002 = mysqli_query($conn,"SELECT * FROM `student_details`");

                          while($row002 = mysqli_fetch_assoc($sql002))
                          {
                                $student_name = $row002['F_NAME']." ".$row002['L_NAME'];
                                $st_id = $row002['STU_ID'];

                                $sql00311 = mysqli_query($conn,"SELECT * FROM `stu_login` WHERE `STU_ID` = '$st_id'");

                                while($row00311 = mysqli_fetch_assoc($sql00311))
                                {
                                      $reg_id = $row00311['REGISTER_ID'];
                                }

                                $sql005 = mysqli_query($conn,"SELECT * FROM `exam_result` WHERE `STU_ID` = '$st_id' AND `PAPER_ID` = '$paper_id'");
                                if(mysqli_num_rows($sql005) == '0')
                                {
                                  echo '<option value="'.$reg_id.'">'.$student_name.'</option>';
                                }
                                
                          }
                     ?>
                      
                  </datalist>
              
              </div>
          </div>

          <div class="form-group form-group-default input-group">
            <div class="form-input-group">
                <label>Paper Mark</label>
                <input type="number" name="total_marks" class="form-control" placeholder="Enter Total Marks" min="0" required>
            </div>
          </div>

          <div style="padding-top: 10px;">

            <button type="submit" class="btn btn-success btn-lg btn-block" data-toggle="tooltip" data-title="තහවුරු කිරීම" style="padding: 8px 8px 8px 8px;font-size: 16px;" value="<?php echo $paper_id; ?>" name="add_new_student_marks" ><i class="pg-icon" style="font-size: 22px;">tick_circle</i> Add Marks</button>
          </div>

        </div>

      </div>
      </form>

      
    </div>
  </div>

  

</div>

<div class="col-md-7">

  <div class="col-md-12 bg-white" style="padding: 20px 20px 20px 20px;">

    <div style="border-bottom: 1px solid #cccc;">
      <label class="text-muted text-center" style="font-size: 30px;"><span class="fa fa-plus"></span> <strong> Added Marks</strong></label>
    </div>

    <?php 

      if(empty($_SESSION['new_student_marks']))
      {
        echo '

            <div class="row">
              <div class="col-md-4"></div>
              <div class="col-md-4">
                  
                  <div class="col-md-12" style="padding: 10px 10px 20px 10px;">
                    <center>

                      <div style="background-color: #10ac84;height: 138px;width: 138px;border-radius: 80px;">
                        
                        <h1 style="text-align: center;padding-top: 28%;font-size: 50px;" class="text-white">0 <p style="font-size: 18px;padding-top: 8px;">Marks</p></h1>

                      </div>

                    </center>
                  </div>

              </div>
              <div class="col-md-4"></div>
            </div>

            <div class="row" style="border-top: 1px solid #cccc;padding-top: 20px;padding-bottom: 20px;">
              <div class="col-md-6">
                <label style="font-size: 14px;">Student Name</label> <br>
                <label style="font-size: 22px;font-weight: bold;">XXXXXX</label>
              </div>

              <div class="col-md-6">
                <label style="font-size: 14px;">Registered ID</label> <br>
                <label style="font-size: 22px;font-weight: bold;">XXXXXX</label>
              </div>
            </div>


        ';
      }else
      if(!empty($_SESSION['new_student_marks']))
      {
        echo '

            <div class="row">
              <div class="col-md-4"></div>
              <div class="col-md-4">
                  
                  <div class="col-md-12" style="padding: 10px 10px 20px 10px;">
                    <center>

                      <div style="background-color: #10ac84;height: 138px;width: 138px;border-radius: 80px;">
                        
                        <h1 style="text-align: center;padding-top: 28%;font-size: 50px;" class="text-white">'.$_SESSION['new_student_marks'].' <p style="font-size: 18px;padding-top: 8px;">Marks</p></h1>

                      </div>

                    </center>
                  </div>

              </div>
              <div class="col-md-4"></div>
            </div>

            <div class="row" style="border-top: 1px solid #cccc;padding-top: 20px;padding-bottom: 20px;">
              <div class="col-md-6">
                <label style="font-size: 14px;">Student Name</label> <br>
                <label style="font-size: 22px;font-weight: bold;">'.$_SESSION['added_student'].'</label>
              </div>

              <div class="col-md-6">
                <label style="font-size: 14px;">Registered ID</label> <br>
                <label style="font-size: 22px;font-weight: bold;">'.$_SESSION['added_reg_id'].'</label>
              </div>
            </div>


        ';
      }

     ?>

    

  </div> 
</div>


</div>

</form>

<script type="text/javascript">
  $(document).ready(function(){
      //$('#pay_btn').prop('disabled',true); //refresh with disable button

      setTimeout(function() {
          <?php $_SESSION['msg'] = ''; ?>
          $('#successMessage').fadeOut('slow');
      }, 3000); // <-- time in milliseconds
  });
</script>

<?php  include('footer/footer.php'); ?>


<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
<!-- search data -->

<!-- no data message/no found data show in search-->

<script type="text/javascript">
  $(document).ready(function () {

    (function ($) {

        $('#myInput').keyup(function () {
            var rex = new RegExp($(this).val(), 'i');
            $('#myTable tr').hide();
            $('#myTable tr').filter(function () {
                return rex.test($(this).text());
            }).show();
            $('.no-data').hide();
            if($('#myTable tr:visible').length == 0)
            {
                $('.no-data').show();
            }

        })

    }(jQuery));

});
</script>

<script type="text/javascript">


    function find_class()
    {
                  
        var teacher_id = document.getElementById('teacher_id').value;

         $.ajax({
          url:'../teacher/query/check.php',
          method:"POST",
          data:{check_teacher_mcq:teacher_id},
          success:function(data)
          {
            //alert(data)
            document.getElementById('class_id001').innerHTML = data;
          }
        });
  }


</script>

<script type="text/javascript">


    function add_questions()
    {
      var show_question = document.getElementById('question').value;
      //alert(show_question)
      $.ajax({
          url:'../teacher/query/check.php',
          method:"POST",
          data:{str_que_ans:show_question},
          success:function(data)
          {
            //alert(data)
            document.getElementById('que_ans').innerHTML = data;
          }
        });
  }


</script>

