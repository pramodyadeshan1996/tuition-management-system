
<?php

$page = "Create Grade";
$folder_in = '0';

include('header/header.php'); 

$level_id = $_GET['level_id'];

$sql001 = mysqli_query($conn,"SELECT * FROM `level` WHERE `LEVEL_ID` = '$level_id'");
while($row001=mysqli_fetch_assoc($sql001))
{
    $my_level_name = $row001['LEVEL_NAME'];

}

 ini_set( "display_errors", 0); 


  ?>
  <style type="text/css">
    .btn_pulse {
  
  display: block;
  border-radius: 10%;
  cursor: pointer;
  animation: none;
  float: left;
  bottom: 5px;
  right: 5px;
  font-weight: bold;
  padding-top: 2px;
  text-align: center;
  color: white;
  font-size: 14px;
  animation: pulse 1.6s infinite;
}


@keyframes pulse {
  0% {
    -moz-box-shadow: 0 0 0 0 #7252d3;
    box-shadow: 0 0 0 0 #7252d3;
  }
  70% {
    -moz-box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
  }
  100% {
    -moz-box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
  }
}
  </style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<div class="row" style="border-bottom: 1px solid #cccc;margin-top: 60px;">
  <div class="col-md-11">
    <ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-bottom: 04%;margin-bottom: 0;">
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="add_level.php">Create level</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="Create Grade"> Create Grade</li>
    </ol>
  </div>
  <div class="col-md-1">
        <div class="row">
          <divc class="col-md-5"><a data-toggle="tooltip" data-title="Create Grade" data-placement="right"><button class="btn btn-primary btn-lg btn-rounded btn_pulse pull-right" data-toggle="modal" data-target="#grade" style="padding: 10px 10px;margin-top: 10px;"><i class="fa fa-plus"></i> Create Grade</button></a></div>
          <divc class="col-md-7"></div>
        </div>



         <div class="modal fade slide-up disable-scroll" id="grade" tabindex="-1" role="dialog" aria-hidden="false" style="overflow: auto;">
          <div class="modal-dialog ">
          <div class="modal-content-wrapper">
          <div class="modal-content">
          <div class="modal-header clearfix text-left">
          <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
          </button>
          <h5>Create Grade</h5>
          </div>
          <div class="modal-body">
            <form action="../admin/query/insert.php" method="POST">

            <div class="row">
            <div class="col-md-12">
            <div class="form-group form-group-default input-group">
            <div class="form-input-group" style="height: 62px;">
            <label>Grade Name</label>
            <input type="text" name="grade" class="form-control change_inputs" placeholder="XXXXXXXX"  required>
            </div>
            </div>
            </div>

            </div>

            <button type="submit" class="btn btn-success btn-block btn-lg" name="add_grade" value="<?php echo $level_id; ?>"><i class="pg-icon">add</i> Add</button>
            </form>
            </div>
            </div>
            </div>
            </div>

            </div>
            

    </div>
  </div>
</div>




<div class="col-md-12" style="margin-top: 0;">
  <h3 style="text-transform: capitalize;"><a href="dashboard.php" data-toggle="tooltip" data-title="Back"><i class="pg-icon" style="font-size: 22px;">chevron_left</i></a> Create Grade <small>(<?php echo $my_level_name; ?>)</small></h3>

<div class=" container-fluid   container-fixed-lg bg-white">
<div class="card card-transparent">
<div class="card-header ">
<div class="card-title">
</div>
<div class="pull-right">
<div class="col-xs-12">
<input type="text" id="search" class="form-control pull-right" placeholder="Search" data-toggle="tooltip" data-title="අවශ්‍ය දත්ත සෙවීම'">
</div>
</div>
<div class="clearfix"></div>
</div>

<div class="card-body table-responsive" style="height: 500px;overflow: auto;margin-bottom: 0;">
  <table class="table demo-table-search table-responsive-block text-left" id="tableWithSearch">
  <thead>
    <th>Level Name</th>
    <th>Grade Name</th>
    <th class="text-center">Action</th>

  </thead>
  <tbody id="myTable">

    <tr class="no-data alert alert-danger" style="margin-top: 20px;display: none;">
      <td colspan="6" class="text-danger"><span class="fa fa-warning"></span> Not Found Data</td>
    </tr>
      
   <?php 

          $sql001 = mysqli_query($conn,"SELECT * FROM `grade` WHERE `LEVEL_ID` = '$level_id' ORDER BY `GRADE_ID` DESC");
          $ch = mysqli_num_rows($sql001);
          if($ch > 0)
          {
                while($row = mysqli_fetch_assoc($sql001))
                {

                      
                    $grade_id = $row['GRADE_ID'];
                    $grade_name = $row['GRADE_NAME'];
                    $level_id = $row['LEVEL_ID'];

                    $sql008 = mysqli_query($conn,"SELECT * FROM `level` WHERE `LEVEL_ID` = '$level_id'");
                    while($row008=mysqli_fetch_assoc($sql008))
                    {
                        $level_name = $row008['LEVEL_NAME'];

                    }
                    
                    echo '<tr>';

                    echo '
                          <td class="v-align-middle bold">'.$level_name.'</td>
                          <td class="v-align-middle bold">'.$grade_name.'</td>
                          <td class="v-align-middle text-center">

                              '.$btn.'
                          ';


                          echo '

                                      <a data-toggle="tooltip" data-title="Edit Grade"><button type="button" class="btn btn-complete  btn-xs btn-rounded" data-target="#edit_grade'.$grade_id.'" data-toggle="modal" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;margin-right:10px;margin-left:10px;"><i class="pg-icon">edit</i></button></a>';
                                        
                                        ?>
                                          <a href="../admin/query/delete.php?delete_grade=<?php echo $grade_id; ?>&&level_id=<?php echo $level_id; ?>" onclick="return confirm('Are you sure Delete?')" class="btn btn-danger btn-rounded btn-xs" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;" data-toggle="tooltip" data-title="Delete Class"><i class="pg-icon">trash_alt</i></a>

                                          <div class="modal fade slide-up disable-scroll" id="edit_grade<?php echo $grade_id; ?>" tabindex="-1" role="dialog" aria-hidden="false" style="overflow: auto;">
                                            <div class="modal-dialog ">
                                            <div class="modal-content-wrapper">
                                            <div class="modal-content">
                                            <div class="modal-header clearfix text-left">
                                            <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
                                            </button>
                                            <h5>Edit Grade</h5>
                                            </div>
                                            <div class="modal-body">
                                              <form action="../admin/query/update.php" method="POST">

                                              <div class="row text-left">
                                              <div class="col-md-12">
                                              <div class="form-group form-group-default input-group">
                                              <div class="form-input-group" style="height: 62px;">
                                              <label>Grade Name</label>
                                              <input type="text" name="grade" class="form-control change_inputs" placeholder="XXXXXXXX" required value="<?php echo $grade_name; ?>">

                                              </div>
                                              </div>
                                              </div>
                                              <input type="hidden" name="grade_id" value="<?php echo $grade_id; ?>">
                                              </div>

                                              <button type="submit" class="btn btn-success btn-block btn-lg" name="edit_grade" value="<?php echo $level_id; ?>"><i class="pg-icon">edit</i> Update</button>
                                              </form>
                                              </div>
                                              </div>
                                              </div>
                                              </div>

                                              </div>

                                          <?php


                          
                        }
                      }else
                      if( $ch == '0')
                      {
                          echo '<tr><td colspan="6" class="text-danger text-center"><span class="fa fa-warning text-danger"></span> Empty Data!</td></tr>';
                      }
 ?>
    
    </tr>
  </tbody>
  </table>
</div>
</div>

</div>

<script type="text/javascript">
  
    $(document).ready(function(){  
     $('#level_clz').change(function(){

       var level_clz = $(this).val();
       var teach_id = $('#teach_id').val();

       $.ajax({
        url:'../teacher/query/check.php',
        method:"POST",
        data:{create_clz:level_clz,teach_id:teach_id},
        success:function(data)
        {
          //alert(data)
            $('#subject').html(data);
          
        }
       })
     

    });

     
   });

  </script>

<script type="text/javascript">
	setInterval(function() {
    $('blink').fadeIn(1300).fadeOut(1500);
}, 1000);
</script>

<script>
$(document).ready(function(){
  $("#search").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $(".myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

<script type="text/javascript">
  $(document).ready(function () {

    (function ($) {

        $('#search').keyup(function () {
            var rex = new RegExp($(this).val(), 'i');
            $('#myTable tr').hide();
            $('#myTable tr').filter(function () {
                return rex.test($(this).text());
            }).show();
            $('.no-data').hide();
            if($('#myTable tr:visible').length == 0)
            {
                $('.no-data').show();
            }

        })

    }(jQuery));

});
</script>

<script>
$(document).ready(function(){

  $("#up_rec").on("change", function() {
      $('.show_image').hide();
  });
});
</script>
<script type="text/javascript">
  setInterval(function() {
    $('.blink').fadeIn(1300).fadeOut(1500);
}, 1000);
</script>

<script type="text/javascript">
  $("#show_hide_password a").on('click', function(event) {
        event.preventDefault();
        if($('#show_hide_password input').attr("type") == "text"){
            $('#show_hide_password input').attr('type', 'password');
            $('#show_hide_password i').addClass( "fa-eye-slash" );
            $('#show_hide_password i').removeClass( "fa-eye" );
        }else if($('#show_hide_password input').attr("type") == "password"){
            $('#show_hide_password input').attr('type', 'text');
            $('#show_hide_password i').removeClass( "fa-eye-slash" );
            $('#show_hide_password i').addClass( "fa-eye" );
        }
    });
</script>

<?php  include('footer/footer.php'); ?>


