-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 13, 2021 at 01:34 PM
-- Server version: 10.4.18-MariaDB-cll-lve
-- PHP Version: 7.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `uzjqdyrj_sipsatha`
--

-- --------------------------------------------------------

--
-- Table structure for table `add_subject`
--

CREATE TABLE `add_subject` (
  `ADD_SUB_ID` int(11) NOT NULL,
  `ADD_DATE` varchar(45) DEFAULT NULL,
  `T_S_ID` int(11) NOT NULL,
  `STU_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `admin_login`
--

CREATE TABLE `admin_login` (
  `ADMIN_ID` int(11) NOT NULL,
  `ADMIN_NAME` varchar(45) DEFAULT NULL,
  `USERNAME` varchar(45) DEFAULT NULL,
  `PASSWORD` varchar(45) DEFAULT NULL,
  `TYPE` varchar(45) DEFAULT NULL,
  `STATUS` varchar(45) DEFAULT NULL,
  `PICTURE` text DEFAULT NULL,
  `GENDER` varchar(45) DEFAULT NULL,
  `REG_DATE` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin_login`
--

INSERT INTO `admin_login` (`ADMIN_ID`, `ADMIN_NAME`, `USERNAME`, `PASSWORD`, `TYPE`, `STATUS`, `PICTURE`, `GENDER`, `REG_DATE`) VALUES
(1, 'Pramodya Deshan', 'super', '34f33e7107792f5c6077aac965fb2191', 'S_ADMIN', 'Active', '0', 'Male', '2021-01-19 4:00 PM'),
(3, 'Test Staff', 'staff', '202cb962ac59075b964b07152d234b70', 'STAFF', 'Active', '0', 'Male', '2021-05-06 04:23:56 PM');

-- --------------------------------------------------------

--
-- Table structure for table `alert`
--

CREATE TABLE `alert` (
  `ALERT_ID` int(11) NOT NULL,
  `MESSAGE` text DEFAULT NULL,
  `AUDIENCE` varchar(45) DEFAULT NULL,
  `ADDED_DATE` varchar(45) DEFAULT NULL,
  `TITLE` text DEFAULT NULL,
  `PUBLISH` varchar(45) DEFAULT NULL,
  `PUB_DATE` varchar(45) DEFAULT NULL,
  `TIMES` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `attendence`
--

CREATE TABLE `attendence` (
  `ATT_ID` int(11) NOT NULL,
  `YEAR` varchar(45) DEFAULT NULL,
  `MONTH` varchar(45) DEFAULT NULL,
  `Y_M` varchar(45) DEFAULT NULL,
  `ATT_DATE` varchar(45) DEFAULT NULL,
  `ATT_D_T` varchar(45) DEFAULT NULL,
  `PAYMENT` varchar(45) DEFAULT NULL COMMENT 'CHECK PAY OR NOT PAID?',
  `STU_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `audience`
--

CREATE TABLE `audience` (
  `AUD_ID` int(11) NOT NULL,
  `LEVEL_ID` varchar(45) DEFAULT NULL,
  `SUB_ID` varchar(45) DEFAULT NULL,
  `CLASS_ID` varchar(45) DEFAULT NULL,
  `ALERT_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

CREATE TABLE `classes` (
  `CLASS_ID` int(11) NOT NULL,
  `CLASS_NAME` varchar(45) DEFAULT NULL,
  `FEES` varchar(45) DEFAULT NULL,
  `START_TIME` varchar(45) DEFAULT NULL,
  `END_TIME` varchar(45) DEFAULT NULL,
  `LINK` text DEFAULT NULL,
  `TEACH_ID` int(11) NOT NULL,
  `SUB_ID` int(11) NOT NULL,
  `ZOOM_LINK` text DEFAULT NULL,
  `MS_LINK` text DEFAULT NULL,
  `JITSI_LINK` text DEFAULT NULL,
  `GOOGLE_LINK` text DEFAULT NULL,
  `WHATSAPP_LINK` text DEFAULT NULL,
  `DAY` text DEFAULT NULL,
  `FREE` varchar(45) DEFAULT NULL,
  `START_DATE` varchar(45) DEFAULT NULL,
  `END_DATE` varchar(45) DEFAULT NULL,
  `LIMIT_STUDENT` int(11) NOT NULL,
  `LAST_UPDATED` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `classes`
--

INSERT INTO `classes` (`CLASS_ID`, `CLASS_NAME`, `FEES`, `START_TIME`, `END_TIME`, `LINK`, `TEACH_ID`, `SUB_ID`, `ZOOM_LINK`, `MS_LINK`, `JITSI_LINK`, `GOOGLE_LINK`, `WHATSAPP_LINK`, `DAY`, `FREE`, `START_DATE`, `END_DATE`, `LIMIT_STUDENT`, `LAST_UPDATED`) VALUES
(29, 'Grade 06 Mathematics (English Medium)', '1000', '14:00', '16:00', '0', 10, 29, '0', '0', '0', '0', '0', 'Monday', '0', '0', '0', 200, '2021-05-13'),
(30, 'Grade 07 Mathematics (English Medium)', '1000', '14:00', '16:00', '0', 10, 29, '0', '0', '0', '0', '0', 'Wednesday', '0', '0', '0', 200, '2021-05-13'),
(31, 'Grade 06 Mathematics (Sinhala Medium)', '600', '16:00', '18:00', '0', 10, 29, '0', '0', '0', '0', '0', 'Monday', '0', '0', '0', 200, '2021-05-13'),
(32, 'Grade 07 Mathematics (Sinhala Medium)', '700', '16:00', '18:00', '0', 10, 29, '0', '0', '0', '0', '0', 'Wednesday', '0', '0', '0', 200, '2021-05-13'),
(33, 'Grade 06', '0', '14:00', '16:00', '0', 13, 37, '0', '0', '0', '0', '0', 'Wednesday', '0', '0', '0', 200, '2021-05-13'),
(34, 'Grade 07', '0', '16:00', '18:00', '0', 13, 37, '0', '0', '0', '0', '0', 'Wednesday', '0', '0', '0', 200, '2021-05-13'),
(35, 'Grade 08', '0', '14:00', '16:00', '0', 13, 37, '0', '0', '0', '0', '0', 'Thursday', '0', '0', '0', 200, '2021-05-13'),
(36, 'Grade 09', '0', '16:00', '18:00', '0', 13, 37, '0', '0', '0', '0', '0', 'Thursday', '0', '0', '0', 200, '2021-05-13'),
(37, 'Grade 10', '0', '14:00', '16:00', '0', 13, 37, '0', '0', '0', '0', '0', 'Friday', '0', '0', '0', 200, '2021-05-13'),
(38, 'Grade 11', '0', '14:00', '16:00', '0', 13, 37, '0', '0', '0', '0', '0', 'Tuesday', '0', '0', '0', 200, '2021-05-13'),
(39, 'Grade 10 / 11 Revision', '0', '18:00', '20:00', '0', 13, 37, '0', '0', '0', '0', '0', 'Wednesday', '0', '0', '0', 200, '2021-05-13');

-- --------------------------------------------------------

--
-- Table structure for table `custom_student`
--

CREATE TABLE `custom_student` (
  `SHOW_STU_ID` int(11) NOT NULL,
  `STATUS` varchar(45) DEFAULT NULL,
  `ALERT_ID` int(11) NOT NULL,
  `STU_ID` int(11) DEFAULT NULL,
  `SHOW_TIMES` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `essay_master_question`
--

CREATE TABLE `essay_master_question` (
  `ESSAY_TRANS_ID` int(11) NOT NULL,
  `ADD_DATE` varchar(45) DEFAULT NULL,
  `ADD_TIME` varchar(45) DEFAULT NULL,
  `PAPER_ID` int(11) NOT NULL,
  `QUESTION_NO` varchar(45) DEFAULT NULL,
  `MASTER_MARK` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `essay_sub_question`
--

CREATE TABLE `essay_sub_question` (
  `ESSAY_SUB_QUE_ID` int(11) NOT NULL,
  `MASTER_QUESTION` varchar(45) DEFAULT NULL,
  `SUB_QUESTION_NO` varchar(45) DEFAULT NULL,
  `SUB_MARK` varchar(45) DEFAULT NULL,
  `ESSAY_TRANS_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exam_result`
--

CREATE TABLE `exam_result` (
  `EXAM_RESULT_ID` int(11) NOT NULL,
  `ADD_DATE` varchar(45) DEFAULT NULL,
  `ADD_TIME` varchar(45) DEFAULT NULL,
  `START_DATE` varchar(45) DEFAULT NULL,
  `MCQ_MARK` double NOT NULL,
  `SE_MARK` double NOT NULL,
  `TOTAL_MARK` double NOT NULL COMMENT 'Total Marks =  Total Mcq Marks / 2 +  Total se marks / 20',
  `EXAM_KEY` varchar(45) DEFAULT NULL,
  `CLASS_ID` int(11) NOT NULL,
  `PAPER_ID` int(11) DEFAULT NULL,
  `STU_ID` int(11) DEFAULT NULL,
  `MCQ_PAPER_NAME` varchar(200) DEFAULT NULL,
  `SE_PAPER_NAME` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `expenditure`
--

CREATE TABLE `expenditure` (
  `EXP_ID` int(11) NOT NULL,
  `PAYMENT` varchar(45) DEFAULT NULL,
  `ADD_DATE` varchar(45) DEFAULT NULL,
  `ADD_D_T` varchar(45) DEFAULT NULL,
  `REASON_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `income`
--

CREATE TABLE `income` (
  `INCOME_ID` int(11) NOT NULL,
  `PAYMENT` varchar(45) DEFAULT NULL,
  `ADD_DATE` varchar(45) DEFAULT NULL,
  `ADD_D_T` varchar(45) DEFAULT NULL,
  `PAY_ID` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `institute`
--

CREATE TABLE `institute` (
  `INS_ID` int(11) NOT NULL,
  `INS_NAME` varchar(45) DEFAULT NULL,
  `INS_TP` varchar(45) DEFAULT NULL,
  `INS_ADDRESS` varchar(45) DEFAULT NULL,
  `INS_MOBILE` varchar(45) DEFAULT NULL,
  `PICTURE` varchar(45) DEFAULT NULL,
  `FREE` varchar(45) DEFAULT NULL,
  `REG_CODE` varchar(45) DEFAULT NULL,
  `SMS_ACTIVE` varchar(45) DEFAULT NULL COMMENT 'with sender name',
  `SINGLE_SMS` varchar(45) DEFAULT NULL COMMENT 'Show without sender name, only a telephone no',
  `WEB` text DEFAULT NULL,
  `REG_WITH_ACTIVE` varchar(45) DEFAULT NULL,
  `EXAM_TAB` int(11) DEFAULT NULL,
  `BG_COLOR` text DEFAULT NULL,
  `TIMETABLE` text DEFAULT NULL,
  `SUSPEND` int(11) NOT NULL,
  `ENABLE_REGISTER_BTN` int(11) NOT NULL COMMENT '1 = Hidden , 0 = Show',
  `CLZ_LIMIT` int(11) NOT NULL COMMENT 'YES = 1 , NO = 0',
  `NO_LIMIT` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `institute`
--

INSERT INTO `institute` (`INS_ID`, `INS_NAME`, `INS_TP`, `INS_ADDRESS`, `INS_MOBILE`, `PICTURE`, `FREE`, `REG_CODE`, `SMS_ACTIVE`, `SINGLE_SMS`, `WEB`, `REG_WITH_ACTIVE`, `EXAM_TAB`, `BG_COLOR`, `TIMETABLE`, `SUSPEND`, `ENABLE_REGISTER_BTN`, `CLZ_LIMIT`, `NO_LIMIT`) VALUES
(1, 'Sipsatha Online Educational Institute', '0777650121', 'Matara', '0', '1698941616962281.png', 'No', 'SP', '1', '1', 'https://www.sipsatha.lk', '0', 1, '#4b7bec', '0', 0, 0, 0, '200');

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE `level` (
  `LEVEL_ID` int(11) NOT NULL,
  `LEVEL_NAME` varchar(100) DEFAULT NULL,
  `SHORT_NAME` varchar(45) DEFAULT NULL,
  `PICTURE` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`LEVEL_ID`, `LEVEL_NAME`, `SHORT_NAME`, `PICTURE`) VALUES
(17, 'Grade 1 - 5', 'Grade 1 - 5', '0'),
(18, 'Grade 6 - 11', 'Grade 6 - 11', '0'),
(19, 'Advance Level', 'Advance Level', '0'),
(20, 'Professional Courses', 'Professional Courses', '0');

-- --------------------------------------------------------

--
-- Table structure for table `link_clicked`
--

CREATE TABLE `link_clicked` (
  `LINK_C_ID` int(11) NOT NULL,
  `STU_ID` int(11) NOT NULL,
  `TEACH_ID` int(11) NOT NULL,
  `CLASS_ID` int(11) NOT NULL,
  `ZOOM` varchar(45) DEFAULT NULL,
  `ZOOM_CLICKED` varchar(45) DEFAULT NULL,
  `CLICK_DATE` varchar(100) DEFAULT NULL,
  `GOOGLE` varchar(45) DEFAULT NULL,
  `GOOGLE_CLICKED` varchar(45) DEFAULT NULL,
  `JITSI` varchar(45) DEFAULT NULL,
  `JITSI_CLICKED` varchar(45) DEFAULT NULL,
  `MS` varchar(45) DEFAULT NULL,
  `MS_CLICKED` varchar(45) DEFAULT NULL,
  `WHATSAPP` varchar(45) DEFAULT NULL,
  `WHATSAPP_CLICKED` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `login_report`
--

CREATE TABLE `login_report` (
  `LOG_R_ID` int(11) NOT NULL,
  `LOGIN_DATE` varchar(45) DEFAULT NULL,
  `LOGIN_TIME` varchar(45) DEFAULT NULL,
  `LOGOUT_TIME` varchar(45) DEFAULT NULL,
  `STU_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `login_report`
--

INSERT INTO `login_report` (`LOG_R_ID`, `LOGIN_DATE`, `LOGIN_TIME`, `LOGOUT_TIME`, `STU_ID`) VALUES
(8080, '2021-05-05', '2021-05-05 02:15:14 AM', '0', 263),
(8081, '2021-05-05', '2021-05-05 02:27:52 AM', '0', 263),
(8082, '2021-05-05', '2021-05-05 12:45:06 PM', '2021-05-05 12:45:11 PM', 263),
(8083, '2021-05-05', '2021-05-05 12:48:47 PM', '0', 263),
(8084, '2021-05-05', '2021-05-05 02:17:24 PM', '0', 263),
(8085, '2021-05-05', '2021-05-05 02:33:32 PM', '2021-05-05 02:33:39 PM', 263),
(8086, '2021-05-08', '2021-05-08 08:32:02 PM', '2021-05-08 08:32:30 PM', 263),
(8087, '2021-05-10', '2021-05-10 03:07:22 PM', '0', 263),
(8088, '2021-05-10', '2021-05-10 03:07:23 PM', '2021-05-10 03:07:41 PM', 263),
(8089, '2021-05-10', '2021-05-10 04:48:44 PM', '2021-05-10 04:48:52 PM', 263),
(8090, '2021-05-11', '2021-05-11 02:26:40 PM', '0', 265),
(8091, '2021-05-12', '2021-05-12 08:28:12 PM', '0', 267),
(8092, '2021-05-13', '2021-05-13 09:43:18 AM', '2021-05-13 09:55:08 AM', 267),
(8093, '2021-05-13', '2021-05-13 10:00:45 AM', '2021-05-13 10:33:44 AM', 267),
(8094, '2021-05-13', '2021-05-13 10:33:49 AM', '2021-05-13 10:36:01 AM', 267),
(8095, '2021-05-13', '2021-05-13 10:36:09 AM', '0', 267),
(8096, '2021-05-13', '2021-05-13 03:42:36 PM', '2021-05-13 03:46:10 PM', 276),
(8097, '2021-05-13', '2021-05-13 04:56:49 PM', '0', 276);

-- --------------------------------------------------------

--
-- Table structure for table `master_sms`
--

CREATE TABLE `master_sms` (
  `MASTER_SMS_ID` int(11) NOT NULL,
  `SENT_DATE` varchar(45) DEFAULT NULL,
  `SENT_TIME` varchar(45) DEFAULT NULL,
  `SMS_COUNT` varchar(45) DEFAULT NULL,
  `CURRENT_SMS` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mcq_trans`
--

CREATE TABLE `mcq_trans` (
  `MCQ_TRAN_ID` int(11) NOT NULL,
  `QUESTION` varchar(45) DEFAULT NULL,
  `CORRECT_ANSWER` varchar(45) DEFAULT NULL,
  `MARKS_ANSWER` varchar(45) DEFAULT NULL,
  `PAPER_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `multi_sms`
--

CREATE TABLE `multi_sms` (
  `MULTI_SMS_ID` int(11) NOT NULL,
  `TP` varchar(45) DEFAULT NULL,
  `SMS_BODY` text DEFAULT NULL,
  `STUDENT_ID` varchar(45) DEFAULT NULL,
  `MASTER_SMS_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `paper_image`
--

CREATE TABLE `paper_image` (
  `PAPER_IMG_ID` int(11) NOT NULL,
  `ADD_TIME` varchar(45) DEFAULT NULL,
  `ADD_DATE` varchar(45) DEFAULT NULL,
  `IMAGE_NAME` text DEFAULT NULL,
  `STU_ESSAY_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `paper_master`
--

CREATE TABLE `paper_master` (
  `PAPER_ID` int(11) NOT NULL,
  `PAPER_NAME` varchar(255) DEFAULT NULL,
  `EXAM_KEY` varchar(45) DEFAULT NULL,
  `UPLOAD_DATE` varchar(45) DEFAULT NULL,
  `UPLOAD_TIME` varchar(45) DEFAULT NULL,
  `NO_OF_QUESTIONS` varchar(45) DEFAULT NULL,
  `NO_QUESTION_ANSWER` varchar(45) DEFAULT NULL,
  `ANSWER_MARKS` varchar(45) DEFAULT NULL,
  `PDF_NAME` text DEFAULT NULL,
  `UPLOAD_BY` varchar(45) DEFAULT NULL COMMENT 'Teacher ID',
  `TIME_DURATION_HOUR` varchar(45) DEFAULT NULL,
  `TIME_DURATION_MIN` varchar(45) DEFAULT NULL,
  `FINISH_DATE` varchar(45) DEFAULT NULL,
  `FINISH_TIME` varchar(45) DEFAULT NULL,
  `STATUS` varchar(45) DEFAULT NULL,
  `CLASS_ID` int(11) NOT NULL,
  `COMMENT` text DEFAULT NULL,
  `START_DATE` varchar(45) DEFAULT NULL,
  `START_TIME` varchar(45) DEFAULT NULL,
  `FINISH_D_T` varchar(45) DEFAULT NULL,
  `START_D_T` varchar(45) DEFAULT NULL,
  `LAST_ATTENT_TIME` varchar(45) DEFAULT NULL COMMENT 'Calculate after insert it\n',
  `MAX_HOUR` int(11) NOT NULL,
  `MAX_MINUTE` int(11) NOT NULL,
  `PAPER_TYPE` varchar(45) DEFAULT NULL,
  `TEACH_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `payment_data`
--

CREATE TABLE `payment_data` (
  `PAY_ID` int(11) NOT NULL,
  `FEES` varchar(45) DEFAULT NULL,
  `YEAR` varchar(45) DEFAULT NULL,
  `MONTH` varchar(45) DEFAULT NULL,
  `Y_M` varchar(45) DEFAULT NULL,
  `UPLOAD_STATUS` varchar(45) DEFAULT NULL,
  `PAY_TIME` varchar(45) DEFAULT NULL,
  `ADMIN_SUBMIT` varchar(45) DEFAULT NULL,
  `FILE_NAME` text DEFAULT NULL,
  `TRA_ID` int(11) NOT NULL,
  `CLASS_ID` varchar(45) DEFAULT NULL,
  `LAST_UPLOAD_FILE` text DEFAULT NULL,
  `STU_ID` varchar(45) DEFAULT NULL,
  `PAY_METHOD` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `profile_settings`
--

CREATE TABLE `profile_settings` (
  `PRO_S_ID` int(11) NOT NULL,
  `TOUR_ENABLE` varchar(45) DEFAULT NULL COMMENT 'FIRST TIME ONLY GO TO TOUR',
  `WELCOME_MESSAGE` varchar(45) DEFAULT NULL COMMENT 'FIRST TIME ONLY SHOW WELCOME MESSAGE',
  `COLOR` varchar(45) DEFAULT NULL COMMENT 'PROFILE_COLOR',
  `DARK_MODE` varchar(45) DEFAULT NULL COMMENT 'DARK MODE ENABLE!',
  `DM_START_TIME` varchar(45) DEFAULT NULL COMMENT 'DARK MODE START TIME',
  `DM_END_TIME` varchar(45) DEFAULT NULL COMMENT 'DARK MODE END TIME!\n',
  `STU_ID` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `qualification`
--

CREATE TABLE `qualification` (
  `QUALIFI_ID` int(11) NOT NULL,
  `QUALIFICATION` varchar(100) DEFAULT NULL,
  `YEAR` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `reason`
--

CREATE TABLE `reason` (
  `REASON_ID` int(11) NOT NULL,
  `REASON` text DEFAULT NULL,
  `ADD_DATE` varchar(45) DEFAULT NULL,
  `ADD_D_T` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `reg_count`
--

CREATE TABLE `reg_count` (
  `REG_COUNT_ID` int(11) NOT NULL,
  `CURRENT_REG_ID` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `reg_count`
--

INSERT INTO `reg_count` (`REG_COUNT_ID`, `CURRENT_REG_ID`) VALUES
(1, '1');

-- --------------------------------------------------------

--
-- Table structure for table `seminar`
--

CREATE TABLE `seminar` (
  `SEMI_ID` int(11) NOT NULL,
  `NAME` varchar(45) DEFAULT NULL,
  `TELEPHONE` varchar(45) DEFAULT NULL,
  `ACCESS_CODE` varchar(100) DEFAULT NULL,
  `PASSWORD` varchar(100) DEFAULT NULL,
  `ADD_DATE` varchar(45) DEFAULT NULL,
  `ADD_TIME` varchar(45) DEFAULT NULL,
  `CLASS_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `single_sms`
--

CREATE TABLE `single_sms` (
  `SINGLE_SMS_ID` int(11) NOT NULL,
  `TP` varchar(45) DEFAULT NULL,
  `SEND_TIME` varchar(45) DEFAULT NULL,
  `SMS_BODY` text DEFAULT NULL,
  `STUDENT_NAME` varchar(45) DEFAULT NULL,
  `SEND_DATE` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `single_sms`
--

INSERT INTO `single_sms` (`SINGLE_SMS_ID`, `TP`, `SEND_TIME`, `SMS_BODY`, `STUDENT_NAME`, `SEND_DATE`) VALUES
(356, '0702515852', '2021-05-13 02:55:10 PM', 'Your Registration has been Successfully! You can login now!\nhttps://www.sipsatha.lk/app/teacher/login/index.php\n\n- SIPSATHA.LK - ', 'Mr. Udagama Liyanage Kusal Akalanka', '2021-05-13'),
(357, '0777293473', '2021-05-13 02:55:17 PM', 'Your Registration has been Successfully! You can login now!\nhttps://www.sipsatha.lk/app/teacher/login/index.php\n\n- SIPSATHA.LK - ', 'Mr. DUMINDU CHAMARA DHARMADASA', '2021-05-13'),
(358, '0775078645', '2021-05-13 02:55:24 PM', 'Your Registration has been Successfully! You can login now!\nhttps://www.sipsatha.lk/app/teacher/login/index.php\n\n- SIPSATHA.LK - ', 'Ms. Chamali Dilrangi', '2021-05-13'),
(361, '0764579113', '2021-05-13 04:08:19 PM', 'Your Registration has been Successfully! You can login now!\nhttps://www.sipsatha.lk/app/teacher/login/index.php\n\n- SIPSATHA.LK - ', 'Ms. Nuwani Tharangika', '2021-05-13'),
(362, '0777310295', '2021-05-13 04:47:02 PM', 'Your Registration has been Successfully! You can login now!\nhttps://www.sipsatha.lk/app/teacher/login/index.php\n\n- SIPSATHA.LK - ', 'Mr. Sagara Gunawardana  Gunawardana ', '2021-05-13');

-- --------------------------------------------------------

--
-- Table structure for table `single_sms_gateway`
--

CREATE TABLE `single_sms_gateway` (
  `SINGLE_SMS_ID` int(11) NOT NULL,
  `USERNAME` varchar(100) DEFAULT NULL,
  `API_KEY` varchar(100) DEFAULT NULL,
  `GATEWAY_TYPE` varchar(100) DEFAULT NULL,
  `COUNTRY_CODE` varchar(100) DEFAULT NULL,
  `ADD_D_T` varchar(100) DEFAULT NULL,
  `SMS_CHARGE` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `single_sms_gateway`
--

INSERT INTO `single_sms_gateway` (`SINGLE_SMS_ID`, `USERNAME`, `API_KEY`, `GATEWAY_TYPE`, `COUNTRY_CODE`, `ADD_D_T`, `SMS_CHARGE`) VALUES
(1, 'ibsglobe', '390b6c6dd19956004930', '1', '94', '20201-01-19 4:00:00 PM', '1');

-- --------------------------------------------------------

--
-- Table structure for table `sms_counter`
--

CREATE TABLE `sms_counter` (
  `SUB_SMS_C_ID` int(11) NOT NULL,
  `ADD_DATE` varchar(45) DEFAULT NULL,
  `ADD_D_T` varchar(45) DEFAULT NULL,
  `PAYMENT` varchar(45) DEFAULT NULL,
  `ADD_SMS` varchar(45) DEFAULT NULL,
  `REMAINED_SMS` varchar(45) DEFAULT NULL,
  `CURRENT_SMS` varchar(45) DEFAULT NULL,
  `CURRENT_AMOUNT` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sms_counter`
--

INSERT INTO `sms_counter` (`SUB_SMS_C_ID`, `ADD_DATE`, `ADD_D_T`, `PAYMENT`, `ADD_SMS`, `REMAINED_SMS`, `CURRENT_SMS`, `CURRENT_AMOUNT`) VALUES
(1, '2021-01-19', '2021-01-19 04:00:00 PM', '1000', '1000', '1000', '995', '995');

-- --------------------------------------------------------

--
-- Table structure for table `sms_gateway`
--

CREATE TABLE `sms_gateway` (
  `SMS_G_ID` int(11) NOT NULL,
  `USERNAME` text DEFAULT NULL,
  `API_KEY` text DEFAULT NULL,
  `GATEWAY_TYPE` varchar(45) DEFAULT NULL,
  `SENDER_ID` text DEFAULT NULL,
  `SENDER_TYPE` text DEFAULT NULL,
  `COUNTRY_CODE` text DEFAULT NULL,
  `ADD_D_T` varchar(45) DEFAULT NULL,
  `INS_ID` int(11) NOT NULL,
  `SMS_CHARGE` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sms_gateway`
--

INSERT INTO `sms_gateway` (`SMS_G_ID`, `USERNAME`, `API_KEY`, `GATEWAY_TYPE`, `SENDER_ID`, `SENDER_TYPE`, `COUNTRY_CODE`, `ADD_D_T`, `INS_ID`, `SMS_CHARGE`) VALUES
(1, 'ibsglobe', '390b6c6dd19956004930', '2', '0', '0', '94', '2021-01-19 4:00:00 PM', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `staff_login`
--

CREATE TABLE `staff_login` (
  `STAFF_ID` int(11) NOT NULL,
  `NAME` varchar(45) DEFAULT NULL,
  `TYPE` varchar(45) DEFAULT NULL,
  `STATUS` varchar(45) DEFAULT NULL,
  `USERNAME` varchar(45) DEFAULT NULL,
  `PASSWORD` varchar(45) DEFAULT NULL,
  `PICTURE` text DEFAULT NULL,
  `GENDER` varchar(45) DEFAULT NULL,
  `REG_DATE` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `student_access_count`
--

CREATE TABLE `student_access_count` (
  `ST_COUNT_ID` int(11) NOT NULL,
  `COUNT_DATE` varchar(45) DEFAULT NULL,
  `AMOUNT` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `student_access_count`
--

INSERT INTO `student_access_count` (`ST_COUNT_ID`, `COUNT_DATE`, `AMOUNT`) VALUES
(6, '2021-05-11', 0),
(7, '2021-05-12', 0),
(8, '2021-05-13', 0);

-- --------------------------------------------------------

--
-- Table structure for table `student_details`
--

CREATE TABLE `student_details` (
  `STU_ID` int(11) NOT NULL,
  `F_NAME` varchar(100) DEFAULT NULL,
  `L_NAME` varchar(100) DEFAULT NULL,
  `DOB` varchar(45) DEFAULT NULL,
  `EMAIL` varchar(100) DEFAULT NULL,
  `ADDRESS` text DEFAULT NULL,
  `PICTURE` text DEFAULT NULL,
  `TP` varchar(20) DEFAULT NULL,
  `NEW` int(2) DEFAULT NULL,
  `GENDER` varchar(45) DEFAULT NULL,
  `PAYMENT_TAB` int(11) NOT NULL,
  `EXAM_TAB` int(11) NOT NULL,
  `ALERT` int(10) DEFAULT NULL,
  `SCHOOL` text DEFAULT NULL,
  `SUBJECT` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `student_details`
--

INSERT INTO `student_details` (`STU_ID`, `F_NAME`, `L_NAME`, `DOB`, `EMAIL`, `ADDRESS`, `PICTURE`, `TP`, `NEW`, `GENDER`, `PAYMENT_TAB`, `EXAM_TAB`, `ALERT`, `SCHOOL`, `SUBJECT`) VALUES
(276, 'Test', 'Student', 'N/A', '', 'matara', '0', '0779707981', 1, 'Male', 0, 0, 0, '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `student_essay_master`
--

CREATE TABLE `student_essay_master` (
  `STU_ESSAY_ID` int(11) NOT NULL,
  `ADD_DATE` varchar(45) DEFAULT NULL,
  `ADD_TIME` varchar(45) DEFAULT NULL,
  `TOTAL_MARKS` varchar(45) DEFAULT NULL,
  `PAPER_FILE` text DEFAULT NULL,
  `PAPER_ID` varchar(45) DEFAULT NULL,
  `COMMENT` text DEFAULT NULL COMMENT 'Paper Special Marks and special notes',
  `STU_ID` int(11) NOT NULL,
  `STATUS` int(11) NOT NULL,
  `TOTAL_QUESTION_MARKS` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `student_essay_question_master`
--

CREATE TABLE `student_essay_question_master` (
  `STU_ESSAY_MASTER_ID` int(11) NOT NULL,
  `MASTER_QUESTION_NO` varchar(45) DEFAULT NULL,
  `MASTER_Q_MARK` varchar(45) DEFAULT NULL,
  `PAPER_ID` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `student_essay_question_trans`
--

CREATE TABLE `student_essay_question_trans` (
  `STU_ESSAY_TRANS_ID` int(11) NOT NULL,
  `SUB_QUESTION` varchar(45) DEFAULT NULL,
  `SUB_MARK` varchar(45) DEFAULT NULL,
  `STU_ESSAY_MASTER_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `student_mcq_main`
--

CREATE TABLE `student_mcq_main` (
  `STU_MAIN_ID` int(11) NOT NULL,
  `ATTEND_TIME` varchar(45) DEFAULT NULL,
  `START_TIME` varchar(45) DEFAULT NULL,
  `PAPER_ID` int(11) NOT NULL,
  `STU_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `student_mcq_master`
--

CREATE TABLE `student_mcq_master` (
  `STU_MCQ_MASTER_ID` int(11) NOT NULL,
  `SUBMIT_DATE` varchar(45) DEFAULT NULL,
  `SUBMIT_DATE_TIME` varchar(45) DEFAULT NULL,
  `CORRECT_ANSWERS` varchar(45) DEFAULT NULL,
  `WRONG_ANSWER` varchar(45) DEFAULT NULL,
  `TOTAL_QUESTION` varchar(45) DEFAULT NULL,
  `TOTAL_MARKS` varchar(45) DEFAULT NULL,
  `QUESTION_MARK` varchar(45) DEFAULT NULL,
  `SPEND_TIME_HOUR` varchar(45) DEFAULT NULL,
  `SPEND_TIME_MIN` varchar(45) DEFAULT NULL,
  `STU_ID` int(11) NOT NULL,
  `ATTEND_TIME` varchar(45) DEFAULT NULL,
  `PAPER_ID` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `student_mcq_transaction`
--

CREATE TABLE `student_mcq_transaction` (
  `STU_MCQ_TRANS_ID` int(11) NOT NULL,
  `ANSWER_TIME` varchar(45) DEFAULT NULL,
  `ANSWER` varchar(45) DEFAULT NULL,
  `MARKS` varchar(45) DEFAULT NULL,
  `STU_ID` int(11) NOT NULL,
  `QUESTION` varchar(45) DEFAULT NULL,
  `CORRECT_ANSWER` varchar(45) DEFAULT NULL,
  `STU_MAIN_ID` int(11) NOT NULL,
  `PAPER_ID` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `stu_login`
--

CREATE TABLE `stu_login` (
  `STU_L_ID` int(11) NOT NULL,
  `REGISTER_ID` varchar(45) DEFAULT NULL,
  `PASSWORD` varchar(45) DEFAULT NULL,
  `STATUS` varchar(45) DEFAULT NULL,
  `STU_ID` int(11) NOT NULL,
  `LOGIN_TIME` varchar(45) DEFAULT NULL,
  `LOGOUT_TIME` varchar(45) DEFAULT NULL,
  `REG_DATE` varchar(45) DEFAULT NULL,
  `LOGGED` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stu_login`
--

INSERT INTO `stu_login` (`STU_L_ID`, `REGISTER_ID`, `PASSWORD`, `STATUS`, `STU_ID`, `LOGIN_TIME`, `LOGOUT_TIME`, `REG_DATE`, `LOGGED`) VALUES
(310, 'SP00001', '202cb962ac59075b964b07152d234b70', 'Active', 276, '2021-05-13 04:56:49 PM', '2021-05-13 04:56:49 PM', '2021-05-13 12:53:36 PM', '0');

-- --------------------------------------------------------

--
-- Table structure for table `subject`
--

CREATE TABLE `subject` (
  `SUB_ID` int(11) NOT NULL,
  `SUBJECT_NAME` text DEFAULT NULL,
  `LEVEL_ID` int(11) NOT NULL,
  `PICTURE` text DEFAULT NULL,
  `SH_LEVEL` varchar(45) DEFAULT NULL COMMENT 'Agriculture = AGR,\nBST = BST,\nBA = Agri/BST'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `subject`
--

INSERT INTO `subject` (`SUB_ID`, `SUBJECT_NAME`, `LEVEL_ID`, `PICTURE`, `SH_LEVEL`) VALUES
(29, 'Mathematics', 18, '0', 'NO'),
(30, 'Science', 18, '0', 'NO'),
(31, 'History', 18, '0', 'NO'),
(32, 'Music', 18, '0', 'NO'),
(33, 'Civic Education', 18, '0', 'NO'),
(34, 'English', 18, '0', 'NO'),
(35, 'Commerce', 18, '0', 'NO'),
(36, 'Buddhism', 18, '0', 'NO'),
(37, 'ICT', 18, '0', 'NO'),
(38, 'Sinhala', 18, '0', 'NO'),
(39, 'Tamil', 18, '0', 'NO'),
(40, 'Art', 18, '0', 'NO'),
(41, 'Entrepreneurship Education', 18, '0', 'NO'),
(42, 'Drama & Theater', 18, '0', 'NO'),
(43, 'Agriculture', 18, '0', 'NO'),
(44, 'Health', 18, '0', 'NO'),
(45, 'Geography', 18, '0', 'NO'),
(46, 'Sinhala Literature', 18, '0', 'NO'),
(47, 'Biology', 19, '0', 'NO'),
(48, 'Chemistry', 19, '0', 'NO'),
(49, 'Physics', 19, '0', 'NO'),
(50, 'ICT', 19, '0', 'NO'),
(51, 'Engineering Technology', 19, '0', 'NO'),
(52, 'Bio Science Technology', 19, '0', 'NO'),
(53, 'Science For Technology', 19, '0', 'NO'),
(54, 'Agriculture', 19, '0', 'NO'),
(55, 'Geography', 19, '0', 'NO'),
(56, 'Media', 19, '0', 'NO'),
(57, 'Drama & Theater', 19, '0', 'NO'),
(58, 'Political Science', 19, '0', 'NO'),
(59, 'Accounting', 19, '0', 'NO'),
(60, 'Business Studies', 19, '0', 'NO'),
(61, 'Economic', 19, '0', 'NO'),
(62, 'Combine Maths', 19, '0', 'NO'),
(63, 'Sinhala', 19, '0', 'NO'),
(64, 'Buddhist Civilization', 19, '0', 'NO');

-- --------------------------------------------------------

--
-- Table structure for table `teacher_details`
--

CREATE TABLE `teacher_details` (
  `TEACH_ID` int(11) NOT NULL,
  `POSITION` varchar(45) DEFAULT NULL,
  `F_NAME` varchar(45) DEFAULT NULL,
  `L_NAME` varchar(45) DEFAULT NULL,
  `DOB` varchar(45) DEFAULT NULL,
  `ADDRESS` text DEFAULT NULL,
  `TP` varchar(40) DEFAULT NULL,
  `PICTURE` text DEFAULT NULL,
  `EMAIL` varchar(100) DEFAULT NULL,
  `ZOOM_LINK` text DEFAULT NULL,
  `GENDER` varchar(20) DEFAULT NULL,
  `QUALIFICATION` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `teacher_details`
--

INSERT INTO `teacher_details` (`TEACH_ID`, `POSITION`, `F_NAME`, `L_NAME`, `DOB`, `ADDRESS`, `TP`, `PICTURE`, `EMAIL`, `ZOOM_LINK`, `GENDER`, `QUALIFICATION`) VALUES
(8, 'Mr', 'Udagama Liyanage', 'Kusal Akalanka', '1996-02-01', 'Ihala karannagoda, warakagoda, neboda', '0702515852', '0', 'kusalakalanka9@gmail.com', '0', 'Male', 'B.ed '),
(9, 'Mr', 'DUMINDU CHAMARA', 'DHARMADASA', '1982-10-30', '97/17 kirimatihena Godagama Homagama', '0777293473', '0', 'duminduchamara@690.lk', '0', 'Male', 'Med Open university\r\nBed Colombo university'),
(10, 'Ms', 'M.C', 'Dilrangi', '1996-11-21', 'No.134/f, Udumulla Padukka', '0775078645', '0', 'chamalidilrangi29@gmail.com', '0', 'Female', 'Passed a/l with maths stream.government school taining teacher'),
(13, 'Ms', 'Nuwani', 'Tharangika', '2001-05-13', 'Gintota', '0764579113', '0', 'sknuwanitharangika@gmail.com', '0', 'Female', 'Dip in ICT\r\nWorking government teacher science 2008'),
(14, 'Mr', 'Sagara', 'Gunawardana ', '1971-04-03', '168/23 Amarasiri Gunawardana mawatha,Piliyandala. ', '0777310295', '0', 'sagaragun3@gmail.com', '0', 'Male', 'B.Sc, PGDE , M.Ed( reading)');

-- --------------------------------------------------------

--
-- Table structure for table `teacher_login`
--

CREATE TABLE `teacher_login` (
  `TECH_L_ID` int(11) NOT NULL,
  `USERNAME` varchar(45) DEFAULT NULL,
  `PASSWORD` varchar(45) DEFAULT NULL,
  `TEACH_ID` int(11) NOT NULL,
  `STATUS` varchar(45) DEFAULT NULL,
  `LOGIN_TIME` varchar(45) DEFAULT NULL,
  `LOGOUT_TIME` varchar(45) DEFAULT NULL,
  `REG_DATE` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `teacher_login`
--

INSERT INTO `teacher_login` (`TECH_L_ID`, `USERNAME`, `PASSWORD`, `TEACH_ID`, `STATUS`, `LOGIN_TIME`, `LOGOUT_TIME`, `REG_DATE`) VALUES
(8, 'kusal akalanka', 'bc8673950dae08318b1b5794c2284224', 8, 'Active', '2021-05-13 10:32:04 AM', '2021-05-13 10:32:59 AM', '2021-05-13 10:24:32 AM'),
(9, 'dumindu', 'fb87582825f9d28a8d42c5e5e5e8b23d', 9, 'Active', NULL, NULL, '2021-05-13 11:27:39 AM'),
(10, 'chamali', '94da69f99954898c02bbe81ddb6a5d10', 10, 'Active', '2021-05-13 03:39:41 PM', '2021-05-13 03:42:33 PM', '2021-05-13 02:51:13 PM'),
(13, 'nuwani', '05a5cf06982ba7892ed2a6d38fe832d6', 13, 'Active', NULL, NULL, '2021-05-13 04:05:45 PM'),
(14, 'sagarag', '597f2bb425ba7ea561d90f91fe99eea7', 14, 'Active', NULL, NULL, '2021-05-13 04:41:20 PM');

-- --------------------------------------------------------

--
-- Table structure for table `teacher_qualifications`
--

CREATE TABLE `teacher_qualifications` (
  `T_Q_ID` int(11) NOT NULL,
  `TEACH_ID` int(11) NOT NULL,
  `QUALIFI_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `teacher_subject`
--

CREATE TABLE `teacher_subject` (
  `T_S_ID` int(11) NOT NULL,
  `SUB_ID` int(11) NOT NULL,
  `TEACH_ID` int(11) NOT NULL,
  `LEVEL_ID` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `teacher_subject`
--

INSERT INTO `teacher_subject` (`T_S_ID`, `SUB_ID`, `TEACH_ID`, `LEVEL_ID`) VALUES
(13, 29, 10, '18'),
(14, 37, 13, '18');

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `TRA_ID` int(11) NOT NULL,
  `UPDATE_TIME` varchar(100) DEFAULT NULL,
  `CLASS_ID` int(11) NOT NULL,
  `TEACH_ID` int(11) NOT NULL,
  `STU_ID` int(11) NOT NULL,
  `FREE_CLASS` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `uploads`
--

CREATE TABLE `uploads` (
  `UPLOAD_ID` int(11) NOT NULL,
  `FILE_NAME` text DEFAULT NULL,
  `TITLE` varchar(100) DEFAULT NULL,
  `DESCRIPTION` text DEFAULT NULL,
  `UPL_DATE` varchar(45) DEFAULT NULL,
  `UPL_TIME` varchar(45) DEFAULT NULL,
  `TYPE` varchar(45) DEFAULT NULL,
  `EXTENSION` varchar(45) DEFAULT NULL,
  `CLASS_ID` int(11) NOT NULL,
  `UPLOAD_TIME` varchar(45) DEFAULT NULL,
  `YOUTUBE_LINK` text DEFAULT NULL,
  `PUBLIC` varchar(45) DEFAULT NULL COMMENT 'DOCUMENT,VIDEO LINKS,AUDIO SHOW OR HIDDEN FOR STUDENT(PAY OR NOT)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `uploads_clicked`
--

CREATE TABLE `uploads_clicked` (
  `UP_CL_ID` int(11) NOT NULL,
  `UPLOAD_ID` int(11) NOT NULL,
  `STU_ID` int(11) NOT NULL,
  `TYPE` varchar(45) DEFAULT NULL,
  `CLICKED_TIME` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `add_subject`
--
ALTER TABLE `add_subject`
  ADD PRIMARY KEY (`ADD_SUB_ID`,`T_S_ID`,`STU_ID`),
  ADD KEY `fk_add_subject_teacher_subject1_idx` (`T_S_ID`),
  ADD KEY `fk_add_subject_student_details1_idx` (`STU_ID`);

--
-- Indexes for table `admin_login`
--
ALTER TABLE `admin_login`
  ADD PRIMARY KEY (`ADMIN_ID`);

--
-- Indexes for table `alert`
--
ALTER TABLE `alert`
  ADD PRIMARY KEY (`ALERT_ID`);

--
-- Indexes for table `attendence`
--
ALTER TABLE `attendence`
  ADD PRIMARY KEY (`ATT_ID`,`STU_ID`),
  ADD KEY `fk_attendence_student_details1_idx` (`STU_ID`);

--
-- Indexes for table `audience`
--
ALTER TABLE `audience`
  ADD PRIMARY KEY (`AUD_ID`,`ALERT_ID`),
  ADD KEY `fk_audience_alert1_idx` (`ALERT_ID`);

--
-- Indexes for table `classes`
--
ALTER TABLE `classes`
  ADD PRIMARY KEY (`CLASS_ID`,`TEACH_ID`,`SUB_ID`),
  ADD KEY `fk_classes_teacher_details1_idx` (`TEACH_ID`),
  ADD KEY `fk_classes_subject1_idx` (`SUB_ID`);

--
-- Indexes for table `custom_student`
--
ALTER TABLE `custom_student`
  ADD PRIMARY KEY (`SHOW_STU_ID`,`ALERT_ID`),
  ADD KEY `fk_show_students_alert1_idx` (`ALERT_ID`);

--
-- Indexes for table `essay_master_question`
--
ALTER TABLE `essay_master_question`
  ADD PRIMARY KEY (`ESSAY_TRANS_ID`,`PAPER_ID`),
  ADD KEY `fk_essay_trans_paper_master1_idx` (`PAPER_ID`);

--
-- Indexes for table `essay_sub_question`
--
ALTER TABLE `essay_sub_question`
  ADD PRIMARY KEY (`ESSAY_SUB_QUE_ID`,`ESSAY_TRANS_ID`),
  ADD KEY `fk_essay_sub_question_essay_master_question1_idx` (`ESSAY_TRANS_ID`);

--
-- Indexes for table `exam_result`
--
ALTER TABLE `exam_result`
  ADD PRIMARY KEY (`EXAM_RESULT_ID`,`CLASS_ID`),
  ADD KEY `fk_exam_result_classes1_idx` (`CLASS_ID`);

--
-- Indexes for table `expenditure`
--
ALTER TABLE `expenditure`
  ADD PRIMARY KEY (`EXP_ID`,`REASON_ID`),
  ADD KEY `fk_expenditure_reason1_idx` (`REASON_ID`);

--
-- Indexes for table `income`
--
ALTER TABLE `income`
  ADD PRIMARY KEY (`INCOME_ID`);

--
-- Indexes for table `institute`
--
ALTER TABLE `institute`
  ADD PRIMARY KEY (`INS_ID`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`LEVEL_ID`);

--
-- Indexes for table `link_clicked`
--
ALTER TABLE `link_clicked`
  ADD PRIMARY KEY (`LINK_C_ID`,`STU_ID`,`TEACH_ID`,`CLASS_ID`),
  ADD KEY `fk_link_clicked_student_details1_idx` (`STU_ID`),
  ADD KEY `fk_link_clicked_teacher_details1_idx` (`TEACH_ID`),
  ADD KEY `fk_link_clicked_classes1_idx` (`CLASS_ID`);

--
-- Indexes for table `login_report`
--
ALTER TABLE `login_report`
  ADD PRIMARY KEY (`LOG_R_ID`,`STU_ID`),
  ADD KEY `fk_login_report_stu_login1_idx` (`STU_ID`);

--
-- Indexes for table `master_sms`
--
ALTER TABLE `master_sms`
  ADD PRIMARY KEY (`MASTER_SMS_ID`);

--
-- Indexes for table `mcq_trans`
--
ALTER TABLE `mcq_trans`
  ADD PRIMARY KEY (`MCQ_TRAN_ID`,`PAPER_ID`),
  ADD KEY `fk_mcq_trans_mcq_master1_idx` (`PAPER_ID`);

--
-- Indexes for table `multi_sms`
--
ALTER TABLE `multi_sms`
  ADD PRIMARY KEY (`MULTI_SMS_ID`,`MASTER_SMS_ID`),
  ADD KEY `fk_multi_sms_master_sms1_idx` (`MASTER_SMS_ID`);

--
-- Indexes for table `paper_image`
--
ALTER TABLE `paper_image`
  ADD PRIMARY KEY (`PAPER_IMG_ID`,`STU_ESSAY_ID`),
  ADD KEY `fk_paper_image_student_essay_master1_idx` (`STU_ESSAY_ID`);

--
-- Indexes for table `paper_master`
--
ALTER TABLE `paper_master`
  ADD PRIMARY KEY (`PAPER_ID`,`CLASS_ID`,`TEACH_ID`),
  ADD KEY `fk_mcq_master_classes1_idx` (`CLASS_ID`),
  ADD KEY `fk_paper_master_teacher_details1_idx` (`TEACH_ID`);

--
-- Indexes for table `payment_data`
--
ALTER TABLE `payment_data`
  ADD PRIMARY KEY (`PAY_ID`,`TRA_ID`),
  ADD KEY `fk_payment_data_transactions1_idx` (`TRA_ID`);

--
-- Indexes for table `profile_settings`
--
ALTER TABLE `profile_settings`
  ADD PRIMARY KEY (`PRO_S_ID`);

--
-- Indexes for table `qualification`
--
ALTER TABLE `qualification`
  ADD PRIMARY KEY (`QUALIFI_ID`);

--
-- Indexes for table `reason`
--
ALTER TABLE `reason`
  ADD PRIMARY KEY (`REASON_ID`);

--
-- Indexes for table `reg_count`
--
ALTER TABLE `reg_count`
  ADD PRIMARY KEY (`REG_COUNT_ID`);

--
-- Indexes for table `seminar`
--
ALTER TABLE `seminar`
  ADD PRIMARY KEY (`SEMI_ID`,`CLASS_ID`),
  ADD KEY `fk_seminar_classes1_idx` (`CLASS_ID`);

--
-- Indexes for table `single_sms`
--
ALTER TABLE `single_sms`
  ADD PRIMARY KEY (`SINGLE_SMS_ID`);

--
-- Indexes for table `single_sms_gateway`
--
ALTER TABLE `single_sms_gateway`
  ADD PRIMARY KEY (`SINGLE_SMS_ID`);

--
-- Indexes for table `sms_counter`
--
ALTER TABLE `sms_counter`
  ADD PRIMARY KEY (`SUB_SMS_C_ID`);

--
-- Indexes for table `sms_gateway`
--
ALTER TABLE `sms_gateway`
  ADD PRIMARY KEY (`SMS_G_ID`,`INS_ID`),
  ADD KEY `fk_sms_gateway_institute1_idx` (`INS_ID`);

--
-- Indexes for table `staff_login`
--
ALTER TABLE `staff_login`
  ADD PRIMARY KEY (`STAFF_ID`);

--
-- Indexes for table `student_access_count`
--
ALTER TABLE `student_access_count`
  ADD PRIMARY KEY (`ST_COUNT_ID`);

--
-- Indexes for table `student_details`
--
ALTER TABLE `student_details`
  ADD PRIMARY KEY (`STU_ID`);

--
-- Indexes for table `student_essay_master`
--
ALTER TABLE `student_essay_master`
  ADD PRIMARY KEY (`STU_ESSAY_ID`,`STU_ID`),
  ADD KEY `fk_student_assay_master_student_details1_idx` (`STU_ID`);

--
-- Indexes for table `student_essay_question_master`
--
ALTER TABLE `student_essay_question_master`
  ADD PRIMARY KEY (`STU_ESSAY_MASTER_ID`);

--
-- Indexes for table `student_essay_question_trans`
--
ALTER TABLE `student_essay_question_trans`
  ADD PRIMARY KEY (`STU_ESSAY_TRANS_ID`,`STU_ESSAY_MASTER_ID`),
  ADD KEY `fk_student_essay_trans_student_essay_master1_idx` (`STU_ESSAY_MASTER_ID`);

--
-- Indexes for table `student_mcq_main`
--
ALTER TABLE `student_mcq_main`
  ADD PRIMARY KEY (`STU_MAIN_ID`,`PAPER_ID`,`STU_ID`),
  ADD KEY `fk_student_mcq_main_paper_master1_idx` (`PAPER_ID`),
  ADD KEY `fk_student_mcq_main_student_details1_idx` (`STU_ID`);

--
-- Indexes for table `student_mcq_master`
--
ALTER TABLE `student_mcq_master`
  ADD PRIMARY KEY (`STU_MCQ_MASTER_ID`,`STU_ID`),
  ADD KEY `fk_student_mcq_master_student_details1_idx` (`STU_ID`);

--
-- Indexes for table `student_mcq_transaction`
--
ALTER TABLE `student_mcq_transaction`
  ADD PRIMARY KEY (`STU_MCQ_TRANS_ID`,`STU_ID`,`STU_MAIN_ID`),
  ADD KEY `fk_student_mcq_transaction_student_details1_idx` (`STU_ID`),
  ADD KEY `fk_student_mcq_transaction_student_mcq_main1_idx` (`STU_MAIN_ID`);

--
-- Indexes for table `stu_login`
--
ALTER TABLE `stu_login`
  ADD PRIMARY KEY (`STU_L_ID`,`STU_ID`),
  ADD KEY `fk_stu_login_student_details1_idx` (`STU_ID`);

--
-- Indexes for table `subject`
--
ALTER TABLE `subject`
  ADD PRIMARY KEY (`SUB_ID`,`LEVEL_ID`),
  ADD KEY `fk_subject_level1_idx` (`LEVEL_ID`);

--
-- Indexes for table `teacher_details`
--
ALTER TABLE `teacher_details`
  ADD PRIMARY KEY (`TEACH_ID`);

--
-- Indexes for table `teacher_login`
--
ALTER TABLE `teacher_login`
  ADD PRIMARY KEY (`TECH_L_ID`,`TEACH_ID`),
  ADD KEY `fk_teacher_login_teacher_details1_idx` (`TEACH_ID`);

--
-- Indexes for table `teacher_qualifications`
--
ALTER TABLE `teacher_qualifications`
  ADD PRIMARY KEY (`T_Q_ID`,`TEACH_ID`,`QUALIFI_ID`),
  ADD KEY `fk_teacher_qulifications_teacher_details1_idx` (`TEACH_ID`),
  ADD KEY `fk_teacher_qulifications_qualification1_idx` (`QUALIFI_ID`);

--
-- Indexes for table `teacher_subject`
--
ALTER TABLE `teacher_subject`
  ADD PRIMARY KEY (`T_S_ID`,`SUB_ID`,`TEACH_ID`),
  ADD KEY `fk_teacher_subject_subject1_idx` (`SUB_ID`),
  ADD KEY `fk_teacher_subject_teacher_details1_idx` (`TEACH_ID`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`TRA_ID`,`CLASS_ID`,`TEACH_ID`,`STU_ID`),
  ADD KEY `fk_transactions_classes1_idx` (`CLASS_ID`),
  ADD KEY `fk_transactions_teacher_details1_idx` (`TEACH_ID`),
  ADD KEY `fk_transactions_student_details1_idx` (`STU_ID`);

--
-- Indexes for table `uploads`
--
ALTER TABLE `uploads`
  ADD PRIMARY KEY (`UPLOAD_ID`,`CLASS_ID`),
  ADD KEY `fk_uploads_classes1_idx` (`CLASS_ID`);

--
-- Indexes for table `uploads_clicked`
--
ALTER TABLE `uploads_clicked`
  ADD PRIMARY KEY (`UP_CL_ID`,`UPLOAD_ID`,`STU_ID`),
  ADD KEY `fk_uploads_clicked_uploads1_idx` (`UPLOAD_ID`),
  ADD KEY `fk_uploads_clicked_student_details1_idx` (`STU_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `add_subject`
--
ALTER TABLE `add_subject`
  MODIFY `ADD_SUB_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `admin_login`
--
ALTER TABLE `admin_login`
  MODIFY `ADMIN_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `alert`
--
ALTER TABLE `alert`
  MODIFY `ALERT_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `attendence`
--
ALTER TABLE `attendence`
  MODIFY `ATT_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `audience`
--
ALTER TABLE `audience`
  MODIFY `AUD_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `classes`
--
ALTER TABLE `classes`
  MODIFY `CLASS_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `custom_student`
--
ALTER TABLE `custom_student`
  MODIFY `SHOW_STU_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `essay_master_question`
--
ALTER TABLE `essay_master_question`
  MODIFY `ESSAY_TRANS_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `essay_sub_question`
--
ALTER TABLE `essay_sub_question`
  MODIFY `ESSAY_SUB_QUE_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;

--
-- AUTO_INCREMENT for table `exam_result`
--
ALTER TABLE `exam_result`
  MODIFY `EXAM_RESULT_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `expenditure`
--
ALTER TABLE `expenditure`
  MODIFY `EXP_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `income`
--
ALTER TABLE `income`
  MODIFY `INCOME_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;

--
-- AUTO_INCREMENT for table `institute`
--
ALTER TABLE `institute`
  MODIFY `INS_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `level`
--
ALTER TABLE `level`
  MODIFY `LEVEL_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `link_clicked`
--
ALTER TABLE `link_clicked`
  MODIFY `LINK_C_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=324;

--
-- AUTO_INCREMENT for table `login_report`
--
ALTER TABLE `login_report`
  MODIFY `LOG_R_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8098;

--
-- AUTO_INCREMENT for table `master_sms`
--
ALTER TABLE `master_sms`
  MODIFY `MASTER_SMS_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mcq_trans`
--
ALTER TABLE `mcq_trans`
  MODIFY `MCQ_TRAN_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `multi_sms`
--
ALTER TABLE `multi_sms`
  MODIFY `MULTI_SMS_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `paper_image`
--
ALTER TABLE `paper_image`
  MODIFY `PAPER_IMG_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;

--
-- AUTO_INCREMENT for table `paper_master`
--
ALTER TABLE `paper_master`
  MODIFY `PAPER_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;

--
-- AUTO_INCREMENT for table `payment_data`
--
ALTER TABLE `payment_data`
  MODIFY `PAY_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;

--
-- AUTO_INCREMENT for table `profile_settings`
--
ALTER TABLE `profile_settings`
  MODIFY `PRO_S_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `qualification`
--
ALTER TABLE `qualification`
  MODIFY `QUALIFI_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reason`
--
ALTER TABLE `reason`
  MODIFY `REASON_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reg_count`
--
ALTER TABLE `reg_count`
  MODIFY `REG_COUNT_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `seminar`
--
ALTER TABLE `seminar`
  MODIFY `SEMI_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `single_sms`
--
ALTER TABLE `single_sms`
  MODIFY `SINGLE_SMS_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=363;

--
-- AUTO_INCREMENT for table `single_sms_gateway`
--
ALTER TABLE `single_sms_gateway`
  MODIFY `SINGLE_SMS_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sms_counter`
--
ALTER TABLE `sms_counter`
  MODIFY `SUB_SMS_C_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sms_gateway`
--
ALTER TABLE `sms_gateway`
  MODIFY `SMS_G_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `staff_login`
--
ALTER TABLE `staff_login`
  MODIFY `STAFF_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `student_access_count`
--
ALTER TABLE `student_access_count`
  MODIFY `ST_COUNT_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `student_details`
--
ALTER TABLE `student_details`
  MODIFY `STU_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=278;

--
-- AUTO_INCREMENT for table `student_essay_master`
--
ALTER TABLE `student_essay_master`
  MODIFY `STU_ESSAY_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;

--
-- AUTO_INCREMENT for table `student_essay_question_master`
--
ALTER TABLE `student_essay_question_master`
  MODIFY `STU_ESSAY_MASTER_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `student_essay_question_trans`
--
ALTER TABLE `student_essay_question_trans`
  MODIFY `STU_ESSAY_TRANS_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=208;

--
-- AUTO_INCREMENT for table `student_mcq_main`
--
ALTER TABLE `student_mcq_main`
  MODIFY `STU_MAIN_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT for table `student_mcq_master`
--
ALTER TABLE `student_mcq_master`
  MODIFY `STU_MCQ_MASTER_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `student_mcq_transaction`
--
ALTER TABLE `student_mcq_transaction`
  MODIFY `STU_MCQ_TRANS_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `stu_login`
--
ALTER TABLE `stu_login`
  MODIFY `STU_L_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=312;

--
-- AUTO_INCREMENT for table `subject`
--
ALTER TABLE `subject`
  MODIFY `SUB_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `teacher_details`
--
ALTER TABLE `teacher_details`
  MODIFY `TEACH_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `teacher_login`
--
ALTER TABLE `teacher_login`
  MODIFY `TECH_L_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `teacher_qualifications`
--
ALTER TABLE `teacher_qualifications`
  MODIFY `T_Q_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `teacher_subject`
--
ALTER TABLE `teacher_subject`
  MODIFY `T_S_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `TRA_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=340;

--
-- AUTO_INCREMENT for table `uploads`
--
ALTER TABLE `uploads`
  MODIFY `UPLOAD_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT for table `uploads_clicked`
--
ALTER TABLE `uploads_clicked`
  MODIFY `UP_CL_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=408;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `add_subject`
--
ALTER TABLE `add_subject`
  ADD CONSTRAINT `fk_add_subject_student_details1` FOREIGN KEY (`STU_ID`) REFERENCES `student_details` (`STU_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_add_subject_teacher_subject1` FOREIGN KEY (`T_S_ID`) REFERENCES `teacher_subject` (`T_S_ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `attendence`
--
ALTER TABLE `attendence`
  ADD CONSTRAINT `fk_attendence_student_details1` FOREIGN KEY (`STU_ID`) REFERENCES `student_details` (`STU_ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `audience`
--
ALTER TABLE `audience`
  ADD CONSTRAINT `fk_audience_alert1` FOREIGN KEY (`ALERT_ID`) REFERENCES `alert` (`ALERT_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `classes`
--
ALTER TABLE `classes`
  ADD CONSTRAINT `fk_classes_subject1` FOREIGN KEY (`SUB_ID`) REFERENCES `subject` (`SUB_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_classes_teacher_details1` FOREIGN KEY (`TEACH_ID`) REFERENCES `teacher_details` (`TEACH_ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `custom_student`
--
ALTER TABLE `custom_student`
  ADD CONSTRAINT `fk_show_students_alert1` FOREIGN KEY (`ALERT_ID`) REFERENCES `alert` (`ALERT_ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `essay_master_question`
--
ALTER TABLE `essay_master_question`
  ADD CONSTRAINT `fk_essay_trans_paper_master1` FOREIGN KEY (`PAPER_ID`) REFERENCES `paper_master` (`PAPER_ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `essay_sub_question`
--
ALTER TABLE `essay_sub_question`
  ADD CONSTRAINT `fk_essay_sub_question_essay_master_question1` FOREIGN KEY (`ESSAY_TRANS_ID`) REFERENCES `essay_master_question` (`ESSAY_TRANS_ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `exam_result`
--
ALTER TABLE `exam_result`
  ADD CONSTRAINT `fk_exam_result_classes1` FOREIGN KEY (`CLASS_ID`) REFERENCES `classes` (`CLASS_ID`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
