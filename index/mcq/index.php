<?php 
	include('../../connect/connect.php');
	session_start();
	$last_attend = '';
	if($_POST['status'] == '1')
	{
		
		$mcq_main_id = $_POST['mcq_main_id'];
		$paper_id = $_POST['paper_id'];
		$student_id = $_POST['student_id'];
		$teacher_id = $_POST['teacher_id'];
		$class_id = $_POST['class_id'];

		$sql007 = mysqli_query($conn,"SELECT * FROM `student_mcq_main` WHERE `PAPER_ID` = '$paper_id' AND `STU_ID` = '$student_id'");
		while($row007 = mysqli_fetch_assoc($sql007))
		{
			$last_attend = $row007['ATTEND_TIME'];
		}

	}else
	if($_POST['status'] == '0')
	{
		
		$student_id = $_POST['student_id'];
		$teacher_id = $_POST['teacher_id'];
		$class_id = $_POST['class_id'];

	}



	$s01 = mysqli_query($conn,"SELECT * FROM `student_details` WHERE `STU_ID` = '$student_id'");
	while($row = mysqli_fetch_assoc($s01))
	{
	    $st_name = $row['F_NAME']." ".$row['L_NAME']; //student name
	    $dob = $row['DOB'];  //student DOB
	    $email = $row['EMAIL'];  //student Email
	    $address = $row['ADDRESS'];  //student Address
	    $picture = $row['PICTURE']; // //student Picture
	    $gender = $row['GENDER']; // //student Gender

	     if($picture == '0')
	     {
	        if($gender == 'Male')
	        {
	          $picture = 'boy202015.png';
	        }else
	        if($gender == 'Female')
	        {
	          $picture = 'girl202015.png';
	        }
	     }
	    

	    $tp = $row['TP'];
	    $have_sub = $row['NEW'];

	    $ssql01 = mysqli_query($conn,"SELECT * FROM stu_login WHERE STU_ID = '$student_id'");
	    while($row001 = mysqli_fetch_assoc($ssql01))
	    {
	        $last_time = $row001['LOGIN_TIME'];
	        $reg_id = $row001['REGISTER_ID'];
	    }

	}
	
 ?>


<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from beshley.com/mcard/theme_colors/purple/index-1-particles.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 26 Jan 2021 18:39:46 GMT -->
<head>
	
	<!-- Basic -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title> Student Profile | <?php echo $st_name; ?> | <?php echo $reg_id; ?></title>
	<meta name="description" content="">
	<meta name="keywords" content="">
	
	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	
	<!-- Load Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic" rel="stylesheet">

	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
	
	<!-- CSS -->
	<link rel="stylesheet" href="css/basic.css" />
	<link rel="stylesheet" href="css/layout.css" />
	<link rel="stylesheet" href="css/ionicons.css" />
	<link rel="stylesheet" href="css/owl.carousel.css" />
	<link rel="stylesheet" href="css/magnific-popup.css" />
	<link rel="stylesheet" href="css/animate.css" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/pdf.js/2.0.943/pdf.min.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=PT+Sans&display=swap" rel="stylesheet">
	
	<!--[if lt IE 9]>
	<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
	<!-- Favicon -->
	<link rel="shortcut icon" href="images/favicons/favicon.ico">

	<style type="text/css">
		#canvas_container {
          width: auto;
          height: 567px;
          overflow: auto;
      }
 
      #canvas_container {
        background: #ccc;
        text-align: center;
      }

      #canvas_container2 {
          width: auto;
          height: 500px;
          overflow: auto;
      }
 
      #canvas_container2 {
        background: #ccc;
        text-align: center;
      }

      /* width */
	::-webkit-scrollbar {
	  width: 10px;
	}

	/* Track */
	::-webkit-scrollbar-track {
	  background: #f1f1f1;
	}

	/* Handle */
	::-webkit-scrollbar-thumb {
	  background: #888;
	}

	/* Handle on hover */
	::-webkit-scrollbar-thumb:hover {
	  background: #555;
	}
#show_paper
{
	opacity: 0;
	pointer-events: none;
}

#back
{
	opacity: 0.5;
	pointer-events: visible;
	outline: none;
	border-radius: 90px;
	width: 60px;
	font-size: 22px;
	padding: 8px 14px 50px 14px;
}
#back:hover
{
	opacity: 5;
	pointer-events: visible;
	outline: none;
	border-radius: 90px;
	width: 60px;
	font-size: 22px;
	padding: 8px 14px 50px 14px;
	
}

@media only screen and (max-width: 600px) {
#exam {
	display: none;
}
#show_paper
{
	opacity: 5;
	pointer-events: visible;
	outline: none;
	border-radius: 90px;
	width: 60px;
	padding: 12px 14px 46px 14px;
}
}

#ctrl_btn
{
	opacity: 0.25;
	transition: 0.7s;
	position: fixed;
	bottom: 40px;
	right: 50px;
	padding: 20px 10px 20px 0px;
	width: 100%;
}

#ctrl_btn:hover
{
	opacity: 5;
}



	</style>
	
</head>

<body style="font-family: 'PT Sans', sans-serif;">
	
	<!-- Page -->
	<div class="page" id="home-section">


		<!-- Started Background -->
		<div class="started-bg">
			<div id="particles-bg" class="slide" style="background-image: url(images/particles-bg.jpg);"></div>
		</div>

		<!-- Header -->
		
		<!-- Container -->
		<div class="container-fluid" style="background-color: #eeeeee;position: relative;">

			<?php 


			$sql001 = mysqli_query($conn,"SELECT * FROM `teacher_details` WHERE `TEACH_ID` = '$teacher_id' "); //check Classes for registered teachers details
            while($row001 = mysqli_fetch_assoc($sql001))
            {

                $teacher_position = $row001['POSITION']; //Teacher Position
                $teacher_f_name = $row001['F_NAME']; //Teacher First Name
                $teacher_l_name = $row001['L_NAME']; //Teacher Last Name
                $teacher_gender = $row001['GENDER']; //Teacher Gender
                $teacher_qualification = $row001['QUALIFICATION']; //Teacher Qualification

                $teacher_picture = $row001['PICTURE']; //Teacher Qualification

                $teacher_full_name = $teacher_position.". ".$teacher_f_name." ".$teacher_l_name;


                //If Teacher Picture zero,Check gender and gender to select picture 
                if($teacher_picture == '0')
                {
                    if($teacher_gender == 'Male')
                    {
                        $teacher_picture = 'b_teacher.jpg';
                    }else
                    if($teacher_gender == 'Female')
                    {
                        $teacher_picture = 'g_teacher.jpg';
                    }
                }
                //If Teacher Picture zero,Check gender and gender to select picture 


            }

            $sql002 = mysqli_query($conn,"SELECT * FROM `classes` WHERE `CLASS_ID` = '$class_id'"); //check Classes for registered teachers details
	        while($row002 = mysqli_fetch_assoc($sql002))
	        {
	            $class_name = $row002['CLASS_NAME'];
	        }

	        $sql003 = mysqli_query($conn,"SELECT * FROM `paper_master` WHERE `CLASS_ID` = '$class_id' AND `PAPER_TYPE` = '1'"); //check mcq test table
	        while($row003 = mysqli_fetch_assoc($sql003))
	        {
	            $finish_time = $row003['FINISH_TIME']; //MCQ Test Hold Time
	            $finish_date = $row003['FINISH_DATE']; //MCQ Test Hold Date

	            $finish_d_t = strtotime($finish_date." ".$finish_time);

	            $during_hours = $row003['TIME_DURATION_HOUR']; //Time duration hour
	            $during_min = $row003['TIME_DURATION_MIN']; //Time duration minute
	            $pdf_name = $row003['PDF_NAME']; //Paper Name
	            $answer_amount = $row003['NO_QUESTION_ANSWER'];
	            $question_amount = $row003['NO_OF_QUESTIONS'];
				
				$total_marks_answer = $row003['ANSWER_MARKS'];

	            $paper_id = $row003['PAPER_ID']; //Paper ID
	        }

	       
	        /*Attend date and time using get time*/

	        $today = strtotime($last_attend);
	        $month = date('F',$today);
	        $day = date('d',$today);
	        $year = date('Y',$today);

	        $h_m = date('H:i:s',$today); //Hours and min

			 ?>

			<!-- Started -->
			<div class="section started" style="padding:0;margin: 0;">
				<div class="st-box" style="padding-bottom: 0;margin-bottom: 0;">
					<div class="st-image"><img src="../../teacher/images/profile/<?php echo $teacher_picture; ?>" alt="" /></div>
					<div class="st-title" style="font-weight: 500;text-transform: capitalize;"><?php echo $teacher_full_name; ?></div>
					<small><?php echo $teacher_qualification; ?></small>
					<div class="st-subtitle" style="text-transform: capitalize;"><?php echo $class_name; ?> MCQ Test</div>

					<small class="st-subtitle" style="color: #dd20fdeb;">(<?php echo $during_hours ?> Hour <?php echo $during_min ?> Minute)</small>
					<div class="st-soc">
						<a class="btn_animated" style="width: 200px;padding-bottom: 45px;">
 							<label id="count_time" style="color: white;font-size: 24px;font-weight: 400;"></label>
						</a>

						<script>

				          // The data/time we want to countdown to
				          var countDownDate = new Date("<?php echo $month; ?> <?php echo $day; ?>, <?php echo $year; ?> <?php echo $h_m; ?>").getTime();

				          // Run myfunc every second
				          var myfunc = setInterval(function() {

				          var now = new Date().getTime();
				          var timeleft = countDownDate - now;

				          //alert(timeleft)
				              
				          // Calculating the days, hours, minutes and seconds left
				          var days = Math.floor(timeleft / (1000 * 60 * 60 * 24));
				          var hours = Math.floor((timeleft % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
				          var minutes = Math.floor((timeleft % (1000 * 60 * 60)) / (1000 * 60));
				          var seconds = Math.floor((timeleft % (1000 * 60)) / 1000);
				              
				          // Result is output to the specific element
				          document.getElementById("count_time").innerHTML = hours + "h : " + minutes + "m  : " + seconds + 's'

				          /*============================================================*/
				          
				        //document.getElementById("answer_button").disabled = true; //Disable Answer Button until time out

        				//document.getElementById("high_score_button").disabled = true; //Disable Answer Button until time out

				          /*============================================================*/



				          // Display the message when countdown is over
				          if (timeleft < 0) 
				          {

				            clearInterval(myfunc);
				          	document.getElementById("count_time").innerHTML = hours + "h : " + minutes + "m  : " + seconds + 's'
				            document.getElementById("count_time").innerHTML = "TIME OUT!!";

				            paper_submit(<?php echo $paper_id; ?>,<?php echo $student_id; ?>,<?php echo $question_amount; ?>);

				            //document.getElementById("answer_button").disabled = false //Active Answer Button
				            //document.getElementById("high_score_button").disabled = false //Active Answer Button


				          }


				          if (hours == '1' && minutes == '00' && seconds == '0') 
				          {

				            const Toast = Swal.mixin({
							  toast: true,
							  position: 'top-end',
							  showConfirmButton: false,
							  timer: 5000,
							  timerProgressBar: true,
							  didOpen: (toast) => {
							    toast.addEventListener('mouseenter', Swal.stopTimer)
							    toast.addEventListener('mouseleave', Swal.resumeTimer)
							  }
							})

							Toast.fire({
							  icon: 'success',
							  title: 'ඔබට තවත් පැයක කාලයක් ඉතිරි වී ඇත.'
							})


				          }

				          if (minutes == '30' && seconds == '0') 
				          {

				            const Toast = Swal.mixin({
							  toast: true,
							  position: 'top-end',
							  showConfirmButton: false,
							  timer: 5000,
							  timerProgressBar: true,
							  didOpen: (toast) => {
							    toast.addEventListener('mouseenter', Swal.stopTimer)
							    toast.addEventListener('mouseleave', Swal.resumeTimer)
							  }
							})

							Toast.fire({
							  icon: 'success',
							  title: 'ඔබට තවත් විනාඩි 30ක කාලයක් ඉතිරි වී ඇත.'
							})


				          }

				          if (minutes == '5' && seconds == '0') 
				          {

				            const Toast = Swal.mixin({
							  toast: true,
							  position: 'top-end',
							  showConfirmButton: false,
							  timer: 5000,
							  timerProgressBar: true,
							  didOpen: (toast) => {
							    toast.addEventListener('mouseenter', Swal.stopTimer)
							    toast.addEventListener('mouseleave', Swal.resumeTimer)
							  }
							})

							Toast.fire({
							  icon: 'success',
							  title: 'ඔබට තවත් විනාඩි 05ක කාලයක් ඉතිරි වී ඇත.'
							})


				          }



				          }, 1000);
				          </script>
					</div>
				</div>
			</div>


			<hr style="border: 1px solid #cccc;">
			<!-- Wrapper -->
			<?php 
				$sql005 = mysqli_query($conn,"SELECT * FROM `student_mcq_master` WHERE `PAPER_ID` = '$paper_id' AND `STU_ID` = '$student_id'");

				$check001 = mysqli_num_rows($sql005); //Check Submited?

				if($check001 > 0)
				{
					$result_div = 'show';
					$paper_div = 'none';
				}else
				if($check001 == '0')
				{
					$result_div = 'none';
					$paper_div = 'show';
				}

			 ?>

			<div class="wrapper" style="padding-top: 25px;display: <?php echo $paper_div; ?>;" id="paper">

				<!-- Section Skills -->
				<div class="section skills" id="skills-section" style="padding-top: 0;">
					<div class="title" style="font-weight: 400;"></div>
					<div class="i_title" style="padding-bottom: 20px;">
						<div class="icon"><i class="ion-ios-lightbulb" style="font-size: 40px;color: white;"></i> </div>
						<h2 style="padding-top: 0;">Examination </h2>
					</div>
					<div class="row">
						<div class="col col-m-12 col-t-7 col-d-7" id="exam">
							<div class="content-box animated">
								<div class="i_title">
									<div class="icon"><i class="icon ion ion-document-text"></i></div>
									<div class="name">Paper Sheet</div>
								</div>
								<div class="col-m-12">

									<div id="my_pdf_viewer" oncontextmenu="return false;">
								        <div id="canvas_container" style="position: relative;">
								            <canvas id="pdf_renderer"></canvas>
								        </div>

								        <div class="row" id="ctrl_btn">
								        	
								        		<div class="col-md-3" >

												</div>
												<div class="col-md-6" style="padding-top: 60px;">

													<center><input id="current_page" readonly class="form-control" value="1" type="text" style="text-align: center;border: none;outline: none;width:60px;background-color: #ffffff00;font-weight: 700;" /></center>
												</div>
												<div class="col-md-3"> 

													<div id="navigation_controls" style="float: right;">
										        		<button class="btn btn-primary" id="go_previous" style="font-size: 15px;margin-top: 4px;margin-right: 8px;background-color: #3498db;outline: none;border-radius: 80px;padding-left: 14px;padding-right: 14px;border: 1px solid #3498db;"><span class="fa fa-arrow-left"></span></button>
										            	
										            	<button class="btn btn-primary" id="go_next" style="font-size: 15px;margin-top: 4px;background-color: #3498db;outline: none;border-radius: 80px;padding-left: 14px;padding-right: 14px;border: 1px solid #3498db;"><span class="fa fa-arrow-right"></span>
										            	</button>
													</div>


													<div id="zoom_controls" style="float: right;">
										            	<button class="btn btn-danger" id="zoom_out" style="font-size: 15px;margin-top: 4px;outline: none;margin-right: 9px;background-color: #e74c3c;border-radius: 80px;padding-left: 15px;padding-right: 15px;border: none;"><span class="fa fa-minus"></span></button>

										            	<button class="btn btn-success" id="zoom_in" style="font-size: 15px;margin-top: 4px;outline: none;background-color: #27ae60;border-radius: 80px;padding-left: 15px;padding-right: 15px;border: none;"><span class="fa fa-plus"></span></button>
										        	</div>
								        		</div> 
						        			
									        
								    	</div>
					        		</div>
										    <script>
										        var myState = {
										            pdf: null,
										            currentPage: 1,
										            zoom: 1
										        }
										      
										        pdfjsLib.getDocument('../../admin/images/paper_document/<?php echo $pdf_name; ?>').then((pdf) => {
										      
										            myState.pdf = pdf;
										            render();
										 
										        });
										 
										        function render() {
										            myState.pdf.getPage(myState.currentPage).then((page) => {
										          
										                var canvas = document.getElementById("pdf_renderer");
										                var ctx = canvas.getContext('2d');
										      
										                var viewport = page.getViewport(myState.zoom);
										 
										                canvas.width = viewport.width;
										                canvas.height = viewport.height;
										          
										                page.render({
										                    canvasContext: ctx,
										                    viewport: viewport
										                });
										            });
										        }
										 
										        document.getElementById('go_previous').addEventListener('click', (e) => {
										            if(myState.pdf == null || myState.currentPage == 1) 
										              return;
										            myState.currentPage -= 1;
										            document.getElementById("current_page").value = myState.currentPage;
										            render();
										        });
										 
										        document.getElementById('go_next').addEventListener('click', (e) => {
										            if(myState.pdf == null || myState.currentPage > myState.pdf._pdfInfo.numPages) 
										               return;
										            myState.currentPage += 1;
										            document.getElementById("current_page").value = myState.currentPage;
										            render();
										        });
										 
										        document.getElementById('current_page').addEventListener('keypress', (e) => {
										            if(myState.pdf == null) return;
										          
										            // Get key code
										            var code = (e.keyCode ? e.keyCode : e.which);
										          
										            // If key code matches that of the Enter key
										            if(code == 13) {
										                var desiredPage = 
										                document.getElementById('current_page').valueAsNumber;
										                                  
										                if(desiredPage >= 1 && desiredPage <= myState.pdf._pdfInfo.numPages) {
										                    myState.currentPage = desiredPage;
										                    document.getElementById("current_page").value = desiredPage;
										                    render();
										                }
										            }
										        });
										 
										        document.getElementById('zoom_in').addEventListener('click', (e) => {
										            if(myState.pdf == null) return;
										            myState.zoom += 0.1;
										            render();
										        });
										 
										        document.getElementById('zoom_out').addEventListener('click', (e) => {
										            if(myState.pdf == null) return;
										            myState.zoom -= 0.1;
										            render();
										        });
										    </script>



								</div>
							</div>
						</div>
						<div class="col col-m-12 col-t-5 col-d-5">
							<div class="content-box animated">
								<div class="i_title">
									<div class="icon"><i class="icon ion ion-edit"></i></div>
									<div class="name">Answers</div>
								</div>
								<div class="skills">
									<ul style="height: 568px;overflow-y: auto;overflow-x: hidden;padding-right: 10px;">
										<li style="padding-top: 0px;">
											<table class="table" style="overflow:auto;width: auto;padding:0px;">
											<?php 

												$sql004 = mysqli_query($conn,"SELECT * FROM `mcq_trans` WHERE `PAPER_ID` = '$paper_id'"); //Get Paper Question
												while($row004 = mysqli_fetch_assoc($sql004))
												{
													$q_no = $row004['QUESTION'];
													$question = $row004['QUESTION'];
													$qa_no = $row004['CORRECT_ANSWER'];

													if($q_no < 10)
													{
														$q_no = '0'.$q_no;
													}

												echo '<tr style="font-size: 20px !important;padding-top: 0px;padding-bottom: 0px;color: #ED4C67;">

														<td colspan="10" style="border:1px solid white;padding:0px;margin:0px;">

															<label style="font-size: 20px !important;padding: 0;color: #ED4C67;">Question '.$q_no.'</label>

														</td>
															
													</tr>'; // Paper No

											echo '<tr style="padding-top:0px;padding-bottom:0px;margin:0px;">';

													for($i=1;$i<=$answer_amount;$i++)
													{
														$checked_answer = '';
														$sql002 = mysqli_query($conn,"SELECT * FROM `student_mcq_transaction` WHERE `PAPER_ID` = '$paper_id' AND `QUESTION` = '$question' AND `STU_ID` = '$student_id' AND `STU_MAIN_ID` = '$mcq_main_id'"); //Get selected radio button
											             while($row002 = mysqli_fetch_assoc($sql002))
											             {
											                $given_answer = $row002['ANSWER']; //Correct Answer

											                if($i == $given_answer)
											                {
											                	$checked_answer = 'checked';
											                }
											             }

														echo '<td style="width:1%;padding:0px;border:1px solid white;margin:0px;">
													
															<label for="ans1" style="font-size: 20px;">

															'.$i.'. <input type="radio" onchange="give_answer('.$q_no.','.$i.','.$paper_id.','.$student_id.','.$class_id.','.$question_amount.','.$finish_d_t.','.$mcq_main_id.')" '.$checked_answer.' name="qno'.$q_no.'" style="zoom:2;"></label>
																</td>'; 
															//question for answer
													}

											echo '</tr>';

												}
											 ?>
											</table>
											
										</li>

										<li style="padding-top: 10px;border-top: 1px solid #cccc;">
											<button class="btn btn-success btn-block" data-toggle="modal" data-target="#confirm_submit" style="background-color: #10ac84;font-size: 18px;border-radius: 30px;border:1px solid #10ac84;outline: none;"><i class="icon ion ion-checkmark-circled"></i> Submit</button>
										</li>
									</ul>
								</div>

											 <script type="text/javascript">

											 	function give_answer(question_no,answer_no,paper_id,student_id,class_id,question_amount,finish_d_t,mcq_main_id)
											 	{
											 		//Start Examination
											 		$.ajax({
                                                    type:"POST",
                                                    url:"../../student/query/update.php",
                                                    data : {start_exam:question_no,answer_no:answer_no,student_id:student_id,class_id:class_id,paper_id:paper_id,question_amount:question_amount,finish_d_t:finish_d_t,mcq_main_id:mcq_main_id},
                                                    success: function(data) {
                                                      
                                                      if(data == 0)
                                                      {
                                                      	Swal.fire({
														  position: 'top-middle',
														  icon: 'success',
														  title: 'ඔබට පිළිතුරු සැපයීමට ලබාදුන් කාලය අවසන්.',
														  showConfirmButton: false,
														  timer: 2000
														})
                                                      }
                                                        
                                                    }
                                                  	});
											 		//Start Examination


											 	}
											 </script>


							</div>
						</div>

					



					<button class="btn btn-success btn-lg" data-toggle="modal" data-target="#show_my_paper" id="show_paper" style="position: fixed;bottom: 110px;right: 40px;background-color: #16a085;"><i class="icon ion ion-document-text" style="font-size: 35px;"></i></button>


					</div>

					<a href="../index.php" class="btn btn-danger btn-lg" id="back" style="position: fixed;bottom: 40px;right: 40px;background-color: #ee5253;border:1px solid #ee5253;"><span class="fa fa-angle-left"></span></a>
				</div>

			</div>
			

			<div class="wrapper" style="padding-top: 0px;display: <?php echo $result_div; ?>;" id="result">

				<!-- Section Skills -->
				<div class="section skills" id="skills-section" style="padding-top: 0;">
					<div class="row">

						<div class="col col-m-12 col-t-2 col-d-2">
							<p></p>
						</div>
						<div class="col col-m-12 col-t-8 col-d-8">
							<div class="content-box animated">

								<?php 

									$sql006 = mysqli_query($conn,"SELECT * FROM `student_mcq_transaction` WHERE `PAPER_ID` = '$paper_id' AND `STU_ID` = '$student_id'"); //Get deleted given answers

						           if(mysqli_num_rows($sql006)>0)
						           {
						           		$check_delete_answer = 'show';
						           }else
						           if(mysqli_num_rows($sql006) == '0')
						           {
						           		$check_delete_answer = 'none';
						           }

								 ?>

								<div class="col-m-10" style="height: auto;" id="final_result">

									<!-- Result Sheet Using JS,AJAX -->


									<?php
									
									$sql001 = mysqli_query($conn,"SELECT * FROM `paper_master` WHERE `PAPER_ID` = '$paper_id'");
									while($row001 = mysqli_fetch_assoc($sql001))
									{
										$class_id = $row001['CLASS_ID'];
										$paper_name = $row001['PAPER_NAME'];
										
										$paper_total_question = $row001['NO_OF_QUESTIONS'];
										$total_marks_answer = $row001['ANSWER_MARKS'];
									}

										$correct = 0;
									    $correct_mark = 0;

									    $wrong = 0;

									    $invalid = 0;
										
										$my_total_marks = 0;
										$my_final_total_marks = 0;
										$final_mark = 0;

								          $sql001 = mysqli_query($conn,"SELECT * FROM `student_mcq_master` WHERE `PAPER_ID` = '$paper_id' AND `STU_ID` = '$student_id'"); //Get Correct Answer
								          while($row001 = mysqli_fetch_assoc($sql001))
								          {
								              $correct = $row001['CORRECT_ANSWERS']; //System Correct Answer
								              $wrong = $row001['WRONG_ANSWER']; //Mark
								              $question_amount = $row001['TOTAL_QUESTION']; //Student Answer
								              $correct_mark = $row001['TOTAL_MARKS']; //Student Answer
								              $spent_hour = $row001['SPEND_TIME_HOUR']; //Student Answer
								              $spent_minute = $row001['SPEND_TIME_MIN']; //Student Answer
								              $marks_answer = $row001['QUESTION_MARK']; //Student Answer
								              $mcq_master_id = $row001['STU_MCQ_MASTER_ID']; //Student Answer
											  
											  $my_total_marks = 0;
												$my_final_total_marks = 0;
												$final_mark = 0;

												$sql0033 = mysqli_query($conn,"SELECT * FROM `student_mcq_transaction` WHERE `PAPER_ID` = '$paper_id' AND `STU_ID` = '$student_id'"); //check mcq test table
												while($row0033 = mysqli_fetch_assoc($sql0033))
												{
												
													$correct_answer_no = $row0033['CORRECT_ANSWER']; //System Correct Answer
													$marks_answer = $row0033['MARKS']; //Mark
													$student_answer = $row0033['ANSWER']; //Student Answer
													$question = $row0033['QUESTION']; //Question

													$total_paper_mark = 0; //total question X Question for Mark
													
													if($correct_answer_no == $student_answer)
													{
													$my_total_marks = $my_total_marks+1;
													}

												}

								          }

										  

										  $total_paper_mark = $paper_total_question*$total_marks_answer;
                              	
										  $mcq_my_mark = $my_total_marks*$total_marks_answer; //Correct answer amount x per question for marks
											
										  $final_mark = (50*$mcq_my_mark)/$total_paper_mark;


								      echo '<div class="row">
								                    <div class="col-md-4"><a href="../index.php" style="color: gray;text-decoration: none;">Back</a></div>
								                    <div class="col-md-4">
								                      
								                      <div class="i_title">
								                        <div class="icon" style="padding-top:20px;padding-bottom:20px;width: 120px;height: 120px;">
								                          <label style="font-size: 50px;color: white;text-align:center;">'.$final_mark.' <p style="padding: 0;line-height: 0;">Marks</p></label> </div>
								                      </div>

								                    </div>
								                    <div class="col-md-4"></div>
								                  </div>

								                  <div class="row">
								                    <div class="col-md-2">


								                    </div>
								                    <div class="col-md-8">

								                      <div class="row" style="padding-bottom: 0px;">
								                        <div class="col-md-8"><h3 style="font-weight: 550;color:#7f8c8d"><span class="fa fa-question-circle"></span> Total Question</h3></div>
								                        <div class="col-md-4"><h3 style="text-shadow:  1px 1px #cccc;">'.$question_amount.'</h3></div>
								                      </div>


								                      <div class="row" style="padding-bottom: 0px;">
								                        <div class="col-md-8"><h3 style="font-weight: 550;color:#7f8c8d"><span class="fa fa-check-circle"></span> Correct Answers</h3></div>
								                        <div class="col-md-4"><h3 style="text-shadow:  1px 1px #cccc;">'.$correct.'</h3></div>
								                      </div>


								                      <div class="row" style="padding-bottom: 0px;">
								                        <div class="col-md-8"><h3 style="font-weight: 550;color:#7f8c8d"><span class="fa fa-times"></span> Wrong Answers</h3></div>
								                        <div class="col-md-4"><h3 style="text-shadow:  1px 1px #cccc;">'.$wrong.'</h3></div>
								                      </div>


								                      <div class="row" style="padding-bottom: 0px;">
								                        <div class="col-md-8"><h3 style="font-weight: 550;color:#7f8c8d"><span class="fa fa-ban"></span>  Invalid Answers</h3></div>
								                        <div class="col-md-4"><h3 style="text-shadow:  1px 1px #cccc;">'.$invalid.'</h3></div>
								                      </div>

								                      <div class="row" style="padding-bottom: 10px;">
								                        <div class="col-md-8"><h3 style="font-weight: 550;color:#7f8c8d"><span class="fa fa-bell"></span> Question For Mark</h3></div>
								                        <div class="col-md-4"><h3 style="text-shadow:  1px 1px #cccc;"> '.$marks_answer.'</h3></div>
								                      </div>

								                      <div class="row" style="padding-top: 20px;border-top: 1px solid #cccc;">

								                      	<div class="col-md-1"></div>

								                        <div class="col-md-10" style="margin-top:10px;display:'.$check_delete_answer.';">
								                          
								                          <form action="show_answer.php" method="POST">

							                                  <input type="hidden" name="student_id" value="'.$student_id.'">
							                                  <input type="hidden" name="teacher_id" value="'.$teacher_id.'">
							                                  <input type="hidden" name="class_id" value="'.$class_id.'">
							                                  <input type="hidden" name="last_attend" value="'.$last_attend.'">
							                                  <input type="hidden" name="paper_id" value="'.$paper_id.'">
							                                  <input type="hidden" name="mcq_master_id" value="'.$mcq_master_id.'">
							                                
							                                <button type="submit" class="btn btn-custom btn-block" id="attend_btn" style="font-weight: bold;color:white;"><i class="fa fa-search"></i>&nbsp;Check My Answer</button>

							                                </form>

								                        </div>
								                        

								                        <div class="col-md-1"></div>
								                      </div>
								                    </div>
								                    <div class="col-md-2"></div>
								                  </div>';

								      /* Result Sheet*/

								      /*<div class="col-md-4" style="margin-top:10px;display:none;">
								                          <button class="btn btn-success btn-block" disabled style="background-color: #2980b9;border:none;outline: none;box-shadow: 1px 1px 6px 2px #cccc;font-size: 18px;"><span class="fa fa-print"></span> Print Certificate</button>
								                        </div>*/

								      /*
											

								                        <div class="col-md-4" style="margin-top:10px;display:none;">
								                          <button class="btn btn-success btn-block" disabled style="background-color: #ee5253;border:none;outline: none;box-shadow: 1px 1px 6px 2px #cccc;font-size: 18px;" data-toggle="modal" data-target="#high_score" id="high_score_button" onclick="show_high_score('.$paper_id.','.$student_id.');"><span class="fa fa-flag-checkered"></span> High scores</button>
								                        </div>


								      */

									 ?>

								</div>
							</div>
						</div>

					</div>
				</div>

			</div>


			<!-- Submit Paper -->
			<script type="text/javascript">


			function paper_submit(paper_id,student_id,question_amount)
			{
				document.getElementById('paper').style.display="none";
				document.getElementById('result').style.display="block";



				$.ajax({
                    type:"POST",
                    url:"../../student/query/update.php",
                    data : {finish_exam:paper_id,student_id:student_id,question_amount:question_amount},
                    success: function(data) {
                      
                        document.getElementById('final_result').innerHTML = data;
                    }
                });



			}
			</script>
			<!-- Submit Paper -->

			<!-- Submit Paper -->
			<script type="text/javascript">


			function show_answer(paper_id,student_id,question_amount)
			{
				$.ajax({
                    type:"POST",
                    url:"../../student/query/check.php",
                    data : {show_answer:paper_id,student_id:student_id,question_amount:question_amount},
                    success: function(data) 
                    {
                      	alert(data)
                        document.getElementById('myTable').innerHTML = data;
                    }
                });



			}
			</script>
			<!-- Submit Paper -->

			<!-- Check High score -->
			<script type="text/javascript">


			function show_high_score(paper_id,student_id)
			{
				$.ajax({
                    type:"POST",
                    url:"../../student/query/check.php",
                    data : {show_high_score:paper_id,student_id:student_id},
                    success: function(data) {
                      	
                      	alert(data)
                        document.getElementById('high_mark').innerHTML = data;
                    }
                });



			}
			</script>
			<!-- Check High score -->
			
		</div>

	</div>
	
	<!-- jQuery Scripts -->
	<script src="js/jquery.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/jquery.validate.js"></script>
	<script src="js/magnific-popup.js"></script>
	<script src="js/masonry.pkgd.js"></script>
	<script src="js/imagesloaded.pkgd.js"></script>
	<script src="js/masonry-filter.js"></script>
	<script src="js/scrollreveal.js"></script>
	<script src="js/jquery.mb.YTPlayer.js"></script>
	<script src="js/particles.js"></script>
	<script src="js/particles-setting.js"></script>

	<!-- Google map api -->
	<script src="https://maps.google.com/maps/api/js?sensor=false"></script>
	
	<!-- Main Scripts -->
	<script src="js/main.js"></script>
	
</body>

<!-- Mirrored from beshley.com/mcard/theme_colors/purple/index-1-particles.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 26 Jan 2021 18:39:47 GMT -->
</html>


								<div id="show_my_paper" class="modal fade" role="dialog">
								  <div class="modal-dialog">

								    <!-- Modal content-->
								    <div class="modal-content">
								      <div class="modal-body">

								      	<div id="my_pdf_viewer2" oncontextmenu="return false;">
									        <div id="canvas_container2">
									            <canvas id="pdf_renderer2"></canvas>
									        </div>

									        		<div class="row">
									        			<div class="col-md-12 col-sm-12 col-xs-12" style="padding-left: 16%;">
									        				
									        				<div class="row" style="padding-top: 10px;">
											        			<div class="col-md-2 col-sm-2 col-xs-2">

											        				<button class="btn btn-primary btn-sm btn-block" id="go_previous2" style="margin-top: 4px;margin-right: 8px;background-color: #2980b9;outline: none;width: 100%;border-radius: 80px;padding: 10px 25px 10px 16px;text-align: center;"><span class="fa fa-arrow-left"></span></button>

											        			</div>
											        			<div class="col-md-2 col-sm-2 col-xs-2">

											        				<input id="current_page2" value="1" type="hidden"/>
														    		<button class="btn btn-primary btn-block" id="go_next2" style="margin-top: 4px;background-color: #2980b9;outline: none;width: 100%;border-radius: 80px;padding: 10px 25px 10px 16px;text-align: center;"><span class="fa fa-arrow-right"></span></button>

											        			</div>

											        			<div class="col-md-2 col-sm-2 col-xs-2">

											        				<button class="btn btn-danger btn-sm" id="zoom_out2" style="margin-top: 4px;outline: none;background-color: #e74c3c;width: 100%;border-radius: 80px;padding: 10px 25px 10px 16px;text-align: center;"><span class="fa fa-minus"></span></button>

											        			</div>
											        			<div class="col-md-2 col-sm-2 col-xs-2">

											        				<button class="btn btn-success btn-sm" id="zoom_in2" style="margin-top: 4px;outline: none;margin-right: 8px;background-color: #27ae60;width: 100%;border-radius: 80px;padding: 10px 25px 10px 16px;text-align: center;"><span class="fa fa-plus"></span></button>

											        			</div>
											        			<div class="col-md-2 col-sm-2 col-xs-2">

											        				<button class="btn btn-default btn-sm btn-block" data-dismiss="modal" style="outline: none;margin-top: 4px;width: 100%;padding: 10px 25px 10px 16px;border-radius: 80px;text-align: center;"><span class="fa fa-times"></span></button>

											        			</div>
											        		</div>

									        			</div>
										        		
									        		</div>

						        			</div>
						 
									    <script>
									        var myState = {
									            pdf: null,
									            currentPage: 1,
									            zoom: 1
									        }
									      
									        pdfjsLib.getDocument('../../admin/images/paper_document/<?php echo $pdf_name; ?>').then((pdf) => {
									      
									            myState.pdf = pdf;
									            render2();
									 
									        });
									 
									        function render2() {
									            myState.pdf.getPage(myState.currentPage).then((page) => {
									          
									                var canvas = document.getElementById("pdf_renderer2");
									                var ctx = canvas.getContext('2d');
									      
									                var viewport = page.getViewport(myState.zoom);
									 
									                canvas.width = viewport.width;
									                canvas.height = viewport.height;
									          
									                page.render({
									                    canvasContext: ctx,
									                    viewport: viewport
									                });
									            });
									        }
									 
									        document.getElementById('go_previous2').addEventListener('click', (e) => {
									            if(myState.pdf == null || myState.currentPage == 1) 
									              return;
									            myState.currentPage -= 1;
									            document.getElementById("current_page2").value = myState.currentPage;
									            render2();
									        });
									 
									        document.getElementById('go_next2').addEventListener('click', (e) => {
									            if(myState.pdf == null || myState.currentPage > myState.pdf._pdfInfo.numPages) 
									               return;
									            myState.currentPage += 1;
									            document.getElementById("current_page2").value = myState.currentPage;
									            render2();
									        });
									 
									        document.getElementById('current_page2').addEventListener('keypress', (e) => {
									            if(myState.pdf == null) return;
									          
									            // Get key code
									            var code = (e.keyCode ? e.keyCode : e.which);
									          
									            // If key code matches that of the Enter key
									            if(code == 13) {
									                var desiredPage = 
									                document.getElementById('current_page2').valueAsNumber;
									                                  
									                if(desiredPage >= 1 && desiredPage <= myState.pdf._pdfInfo.numPages) {
									                    myState.currentPage = desiredPage;
									                    document.getElementById("current_page2").value = desiredPage;
									                    render2();
									                }
									            }
									        });
									 
									        document.getElementById('zoom_in2').addEventListener('click', (e) => {
									            if(myState.pdf == null) return;
									            myState.zoom += 0.1;
									            render2();
									        });
									 
									        document.getElementById('zoom_out2').addEventListener('click', (e) => {
									            if(myState.pdf == null) return;
									            myState.zoom -= 0.1;
									            render2();
									        });
									    </script>

								      </div>
								    </div>

								  </div>
								</div>


													<div id="check_answer" class="modal fade" role="dialog">
													  <div class="modal-dialog modal-lg">

													    <!-- Modal content-->
													    <div class="modal-content" style="width:1000px;">
													      <div class="modal-header">
													      	
													      	<h4 class="modal-title">Check Answer</h4>
													        
													      </div>
													      <div class="modal-body">

													      <div class="row">
													      	<div class="col-md-6">
													      		
													      		<div id="my_pdf_viewer3" oncontextmenu="return false;">
													        <div id="canvas_container3" style="height:300px;overflow: auto;">
													            <canvas id="pdf_renderer3"></canvas>
													        </div>

													        <div class="row" id="ctrl_btn3">
													        	
																	<div class="col-md-12" style="padding-top: 10px;">

																		<center><input id="current_page3" readonly class="form-control" value="1" type="text" style="text-align: center;border: none;outline: none;width:60px;background-color: #ffffff00;font-weight: 700;" /></center>
																	</div>
																	<div class="row">
									        			<div class="col-md-12 col-sm-12 col-xs-12" style="padding-left: 16%;">
									        				
									        				<div class="row" style="padding-top: 10px;">
											        			<div class="col-md-2 col-sm-2 col-xs-2">

											        				<button class="btn btn-primary btn-sm btn-block" id="go_previous3" style="margin-top: 4px;margin-right: 8px;background-color: #2980b9;outline: none;width: 100%;border-radius: 80px;padding: 12px 26px 12px 16px;text-align: center;"><span class="fa fa-arrow-left"></span></button>

											        			</div>
											        			<div class="col-md-2 col-sm-2 col-xs-2">

														    		<button class="btn btn-primary btn-block" id="go_next3" style="margin-top: 4px;background-color: #2980b9;outline: none;width: 100%;border-radius: 80px;text-align: center;"><span class="fa fa-arrow-right"></span></button>

											        			</div>

											        			<div class="col-md-2 col-sm-2 col-xs-2">

											        				<button class="btn btn-danger btn-sm" id="zoom_out3" style="margin-top: 4px;outline: none;background-color: #e74c3c;width: 100%;border-radius: 80px;text-align: center;"><span class="fa fa-minus"></span></button>

											        			</div>
											        			<div class="col-md-2 col-sm-2 col-xs-2">

											        				<button class="btn btn-success btn-sm" id="zoom_in3" style="margin-top: 4px;outline: none;margin-right: 8px;background-color: #27ae60;width: 100%;border-radius: 80px;text-align: center;"><span class="fa fa-plus"></span></button>

											        			</div>
											        			<div class="col-md-2 col-sm-2 col-xs-2">

											        				<button class="btn btn-default btn-sm btn-block" data-dismiss="modal" style="outline: none;margin-top: 4px;width: 100%;border-radius: 80px;text-align: center;"><span class="fa fa-times"></span></button>

											        			</div>
											        		</div>

									        			</div>
										        		
									        		</div>
											        			
														        
													    	</div>
										        		</div>
															    <script>
															        var myState = {
															            pdf: null,
															            currentPage: 1,
															            zoom: 1
															        }
															      
															        pdfjsLib.getDocument('../../admin/images/paper_document/<?php echo $pdf_name; ?>').then((pdf) => {
															      
															            myState.pdf = pdf;
															            render3();
															 
															        });
															 
															        function render3() {
															            myState.pdf.getPage(myState.currentPage).then((page) => {
															          
															                var canvas = document.getElementById("pdf_renderer3");
															                var ctx = canvas.getContext('2d');
															      
															                var viewport = page.getViewport(myState.zoom);
															 
															                canvas.width = viewport.width;
															                canvas.height = viewport.height;
															          
															                page.render({
															                    canvasContext: ctx,
															                    viewport: viewport
															                });
															            });
															        }
															 
															        document.getElementById('go_previous3').addEventListener('click', (e) => {
															            if(myState.pdf == null || myState.currentPage == 1) 
															              return;
															            myState.currentPage -= 1;
															            document.getElementById("current_page3").value = myState.currentPage;
															            render3();
															        });
															 
															        document.getElementById('go_next3').addEventListener('click', (e) => {
															            if(myState.pdf == null || myState.currentPage > myState.pdf._pdfInfo.numPages) 
															               return;
															            myState.currentPage += 1;
															            document.getElementById("current_page3").value = myState.currentPage;
															            render3();
															        });
															 
															        document.getElementById('current_page3').addEventListener('keypress', (e) => {
															            if(myState.pdf == null) return;
															          
															            // Get key code
															            var code = (e.keyCode ? e.keyCode : e.which);
															          
															            // If key code matches that of the Enter key
															            if(code == 13) {
															                var desiredPage = 
															                document.getElementById('current_page3').valueAsNumber;
															                                  
															                if(desiredPage >= 1 && desiredPage <= myState.pdf._pdfInfo.numPages) {
															                    myState.currentPage = desiredPage;
															                    document.getElementById("current_page3").value = desiredPage;
															                    render3();
															                }
															            }
															        });
															 
															        document.getElementById('zoom_in3').addEventListener('click', (e) => {
															            if(myState.pdf == null) return;
															            myState.zoom += 0.1;
															            render3();
															        });
															 
															        document.getElementById('zoom_out3').addEventListener('click', (e) => {
															            if(myState.pdf == null) return;
															            myState.zoom -= 0.1;
															            render3();
															        });
															    </script>

													      	</div>
													      	<div class="col-md-6">

													      <div class="row" style="padding-top: 0px;padding-bottom: 0px;">
													      	<!--<div class="col-md-3"> <a href="#" class="btn btn-success" onclick="printDiv('printableArea')" style="border-radius: 80px;"><span class="fa fa-print"></span></a></div>
													      	<div class="col-md-3"></div> -->
													      	<div class="col-md-12" style="padding-bottom: 0px;padding-top: 0px;">
													      		<input class="form-control" id="myInput" type="text" placeholder="Search..">
													      	</div>
													      </div>
													      <div class="table-responsive" style="height: 300px;overflow: auto;" id="printableArea">
														  <table class="table table-bordered table-striped">
														    <thead>
														      <tr>
														        <th style="text-align: center;"><span class="fa fa-question-circle"></span> Question No</th>
														        <th style="text-align: center;"><span class="fa fa-user"></span> Your Answer</th>
														        <th style="text-align: center;"><span class="fa fa-check-circle"></span> Correct Answer</th>
														        <th style="text-align: center;"><span class="fa fa-flag-checkered"></span> Correction</th>
														      </tr>
														    </thead>
														    <tbody id="myTable">
														      
														    </tbody>

														    <thead>
														      <tr>
														        <th style="text-align: center;"><span class="fa fa-question-circle"></span> Question No</th>
														        <th style="text-align: center;"><span class="fa fa-user"></span> Your Answer</th>
														        <th style="text-align: center;"><span class="fa fa-check-circle"></span> Correct Answer</th>
														        <th style="text-align: center;"><span class="fa fa-flag-checkered"></span> Correction</th>
														      </tr>
														    </thead>


														  </table>
														  </div>
														</div>
													</div>

													      </div>
													      <div class="modal-footer">
													        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
													      </div>
													    </div>

													  </div>
													</div>

													<div id="high_score" class="modal fade" role="dialog">
													  <div class="modal-dialog">

													    <!-- Modal content-->
													    <div class="modal-content">
													      <div class="modal-header">
													      	
													      	<h4 class="modal-title">High Scores</h4>
													        
													      </div>
													      <div class="modal-body">

													      		<input class="form-control" id="myInput2" type="text" placeholder="Search..">
													      	
													      <div class="table-responsive" style="height: 300px;overflow: auto;padding-top: 10px;">
														  <table class="table table-bordered table-striped">
														    <thead>
														      <tr>
														        <th style="text-align: center;"><span class="fa fa-arrow-up"></span> Rank</th>
														        <th style="text-align: center;"><span class="fa fa-user"></span> Student Name</th>
														        <th style="text-align: center;"><span class="fa fa-check-circle"></span> Marks</th>
														      </tr>
														    </thead>

														      

														    <tbody id="high_mark">
														    	<?php 

														    		$i = 0;

															          $sql001 = mysqli_query($conn,"SELECT * FROM `student_mcq_master` WHERE `PAPER_ID` = '$paper_id'"); //Get Correct Answer

															          while($row001 = mysqli_fetch_assoc($sql001))
															          {
															              $i = $i+1;
															              $total_marks = $row001['TOTAL_MARKS']; //Total Mark
															              $student_id002 = $row001['STU_ID']; //Student ID

															            $sql002 = mysqli_query($conn,"SELECT * FROM `student_details` WHERE `STU_ID` = '$student_id002' "); //Institute Data assign to variables
															            while($row002 = mysqli_fetch_assoc($sql002))
															            {

															                $student_name = $row002['F_NAME']." ".$row002['L_NAME']; // **Institute Free Period**

															            }

															            echo '<tr>
															                    <td style="text-align: center;">'.$total_marks.'</td>
															                    <td style="text-align: center;">'.$i.'</td>
															                    <td style="text-align: center;">'.$student_name.'</td>
															                  </tr>';

															          }

														    	 ?>
														    </tbody>
														  </table>
														  </div>

													      </div>
													      <div class="modal-footer">
													        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
													      </div>
													    </div>

													  </div>
													</div>



			<script>
			$(document).ready(function(){
			  $("#myInput").on("keyup", function() {
			    var value = $(this).val().toLowerCase();
			    $("#myTable tr").filter(function() {
			      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
			    });
			  });
			});
			</script>

			<script>
			$(document).ready(function(){
			  $("#myInput2").on("keyup", function() {
			    var value = $(this).val().toLowerCase();
			    $("#high_mark tr").filter(function() {
			      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
			    });
			  });
			});
			</script>


			<script type="text/javascript">
				
				//document.getElementById("answer_button").disabled = true; //Disable Answer Button until time out

				document.getElementById("high_score_button").disabled = true; //Disable Answer Button until time out

				function printDiv(divName) {
				     var printContents = document.getElementById(divName).innerHTML;
				     var originalContents = document.body.innerHTML;

				     document.body.innerHTML = printContents;

				     window.print();

				     document.body.innerHTML = originalContents;
				}

			</script>

			<!-- Confirm submit Modal -->

			<div id="confirm_submit" class="modal fade" role="dialog">
			  <div class="modal-dialog" style="margin-top: 100px;">

			    <!-- Modal content-->
			    <div class="modal-content modal-sm">
			      <div class="modal-header">
			      	
			      	<h4 class="modal-title">Confirmation</h4>
			        
			      </div>
			      <div class="modal-body">

			      <div class="row" style="padding-top: 0px;padding-bottom: 0px;">
			      	<!--<div class="col-md-3"> <a href="#" class="btn btn-success" onclick="printDiv('printableArea')" style="border-radius: 80px;"><span class="fa fa-print"></span></a></div>
			      	<div class="col-md-3"></div> -->
			      	<div class="col-md-12" style="padding-bottom: 0px;padding-top: 0px;">
			      		
			      		<center>
			      			<span class="fa fa-check-circle" style="font-size: 80px;color: #10ac84;"></span>
			      			<h2>Are you sure submit your answers?</h2>

			      		</center>
			      		<br>
			      		<center>
			      			<button type="button" class="btn btn-success btn-block" style="background-color: #10ac84;border: 1px solid #10ac84;" onclick="paper_submit(<?php echo $paper_id; ?>,<?php echo $student_id; ?>,<?php echo $question_amount; ?>);" data-dismiss="modal"><span class="fa fa-check-circle"></span> Yes</button>

			        		<button type="button" class="btn btn-default btn-block" data-dismiss="modal"><span class="fa fa-times-circle"></span> No</button>

			      		</center>

			      	</div>
			      </div>

				  </div>
			      <div class="modal-footer">
			        
			      </div>
			    </div>
			</div>


			<!-- Confirm submit Modal -->