
<?php

require '../connect/connect.php';
session_start();
$msg = '';

  

if(!empty($_GET['update_account']))
{   
    $account_id = $_GET['update_account'];

    $ssql01 = mysqli_query($conn,"SELECT * FROM `zoom_accounts` WHERE `id` = '$account_id'");
    while($row001 = mysqli_fetch_assoc($ssql01))
    {
        $account_email = $row001['email_address'];
        $account_name = $row001['zoom_name'];
        $account_api_key = $row001['api_key'];
        $account_api_secret = $row001['api_secret'];
    }
}else
{
    $account_email = '';
    $account_name = '';
    $account_api_key = '';
    $account_api_secret = '';
}

?>
<!doctype HTML>
<html lang="en">
<head>
    <meta charset="UTF-8">

    <!-- viewport meta -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="DigiPro - Digital Products Marketplace ">
    <meta name="keywords" content="marketplace, easy digital download, digital product, digital, html5">
    <title>Zoom Settings</title>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200&display=swap" rel="stylesheet">


</head>
<body class="preload" style="font-family: 'Poppins', sans-serif;font-weight: bold;">
    <section class="hero-area4 bgimage">
        <div class="hero-content content_above" style="padding: 0px 0 0 0 ">
            <div class="content-wrapper">

                <div class="container-fluid" style="margin: 40px 10px 10px 10px;">
                    
                    <a href="profile_setting.php" class="btn btn-danger" style="margin-bottom:10px;"><span class="fa fa-angle-left"></span> Back</a>

                    <?php 
                        if(!empty($_SESSION['alert']))
                        {
                            echo '<div class="alert text-white col-md-2" role="alert" style="position: fixed;bottom: 15px;right: 0px;background-color:#34495e">

                                    <div class="row">
                                      <div class="col-md-2 col-sm-2">
                                        <div class="iq-alert-icon">
                                           <i class="fa fa-check" style="font-size:26px;color:white"></i>
                                        </div>
                                      </div>
                                      <div class="col-md-10 col-sm-10">

                                        <div class="iq-alert-text" style="font-size: 18px;font-weight: bold;text-transform:capitalize;color:white;">'.$_SESSION['alert'].'</div>
                                            <button type="button" class="close cls" data-dismiss="alert" aria-label="Close" style="position:absolute;top:8px;right:10px;">
                                                <i class="ri-close-line" style="color:white;"></i>
                                            </button>
                                        </div>

                                      </div>
                                    </div>';

                            unset($_SESSION['alert']);
                        }

                    /* Update Message*/


                     ?>
                     <script type="text/javascript">
                        //Close Message after timeout

                            setTimeout(function(){ 

                            document.getElementsByClassName('cls')[0].click(); //Save Message


                            }, 1500); //after function execute 1 second

                            //setTimeout(function(){ document.getElementById('desc_order').click(); }, 50);

                        //Close Message after timeout
                    </script>

                    <div class="row">
                        <div class="col-lg-3 col-md-12">
                            
                            <form action="../admin/query/insert.php" method="post" enctype="multipart/form-data">
                                <div class="cardify signup_form" style="background: white; border-radius: 10px; padding: 20px;border:1px solid #cccc;">
                                <!-- end .login_header -->
                                    <div class="section-title">
                                        <h3 style="margin-top: 0px;text-align: left;font-weight: bold;margin-bottom: 20px;">Manage Zoom Accounts </h3>
                                    </div>
                                    <div class="login--form">

                                        <div class="form-group">
                                            <label>Email Address</label>
                                            <input type="email" placeholder="Email Address" name="email_address" required="" class="form-control" value="<?php echo $account_email; ?>">
                                        </div>

                                        <div class="form-group">
                                            <label>Zoom Name</label>
                                            <input type="text" placeholder="Name" name="zoom_name" required="" class="form-control" value="<?php echo $account_name; ?>">
                                        </div>

                                        <div class="form-group">
                                            <label>API Key</label>
                                            <input type="text" placeholder="API Key" name="api_key" required="" class="form-control" value="<?php echo $account_api_key; ?>">
                                        </div>

                                        <div class="form-group">
                                            <label>API Secret</label>
                                            <input type="text"  placeholder="API Secret" name="api_secret" required="" class="form-control" value="<?php echo $account_api_secret; ?>">
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-12 mt-30">

                                                <?php   


                                                    if(!empty($_GET['update_account']))
                                                    {   
                                                        $zoom_account_id = $_GET['update_account'];

                                                        echo '<button class="btn btn-success btn-block" name="update_zoom_account" style="font-weight:bold;" value="'.$zoom_account_id.'"><span class="fa fa-edit"></span> Update Now</button>';

                                                        echo '<a href="admin_zoom.php" class="btn btn-link btn-block" style="font-weight:bold;color:#e74c3c;text-decoration:none;"><span class="fa fa-check-circle"></span> Create New</a>';

                                                    }else
                                                    {
                                                        echo '<button class="btn btn-success btn-block" name="save_zoom_account" style="font-weight:bold;"><span class="fa fa-check-circle"></span> Save Now</button>';
                                                    }


                                                 ?>
                                                
                                            </div>
                                        </div>
                                    </div><!-- end .login--form -->
                                </div><!-- end .cardify -->
                            </form><!-- Ends: .product-single -->

                            <?php echo $msg; ?>

                        </div>
                                
                        <div class="col-lg-9" style="background: white; border-radius: 10px; padding: 20px;border:1px solid #cccc;">

                            <!-- List Account -->
                            <h3 style="font-weight: bold;margin-bottom: 20px;">List Zoom Account</h3>

                            <div class="row">
                  

                                <div class="col-md-12">
                                                        
                                    <div class="table-responsive" style="height: 305px;overflow: auto;">
                                        <table class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th>Email</th>
                                                <th>Name</th>
                                                <th>API Key</th>
                                                <th>API Secret</th>
                                                <th colspan="3">Action</th>
                                            </tr>
                                            </thead>

                                            <tbody>

                                            <?php   

                                                    $result_cat001 = mysqli_query($conn,"SELECT * FROM zoom_accounts ORDER by id DESC");

                                                    $ch = mysqli_num_rows($result_cat001);

                                                    if($ch > 0)
                                                    {
                                                    while($pra_cat001 = mysqli_fetch_assoc($result_cat001))
                                                    { 

                                                        if($pra_cat001['active'] == '1')
                                                        {
                                                            $tip = '<small class="label label-success"> Activated</small>';
                                                        }else
                                                        if($pra_cat001['active'] == '0')
                                                        {
                                                            $tip = '<small class="label label-danger"> Deactivated</small>';
                                                        }
                                                        ?>     

                                                        <tr>
                                                               
                                                            
                                                            <td><?php echo $pra_cat001['email_address']; ?> <br> <?php echo $tip; ?></td>
                                                            <td><?php echo $pra_cat001['zoom_name']; ?></td>
                                                            <td><?php echo $pra_cat001['api_key']; ?></td>
                                                            <td><?php echo $pra_cat001['api_secret']; ?></td>
                                                           
                                                            <td colspan="3">

                                                                <!-- Update -->
                                                                <a href="admin_zoom.php?update_account=<?php echo $pra_cat001['id']; ?>" class="btn btn-primary btn-sm" style="border-radius: 80px;"><span class="fa fa-pencil"></span></a> 
                                                                <!-- Update -->

                                                                <?php 

                                                                    if($pra_cat001['active'] == '1')
                                                                    {?>

                                                                        <!-- Current Activated but show Deactive btn -->
                                                                        <a href="../admin/query/insert.php?zoom_account_status=<?php echo $pra_cat001['id']; ?>&&active=0" onclick="return confirm('Are you sure deactive?')" class="btn btn-warning btn-sm" style="border-radius: 80px;"><span class="fa fa-times"></span></a> 
                                                                        <!-- Current Activated but show Deactive btn -->
                                                                    
                                                                <?php 
                                                                    }

                                                                 ?>

                                                                  <?php 

                                                                    if($pra_cat001['active'] == '0')
                                                                    {?>

                                                                        <!-- Current Activated but show Deactive btn -->
                                                                        <a href="../admin/query/insert.php?zoom_account_status=<?php echo $pra_cat001['id']; ?>&&active=1" onclick="return confirm('Are you sure active?')" class="btn btn-success btn-sm" style="border-radius: 80px;"><span class="fa fa-check"></span></a> 
                                                                        <!-- Current Activated but show Deactive btn -->
                                                                    
                                                                <?php 
                                                                    }

                                                                 ?>

                                                                <!-- Delete -->
                                                                <a href="../admin/query/insert.php?delete_zoom_account=<?php echo $pra_cat001['id']; ?>" onclick="return confirm('Are you sure delete?')" class="btn btn-danger btn-sm" style="border-radius: 80px;"><span class="fa fa-trash"></span></a> 
                                                                <!-- Delete -->

                                                            </td> 
                                                        </tr>

                                            <?php   }
                                                }else
                                                if($ch == '0')
                                                {
                                                    echo '<tr><td colspan="5" class="text-danger text-center"><i class="fa fa-warning"></i> Not Found Data!</td></tr>';
                                                }
                                             ?>


                                            </tbody>
                                        </table>

                                    </div>

                                </div>

                            </div>

                            <!-- List Account -->
                        </div>
                    </div>

                

                </div>
            </div>
        </div><!-- end hero-content -->
    </section><!-- ends: .hero-area -->

</body>
</html>