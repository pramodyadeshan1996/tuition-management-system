<?php

include('../connect/connect.php');
$student_id = $_GET['student_id'];

?>
<!DOCTYPE html>
<html>
<head>
<title>Paper Marking</title>
<!-- Latest compiled and minified CSS -->
<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>

<div class="container-fluid" style="padding:10px 30px 10px 30px;">

<a href="index.php" class="btn btn-info btn-sm"><span class="fa fa-angle-double-left"></span> Back</a>

<h5 class="text-muted" style="margin-top:15px;"><span class="fa fa-download"></span> Download Answersheet</h5>
<div class="row" style="padding-left:15px;">
  <?php

$strto = strtotime('-2 week');
$st_date = date('Y-m-d',$strto); //Today from 2 week

$strfrm = strtotime('+2 week');
$en_date = date('Y-m-d',$strfrm); //Today from 2 week

  $sql3 = mysqli_query($conn,"SELECT * FROM `paper_mark` WHERE `ADD_DATE` BETWEEN '$st_date' AND '$en_date' AND `STU_ID` = '$student_id' ORDER BY `ADD_TIME` DESC");
  $check_upload = mysqli_num_rows($sql3);

  if($check_upload > 0)
  {
    $sql = mysqli_query($conn,"SELECT * FROM `student_essay_master` WHERE `STU_ID` = '$student_id'");
    while($row = mysqli_fetch_assoc($sql))
    {
      $paper_id = $row['PAPER_ID'];
    }

      $sql2 = mysqli_query($conn,"SELECT * FROM `paper_master` WHERE `PAPER_ID` = '$paper_id'");
      while($row2 = mysqli_fetch_assoc($sql2))
      {
        $paper_name = $row2['PAPER_NAME'];
        $clz_id = $row2['CLASS_ID'];
        $start_dt = $row2['START_D_T'];
        $finish_dt = $row2['FINISH_D_T'];

        $exam_start_time_str = strtotime($start_dt);
        $exam_start_time = date('Y-m-d h:i A',$exam_start_time_str);

        $exam_end_time_str = strtotime($finish_dt);
        $exam_end_time = date('Y-m-d h:i A',$exam_end_time_str);
      }

      $sql002 = mysqli_query($conn,"SELECT * FROM `classes` WHERE `CLASS_ID` = '$clz_id'");
      while($row002 = mysqli_fetch_assoc($sql002))
      {
        $clz_name = $row002['CLASS_NAME'];
        $teach_id = $row002['TEACH_ID'];
      }

      $sql0024 = mysqli_query($conn,"SELECT * FROM `teacher_details` WHERE `TEACH_ID` = '$teach_id'");
      while($row0024 = mysqli_fetch_assoc($sql0024))
      {
        $f_name = $row0024['F_NAME'];
        $l_name = $row0024['L_NAME'];
        $posi = $row0024['POSITION'];

        $teacher_name = $posi.'. '.$f_name.' '.$l_name;
      }

      while($row3 = mysqli_fetch_assoc($sql3))
      {
        $pdf = $row3['FILE_NAME'];
      
        ?>

        <div class="card col-md-3" style="margin-top:20px;margin-left:15px;">

        <div class="card-body">
          <h4 class="card-title"><span class="fa fa-check-circle"></span> <?php echo $paper_name; ?></h4>
          <small class="text-muted" style="font-weight:bold;">
            Class Name - <?php echo $clz_name; ?>
            <br>
            Teacher Name - <?php echo $teacher_name; ?>
          </small>
          <br>
          <small style="font-size:16px;font-weight:bold;">
            Start Time - <?php echo $exam_start_time; ?>
          </small>
          <br>
          <small style="font-size:16px;font-weight:bold;">
            End Time - <?php echo $exam_end_time; ?>
          </small>
          <br>
          <a href="../paper_marker/<?php echo $pdf; ?>" download class="btn btn-danger btn-block" style="margin-top:10px;"><i class="fa fa-download"></i> Download</a>
        </div>
        </div>

        <?php

    }
  }else
  if($check_upload == '0')
  {
    echo '<div class="col-md-12 alert alert-danger" style="margin-top:20px;text-align:center;"><span class="fa fa-warning"></span> Not Found Data!</div>';
  }

  ?>
</div>

	
</div>
</body>
</html>