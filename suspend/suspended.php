<!DOCTYPE html>
<html>
<head>
	<title>Suspended</title>
	<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link href="https://fonts.googleapis.com/css?family=Muli&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body style="font-family: 'Muli', sans-serif;">
	<div class="container" style="padding-top: 10%;">
	<center>

		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-4"><img src="../../suspend/suspend2.svg" style="width: 300px; height: 300px;"></div>
			<div class="col-md-4"></div>
		</div>

		<div class="col-md-12">
		<h1 style="font-weight: bold;color: #FB4B4B;">Your Account has been suspended. <br><small style="font-size: 20px;">Please contact System Administrator.</small></h1>
		</div>
	</center></div>

</body>
</html>