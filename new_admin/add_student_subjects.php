
<?php

$page = "Class Registeration";
$folder_in = '3';

  include('header/header.php'); 
 ini_set( "display_errors", 0); 

 $admin_id = $_SESSION['ADMIN_ID'];

  ?>
  <style type="text/css">
    .btn_pulse {
  
  display: block;
  border-radius: 10%;
  cursor: pointer;
  animation: none;
  float: left;
  bottom: 5px;
  right: 5px;
  font-weight: bold;
  padding-top: 2px;
  text-align: center;
  color: white;
  font-size: 14px;
  animation: pulse 1.6s infinite;
}


@keyframes pulse {
  0% {
    -moz-box-shadow: 0 0 0 0 #7252d3;
    box-shadow: 0 0 0 0 #7252d3;
  }
  70% {
    -moz-box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
  }
  100% {
    -moz-box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
  }
}
  </style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<div class="row">
  <div class="col-md-9">
    
    <ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-bottom: 04%;margin-bottom: 0;">
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="Class Registeration"> Class Registeration</li>
    </ol>


  </div>
  <div class="col-md-3">
    <div class="form-group form-group-default" style="margin-top: 0px;">

    <form action="add_student_subjects.php" method="GET">
        <input type="text" name="reg_id" id="student" required class="form-control" placeholder="Type Student ID | Name" onchange="document.getElementById('student_search_btn').click();" value="<?php echo $_GET['reg_id']; ?>">

       
        <button type="submit" id="student_search_btn" style="display: none;">X</button>
      </form>
    </div>
    <?php 
    $register_id = $_GET['reg_id'];
    $sql008 = mysqli_query($conn,"SELECT * FROM `stu_login` WHERE `REGISTER_ID` = '$register_id'");

    $check01 = mysqli_num_rows($sql008);

    if($check01 == '0')
    {
      $disable = 'disabled';
      echo '<h5 class="text-danger"> <span class="fa fa-warning"></span> Please select student Register ID</h5>';
    }else
    if($check01 > 0)
    {
      $disable = '';
      echo '';

      while($row008 = mysqli_fetch_assoc($sql008))
      {
        $student_id001 = $row008['STU_ID'];
      }
    }


    if(empty($_SESSION['register_student_click_tab']))
    {
      $add_subject = 'active';
      $show_class = '';
    }else
    if(!empty($_SESSION['register_student_click_tab']))
    {
      if($_SESSION['register_student_click_tab'] == 'add')
      {
        $add_subject = 'active';
        $show_class = '';
      }else
      if($_SESSION['register_student_click_tab'] == 'registered')
      {
        $add_subject = '';
        $show_class = 'active';
      }
    }
    

  ?>


  </div>
</div>

  

<ul class="nav nav-tabs nav-tabs-fillup" data-init-reponsive-tabs="dropdownfx">

  <li class="nav-item">
  <a href="#" data-toggle="tab" onclick="register_student_click_tab('add');" class="<?php echo $add_subject; ?>" data-target="#slide1"><span><i class="fa fa-plus"></i> Add Classes</span></a>
  </li>

  <li class="nav-item">
  <a href="#" data-toggle="tab" onclick="register_student_click_tab('registered'); location.reload();" class="<?php echo $show_class; ?>" data-target="#slide2"><span><i class="fa fa-search"></i> Registered Classes</span></a>
  </li>

</ul>

 <script type="text/javascript">
    function register_student_click_tab(tab_name)
    {
          
          $.ajax({  
          url:"../admin/query/check.php",  
          method:"POST",  
          data:{register_student_click_tab:tab_name},  
          success:function(data){ 
             
             //alert(data)
           }           
         });

    }
 </script>

<div class="tab-content">
<div class="tab-pane slide-left <?php echo $add_subject; ?>" id="slide1">

              <div class="row">
                <div class="col-md-4">

                    
                    
                      <input type="hidden" id="st_id" value="<?php echo $student_id001; ?>">
                    <div class="form-group form-group-default" style="margin-top: 10px;">
                      <label>Level Name</label>
                      

                      <select class="form-control"  id="find_level_name" <?php echo $disable; ?> name="level" style="cursor: pointer;" onchange="find_subject()">
                        <option value="">Select Level Name </option>

                        <?php 

                            $sql0015 = mysqli_query($conn,"SELECT * FROM level");
                            while($row0015=mysqli_fetch_assoc($sql0015))
                            {
                              $level_name = $row0015['LEVEL_NAME'];
                              $level_id = $row0015['LEVEL_ID'];

                              echo '<option value='.$level_id.'>'.$level_name.'</option>';

                            }

                             ?>
                        </select>

                    </div>



                </div>
                <div class="col-md-4">
                    
                    <div class="form-group form-group-default" style="margin-top: 10px;">
                  <label>Subject</label>
                  <select class="form-control"  id="subject" <?php echo $disable; ?> name="subject" required style="cursor: pointer;" onchange="find_teacher()">
                    <option value="">Select Subject Name</option>
                  </select>
                </div>

                </div>


                <div class="col-md-4" style="padding-top: 10px;">
                  
                  <button type="submit" class="btn btn-primary btn-block btn-lg" <?php echo $disable; ?> onclick="find_teacher()" style="padding: 12px 10px 12px 10px;font-size: 18px;" name="search_btn" id="search_btn"><i class="pg-icon" style="font-size: 25px;">search</i> Search</button>
                
                 

               </div>
              </div>

            
      
        
        <div id="accordion" style="height: 600px;overflow: auto;margin-top: 10px;border-top: 1px solid #cccc;">

          <div id="my_teachers">
            
            <div class="col-md-12 text-center" style="padding: 10px 10px 10px 10px;margin-top: 60px;opacity: 0.6;">
              <img src="images/not_found_data.svg" style="width: 300px;height: 300px;">
              <h4 class="text-muted"><span class="fa fa-warning"></span> Not found data!</h4>
            </div>

          </div>

          <script type="text/javascript">
            function add_student(class_id,teach_id,st_id)
            {
              $.ajax({
                url:'../admin/query/insert.php',
                method:"POST",
                data:{reg_class_student_subject:st_id,clz_id:class_id,teach_id:teach_id},
                success:function(data)
                {
                    Swal.fire({
                      position: 'top-middle',
                      icon: 'success',
                      title: 'Registered Success!',
                      showConfirmButton: false,
                      timer: 500
                    })
                  
                }
               });
            }
          </script>

        </div>

      </div>


<script type="text/javascript">
    
      function find_subject()
      {

       var level_clz = document.getElementById('find_level_name').value;
       //alert(level_clz);

       $.ajax({
        url:'../admin/query/check.php',
        method:"POST",
        data:{check_level_subject:level_clz},
        success:function(data)
        {
            //alert(data)
            document.getElementById('subject').innerHTML = data;
          
        }
       });
     }

     function find_teacher()
      {

       var level_clz = document.getElementById('find_level_name').value;
       var st_id = document.getElementById('st_id').value;
       var subject = document.getElementById('subject').value;
       //alert(teach_id);

       $.ajax({
        url:'../admin/query/check.php',
        method:"POST",
        data:{find_teachers:level_clz,subject:subject,st_id:st_id},
        success:function(data)
        {
            //alert(data)
            document.getElementById('my_teachers').innerHTML = data;
          
        }
       });
     }



  </script>



<div class="tab-pane slide-left <?php echo $show_class; ?>" id="slide2">
      <div class=" container-fluid container-fixed-lg bg-white" style="margin-top: 0px;">
        <div class="card card-transparent">
          <div class="card-header ">
                <div class="card-title">
                </div>
                <div>
                  <div class="row">
                    <div class="col-md-6">
                    </div>
                  <div class="col-md-2"></div>
                  <div class="col-md-4">
                  <div class="col-md-12" style="padding-top: 18px;"></div>
                  <input type="text" id="search-table" class="form-control pull-right" placeholder="Search">
                  </div>

                </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="card-body">
            <table class="table table-hover demo-table-search table-responsive-block" id="tableWithSearch">
              <thead>
                  <tr>
                    <th style="width: 12%;" class="text-danger">Added Date</th>
                    <th style="width: 1%;">Time</th>
                    <th style="width: 1%;">Subject</th>
                    <th style="width: 1%;">Teacher Name</th>
                    <th style="width: 1%;">Class Name</th>
                    <th style="width: 1%;text-align: center;">Action</th>
                  </tr>
                </thead>

                  <?php 
                  if(isset($_GET['reg_id']))
                  {
                      $student_id = $_GET['reg_id'];
                      $sql005 = mysqli_query($conn,"SELECT * FROM `stu_login` WHERE `REGISTER_ID` = '$student_id'");
                      while($row005 = mysqli_fetch_assoc($sql005))
                      {
                          $stu_id = $row005['STU_ID'];
                      }

                      $_SESSION['student_id'] = $student_id;

                      $sql002 = mysqli_query($conn,"SELECT * FROM `transactions` WHERE `STU_ID` = '$stu_id' ORDER BY `UPDATE_TIME` DESC");
                      if(mysqli_num_rows($sql002)>0)
                      {
                          while($row003 = mysqli_fetch_assoc($sql002))
                          {
                              
                              $clz_id = $row003['CLASS_ID'];
                              $teach_id = $row003['TEACH_ID'];
                              $added_date = $row003['UPDATE_TIME'];

                              $sql004 = mysqli_query($conn,"SELECT * FROM `classes` WHERE `CLASS_ID` = '$clz_id' AND `TEACH_ID` = '$teach_id'");
                              while($row004 = mysqli_fetch_assoc($sql004))
                              {
                                      $class_id = $row004['CLASS_ID'];
                                      $clz_name = $row004['CLASS_NAME'];
                                      $sub_id = $row004['SUB_ID'];
                                      $day = $row004['DAY'];
                                      $fees = $row004['FEES'];


                                      $strt = strtotime($row004['START_TIME']);
                                      $start_time = date('h:i A',$strt);


                                      $end = strtotime($row004['END_TIME']);
                                      $end_time = date('h:i A',$end);

                                      $class_time = $start_time." - ".$end_time;




                              }

                              $sql005 = mysqli_query($conn,"SELECT * FROM `subject` WHERE `SUB_ID` = '$sub_id'");
                              while($row005 = mysqli_fetch_assoc($sql005))
                              {
                                      $sub_name = $row005['SUBJECT_NAME'];
                                      $level_id = $row005['LEVEL_ID'];
                              }

                              $sql006 = mysqli_query($conn,"SELECT * FROM `level` WHERE `LEVEL_ID` = '$level_id'");
                              while($row006 = mysqli_fetch_assoc($sql006))
                              {
                                      $level_name = $row006['LEVEL_NAME'];
                              }

                              $sql007 = mysqli_query($conn,"SELECT * FROM `teacher_details` WHERE `TEACH_ID` = '$teach_id'");
                              while($row007 = mysqli_fetch_assoc($sql007))
                              {
                                      $teacher_name = $row007['POSITION'].". ".$row007['F_NAME']." ".$row007['L_NAME'];
                              }

                              echo '<tr>
                                  <td class="v-align-middle">'.$added_date.'</td>
                                  <td class="v-align-middle">'.$class_time.'<br> <small>('.$day.')</small></td>
                                  <td class="v-align-middle">'.$sub_name.'<br> <small>'.$level_name.'</small></td>
                                  <td class="v-align-middle">'.$teacher_name.'<br> <small class="text-danger"><b>LKR '.number_format($fees,2).'</b></small></td>
                                  <td class="v-align-middle">'.$clz_name.'</td>';
                                  ?>
                                  <td class="v-align-middle text-center"><a href="../admin/query/delete.php?delete_reg_class=<?php echo $class_id; ?>&&stu_id=<?php echo $stu_id; ?>&&teach_id=<?php echo $teach_id; ?>&&student_regcode=<?php echo $student_id; ?>" onclick="return confirm('Are you sure remove subject?')" class="btn btn-danger"><i class="pg-icon">close</i> Unregister</a></td>
                                  <?php echo '
                              </tr>';

                              
                          }
                      }
                  }
                 ?>
                
                <tbody>
                </tbody>
            </table>
          </div>
        </div>
        </div>
</div>
</div>

</div>
</div>
</div>

<script type="text/javascript">

    function changed_teacher(){
                  
        var student_id = document.getElementById("student").value;
        var teacher_id = document.getElementById("teacher").value;
        var clz_id = document.getElementById("clz").value;

         $.ajax({
          url:'../admin/query/check.php',
          method:"POST",
          data:{search_regiser:student_id,teach_id:teacher_id},
          success:function(data)
          {
            //alert(data)
            //$('#clz').html(data);
            document.getElementById("clz").innerHTML = data;
          }
        });
  }
</script>

<script type="text/javascript">
  
    function reg_class(){
                  
        var student_id = document.getElementById("student").value;
        var teacher_id = document.getElementById("teacher").value;
        var clz_id = document.getElementById("clz").value;

         $.ajax({
          url:'../admin/query/insert.php',
          method:"POST",
          data:{reg_student_subject:student_id,teach_id:teacher_id,clz_id:clz_id},
          success:function(data)
          {
            if(data == 0)
            {
              Swal.fire({
                position: 'top-middle',
                type: 'error',
                icon: 'error',
                title: 'Already Registered Subjects!',
                showConfirmButton: false,
                timer: 1000
              })
            }

            if(data>0)
            {
              Swal.fire({
                position: 'top-middle',
                type: 'success',
                icon: 'success',
                title: 'Registered Successfully!',
                showConfirmButton: false,
                timer: 1000
              }).then(function(){
                location.reload();
              });
            }
          }
        });
  }
</script>



<?php  include('footer/footer.php'); ?>


