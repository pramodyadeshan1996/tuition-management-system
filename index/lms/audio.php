    
    <div class="row">
        <div class="col-md-12">
            <div class="section-title">
                <h5>Audio</h5>
            </div>
        </div>
    </div>
    <div class="row ">

        <?php 
      
      $student_session_id = $_SESSION['STU_ID']; //student ID

      $sql001 = mysqli_query($conn,"SELECT * FROM `uploads` WHERE `CLASS_ID` = '$class_id' AND `TYPE` = 'Audio' AND `PUBLIC` = 'checked' ORDER BY `UPLOAD_TIME` DESC");

      $sql00101 = mysqli_query($conn,"SELECT * FROM `uploads` WHERE `CLASS_ID` = '$class_id' AND `TYPE` = 'Audio' ORDER BY `UPLOAD_TIME` DESC");

      $sql00102 = mysqli_query($conn,"SELECT * FROM `payment_data` WHERE `STU_ID` = '$student_session_id' AND `ADMIN_SUBMIT` = '1' AND `YEAR` = '$year' AND `MONTH` = '$month' AND `CLASS_ID` = '$class_id' "); //Check Paid for this month

      $check_paid = mysqli_num_rows($sql00102); //Check available paid?

      $check_upload = mysqli_num_rows($sql00101); //Check available uploads

      $check_public = mysqli_num_rows($sql001); //Check enabled Public
       /*
            
            Upload Table  ||  Payment Table

            Checked     AND   Paid   OK
            Checked     AND   Unpaid OK
            Unchecked   AND   Paid   OK
            UnChecked   AND   Unpaid Wrong



      */

      if($check_upload > 0)
      {
        if(($check_paid > 0 && $check_public > 0) || ($check_paid > 0 && $check_public == '0') || ($check_paid == '0' & $check_public > 0) )
        {
            //Now Check Enable Public show for paid and public enable students (Checked AND Paid   OK , Checked AND Unpaid OK)
              while($row001 = mysqli_fetch_assoc($sql001))
              {
                $file_name = $row001['FILE_NAME'];
                $upload_id = $row001['UPLOAD_ID'];

                $audio_title = $row001['TITLE'];
                $audio_description = $row001['DESCRIPTION'];
                $audio_upload_time = $row001['UPLOAD_TIME'];

                $sql0012 = mysqli_query($conn,"SELECT * FROM `classes` WHERE `CLASS_ID` = '$class_id' "); //Class Details
                while($row0012 = mysqli_fetch_assoc($sql0012))
                {
                    $class_name = $row0012['CLASS_NAME']; //Class Name
                    $teacher_id = $row0012['TEACH_ID']; //Teacher ID
                    $class_fees = $row0012['FEES']; //Teacher ID

                    $class_start_time = $row0012['START_TIME']; //Class Start time
                    $class_end_time = $row0012['END_TIME']; //Class End time

                    $class_day = $row0012['DAY']; //Class End time
                    
                    $str = strtotime($class_start_time);
                    $class_start_time = date('h:i A',$str);

                    $str2 = strtotime($class_end_time);
                    $class_end_time = date('h:i A',$str2);

                }

                $sql0013 = mysqli_query($conn,"SELECT * FROM `teacher_details` WHERE `TEACH_ID` = '$teacher_id' "); //check Classes for registered teachers details
                while($row0013 = mysqli_fetch_assoc($sql0013))
                {
                    $teacher_position = $row0013['POSITION']; //Teacher Position
                    $teacher_f_name = $row0013['F_NAME']; //Teacher First Name
                    $teacher_l_name = $row0013['L_NAME']; //Teacher Last Name

                    $teacher_picture = $row0013['PICTURE']; //Teacher Profile Picture
                    $teacher_gender = $row0013['GENDER']; //Teacher Gender


                    $teacher_full_name = $teacher_position.". ".$teacher_f_name." ".$teacher_l_name; //Teacher Full Name

                    //If Teacher Picture zero,Check gender and gender to select picture 
                    if($teacher_picture == '0')
                    {
                        if($teacher_gender == 'Male')
                        {
                            $teacher_picture = 'b_teacher.jpg';
                        }else
                        if($teacher_gender == 'Female')
                        {
                            $teacher_picture = 'g_teacher.jpg';
                        }
                    }
                    //If Teacher Picture zero,Check gender and gender to select picture

                }
                $sql0012 = mysqli_query($conn,"SELECT * FROM `institute` WHERE `INS_ID` = '1' "); //Institute Data assign to variables
                while($row0012 = mysqli_fetch_assoc($sql0012))
                {

                    $institute_name = $row0012['INS_NAME']; //Institute Name
                    $institute_tp = $row0012['INS_TP']; //Institute Tp
                    $institute_address = $row0012['INS_ADDRESS']; //Institute Address
                    $institute_mobile = $row0012['INS_MOBILE']; //Institute Mobile No
                    $institute_picture = $row0012['PICTURE']; //Institute Picture

                    $institute_free_period = $row0012['FREE']; // **Institute Free Period**

                }

                ?>
                    <!-- Register Class Box -->

                    <div class="col-md-4">
                        <div class="pricing" style="border: 1px solid #cccc;padding: 0;" id="remove_box<?php echo $class_id; ?>">
                            <div class="content">

                                  <div class="row">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-8 image"><center><img src="../assets/img/icon/audio.svg" alt="" style="border-radius: 10px;width: 120px;height: 120px;background-size: cover;"></center></div>
                                    <div class="col-md-2"></div>
                                  </div>


                            <h5 class="text-dark mt-10 col-md-12"><?php echo $file_name; ?></h5>
                            <div class="col-md-12"><label class="text-center"><?php echo $class_name; ?></label></div>
                            
                              <div class="col-md-12">
                                <audio controls class="form-control" style="border: 0;height: 60px;outline: none;">
                                  <source src="horse.ogg" type="audio/ogg">
                                  <source src="../../uploads/audio/<?php echo $file_name; ?>" type="audio/mp3">
                                  Your browser does not support the audio tag.
                                </audio>
                                <br>

                                <div class="col-md-12">
                                <a href="../../uploads/audio/<?php echo $file_name; ?>" onclick="clicked_audio('<?php echo $upload_id; ?>','<?php echo $student_session_id; ?>');" download="<?php echo $file_name; ?>" class="btn btn-primary btn-block" style="font-weight: bold"><i class="fas fa-download"></i>&nbsp;Download</a>

                                <small class="btn base-color btn-link" style="font-size: 14px;font-weight: 500px;" data-toggle="modal" data-target="#audio_info" onclick="audio_info('<?php echo $audio_title; ?>','<?php echo $audio_description; ?>','<?php echo $audio_upload_time; ?>')">Audio Info</small>
                              </div>

                                <!-- Link Click with inform database -->

                                <script type="text/javascript">

                                      function clicked_audio(upload_id,student_id)
                                      {
                                          var type = 'Audio';
                                          
                                          $.ajax({
                                            type:"POST",
                                            url:"../../student/query/insert.php",
                                            data : {click_lms:upload_id,student_id:student_id,type:type},
                                            success: function (data) {

                                            }
                                          });

                                        }
                                </script>

                                <!-- Link Click with inform database -->


                              </div>
                                
                            </div>
                        </div>
                    </div>
                    

                    <!-- Register Class Box -->

                <?php

              }
            }else
            if(($check_paid == '0' & $check_public == '0') )
            {
                
                ?>


                <div class="col-md-12 row mt-20" style="padding: 10px 10px 10px 10px;">
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                            <center>
                                <img src="../assets/img/icon/unavailable002.svg" style="width: 250px;height: 250px;">
                                <h5 class="mt-20 text-muted"><span class="fas fa-search"></span> Audio Unavailable</h5>
                            </center>
                    </div>
                    <div class="col-md-3"></div>
                </div>


                <?php

                
            }
            //Now Check Enable Public show for paid and public enable students (Checked AND Paid   OK , Checked AND Unpaid OK)
        }else
        if($check_upload == '0')
        {
          /*Empty Payments*/
          
            ?>


            <div class="col-md-12 row mt-20" style="padding: 10px 10px 10px 10px;">
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                            <center>
                                <img src="../assets/img/icon/unavailable002.svg" style="width: 250px;height: 250px;">
                                <h5 class="mt-20 text-muted"><span class="fas fa-search"></span> Audio Unavailable</h5>
                            </center>
                    </div>
                    <div class="col-md-3"></div>
                </div>


            <?php
            //Now Check Enable Public show for paid and public enable students (Unchecked   AND   Paid   OK UnChecked   AND   Unpaid Wrong)
                //But Public Uncheck with show not available message and check with under this condition show Audio
                /*Empty Payments*/
        }

       ?>


    </div>