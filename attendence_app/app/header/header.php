<?php 
    session_start();

    include('../../../connect/connect.php');
    $position = $_SESSION['POSITION'];

     if($position == 'S_ADMIN')
    {
        $admin_id = $_SESSION['ADMIN_ID'];

        $s01 = mysqli_query($conn,"SELECT * FROM admin_login WHERE ADMIN_ID = '$admin_id'");
        while($row = mysqli_fetch_assoc($s01))
        {
          $admin_name = $row['ADMIN_NAME'];
          $type = $row['TYPE'];
          $picture = $row['PICTURE'];
           $gender = $row['GENDER'];

           if($picture == '0')
           {
              if($gender == 'Male')
              {
                $picture = 'male_admin.jpg';
              }else
              if($gender == 'Female')
              {
                $picture = 'female_admin.png';
              }
           }
          

        }
      }else
    if($position == 'ADMIN')
    {
        $admin_id = $_SESSION['ADMIN_ID'];

        $s01 = mysqli_query($conn,"SELECT * FROM admin_login WHERE ADMIN_ID = '$admin_id'");
        while($row = mysqli_fetch_assoc($s01))
        {
          $admin_name = $row['ADMIN_NAME'];
          $type = $row['TYPE'];
          $picture = $row['PICTURE'];
           $gender = $row['GENDER'];

           if($picture == '0')
           {
              if($gender == 'Male')
              {
                $picture = 'male_admin.jpg';
              }else
              if($gender == 'Female')
              {
                $picture = 'female_admin.png';
              }
           }
          

        }
      }else
      if($position == 'STAFF')
      {
          $staff_id = $_SESSION['STAFF_ID'];

          $s01 = mysqli_query($conn,"SELECT * FROM staff_login WHERE STAFF_ID = '$staff_id'");
          while($row = mysqli_fetch_assoc($s01))
          {
            $admin_name = $row['NAME'];
            $type = $row['TYPE'];
            $picture = $row['PICTURE'];
             $gender = $row['GENDER'];

             if($picture == '0')
             {
                if($gender == 'Male')
                {
                  $picture = 'male_admin.jpg';
                }else
                if($gender == 'Female')
                {
                  $picture = 'female_admin.png';
                }
             }
            

          }
    
      }
      

 ?>

 <!DOCTYPE html>
<html lang="en">

<!-- Mirrored from pages.revox.io/dashboard/latest/html/condensed/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 15 Jul 2020 08:48:43 GMT -->
<head>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta charset="utf-8" />
<title><?php echo $page; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
<script src="../../../../cdn-cgi/apps/head/8jwJmQl7fEk_9sdV6OByoscERU8.js"></script><link rel="apple-touch-icon" href="pages/ico/60.png">
<link rel="apple-touch-icon" sizes="76x76" href="pages/ico/76.png">
<link rel="apple-touch-icon" sizes="120x120" href="pages/ico/120.png">
<link rel="apple-touch-icon" sizes="152x152" href="pages/ico/152.png">
<link rel="icon" type="image/x-icon" href="favicon.ico" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-touch-fullscreen" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="default">
<meta content="Meet pages - The simplest and fastest way to build web UI for your dashboard or app." name="description" />
<meta content="Ace" name="author" />
<link href="../assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" />
<link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="../assets/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
<link href="../assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" media="screen" />
<link href="../assets/plugins/jquery-datatable/media/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/plugins/datatables-responsive/css/datatables.responsive.css" rel="stylesheet" type="text/css" media="screen" />
<link class="main-stylesheet" href="../pages/css/pages.css" rel="stylesheet" type="text/css" />
  
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<link class="main-stylesheet" href="../assets/css/style.css" rel="stylesheet" type="text/css" />

<style type="text/css">
  .no-data {
    display: none;
    text-align: center;
}


::-webkit-scrollbar {
  width: 10px;
}

/* Track */
::-webkit-scrollbar-track {
  background: #f1f1f1; 
}
 
/* Handle */
::-webkit-scrollbar-thumb {
  background: #888; 
  border-radius: 10px;
  
}

/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
  background: #555; 
  border-radius: 10px;
}


.spinner {
  margin: 100px auto;
  width: 80px;
  height: 70px;
  text-align: center;
  font-size: 10px;
}

.spinner > div {
  background-color: #333;
  height: 100%;
  width: 6px;
  display: inline-block;
  
  -webkit-animation: sk-stretchdelay 1.2s infinite ease-in-out;
  animation: sk-stretchdelay 1.2s infinite ease-in-out;
}

.spinner .rect2 {
  -webkit-animation-delay: -1.1s;
  animation-delay: -1.1s;
}

.spinner .rect3 {
  -webkit-animation-delay: -1.0s;
  animation-delay: -1.0s;
}

.spinner .rect4 {
  -webkit-animation-delay: -0.9s;
  animation-delay: -0.9s;
}

.spinner .rect5 {
  -webkit-animation-delay: -0.8s;
  animation-delay: -0.8s;
}

@-webkit-keyframes sk-stretchdelay {
  0%, 40%, 100% { -webkit-transform: scaleY(0.4) }  
  20% { -webkit-transform: scaleY(1.0) }
}

@keyframes sk-stretchdelay {
  0%, 40%, 100% { 
    transform: scaleY(0.4);
    -webkit-transform: scaleY(0.4);
  }  20% { 
    transform: scaleY(1.0);
    -webkit-transform: scaleY(1.0);
  }
}

</style>

</head>
<body class="fixed-header dashboard">



<div class="page-container " style="padding: 0;">

<div class="header">

<a href="#" class="btn-link toggle-sidebar d-lg-none pg-icon btn-icon-link" data-toggle="sidebar">
menu</a>

<div class="">
<div class="brand inline   ">

<!-- <img src="assets/img/logo.png" alt="logo" data-src="assets/img/logos.png" data-src-retina="assets/img/logo_2x.png" width="78" height="22"> -->

<h3 class="text-center"><img src="../images/ibs.png" height="40px" width="40px"> IBS</h3>
</div>


</div>


<div class="d-flex align-items-center">
<!-- <a href="#" class="header-icon m-l-5 sm-no-margin d-inline-block" data-toggle="quickview" data-toggle-element="#quickview">
<i class="pg-icon btn-icon-link">menu_add</i>
</a> -->
<div class="dropdown pull-right d-lg-block d-none">
<button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" aria-label="profile dropdown">
<span class="thumbnail-wrapper d32 circular inline">
<img src="../../../admin/images/profile/<?php echo $picture; ?>" alt="" data-src="../../../admin/images/profile/<?php echo $picture; ?>" data-src-retina="../../../admin/images/profile/<?php echo $picture; ?>" width="32" height="32">
</span>
</button>
<div class="dropdown-menu dropdown-menu-right profile-dropdown" role="menu">
<a class="dropdown-item" style="text-transform: capitalize;"><span>Signed <br /><b><?php echo $admin_name; ?></b></span></a>
<div class="dropdown-divider"></div>
<a href="../logout.php" class="dropdown-item" onclick="return confirm('Are you sure logout?')">Logout</a>
</div>
</div>


</div>
</div>


<div class="page-content-wrapper">

<div class="content sm-gutter">
<div class="container-fluid padding-10 sm-padding-0">