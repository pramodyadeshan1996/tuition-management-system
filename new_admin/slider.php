<?php
	$page = "Slider";
	$folder_in = '3';

  
      include('header/header.php');


        $s04 = mysqli_query($conn,"SELECT * FROM teacher_details WHERE TEACH_ID = '$teach_id'");
        while($row4 = mysqli_fetch_assoc($s04))
        {
          $teacher_name = $row4['POSITION'].". ".$row4['F_NAME']." ".$row4['L_NAME'];
          $teacher_name2 = $row4['F_NAME']." ".$row4['L_NAME'];

          $t_pic = $row4['PICTURE'];

          if($t_pic == '0')
          {
          	$t_pic = 'teacher.png';
          }
        }

         $sql0015 = mysqli_query($conn,"SELECT * FROM level");
        while($row0015=mysqli_fetch_assoc($sql0015))
        {
          $level_name = $row0015['LEVEL_NAME'];

        }

 ?>
<script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>

<ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-bottom: 04%;margin-bottom: 0;">
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="profile_setting.php">Profile Setting</a></li>
       <li class="breadcrumb-item" data-toggle="tooltip" data-title="Setup Slider"> Setup Slider Images</li>
    </ol>


  <div class="col-md-12 bg-white" style="padding-top: 20px;padding-bottom: 20px;">
    
  <form action="../admin/query/insert.php" method="POST" enctype="multipart/form-data">
      <div class="col-md-12" style="border-bottom: 1px solid #cccc;">
        <div class="row">

            <div class="col-md-6" style="margin-bottom: 10px;"><h2><a href="profile_setting.php" style="color: #626462;"><span class="fa fa-angle-left"></span></a> Setup Slider Images</h2></div>
            <div class="col-md-6" style="padding-top: 6px;">

              <div class="row">
                <div class="col-md-8">

                <input type="file" id="selectedFile2" name="upload_file" style="display: none;" onchange="uploaded99()"/>

                <div style="padding: 0px;margin: 0px;" class="pt-10" id="td_btn22" onclick="document.getElementById('selectedFile2').click();">
                  <button type="button" class="btn btn-block btn-sm" name="create_payment" id="upload_btn99" style="font-weight:bold;padding: 6px 2px 6px 2px;border:3px dashed gray;font-size: 16px;outline: none;background-color: white;" value="Browse..."> <i class="fa fa-plus"></i>&nbsp; 
                  Upload Image
                </button>

              </div>

                <script type="text/javascript">
                    

                    function uploaded99()
                    {
                        
                        var selectedFile22 = document.getElementById('selectedFile2').value;
                        //alert(selectedFile22)

                         if(selectedFile22 == '')
                        {
                          $("#td_btn22").html('<button type="button" class="btn btn-block btn-sm" name="create_payment" id="upload_btn99" style="font-weight:bold;padding: 8px 2px 8px 2px;border:3px dashed gray;font-size: 16px;outline: none;background-color: white;" value="Browse..."> <i class="fa fa-plus"></i>&nbsp; Upload Image</button>');
                          $('#upload_btn99').prop('disabled',true);

                        }else 
                        if(selectedFile22 !== '')
                        {
                          //alert(selectedFile22)

                            document.getElementById("td_btn22").innerHTML = '<button type="button" class="btn btn-block btn-sm" id="upload_btn99" style="padding: 8px 2px 8px 2px;font-size: 18px;outline: none;background-color: #ff6500;color:white;font-weight:bold;" value="Browse..." > <i class="fa fa-spinner fa-pulse"></i>&nbsp; Changing..</button>';
                            
                            setTimeout(function() { 


                            document.getElementById("td_btn22").innerHTML = '<button type="button" class="btn btn-block btn-sm" name="create_payment" id="upload_btn99" style="padding: 8px 2px 8px 2px;font-size: 18px;outline: none;background-color: #27ae60;color:white;" value="Browse..." > <i class="fa fa-check-circle"></i> &nbsp; Upload successfully</button>';

                            $('#upload_btn99').prop('disabled',false);

                            },400); 
                          

                          
                        }

                        
                    }

                  </script>

                </div>
                <div class="col-md-4">

                    <button type="submit" name="upload_slide_image" id="upload_btn" class="btn btn-success btn-block btn-lg" style="max-height:60px;"><i class="fa fa-check"></i>&nbsp; Upload</button>

                </div>
              </div>

            </div>

        </div>
                  </form>
      </div>

      
        <div class="col-md-12">
          <form action="../admin/query/update.php" method="POST" enctype="multipart/form-data">
          <div class="row">
                <?php 
                  $sql001 = mysqli_query($conn,"SELECT * FROM `slide` ORDER BY `ORDER` ASC");

                  $ch = mysqli_num_rows($sql001);

                  if($ch > 0)
                  {
                    while($row001=mysqli_fetch_assoc($sql001))
                    {
                      $add_date = $row001['ADD_DATE'];
                      $add_time = $row001['ADD_TIME'];
                      $myorder = $row001['ORDER'];
                      $img = $row001['IMG_NAME'];  
                      $slid_id = $row001['SLID_ID'];  

                      
                      echo '
                      
                      
                      <div class="col-md-3" style="margin-top:20px;">
                            <div class="col-md-12" style="padding:20px 20px 20px 20px;border:1px solid #cccc;">
                              <img src="../student/slider_images/'.$img.'" style="width:100%;height:auto;">

                              <div class="row mt-10" style="margin-top:20px;">
                                <div class="col-md-4"><label style="padding-top:10px;">Slide No :</label></div>
                                <div class="col-md-8"> 

                                  <input type="number" name="myOrder['.$slid_id.']" class="form-control" value="'.$myorder.'">

                                </div>
                              </div>

                              <input type="hidden" name="recent_img['.$slid_id.']" value="'.$img.'">

                              <div class="row mt-10" style="margin-top:15px;">
                                <div class="col-md-4"><label style="padding-top:10px;font-size:13px;">Update Image : </label></div>
                                <div class="col-md-8"> <input type="file" name="img['.$slid_id.']" class="form-control" value="'.$img.'"></div>
                              </div>
                              <div class="col-md-12" style="border-top:1px solid #cccc;margin-top:10px;"></div>
                              <div class="row mt-10" style="margin-top:15px;">
                                <div class="col-md-8">
                                  <button type="submit" name="update_slider" value="'.$slid_id.'" class="btn btn-success btn-block btn-lg"><i class="fa fa-edit"></i>&nbsp;Update</button>
                                </div>
                                <div class="col-md-4">';?>                                 
                                  <a href="../admin/query/delete.php?delete_slide=<?php echo $slid_id; ?>&&img=<?php echo $img; ?>" name="update_slider" class="btn btn-danger btn-block btn-lg" onclick="return confirm('Are you sure remove?')"><i class="fa fa-trash"></i>&nbsp;Remove</a>
                                  
                                  <?php echo '
                                </div>
                              </div>

                            </div>
                        </div>
                        
                        ';
                    }
                  }else
                  {
                    echo '<div class="alert alert-danger col-md-12 text-center" style="margin-top:20px;"><i class="fa fa-warning"></i>&nbsp; Not found data!</div>';
                  }
                ?>
                </div>
                </form>
        </div>
                
    </div>


                                    


<script type="text/javascript">
  $('.save_btn').click(function(){
    
    var upload_btn0 = $('#selectedFile0').val();
    var upload_btn = $('#sel_file').val();
      
    if(empty(upload_btn0))
    {
      alert('Please select your audio file.');
    }else
    if(empty(upload_btn))
    {
      alert('Please select your document file.');
    }
   });
</script>

<script type="text/javascript">
  $('.navi').click(function(){
    $('.frm')[0].reset();
      
   });
</script>

<script>
$(document).ready(function(){
  $("#search").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#table tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

<!-- no data message/no found data show in search-->

<script type="text/javascript">
  $(document).ready(function () {

    (function ($) {

        $('#search').keyup(function () {
            var rex = new RegExp($(this).val(), 'i');
            $('#table tr').hide();
            $('#table tr').filter(function () {
                return rex.test($(this).text());
            }).show();
            $('.no-data').hide();
            if($('#table tr:visible').length == 0)
            {
                $('.no-data').show();
            }

        })

    }(jQuery));

});
</script>




</div>
</div>
</div>

<?php include('footer/footer.php'); ?>