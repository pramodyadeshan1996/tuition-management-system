
<?php

$page = "Create Class";
$folder_in = '3';

  include('header/header.php'); 
 ini_set( "display_errors", 0); 


  ?>
  <style type="text/css">
    .btn_pulse {
  
  display: block;
  border-radius: 10%;
  cursor: pointer;
  animation: none;
  float: left;
  bottom: 5px;
  right: 5px;
  font-weight: bold;
  padding-top: 2px;
  text-align: center;
  color: white;
  font-size: 14px;
  animation: pulse 1.6s infinite;
}


@keyframes pulse {
  0% {
    -moz-box-shadow: 0 0 0 0 #7252d3;
    box-shadow: 0 0 0 0 #7252d3;
  }
  70% {
    -moz-box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
  }
  100% {
    -moz-box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
  }
}

table {
  border: 0px solid black;
}
th, td {
  border: 0px solid black;
}
td {
  padding: 5px;
}

#pagination {
  width: 100%;
  text-align: center;
}

#pagination ul li {
  display: inline;
  margin-left: 10px;
}
body
{
  overflow-x: hidden;
}

  </style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script src="https://pagination.js.org/dist/2.1.4/pagination.min.js"></script>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<div class="row" style="border-bottom: 1px solid #cccc;">
  <div class="col-md-11">
    <ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-bottom: 04%;margin-bottom: 0;">
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="new_class.php?teach_id=<?php echo $_GET['teach_id']; ?>">Show Class</a></li>
    </ol>
  </div>
  <div class="col-md-1">
            

    </div>
  </div>
</div>

<div class="col-md-12">

  <div class="row">
    <div class="col-md-3"></div>

    <div class="col-md-6" style="margin-top: 0;">
   

  <div class=" container-fluid   container-fixed-lg bg-white">
  <div class="card card-transparent" style="padding: 10px 10px 30px 10px;">
   <h3 style="text-transform: capitalize;"><a href="new_class.php?teach_id=<?php echo $_GET['teach_id']; ?>" data-toggle="tooltip" data-title="Back" class="text-muted" style="font-size: 12px;opacity: 0.6;"> Back</a> 
    <br>
  Create Class</h3>
    
  <form action="../admin/query/insert.php" method="POST">

<div class="row">
  <div class="col-md-6">
    
    <div class="form-group form-group-default" style="margin-top: 10px;">
      <input type="hidden" id="teacher_id" value="<?php echo $teach_id; ?>">
      <label>Classes</label>
      <select class="form-control select_class" name="clz" id="level_clz" required style="cursor: pointer;">
        <option value="">Select Classes </option>

          <?php 

              $sql0015 = mysqli_query($conn,"SELECT * FROM level");
              while($row0015=mysqli_fetch_assoc($sql0015))
              {
                $level_name = $row0015['LEVEL_NAME'];
                $level_id = $row0015['LEVEL_ID'];

                echo '<option value='.$level_id.'>'.$level_name.'</option>';

              }

               ?>
        
      </select>
    </div>

  </div>
  <div class="col-md-6">
    
    <div class="form-group form-group-default" style="margin-top: 10px;">
      <label>Subject</label>
      <select class="form-control"  id="subject"  name="subject" required style="cursor: pointer;">
        <option value="">Select Subject</option>
      </select>
    </div>

  </div>
</div>

<div class="row">
  <div class="col-md-6">
    
    <div class="form-group form-group-default" style="margin-top: 10px;">
      <label>Class Name</label>
      
      <input type="text" name="clz_name" class="form-control change_inputs"  placeholder="XXXXXXXX" required >

    </div>

  </div>
    <div class="col-md-6">

    <div class="form-group form-group-default" style="margin-top: 10px;">
      <label>Help Contact No</label>
      
      <input type="text" name="contact_no" class="form-control check_contact" value="0" maxlength="10" required>

    </div>


  </div>
</div>



<div class="row">
  
  <div class="col-md-6">
      
      <div class="form-group form-group-default" style="margin-top: 10px;">
        <label>Class Fees</label>
        
        <input type="number" name="clz_fees" class="form-control change_inputs" placeholder="XXXXXXXX" value="0" onclick="this.select();" required>

      </div>

  </div>
  <div class="col-md-6">
    
    <div class="form-group form-group-default" style="margin-top: 10px;">
      <label>Age Range</label>
      
      <input type="number" name="age_range" class="form-control change_inputs" value="0" min="0" required>

    </div>

  </div>
</div>



<script type="text/javascript">
  function checkbox(){

    var checkboxes = document.getElementsByName('weekdays');
    var checkboxesChecked = [];
    // loop over them all
    for (var i=0; i<checkboxes.length; i++) {
       // And stick the checked ones onto an array...
       if (checkboxes[i].checked) {
          checkboxesChecked.push(checkboxes[i].value);
       }
    }
    document.getElementById("show").value = checkboxesChecked;

  }
</script>


<div class="row">
  <div class="col-md-6">

    <div class="form-group form-group-default" style="margin-top: 10px;">
      <label>Start Time</label>
      
      <input type="time" name="start" class="form-control change_inputs" required>

    </div>

  </div>
  <div class="col-md-6">

    <div class="form-group form-group-default" style="margin-top: 10px;">
      <label>End Time</label>
      
      <input type="time" name="end" class="form-control change_inputs" required>

    </div>

  </div>
</div>

  <div class="row">
    <div class="col-md-12">
      <div class="form-group form-group-default" style="margin-top: 10px;">
        <label for="">Hide Telegram Button</label>

        <div class="form-check form-check-inline switch switch-lg success" style="width:100%;">
          <input type="checkbox" id="active_tg_btn" value="1" name="hide_tg_btn">
          <label for="active_tg_btn" class="text-danger" style="text-transform: capitalize;cursor: pointer;"></label>

        </div>

      </div>
    </div>

  </div>

<div class="col-md-12">

<div class="row">
  <div class="col-md-4">
    <label>
    <input type="checkbox" id="bk" name="weekdays" onClick="checkbox();" value="Monday" style="-ms-transform: scale(2);-moz-transform: scale(2);-webkit-transform: scale(2);-o-transform: scale(2); transform: scale(2);padding: 1px;margin-top: 12px;"> <b style="font-size: 16px;padding-left: 10px; ">Monday</b> &nbsp;
    <label>
  </div>
  <div class="col-md-4">
    <label>
    <input type="checkbox" id="cr" name="weekdays" onClick="checkbox();" value="Tuesday" style="-ms-transform: scale(2);-moz-transform: scale(2);-webkit-transform: scale(2);-o-transform: scale(2); transform: scale(2);padding: 1px;margin-top: 12px;"> <b style="font-size: 16px;padding-left: 10px; ">Tuesday</b> &nbsp;
    </label>
  </div>
  <div class="col-md-4">
    <label>
    <input type="checkbox" id="cr" name="weekdays" onClick="checkbox();" value="Wednesday" style="-ms-transform: scale(2);-moz-transform: scale(2);-webkit-transform: scale(2);-o-transform: scale(2); transform: scale(2);padding: 1px;margin-top: 12px;"> <b style="font-size: 16px;padding-left: 10px; "> Wednesday</b> &nbsp;
    </label>
  </div>
</div>

<div class="row">
  <div class="col-md-4">
    <label>
    <input type="checkbox" id="cr" name="weekdays" onClick="checkbox();" value="Thursday" style="-ms-transform: scale(2);-moz-transform: scale(2);-webkit-transform: scale(2);-o-transform: scale(2); transform: scale(2);padding: 1px;margin-top: 12px;"> <b style="font-size: 16px;padding-left: 10px; "> Thursday</b> &nbsp;
    </label>
  </div>
  <div class="col-md-4">
    <label>
    <input type="checkbox" id="cr" name="weekdays" onClick="checkbox();" value="Friday" style="-ms-transform: scale(2);-moz-transform: scale(2);-webkit-transform: scale(2);-o-transform: scale(2); transform: scale(2);padding: 1px;margin-top: 12px;"> <b style="font-size: 16px;padding-left: 10px; "> Friday </b> &nbsp;
    </label>
  </div>
  <div class="col-md-4">
    <label>
      <input type="checkbox" id="cr" name="weekdays" onClick="checkbox();" value="Saturday" style="-ms-transform: scale(2);-moz-transform: scale(2);-webkit-transform: scale(2);-o-transform: scale(2); transform: scale(2);padding: 1px;margin-top: 12px;"> <b style="font-size: 16px;padding-left: 10px; "> Saturday</b>&nbsp;
    </label>
  </div>
</div>

<div class="row">
  <div class="col-md-4">
    
    <label><input type="checkbox" id="cr" name="weekdays" onClick="checkbox();" value="Sunday" style="-ms-transform: scale(2);-moz-transform: scale(2);-webkit-transform: scale(2);-o-transform: scale(2); transform: scale(2);padding: 1px;margin-top: 12px;"> <b style="font-size: 16px;padding-left: 10px; "> Sunday</b></label> &nbsp;

  </div>
</div>
</div>

<div class="form-group form-group-default" style="margin-top: 10px;">
  <label>Day(s)</label>
  <textarea id="show" name="weekday" required class="form-control" style="height: 40px;font-weight: bold;" placeholder="Days.."></textarea>

</div>

<button type="submit" class="btn btn-success btn-block btn-lg" name="add_clz" value="<?php echo $teach_id; ?>"><i class="pg-icon">add</i> Add</button>
</form>

  </div>
  </div>

  </div>
  </div>
</div>



<script type="text/javascript">
  
    $(document).ready(function(){  
     $('#level_clz').change(function(){

       var level_clz = $(this).val();
       var teach_id = $('#teach_id').val();

       $.ajax({
        url:'../teacher/query/check.php',
        method:"POST",
        data:{create_clz:level_clz,teach_id:teach_id},
        success:function(data)
        {
          //alert(data)
            $('#subject').html(data);
          
        }
       })
     

    });

     
   });

  </script>

<script type="text/javascript">
	setInterval(function() {
    $('blink').fadeIn(1300).fadeOut(1500);
}, 1000);
</script>

<script>
$(document).ready(function(){
  $("#search").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $(".myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

<script type="text/javascript">
  $(document).ready(function () {

    (function ($) {

        $('#search').keyup(function () {
            var rex = new RegExp($(this).val(), 'i');
            $('#myTable tr').hide();
            $('#myTable tr').filter(function () {
                return rex.test($(this).text());
            }).show();
            $('.no-data').hide();
            if($('#myTable tr:visible').length == 0)
            {
                $('.no-data').show();
            }

        })

    }(jQuery));

});
</script>

<script>
$(document).ready(function(){

  $("#up_rec").on("change", function() {
      $('.show_image').hide();
  });
});
</script>
<script type="text/javascript">
  setInterval(function() {
    $('.blink').fadeIn(1300).fadeOut(1500);
}, 1000);
</script>

<script type="text/javascript">
  $("#show_hide_password a").on('click', function(event) {
        event.preventDefault();
        if($('#show_hide_password input').attr("type") == "text"){
            $('#show_hide_password input').attr('type', 'password');
            $('#show_hide_password i').addClass( "fa-eye-slash" );
            $('#show_hide_password i').removeClass( "fa-eye" );
        }else if($('#show_hide_password input').attr("type") == "password"){
            $('#show_hide_password input').attr('type', 'text');
            $('#show_hide_password i').removeClass( "fa-eye-slash" );
            $('#show_hide_password i').addClass( "fa-eye" );
        }
    });
</script>

<script type="text/javascript">
  let rows = []
$('table tbody tr').each(function(i, row) {
  return rows.push(row);
});

$('#pagination').pagination({
    dataSource: rows,
    pageSize: 8,
    callback: function(data, pagination) {
        $('tbody').html(data);
    }
})
</script>



<script type="text/javascript">
  $('#error').hide(); //error message hidden

  $(document).ready(function(){


    $('#error').hide(); //error message hidden



/*   validation form*/

  /* $('#password,#re_password').keyup(function(){

   var psw = $('#password').val();
   var re_psw = $('#re_password').val();
  
  if(psw !== '' && re_psw !== '')
  {
        if(psw !== re_psw)
       {    
            $('#error').show();

            $('#save').prop("disabled",true);
            $('#save').css('opacity','0.8');
            $('#save').css('cursor','not-allowed');

            $('#error').html('<div class="alert alert-danger col-md-12 text-center" style="color:red;font-size:16px;width:100%;"><span class="fa fa-warning text-danger"> The password you entered is incorrect.</span></div>');

       }else
       if(psw == re_psw)
       {    
            $('#error').show();

            $('#save').prop("disabled",false);
            $('#save').css('opacity','3.5');
            $('#save').css('cursor','pointer');

            $('#error').html('<div class="alert alert-success col-md-12 text-center" style="color:green;font-size:16px;width:100%;"><span class="fa fa-check-circle text-success"> The password you entered is correct.</span></div>');
       }
   }
   
   });*/

/*   validation form*/

});
</script>

<script>
$(document).ready(function () {

    $('#f_name,#l_name').bind('keyup blur',function()
    { 
           var thistext = $(this);
           thistext.val(thistext.val().replace(/[^a-z A-Z .]/g,'') ); 

           
    });

})
</script>

<script>
$(document).ready(function () {

    $('#address').bind('keyup blur',function()
    { 
           var thistext = $(this);
           thistext.val(thistext.val().replace(/[^a-z A-z . 0-9-/,]/g,'') ); 

           
    });

})
</script>

<script>
$(document).ready(function () {

    $('#email').bind('keyup blur',function()
    { 
           var thistext = $(this);
           thistext.val(thistext.val().replace(/[^a-zA-z0-9!#$%&'*+-/=?^_`{|,@]/g,'') ); 

           
    });

})
</script>

<script>
$(document).ready(function () {

    $('#number').bind('keyup blur',function()
    { 
           var thistext = $(this);
           thistext.val(thistext.val().replace(/[^0-9]/g,'') ); 

           
    });

})
</script>

<?php  include('footer/footer.php'); ?>



<script type="text/javascript">
    $(document).ready(function(){

            $('#myTable').hide();
            var st = $('#st_tbl').val();

        $('#search_students').click(function(){
          
            var st = $('#st_tbl').val();
            if(st == 1)
            {
            
              $('#myTable').hide();
              $('#st_tbl').val('0');

              
            }

            if(st == '0')
            {
            
              $('#myTable').show();
              $('#st_tbl').val('1');
              
            }
            
        });
    });
  </script>
