

<?php
  $page = "Teacher Approved";
  $folder_in = '3';

      include('header/header.php');

        $s04 = mysqli_query($conn,"SELECT * FROM admin_login WHERE ADMIN_ID = '$admin_id'");
        while($row4 = mysqli_fetch_assoc($s04))
        {
          $name = $row4['ADMIN_NAME'];
        }

 ?>

    <ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-bottom: 04%;margin-bottom: 0;">
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="Teacher Approved"> Teacher Approved</li>
    </ol>


      <div class="col-md-12 bg-white" style="border:1px solid #cccc;height: auto;">
      
      <div class="col-md-12" style="border-bottom: 1px solid #cccc;">
        <div class="row">

            <div class="col-md-9" style="margin-bottom: 10px;"><h2><a href="dashboard.php" data-toggle="tooltip" data-title="Back"><i class="pg-icon" style="font-size: 22px;">chevron_left</i></a> Teacher Approved</h2></div>

        </div>
      </div>

          
          <div class=" container-fluid container-fixed-lg bg-white" style="margin-top: 0px;">
            <div class="card card-transparent">
              <div class="card-header ">
                    <div class="card-title">
                    </div>
                    <div>
                      <div class="row">
                        <div class="col-md-6">
                          
                        </div>
                      <div class="col-md-2"></div>
                      <div class="col-md-4">
                      <div class="col-md-12" style="padding-top: 18px;"></div>
                      <input type="text" id="search-table" class="form-control pull-right" placeholder="Search">
                      </div>

                    </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="card-body">
                <table class="table table-hover demo-table-search table-responsive-block" id="tableWithSearch">
                  <thead>
                      <tr>
                        <th style="width: 1%;" class="text-danger">Registered Date</th>
                        <th style="width: 1%;">Name</th>
                        <th style="width: 1%;">TP No</th>
                        <th style="width: 1%;">DOB</th>
                        <th style="width: 1%;">Gender</th>
                        <th style="width: 1%;text-align: center;">Action</th>
                      </tr>
                    </thead>
                    <tbody>

                      <?php 

                          $today = date('Y-m-d h:i:s A');

                          //mysqli_query($conn,"UPDATE stu_login SET REG_DATE = '$today'");

                          $sql001 = mysqli_query($conn,"SELECT * FROM `teacher_login` WHERE `STATUS` = 'Active' ORDER BY `REG_DATE` DESC");

                          $check = mysqli_num_rows($sql001);

                          if($check>0){
                          while($row001 = mysqli_fetch_assoc($sql001))
                          {
                            $teach_id = $row001['TEACH_ID'];
                            $reg_date = $row001['REG_DATE'];


                            $sql002 = mysqli_query($conn,"SELECT * FROM teacher_details WHERE TEACH_ID = '$teach_id'");
                            while($row002 = mysqli_fetch_assoc($sql002))
                            {
                              $name = $row002['POSITION'].". ".$row002['F_NAME']." ".$row002['L_NAME'];
                              $fnm = $row002['F_NAME'];
                              $lnm = $row002['L_NAME'];

                              $address = $row002['ADDRESS'];
                              $tp = $row002['TP'];
                              $dob = $row002['DOB'];
                              $gender = $row002['GENDER'];
                              $email = $row002['EMAIL'];
                              $position = $row002['POSITION'];
                              $qualification = $row002['QUALIFICATION'];
                            }
                            

                            echo '

                        <tr>
                          <td class="v-align-middle text-danger bold">'.$reg_date.'</td>
                          <td class="v-align-middle">'.$name.'</td>
                          <td class="v-align-middle">'.$tp.'</td>
                          <td class="v-align-middle">'.$dob.'</td>
                          <td class="v-align-middle">'.$gender.'</td>
                           <td class="v-align-middle text-center" style="padding:0;">

                          <button data-toggle="modal" data-target="#teach_data'.$teach_id.'" class="btn btn-success  btn-xs btn-rounded" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;" data-title="Approve" data-toggle="tooltip" id="edit1'.$teach_id.'"><i class="pg-icon">edit</i></button>
                          ' ?>
                          <a href="../admin/query/update.php?approved_teach=<?php echo $teach_id; ?>&&approve=Disapprove" onclick="return confirm('Are you sure disapprove?')" class="btn btn-danger  btn-xs btn-rounded" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;" data-title="Disapprove" data-toggle="tooltip"><i class="pg-icon">trash_alt</i></a>

                          <?php echo '

                          </td>
                        </tr>

                      ';
                            ?>
                      <div class="modal fade slide-up disable-scroll" id="teach_data<?php echo $teach_id; ?>" tabindex="-1" role="dialog" aria-hidden="false">
                                        <div class="modal-dialog ">
                                        <div class="modal-content-wrapper">
                                        <div class="modal-content modal-lg">
                                        <div class="modal-header clearfix text-left">
                                        <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
                                        </button>
                                        <h5>Teacher Details</h5>
                                        </div>
                                        <div class="modal-body">

                                            <form  action="../admin/query/update.php" method="POST" id="form-personal" role="form" autocomplete="off">

                                            <div class="row">

                                              <div class="col-md-12">
                                            <div class="form-group form-group-default input-group">
                                            <div class="form-input-group">
                                            <label>Position</label>

                                              <select class="form-control change_inputs" name="position">
                                                
                                                <?php 
                                                echo '
                                                <option value="'.$position.'">'.$position.'.</option>';

                                                if($position == 'Mr')
                                                {
                                                    echo '<option value="Mrs">Mrs.</option>
                                                <option value="Ms">Ms.</option>
                                                <option value="Rev">Rev.</option>
                                                <option value="Dr">Dr.</option>
                                                <option value="Prof">Prof.</option>';
                                                }else
                                                if($position == 'Mrs')
                                                {
                                                  echo '<option value="Mr">Mr.</option>
                                                        <option value="Ms">Ms.</option>
                                                        <option value="Rev">Rev.</option>
                                                        <option value="Dr">Dr.</option>
                                                        <option value="Prof">Prof.</option>';
                                                }else
                                                if($position == 'Ms')
                                                {
                                                  echo '<option value="Mr">Mr.</option>
                                                        <option value="Mrs">Mrs.</option>
                                                        <option value="Rev">Rev.</option>
                                                        <option value="Dr">Dr.</option>
                                                        <option value="Prof">Prof.</option>';
                                                }else
                                                if($position == 'Rev')
                                                {
                                                  echo '<option value="Mr">Mr.</option>
                                                        <option value="Mrs">Mrs.</option>
                                                        <option value="Ms">Ms.</option>
                                                        <option value="Dr">Dr.</option>
                                                        <option value="Prof">Prof.</option>';
                                                }else
                                                if($position == 'Dr')
                                                {
                                                  echo '<option value="Mr">Mr.</option>
                                                        <option value="Mrs">Mrs.</option>
                                                        <option value="Ms">Ms.</option>
                                                        <option value="Rev">Rev.</option>
                                                        <option value="Prof">Prof.</option>';
                                                }else
                                                if($position == 'Prof')
                                                {
                                                  echo '<option value="Mr">Mr.</option>
                                                        <option value="Mrs">Mrs.</option>
                                                        <option value="Ms">Ms.</option>
                                                        <option value="Rev">Rev.</option>
                                                        <option value="Dr">Dr.</option>';
                                                }


                                               ?>

                                                </select>

                                              </div>
                                              </div>
                                              </div>
                                              </div>


                                              <div class="row">
                                              <div class="col-md-6">
                                              <div class="form-group form-group-default input-group">
                                              <div class="form-input-group">
                                              <label>First Name</label>
                                              <input type="text" name="fnm" class="form-control change_inputs" value="<?php echo $fnm; ?>" pattern="[a-zA-Z. ]+"  required>
                                              </div>
                                              </div>
                                              </div>

                                              <div class="col-md-6">
                                              <div class="form-group form-group-default input-group">
                                              <div class="form-input-group">
                                              <label>Last Name</label>
                                              <input type="text" name="lnm" class="form-control change_inputs" value="<?php echo $lnm; ?>" pattern="[a-zA-Z. ]+"  required>
                                              </div>
                                              </div>
                                              </div>

                                              </div>

                                              <div class="row">
                                              <div class="col-md-6">
                                              <div class="form-group form-group-default input-group">
                                              <div class="form-input-group">
                                              <label>Date Of Birth</label>
                                              <input class="form-control change_inputs" type="date" name="dob" required min="<?php $d = strtotime("-60 year"); echo date('Y-m-d',$d); ?>" max="<?php $d = strtotime("-5 year"); echo date('Y-m-d',$d); ?>" style="margin-bottom: 10px;" value="<?php echo $dob; ?>">
                                              </div>
                                              </div>
                                              </div>

                                              <div class="col-md-6">
                                              <div class="form-group form-group-default input-group">
                                              <div class="form-input-group">
                                              <label>Telephone No</label>
                                              <input class="form-control change_inputs" type="telephone" pattern="[0-9]{10}" minlength="10" maxlength="10" name="number" id="number" value="<?php echo $tp; ?>" required placeholder="0xxx xxxx xx" style="margin-bottom: 10px;">
                                              </div>
                                              </div>
                                              </div>

                                              </div>

                                              <div class="row">
                                              <div class="col-md-12">
                                              <div class="form-group form-group-default input-group">
                                              <div class="form-input-group">
                                              <label>E-mail Address</label>
                                              <input type="email" class="form-control change_inputs" name="email" value="<?php echo $email; ?>" required  style="margin-bottom: 10px;" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$">
                                              </div>
                                              </div>
                                              </div>
                                              </div>
                                              <div class="row">
                                              <div class="col-md-12">
                                              <div class="form-group form-group-default">
                                              <label>Address</label>
                                              <textarea name="address" required placeholder="xxxxxxxx" style="margin-bottom: 10px;" class="form-control change_inputs"><?php echo $address; ?></textarea>
                                              </div>
                                              </div>
                                              </div>
                                              <div class="row">
                                              <div class="col-md-6">
                                              <div class="form-group form-group-default">
                                              <label>Gender</label>
                                              <div class="row">
                                              <?php 
                                                  if($gender == 'Male')
                                                  {?>



                                                    <div class="col-md-6" style="padding-left: 6px;">

                                                      <label><span class="fa fa-male"></span> Male : <input type="radio" checked="checked" name="gender" value="Male" class="change_inputs"></label>
                                                    </div>
                                                    <div class="col-md-6" style="padding-left: 6px;">

                                                      <label style="padding-left: 10px;margin-bottom: 10px;"><span class="fa fa-female"></span> Female : <input type="radio" name="gender" value="Female" class="change_inputs"> </label>

                                                    </div>
                                                    <?php
                                                  }else
                                                  if($gender == 'Female')
                                                  {?>



                                                    <div class="col-md-6" style="padding-left: 6px;"><label><span class="fa fa-male"></span> Male : <input type="radio"name="gender" value="Male" class="change_inputs"></label></div>

                                                    <div class="col-md-6" style="padding-left: 6px;"><label style="padding-left: 10px;margin-bottom: 10px;"><span class="fa fa-female"></span> Female : <input type="radio" checked="checked"  name="gender" value="Female" class="change_inputs"> </label></div>
                                                    <?php
                                                  }
                                                 ?>
                                                 </div>
                                              </div>
                                              </div>
                                              <div class="col-md-6">
                                              <div class="form-group form-group-default">
                                                <label>Qualification</label>
                                                
                                                <textarea name="qualification" required placeholder="xxxxxxxx" style="margin-bottom: 10px;" class="form-control change_inputs"><?php echo $qualification; ?></textarea>


                                              </div>
                                              </div>
                                              </div>
                                              <div class="clearfix"></div>
                                              <div class="row m-t-25">
                                              <div class="col-xl-6 p-b-10">
                                              <p class="small-text hint-text">Click the Update button to change all the data you have changed. (ඔබ වෙනස් කල සියලු දත්තයන් වෙනස් කිරීමට Update බොත්තම ඔබන්න.)</p>
                                              </div>
                                              <div class="col-xl-6">
                                              <button aria-label="" id="up_btn" class="btn btn-primary pull-right btn-lg btn-block" type="submit" name="update_teacher" value="<?php echo $teach_id; ?>">Update
                                              </button>
                                              </div>
                                              </div>
                                              </form>

                                          </div>

                                      </div>
                                    </div>
                                  </div>
                  <?php
                          }
                        }
                      
                         ?>


                    </tbody>
                </table>
              </div>
            </div>
            </div>
            </div>
            </div>

<?php include('footer/footer.php'); ?>