
<?php

$page = "Dashboard";
$folder_in = '0';


  include('header/header.php'); 

 //session_destroy();

  ?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<style type="text/css">
 

  .btn1 {
    background: linear-gradient(144deg, #490a87, #6a12bf);
    background-size: 400% 400%;
    border-radius: 20px;
    box-shadow: 1px 1px 8px 6px #cccc;
    
    -webkit-animation: AnimationName 23s ease infinite;
    -moz-animation: AnimationName 23s ease infinite;
    animation: AnimationName 23s ease infinite;
}

@-webkit-keyframes AnimationName {
    0%{background-position:0% 2%}
    50%{background-position:100% 99%}
    100%{background-position:0% 2%}
}
@-moz-keyframes AnimationName {
    0%{background-position:0% 2%}
    50%{background-position:100% 99%}
    100%{background-position:0% 2%}
}
@keyframes AnimationName {
    0%{background-position:0% 2%}
    50%{background-position:100% 99%}
    100%{background-position:0% 2%}
}

  .count {
  
  display: block;
  cursor: pointer;
  animation: none;
  float: left;
  bottom: 5px;
  right: 5px;
  font-weight: bold;
  padding-top: 2px;
  text-align: center;
  color: white;
  font-size: 14px;
  animation: pulse 1.6s infinite;
}


@keyframes pulse {
  0% {
    -moz-box-shadow: 0 0 0 0 white;
    box-shadow: 0 0 0 0 white;
  }
  70% {
    -moz-box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
  }
  100% {
    -moz-box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
  }
}



</style>

<?php $admin_type = $_SESSION['TYPE'];
      

      if($admin_type == 'ADMIN')
      {
          $hide0 = "none";
          $user_name001 = 'Administrator';
      }else
      if($admin_type == 'S_ADMIN')
      {
          $user_name001 = 'Super Administrator';
      }else
      if($admin_type == 'STAFF')
      {
          $user_name001 = 'Staff member';
      }


 ?>



<ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-top: 2%;margin-bottom: 0;">
<li class="breadcrumb-item"></li>

</ol>


<div class="row" style="padding-bottom: 10px;border-bottom: 1px solid #cccc;">
  <div class="col-md-8">
    
    <div class="col-md-12" style="margin-top: 0;">

      <h3 style="text-transform: capitalize;">Dashboard <br><label style="font-weight: bold;line-height: 0.2;font-size: 12px;text-transform: capitalize;" class="text-muted">(<?php echo $user_name001; ?>)</label></h3>

      
    </div>

  </div>
  <div class="col-md-4">
  

        <div class="d-flex flex-row" style="overflow: auto;">
          <div class=" p-2" style="width: 100%;">

            <div class="counters" data-toggle="modal" data-target="#class_join">

              <div class="col-md-12">
                <center><span class="fa fa-user text-white text-center" style="font-size: 18px;"></span>
                  <br>
                  <small class="text-white" style="font-size: 20px;" id="count_student">0</small>
                  <br>
                  <small class="text-white text-center">Class Joined</small>
                </center>
              </div>
            </div>

          </div>
          <div class="p-2" style="width: 100%;">

            <div class="counters" data-toggle="modal" data-target="#count_visitor">

              <div class=" col-md-12">
                <center><span class="fa fa-bullhorn text-white text-center" style="font-size: 18px;"></span>
                  <br>
                  <small class="text-white" style="font-size: 20px;" id="visitor_count">00</small>
                  <br>
                  <small class="text-white text-center">Visitors</small>
                </center>
              </div>

            </div>
          </div>

      </div>


          <!-- Modal -->
          <div id="class_join" class="modal fade" role="dialog">
            <div class="modal-dialog">

              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Classes Joined</h4>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body row">


                      
                    <div class="col-md-6" style="margin-top: 8px;">
                      <div class="col-md-12" style="padding: 20px 20px 20px 20px;background-color: #000000b8;border-radius: 10px;height: 180px;">
                        <center><span class="fa fa-check-circle text-white text-center" style="font-size: 30px;"></span>
                          
                          <h1 class="text-white" id="count_student002">00</h1>
                          
                          <small class="text-white text-center" style="font-size: 18px;">Today Class Joined</small>
                        </center>
                      </div>
                    </div>

                      <div class="col-md-6" style="margin-top: 8px;">
                        <div class="col-md-12" style="padding: 20px 20px 20px 20px;background-color: #000000b8;border-radius: 10px;height: 180px;">
                          <center><span class="fa fa-arrow-up text-white text-center" style="font-size: 30px;"></span>
                            
                            <h1 class="text-white" id="yester_day_count">00</h1>
                            
                            <small class="text-white text-center" style="font-size: 16px;">Yesterday Class Joined</small>
                          </center>
                        </div>
                      </div>


                </div>
              </div>

            </div>
          </div>

           <div id="count_visitor" class="modal fade" role="dialog">
            <div class="modal-dialog">

              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Visitor Count</h4>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">



                  <div class="d-flex flex-row">

                    <div class="p-2" style="width: 50%;">
                      
                      <div class="col-md-12" style="padding: 20px 20px 20px 20px;background-color: #000000b8;border-radius: 10px;height: 180px;">
                        <center><span class="fa fa-arrow-up text-white text-center" style="font-size: 30px;"></span>
                          
                          <h1 class="text-white" id="visitor_count002">00</h1>
                          
                          <small class="text-white text-center" style="font-size: 18px;">Today Visitors</small>
                        </center>
                      </div>

                    </div>

                    <div class="p-2" style="width: 50%;">
                      

                      <div class="col-md-12" style="padding: 20px 20px 20px 20px;background-color: #000000b8;border-radius: 10px;height: 180px;">
                        <center><span class="fa fa-calendar text-white text-center" style="font-size: 30px;"></span>
                          
                          <h1 class="text-white" id="month_visitor_count">00</h1>
                          
                          <small class="text-white text-center" style="font-size: 18px;">Month Visitors</small>
                        </center>
                      </div>


                    </div>

                  </div>

                </div>
              </div>

            </div>
          </div>

  </div>

<script type="text/javascript">

  setInterval(function()
  { 

    var check_it = '1';

//class for joined students count
    $.ajax({
        url:'../admin/query/check.php',
        method:"POST",
        data:{check_joined_student:check_it},
        success:function(data)
        {
          //alert(data)
          document.getElementById('count_student').innerHTML = data;
          document.getElementById('count_student002').innerHTML = data;
          
        }
      });

//class for joined students count

//today visitor count
    $.ajax({
        url:'../admin/query/check.php',
        method:"POST",
        data:{check_visit_count:check_it},
        success:function(data)
        {
          //alert(data)
          document.getElementById('visitor_count').innerHTML = data;
          document.getElementById('visitor_count002').innerHTML = data;
          
        }
      });


//today visitor count

//yesterday visitor count
    $.ajax({
        url:'../admin/query/check.php',
        method:"POST",
        data:{yesterday_visitor_count:check_it},
        success:function(data)
        {
          //alert(data)
          document.getElementById('yesterday_visitor_count').innerHTML = data;
          
        }
      });

//yesterday visitor count

//Month visitor count
    $.ajax({
        url:'../admin/query/check.php',
        method:"POST",
        data:{month_visitor_count:check_it},
        success:function(data)
        {
          //alert(data)
          document.getElementById('month_visitor_count').innerHTML = data;
          
        }
      });

//Month visitor count

//Payment Details
    $.ajax({
        url:'../admin/query/check.php',
        method:"POST",
        data:{check_payment_details:check_it},
        success:function(data)
        {
          //alert(data)
          document.getElementById('payment_data').innerHTML = data;
          
        }
      });

//Payment Details


//Student Pending
    $.ajax({
        url:'../admin/query/check.php',
        method:"POST",
        data:{count_student_pending:check_it},
        success:function(data)
        {
          //alert(data)
          document.getElementById('stu_pending_count_data').innerHTML = data;
          
        }
      });

//Student Pending

//Teacher Pending
    $.ajax({
        url:'../admin/query/check.php',
        method:"POST",
        data:{count_teacher_pending:check_it},
        success:function(data)
        {
          //alert(data)
          document.getElementById('teach_pending_count_data').innerHTML = data;
          
        }
      });

//Teacher Pending


//student Approved
    $.ajax({
        url:'../admin/query/check.php',
        method:"POST",
        data:{student_approve_count_data:check_it},
        success:function(data)
        {
          //alert(data)
          document.getElementById('student_approve_count_data').innerHTML = data;
          
        }
      });

//student Approved


//teacher Approved
    $.ajax({
        url:'../admin/query/check.php',
        method:"POST",
        data:{teacher_approve_count_data:check_it},
        success:function(data)
        {
          //alert(data)
          document.getElementById('teacher_approve_count_data').innerHTML = data;
          
        }
      });

//teacher Approved



//SMS Count data
    $.ajax({
        url:'../admin/query/check.php',
        method:"POST",
        data:{sms_count_data:check_it},
        success:function(data)
        {
          //alert(data)
          document.getElementById('sms_count_data').innerHTML = data;
          
        }
      });

//SMS Count data




//check online status staff
  
  var user_id = document.getElementById('user_id').value;

  //alert(user_id);

    $.ajax({
        url:'../admin/query/check.php',
        method:"POST",
        data:{check_live_staff:user_id},
        success:function(data)
        {
         // alert(data)
          document.getElementById('show_active_status').innerHTML = data;
          
        }
      });

//check online status staff




  }, 1000);


//class for yesterday joined student count

  var yesterday_count = '1';

    $.ajax({
        url:'../admin/query/check.php',
        method:"POST",
        data:{yesterday_count:yesterday_count},
        success:function(data)
        {
         // alert(data)
          document.getElementById('yester_day_count').innerHTML = data;
          
        }
      });

//class for yesterday joined student count



</script>
</div>

<div class="col-md-12" id="show_active_status">
  
</div>


<div class="col-md-12">


        <div class="row">
                        <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;display: <?php echo $class_sms; ?>">
                            <a href="class_sms.php">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                                <center><span class="fa fa-comment" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 16px;">Class SMS</h6>
                              </div>
                            </a>
                         </div>


                        <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;display: <?php echo $student_search; ?>">
                            <a href="student_search.php">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                                <center><span class="fa fa-search" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 16px;">Student Search</h6>
                              </div>
                            </a>
                         </div>


                         <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;display: <?php echo $class_search; ?>">
                            <a href="class_search.php">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                                <center><span class="fa fa-thumb-tack" style="font-size: 80px;color: white;padding-bottom: 10px;transform: rotate(40deg);"></span></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 16px;">Class Search</h6>
                              </div>
                            </a>
                         </div>


                         <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;display: <?php echo $today_class; ?>">
                            <a href="today_class_search.php">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                                <center><span class="fa fa-calendar" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 16px;">Today Classes</h6>
                              </div>
                            </a>
                         </div>



                         <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;display: <?php echo $bank_details; ?>">
                            <a href="class_teacher_bank_data.php">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                                <center><span class="fa fa-bank" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 16px;">Bank Details</h6>
                              </div>
                            </a>
                         </div>


                         <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;display: <?php echo $todo_list; ?>">
                            <a href="todo_list.php">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                                <center><span class="fa fa-list" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 16px;">Todo List</h6>
                              </div>
                            </a>
                         </div>


                         <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;display: <?php echo $seminar; ?>">
                            <a href="seminar.php">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                                <center><span class="fa fa-users" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 16px;">Seminar</h6>
                              </div>
                            </a>
                         </div>

                         <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;display: <?php echo $realtime_payment; ?>">
                            <a href="realtime_payment.php">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                                <center><span class="fa fa-hourglass-start" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 16px;">Realtime Payment</h6>
                              </div>
                            </a>
                         </div>
                          
                          <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;display: <?php echo $payment_details; ?>">
                            <a href="payment_pending.php">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                                <div id="payment_data"></div>

                                <center><span class="fa fa-money" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 16px;">Payment Details</h6>
                              </div>
                            </a>
                         </div>

                         <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;display: <?php echo $class_register; ?>">
                            <a href="add_student_subjects.php">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                                <center><span class="fa fa-check-circle" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 16px;">Class Registeration</h6>
                              </div>
                            </a>
                         </div>

                         <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;display: <?php echo $setup_link; ?>">
                            <a href="setup_link.php" style="cursor: pointer;">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                                <center><span class="fa fa-link" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 16px;">Setup Links</h6>
                              </div>
                            </a>
                         </div>


                         <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;display: <?php echo $create_teacher; ?>">
                            <a href="create_teacher.php">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                                <?php 
                                    /*$sql001 = mysqli_query($conn,"SELECT * FROM teacher_login WHERE STATUS = 'Active'");
                                    $ch0 = mysqli_num_rows($sql001);
                                    
                                    if($ch0 == '0')
                                    {

                                    }else
                                    if($ch0<9)
                                    {
                                      echo '<div style="position: absolute;right: 12px;top:22px;border-radius: 80px;;height: 30px;padding: 7px 6px 8px 6px;color: black;" class="btn btn-default">0'.$ch0.'</div>';
                                      
                                    }else
                                    {
                                      echo '<div style="position: absolute;right: 12px;top:22px;border-radius: 80px;;height: 30px;padding: 7px 6px 8px 6px;color: black;" class="btn btn-default">'.$ch0.'</div>';
                                    }*/
                                   ?>

                                <center><span class="fa fa-plus" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 16px;">Create Teachers</h6>
                              </div>
                            </a>
                         </div>

                         <?php 

                            $sql0012 = mysqli_query($conn,"SELECT * FROM `institute` WHERE `INS_ID` = '1'");
                            $row0012=mysqli_fetch_assoc($sql0012);
                            $exam = $row0012['EXAM_TAB'];

                            if($position !== 'S_ADMIN')
                            {
                              if($exam == '0')
                              {
                                $display_exam_btn = 'none';
                              }else
                              if($exam == '1')
                              {
                                //Staff >> not give permission for show exam button , Others show
                                if($show_exam_access == 'none')
                                {
                                  $display_exam_btn = 'none';
                                }else
                                if($show_exam_access == 'show')
                                {
                                  $display_exam_btn = 'show';
                                }
                                
                              }
                            }else
                            if($position == 'S_ADMIN')
                            {
                                $display_exam_btn = 'show';
                              
                            }

                          ?>

                         

                         


            <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;display: <?php echo $student_pending; ?>">
                            <a href="student_pending.php">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                                <div id="stu_pending_count_data"></div>

                                <center><span class="fa fa-spinner" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 16px;">STUDENTS PENDING</h6>
                              </div>


                            </a>
                         </div>


                         <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;display: <?php echo $teacher_pending; ?>">
                            <a href="teacher_pending.php">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">


                                <div id="teach_pending_count_data"></div>

                                <center><span class="fa fa-spinner" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 16px;">Teachers Pending</h6>
                              </div>
                            </a>
                         </div>

                         <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;display: <?php echo $student_approve; ?>">
                            <a href="student_approved.php">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                                  <div id="student_approve_count_data"></div>
                                
                                <center><span class="fa fa-check" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 16px;">Students Approved</h6>
                              </div>
                            </a>
                         </div>

                         <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;display: <?php echo $teacher_approve; ?>">
                            <a href="teacher_approved.php">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">
                                
                                <div id="teacher_approve_count_data"></div>

                                <center><span class="fa fa-check" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 16px;">Teachers Approved</h6>
                              </div>
                            </a>
                         </div>

                          <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;display: none;">
                            <a href="student_reactive.php">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">


                                <?php 
                                    $sql001 = mysqli_query($conn,"SELECT * FROM stu_login WHERE STATUS = 'delete'");
                                    $ch0 = mysqli_num_rows($sql001);
                                    
                                    if($ch0 == '0')
                                    {

                                    }else
                                    if($ch0<9)
                                    {
                                      echo '<div style="position: absolute;right: 12px;top:22px;border-radius: 80px;;height: 30px;padding: 7px 6px 8px 6px;color: black;" class="btn btn-default count">0'.$ch0.'</div>';
                                      
                                    }else
                                    {
                                      echo '<div style="position: absolute;right: 12px;top:22px;border-radius: 80px;;height: 30px;padding: 7px 6px 8px 6px;color: black;" class="btn btn-default count">'.$ch0.'</div>';
                                    }

                                   ?>

                                <center><img src="images/icon/reactive_student.png" class="image-responsive dash_img" height="100px"></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 16px;">Students Reactive</h6>
                              </div>
                            </a>
                         </div>

                         <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;display: none">
                            <a href="teacher_reactive.php">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                                <?php 
                                    $sql001 = mysqli_query($conn,"SELECT * FROM teacher_login WHERE STATUS = 'delete'");
                                    $ch0 = mysqli_num_rows($sql001);
                                    
                                    if($ch0 == '0')
                                    {

                                    }else
                                    if($ch0<9)
                                    {
                                      echo '<div style="position: absolute;right: 12px;top:22px;border-radius: 80px;;height: 30px;padding: 7px 6px 8px 6px;color: black;" class="btn btn-default count">0'.$ch0.'</div>';
                                      
                                    }else
                                    {
                                      echo '<div style="position: absolute;right: 12px;top:22px;border-radius: 80px;;height: 30px;padding: 7px 6px 8px 6px;color: black;" class="btn btn-default count">'.$ch0.'</div>';
                                    }

                                   ?>

                                <center><img src="images/icon/teacher_reactive.png" class="image-responsive dash_img" height="100px"></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 16px;">Teachers Reactive</h6>
                              </div>
                            </a>
                         </div>


                         
                         <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;display: <?php echo $sms; ?>">
                            <a href="sms.php">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                                <div id="sms_count_data"></div>

                                <center><span class="fa fa-envelope" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 16px;">SMS</h6>
                              </div>
                            </a>
                         </div>
          
          
                         <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;display: <?php echo $google_form; ?>">
                            <a href="google_form.php">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                                <center><span class="fa fa-google" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 16px;">Google Form</h6>
                              </div>
                            </a>
                         </div>

                         <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;display: <?php echo $show_exam_access; ?>">
                            <a href="exam.php">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                                <center><span class="fa fa-pencil" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 16px;">Online Examination</h6>
                              </div>
                            </a>
                         </div>

                         

                         <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;display: <?php echo $create_level; ?>">
                            <a href="add_level.php">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                                <?php 
                                    $sql001 = mysqli_query($conn,"SELECT * FROM level");
                                    $ch0 = mysqli_num_rows($sql001);
                                    
                                    if($ch0 == '0')
                                    {

                                    }else
                                    if($ch0<9)
                                    {
                                      echo '<div style="position: absolute;right: 12px;top:22px;border-radius: 80px;;height: 30px;padding: 7px 6px 8px 6px;color: black;" class="btn btn-default">0'.$ch0.'</div>';
                                      
                                    }else
                                    {
                                      echo '<div style="position: absolute;right: 12px;top:22px;border-radius: 80px;;height: 30px;padding: 7px 6px 8px 6px;color: black;" class="btn btn-default">'.$ch0.'</div>';
                                    }

                                   ?>

                                <center><span class="fa fa-sitemap" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 16px;">Create Level</h6>
                              </div>
                            </a>
                         </div>

                         <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;display: <?php echo $create_subject; ?>">
                            <a href="add_subject.php">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                                <?php 
                                    $sql001 = mysqli_query($conn,"SELECT * FROM subject");
                                    $ch0 = mysqli_num_rows($sql001);
                                    
                                    if($ch0 == '0')
                                    {

                                    }else
                                    if($ch0<9)
                                    {
                                      echo '<div style="position: absolute;right: 12px;top:22px;border-radius: 80px;;height: 30px;padding: 7px 6px 8px 6px;color: black;" class="btn btn-default">0'.$ch0.'</div>';
                                      
                                    }else
                                    {
                                      echo '<div style="position: absolute;right: 12px;top:22px;border-radius: 80px;;height: 30px;padding: 7px 6px 8px 6px;color: black;" class="btn btn-default">'.$ch0.'</div>';
                                    }

                                   ?>

                                <center><span class="fa fa-slack" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 16px;">Create Subjects</h6>
                              </div>
                            </a>
                         </div>

                          <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;display: <?php echo $create_class; ?>">
                            <a href="new_class.php" style="cursor: pointer;">

                              <!-- data-toggle="modal" data-target="#new_class" -->

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                                <?php 
                                    $sql001 = mysqli_query($conn,"SELECT * FROM classes");
                                    $ch0 = mysqli_num_rows($sql001);
                                    
                                    if($ch0 == '0')
                                    {

                                    }else
                                    if($ch0<9)
                                    {
                                      echo '<div style="position: absolute;right: 12px;top:22px;border-radius: 80px;;height: 30px;padding: 7px 6px 8px 6px;color: black;" class="btn btn-default">0'.$ch0.'</div>';
                                      
                                    }else
                                    {
                                      echo '<div style="position: absolute;right: 12px;top:22px;border-radius: 80px;;height: 30px;padding: 7px 6px 8px 6px;color: black;" class="btn btn-default">'.$ch0.'</div>';
                                    }

                                   ?>

                                <center><span class="fa fa-star" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 16px;">Create Classes</h6>
                              </div>
                            </a>


                            <div class="modal fade slide-up disable-scroll" id="new_class" tabindex="-1" role="dialog" aria-hidden="false" style="overflow: auto;">
                            <div class="modal-dialog ">
                            <div class="modal-content-wrapper">
                            <div class="modal-content">
                            <div class="modal-header clearfix text-left">
                            <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
                            </button>
                            <h5>Select Teacher</h5>
                            </div>
                            <div class="modal-body">
                              <form action="new_class.php" method="GET">

                              <div class="row">
                              <div class="col-md-12">
                              <div class="form-group form-group-default input-group">
                              <div class="form-input-group">
                               <label>Teacher Name</label>
                                  <select class="form-control select_class" name="teach_id" id="teach_id" required style="cursor: pointer;">
                                    <option value="">Select Teacher Name </option>

                                      <?php 

                                          $sql0015 = mysqli_query($conn,"SELECT * FROM teacher_details");
                                          while($row0015=mysqli_fetch_assoc($sql0015))
                                          {
                                            $name = $row0015['POSITION'].". ".$row0015['F_NAME']." ".$row0015['L_NAME'];
                                            $teach_id = $row0015['TEACH_ID'];

                                            echo '<option value='.$teach_id.'>'.$name.'</option>';

                                          }

                                           ?>
                                    
                                  </select>
                              </div>
                              </div>
                              </div>
                                <button type="submit" class="btn btn-success btn-block btn-lg" ><i class="pg-icon">search</i> Search</button>
                              </div>

                              
                              </form>
                              </div>
                              </div>
                              </div>
                              </div>

                              </div>


                         </div>

                         <?php 

                            $sql0011 = mysqli_query($conn,"SELECT * FROM `institute` WHERE `INS_ID` = '1'");
                            $row0011=mysqli_fetch_assoc($sql0011);
                            $free_period_active = $row0011['FREE'];
                            $exam = $row0011['EXAM_TAB'];

                            $cur = 'cursor: pointer;';
                            $msg = '';

                            if($free_period_active == 'Yes')
                            {
                              $disable_btn = ';opacity:0.6;pointer-events:none;';
                              $cur = 'cursor:not-allowed;';
                              $msg = '<label style="position:absolute;top:50%;opacity:12;font-size:18px;background-color:black;color:white;padding:6px 8px 6px 8px;" class="btn-block text-center">Free Period Started!</label>';
                            }else
                            if($free_period_active == 'No')
                            {
                              $disable_btn = '';
                            }

                          ?>

                          <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;display: <?php echo $free_class; ?>">
                            <a href="free_classes.php">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                                <?php 
                                    $sql001 = mysqli_query($conn,"SELECT * FROM `classes` WHERE `FREE` = '1'");
                                    $ch0 = mysqli_num_rows($sql001);
                                    
                                    if($ch0 == '0')
                                    {

                                    }else
                                    if($ch0<9)
                                    {
                                      echo '<div style="position: absolute;right: 12px;top:22px;border-radius: 80px;;height: 30px;padding: 7px 6px 8px 6px;color: black;" class="btn btn-default">0'.$ch0.'</div>';
                                      
                                    }else
                                    {
                                      echo '<div style="position: absolute;right: 12px;top:22px;border-radius: 80px;;height: 30px;padding: 7px 6px 8px 6px;color: black;" class="btn btn-default">'.$ch0.'</div>';
                                    }

                                   ?>

                                <center><span class="fa fa-cloud" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 16px;">Free Classes</h6>

                                
                              </div>
                            </a>


                         </div>

                         <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;display: <?php echo $free_card; ?>">
                            <a href="free_card.php">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                                <center><span class="fa fa-flag-checkered" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 16px;">Free Card</h6>

                                
                              </div>
                            </a>


                         </div>


                         <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;display: <?php echo $new_upload; ?>">
                            <a data-toggle="modal" data-target="#new_upload" style="cursor: pointer;">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                                <center><span class="fa fa-upload" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 16px;">New Uploads</h6>
                              </div>
                            </a>

                            <div class="modal fade slide-up disable-scroll" id="new_upload" tabindex="-1" role="dialog" aria-hidden="false" style="overflow: auto;">
                            <div class="modal-dialog ">
                            <div class="modal-content-wrapper">
                            <div class="modal-content">
                            <div class="modal-header clearfix text-left">
                            <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
                            </button>
                            <h5>Select Teacher</h5>
                            </div>
                            <div class="modal-body">
                              <form action="upload.php" method="GET">

                              <div class="row">
                              <div class="col-md-12">
                              <div class="form-group form-group-default input-group">
                              <div class="form-input-group">
                               <label>Teacher Name</label>
                                  <select class="form-control select_class" name="teach_id" id="teach_id" required style="cursor: pointer;">
                                    <option value="">Select Teacher Name </option>

                                      <?php 

                                          $sql0015 = mysqli_query($conn,"SELECT * FROM teacher_details");
                                          while($row0015=mysqli_fetch_assoc($sql0015))
                                          {
                                            $name = $row0015['POSITION'].". ".$row0015['F_NAME']." ".$row0015['L_NAME'];
                                            $teach_id = $row0015['TEACH_ID'];

                                            echo '<option value='.$teach_id.'>'.$name.'</option>';

                                          }

                                           ?>
                                    
                                  </select>
                              </div>
                              </div>
                              </div>
                                <button type="submit" class="btn btn-success btn-block btn-lg" ><i class="pg-icon">search</i> Search</button>
                              </div>

                              
                              </form>
                              </div>
                              </div>
                              </div>
                              </div>

                              </div>


                         </div>

                         <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;display: <?php echo $view_upload; ?>">
                            <a data-toggle="modal" data-target="#view_upload" style="cursor: pointer;">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                                <?php 
                                    $sql001 = mysqli_query($conn,"SELECT * FROM uploads");
                                    $ch0 = mysqli_num_rows($sql001);
                                    
                                    if($ch0 == '0')
                                    {

                                    }else
                                    if($ch0<9)
                                    {
                                      echo '<div style="position: absolute;right: 12px;top:22px;border-radius: 80px;;height: 30px;padding: 7px 6px 8px 6px;color: black;" class="btn btn-default">0'.$ch0.'</div>';
                                      
                                    }else
                                    {
                                      echo '<div style="position: absolute;right: 12px;top:22px;border-radius: 80px;;height: 30px;padding: 7px 6px 8px 6px;color: black;" class="btn btn-default">'.$ch0.'</div>';
                                    }

                                   ?>

                                <center><span class="fa fa-check" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 16px;">View Uploads</h6>
                              </div>
                            </a>

                             <div class="modal fade slide-up disable-scroll" id="view_upload" tabindex="-1" role="dialog" aria-hidden="false" style="overflow: auto;">
                            <div class="modal-dialog ">
                            <div class="modal-content-wrapper">
                            <div class="modal-content">
                            <div class="modal-header clearfix text-left">
                            <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
                            </button>
                            <h5>Select Teacher</h5>
                            </div>
                            <div class="modal-body">
                              <form action="view_upload.php" method="GET">

                              <div class="row">
                              <div class="col-md-12">
                              <div class="form-group form-group-default input-group">
                              <div class="form-input-group">
                               <label>Teacher Name</label>
                                  <select class="form-control select_class" name="teach_id" id="teach_id" required style="cursor: pointer;">
                                    <option value="">Select Teacher Name </option>

                                      <?php 

                                          $sql0015 = mysqli_query($conn,"SELECT * FROM teacher_details");
                                          while($row0015=mysqli_fetch_assoc($sql0015))
                                          {
                                            $name = $row0015['POSITION'].". ".$row0015['F_NAME']." ".$row0015['L_NAME'];
                                            $teach_id = $row0015['TEACH_ID'];

                                            echo '<option value='.$teach_id.'>'.$name.'</option>';

                                          }

                                           ?>
                                    
                                  </select>
                              </div>
                              </div>
                              </div>
                                <button type="submit" class="btn btn-success btn-block btn-lg" ><i class="pg-icon">search</i> Search</button>
                              </div>

                              
                              </form>
                              </div>
                              </div>
                              </div>
                              </div>

                              </div>


                         </div>

                         <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;display: <?php echo $alert_manage; ?>">
                            <a href="alert.php">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                                <center><span class="fa fa-bullhorn" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 16px;">Alert Management</h6>
                              </div>
                            </a>
                         </div>

                         
                         <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;display: <?php echo $blog_post; ?>">
                            <a href="blog_post.php">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                                <center><span class="fa fa-bell" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 16px;">Blog Post</h6>
                              </div>
                            </a>
                         </div>

                         <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;display: <?php echo $student_details; ?>">
                            <a href="student_email.php">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                                <?php 
                                    /*$sql001 = mysqli_query($conn,"SELECT * FROM stu_login WHERE STATUS = 'Active'");
                                    $ch0 = mysqli_num_rows($sql001);
                                    
                                    if($ch0 == '0')
                                    {

                                    }else
                                    if($ch0<9)
                                    {
                                      echo '<div style="position: absolute;right: 12px;top:22px;border-radius: 80px;;height: 30px;padding: 7px 6px 8px 6px;color: black;" class="btn btn-default">0'.$ch0.'</div>';
                                      
                                    }else
                                    {
                                      echo '<div style="position: absolute;right: 12px;top:22px;border-radius: 80px;;height: 30px;padding: 7px 6px 8px 6px;color: black;" class="btn btn-default">'.$ch0.'</div>';
                                    }*/

                                   ?>

                                
                                <center><span class="fa fa-drivers-license" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 16px;">Student Details</h6>
                              </div>
                            </a>
                         </div>

                         
                         <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;display: <?php echo $create_admin; ?>">
                            <a href="create_admin.php">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                                <?php 
                                    $sql001 = mysqli_query($conn,"SELECT * FROM admin_login WHERE STATUS = 'Active' AND TYPE = 'ADMIN'");
                                    $ch0 = mysqli_num_rows($sql001);
                                    
                                    if($ch0 == '0')
                                    {

                                    }else
                                    if($ch0<9)
                                    {
                                      echo '<div style="position: absolute;right: 12px;top:22px;border-radius: 80px;;height: 30px;padding: 7px 6px 8px 6px;color: black;" class="btn btn-default">0'.$ch0.'</div>';
                                      
                                    }else
                                    {
                                      echo '<div style="position: absolute;right: 12px;top:22px;border-radius: 80px;;height: 30px;padding: 7px 6px 8px 6px;color: black;" class="btn btn-default">'.$ch0.'</div>';
                                    }

                                   ?>

                                <center><span class="fa fa-plus" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 16px;">Create Admin</h6>
                              </div>
                            </a>
                         </div>

                         <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;display: <?php echo $create_staff; ?>">
                            <a href="create_staff.php">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                                <?php 
                                    $sql001 = mysqli_query($conn,"SELECT * FROM admin_login WHERE STATUS = 'Active' AND `TYPE` = 'STAFF'");
                                    $ch0 = mysqli_num_rows($sql001);
                                    
                                    if($ch0 == '0')
                                    {

                                    }else
                                    if($ch0<9)
                                    {
                                      echo '<div style="position: absolute;right: 12px;top:22px;border-radius: 80px;;height: 30px;padding: 7px 6px 8px 6px;color: black;" class="btn btn-default">0'.$ch0.'</div>';
                                      
                                    }else
                                    {
                                      echo '<div style="position: absolute;right: 12px;top:22px;border-radius: 80px;;height: 30px;padding: 7px 6px 8px 6px;color: black;" class="btn btn-default">'.$ch0.'</div>';
                                    }

                                   ?>

                                <center><span class="fa fa-plus" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 16px;">Create Staff</h6>
                              </div>
                            </a>
                         </div>




                         <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;display: <?php echo $password_reset01; ?>">
                            <a href="student_psw_reset.php">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                                <center><span class="fa fa-asterisk" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 16px;">Password Reset</h6>
                              </div>
                            </a>
                         </div>

                         <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;display: <?php echo $erase_duplicate; ?>">
                            <a href="erase_duplicate.php">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                                <center><span class="fa fa-eraser" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 16px;">Erase Duplicate</h6>
                              </div>
                            </a>
                         </div>


                          <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;display: <?php echo $reason; ?>">
                            <a href="reason.php">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                                <center><span class="fa fa-question-circle" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 16px;">Reason</h6>
                              </div>
                            </a>
                         </div>


                         <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;display: <?php echo $income; ?>">
                            <a href="income.php">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                                <center><span class="fa fa-arrow-circle-up" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 16px;">Income</h6>
                              </div>
                            </a>
                         </div>


                         <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;display: <?php echo $expenditure; ?>">
                            <a href="expenditure.php">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                                <center><span class="fa fa-arrow-circle-down" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 16px;">Expenditure</h6>
                              </div>
                            </a>
                         </div>



                          <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;">
                            <a href="profile_setting.php">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">


                                <center><span class="fa fa-wrench" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 16px;">Settings</h6>
                              </div>
                            </a>
                         </div>



                         <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;display: <?php echo $payment_report; ?>">
                            <a href="student_status.php">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                                <center><span class="fa fa-file" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 16px;">Payment Reports</h6>
                              </div>
                            </a>
                         </div>

                         <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;display: <?php echo $click_report; ?>">
                            <a href="click_status.php">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                                <center><span class="fa fa-file" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 16px;">Clicks Reports</h6>
                              </div>
                            </a>
                         </div>

                         <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;display: <?php echo $upload_click_report; ?>">
                            <a href="upload_click_status.php">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                                <center><span class="fa fa-file" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 14px;padding-bottom: 2px;">Upload Clicks Reports</h6>
                              </div>
                            </a>
                         </div>

                         <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;display: <?php echo $login_report; ?>">
                            <a href="login_status.php">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                                <center><span class="fa fa-file" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 16px;">Login Reports</h6>
                              </div>
                            </a>
                         </div>


                         <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;display: <?php echo $class_register_report; ?>">
                            <a data-toggle="modal" data-target="#class_list" style="cursor: pointer;">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                                <center><span class="fa fa-file" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 14px;padding-bottom: 2px;">Class Register Report</h6>
                              </div>
                            </a>

                            <div class="modal fade slide-up disable-scroll" id="class_list" tabindex="-1" role="dialog" aria-hidden="false" style="overflow: auto;">
                            <div class="modal-dialog ">
                            <div class="modal-content-wrapper">
                            <div class="modal-content">
                            <div class="modal-header clearfix text-left">
                            <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
                            </button>
                            <h5>Select Teacher</h5>
                            </div>
                            <div class="modal-body">
                              <form action="student_register_report.php" method="GET" target="_blank_">

                              <div class="row">
                                <div class="col-md-12">
                              <div class="form-group form-group-default" style="margin-top: 10px;">
                              <label>Teacher Name</label>
                              <select class="form-control select_class" name="teacher_id" id="teacher_id123" required style="cursor: pointer;">
                                <option value="">Select Teacher Name </option>

                                  <?php 

                                      $sql0015 = mysqli_query($conn,"SELECT * FROM teacher_details");
                                      while($row0015=mysqli_fetch_assoc($sql0015))
                                      {
                                        $name = $row0015['F_NAME']." ".$row0015['L_NAME'];
                                        $teach_id = $row0015['TEACH_ID'];

                                        echo '<option value='.$teach_id.'>'.$name.'</option>';

                                      }

                                       ?>
                                
                              </select>

                            </div>
                            
                            <div class="form-group form-group-default" style="margin-top: 10px;">
                              <label>Classes</label>
                              
                              <select class="form-control select_class" name="class_id" id="level_clz" required style="cursor: pointer;">
                                <option value="">Select Class Name </option>
                                
                              </select>

                            </div>
                                <button type="submit" class="btn btn-success btn-block btn-lg" ><i class="pg-icon">search</i> Search</button>
                              </div>

                              </div>

                              
                              </form>
                              </div>
                              </div>
                              </div>
                              </div>

                              </div>


                         </div>

                        <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;display: <?php echo $subject_register_report; ?>">
                            <a data-toggle="modal" data-target="#subject_register_modal" style="cursor: pointer;">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                                <center><span class="fa fa-file" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 14px;padding-bottom: 2px;">Subject Register Report</h6>
                              </div>
                            </a>

                            <div class="modal fade slide-up disable-scroll" id="subject_register_modal" tabindex="-1" role="dialog" aria-hidden="false" style="overflow: auto;">
                            <div class="modal-dialog ">
                            <div class="modal-content-wrapper">
                            <div class="modal-content">
                            <div class="modal-header clearfix text-left">
                            <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
                            </button>
                            <h5>Select Subject Name</h5>
                            </div>
                            <div class="modal-body">
                              <form action="subject_register_report.php" method="GET" target="_blank_">

                              <div class="row">
                                <div class="col-md-12">
                              <div class="form-group form-group-default" style="margin-top: 10px;">
                              <label>Teacher Name</label>
                              <select class="form-control select_class" name="subject_id" required style="cursor: pointer;">
                                <option value="">Select Subject Name </option>

                                  <?php 

                                      $sql0015 = mysqli_query($conn,"SELECT * FROM subject");
                                      while($row0015=mysqli_fetch_assoc($sql0015))
                                      {
                                        $subject_name = $row0015['SUBJECT_NAME'];
                                        $subject_id = $row0015['SUB_ID'];

                                        echo '<option value='.$subject_id.'>'.$subject_name.'</option>';

                                      }

                                       ?>
                                
                              </select>

                            </div>
                                <button type="submit" class="btn btn-success btn-block btn-lg" ><i class="pg-icon">search</i> Search</button>
                              </div>

                              </div>

                              
                              </form>
                              </div>
                              </div>
                              </div>
                              </div>

                              </div>


                         </div>


                         <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;display: <?php echo $daily_access_report; ?>">
                            <a href="daily_click_status.php">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                                <center><span class="fa fa-file" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 16px;">Daily Access Reports</h6>
                              </div>
                            </a>
                         </div>


                         <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;display: <?php echo $class_attend_report; ?>">
                            <a href="class_attend.php">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                                <center><span class="fa fa-file" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 14px;">CLass Attend Reports</h6>
                              </div>
                            </a>
                         </div>

                         <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;display: <?php echo $total_class_register_report; ?>">
                            <a data-toggle="modal" data-target="#register_list" style="cursor: pointer;">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                                <center><span class="fa fa-file" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 12px;">Total CLass Register Reports</h6>
                              </div>
                            </a>

                            <div class="modal fade slide-up disable-scroll" id="register_list" tabindex="-1" role="dialog" aria-hidden="false" style="overflow: auto;">
                            <div class="modal-dialog ">
                            <div class="modal-content-wrapper">
                            <div class="modal-content">
                            <div class="modal-header clearfix text-left">
                            <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
                            </button>
                            <h5>Total Class Register Reports</h5>
                            </div>
                            <div class="modal-body">
                              <form action="student_clz_register_report.php" method="POST" target="_blank_">

                              <div class="row">
                                <div class="col-md-12">
                              <div class="form-group form-group-default" style="margin-top: 10px;">
                              <label>Regsitered Status</label>
                              <select class="form-control select_class" name="register_status" required style="cursor: pointer;">
                                <option value="0">Not Registered Student </option>
                                <option value="1">Registered Student </option>
                                
                              </select>

                            </div>
                                <button type="submit" class="btn btn-success btn-block btn-lg" ><i class="pg-icon">search</i> Search</button>
                              </div>

                              </div>

                              
                              </form>
                              </div>
                              </div>
                              </div>
                              </div>

                              </div>
                         </div>

                         <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;display: <?php echo $held_class_report; ?>">
                            <a data-toggle="modal" data-target="#held_class_list" style="cursor: pointer;">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                                <center><span class="fa fa-file" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 14px;padding-bottom: 2px;">Held Class Report</h6>
                              </div>
                            </a>

                            <div class="modal fade slide-up disable-scroll" id="held_class_list" tabindex="-1" role="dialog" aria-hidden="false" style="overflow: auto;">
                            <div class="modal-dialog ">
                            <div class="modal-content-wrapper">
                            <div class="modal-content">
                            <div class="modal-header clearfix text-left">
                            <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
                            </button>
                            <h5>Search Held Class</h5>
                            </div>
                            <div class="modal-body">
                              <form action="class_held_report.php" method="POST" target="_blank_">

                              <div class="row">
                                <div class="col-md-12">
                                  
                                  <div class="form-group form-group-default" style="margin-top: 10px;">
                                    <label>Start Date</label>
                                    <input type="date" value="<?php echo date('Y-m-01') ?>" name="start_date" class="form-control">
                                  </div>

                                  <div class="form-group form-group-default" style="margin-top: 10px;">
                                    <label>End Date</label>
                                    <input type="date" value="<?php echo date('Y-m-01') ?>" name="end_date" class="form-control">
                                  </div>

                                <button type="submit" class="btn btn-success btn-block btn-lg" ><i class="pg-icon">search</i> Search</button>
                              </div>

                              </div>

                              
                              </form>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

<?php  include('footer/footer.php'); ?>


<script>
        $(function(){
            $("a.hidelink").each(function (index, element){
                var href = $(this).attr("href");
                $(this).attr("hiddenhref", href);
                $(this).removeAttr("href");
            });
            $("a.hidelink").click(function(){
                url = $(this).attr("hiddenhref");
                window.open(url, '_blank');
            })
        });
    </script>




<script type="text/javascript">
  $(document).ready(function(){
    $('#teacher_id123').change(function(){

                   var teach_id = $('#teacher_id123').val();

                   $.ajax({
                    url:'../admin/query/check.php',
                    method:"POST",
                    data:{get_classes_data:teach_id},
                    success:function(data)
                    {
                      //alert(data)
                      $('#level_clz').html(data);

                      
                    }
                   });
                 

                });
  })




</script>
