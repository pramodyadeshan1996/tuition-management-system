<?php
	$page = "Student Status";
	$folder_in = '0';

  
      include('header/header.php');

      $level_id = $_POST['level'];
      $teach_id = $_POST['teach_id'];
      $sub_id = $_POST['subject'];
      $class_id = $_POST['class_id'];


        $s04 = mysqli_query($conn,"SELECT * FROM teacher_details WHERE TEACH_ID = '$teach_id'");
        while($row4 = mysqli_fetch_assoc($s04))
        {
          $teacher_name = $row4['POSITION'].". ".$row4['F_NAME']." ".$row4['L_NAME'];
          $teacher_name2 = $row4['F_NAME']." ".$row4['L_NAME'];

          $t_pic = $row4['PICTURE'];

          if($t_pic == '0')
          {
          	$t_pic = 'teacher.png';
          }
        }

         $sql0015 = mysqli_query($conn,"SELECT * FROM level WHERE LEVEL_ID = '$level_id'");
        while($row0015=mysqli_fetch_assoc($sql0015))
        {
          $level_name = $row0015['LEVEL_NAME'];

        }

        $sql00151 = mysqli_query($conn,"SELECT * FROM subject WHERE SUB_ID = '$sub_id'");
        while($row00151=mysqli_fetch_assoc($sql00151))
        {
          $subject_name = $row00151['SUBJECT_NAME'];

        }

        $sql00152 = mysqli_query($conn,"SELECT * FROM classes WHERE CLASS_ID = '$class_id'");
        while($row00152=mysqli_fetch_assoc($sql00152))
        {
          $clz_name = $row00152['CLASS_NAME'];

        }

 ?>

<ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-bottom: 04%;margin-bottom: 0;">
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
       <li class="breadcrumb-item" data-toggle="tooltip" data-title="New Student Status(Search)"> <a href="student_status.php">Student Status (Search)</a></li>
       <li class="breadcrumb-item" data-toggle="tooltip" data-title="New Student Status(View Student)"> Student Status (Search)</li>
    </ol>


  <div class="col-md-12">
    
    <div class="row" style="padding-top: 10px;">
      <div class="col-md-1"></div>
      <div class="col-md-10 bg-white" style="border:1px solid #cccc;height: auto;">
      
      <div class="col-md-12" style="border-bottom: 1px solid #cccc;">
        <div class="row">

            <div class="col-md-9" style="margin-bottom: 10px;"><h2>Student Status</h2><?php echo $level_name; ?><span class="fa fa-angle-right"></span> <?php echo $subject_name; ?> <span class="fa fa-angle-right"></span> <?php echo $clz_name; ?></div>
            <div class="col-md-3" style="padding-top: 20px;">
            </div>

        </div>
      </div>
 <div class="col-md-12" style="margin-top: 10px;">
<div class="row" style="border-bottom: 1px solid #cccc;padding-bottom: 15px;">
    <div class="col-md-7">
        <form action="clz_fees_report.php" method="POST" target="_blank">

          <input type="hidden" name="class_id" value="<?php echo $class_id; ?>">

          <div class="row">
            <div class="col-md-3" style="margin-top: 10px;"><select class="form-control" name="year" required>
              <option value="">Year</option>
              <?php 
                    
                    $year = date('Y');
                    $few = strtotime("-6 years");
                    $few_5year = date('Y',$few);
                    for($year;$year>$few_5year;$year--)
                    {
                      echo '<option value="'.$year.'">'.$year.'</value>';
                    }

                  ?>
            </select></div>
            <div class="col-md-3" style="margin-top: 10px;"><select class="form-control" name="month" required>
              <option value="">Month</option>
              <?php 
                    
                    for($m=1;$m<13;$m++)
                    {
                       $month = $m;
                       $mon_name = date('F', mktime(0, 0, 0, $month, 10)); // March
                      echo '<option value="'.$m.'">'.$mon_name.'</value>';
                    }

                  ?>
            </select>
          </div>

          <div class="col-md-3" style="margin-top: 10px;"><select class="form-control" name="pay_type" required>
              <option value="">Payment Type</option>
              <option value="1">Paid Students</option>
              <option value="0">Not Paid Students</option>
            </select>
          </div>
            <div class="col-md-1" style="margin-top: 10px;"> <button type="submit" class="btn btn-success btn-block" style="border-radius: 80px;padding: 8px 10px 8px 10px;" name="print_class_fees"><span class="fa fa-print"></span></button></div>
          </div>
          </form>

    </div>

    <div class="col-md-1"></div>
    <div class="col-md-4" style="padding-top: 8px">
        <input type="text" id="search-table" class="form-control pull-right" style="width: 100%;" placeholder="Search" data-toggle="tooltip" data-title="අවශ්‍ය දත්ත සෙවීම'">
      
  </div>
</div>
</div>

<?php 

  $sql0011 = mysqli_query($conn,"SELECT * FROM transactions WHERE CLASS_ID = '$class_id' AND TEACH_ID = '$teach_id'");
  $count = mysqli_num_rows($sql0011);

 ?>


<div class="table-responsive" style="height: 800px;overflow: auto;margin-top: 4px;">
<table class="table demo-table-search table-responsive-block text-left" id="tableWithSearch">
<thead>
<tr>
<th style="width: 20%;">Registered ID</th>
<th style="width: 20%;">Student Name <span class="label label-success"><?php echo $count; ?></span></th>
<th style="width: 20%;">Last Payment</th>
<th style="text-align: center;">Action</th>
</tr>
</thead>
<tbody id="myTable">

<tr class="no-data col-md-12 alert alert-danger" style="margin-top: 20px;display: none;">
  <td><span class="fa fa-warning"></span> Not Found Data</td>
</tr>

<?php 
$last_payment = 0;

$sql001 = mysqli_query($conn,"SELECT * FROM transactions WHERE CLASS_ID = '$class_id' AND TEACH_ID = '$teach_id'");
while($row001=mysqli_fetch_assoc($sql001))
{
  $stu_id = $row001['STU_ID'];
  $reg_date = $row001['UPDATE_TIME'];

   $sql0033 = mysqli_query($conn,"SELECT * FROM student_details WHERE STU_ID = '$stu_id'");
                                  while($row0033=mysqli_fetch_assoc($sql0033))
                                  {
                                    $f = $row0033['F_NAME'];
                                    $l = $row0033['L_NAME'];

                                    $pic = $row0033['PICTURE'];
                                    $gen = $row0033['GENDER'];
                                    $dob = $row0033['DOB'];
                                    $tp = $row0033['TP'];
                                    $address = $row0033['ADDRESS'];
                                    $email = $row0033['EMAIL'];

                                    if($pic == '0')
                                    {
                                      if($gen == 'Male')
                                      {
                                        $pic = 'boy.png';
                                      }else
                                      if($gen == 'Female')
                                      {
                                        $pic = 'girl.png';
                                      }
                                      
                                    }

                                     $sql0034 = mysqli_query($conn,"SELECT * FROM stu_login WHERE STU_ID = '$stu_id'");
                                    while($row0034=mysqli_fetch_assoc($sql0034))
                                    {
                                      $reg_id = $row0034['REGISTER_ID'];
                                    }
                                  }

                                  /*$sql0035 = mysqli_query($conn,"SELECT * FROM payment_data WHERE STU_ID = '$stu_id'");
                                  $row0035=mysqli_fetch_assoc($sql0035);
                                  $last_payment = $row0035['PAY_TIME'];

                                  if($last_payment == '0')
                                  {
                                    $last_payment = 'N/A';
                                  }*/
                                  

            echo '<tr>

                    <td class="v-align-middle">'.$reg_id.'</td>

                    <td class="v-align-middle">'.$f.' '.$l.'</td>

                    <td class="v-align-middle">'.$reg_date.'</td>

                    <td class="v-align-middle">

                    <center>
                    <a data-toggle="tooltip" data-title="Student Profile"><button type="button" class="btn btn-danger  btn-xs btn-rounded" data-target="#profile'.$stu_id.'" data-toggle="modal" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;"><i class="pg-icon">user</i></button></a>


                    <a data-toggle="tooltip" data-title="All Payments"><button type="button" class="btn btn-success  btn-xs btn-rounded" data-target="#payments'.$stu_id.'" data-toggle="modal" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;"><i class="pg-icon">tick</i></button></a></center>

                    ';


                  echo '

                    <div class="modal fade slide-up disable-scroll" id="profile'.$stu_id.'" tabindex="-1" role="dialog" aria-hidden="false">
                      <div class="modal-dialog ">
                      <div class="modal-content-wrapper">
                      <div class="modal-content modal-lg">
                      <div class="modal-header clearfix text-left">
                      <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
                      </button>
                      <h5>Student Details</h5>
                      </div>
                      <div class="modal-body">';
                      ?>

                        <div class="row" style="border-bottom:1px solid #cccc;padding-bottom: 10px;">
                          <div class="col-md-4"></div>
                          <div class="col-md-4">
                            <center><img src="../student/images/profile/<?php echo $pic; ?>" width="140px" height="140px;"></center>
                          </div>
                          <div class="col-md-4"></div>
                        </div>

                        <div class="row" style="margin-top: 8px;">
                          <div class="col-md-6">
                          <div class="form-group form-group-default">
                          <label>Full Name</label>
                          <h5><?php echo $f." ".$l; ?></h5>
                          </div>
                          </div>

                          <div class="col-md-6">
                          <div class="form-group form-group-default">
                          <label>Date Of Birth</label>
                          <h5><?php echo $dob; ?></h5>
                          </div>
                          </div>

                        </div>



                        <div class="row">
                          <div class="col-md-6">
                          <div class="form-group form-group-default">
                          <label>Telephone No</label>
                          <h5><?php echo  $tp; ?></h5>
                          </div>
                          </div>

                          <div class="col-md-6">
                          <div class="form-group form-group-default">
                          <label>E-mail Address</label>
                          <h5><?php echo $email; ?></h5>
                          </div>
                          </div>

                        </div>







                        <div class="row">
                          <div class="col-md-6">
                          <div class="form-group form-group-default">
                          <label>Address</label>
                          <h5><?php echo  $address; ?></h5>
                          </div>
                          </div>

                          <div class="col-md-6">
                          <div class="form-group form-group-default">
                          <label>Gender</label>
                          <h5><?php echo $gender; ?></h5>
                          </div>
                          </div>

                        </div>




                              <?php echo '</div>
                              </div>
                              </div>
                              </div>

                          </div>
                          </div>
                          ';
        
                    
           


         echo '

                    <div class="modal fade slide-up disable-scroll" id="payments'.$stu_id.'" tabindex="-1" role="dialog" aria-hidden="false">
                      <div class="modal-dialog ">
                      <div class="modal-content-wrapper">
                      <div class="modal-content modal-lg">
                      <div class="modal-header clearfix text-left">
                      <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
                      </button>
                      <h5>Profile Details</h5>
                      </div>
                      <div class="modal-body">';
                      ?>
                      <div class="row">
                        <div class="col-md-8">

                          <?php 
                            $sql0021 = mysqli_query($conn,"SELECT * FROM payment_data WHERE STU_ID = '$stu_id' AND CLASS_ID = '$class_id'");

                            if(mysqli_num_rows($sql0021)>0)
                            { 

                              echo '<button class="btn btn-success" data-toggle="modal" data-target="#filter'.$stu_id.'"  data-dismiss="modal" style="border-radius: 80px;padding:10px 10px 10px 10px;"><span class="fa fa-print" style="font-size: 19px;"></span></button>';

                            }else
                            if(mysqli_num_rows($sql0021)== '0')
                            { 
                              echo '<button class="btn btn-success disabled" style="border-radius: 80px;padding:10px 10px 10px 10px;cursor:not-allowed"><span class="fa fa-print" style="font-size: 19px;"></span></button>';
                            }
                           ?>
                          
                        </div>
                        <div class="col-md-4"><input type="text" id="search-table33" class="form-control pull-right" placeholder="Search"></div>
                      </div>

                        

                        <div class="table-responsive" style="height: 450px;overflow: auto;margin-bottom: 0;">
                        <table class="table demo-table-search table-responsive-block text-left" id="tableWithSearch">
                        <thead>
                        <tr>
                        <!-- <th style="width: 8%;">Picture</th> -->
                        <th>Payment Date</th>
                        <th style="width: 10%;">Fees(Rs)</th>
                        <th>Month</th>
                        <th>Status</th>
                        </tr>
                        </thead>
                        <tbody id="myTable33">

                          <tr class="no-data col-md-12 alert alert-danger" style="margin-top: 20px;display: none;">
                              <td><span class="fa fa-warning"></span> Not Found Data</td>
                            </tr>
                            <?php 

                                $sql002 = mysqli_query($conn,"SELECT * FROM payment_data WHERE STU_ID = '$stu_id' AND CLASS_ID = '$class_id'");

                                if(mysqli_num_rows($sql002)>0)
                                {
                                while($row002=mysqli_fetch_assoc($sql002))
                                {
                                  $pay_time = $row002['PAY_TIME'];
                                  $ym = $row002['Y_M'];
                                  $monthNum = $row002['MONTH'];
                                  $y = $row002['YEAR'];
                                  $fees = $row002['FEES'];
                                  $status = $row002['ADMIN_SUBMIT'];

                                  $m = date('F', mktime(0, 0, 0, $monthNum, 10)); // March

                                  if($status == '0')
                                  {
                                    $result = '<span class="label label-warning"><span class="fa fa-spinner"></span> Pending</span>';
                                  }else
                                  if($status == '1')
                                  {
                                    $result = '<span class="label label-success"><span class="fa fa-check-circle"></span> Paid</span>';
                                  }else
                                  if($status == '2')
                                  {
                                    $result = '<span class="label label-danger"><span class="fa fa-times-circle"></span> Rejected</span>';
                                  }
                                

                                  echo '
                                    <tr>
                                      <td>'.$pay_time.'</td>
                                      <td>'.number_format($fees,2).'</td>
                                      <td>'.$y.'-'.$m.'</td>
                                      <td>'.$result.'</td>
                                    </tr>';
                                }
                              }else
                              if(mysqli_num_rows($sql002) == '0')
                              {
                                 echo '<tr><td colspan="4" class="text-danger text-center"><span class="fa fa-warning text-danger"></span> Empty Data!</td></tr>';
                              }

                             ?>
                        </tbody>
                        </table>
                        </div>



                      <?php echo '</div>
                      </div>
                      </div>
                      </div>

                      </div>
                    </div>

                    <div class="modal fade slide-up disable-scroll" id="filter'.$stu_id.'" tabindex="-1" role="dialog" aria-hidden="false">
                      <div class="modal-dialog ">
                      <div class="modal-content-wrapper">
                      <div class="modal-content">
                      <div class="modal-header clearfix text-left">
                      <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
                      </button>
                      <h5>Search Payment Details</h5>
                      </div>
                      <div class="modal-body">';
                      ?>
                          <div class="col-md-12">
                                  <form action="print_payment.php" method="POST" target="_blank">
                                    <input type="hidden" name="clz_id" value="<?php echo $class_id; ?>">
                                  <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group form-group-default" style="margin-top: 10px;">
                                          <label>Start Year</label>
                                          
                                          <select class="form-control select_class" name="start_year" id="start_year" required style="cursor: pointer;">
                                            <option value="">Select Year </option>

                                            <?php 
                                              $this_year = date('Y');
                                              $ten = strtotime("-10 years");
                                              $ten_year = date('Y',$ten);

                                              for($i=$ten_year;$i<=$this_year;$i++)
                                              {
                                                  echo '<option value="'.$i.'">'.$i.'</option>';
                                              }

                                             ?>
                                            
                                          </select>

                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group form-group-default" style="margin-top: 10px;">
                                          <label>Start Month</label>
                                          
                                          <select class="form-control select_class" name="start_month" id="start_month" required style="cursor: pointer;">

                                            <option selected  value="">Select Month </option>
                                            <option value='01'>Janaury</option>
                                            <option value='02'>February</option>
                                            <option value='03'>March</option>
                                            <option value='04'>April</option>
                                            <option value='05'>May</option>
                                            <option value='06'>June</option>
                                            <option value='07'>July</option>
                                            <option value='08'>August</option>
                                            <option value='09'>September</option>
                                            <option value='10'>October</option>
                                            <option value='11'>November</option>
                                            <option value='12'>December</option>
                                            
                                          </select>

                                        </div>
                                    </div>
                                  </div> 

                                  <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group form-group-default" style="margin-top: 10px;">
                                          <label>End Year</label>
                                          
                                          <select class="form-control select_class" name="end_year" id="end_year" required style="cursor: pointer;">
                                            <option selected value="">Select Year </option>

                                             
                                             <?php 
                                              $this_year = date('Y');
                                              $ten = strtotime("+10 years");
                                              $ten_year = date('Y',$ten);

                                              for($i=$this_year;$i<=$ten_year;$i++)
                                              {
                                                  echo '<option value="'.$i.'">'.$i.'</option>';
                                              }

                                             ?>
                                            
                                          </select>

                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group form-group-default" style="margin-top: 10px;">
                                          <label>End Month</label>
                                          
                                          <select class="form-control select_class" name="end_month" id="end_month" required style="cursor: pointer;">
                                            <option selected value="">Select Month </option>
                                            <option value='01'>Janaury</option>
                                            <option value='02'>February</option>
                                            <option value='03'>March</option>
                                            <option value='04'>April</option>
                                            <option value='05'>May</option>
                                            <option value='06'>June</option>
                                            <option value='07'>July</option>
                                            <option value='08'>August</option>
                                            <option value='09'>September</option>
                                            <option value='10'>October</option>
                                            <option value='11'>November</option>
                                            <option value='12'>December</option>
                                            
                                            
                                          </select>

                                        </div>
                                    </div>
                                  </div>

                                  <div id="msg" class="text-danger" style="display: none;"></div>

                                  <script type="text/javascript">
                                    $(document).ready(function(){
                                      $('#end_year,#end_month').change(function(){
                                        var start_year = $('#start_year').val();
                                        var start_month = $('#start_month').val();

                                        var end_year = $('#end_year').val();
                                        var end_month = $('#end_month').val();

                                        if(start_year == end_year && start_month == end_month)
                                        {
                                          $('#search_btn').prop('disabled',true);
                                          $('#msg').show();
                                          //$('#search_btn').css('cursor','not-allowed');
                                          $('#msg').html('<span class="fa fa-warning"></span> Please select different years & month.');
                                        }

                                        if(start_year !== end_year && start_month !== end_month)
                                        {
                                            $('#search_btn').prop('disabled',false);
                                            $('#msg').hide();

                                        }

                                      });
                                    });
                                  </script>

                                          <button type="submit" class="btn btn-success btn-lg btn-block" name="filter_data" value="<?php echo $stu_id; ?>" style="width: 100%;margin: none;" id="search_btn"><i class="pg-icon">search</i> Search</button>

                                          <button type="submit" class="btn btn-default btn-lg btn-block" data-dismiss="modal" data-toggle="modal" data-target="#payments<?php echo $stu_id; ?>" style="width: 100%;margin: none;"><i class="pg-icon">arrow_left</i> Back</button>


                                  </form>
                                  </div>


                      <?php echo '</div>
                      </div>
                      </div>
                      </div>

                      </div>
                    </div>




                    </td>

                  </tr>
                  ';



        }
                    
              
         ?>

        </tbody>
        </table>
        </div>

                      







        </div>
        </div>



      </div>




<script type="text/javascript">
  $('.save_btn').click(function(){
    
    var upload_btn0 = $('#selectedFile0').val();
    var upload_btn = $('#sel_file').val();
      
    if(empty(upload_btn0))
    {
      alert('Please select your audio file.');
    }else
    if(empty(upload_btn))
    {
      alert('Please select your document file.');
    }
   });
</script>

<script type="text/javascript">
  $('.navi').click(function(){
    $('.frm')[0].reset();
      
   });
</script>

<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
<script>
$(document).ready(function(){
  $("#search-table").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

<script>
$(document).ready(function(){
  $("#search-table33").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable33").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

<!-- search data -->

<!-- no data message/no found data show in search-->

<script type="text/javascript">
  $(document).ready(function () {

    (function ($) {

        $('##search-table2').keyup(function () {
            var rex = new RegExp($(this).val(), 'i');
            $('#myTable2').hide();
            $('#myTable2').filter(function () {
                return rex.test($(this).text());
            }).show();
            $('.no-data').hide();
            if($('#myTable2:visible').length == 0)
            {
                $('.no-data').show();
            }

        })

    }(jQuery));

});
</script>




<?php include('footer/footer.php'); ?>