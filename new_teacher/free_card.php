  
<?php

$page = "Free Card";
$folder_in = '0';

  include('header/header.php'); 
 ini_set( "display_errors", 0); 

 $admin_id = $_SESSION['ADMIN_ID'];

  ?>
  <style type="text/css">
    .btn_pulse {
  
  display: block;
  border-radius: 10%;
  cursor: pointer;
  animation: none;
  float: left;
  bottom: 5px;
  right: 5px;
  font-weight: bold;
  padding-top: 2px;
  text-align: center;
  color: white;
  font-size: 14px;
  animation: pulse 1.6s infinite;
}


@keyframes pulse {
  0% {
    -moz-box-shadow: 0 0 0 0 #7252d3;
    box-shadow: 0 0 0 0 #7252d3;
  }
  70% {
    -moz-box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
  }
  100% {
    -moz-box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
  }
}
 .no-data {
    display: none;
    text-align: center;
}
  </style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


      <ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-bottom: 04%;margin-bottom: 0;">
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title=" Free Card"> Free Card</li>
    </ol>
    


<div class="col-md-12" style="border-top: 1px solid #cccc;">
  
<div class="row" style="margin-top: 3%;margin-bottom: 3%;">
<div class="col-md-1"></div>
<div class="col-md-10 bg-white" style="padding: 10px 20px 15px 20px;">



        <h2><a href="dashboard.php" style="color: #4b4b4b;"><span class="fa fa-angle-left"></span></a> Free Card</h2>
      
    <div class="row">
      <div class="col-md-9">
          <div class="form-group form-group-default" style="margin-top: 10px;">
              <label>Teacher Name</label>
              <input list="search_teacher" name="teacher" id="teacher" required class="form-control" placeholder="Type Teacher name" value="<?php echo $get_user_reg_id; ?>" onclick="this.select();">

              <datalist id="search_teacher">
                <?php 

                      $sql0015 = mysqli_query($conn,"SELECT * FROM `teacher_details`");
                      while($row0015=mysqli_fetch_assoc($sql0015))
                      {
                        $f_name = $row0015['F_NAME'];
                        $l_name = $row0015['L_NAME'];
                        $position = $row0015['POSITION'];
                        $teach_id = $row0015['TEACH_ID'];

                          echo '<option value='.$teach_id.'>'.$f_name.' '.$l_name.'</option>';

                      }

                       ?>
              </datalist>



              

            </div>
      </div>
      <div class="col-md-3" style="padding-top: 10px;">
        <button class="btn btn-success btn-block btn-lg" id="search_free" style="padding-top: 10px;font-size: 22px;padding-bottom: 10px;"><i class="pg-icon" style="font-size: 30px;">search</i> Search</button></div>
    </div>

    <div class="col-md-12" id="teacher_data" style="padding:0;border-bottom: 1px solid #cccc;margin-bottom: 6px;"></div>
  

  <div class="row">
    <div class="col-md-6">

      <h5>Class Details</h5>
        
           <div class="table-responsive" style="height: 370px;overflow: auto;border-top: 1px solid #cccc;">
            <table class="table demo-table-search table-responsive-block text-left" id="tableWithSearch">
            <thead>
            <tr>
              <th style="width: 10%;">Class Name</th><!-- 
              <th style="width: 1%;">Start Time</th>
              <th style="width: 1%;">End Time</th> -->
              <th style="width: 1%;">Day</th>
              <th style="width: 1%;">Action</th>
            </tr>
            </thead>
            <tbody id="class_table">

            <tr class="no-data11 alert alert-danger" style="margin-top: 20px;display: none;">
              <td colspan="3" class="text-danger"><span class="fa fa-warning"></span> Not Found Data</td>
            </tr>

            <?php 
              echo '<tr><td colspan="3" class="text-center text-danger"><span class="fa fa-warning"></span> Not Found Data</td></tr>';

             ?>


            </tbody>
            </table>
            </div>

    </div>
    <div class="col-md-6">

      <div class="row">
        <div class="col-md-6"><h5>Student Details </h5></div>
        <div class="col-md-6"><input type="search" name="teacher" id="search" required class="form-control" placeholder="Type Student name"></div>
      </div>
      <div class="table-responsive" style="height: 370px;overflow: auto;border-top: 1px solid #cccc;">
            <table class="table demo-table-search table-responsive-block text-left" id="tableWithSearch">
            <thead>
            <tr>
              <th style="width: 1%;">Student Name</th>
              <th style="width: 1%;">Status</th>
              <th style="width: 1%;" class="text-center">Action</th>
            </tr>
            </thead>
            <tbody id="students_table" class="searchable">

            <tr class="no-data alert alert-danger" style="margin-top: 20px;display: none;">
              <td colspan="3" class="text-danger"><span class="fa fa-warning"></span> Not Found Data</td>
            </tr>

            <?php 
              echo '<tr><td colspan="3" class="text-center text-danger"><span class="fa fa-warning"></span> Not Found Data</td></tr>';

             ?>


            </tbody>
            </table>
            </div>

    </div>
  </div>
    
 


</div>
<div class="col-md-1"></div>

</div>


<?php  include('footer/footer.php'); ?>


<script type="text/javascript">
  
  $(document).ready(function(){

    $('#search_free').bind('click blur',function(){
                  
        var teach_id = $('#teacher').val();
         $.ajax({
          url:'../admin/query/check.php',
          method:"POST",
          data:{search_free_card:teach_id},
          success:function(data)
          {
            //alert(data)
            $('#class_table').html(data);
          }
        });
  });

  });
</script>

<script type="text/javascript">
  
  $(document).ready(function(){

    $('#teacher').bind('change blur',function(){
                  
        var teach_id = $('#teacher').val();
         $.ajax({
          url:'../admin/query/check.php',
          method:"POST",
          data:{show_teacher:teach_id},
          success:function(data)
          {
            $('#teacher_data').html('<label class="text-success text-left" style="padding:0;"><span class="fa fa-check-circle"></span>'+data+'</label>');
          }
        });
  });

  });
</script>





<script>
$(document).ready(function(){
  $("#search").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#students_table tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
  (function ($) {

        $('#search').keyup(function () {
            var rex = new RegExp($(this).val(), 'i');
            $('.searchable tr').hide();
            $('.searchable tr').filter(function () {
                return rex.test($(this).text());
            }).show();
            $('.no-data').hide();
            if($('.searchable tr:visible').length == 0)
            {
                $('.no-data').show();
            }

        })

    }(jQuery));
});
</script>