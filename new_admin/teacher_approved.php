<?php
  $page = "Teacher Approved";
  $folder_in = '3';

      include('header/header.php');

        $s04 = mysqli_query($conn,"SELECT * FROM admin_login WHERE ADMIN_ID = '$admin_id'");
        while($row4 = mysqli_fetch_assoc($s04))
        {
          $name = $row4['ADMIN_NAME'];
        }

 ?>
<meta name="description" content="Description">
<meta name="keywords" content="Keyword1, Keyword2">

    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4"></div>
        <div class="col-md-4"></div>
    </div>

    <ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-bottom: 04%;margin-bottom: 0;">
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="Teacher Approved"> Teacher Approved</li>
    </ol>


      <div class="col-md-12 bg-white" style="border:1px solid #cccc;height: auto;margin-bottom: 10px;">
      
      <div class="col-md-12" style="border-bottom: 1px solid #cccc;">
        <div class="row">

            <div class="col-md-10" style="margin-bottom: 10px;"><h2><a href="dashboard.php" data-toggle="tooltip" data-title="Back"><i class="pg-icon" style="font-size: 22px;">chevron_left</i></a> Teacher Approved</h2></div>
          <div class="col-md-2">

          <a href="show_teacher_login.php" class="btn btn-info  btn-xs btn-rounded btn-block" style="padding:8px 8px 8px 8px;box-shadow:0px 0px 4px 3px #cccc;margin-top:6px;" data-title="Show teacher in login" data-toggle="tooltip"><i class="pg-icon">edit</i> Show teacher in login</a>

          </div>
        </div>
      </div>

          
          <div class=" container-fluid container-fixed-lg bg-white" style="margin-top: 0px;">
            <div class="card card-transparent">
              <div class="card-header ">

                  <form action="teacher_approved.php" method="POST">
                      <div class="row">
                        <div class="col-md-2">
                          <div class="col-md-12" style="padding: 10px 10px 10px 10px;background-color: #000000b8;border-radius: 10px;" data-toggle="tooltip" data-title="Today Student Joined Amount">
                            <center><span class="fa fa-user text-white text-center" style="font-size: 18px;"></span>
                              <small class="text-white" style="font-size: 20px;">
                                <?php 

                                  $sql004 = mysqli_query($conn,"SELECT * FROM `teacher_login` WHERE `STATUS` = 'Active' ");
                                  $total_student = mysqli_num_rows($sql004);

                                  if($total_student < 10)
                                  {
                                    $total_student = "0".$total_student;
                                  }else
                                  {
                                    $total_student = number_format($total_student);
                                  }

                                  echo $total_student;
                                 ?>
                              </small>
                              <br>
                              <small class="text-white text-center">Total Teachers</small>
                            </center>
                          </div>
                        </div>
                        <div class="col-md-5"> </div>
                      <div class="col-md-3 mt-2">
                        <input list="student_search" id="search-table" name="search_teacher" class="form-control" placeholder="Search Register ID.." >

                        <datalist id="student_search">
                          <?php 

                            $sql002 = mysqli_query($conn,"SELECT * FROM `teacher_details`");
                            while($row002 = mysqli_fetch_assoc($sql002))
                            {
                              $name = $row002['F_NAME']." ".$row002['L_NAME'];
                              $teach_id = $row002['TEACH_ID'];
                              $tp = $row002['TP'];

                              echo '<option value="'.$teach_id.'">'.$name.' | '.$tp.'</option>';
                              
                            }

                           ?>
                          
                        </datalist>
                      </div>
                      <div class="col-md-1 mt-2"> <button type="submit" class="btn btn-success btn-lg btn-block" name="search_teacher_btn" style="border-radius: 20px;"><i class="fa fa-search"></i> &nbsp; Search</button></div>

                      <div class="col-md-1 mt-2"> <button type="submit" class="btn btn-danger btn-lg btn-block" name="reset_student_btn" style="border-radius: 20px;"><i class="fa fa-refresh"></i> &nbsp; Reset</button></div>
                    </div>
                  </form>

                    <div class="clearfix"></div>
                </div>
                <div class="card-body">
                <div class="table-responsive">
                <table class="table table-hover" id="tblFruits">
                  <thead>
                      <tr>
                        <th style="width: 1%;text-align: center;">Picture</th>
                        <th style="width: 1%;">Name</th>
                        <th style="width: 1%;">TP No</th><!-- 
                        <th style="width: 1%;">DOB</th> -->
                        <th style="width: 1%;">Gender</th>
                        <th style="width: 1%;">Username</th>
                        <th style="width: 1%;text-align: center;">Action</th>
                      </tr>
                    </thead>
                    <tbody>

                      <?php 
                          /*------------------ Header Pagination --------------------------------*/
                        $record_per_page = 10;

                        $nxt_five_loop = 0;

                        $page = '';

                        if(isset($_GET["page"]))
                        {
                          $page = $_GET["page"];
                        }
                        else
                        {
                         $page = 1;
                        }

                        $start_from = ($page-1)*$record_per_page; //Current page no -1 X Per Page records

                        if(isset($_POST['search_teacher_btn']))
                        {
                          $teacher_id = $_POST['search_teacher'];

                          $sql001 = "SELECT * FROM `teacher_login` WHERE `STATUS` = 'Active' AND `TEACH_ID` LIKE '%".$teacher_id."%'  order by `TEACH_ID` DESC LIMIT $start_from, $record_per_page";


                        }else
                        {
                          $sql001 = "SELECT * FROM `teacher_login` WHERE `STATUS` = 'Active'  order by `TEACH_ID` DESC LIMIT $start_from, $record_per_page";

                        }

                        
                          $page_query = "SELECT * FROM `teacher_login` WHERE `STATUS` = 'Active'  ORDER BY `TEACH_ID` DESC";

                        /*------------------ Header Pagination --------------------------------*/

                        $j = 0;
                        
                          $today = date('Y-m-d h:i:s A');
                        //LIMIT = 10-1 x 10 = 90 , 10";
                        $result = mysqli_query($conn, $sql001);
                        $page_result = mysqli_query($conn, $page_query);

                        $check = mysqli_num_rows($result);

                        if($check>0){
                        while($row001 = mysqli_fetch_assoc($result))
                        {
                          $teach_id = $row001['TEACH_ID'];
                          $reg_date = $row001['REG_DATE'];

                          $confirm = $row001['CONFIRM'];
                          $username = $row001['USERNAME'];



                          $sql002 = mysqli_query($conn,"SELECT * FROM teacher_details WHERE TEACH_ID = '$teach_id'");
                          while($row002 = mysqli_fetch_assoc($sql002))
                          {
                            $name = $row002['POSITION'].". ".$row002['F_NAME']." ".$row002['L_NAME'];
                            $fnm = $row002['F_NAME'];
                            $lnm = $row002['L_NAME'];

                            $address = $row002['ADDRESS'];
                            $tp = $row002['TP'];
                            $dob = $row002['DOB'];
                            $email = $row002['EMAIL'];
                            $position = $row002['POSITION'];
                            $qualification = $row002['QUALIFICATION'];
                            $about_me = $row002['INTRODUCTION'];
                            $video_link = $row002['INTRO_VIDEO'];
                            $gender = $row002['GENDER'];
                            $picture = $row002['PICTURE'];
                            $teacher_title = $row002['CLASS_TITLE'];

                            if($picture == '0')
                             {
                                if($gender == 'Male')
                                {
                                  $picture = 'b_teacher.jpg';
                                }else
                                if($gender == 'Female')
                                {
                                  $picture = 'g_teacher.jpg';
                                }
                             }
                          }


                          if($confirm == '0')
                          {
                            $confirm_bg = 'danger';
                            $confirm_txt = 'Not-confirm';
                            $message_admin = '<span class="fa fa-star"></span> ගුරුවරයා Confirm කිරීම අනිවාර්යය වේ.නැතහොත් ගුරුවරයාගේ පන්ති පිළිබද විස්තර සිසුන්ට නොපෙන්වයි.';
                          }else
                           if($confirm == '1')
                          {
                            $confirm_bg = 'success';
                            $confirm_txt = 'Confirm';
                            $message_admin = '';
                          }



                            echo '

                        <tr>
                          <td class="v-align-middle"><center><img src="../teacher/images/profile/'.$picture.'" style="width:100px;height:100px;max-width:100px;max-height:100px;border-radius:80px;"> 

                          </center>

                            <center><span class="label label-'.$confirm_bg.'" style="bottom:1px;text-align:center;">'.$confirm_txt.'</span></center>

                            </td>
                          <td class="v-align-middle">'.$name.' <br> <small class="text-danger">('.$reg_date.')</small> <br>

                          <small style="bottom:1px;text-align:center;font-weight:bold;color:#3498db">'.$message_admin.'</small>

                          </td>
                          <td class="v-align-middle">'.$tp.'</td>
                          <td class="v-align-middle">'.$gender.'</td>
                          <td class="v-align-middle"style="font-size:16px;">'.$username.' <br><a href="../teacher/login/index.php"><small class="label label-success" style="font-size:12px;"><i class="fa fa-link"></i></small></a></td>
                           <td class="v-align-middle text-center">

                          '; 

                          /*<td class="v-align-middle">'.$dob.'</td>*/

                            if($confirm == '0')
                            {
                           ?>

                            <a href="../admin/query/update.php?approved_teacher_dis_confirm=<?php echo $teach_id; ?>&&confirm=1&&page=<?php echo $page; ?>" class="btn btn-complete  btn-xs btn-rounded" style="padding:8px 8px 8px 8px;box-shadow:0px 0px 4px 3px #cccc;margin-top:6px;" data-title="Confirm Teacher" data-toggle="tooltip" onclick="return confirm('Are you sure confirm?')"><i class="pg-icon">tick</i></a>

                           <?php 

                            }else
                            if($confirm == '1')
                            {
                            ?>


                            <a href="../admin/query/update.php?approved_teacher_dis_confirm=<?php echo $teach_id; ?>&&confirm=0&&page=<?php echo $page; ?>" class="btn btn-info  btn-xs btn-rounded" style="padding:8px 8px 8px 8px;box-shadow:0px 0px 4px 3px #cccc;margin-top:6px;" data-title="Not Confirm Teacher" data-toggle="tooltip" onclick="return confirm('Are you sure not-confirm?')"><i class="pg-icon">close</i></a>

                            <?php 

                            }


                          echo '<button data-toggle="modal" data-target="#teach_data'.$teach_id.'" class="btn btn-success  btn-xs btn-rounded" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;margin-top:6px;" data-title="Approve" data-toggle="tooltip" id="edit1'.$teach_id.'"><i class="pg-icon">edit</i></button>';

                          echo '<button data-toggle="modal" data-target="#change_password'.$teach_id.'" class="btn btn-primary btn-xs btn-rounded" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;margin-top:6px;margin-right:6px;margin-left:6px;" data-title="Approve" data-toggle="tooltip" id="edit1'.$teach_id.'"><i class="pg-icon">cog</i></button>';

                             ?>
                            

                             <br>

                             <div id="change_password<?php echo $teach_id; ?>" class="modal fade" role="dialog">
                                <div class="modal-dialog">

                                  <!-- Modal content-->
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4 class="modal-title">Change Profile Password</h4>
                                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <form action="../admin/query/update.php" method="POST">
                                    <div class="modal-body text-left">
                                      <h5 class="text-muted"><?php echo $name; ?></h5>

                                      <div class="row">
                                        <div class="col-md-12">
                                          <div class="form-group form-group-default">
                                            <label>Username</label>
                                            <input type="text" name="username" value="<?php echo $username; ?>" class="form-control" placeholder="Enter Username.." autofocus>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-md-12">
                                          <div class="form-group form-group-default">
                                            <label>Password</label>
                                            <input type="text" name="reset_password" class="form-control" placeholder="Enter Password.." autofocus>
                                          </div>
                                        </div>
                                      </div>

                                      <input type="hidden" name="page" value="<?php echo $page; ?>">

                                    </div>
                                    <div class="modal-footer">
                                      <button type="submit" class="btn btn-success" name="update_password" value="<?php echo $teach_id; ?>"><i class="fa fa-pencil"></i>&nbsp; Update</button>
                                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                    </form>
                                  </div>

                                </div>
                              </div>

                          <a href="../admin/query/update.php?approved_teacher_deactive=<?php echo $teach_id; ?>&&page=<?php echo $page; ?>" class="btn btn-warning  btn-xs btn-rounded" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;margin-top:6px;" data-title="Deactive" data-toggle="tooltip" onclick="return confirm('Are you sure Deactive?')"><i class="pg-icon">close</i></a>


                          <a href="../admin/query/delete.php?approved_teach=<?php echo $teach_id; ?>&&approve=Disapprove" onclick="return confirm('Are you sure disapprove?')" class="btn btn-danger  btn-xs btn-rounded" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;margin-top:6px;" data-title="Disapprove" data-toggle="tooltip"><i class="pg-icon">trash_alt</i></a>

                          <?php 

                            $sql0017 = mysqli_query($conn,"SELECT * FROM `teacher_bank_details` WHERE `TEACH_ID` = '$teach_id'");
                            $count_account = mysqli_num_rows($sql0017);

                           ?>

                          <a href="bank_details.php?teacher_id=<?php echo $teach_id; ?>&&last_pagination=<?php echo $page; ?>" class="btn btn-danger  btn-xs btn-rounded" style="padding:8px 7px 8px 7px;box-shadow:0px 0px 4px 3px #cccc;margin-top:6px;background-color: #8c7ae6;border:1px solid #8c7ae6;" data-title="Teacher's Bank Details" data-toggle="tooltip"><i class="fa fa-bank"></i>&nbsp;<span class="badge badge-success"><?php echo $count_account; ?></span></a>


                          <?php echo '

                          </td>
                        </tr>

                      ';
                            ?>
                            
                      <div class="modal fade slide-up disable-scroll" id="teach_data<?php echo $teach_id; ?>" tabindex="-1" role="dialog" aria-hidden="false">
                                        <div class="modal-dialog">
                                        <div class="modal-content-wrapper">
                                        <div class="modal-content modal-lg">
                                        <div class="modal-header clearfix text-left">
                                        <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
                                        </button>
                                        <h5>Teacher Details</h5>
                                        </div>
                                        <div class="modal-body" style="height:600px;overflow: auto;">

                                            <form  action="../admin/query/update.php" method="POST" id="form-personal" role="form" autocomplete="off" enctype="multipart/form-data">

                                            <div class="row">

                                              <div class="col-md-12">
                                            <div class="form-group form-group-default input-group">
                                            <div class="form-input-group">
                                            <label>Position</label>

                                              <select class="form-control change_inputs" name="position">
                                                
                                                <?php 
                                                echo '
                                                <option value="'.$position.'">'.$position.'.</option>';

                                                if($position == 'Mr')
                                                {
                                                    echo '<option value="Mrs">Mrs.</option>
                                                <option value="Ms">Ms.</option>
                                                <option value="Rev">Rev.</option>
                                                <option value="Dr">Dr.</option>
                                                <option value="Prof">Prof.</option>';
                                                }else
                                                if($position == 'Mrs')
                                                {
                                                  echo '<option value="Mr">Mr.</option>
                                                        <option value="Ms">Ms.</option>
                                                        <option value="Rev">Rev.</option>
                                                        <option value="Dr">Dr.</option>
                                                        <option value="Prof">Prof.</option>';
                                                }else
                                                if($position == 'Ms')
                                                {
                                                  echo '<option value="Mr">Mr.</option>
                                                        <option value="Mrs">Mrs.</option>
                                                        <option value="Rev">Rev.</option>
                                                        <option value="Dr">Dr.</option>
                                                        <option value="Prof">Prof.</option>';
                                                }else
                                                if($position == 'Rev')
                                                {
                                                  echo '<option value="Mr">Mr.</option>
                                                        <option value="Mrs">Mrs.</option>
                                                        <option value="Ms">Ms.</option>
                                                        <option value="Dr">Dr.</option>
                                                        <option value="Prof">Prof.</option>';
                                                }else
                                                if($position == 'Dr')
                                                {
                                                  echo '<option value="Mr">Mr.</option>
                                                        <option value="Mrs">Mrs.</option>
                                                        <option value="Ms">Ms.</option>
                                                        <option value="Rev">Rev.</option>
                                                        <option value="Prof">Prof.</option>';
                                                }else
                                                if($position == 'Prof')
                                                {
                                                  echo '<option value="Mr">Mr.</option>
                                                        <option value="Mrs">Mrs.</option>
                                                        <option value="Ms">Ms.</option>
                                                        <option value="Rev">Rev.</option>
                                                        <option value="Dr">Dr.</option>';
                                                }


                                               ?>

                                                </select>

                                              </div>
                                              </div>
                                              </div>
                                              </div>


                                              <div class="row">
                                              <div class="col-md-6">
                                              <div class="form-group form-group-default input-group">
                                              <div class="form-input-group">
                                              <label>First Name</label>
                                              <input type="text" name="fnm" class="form-control change_inputs" value="<?php echo $fnm; ?>" pattern="[a-zA-Z. ]+"  required>
                                              </div>
                                              </div>
                                              </div>

                                              <div class="col-md-6">
                                              <div class="form-group form-group-default input-group">
                                              <div class="form-input-group">
                                              <label>Last Name</label>
                                              <input type="text" name="lnm" class="form-control change_inputs" value="<?php echo $lnm; ?>" pattern="[a-zA-Z. ]+"  required>
                                              </div>
                                              </div>
                                              </div>

                                              </div>

                                              <div class="row">
                                              <div class="col-md-6">
                                              <div class="form-group form-group-default input-group">
                                              <div class="form-input-group">
                                              <label>Date Of Birth</label>
                                              <input class="form-control change_inputs" type="date" name="dob" min="<?php $d = strtotime("-60 year"); echo date('Y-m-d',$d); ?>" max="<?php $d = strtotime("-5 year"); echo date('Y-m-d',$d); ?>" style="margin-bottom: 10px;" value="<?php echo $dob; ?>">
                                              </div>
                                              </div>
                                              </div>

                                              <div class="col-md-6">
                                              <div class="form-group form-group-default input-group">
                                              <div class="form-input-group">
                                              <label>Telephone No</label>
                                              <input class="form-control change_inputs" type="telephone" pattern="[0-9]{10}" minlength="10" maxlength="10" name="number" id="number" value="<?php echo $tp; ?>" required placeholder="0xxx xxxx xx" style="margin-bottom: 10px;">
                                              </div>
                                              </div>
                                              </div>

                                              </div>

                                              <div class="row">
                                              <div class="col-md-12">
                                              <div class="form-group form-group-default input-group">
                                              <div class="form-input-group">
                                              <label>E-mail Address</label>
                                              <input type="email" class="form-control change_inputs" name="email" value="<?php echo $email; ?>"  style="margin-bottom: 10px;" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$">
                                              </div>
                                              </div>
                                              </div>
                                              </div>
                                              <div class="row">
                                              <div class="col-md-12">
                                              <div class="form-group form-group-default">
                                              <label>Address</label>
                                              <textarea name="address" placeholder="xxxxxxxx" style="margin-bottom: 10px;" class="form-control change_inputs"><?php echo $address; ?></textarea>
                                              </div>
                                              </div>
                                              </div>
                                              <div class="row">
                                              <div class="col-md-6">
                                              <div class="form-group form-group-default">
                                              <label>Gender</label>
                                              <div class="row">
                                              <?php 
                                                  if($gender == 'Male')
                                                  {?>



                                                    <div class="col-md-6" style="padding-left: 6px;">

                                                      <label><span class="fa fa-male"></span> Male : <input type="radio" checked="checked" name="gender" value="Male" class="change_inputs"></label>
                                                    </div>
                                                    <div class="col-md-6" style="padding-left: 6px;">

                                                      <label style="padding-left: 10px;margin-bottom: 10px;"><span class="fa fa-female"></span> Female : <input type="radio" name="gender" value="Female" class="change_inputs"> </label>

                                                    </div>
                                                    <?php
                                                  }else
                                                  if($gender == 'Female')
                                                  {?>



                                                    <div class="col-md-6" style="padding-left: 6px;"><label><span class="fa fa-male"></span> Male : <input type="radio"name="gender" value="Male" class="change_inputs"></label></div>

                                                    <div class="col-md-6" style="padding-left: 6px;"><label style="padding-left: 10px;margin-bottom: 10px;"><span class="fa fa-female"></span> Female : <input type="radio" checked="checked"  name="gender" value="Female" class="change_inputs"> </label></div>
                                                    <?php
                                                  }
                                                 ?>
                                                 </div>
                                              </div>
                                              </div>
                                              <div class="col-md-6">
                                              <div class="form-group form-group-default">
                                                <label>Qualification</label>
                                                
                                                <textarea name="qualification" placeholder="xxxxxxxx" style="margin-bottom: 10px;" class="form-control change_inputs"><?php echo $qualification; ?></textarea>


                                              </div>
                                              </div>
                                            </div>
                                              <input type="hidden" name="recent_picture" value="<?php echo $picture; ?>">

                                                <div class="row">
                                                <div class="col-md-12">
                                                <div class="form-group form-group-default text-left">
                                                <label style="padding-bottom: 10px;">Picture</label>
                                                  <input type="file" name="picture" class="form-control" accept="image/*" value="0">
                                                </div>
                                                </div>
                                              </div>
                                              <div class="row">
                                                 <div class="col-md-12">
                                                    <div class="form-group form-group-default">
                                                      <label>About Me</label>
                                                      
                                                      <textarea name="about" placeholder="xxxxxxxx" style="margin-bottom: 10px;height: 80px;" class="form-control change_inputs"><?php echo $about_me; ?></textarea>


                                                    </div>
                                                    </div>
                                              </div>
                                              <div class="row">
                                                 <div class="col-md-12">
                                                    <div class="form-group form-group-default">
                                                      <label>Sample Video</label>
                                                      
                                                      <textarea name="video_link" placeholder="xxxxxxxx" style="margin-bottom: 10px;" class="form-control change_inputs"><?php echo $video_link; ?></textarea>


                                                    </div>
                                                    </div>
                                              </div>

                                              <div class="row">
                                                 <div class="col-md-12">
                                                    <div class="form-group form-group-default">
                                                      <label>Student Profile Teacher Title</label>
                                                      
                                                      <textarea name="teacher_title" placeholder="xxxxxxxx" style="margin-bottom: 10px;" class="form-control change_inputs"><?php echo $teacher_title; ?></textarea>


                                                    </div>
                                                    </div>
                                              </div>

                                              <div class="clearfix"></div>
                                              <div class="row m-t-25">
                                              <div class="col-xl-6 p-b-10">
                                              <p class="small-text hint-text">Click the Update button to change all the data you have changed. (ඔබ වෙනස් කල සියලු දත්තයන් වෙනස් කිරීමට Update බොත්තම ඔබන්න.)</p>
                                              </div>
                                              <div class="col-xl-6">
                                              <button aria-label="" id="up_btn" class="btn btn-primary pull-right btn-lg btn-block" type="submit" name="update_teacher" value="<?php echo $teach_id; ?>">Update
                                              </button>
                                              </div>
                                              </div>
                                              </form>

                                          </div>

                                      </div>
                                    </div>
                                  </div>
                                </div>
                        <?php
                          }
                        }else
                        if($check == '0')
                        {
                          echo '<tr><td colspan="7" class="text-center text-danger"><span class="fa fa-warning"></span> Not Found Data</td></tr>';
                        } 
                      

                        

                        
                         ?>


                    </tbody>
                </table>
              </div>
              </div>

             <div class="col-md-12" style="width: 100%;overflow: auto;">
                  <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-8" class="php_pagination">
                      
                      <div class="col-md-12">
                    <br />
                    <?php
                    $total_records = mysqli_num_rows($page_result);
                    $total_pages = ceil($total_records/$record_per_page);
                    $start_loop = $page;
                    $difference = $total_pages - $page;
                    if($difference <= 5)
                    {
                     $start_loop = $total_pages - 5;
                    }else
                    if($difference <= 0)
                    {
                      $start_loop = '0';
                    }

                    echo '<ul class="pagination">';
                    $end_loop = $start_loop + 4;
                    $five_loop = $page-5;
                    if($five_loop <= 0)
                    {
                      $five_loop = 1;
                    }
                    if($page > 1)
                    {

                      echo '<li><a href="teacher_approved.php?page=1">First</a></li>';
                      echo '<li><a href="teacher_approved.php?page='.($page - 1).'"> < </a></li>';
                      echo '<li><a href="teacher_approved.php?page='.$five_loop.'"> <<< </a></li>';

                    }
                    if($start_loop !== '1')
                    {
                      $few_no = $start_loop - 1;
                    }else
                    if($start_loop > 0)
                    {
                      $few_no = 1;
                    }
                    
                    for($i=$few_no; $i<=$end_loop+1; $i++)
                    {     
                      if($i == $page)
                      {
                        echo '<li class="active"><a href="teacher_approved.php?page='.$i.'" style="font-weight:bold;background-color:#2980b9;color:white;">'.$i.'</a></li>';
                      }else
                      {
                        if($i == '0')
                        {
                          continue;
                        }else
                        if($i>0)
                        {
                          echo '<li><a href="teacher_approved.php?page='.$i.'" >'.$i.'</a></li>';
                        }
                        
                      }

                    }
                    if($page <= $end_loop)
                    {
                      
                      if($page == '1')
                      {
                        $nxt_five_loop = 5;
                      }else
                      if($page >= 5)
                      {
                        $nxt_five_loop = $page+5;

                        if($total_pages < $nxt_five_loop)
                        {
                          $nxt_five_loop = $total_pages;
                        }
                      }else
                      {
                        $nxt_five_loop = $page+5;
                      }

                        echo '<li><a href="teacher_approved.php?page='.($page + 1).'" > > </a></li>';
                        echo '<li><a href="teacher_approved.php?page='.$nxt_five_loop.'" > >>> </a></li>';
                        echo '<li><a href="teacher_approved.php?page='.$total_pages.'" >Last</a></li>';
                      
                    }
                    
                    echo '</ul>';
                    
                    ?>
                    </div>

                    </div>
                  </div>
                    
                </div>
            </div>

          </div>
        </div>
    </div>

<?php include('footer/footer.php'); ?>
