<div id="register">   
    <div class="row">
        <div class="col-md-12">
            <div class="section-title">
                <h5>ලියාපදිංචි වු පන්ති</h5>
            </div>
        </div>
    </div>
    <div class="row ">

        <?php 
      
      $student_session_id = $_SESSION['STU_ID']; //student ID

      $sql001 = mysqli_query($conn,"SELECT * FROM `transactions` WHERE `STU_ID` = '$student_session_id'");

      $check001 = mysqli_num_rows($sql001); //Check Available Payments

      if($check001 > 0)
      {
          while($row001 = mysqli_fetch_assoc($sql001))
          {
            $class_id = $row001['CLASS_ID'];
            $class_free_card = $row001['FREE_CLASS']; //Free Card Yes = 1,No = 0

            $sql0012 = mysqli_query($conn,"SELECT * FROM `classes` WHERE `CLASS_ID` = '$class_id' "); //Class Details
            while($row0012 = mysqli_fetch_assoc($sql0012))
            {
                $class_name = $row0012['CLASS_NAME']; //Class Name
                $teacher_id = $row0012['TEACH_ID']; //Teacher ID
                $class_fees = $row0012['FEES']; //Teacher ID

                $class_start_time = $row0012['START_TIME']; //Class Start time
                $class_end_time = $row0012['END_TIME']; //Class End time

                $class_day = $row0012['DAY']; //Class End time
                
                $str = strtotime($class_start_time);
                $class_start_time = date('h:i A',$str);

                $str2 = strtotime($class_end_time);
                $class_end_time = date('h:i A',$str2);

            }

            $sql0013 = mysqli_query($conn,"SELECT * FROM `teacher_details` WHERE `TEACH_ID` = '$teacher_id' "); //check Classes for registered teachers details
            while($row0013 = mysqli_fetch_assoc($sql0013))
            {
                $teacher_position = $row0013['POSITION']; //Teacher Position
                $teacher_f_name = $row0013['F_NAME']; //Teacher First Name
                $teacher_l_name = $row0013['L_NAME']; //Teacher Last Name

                $teacher_picture = $row0013['PICTURE']; //Teacher Profile Picture
                $teacher_gender = $row0013['GENDER']; //Teacher Gender


                $teacher_full_name = $teacher_position.". ".$teacher_f_name." ".$teacher_l_name; //Teacher Full Name

                //If Teacher Picture zero,Check gender and gender to select picture 
                if($teacher_picture == '0')
                {
                    if($teacher_gender == 'Male')
                    {
                        $teacher_picture = 'b_teacher.jpg';
                    }else
                    if($teacher_gender == 'Female')
                    {
                        $teacher_picture = 'g_teacher.jpg';
                    }
                }
                //If Teacher Picture zero,Check gender and gender to select picture

            }
            $sql0012 = mysqli_query($conn,"SELECT * FROM `institute` WHERE `INS_ID` = '1' "); //Institute Data assign to variables
            while($row0012 = mysqli_fetch_assoc($sql0012))
            {

                $institute_name = $row0012['INS_NAME']; //Institute Name
                $institute_tp = $row0012['INS_TP']; //Institute Tp
                $institute_address = $row0012['INS_ADDRESS']; //Institute Address
                $institute_mobile = $row0012['INS_MOBILE']; //Institute Mobile No
                $institute_picture = $row0012['PICTURE']; //Institute Picture

                $institute_free_period = $row0012['FREE']; // **Institute Free Period**

            }



            //Check Free

            if($institute_free_period == 'Yes')
            {

                $price_status = '<h3 class="base-color" style="color:#41e266"><span class="fa fa-gift"></span> Free <small style="font-size:12px;font-weight: bold;">Period</small></h3>';

            }else
            if($institute_free_period == 'No')
            {
                if($class_free_card == '0')
                {
                    $price_status = '<h3 class="base-color">'.number_format($class_fees).'<span style="font-size: 10px;"> LKR/month</span></h3>';
                }else
                if($class_free_card>0)
                {
                    $price_status = '<h3 class="base-color" style="color:#41e266"><span class="fa fa-gift"></span> Free <small style="font-size:12px;font-weight: bold;">Card</small></h3>';
                }
            }

            

            //Check Free

            ?>
                <!-- Register Class Box -->

                <div class="col-lg-6">
                    <div class="pricing" style="border: 1px solid #cccc;" id="remove_box<?php echo $class_id; ?>">
                        <div class="content">

                              <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-8 image"><center><img src="../teacher/images/profile/<?php echo $teacher_picture; ?>" alt="" style="border-radius: 80px;width: 100px;height: 100px;background-size: cover;"></center></div>
                                <div class="col-md-2"></div>
                              </div>


                          <h6 class="text-dark mt-10 col-md-12"><b><?php echo $class_name; ?></b></h6>
                        <label class="text-center"><?php echo $teacher_full_name; ?></label>
                        <br>
                        <label class="col-md-12 text-center base-color" style="font-weight: bold;font-size: 12px;"><?php $val = preg_replace('/( )+/', ' ', $class_day); $val_arr = str_getcsv($val); $result = implode(', ', $val_arr); echo $result; ?></label>
                        <br>
                        <label class="text-center text-muted" style="font-weight: bold;font-size: 12px;"><?php echo $class_start_time; ?> - <?php echo $class_end_time; ?></label>
                        <br>
                        
                        <?php echo $price_status; //Price Status ?>
                        
                          <div class="col-md-12">

                            <!-- LMS Material -->
        <?php 

            $student_session_id = $_SESSION['STU_ID']; //Get Student id using session

            $class_day = date('l'); //Day

            $today_date = date('Y-m-d'); //Today Date

            $today__date_time = date('Y-m-d h:i:s A'); //Today Date & Time

            $new_check_transaction = 0;

            $this_year = date('Y'); //This Year

            $this_month = date('m'); //This Month

            $i = 0;
            $j = 0;
            
            
            $sql0010 = mysqli_query($conn,"SELECT * FROM `classes` WHERE `CLASS_ID` = '$class_id' "); //check Today available classes

            $check_today_available_class = mysqli_num_rows($sql0010);

            if($check_today_available_class > 0)
            {

                while($row0010 = mysqli_fetch_assoc($sql0010))
                {
                   $class_id = $row0010['CLASS_ID']; //Class ID
                     $class_name = $row0010['CLASS_NAME']; //Class Name
                    $class_fees = $row0010['FEES']; //Class Fees
                    $class_start_time = $row0010['START_TIME']; //Class Start time
                    $class_end_time = $row0010['END_TIME']; //Class End time
                    $subject_id = $row0010['SUB_ID']; //Subject ID

                    $free_class = $row0010['FREE']; //Free Class
                    $limited_student = $row0010['LIMIT_STUDENT']; //Limit Student

                    $link_msg = ''; //message

                    $zoom_link = $row0010['ZOOM_LINK']; //Zoom Link
                    $whatsapp_link = $row0010['WHATSAPP_LINK']; //Whatsapp Link


                    $str = strtotime($class_start_time);
                    $class_start_time = date('h:i A',$str);

                    $str2 = strtotime($class_end_time);
                    $class_end_time = date('h:i A',$str2);




                    $class_teach_id = $row0010['TEACH_ID']; //Class Teach ID
                    $class_subject_id = $row0010['SUB_ID']; //Class Subject ID

                    $sql0011 = mysqli_query($conn,"SELECT * FROM `teacher_details` WHERE `TEACH_ID` = '$class_teach_id' "); //check Classes for registered teachers details
                    while($row0011 = mysqli_fetch_assoc($sql0011))
                    {

                        $teacher_position = $row0011['POSITION']; //Teacher Position
                        $teacher_f_name = $row0011['F_NAME']; //Teacher First Name
                        $teacher_l_name = $row0011['L_NAME']; //Teacher Last Name
                        $teacher_gender = $row0011['GENDER']; //Teacher Gender
                        $teacher_qualification = $row0011['QUALIFICATION']; //Teacher Qualification

                        $teacher_picture = $row0011['PICTURE']; //Teacher Qualification

                        $teacher_full_name = $teacher_position.". ".$teacher_f_name." ".$teacher_l_name;


                        //If Teacher Picture zero,Check gender and gender to select picture 
                        if($teacher_picture == '0')
                        {
                            if($teacher_gender == 'Male')
                            {
                                $teacher_picture = 'b_teacher.jpg';
                            }else
                            if($teacher_gender == 'Female')
                            {
                                $teacher_picture = 'g_teacher.jpg';
                            }
                        }
                        //If Teacher Picture zero,Check gender and gender to select picture 


                    }

                    $sql0012 = mysqli_query($conn,"SELECT * FROM `institute` WHERE `INS_ID` = '1' "); //Institute Data assign to variables
                    while($row0012 = mysqli_fetch_assoc($sql0012))
                    {

                        $institute_name = $row0012['INS_NAME']; //Institute Name
                        $institute_tp = $row0012['INS_TP']; //Institute Tp
                        $institute_address = $row0012['INS_ADDRESS']; //Institute Address
                        $institute_mobile = $row0012['INS_MOBILE']; //Institute Mobile No
                        $institute_picture = $row0012['PICTURE']; //Institute Picture

                        $institute_free_period = $row0012['FREE']; // **Institute Free Period**

                        $class_limit = $row0012['CLZ_LIMIT']; // **Institute class limit**


                    }


                    $sql0013 = mysqli_query($conn,"SELECT * FROM `transactions` WHERE `CLASS_ID` = '$class_id' AND `TEACH_ID` = '$class_teach_id' AND `STU_ID` = '$student_session_id'"); //check Classes for registered teachers details

                    $check_transaction = mysqli_num_rows($sql0013);

                    $new_check_transaction = $new_check_transaction+$check_transaction;

                    //Check Available registered Classes?

                    if($check_transaction == '0')
                    {
                        $_SESSION['not_registered_students'] = $new_check_transaction; //Not registered student create a session and add value
                    }

                    //Check Available registered Classes?


                    while($row0013 = mysqli_fetch_assoc($sql0013))
                    {

                        $student_free_card = $row0013['FREE_CLASS']; //Student Free Card

                        $student_transaction_id = $row0013['TRA_ID']; //Student Transaction ID  
                        $student_class_id = $row0013['CLASS_ID']; //Student Class ID

                        /* Check Payament Already to this month*/

                        $sql0014 = mysqli_query($conn,"SELECT * FROM `payment_data` WHERE `MONTH` = '$this_month' AND `YEAR` = '$this_year' AND `STU_ID` = '$student_session_id' AND `CLASS_ID` = '$student_class_id'"); //Check Student Paid?

                        $payment_status = mysqli_num_rows($sql0014); //Check Already Paid?

                        if($payment_status > 0)
                        {
                            while($row0014 = mysqli_fetch_assoc($sql0014))
                            {
                                $payment_id = $row0014['PAY_ID'];//Payment ID
                                $payment_time = $row0014['PAY_TIME']; //Paid Time
                                $payment_admin_decision = $row0014['ADMIN_SUBMIT']; //Administrator Decision
                                $payment_method = $row0014['PAY_METHOD']; //Payment Method
                                $payment_first_uploaded_bank_slip = $row0014['FILE_NAME']; //1st time uploaded bank slip
                                $payment_second_uploaded_bank_slip = $row0014['LAST_UPLOAD_FILE']; //2nd time uploaded bank slip

                            }  
                        }
                        
                        $sql0016 = mysqli_query($conn,"SELECT * FROM `classes` WHERE `CLASS_ID` = '$class_id'"); //check Classes for registered teachers details
                        while($row0016 = mysqli_fetch_assoc($sql0016))
                        {
                            $student_subject_id002 = $row0016['SUB_ID'];
                        }

                        /* Subject Details */

                        $sql0015 = mysqli_query($conn,"SELECT * FROM `subject` WHERE `SUB_ID` = '$student_subject_id002' "); //Institute Data assign to variables
                        while($row0015 = mysqli_fetch_assoc($sql0015))
                        {
                            $subject_name = $row0015['SUBJECT_NAME']; //Subject Name

                        }

                        /* Subject Details */


                /* ============================================ Check Show Status Message, Button =====================================*/


                        if($institute_free_period == 'Yes')
                        {
                            /* Enabled Free Period */

                            $status_message = 'Free'; //Status message
                            $status_icon = 'fas fa-gift'; //Status icon

                            $show_button = "00"; //Show Buttons (Free Period)

                            /* Enabled Free Period */

                        }else
                        if($institute_free_period == 'No')
                        {
                            /*Check Enabled Free Class*/

                            if($free_class > 0)
                            {

                                /* Enabled Free Class */

                                    $status_message = 'Free'; //Status message
                                    $status_icon = 'fas fa-gift'; //Status icon

                                    $show_button = "14"; //Show Buttons (Free Class)

                                /* Enabled Free Class */



                            }else
                            if($free_class == '0')
                            {
                                /*Check Enabled Free Class*/

                            /* Not Enabled Free Period but Enabled Free Card? */


                                if($student_free_card > 0)
                                {

                                    /* Enabled Free Card? */

                                    $status_message = 'Free'; //Status message
                                    $status_icon = 'fas fa-gift'; //Status icon

                                    $show_button = "01"; //Show Buttons (Free Card)

                                    /* Enabled Free Card? */

                                }else
                                if($student_free_card == '0')
                                {
                                    /* Check Paid Already */


                                    if($payment_status == '0')
                                    {

                                        /* Already Not Paid*/

                                        $status_message = 'Pay'; //Status message
                                        $status_icon = 'fas fa-coins'; //Status icon

                                        $show_button = "10"; //Show Buttons (Not Paid Yet)

                                        /* Already Not Paid*/

                                    }else
                                    if($payment_status > 0)
                                    { /* Already Paid*/

                                        if($payment_admin_decision == '0')
                                        {
                                            /* Admin Decided Pending to Bank Slip */

                                            $status_message = 'Pay'; //Status message
                                            $status_icon = 'fas fa-coins'; //Status icon
                                            $show_button = "11"; //Show Buttons (Not Paid Yet)

                                            /* Admin Decided Pending to Bank Slip */

                                        }else
                                        if($payment_admin_decision == '1')
                                        {
                                            /* Admin Decided Approved to Bank Slip */

                                            $status_message = 'Join'; //Status message
                                            $status_icon = 'fas fa-check-circle'; //Status icon
                                            $show_button = "12"; //Show Buttons (Not Paid Yet)


                                            /* Admin Decided Approved to Bank Slip */

                                        }if($payment_admin_decision == '2')
                                        {
                                            /* Admin Decided Reject to Bank Slip */

                                            $status_message = 'Pay'; //Status message
                                            $status_icon = 'fas fa-coins'; //Status icon
                                            $show_button = "13"; //Show Buttons (Not Paid Yet)

                                            /* Admin Decided Reject to Bank Slip */

                                        }

                                        

                                    }/* Already Paid*/
                                    
                                }/* Check Paid Already */
                            }//Free Class Enabled
                            
                        }/* Not Enabled Free Period but Enabled Free Card? */


            /* ============================================ Check Show Status Message, Button =====================================*/



/* ================================================== CODE BODY =================================================================*/
                     

                                    /* 
                                        00 = Free Period
                                        01 = Free Card
                                        10 = Not Paid yet
                                        11 = Pending Bank Slip
                                        12 = Paid Already
                                        13 = Rejected Bank Slip

                                        14 = Free Class

                                        $show_button = Buttons Show

                                    */

                                    if($show_button == '00' || $show_button == '14')
                                    { 
                                        //Free Period

                                        ?>
                                        
                                        <!-- opacity:0.4;cursor:not-allowed;pointer-events: none; -->
                                        <a onclick="unregister_subject('<?php echo $student_session_id ?>','<?php echo $class_id; ?>');" class="btn-link" data-toggle="tooltip" title="මෙම පන්තිය ලියාපදිංචියෙන් ඉවත්කර ගැනීම." style="opacity: 0.5;cursor: pointer;">
                                            <span><i class="fas fa-times text-muted"></i></span>
                                            <span>Unregister</span>
                                        </a>


                                        <a href="lms/lms.php?class_id=<?php echo $class_id; ?>" class="btn btn-link btn-block text-muted" style="color: bold;font-weight: initial;" data-toggle="tooltip" title="LMS වෙත පිවිසෙන්න."><i class="fas fa-parachute-box"></i>&nbsp;Class Material</a>


                                        <?php
                                    }else
                                    if($show_button == '01')
                                    {
                                        //Free Card


                                        ?>


                                        <!-- opacity:0.4;cursor:not-allowed;pointer-events: none; -->
                                        <a onclick="unregister_subject('<?php echo $student_session_id ?>','<?php echo $class_id; ?>');" class="btn-link" data-toggle="tooltip" title="මෙම පන්තිය ලියාපදිංචියෙන් ඉවත්කර ගැනීම." style="opacity: 0.5;cursor: pointer;">
                                            <span><i class="fas fa-times text-muted"></i></span>
                                            <span>Unregister</span>
                                        </a>

                                        <a href="lms/lms.php?class_id=<?php echo $class_id; ?>" class="btn btn-link btn-block text-muted" style="color: bold;font-weight: initial;" data-toggle="tooltip" title="LMS වෙත පිවිසෙන්න."><i class="fas fa-parachute-box"></i>&nbsp;Class Material</a>


                                        <?php
                                    }else
                                    if($show_button == '10')
                                    {
                                        //Not Paid
                                        ?>

                                        <button type="button" class="btn btn-custom" onclick="open_modal('<?php echo $class_name; ?>','<?php echo $class_fees; ?>','<?php echo $class_id; ?>','<?php echo $student_transaction_id; ?>')" style="font-weight: bold" data-toggle="tooltip" title="පන්තියට අදාල ගෙවීම් කටයුතු සිදුකිරීම‍."><i class="fas fa-coins"></i>&nbsp;Payment</button>

                                        <a href="lms/lms.php?class_id=<?php echo $class_id; ?>" class="btn btn-link btn-block text-muted disabled" style="color: bold;font-weight: initial;cursor: not-allowed;" data-toggle="tooltip" title="LMS වෙත පිවිසෙන්න."><i class="fas fa-parachute-box"></i>&nbsp;Class Material</a>

                                        <!-- opacity:0.4;cursor:not-allowed;pointer-events: none; -->
                                        <a onclick="unregister_subject('<?php echo $student_session_id ?>','<?php echo $class_id; ?>');" class="btn-link" data-toggle="tooltip" title="මෙම පන්තිය ලියාපදිංචියෙන් ඉවත්කර ගැනීම." style="opacity: 0.5;cursor: pointer;">
                                            <span><i class="fas fa-times text-muted"></i></span>
                                            <span>Unregister</span>
                                        </a>

                                        <?php
                                    }else
                                    if($show_button == '11')
                                    {
                                        //Pending

                                        echo '<h3 class="base-color"><i class="fa fa-spinner"></i> <span style="font-size: 14px;"> Pending</span></h3>';
                                        ?>

                                        <!-- opacity:0.4;cursor:not-allowed;pointer-events: none; -->
                                        <button onclick="unregister_subject('<?php echo $student_session_id ?>','<?php echo $class_id; ?>');" class="btn-custom" data-toggle="tooltip" title="මෙම පන්තිය ලියාපදිංචියෙන් ඉවත්කර ගැනීම.">
                                            <span><i class="fas fa-times text-muted"></i></span>
                                            <span>Unregister</span>
                                        </button>

                                        <a href="lms/lms.php?class_id=<?php echo $class_id; ?>" class="btn btn-link btn-block text-muted disabled" style="color: bold;font-weight: initial;;cursor: not-allowed;" data-toggle="tooltip" title="LMS වෙත පිවිසෙන්න."><i class="fas fa-parachute-box"></i>&nbsp;Class Material</a>

                                        <?php
                                    }else
                                    if($show_button == '12')
                                    {
                                        //Paid


                                        echo '<h3 class="base-color"><i class="fa fa-check-circle"></i> <span style="font-size: 14px;"> Paid</span></h3>';
                                        ?>

                                        <!-- opacity:0.4;cursor:not-allowed;pointer-events: none; -->
                                        <button onclick="unregister_subject('<?php echo $student_session_id ?>','<?php echo $class_id; ?>');" class="btn-custom" data-toggle="tooltip" title="මෙම පන්තිය ලියාපදිංචියෙන් ඉවත්කර ගැනීම.">
                                            <span><i class="fas fa-times text-muted"></i></span>
                                            <span>Unregister</span>
                                        </button>

                                        <a href="lms/lms.php?class_id=<?php echo $class_id; ?>" class="btn btn-link btn-block text-muted" style="color: bold;font-weight: initial;" data-toggle="tooltip" title="LMS වෙත පිවිසෙන්න."><i class="fas fa-parachute-box"></i>&nbsp;Class Material</a>



                                        <?php
                                    }else
                                    if($show_button == '13')
                                    {
                                        
                                        //Rejected


                                        echo '<h3 class="base-color"><i class="fa fa-times"></i> <span style="font-size: 14px;"> Rejected</span></h3>';


                                            ?>

                                            <button type="submit" class="btn btn-custom" onclick="rejected_upload('<?php echo $payment_first_uploaded_bank_slip; ?>','<?php echo $payment_id; ?>')" style="font-weight: bold" data-toggle="tooltip" title="ඔබ එවන ලද රිසිට්පතෙහි යම් දෝෂයක් පවතී.නැවත පරික්ෂා කර Upload කරන්න."><i class="fas fa-plus"></i>&nbsp;Rejected</button>
                                            <!-- opacity:0.4;cursor:not-allowed;pointer-events: none; -->
                                            

                                            <a href="lms/lms.php?class_id=<?php echo $class_id; ?>" class="btn btn-link btn-block text-muted disabled" style="color: bold;font-weight: initial;;cursor: not-allowed;" data-toggle="tooltip" title="LMS වෙත පිවිසෙන්න."><i class="fas fa-parachute-box"></i>&nbsp;Class Material</a>

                                        <!-- opacity:0.4;cursor:not-allowed;pointer-events: none; -->
                                        <a onclick="unregister_subject('<?php echo $student_session_id ?>','<?php echo $class_id; ?>');" class="btn-link" data-toggle="tooltip" title="මෙම පන්තිය ලියාපදිංචියෙන් ඉවත්කර ගැනීම." style="opacity: 0.5;cursor: pointer;">
                                            <span><i class="fas fa-times text-muted"></i></span>
                                            <span>Unregister</span>
                                        </a>
                                            <?php

                                    }

                                    // oncontextmenu="return false;" = Use to disable right click
                                 ?>


                    <?php

/* ================================================== CODE BODY =================================================================*/
                } // transaction table while loop

            } //Class Available while Loop}

        }
    

         ?>

         <!-- LMS Matrial -->


                          </div>
                            
                        </div>
                    </div>
                </div>
                

                <!-- Register Class Box -->

            <?php


          }
        }else
        if($check001 == '0')
        {
          /*Empty Payments*/

            ?>


            <div class="col-md-12 row mt-20" style="padding: 10px 10px 10px 10px;">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                        <center>
                            <img src="assets/img/icon/unavailable.svg" style="width: 250px;height: 250px;">
                            <h5 class="mt-20 text-muted"><span class="fas fa-search"></span> Classes Registered Unavailable</h5>
                        </center>
                </div>
                <div class="col-md-3"></div>
            </div>


            <?php

            /*Empty Payments*/
        }

       ?>

    </div>
    <script type="text/javascript">
        function unregister_subject(student_id,class_id)
        {
            if(confirm('Are you sure unregister this subject?'))
            {
                $.ajax({
                type:"POST",
                url:"../student/query/delete.php",
                data : {unregister_subject:student_id,class_id:class_id},
                success: function (data) {
                  
                    Swal.fire(
                      'Unregistered!',
                      'Registered Subjected has beed unregistered!',
                      'success'
                      
                    ).then((result) => {
                      if (result.isConfirmed) {

                            document.getElementById('remove_box'+class_id+'').style.visibility = "hidden";
                         
                      }
                    });
                    
                }
              });
            }
            

        }

    </script>

    </div> 