<?php 
    session_start();
    
    include('../connect/connect.php');
      
      $teach_id = $_SESSION['TEACH_ID'];


      $dashboard_tab_color = '#3498db';
      $dashboard_hover_tab_color = '#2980b9';

      $s01 = mysqli_query($conn,"SELECT * FROM teacher_details WHERE TEACH_ID = '$teach_id'");
      while($row = mysqli_fetch_assoc($s01))
      {
        $t_name = $row['POSITION'].". ".$row['F_NAME']." ".$row['L_NAME'];
        $dob = $row['DOB'];
        $position = $row['POSITION'];
        $qulification = $row['QUALIFICATION'];
        $email = $row['EMAIL'];
        $address = $row['ADDRESS'];
        $picture = $row['PICTURE'];
        $gender = $row['GENDER'];

         
         if($picture == '0')
         {
            if($gender == 'Male')
            {
              $picture = 'b_teacher.jpg';
            }else
            if($gender == 'Female')
            {
              $picture = 'g_teacher.jpg';
            }
         }
        

        $tp = $row['TP'];

        $ssql01 = mysqli_query($conn,"SELECT * FROM teacher_login WHERE TEACH_ID = '$teach_id'");
        while($row001 = mysqli_fetch_assoc($ssql01))
        {
            $last_time = $row001['LOGIN_TIME'];
        }

      }

      $date_time = date('Y-m-d h:i:s A');
      $update_login = mysqli_query($conn,"UPDATE teacher_login SET LOGOUT_TIME = '$date_time' WHERE TEACH_ID = '$teach_id'");
     
  $url = '';

  if ($_SERVER['HTTP_REFERER'] == $url) {
    header('Location: ../direct_access.php'); //redirect to some other page
    exit();
  }

    if($folder_in == '0')
    {
 ?>

 <!DOCTYPE html>
<html lang="en">

<!-- Mirrored from pages.revox.io/dashboard/latest/html/condensed/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 15 Jul 2020 08:48:43 GMT -->
<head>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta charset="utf-8" />
<title><?php echo $page; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
<script src="cdn-cgi/apps/head/8jwJmQl7fEk_9sdV6OByoscERU8.js"></script><link rel="apple-touch-icon" href="pages/ico/60.png">
<link rel="apple-touch-icon" sizes="76x76" href="pages/ico/76.png">
<link rel="apple-touch-icon" sizes="120x120" href="pages/ico/120.png">
<link rel="apple-touch-icon" sizes="152x152" href="pages/ico/152.png">
<link rel="icon" type="image/x-icon" href="favicon.ico" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-touch-fullscreen" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="default">
<meta content="Meet pages - The simplest and fastest way to build web UI for your dashboard or app." name="description" />
<meta content="Ace" name="author" />
<link href="assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" />
<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="assets/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
<link href="assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" media="screen" />
<link href="assets/plugins/nvd3/nv.d3.min.css" rel="stylesheet" type="text/css" media="screen" />
<link href="assets/plugins/mapplic/css/mapplic.css" rel="stylesheet" type="text/css" />
<link href="assets/plugins/rickshaw/rickshaw.min.css" rel="stylesheet" type="text/css" />
<link href="assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css" media="screen">
<link href="assets/plugins/jquery-metrojs/MetroJs.css" rel="stylesheet" type="text/css" media="screen" />
<link class="main-stylesheet" href="pages/css/pages.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<style type="text/css">
  .no-data {
    display: none;
    text-align: center;
}


::-webkit-scrollbar {
  width: 10px;
}

/* Track */
::-webkit-scrollbar-track {
  background: #f1f1f1; 
}
 
/* Handle */
::-webkit-scrollbar-thumb {
  background: #888; 
  border-radius: 10px;
  
}

/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
  background: #555; 
  border-radius: 10px;
}
.pagination li a {
   padding:8px 16px;
   border:1px solid #ccc;
   color:#333;
   font-weight:bold;
  }

  .counters
{
  padding: 10px 10px 10px 10px;
  background-color: #000000b8;
  border-radius: 10px;
  cursor: pointer;
  width: 100%;
}

.counters:hover
{
  background-color: #2d3436;
}
</style>

</head>
<body class="fixed-header dashboard">




<div class="container-fluid ">

<div class="header ">


<a href="profile_setting.php" class="btn-link toggle-sidebar d-lg-none btn-icon-link" data-toggle="sidebar" style="font-size:18px;">
<span class="fa fa-cog"></span></a>

<div class="">
<div class="brand inline   ">

<!-- <img src="assets/img/logo.png" alt="logo" data-src="assets/img/logos.png" data-src-retina="assets/img/logo_2x.png" width="78" height="22"> -->

<h3 class="text-center"><a href="dashboard.php"><img src="images/ibs.png" height="40px" width="40px"> IBS</a></h3>
</div>


</div>


<div class="d-flex align-items-center">

<a href="logout.php" onclick="return confirm('Are you sure logout?')" class="header-icon m-l-5 sm-no-margin d-inline-block" style="padding-right: 10px;padding-top: 6px;font-size: 18px;">
<span class="fa fa-power-off"></span>
</a> 

<div class="dropdown pull-right d-lg-block d-none">
<button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" aria-label="profile dropdown">

<span class="thumbnail-wrapper d32 circular inline">
<img src="../teacher/images/profile/<?php echo $picture; ?>" alt="" data-src="../teacher/images/profile/<?php echo $picture; ?>" data-src-retina="../teacher/images/profile/<?php echo $picture; ?>" width="32" height="32">
</span>
</button>
<div class="dropdown-menu dropdown-menu-right profile-dropdown" role="menu">
<a href="#" class="dropdown-item" style="text-transform: capitalize;"><span>Signed <br /><b><?php echo ucwords(strtolower($t_name)); ?></b></span></a>
<div class="dropdown-divider"></div>
<a href="profile_setting.php" class="dropdown-item">Settings</a>
<a href="logout.php" class="dropdown-item" onclick="return confirm('Are you sure logout?')">Logout</a>
</div>
</div>


</div>
</div>


<div class="page-content-wrapper">

<div class="page-content-wrapper ">

<div class="content sm-gutter">
<div class="container-fluid padding-10 sm-padding-0">


  <div class="modal fade slide-up disable-scroll" id="new_upload" tabindex="-1" role="dialog" aria-hidden="false" style="overflow: auto;">
                            <div class="modal-dialog ">
                            <div class="modal-content-wrapper">
                            <div class="modal-content">
                            <div class="modal-header clearfix text-left">
                            <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
                            </button>
                            <h5>Select Teacher</h5>
                            </div>
                            <div class="modal-body">
                              <form action="upload.php" method="GET">

                              <div class="row">
                              <div class="col-md-12">
                              <div class="form-group form-group-default input-group">
                              <div class="form-input-group">
                               <label>Teacher Name</label>
                                  <select class="form-control select_class" name="teach_id" id="teach_id" required style="cursor: pointer;">
                                    <option value="">Select Teacher Name </option>

                                      <?php 

                                          $sql0015 = mysqli_query($conn,"SELECT * FROM teacher_details");
                                          while($row0015=mysqli_fetch_assoc($sql0015))
                                          {
                                            $name = $row0015['F_NAME']." ".$row0015['L_NAME'];
                                            $teach_id = $row0015['TEACH_ID'];

                                            echo '<option value='.$teach_id.'>'.$name.'</option>';

                                          }

                                           ?>
                                    
                                  </select>
                              </div>
                              </div>
                              </div>
                                <button type="submit" class="btn btn-success btn-block btn-lg" ><i class="pg-icon">search</i> Search</button>
                              </div>

                              
                              </form>
                              </div>
                              </div>
                              </div>
                              </div>

                              </div>


                              <div class="modal fade slide-up disable-scroll" id="create_subject" tabindex="-1" role="dialog" aria-hidden="false" style="overflow: auto;">
                            <div class="modal-dialog ">
                            <div class="modal-content-wrapper">
                            <div class="modal-content">
                            <div class="modal-header clearfix text-left">
                            <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
                            </button>
                            <h5>Select Teacher</h5>
                            </div>
                            <div class="modal-body">
                              <form action="setup_link.php" method="GET">

                              <div class="row">
                              <div class="col-md-12">
                              <div class="form-group form-group-default input-group">
                              <div class="form-input-group">
                               <label>Teacher Name</label>
                                  <select class="form-control select_class" name="teach_id" id="teach_id" required style="cursor: pointer;">
                                    <option value="">Select Teacher Name </option>

                                      <?php 

                                          $sql0015 = mysqli_query($conn,"SELECT * FROM teacher_details");
                                          while($row0015=mysqli_fetch_assoc($sql0015))
                                          {
                                            $name = $row0015['F_NAME']." ".$row0015['L_NAME'];
                                            $teach_id = $row0015['TEACH_ID'];

                                            echo '<option value='.$teach_id.'>'.$name.'</option>';

                                          }

                                           ?>
                                    
                                  </select>
                              </div>
                              </div>
                              </div>
                                <button type="submit" class="btn btn-success btn-block btn-lg" ><i class="pg-icon">search</i> Search</button>
                              </div>

                              
                              </form>
                              </div>
                              </div>
                              </div>
                              </div>

                              </div>

                              <div class="modal fade slide-up disable-scroll" id="view_upload" tabindex="-1" role="dialog" aria-hidden="false" style="overflow: auto;">
                            <div class="modal-dialog ">
                            <div class="modal-content-wrapper">
                            <div class="modal-content">
                            <div class="modal-header clearfix text-left">
                            <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
                            </button>
                            <h5>Select Teacher</h5>
                            </div>
                            <div class="modal-body">
                              <form action="view_upload.php" method="GET">

                              <div class="row">
                              <div class="col-md-12">
                              <div class="form-group form-group-default input-group">
                              <div class="form-input-group">
                               <label>Teacher Name</label>
                                  <select class="form-control select_class" name="teach_id" id="teach_id" required style="cursor: pointer;">
                                    <option value="">Select Teacher Name </option>

                                      <?php 

                                          $sql0015 = mysqli_query($conn,"SELECT * FROM teacher_details");
                                          while($row0015=mysqli_fetch_assoc($sql0015))
                                          {
                                            $name = $row0015['F_NAME']." ".$row0015['L_NAME'];
                                            $teach_id = $row0015['TEACH_ID'];

                                            echo '<option value='.$teach_id.'>'.$name.'</option>';

                                          }

                                           ?>
                                    
                                  </select>
                              </div>
                              </div>
                              </div>
                                <button type="submit" class="btn btn-success btn-block btn-lg" ><i class="pg-icon">search</i> Search</button>
                              </div>

                              
                              </form>
                              </div>
                              </div>
                              </div>
                              </div>

                              </div>
<!-- other page -->

  <?php }else
  if($folder_in == '3')
    {

 ?>

 <!DOCTYPE html>
<html lang="en">

<!-- Mirrored from pages.revox.io/dashboard/latest/html/condensed/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 15 Jul 2020 08:48:43 GMT -->
<head>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta charset="utf-8" />
<title><?php echo $page; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
<script src="cdn-cgi/apps/head/8jwJmQl7fEk_9sdV6OByoscERU8.js"></script><link rel="apple-touch-icon" href="pages/ico/60.png">
<link rel="apple-touch-icon" sizes="76x76" href="pages/ico/76.png">
<link rel="apple-touch-icon" sizes="120x120" href="pages/ico/120.png">
<link rel="apple-touch-icon" sizes="152x152" href="pages/ico/152.png">
<link rel="icon" type="image/x-icon" href="favicon.ico" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-touch-fullscreen" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="default">
<meta content="Meet pages - The simplest and fastest way to build web UI for your dashboard or app." name="description" />
<meta content="Ace" name="author" />
<link href="assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" />
<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="assets/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
<link href="assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" media="screen" />
<link href="assets/plugins/jquery-datatable/media/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css" rel="stylesheet" type="text/css" />
<link href="assets/plugins/datatables-responsive/css/datatables.responsive.css" rel="stylesheet" type="text/css" media="screen" />
<link class="main-stylesheet" href="pages/css/pages.css" rel="stylesheet" type="text/css" />
  
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<link class="main-stylesheet" href="assets/css/style.css" rel="stylesheet" type="text/css" />

<style type="text/css">
  .no-data {
    display: none;
    text-align: center;
}


::-webkit-scrollbar {
  width: 10px;
}

/* Track */
::-webkit-scrollbar-track {
  background: #f1f1f1; 
}
 
/* Handle */
::-webkit-scrollbar-thumb {
  background: #888; 
  border-radius: 10px;
  
}

/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
  background: #555; 
  border-radius: 10px;
}
.pagination li a {
   padding:8px 16px;
   border:1px solid #ccc;
   color:#333;
   font-weight:bold;
  }

  .counters
{
  padding: 10px 10px 10px 10px;
  background-color: #000000b8;
  border-radius: 10px;
  cursor: pointer;
  width: 100%;
}

.counters:hover
{
  background-color: #2d3436;
}
</style>

</head>
<body class="fixed-header dashboard">


<div class="page-container ">

<div class="header ">


<a href="profile_setting.php" class="btn-link toggle-sidebar d-lg-none btn-icon-link" data-toggle="sidebar" style="font-size:18px;">
<span class="fa fa-cog"></span></a>

<div class="">
<div class="brand inline   ">

<!-- <img src="assets/img/logo.png" alt="logo" data-src="assets/img/logos.png" data-src-retina="assets/img/logo_2x.png" width="78" height="22"> -->

<h3 class="text-center"><a href="dashboard.php"><img src="images/ibs.png" height="40px" width="40px"> IBS</a></h3>
</div>


</div>


<div class="d-flex align-items-center">

<a href="logout.php" onclick="return confirm('Are you sure logout?')" class="header-icon m-l-5 sm-no-margin d-inline-block" style="padding-right: 10px;padding-top: 6px;font-size: 18px;">
<span class="fa fa-power-off"></span>
</a> 
<div class="dropdown pull-right d-lg-block d-none">
<button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" aria-label="profile dropdown">
<span class="thumbnail-wrapper d32 circular inline">
<img src="../teacher/images/profile/<?php echo $picture; ?>" alt="" data-src="../teacher/images/profile/<?php echo $picture; ?>" data-src-retina="../teacher/images/profile/<?php echo $picture; ?>" width="32" height="32">
</span>
</button>
<div class="dropdown-menu dropdown-menu-right profile-dropdown" role="menu">
<a href="#" class="dropdown-item" style="text-transform: capitalize;"><span>Signed <br /><b><?php echo ucwords(strtolower($t_name)); ?></b></span></a>
<div class="dropdown-divider"></div>
<a href="profile_setting.php" class="dropdown-item">Settings</a>
<a href="logout.php" class="dropdown-item" onclick="return confirm('Are you sure logout?')">Logout</a>
</div>
</div>


</div>
</div>


<div class="page-content-wrapper">

<div class="page-content-wrapper ">

<div class="content sm-gutter">
<div class="container-fluid padding-10 sm-padding-0">


  <div class="modal fade slide-up disable-scroll" id="new_upload" tabindex="-1" role="dialog" aria-hidden="false" style="overflow: auto;">
                            <div class="modal-dialog ">
                            <div class="modal-content-wrapper">
                            <div class="modal-content">
                            <div class="modal-header clearfix text-left">
                            <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
                            </button>
                            <h5>Select Teacher</h5>
                            </div>
                            <div class="modal-body">
                              <form action="upload.php" method="GET">

                              <div class="row">
                              <div class="col-md-12">
                              <div class="form-group form-group-default input-group">
                              <div class="form-input-group">
                               <label>Teacher Name</label>
                                  <select class="form-control select_class" name="teach_id" id="teach_id" required style="cursor: pointer;">
                                    <option value="">Select Teacher Name </option>

                                      <?php 

                                          $sql0015 = mysqli_query($conn,"SELECT * FROM teacher_details");
                                          while($row0015=mysqli_fetch_assoc($sql0015))
                                          {
                                            $name = $row0015['F_NAME']." ".$row0015['L_NAME'];
                                            $teach_id = $row0015['TEACH_ID'];

                                            echo '<option value='.$teach_id.'>'.$name.'</option>';

                                          }

                                           ?>
                                    
                                  </select>
                              </div>
                              </div>
                              </div>
                                <button type="submit" class="btn btn-success btn-block btn-lg" ><i class="pg-icon">search</i> Search</button>
                              </div>

                              
                              </form>
                              </div>
                              </div>
                              </div>
                              </div>

                              </div>


                              <div class="modal fade slide-up disable-scroll" id="create_subject" tabindex="-1" role="dialog" aria-hidden="false" style="overflow: auto;">
                            <div class="modal-dialog ">
                            <div class="modal-content-wrapper">
                            <div class="modal-content">
                            <div class="modal-header clearfix text-left">
                            <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
                            </button>
                            <h5>Select Teacher</h5>
                            </div>
                            <div class="modal-body">
                              <form action="setup_link.php" method="GET">

                              <div class="row">
                              <div class="col-md-12">
                              <div class="form-group form-group-default input-group">
                              <div class="form-input-group">
                               <label>Teacher Name</label>
                                  <select class="form-control select_class" name="teach_id" id="teach_id" required style="cursor: pointer;">
                                    <option value="">Select Teacher Name </option>

                                      <?php 

                                          $sql0015 = mysqli_query($conn,"SELECT * FROM teacher_details");
                                          while($row0015=mysqli_fetch_assoc($sql0015))
                                          {
                                            $name = $row0015['F_NAME']." ".$row0015['L_NAME'];
                                            $teach_id = $row0015['TEACH_ID'];

                                            echo '<option value='.$teach_id.'>'.$name.'</option>';

                                          }

                                           ?>
                                    
                                  </select>
                              </div>
                              </div>
                              </div>
                                <button type="submit" class="btn btn-success btn-block btn-lg" ><i class="pg-icon">search</i> Search</button>
                              </div>

                              
                              </form>
                              </div>
                              </div>
                              </div>
                              </div>

                              </div>

                              <div class="modal fade slide-up disable-scroll" id="view_upload" tabindex="-1" role="dialog" aria-hidden="false" style="overflow: auto;">
                            <div class="modal-dialog ">
                            <div class="modal-content-wrapper">
                            <div class="modal-content">
                            <div class="modal-header clearfix text-left">
                            <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
                            </button>
                            <h5>Select Teacher</h5>
                            </div>
                            <div class="modal-body">
                              <form action="view_upload.php" method="GET">

                              <div class="row">
                              <div class="col-md-12">
                              <div class="form-group form-group-default input-group">
                              <div class="form-input-group">
                               <label>Teacher Name</label>
                                  <select class="form-control select_class" name="teach_id" id="teach_id" required style="cursor: pointer;">
                                    <option value="">Select Teacher Name </option>

                                      <?php 

                                          $sql0015 = mysqli_query($conn,"SELECT * FROM teacher_details");
                                          while($row0015=mysqli_fetch_assoc($sql0015))
                                          {
                                            $name = $row0015['F_NAME']." ".$row0015['L_NAME'];
                                            $teach_id = $row0015['TEACH_ID'];

                                            echo '<option value='.$teach_id.'>'.$name.'</option>';

                                          }

                                           ?>
                                    
                                  </select>
                              </div>
                              </div>
                              </div>
                                <button type="submit" class="btn btn-success btn-block btn-lg" ><i class="pg-icon">search</i> Search</button>
                              </div>

                              
                              </form>
                              </div>
                              </div>
                              </div>
                              </div>

                              </div>
<!-- other page -->

  <?php } ?>