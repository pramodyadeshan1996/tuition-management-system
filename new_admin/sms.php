<?php
  $page = "SMS";
  $folder_in = '0';

      include('header/header.php');

        $s04 = mysqli_query($conn,"SELECT * FROM admin_login WHERE ADMIN_ID = '$admin_id'");
        while($row4 = mysqli_fetch_assoc($s04))
        {
          $name = $row4['ADMIN_NAME'];
        }

 ?>

 <style type="text/css">
   .count {
  
  animation: pulse 1s infinite;
}


@keyframes pulse {
  0% {
    -moz-box-shadow: 0 0 0 0 #ffd945;
    box-shadow: 0 0 0 0 #ffd945;
  }
  70% {
    -moz-box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
  }
  100% {
    -moz-box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
  }
}
 </style>

    <ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-top: 4%;margin-bottom: 0;border-bottom:1px solid #cccc;">
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="SMS"> SMS</li>
    </ol>

  <div class="col-md-12">
        <div class="row">

            <div class="col-md-12" style="margin-top:18px;">

            <?php 
              $presentage = 0;
              $sql001 = mysqli_query($conn,"SELECT * FROM `sms_counter` ORDER BY `SUB_SMS_C_ID` DESC");
              $qu001 = mysqli_fetch_assoc($sql001);
              $pay = $qu001['PAYMENT'];
              $total_sms = $qu001['REMAINED_SMS'];
              $current_sms = $qu001['CURRENT_SMS'];

              $current_sms = (int)$current_sms;
              $total_sms = (int)$total_sms;

              if($current_sms > 0)
              {

                if($current_sms == '0' && $total_sms == '0')
                {
                  $presentage = 0;
                }else {
                   $presentage = ($current_sms/$total_sms)*100;
                }

            ?>
            
            <div class="progress" style="height:40px;box-shadow: 1px 1px 10px 1px #cccc;">
              <div class="progress-bar progress-bar-striped active progress-bar-success" role="progressbar"
              aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo ($current_sms/$total_sms)*100; ?>%">
              </div>
            </div>

            <div class="col-md-12 text-center text-success" style="font-size:20px;font-weight: bold;margin-bottom: 10px;text-shadow: 2px 2px 2px #cccc;">
                  <span class="fa fa-envelope"></span> <?php echo $current_sms; ?>/<?php echo $total_sms; ?> (<?php echo number_format($presentage); ?>%)
              </div>
              <?php 

              }else
              if($current_sms == '0')
              {
                  ?>
                
                <div class="progress" style="height:100px;">
                  <div class="progress-bar progress-bar-striped active progress-bar-success" role="progressbar"
                  aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width:0;">
                  </div>
                </div> 
                  
                  <div class="col-md-12 text-center text-danger" style="font-size:20px;font-weight: bold;margin-bottom: 10px;">
                      <span class="fa fa-warning"></span> Your SMS package has been over.Please recharge your SMS package.
                  </div>
                    <?php 
              }
          ?>

          </div>

          

      <div class="col-md-12 bg-white" style="border:1px solid #cccc;height: auto;">
      
      <div class="col-md-12" style="border-bottom: 1px solid #cccc;">
        <div class="row">

            <div class="col-md-9" style="margin-bottom: 10px;"><h2><a href="dashboard.php" data-toggle="tooltip" data-title="Back"><i class="pg-icon" style="font-size: 22px;">chevron_left</i></a> SMS</h2></div>
            <div class="col-md-3" style="float: right;"></div>
        </div>

      </div>
          

<div class="card card-borderless">
<div class="col-md-12" style="padding-top:15px;">

<div class="tab-content">
<div class="tab-pane active" id="home">
<div class="row column-seperation">



<div class="col-md-12">



<div class="row">

  <?php 

    $sql00222 = mysqli_query($conn,"SELECT * FROM `institute` WHERE `SINGLE_SMS` = '1'");

    $active_single_sms = mysqli_num_rows($sql00222);

    //1795

    if($active_single_sms == '0' || $current_sms == '0')
    {
        $cursor001 = "cursor: not-allowed;opacity:0.7;";
        $event_none001 = "pointer-events:none;";
    }else
    if($active_single_sms > 0 && $current_sms > 0)
    {
        $cursor001 = "";
        $event_none001 = "";
    }



     ?>
    <div class="col-md-1"></div>
   


  <?php 

    $sql00222 = mysqli_query($conn,"SELECT * FROM `institute` WHERE `SMS_ACTIVE` = '1'");

    $active_multi_sms = mysqli_num_rows($sql00222);

    //1795

    if($active_multi_sms == '0' || $current_sms == '0')
    {
        $cursor002 = "cursor: not-allowed;opacity:0.7;";
        $event_none002 = "pointer-events:none;";
    }else
    if($active_multi_sms > 0 && $current_sms > 0)
    {
        $cursor002 = "";
        $event_none002 = "";
    }

     ?>

    <div class="col-md-2" style="margin-top:20px;">

        <div class="col-md-12">
          <a href="sms_quick.php" style="<?php echo $event_none002; ?>" >
            <div class="col-md-12 text-center " style="border:0px solid black;background-color:#d63228;box-shadow:1px 1px 6px 3px #cccc;padding:10px 10px 10px 10px;font-size:80px;color:white;border-radius:10px;<?php echo $cursor002; ?>">
              <span class="fa fa-flash"></span>

              <h5 style="text-transform: uppercase;">Multi SMS</h5>
            </div>
          </a>
        </div>
          
                
    </div>

    <?php 

      $show_payment_btn = 'none';
      $position = $_SESSION['POSITION'];

      if($position == 'S_ADMIN')
      {
        $show_payment_btn = '';
      }

     ?>

    <div class="col-md-2" style="margin-top:20px;display: <?php echo $show_payment_btn; ?>;">

                  <div class="col-md-12">

                    <a href="sms_pay.php"> 

                      <div class="col-md-12 text-center" style="border:0px solid black;background-color:#3498db;box-shadow:1px 1px 6px 3px #cccc;padding:10px 10px 10px 10px;font-size:80px;color:white;border-radius:10px;">
                        <span class="fa fa-plus"></span>
                        <h5>PAYMENTS</h5>
                      </div>

                    </a>
                  </div>
          
                
    </div>


    <div class="col-md-2" style="margin-top:20px;">

        <div class="col-md-12">
          <a href="special_sms.php" style="<?php echo $event_none001; ?>">
            <div class="col-md-12 text-center"style="border:0px solid black;background-color:#6365a5;box-shadow:1px 1px 6px 3px #cccc;padding:10px 10px 10px 10px;font-size:80px;color:white;border-radius:10px;<?php echo $cursor001; ?>" >
              <span class="fa fa-plane"></span>

              <h5>SINGLE SMS</h5>
            </div>
          </a>
        </div>
          
                
    </div>

    <div class="col-md-2" style="margin-top:20px;">

                  <div class="col-md-12">
                    <a href="sms_paid.php"> 
                      <div class="col-md-12 text-center" style="border:0px solid black;background-color:#e67e22;box-shadow:1px 1px 6px 3px #cccc;padding:10px 10px 10px 10px;font-size:80px;color:white;border-radius:10px;">
                        <span class="fa fa-check"></span>

                        <h5>PAID</h5>
                      </div>
                    </a>
                  </div>
          
                
    </div>

    <div class="col-md-2" style="margin-top:20px;">

                  <div class="col-md-12">
                    <a href="sms_sent.php">
                      <div class="col-md-12 text-center" style="border:0px solid black;background-color:#27ae60;box-shadow:1px 1px 6px 3px #cccc;padding:10px 10px 10px 10px;font-size:80px;color:white;border-radius:10px;">
                        <span class="fa fa-send"></span>

                        <h5>SENT</h5>
                      </div>
                    </a>
                  </div>
          
                
    </div>

    <div class="col-md-1"></div>



  </div>
              
</div>
</div>
</div>
</div>



</div>
</div>
</div>
</div>



<script type="text/javascript">
  $('.save_btn').click(function(){
    
    var upload_btn0 = $('#selectedFile0').val();
    var upload_btn = $('#sel_file').val();
      
    if(empty(upload_btn0))
    {
      alert('Please select your audio file.');
    }else
    if(empty(upload_btn))
    {
      alert('Please select your document file.');
    }
   });
</script>

<script type="text/javascript">
  $('.navi').click(function(){
    $('.frm')[0].reset();
      
   });
</script>

<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
<!-- search data -->

<!-- no data message/no found data show in search-->

<script type="text/javascript">
  $(document).ready(function () {

    (function ($) {

        $('#myInput').keyup(function () {
            var rex = new RegExp($(this).val(), 'i');
            $('#myTable tr').hide();
            $('#myTable tr').filter(function () {
                return rex.test($(this).text());
            }).show();
            $('.no-data').hide();
            if($('#myTable tr:visible').length == 0)
            {
                $('.no-data').show();
            }

        })

    }(jQuery));

});
</script>




<?php include('footer/footer.php'); ?>