<?php 
    include '../../../../connect/connect.php';
 ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Sipsatha.lk">
    <meta name="author" content="Sipsatha.lk">
    <meta name="keywords" content="Sipsatha.lk">

    <!-- Title Page-->
    <title>Register</title>

    <!-- Icons font CSS-->
    <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- Vendor CSS-->
    <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="vendor/datepicker/daterangepicker.css" rel="stylesheet" media="all">
<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<!-- Latest compiled JavaScript -->
    <!-- Main CSS-->
    <link href="css/main.css" rel="stylesheet" media="all">
</head>

<body>



    <div class="page-wrapper bg-gra-02 p-t-130 p-b-100 font-poppins" style="padding: 50px 10px 10px 10px;">
        <div class="wrapper wrapper--w680">
            <div class="card card-4">
                <h6 align="right" style="padding: 10px 15px 10px 10px;"><a href="reg_teacher_sin.php" style="color: red;font-weight: bold;"><i class="material-icons" style="font-size: 14px;">translate</i> සිංහල</a></h6>
                <div class="card-body">
                    <h2 class="title">Registration Form</h2>
                    <form method="POST" autocomplete="off" action="../../../query/insert.php">


                            <div class="col-md-12">
                                <div class="input-group">   
                                    <label class="label">Salutation</label>
                                    <select class="input--style-4" style="width: 100%;padding: 10px 10px 10px 10px;" required name="position">
                                        <option value="">Select Your Salutation</option>
                                        <option value="Mr">Mr.</option>
                                        <option value="Mrs">Mrs.</option>
                                        <option value="Ms">Ms.</option>
                                        <option value="Rev">Rev.</option>
                                        <option value="Dr">Dr.</option>
                                        <option value="Prof">Prof.</option>

                                    </select>
                                </div>
                            </div>



                        <div class="row row-space">

                            <div class="col-2">
                                <div class="input-group">   
                                    <label class="label">first name</label>
                                    <input class="input--style-4" type="text" name="first_name"  pattern="[a-zA-Z. ]+" required placeholder="xxx xxxxxx"  onclick="document.getElementById(this).value = "";">
                                    
                                    <label style="color: red;">Enter only in English characters.</label>
                                </div>

                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">last name</label>
                                    <input class="input--style-4" type="text" name="last_name"  pattern="[a-zA-Z. ]+" required placeholder="xxx xxxxxx" onclick="this.value('');">
                                </div>
                            </div>
                        </div>
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Birthday</label>
                                    <div class="input-group-icon">
                                        <input class="form-control" type="date" name="dob" min="<?php $d = strtotime("-80 year"); echo date('Y-m-d',$d); ?>" max="<?php $d = strtotime("-20 year"); echo date('Y-m-d',$d); ?>" style="font-size: 15px;color:#525252;;">
                                    </div>
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Gender</label>
                                    <div class="p-t-10">
                                        <label class="radio-container m-r-45">Male
                                            <input type="radio" checked="checked" name="gender" value="Male">
                                            <span class="checkmark"></span>
                                        </label>
                                        <label class="radio-container">Female
                                            <input type="radio" name="gender" value="Female">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Email</label>
                                    <input class="input--style-4" type="email" name="email" placeholder="xxx xxxxxx"  pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Phone Number</label>
                                    <input class="input--style-4" type="telephone" pattern="[0-9]{10}" minlength="10" maxlength="10" name="number" id="number" required placeholder="0xxx xxxx xx">
                                </div>
                            </div>
                        </div>

                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Address</label>
                                    <input class="input--style-4" type="text" name="address" placeholder="xxxxxxxx">
                                </div>
                            </div>

                            <div class="col-2">
                                <div class="input-group">
                                    <label class="label">Username</label>
                                    <input class="input--style-4" type="text" name="username" placeholder="xxxxxxx" required id="username" onkeyup="this.value = this.value.toLowerCase();">
                                    <lable id="availability6" style="padding: 15px 0px 15px 0px;"></lable>

                                </div>
                            </div>

                            
                        </div>

                        <div class="row row-space">
                                <div class="col-2">
                                    <div class="input-group">
                                        <label class="label">Password</label>
                                        <input class="input--style-4" type="password" name="password" id="password" required placeholder="xxxxxxxxx">
                                    </div>
                                </div>

                                <div class="col-2">
                                    <div class="input-group">
                                        <label class="label">Re-Password</label>
                                        <input class="input--style-4" type="password" name="re-password" id="re-password" required placeholder="xxxxxxxxx"> 
                                    </div>
                                </div>
                        </div>
                        <div class="col-md-12" style="display: none;">
                                <div class="input-group">
                                    <label class="label">Qualification</label>
                                    <textarea class="input--style-4" placeholder="xxxxxxx" name="qualification" style="width: 100%;"></textarea>
                                </div>
                            </div>
                        <div class="col-md-12" id="error"></div>

                        <div class="p-t-15">

                            <button class="btn btn--radius-2 btn--blue save_data" type="submit" style="float: right" name="save_data" id="save_btn"> <span class="fa fa-check-circle text-success"></span> Submit</button>
                            <br>
                            <a href="../../index.php">Login</a>
                        </div>

                    </form>

                </div>
                        

            </div>
        </div><div class="col-md-12" style="text-align: center;" style="padding-bottom: 20px;"><small style="color:white;font-size: 14px;">&copy;IBS Developer Team</small></div>
    </div>

    <!-- Jquery JS-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <!-- Vendor JS-->
    <script src="vendor/select2/select2.min.js"></script>
    <script src="vendor/datepicker/moment.min.js"></script>
    <script src="vendor/datepicker/daterangepicker.js"></script>

    <!-- Main JS-->
    <script src="js/global.js"></script>

</body>

</html>
<!-- end document-->

<script type="text/javascript">
  $('#error').hide(); //error message hidden

  $('#first_name').val('');

  $(document).ready(function(){


    $('#error').hide(); //error message hidden

    //telephone number type when every 4 number between hit space

   

    //telephone number type when every 4 number between hit space


/*   validation form*/
 $('.save_data').prop("disabled",true);
  $('.save_data').css('opacity','0.5');
  $('.save_data').css('cursor','not-allowed');

   $('#password,#re-password').keyup(function(){

   var psw = $('#password').val();
   var re_psw = $('#re-password').val();
  
  if(psw !== '' && re_psw !== '')
  {
        if(psw !== re_psw)
       {    
            $('#error').show();

            $('.save_data').prop("disabled",true);
            $('.save_data').css('opacity','0.5');
            $('.save_data').css('cursor','not-allowed');

            $('#error').html('<div class="alert alert-danger" style="color:red;font-size:20px;"><span class="fa fa-warning text-danger"> Password is not match</span></div>');

       }else
       if(psw == re_psw)
       {    
            $('#error').show();

            $('.save_data').prop("disabled",false);
            $('.save_data').css('opacity','3.5');
            $('.save_data').css('cursor','pointer');

            $('#error').html('<div class="alert alert-success" style="color:green;font-size:20px;"><span class="fa fa-check-circle text-success"> Password is match</span></div>');
       }
   }
   
   });

/*   validation form*/

});
</script>

<script type="text/javascript">
    $(document).ready(function(){  
   $('#username').keyup(function(){
     var username = $("#username").val();
     if(username =='')
     {
      $('#availability6').html('');
     }else
     {
     $.ajax({
      url:'../../../query/check.php',
      method:"POST",
      data:{username:username},
      success:function(data)
      {
        if(data>0)
         {
          $('#availability6').html('<span class="text-danger" style="color:red;font-size:13px;"><i class="fa fa-check"></i> This Username Already Registered</span>');
          $(':button[type="submit"]').prop('disabled', true).css('opacity','0.8').css('cursor','not-allowed');
         
         }
         

         if(data == 0)
         {
          $(':button[type="submit"]').prop('disabled', false).css('opacity','5.8').css('cursor','pointer');;
          $('#availability6').html('');

         }
      }
     })
   }

  });
 });
</script>