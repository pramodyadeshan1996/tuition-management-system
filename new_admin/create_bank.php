

<?php
  $page = "Create Bank";
  $folder_in = '3';

      include('header/header.php');

        $s04 = mysqli_query($conn,"SELECT * FROM admin_login WHERE ADMIN_ID = '$admin_id'");
        while($row4 = mysqli_fetch_assoc($s04))
        {
          $name = $row4['ADMIN_NAME'];
        }

 ?>

    <ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-bottom: 04%;margin-bottom: 0;">
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="Teacher Bank Details"> <a href="class_teacher_bank_data.php">Teacher Bank Details</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="Create Bank"> Create Bank</li>
    </ol>


      <div class="col-md-12 bg-white" style="border:1px solid #cccc;height: auto;">
      

          
          <div class=" container-fluid container-fixed-lg bg-white" style="margin-top: 0px;">
            <div class="card card-transparent">
              <div class="card-header ">

                <form action="../admin/query/insert.php" method="POST">
                  <div class="row">
                    <div class="col-md-6">
                      
                      <a href="class_teacher_bank_data.php" class="text-dark">Back</a>

                    </div>
                    <div class="col-md-3">

                      <?php 

                        if(!empty($_GET['update_bank']))
                        {

                          $update_bank_id = $_GET['update_bank'];

                          $sql0013 = mysqli_query($conn,"SELECT * FROM `bank` WHERE `BANK_ID` = '$update_bank_id'");
                          while($row0013 = mysqli_fetch_assoc($sql0013))
                          {

                              $bank_id2 = $row0013['BANK_ID']; //Bank ID
                              $update_bank_name = $row0013['BANK_NAME']; //Bank Name

                          }

                        }else
                        if(empty($_GET['update_bank']))
                        {
                          $update_bank_name = '';
                        }

                       ?>

                      <input type="text" class="form-control" name="bank_name" id="bank_name" placeholder="Enter Bank Name.." required onkeyup="check_bank();" autofocus value="<?php echo $update_bank_name; ?>">

                      <label class="text-danger mt-2" id="result" style="display: none;"><span class="fa fa-warning"></span> Already added this bank name</label>

                    </div>
                    <div class="col-md-3">

                      <?php 

                        if(!empty($_GET['update_bank']))
                        {

                          $update_bank_id = $_GET['update_bank'];

                          $sql0013 = mysqli_query($conn,"SELECT * FROM `bank` WHERE `BANK_ID` = '$update_bank_id'");
                          while($row0013 = mysqli_fetch_assoc($sql0013))
                          {

                              $bank_id2 = $row0013['BANK_ID']; //Bank ID
                              $update_bank_name = $row0013['BANK_NAME']; //Bank Name

                          }

                          ?>  
                            <input type="hidden" name="my_page" value="<?php echo $_GET['page']; ?>">

                            <div class="row">
                              <div class="col-md-6">
                                  <button type="submit" class="btn btn-primary btn-lg btn-block" id="update_bank_btn" name="update_bank" value="<?php echo $update_bank_id; ?>"><i class="fa fa-edit"></i>&nbsp; Update Bank</button>
                              </div>
                              <div class="col-md-6">
                                  <button type="submit" disabled class="btn btn-success btn-lg btn-block" id="bank_btn" name="create_bank" value="<?php if(!empty($_GET['page'])){echo $_GET['page']; } ?>"><i class="fa fa-plus"></i>&nbsp; Create Bank</button>
                              </div>
                            </div>

                          <?php

                        }else
                        if(empty($_GET['update_bank']))
                        {
                          $update_bank_name = '';

                          ?>

                            <button type="submit" disabled class="btn btn-success btn-lg btn-block" id="bank_btn" name="create_bank" value="<?php if(!empty($_GET['page'])){echo $_GET['page']; } ?>"><i class="fa fa-plus"></i>&nbsp; Create Bank</button>

                          <?php
                        }

                       ?>

                     

                    </div>
                  </div>
                </form>

                <script type="text/javascript">

                    function check_bank()
                    {
                                  
                        var bank_name = document.getElementById('bank_name').value;

                         $.ajax({
                          url:'../admin/query/check.php',
                          method:"POST",
                          data:{check_bank:bank_name},
                          success:function(data)
                          {
                            
                            if(data > 0)
                            {
                              document.getElementById('result').style.display = 'inline-block';
                              document.getElementById('bank_btn').disabled = true;
                              document.getElementById('update_bank_btn').disabled = true;
                            }

                            if(data == 0)
                            {
                              document.getElementById('result').style.display = 'none';
                              document.getElementById('bank_btn').disabled = false;
                              document.getElementById('update_bank_btn').disabled = false;

                            }
                            
                          }
                        });
                  }


                </script>



                <div class="card-body">
                  <!--  style="height: 820px;overflow: auto;" -->
                <div class="table-responsive">
                <table class="table table-hover">
                  <thead>
                      <tr>
                        <th style="width: 1%;">Bank Name</th>
                        <th style="width: 1%;text-align: center;">Action</th>
                      </tr>
                    </thead>
                    <tbody>

                      <?php 
                          /*------------------ Header Pagination --------------------------------*/
                        $record_per_page = 10;

                        $nxt_five_loop = 0;

                        $page = '';

                        if(isset($_GET["page"]))
                        {
                          $page = $_GET["page"];
                        }
                        else
                        {
                         $page = 1;
                        }

                        $start_from = ($page-1)*$record_per_page; //Current page no -1 X Per Page records

                       
                          $sql001 = "SELECT * FROM `bank`  order by `BANK_ID` DESC LIMIT $start_from, $record_per_page";

                        
                          $page_query = "SELECT * FROM `bank` ORDER BY `BANK_ID` DESC";

                        /*------------------ Header Pagination --------------------------------*/

                        $j = 0;
                        
                        //LIMIT = 10-1 x 10 = 90 , 10";
                        $result = mysqli_query($conn, $sql001);
                        $page_result = mysqli_query($conn, $page_query);

                          $check = mysqli_num_rows($result);

                          if($check>0)
                          {
                          while($row001 = mysqli_fetch_assoc($result))
                          {
                            $bank_name = $row001['BANK_NAME'];

                            $bank_id = $row001['BANK_ID'];

                            echo '

                              <tr>
                                <td class="v-align-left">'.$bank_name.'</td>
                                 <td class="v-align-middle text-center">

                                <a href="create_bank.php?update_bank='.$bank_id.'&&page='.$page.'" class="btn btn-success mt-1 mr-1 btn-xs btn-rounded" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;"><i class="pg-icon">edit</i></a>
                                ' ?>
                                <a href="../admin/query/delete.php?delete_my_bank_id=<?php echo $bank_id; ?>&&page=<?php echo $page; ?>" onclick="return confirm('Are you sure delete?')" class="btn btn-danger mt-1 mr-1 btn-xs btn-rounded" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;" data-title="Delete" data-toggle="tooltip"><i class="pg-icon">trash_alt</i></a>

                                <?php echo '

                                </td>
                              </tr>

                            ';
                        
                          }
                        }else
                        if($check == '0')
                        {
                          echo '<tr><td colspan="7" class="text-center text-danger"><span class="fa fa-warning"></span> Not Found Data</td></tr>';
                        } 
                      

                        

                        
                         ?>


                    </tbody>
                </table>
                </div>
              </div>

                <div class="col-md-12" style="width: 100%;overflow: auto;">
                  <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-8" class="php_pagination">
                      
                      <div class="col-md-12">
                    <br />
                    <?php
                    $total_records = mysqli_num_rows($page_result);
                    $total_pages = ceil($total_records/$record_per_page);
                    $start_loop = $page;
                    $difference = $total_pages - $page;
                    if($difference <= 5)
                    {
                     $start_loop = $total_pages - 5;
                    }else
                    if($difference <= 0)
                    {
                      $start_loop = '0';
                    }

                    echo '<ul class="pagination">';
                    $end_loop = $start_loop + 4;
                    $five_loop = $page-5;
                    if($five_loop <= 0)
                    {
                      $five_loop = 1;
                    }
                    if($page > 1)
                    {

                      echo '<li><a href="create_bank.php?page=1">First</a></li>';
                      echo '<li><a href="create_bank.php?page='.($page - 1).'"> < </a></li>';
                      echo '<li><a href="create_bank.php?page='.$five_loop.'"> <<< </a></li>';

                    }
                    if($start_loop !== '1')
                    {
                      $few_no = $start_loop - 1;
                    }else
                    if($start_loop > 0)
                    {
                      $few_no = 1;
                    }
                    
                    for($i=$few_no; $i<=$end_loop+1; $i++)
                    {     
                      if($i == $page)
                      {
                        echo '<li class="active"><a href="create_bank.php?page='.$i.'" style="font-weight:bold;background-color:#2980b9;color:white;">'.$i.'</a></li>';
                      }else
                      {
                        if($i == '0')
                        {
                          continue;
                        }else
                        if($i>0)
                        {
                          echo '<li><a href="create_bank.php?page='.$i.'" >'.$i.'</a></li>';
                        }
                        
                      }

                    }
                    if($page <= $end_loop)
                    {
                      
                      if($page == '1')
                      {
                        $nxt_five_loop = 5;
                      }else
                      if($page >= 5)
                      {
                        $nxt_five_loop = $page+5;

                        if($total_pages < $nxt_five_loop)
                        {
                          $nxt_five_loop = $total_pages;
                        }
                      }else
                      {
                        $nxt_five_loop = $page+5;
                      }

                        echo '<li><a href="create_bank.php?page='.($page + 1).'" > > </a></li>';
                        echo '<li><a href="create_bank.php?page='.$nxt_five_loop.'" > >>> </a></li>';
                        echo '<li><a href="create_bank.php?page='.$total_pages.'" >Last</a></li>';
                      
                    }
                    
                    echo '</ul>';
                    
                    ?>
                    </div>

                    </div>
                  </div>
                    
                </div>
            </div>
            </div>
            </div>
            </div>
            </div>
            </div>
            </div>



<?php include 'footer/footer.php'; ?>


<script type="text/javascript">
    function GetSelected() {
        //Create an Array.
        var selected = new Array();
 
        //Reference the Table.
        var tblFruits = document.getElementById("tblFruits");
 
        //Reference all the CheckBoxes in Table.
        var chks = tblFruits.getElementsByTagName("INPUT");
 
        // Loop and push the checked CheckBox value in Array.
        for (var i = 0; i < chks.length; i++) {
            if (chks[i].checked) 
            {
                var s = selected.push(chks[i].value);

            }
        }
 
        //Display the selected CheckBox values.
        if (selected.length > 0) {
          if(confirm('Are you sure delete?'))
          {

              $.ajax({
               type: "POST",
               data: {delete_multi_todo:selected},
               url: "../admin/query/delete.php",
               success: function(msg){
                 //alert(msg)
                 location.reload();
               }
            
              });
          }
        }else{
         
         alert("Please select todo list!")
        
        }
    };
</script>

<!-- Modal -->
                          