

<?php
  $page = "Seminar";
  $folder_in = '3';

      include('header/header.php');

        $s04 = mysqli_query($conn,"SELECT * FROM admin_login WHERE ADMIN_ID = '$admin_id'");
        while($row4 = mysqli_fetch_assoc($s04))
        {
          $name = $row4['ADMIN_NAME'];
        }

 ?>

 <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Titillium+Web:wght@300&display=swap" rel="stylesheet">

    <ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-bottom: 04%;margin-bottom: 0;">
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="Seminar"> Seminar</li>
    </ol>


      <div class="col-md-12 bg-white" style="border:1px solid #cccc;height: auto;">
      
      <div class="col-md-12" style="border-bottom: 1px solid #cccc;">
        <div class="row">

            <div class="col-md-9" style="margin-bottom: 10px;"><h2><a href="dashboard.php" data-toggle="tooltip" data-title="Back"><i class="pg-icon" style="font-size: 22px;">chevron_left</i></a> Seminar</h2></div>
            <div class="col-md-1"></div>
            <div class="col-md-2" style="margin-top:12px;">
              <button class="btn btn-success btn-lg btn-block mb-2" data-toggle="modal" data-target="#create_seminar"><i class="fa fa-plus"></i>&nbsp;Create Seminar</button>

            </div>

            <div class="modal fade slide-up disable-scroll" id="create_seminar" tabindex="-1" role="dialog" aria-hidden="false" style="overflow:auto;">
              <div class="modal-dialog ">
                  <div class="modal-content-wrapper">
                    <div class="modal-content modal-lg">

                      <div class="modal-header clearfix text-left">
                        <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
                        </button>
                        <h5>Create Seminar</h5>
                      </div>
                      <div class="modal-body">
                        <form action="../admin/query/insert.php" method="POST" enctype="multipart/form-data">
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group form-group-default">
                              <label>Seminar Name</label>
                              <input type="text" name="semi_name" class="form-control change_inputs" placeholder=" Enter Seminar Name" required>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group form-group-default">
                              <label>Seminar Code</label>
                              <?php $six_digit_random_number = random_int(100000, 999999); ?>
                              <input type="text" name="semi_code" readonly style="font-size: 18px;" class="form-control change_inputs text-danger" placeholder=" Enter Seminar Name" required value="<?php echo $six_digit_random_number; ?>">
                            </div>
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group form-group-default">
                              <label>Due Date</label>
                              <input type="date" name="semi_date" min="<?php echo date('Y-m-d'); ?>" class="form-control change_inputs" required>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group form-group-default">
                              <label>Teacher Name</label>

                              <input list="teacher_search" id="teacher_search2" name="semi_teacher" class="form-control" placeholder="Search Teacher Name.." required>

                              <datalist id="teacher_search">
                                <?php 


                                    $sql004 = mysqli_query($conn,"SELECT * FROM `teacher_details`");
                                    while($row004 = mysqli_fetch_assoc($sql004))
                                    {
                                      $teacher_id = $row004['TEACH_ID'];

                                      $mysql = mysqli_query($conn,"SELECT * FROM `teacher_login` WHERE `TEACH_ID`='$teacher_id' AND `CONFIRM` = '1'");

                                      if(mysqli_num_rows($mysql)>0)
                                      {
                                        $teacher_name = $row004['POSITION'].". ".$row004['F_NAME']." ".$row004['L_NAME'];
                                      
                                        echo '<option value="'.$teacher_id.'">'.$teacher_name.'</option>';
                                      }
                                      
                                    }

                                 ?>
                                
                              </datalist>
                            </div>
                          </div>
                        </div>

                        
                        <div class="row">
                          <div class="col-md-6">

                            <div class="form-group form-group-default">
                              <label>Start Time</label>
                              
                              <input type="time" name="semi_start" class="form-control change_inputs" required>

                            </div>

                          </div>
                          <div class="col-md-6">

                            <div class="form-group form-group-default">
                              <label>End Time</label>
                              
                              <input type="time" name="semi_end" class="form-control change_inputs" required>

                            </div>

                          </div>
                        </div>

                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group form-group-default">
                              <label>Seminar Link</label>
                              <textarea name="semi_link" class="form-control" style="height: 40px;font-weight: bold;" placeholder="Enter Seminar Link"></textarea>

                            </div>
                          </div>
                        </div>

                        <div class="row">
                          
                          <div class="col-md-6">
                            <div class="form-group form-group-default">
                              <label>Whatsapp Link</label>
                              <textarea name="whatsapp_link" class="form-control" style="height: 40px;font-weight: bold;" placeholder="Enter Whatsapp Link"></textarea>

                            </div>
                          </div>

                          <div class="col-md-6">
                            <div class="form-group form-group-default">
                              <label>Post Title</label>
                              <textarea name="post_name" class="form-control" style="height: 40px;font-weight: bold;" placeholder="Enter Post Title"></textarea>

                            </div>
                          </div>

                        </div>
                        <div class="row">

                          <div class="col-md-6">
                            <div class="form-group form-group-default">
                              <label>Post Image</label>
                              <input type="file" name="post_image" id="post_image" class="form-control" onchange="post_img();">

                              <script type="text/javascript">
                                function post_img()
                                {
                                  var post_img = document.getElementById('post_image').value;

                                  if(post_img !== '')
                                  {
                                    document.getElementById('publish_id').disabled = false;
                                    document.getElementById('post_msg').style.display = 'none';
                                  }

                                  if(post_img == '')
                                  {
                                    document.getElementById('publish_id').disabled = true;
                                    document.getElementById('post_msg').style.display = 'inline-block';
                                  }
                                  
                                }
                              </script>

                            </div>
                          </div>

                          <div class="col-md-6">
                            <div class="form-group form-group-default">
                              <label>Publish Status</label>
                              <select class="form-control" id="publish_id" name="publish" disabled>
                                <option value="0">Not Publish</option>
                                <option value="1">Publish</option>
                              </select>

                            </div>
                          </div>

                        </div>

                        <small class="text-danger" id="post_msg">පින්තූරය තෝරාගතහොත් පමණි Post එක Publish කල හැක්කේ.</small>

                        <input type="hidden" name="page" value="<?php if(empty($_GET['page'])){echo $page = "1";}else{echo $_GET['page'];}?>">


                        <div class="clearfix"></div>
                        <div class="row m-t-25">
                          <div class="col-xl-6 p-b-10">
                            <p class="small-text hint-text">Click the Add button to save all the data you entered. <br>(ඔබ ඇතුලත් කල සියලු දත්තයන් Save කිරීමට Submit බොත්තම ඔබන්න.)</p>
                          </div>
                          <div class="col-xl-6">
                            <button class="btn btn-primary pull-right btn-lg btn-block" type="submit" name="semi_submit"><i class="fa fa-plus"></i>&nbsp; Submit
                            </button>
                          </div>
                        </div>

                      </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>



        </div>
      </div>

          
          <div class=" container-fluid container-fixed-lg bg-white" style="margin-top: 0px;">
            <div class="card card-transparent">
              <div class="card-header ">

                  <form action="seminar.php" method="POST">
                      <div class="row">
                        <div class="col-md-2" style="margin-top:4px;">
                          <div class="col-md-12" style="padding: 10px 10px 10px 10px;background-color: #000000b8;border-radius: 10px;" data-toggle="tooltip" data-title="Today Student Joined Amount">
                            <center><span class="fa fa-check-circle text-white text-center pt-2" style="font-size: 18px;"></span>
                              <small class="text-white" style="font-size: 20px;">
                                <?php 

                                  $sql004 = mysqli_query($conn,"SELECT * FROM `seminar_master`");
                                  $total_seminar = mysqli_num_rows($sql004);

                                  if($total_seminar < 10)
                                  {
                                    $total_seminar = "0".$total_seminar;
                                  }else
                                  {
                                    $total_seminar = number_format($total_seminar);
                                  }

                                  echo $total_seminar;
                                 ?>
                              </small>
                              <br>
                              <small class="text-white text-center">Total Seminar</small>
                            </center>
                          </div>
                        </div>

                        <div class="col-md-2" style="margin-top:4px;">
                          <div class="col-md-12" style="padding: 10px 10px 10px 10px;background-color: #000000b8;border-radius: 10px;" data-toggle="tooltip" data-title="Today Student Joined Amount">
                            <center><span class="fa fa-check-circle text-white text-center pt-2" style="font-size: 18px;"></span>
                              <small class="text-white" style="font-size: 20px;">
                                <?php 

                                  $sql005 = mysqli_query($conn,"SELECT * FROM `seminar_master` WHERE `PUBLISH` = '1'");
                                  $total_publish = mysqli_num_rows($sql005);

                                  if($total_publish < 10)
                                  {
                                    $total_publish = "0".$total_publish;
                                  }else
                                  {
                                    $total_publish = number_format($total_publish);
                                  }

                                  echo $total_publish;
                                 ?>
                              </small>
                              <br>
                              <small class="text-white text-center">Published Post</small>
                            </center>
                          </div>
                        </div>


                        <div class="col-md-3"> </div>
                      <div class="col-md-3 mt-2">
                        <input list="student_search" id="search-table" name="search_seminar" class="form-control" placeholder="Search Seminar Name | Teacher Name.." >

                        <datalist id="student_search">
                          <?php 

                            $sql002 = mysqli_query($conn,"SELECT * FROM `seminar_master`");
                            while($row002 = mysqli_fetch_assoc($sql002))
                            {
                              $semi_name = $row002['SEM_NAME'];
                              $semi_id = $row002['SEMI_ID'];
                              $teacher_id = $row002['TEACH_ID'];

                              $sql003 = mysqli_query($conn,"SELECT * FROM `teacher_details` WHERE `TEACH_ID` = '$teacher_id'");
                              while($row003 = mysqli_fetch_assoc($sql003))
                              {
                                
                                $teacher_name = $row003['POSITION'].". ".$row003['F_NAME']." ".$row003['L_NAME'];
                              
                                echo '<option value="'.$semi_id.'">'.$semi_name.' | '.$teacher_name.'</option>';
                              }

                              
                            }

                           ?>
                          
                        </datalist>
                      </div>
                      <div class="col-md-1 mt-2"> <button type="submit" class="btn btn-success btn-lg btn-block" name="search_student_btn" style="border-radius: 20px;"><i class="fa fa-search"></i> &nbsp; Search</button></div>

                      <div class="col-md-1 mt-2"> <button type="submit" class="btn btn-danger btn-lg btn-block" name="reset_student_btn" style="border-radius: 20px;"><i class="fa fa-list"></i> &nbsp; Show All</button></div>
                    </div>
                  </form>

                    <div class="clearfix"></div>
                </div>
                <div class="card-body">
                  <!--  style="height: 820px;overflow: auto;" -->
                <div class="table-responsive">
                <table class="table table-hover" id="tblFruits">
                  <thead>
                      <tr>
                        <th style="width: 1%;">Create Date</th>
                        <th style="width: 1%;">Seminar Code</th>
                        <th style="width: 1%;">Due Date</th>
                        <th style="width: 1%;">Seminar Name</th>
                        <th style="width: 1%;">Teacher Name</th>
                        <th style="width: 1%;">Access Count</th>
                        <th style="width: 1%;">Publish Status</th>
                        <th style="width: 1%;text-align: center;">Action</th>
                      </tr>
                    </thead>
                    <tbody>

                      <?php 
                          /*------------------ Header Pagination --------------------------------*/
                        $record_per_page = 10;

                        $nxt_five_loop = 0;

                        $page = '';

                        if(isset($_GET["page"]))
                        {
                          $page = $_GET["page"];
                        }
                        else
                        {
                         $page = 1;
                        }

                        $start_from = ($page-1)*$record_per_page; //Current page no -1 X Per Page records

                        if(isset($_POST['search_seminar']))
                        {
                          $seminar_id = $_POST['search_seminar'];

                          $sql001 = "SELECT * FROM `seminar_master` WHERE  `SEMI_ID` LIKE '%".$seminar_id."%' order by `SEMI_ID` DESC LIMIT $start_from, $record_per_page";


                        }else
                        {
                          $sql001 = "SELECT * FROM `seminar_master` order by `DUE_DATE` DESC LIMIT $start_from, $record_per_page";

                        }

                        /*------------------ Header Pagination --------------------------------*/

                        $j = 0;
                        
                        //LIMIT = 10-1 x 10 = 90 , 10";
                        $result = mysqli_query($conn, $sql001);
                        $page_result = mysqli_query($conn, "SELECT * FROM `seminar_master` ORDER BY `DUE_DATE` DESC");

                          $check = mysqli_num_rows($result);

                          if($check>0)
                          {
                          while($row001 = mysqli_fetch_assoc($result))
                          {
                            $semi_id = $row001['SEMI_ID'];
                            $teach_id = $row001['TEACH_ID'];
                            $register_date = $row001['ADD_TIME'];
                            $access_count = $row001['ATTEND_COUNT'];
                            $semi_code = $row001['SEM_CODE'];
                            $semi_day = $row001['DAY'];
                            $due_date = $row001['DUE_DATE'];
                            $seminar_name = $row001['SEM_NAME'];
                            $semi_link = $row001['SEMINAR_LINK'];

                            $post_name = $row001['POST_NAME'];
                            $post_image = $row001['POST_IMG'];
                            $wh_link = $row001['WHATSAPP_LINK'];
                            $publish = $row001['PUBLISH'];

                            $disable_wh_btn = '';

                            if($wh_link == '' || $wh_link == '0')
                            {
                              $disable_wh_btn = 'opacity:0.7;pointer-events: none;';
                            }

                            if($publish == '0')
                            {
                              $publish_notify = '<small class="badge badge-danger" style="padding:6px 8px 6px 8px;"><span class="fa fa-times"></span> Not Publish</small>';
                            }else
                            if($publish == '1')
                            {
                              $publish_notify = '<small class="badge badge-success" style="padding:6px 8px 6px 8px;"><span class="fa fa-check-circle"></span> Published</small>';
                            }


                            $start_time = $row001['START_TIME']; //Class Start time
                            $end_time = $row001['END_TIME']; //Class End time

                            $str = strtotime($start_time);
                            $class_start_time = date('h:i A',$str);

                            $str2 = strtotime($end_time);
                            $class_end_time = date('h:i A',$str2);

                            $due_time = $class_start_time." - ".$class_end_time;

                            $sql002 = mysqli_query($conn,"SELECT * FROM `teacher_details` WHERE `TEACH_ID` = '$teach_id'");
                            while($row002 = mysqli_fetch_assoc($sql002))
                            {
                              $teacher_name = $row002['POSITION'].". ".$row002['F_NAME']." ".$row002['L_NAME'];
                              $teacher_picture = $row002['PICTURE'];

                            }

                            $st_time001 = strtotime($due_date);
                            $year = date('Y',$st_time001);


                            $st_time002 = strtotime($due_date);
                            $month = date('F',$st_time002);


                            $st_time003 = strtotime($due_date);
                            $day = date('d',$st_time003);
                              
                            $st_time004 = strtotime($start_time);
                            $start_per_hour = date('H:i:s',$st_time004);

                            $set_time = date("M d, Y H:i:s", strtotime(''.$year.'-'.$month.'-'.$day.' '.$start_per_hour.''));

                            echo '

                              <tr>
                                <td class="v-align-middle">'.$register_date.'</td>
                                <td class="v-align-middle"><h4>'.$semi_code.'</h4></td>
                                <td class="v-align-middle"><label style="font-weight:600;">'.$due_date.' <br> <small>'.$due_time.'</small><br> <small class="badge badge-success">'.$semi_day.'</small> 
                                </label></td>
                                <td class="v-align-middle">'.$seminar_name.'</td>
                                <td class="v-align-middle">'.$teacher_name.'</td>
                                <td class="v-align-middle">'.$access_count.'</td>
                                <td class="v-align-middle">'.$publish_notify.'

                                <div style="padding-top:4px;font-size:12px;float:left;" class="clockdiv" data-date2="'.$month.' '.$day.', '.$year.' '.$start_time.'" data-date3="'.$semi_id.'">
                                                  
                                      <span class="days" style="font-weight:600;color:gray;">0</span>
                                      <label class="smalltext" style="font-weight:600;color:gray;"> d </label>
                                    
                                      <span class="hours" style="font-weight:600;color:gray;">0</span>
                                      <label class="smalltext" style="font-weight:600;color:gray;"> hours</label>
                                    
                                      <span class="minutes" style="font-weight:600;color:gray;">0</span>
                                      <label class="smalltext" style="font-weight:600;color:gray;"> m</label>
                                    
                                      <span class="seconds" style="font-weight:600;color:gray;">0</span>
                                      <label class="smalltext" style="font-weight:600;color:gray;"> s</label>
                                  </div>

                                </td>
                                 <td class="v-align-middle text-center">

                                <a href="seminar_student_report.php?seminar_id='.$semi_id.'" target="_blank" class="btn btn-info  btn-xs btn-rounded mt-1 mr-1"  style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;"><i class="pg-icon">users</i></a>';

                                if($post_image !== '0')
                                {
                                  echo '<button type="button" data-toggle="modal" data-target="#preview_post'.$semi_id.'" class="btn btn-primary btn-xs btn-rounded mt-1 mr-1" style="padding:8px 8px 8px 8px;box-shadow:0px 0px 4px 3px #cccc;display:none;" data-title="Approve" data-toggle="tooltip"
                                "><i class="fa fa-expand"></i></button>';
                                }
                                


                                echo '<button type="button" data-toggle="modal" onclick="click_modal('.$semi_id.')" data-target="#edit_seminar'.$semi_id.'" class="btn btn-success btn-xs btn-rounded mt-1 mr-1" style="padding:8px 8px 8px 8px;box-shadow:0px 0px 4px 3px #cccc;" data-title="Approve" data-toggle="tooltip"
                                "><i class="fa fa-edit"></i></button>
                                ' ?>
                                <a href="../admin/query/delete.php?delete_seminar=<?php echo $semi_id; ?>&&page=<?php echo $page; ?>&&current_image=<?php echo $post_image ?>" onclick="return confirm('Are you sure delete?')" class="btn btn-danger mt-1 mr-1 btn-xs btn-rounded" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;" data-title="Delete" data-toggle="tooltip"><i class="pg-icon">trash_alt</i></a>

                                <?php echo '

                                </td>
                              </tr>

                            ';
                        ?>

                        <script>

                              document.addEventListener('readystatechange', event => {
                                if (event.target.readyState === "complete") {
                                    var clockdiv = document.getElementsByClassName("clockdiv");

                                  var countDownDate = new Array();
                                    for (var i = 0; i < clockdiv.length; i++) {
                                        countDownDate[i] = new Array();
                                        countDownDate[i]['el'] = clockdiv[i];
                                        countDownDate[i]['time'] = new Date(clockdiv[i].getAttribute('data-date2')).getTime();

                                        countDownDate[i]['name'] = clockdiv[i].getAttribute('data-date3');
                                        countDownDate[i]['days'] = 0;
                                        countDownDate[i]['hours'] = 0;
                                        countDownDate[i]['seconds'] = 0;
                                        countDownDate[i]['minutes'] = 0;

                                        //alert(countDownDate[i]['name'])

                                    }

                                  var countdownfunction = setInterval(function() {
                                  for (var i = 0; i < countDownDate.length; i++) {
                                                  var now = new Date().getTime();
                                                  var distance = countDownDate[i]['time'] - now;

                                                   countDownDate[i]['days'] = Math.floor(distance / (1000 * 60 * 60 * 24));
                                                   countDownDate[i]['hours'] = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                                                   countDownDate[i]['minutes'] = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                                                   countDownDate[i]['seconds'] = Math.floor((distance % (1000 * 60)) / 1000);

                                                   

                                                   if (distance<0) 
                                                   {
      
                                                      countDownDate[i]['el'].querySelector('.days').innerHTML = 0;
                                                      countDownDate[i]['el'].querySelector('.hours').innerHTML = 0;
                                                      countDownDate[i]['el'].querySelector('.minutes').innerHTML = 0;
                                                      countDownDate[i]['el'].querySelector('.seconds').innerHTML = 0;

                                                  
                                                   }else
                                                   {
                                                        countDownDate[i]['el'].querySelector('.days').innerHTML = countDownDate[i]['days'];
                                                        countDownDate[i]['el'].querySelector('.hours').innerHTML = countDownDate[i]['hours'];
                                                        countDownDate[i]['el'].querySelector('.minutes').innerHTML = countDownDate[i]['minutes'];
                                                        countDownDate[i]['el'].querySelector('.seconds').innerHTML = countDownDate[i]['seconds'];
                                                  }

                                    
                                       }
                                              }, 1000);
                                      }
                                  });

                            </script>
                          

                        <div id="preview_post<?php echo $semi_id; ?>" class="modal fade" role="dialog">
                          <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                              <div class="modal-header">
                                <h4 class="modal-title">Preview Post</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                              </div>
                              <div class="modal-body">
                                

                                      <div class="latest__item2" style="padding:4px 4px 4px 4px;border:1px solid gray;background-color: #2f0173;">

                                      <img src="../web/post/<?php echo $post_image; ?>" style="margin-bottom:2px;width: 100%;height: auto; ">

                                      <div style="padding: 6px 6px 6px 6px;">
                                        <h4 style="color: white;margin-top:10px;text-align:left;font-family: 'Titillium Web', sans-serif;font-weight: 600;"><?php echo $post_name; ?></h4>
                                        <small style="color: white;margin-top:10px;text-align:left;font-size: 16px;font-family: 'Titillium Web', sans-serif;"><?php echo $due_date; ?> <?php echo $due_time; ?> 
                                          <label class="badge badge-success" style="color: white;margin-top:10px;text-align:left;background-color: #e67e22;width: 40px;text-align: center;border-radius: 80px;">Free</label>

                                        </small>
                                        


                                      </div>

                                      <center>
                                        <a href="<?php echo $wh_link; ?>" target="_blank" style="cursor: pointer;<?php echo $disable_wh_btn; ?>">
                                          <label class="badge badge-success" style="background-color:#2ecc71;padding:7px 8px 6px 8px;border-radius: 80px;color: white;font-weight: 600;margin-top: 10px;cursor: pointer;font-size: 14px;width: 85%;"><i class="fa fa-whatsapp"></i> අපගේ ගෘප් එකට සම්බන්ධවීම</label>
                                        </a>

                                        <a href="../student/seminar_login/index.php?sem_code=<?php echo $semi_code; ?>" style="cursor: pointer;" target="_blank">
                                          <label class="badge badge-success" style="font-size: 14px;background-color:#3498db;padding:7px 8px 6px 8px;border-radius: 80px;color: white;font-weight: 600;width: 85%;margin-top: 10px;cursor: pointer;"><span class="fa fa-lock"></span> සම්මන්ත්‍රණය සදහා ලියාපදිංචි වීම</label>
                                        </a>
                                        <br>
                                        <img src="../web/img/loading.gif" style="width:40px;height:40px;">
                                      </center>

                                      </div>                                
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                              </div>
                            </div>

                          </div>
                        </div>


                        <div class="modal fade slide-up disable-scroll" id="edit_seminar<?php echo $semi_id; ?>" tabindex="-1" role="dialog" aria-hidden="false" style="overflow:auto;">
                          <div class="modal-dialog ">
                              <div class="modal-content-wrapper">
                                <div class="modal-content modal-lg">

                                  <div class="modal-header clearfix text-left">
                                    <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
                                    </button>
                                    <h5>Edit Seminar</h5>
                                  </div>
                                  <div class="modal-body">
                                    <form action="../admin/query/update.php" method="POST" enctype="multipart/form-data">
                                    <div class="row">
                                      <div class="col-md-6">
                                        <div class="form-group form-group-default">
                                          <label>Seminar Name</label>
                                          <input type="text" name="semi_name" class="form-control change_inputs" placeholder=" Enter Seminar Name" required value="<?php echo $seminar_name; ?>">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group form-group-default">
                                          <label>Seminar Code</label>
                                          <input type="text" name="semi_code" readonly style="font-size: 18px;" class="form-control change_inputs text-danger" placeholder=" Enter Seminar Name" required value="<?php echo $semi_code; ?>">
                                        </div>
                                      </div>
                                    </div>

                                    <div class="row">
                                      <div class="col-md-6">
                                        <div class="form-group form-group-default">
                                          <label>Due Date</label>
                                          <input type="date" name="semi_date" min="<?php echo date('Y-m-d'); ?>" class="form-control change_inputs" required value="<?php echo $due_date; ?>">
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group form-group-default">
                                          <label>Teacher Name</label>

                                          <input list="teacher_search" id="teacher_search2" name="semi_teacher" class="form-control" placeholder="Search Teacher Name.." required value="<?php echo $teach_id; ?>" onfocus="this.select();">

                                          <datalist id="teacher_search">
                                            <?php 


                                                $sql004 = mysqli_query($conn,"SELECT * FROM `teacher_details`");
                                                while($row004 = mysqli_fetch_assoc($sql004))
                                                {
                                                  $teacher_id = $row004['TEACH_ID'];

                                                  $mysql = mysqli_query($conn,"SELECT * FROM `teacher_login` WHERE `TEACH_ID`='$teacher_id' AND `CONFIRM` = '1'");

                                                  if(mysqli_num_rows($mysql)>0)
                                                  {
                                                    $teacher_name = $row004['POSITION'].". ".$row004['F_NAME']." ".$row004['L_NAME'];
                                                  
                                                    echo '<option value="'.$teacher_id.'">'.$teacher_name.'</option>';
                                                  }
                                                  
                                                }

                                             ?>
                                            
                                          </datalist>
                                        </div>
                                      </div>
                                    </div>

                                    
                                    <div class="row">
                                      <div class="col-md-6">

                                        <div class="form-group form-group-default">
                                          <label>Start Time</label>
                                          
                                          <input type="time" name="semi_start" class="form-control change_inputs" required value="<?php echo $start_time; ?>">

                                        </div>

                                      </div>
                                      <div class="col-md-6">

                                        <div class="form-group form-group-default">
                                          <label>End Time</label>
                                          
                                          <input type="time" name="semi_end" class="form-control change_inputs" required value="<?php echo $end_time; ?>">

                                        </div>

                                      </div>
                                    </div>

                                    <div class="row">
                                      <div class="col-md-12">
                                        <div class="form-group form-group-default">
                                        <label>Seminar Link</label>
                                        <textarea name="semi_link" class="form-control" style="height: 40px;font-weight: bold;" placeholder="Enter Seminar Link"><?php echo $semi_link; ?></textarea>

                                    </div>
                                    <div class="row">
                          
                                      <div class="col-md-6">
                                        <div class="form-group form-group-default">
                                          <label>Whatsapp Link</label>
                                          <textarea name="whatsapp_link" class="form-control" style="height: 40px;font-weight: bold;" placeholder="Enter Whatsapp Link"><?php echo $wh_link; ?></textarea>

                                        </div>
                                      </div>

                                      <div class="col-md-6">
                                        <div class="form-group form-group-default">
                                          <label>Post Title</label>
                                          <textarea name="post_name" class="form-control" style="height: 40px;font-weight: bold;" placeholder="Enter Post Title"><?php echo $post_name; ?></textarea>

                                        </div>
                                      </div>

                                    </div>
                                    <div class="row">

                                      <div class="col-md-6">

                                        <input type="hidden" name="current_image" id="current_image<?php echo $semi_id; ?>" value="<?php echo $post_image; ?>" class="form-control">

                                        <div class="form-group form-group-default">
                                          <label>Post Image</label>
                                          
                                          

                                          <input type="file" name="post_image" class="form-control" onchange="post_img2(<?php echo $semi_id; ?>);" id="select_image<?php echo $semi_id; ?>">

                                        </div>
                                      </div>

                                      



                                      <div class="col-md-6">
                                        <div class="form-group form-group-default">
                                          <label>Publish Status</label>
                                          <select class="form-control" name="publish" id="publish_id2<?php echo $semi_id; ?>" disabled>

                                            <?php 

                                              if($publish == '0')
                                              {
                                                echo '

                                                  <option value="0">Not Publish</option>
                                                  <option value="1">Publish</option>

                                                ';
                                              }else
                                              if($publish == '1')
                                              {
                                                echo '

                                                  <option value="1">Publish</option>
                                                  <option value="0">Not Publish</option>

                                                ';
                                              }

                                             ?>
                                          </select>

                                        </div>
                                      </div>

                                    </div>
                                        

                                      <small class="text-danger" id="post_msg2<?php echo $semi_id; ?>">පින්තූරය තෝරාගතහොත් පමණි Post එක Publish කල හැක්කේ.</small>
                                      
                                    
                                      </div>
                                    </div>
                                    <input type="hidden" name="page" value="<?php echo $page ?>">

                                    <div class="clearfix"></div>
                                    <div class="row m-t-25">
                                      <div class="col-xl-6 p-b-10">
                                        <p class="small-text hint-text">Click the Update button to change all the data you have changed. (ඔබ වෙනස් කල සියලු දත්තයන් වෙනස් කිරීමට Update බොත්තම ඔබන්න.)</p>
                                      </div>
                                      <div class="col-xl-6">
                                        <button class="btn btn-primary pull-right btn-lg btn-block" type="submit" name="semi_update" value="<?php echo $semi_id; ?>"><i class="fa fa-edit"></i>&nbsp; Update
                                        </button>
                                      </div>
                                    </div>

                                  </form>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        <?php
                          }
                        }else
                        if($check == '0')
                        {
                          echo '<tr><td colspan="7" class="text-center text-danger"><span class="fa fa-warning"></span> Not Found Data</td></tr>';
                        } 
                      

                        

                        
                         ?>


                    </tbody>
                </table>

                </div>
              </div>

              <script type="text/javascript">

                function click_modal(semi_id)
                {


                  var img2 = document.getElementById('current_image'+semi_id+'').value;
                  //alert(img2)
                  if(img2 !== '0')
                  {
                    document.getElementById('publish_id2'+semi_id+'').disabled = false;
                    document.getElementById('post_msg2'+semi_id+'').innerHTML = 'පින්තූරය තෝරා ඇත';
                  }

                   if(img2 == '0')
                  {
                    document.getElementById('publish_id2'+semi_id+'').disabled = true;
                    document.getElementById('post_msg2'+semi_id+'').style.display = 'inline-block';
                  }
                }


                function post_img2(semi_id)
                {
                  var img = document.getElementById('select_image'+semi_id+'').value;

                  if(img !== '')
                  {
                    document.getElementById('publish_id2'+semi_id+'').disabled = false;
                    document.getElementById('post_msg2'+semi_id+'').style.display = 'none';
                  }

                   if(img == '')
                  {
                    document.getElementById('publish_id2'+semi_id+'').disabled = true;
                    document.getElementById('post_msg2'+semi_id+'').style.display = 'inline-block';
                  }
                }
              </script>

                <div class="col-md-12" style="width: 100%;overflow: auto;">
                  <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-8" class="php_pagination">
                      
                      <div class="col-md-12">
                    <br />
                    <?php
                    $total_records = mysqli_num_rows($page_result);
                    $total_pages = ceil($total_records/$record_per_page);
                    $start_loop = $page;
                    $difference = $total_pages - $page;
                    if($difference <= 5)
                    {
                     $start_loop = $total_pages - 5;
                    }else
                    if($difference <= 0)
                    {
                      $start_loop = '0';
                    }

                    echo '<ul class="pagination">';
                    $end_loop = $start_loop + 4;
                    $five_loop = $page-5;
                    if($five_loop <= 0)
                    {
                      $five_loop = 1;
                    }
                    if($page > 1)
                    {

                      echo '<li><a href="seminar.php?page=1">First</a></li>';
                      echo '<li><a href="seminar.php?page='.($page - 1).'"> < </a></li>';
                      echo '<li><a href="seminar.php?page='.$five_loop.'"> <<< </a></li>';

                    }
                    if($start_loop !== '1')
                    {
                      $few_no = $start_loop - 1;
                    }else
                    if($start_loop > 0)
                    {
                      $few_no = 1;
                    }
                    
                    for($i=$few_no; $i<=$end_loop+1; $i++)
                    {     
                      if($i == $page)
                      {
                        echo '<li class="active"><a href="seminar.php?page='.$i.'" style="font-weight:bold;background-color:#2980b9;color:white;">'.$i.'</a></li>';
                      }else
                      {
                        if($i == '0')
                        {
                          continue;
                        }else
                        if($i>0)
                        {
                          echo '<li><a href="seminar.php?page='.$i.'" >'.$i.'</a></li>';
                        }
                        
                      }

                    }
                    if($page <= $end_loop)
                    {
                      
                      if($page == '1')
                      {
                        $nxt_five_loop = 5;
                      }else
                      if($page >= 5)
                      {
                        $nxt_five_loop = $page+5;

                        if($total_pages < $nxt_five_loop)
                        {
                          $nxt_five_loop = $total_pages;
                        }
                      }else
                      {
                        $nxt_five_loop = $page+5;
                      }

                        echo '<li><a href="seminar.php?page='.($page + 1).'" > > </a></li>';
                        echo '<li><a href="seminar.php?page='.$nxt_five_loop.'" > >>> </a></li>';
                        echo '<li><a href="seminar.php?page='.$total_pages.'" >Last</a></li>';
                      
                    }
                    
                    echo '</ul>';
                    
                    ?>
                    </div>

                    </div>
                  </div>
                    
                </div>
            </div>
            </div>
            </div>
            </div>
            </div>
            </div>

<?php include('footer/footer.php'); ?>

<script type="text/javascript">
  function sortTable(table, order) {
    var asc   = order === 'asc',
        tbody = table.find('tbody');
    
    tbody.find('tr').sort(function(a, b) {
        if (asc) {
            return $('td:first', a).text().localeCompare($('td:first', b).text());
        } else {
            return $('td:first', b).text().localeCompare($('td:first', a).text());
        }
    }).appendTo(tbody);
}
</script>

<script type="text/javascript">
    function GetSelected() {
        //Create an Array.
        var selected = new Array();
 
        //Reference the Table.
        var tblFruits = document.getElementById("tblFruits");
 
        //Reference all the CheckBoxes in Table.
        var chks = tblFruits.getElementsByTagName("INPUT");
 
        // Loop and push the checked CheckBox value in Array.
        for (var i = 0; i < chks.length; i++) {
            if (chks[i].checked) 
            {
                var s = selected.push(chks[i].value);

            }
        }
 
        //Display the selected CheckBox values.
        if (selected.length > 0) {
          if(confirm('Are you sure delete?'))
          {

              $.ajax({
               type: "POST",
               data: {delete_multi:selected},
               url: "../admin/query/delete.php",
               success: function(msg){
                 //alert(msg)
                 location.reload();
               }
            
              });
          }
        }else{
         
         alert("Please select student!")
        
        }
    };


</script>