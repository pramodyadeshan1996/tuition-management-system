<?php 
  session_start();
  include('../connect/connect.php');
  $class_id = $_POST['class_id'];
  $teacher_id = $_POST['teacher_id'];

  $year = $_POST['year'];
  $month = $_POST['month'];

  if($month<10)
  {
    $month = '0'.$month;
  }
  $pay_type = $_POST['pay_type'];


  $sql = mysqli_query($conn,"SELECT * FROM institute WHERE INS_ID = '1'");
  while($row = mysqli_fetch_assoc($sql))
  {
    $n = $row['INS_NAME'];
    $p = $row['PICTURE'];
    if($row['INS_MOBILE'] !== '0')
    {
      $mobile = $row['INS_MOBILE'];
    }else
    {
      $mobile = "";
    }

    if($row['INS_TP'] !== '0')
    {
      $tele = $row['INS_TP'];
    }else
    {
      $tele = "";
    }


    if(!empty($mobile) && !empty($tele))
    {
      $no = $mobile." / ".$tele;
    }

    $a = $row['INS_ADDRESS'];
  }header('Content-type: text/html; charset=UTF-8');


    $date = date('Y-m-d');
    $time = date('h:i:s A');

 ?>
<html>
<head>
<style type="text/css" media="print,screen">
.hideMe{
display:block;
}

.PrintClass {
display:block;

}
.NoPrintClass{
display:block;
}

</style>
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script type="text/javascript"
    src="http://jqueryjs.googlecode.com/files/jquery-1.3.1.min.js"> </script>
    <link href="https://fonts.googleapis.com/css?family=PT+Sans+Narrow&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<script type="text/javascript">

function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     document.body.innerHTML = originalContents;

     window.print();
    window.onafterprint = function() {
    };
     

}
function close_win() {
   window.close();
}
   </script>
</head>
  <style type="text/css">
  @media print {
  .print_btn {
    display: none;
  }
}
</style>
</head>


<body>
 <div class="col-md-12" id="printableArea" style="display:block;">

 <p style="text-align:center">
        <?php 

        if(!empty($mobile) && !empty($tele))
        {
          $no = $mobile."/".$tele;
        }else
        if(!empty($mobile) && empty($tele))
        {
          $no = $mobile;
        }else
        if(!empty($tele) && empty($mobile))
        {
          $no = $tele;
        }else
        {
          $no = '';
        }



        if($p == "0")
        {
          echo '<h3 style="text-align:center;text-transform:uppercase;">'.$n.'</h3><p style="text-align:center">'.$a.'</p><p style="text-align:center">'.$no.'</p><div style="border-top: 1px dashed black;padding:0px;"></div>';
        }else
        if($p !== "0")
        {
          echo '<h3 style="text-align:center;text-transform:uppercase;">'.$n.'</h3><p style="text-align:center">'.$a.'</p><p style="text-align:center">'.$no.'</p><div style="border-top: 1px dashed black;padding:0px;"></div>';
        }
        
        ?>
      </p>


        <h4 style="float: left;text-transform: uppercase;">Date & Time : <?php echo $date; ?> <?php echo $time; ?>
          <br>
          <br>

          <?php 

            if($class_id == 'all')
            {
              echo "Class Name : All Classes <br>";
              

            }else
            if($class_id !== 'all')
            {
              $sql001 = mysqli_query($conn,"SELECT * FROM `classes` WHERE `CLASS_ID` = '$class_id'");
              while($row001 = mysqli_fetch_assoc($sql001))
              {
                $clz_name = $row001['CLASS_NAME'];
                $clz_fees = $row001['FEES'];
              }

              ?>

                <?php echo "Class Name : ".$clz_name; ?> <br>
                <?php echo "Class Fees : LKR ".number_format($clz_fees,2); ?> <br>

              <?php
            }
            

           ?>
          
          <?php echo "Month : ".$year." - ".date('F', mktime(0, 0, 0, $month, 10)); // March ?> <br>
        </h4>
        <br>
        <br>
        <br>
        <br>
        <br>

        <?php 
          if($pay_type == '1') //Paid Students
          {
         ?>

        <div class="row">
          <div class="col-md-2"></div>
          <div class="col-md-5" style="text-align: center;"><center><h1 style="text-align: center;margin-bottom: 20px;"><u>PAID REPORT</u></h1></center></div>
          <div class="col-md-5"></div>
        </div>
   
        <div class="table-responsive">
            
            <table class="table" style="font-size: 14px;">
              <thead>
                <th>Paid Time</th>
                <th>Register ID</th>
                <th>Class Name</th>
                <th>Month</th>
                <th>Payment Method</th>
                <th style="text-align: right;">Fees(LKR)</th>
              </thead>
            <tbody>
              <?php 
                $total_fees = 0;
                $collect_student = 0;


                $sql001 = mysqli_query($conn,"SELECT * FROM `student_details` ORDER BY `STU_ID` ASC");
                while($row001 = mysqli_fetch_assoc($sql001))
                {
                  $stu_name = $row001['F_NAME']." ".$row001['L_NAME'];
                  $stu_tp = $row001['TP'];
                  $stu_id001 = $row001['STU_ID'];

                  $sql002 = mysqli_query($conn,"SELECT * FROM `stu_login` WHERE `STU_ID` = '$stu_id001'");
                  while($row002 = mysqli_fetch_assoc($sql002))
                  {
                    $reg_id = $row002['REGISTER_ID'];
                  }

                  if($class_id !== 'all')
                  {

                    $sql003 = mysqli_query($conn,"SELECT * FROM `transactions` WHERE `CLASS_ID` = '$class_id' AND `STU_ID` = '$stu_id001'");

                  }else
                  if($class_id == 'all')
                  {
                      $sql003 = mysqli_query($conn,"SELECT * FROM `transactions` WHERE `STU_ID` = '$stu_id001' AND `TEACH_ID` = '$teacher_id' ORDER BY `STU_ID` DESC");
                  }

                  $mon_name = date('F', mktime(0, 0, 0, $month, 10)); // March
                  
                  while($row003 = mysqli_fetch_assoc($sql003))
                  {
                    $trans_class_id = $row003['CLASS_ID'];
                    $trans_teach_id = $row003['TEACH_ID'];

                    $sql009 = mysqli_query($conn,"SELECT * FROM `classes` WHERE `CLASS_ID` = '$trans_class_id'");
                    while($row009 = mysqli_fetch_assoc($sql009))
                    {
                      $clz_fees = $row009['FEES'];
                      $class_name = $row009['CLASS_NAME'];
                    }

                    $sql004 = mysqli_query($conn,"SELECT * FROM `payment_data` WHERE `CLASS_ID` = '$trans_class_id' AND `STU_ID` = '$stu_id001' AND  `YEAR` = '$year' AND `MONTH` = '$month' AND `ADMIN_SUBMIT` = '1'");

                    $check_data = mysqli_num_rows($sql004); //check payment status

                    if($check_data > 0)
                    {

                      $row005 = mysqli_fetch_assoc($sql004);
                      $paid_time = $row005['PAY_TIME'];
                      $pay_method = $row005['PAY_METHOD'];

                      $collect_student = $collect_student+1;
                      $total_fees = $total_fees+$clz_fees;
                      
                      echo '
                                    <tr>
                                      <td>'.$paid_time.'</td>
                                      <td>'.$reg_id.'</td>
                                      <td>'.$class_name.'</td>
                                      <td>'.$year.' - '.$mon_name.'</td>
                                      <td>'.$pay_method.'</td>
                                      <td class="text-right">'.number_format($clz_fees,2).'</td>
                                    </tr>
                                  ';

                    }

                  }
                    
                }
                  
                

                //Final clz fees
                $final_clz_fees = 0; //Final Fees variable defined
                $presentage = 0.20; //Institute Presentage
                $final_clz_fees = $total_fees*$presentage; //Total Institute Fees
                $total_final_fees = $total_fees-$final_clz_fees; //Final Total Fees
                  

                 ?>
                 <tr style="font-size: 14px;">
                   <th>
                     No Of Students
                   </th>
                   <th colspan="4"></th>
                   <th colspan="1" style="text-align: right;"><?php echo $collect_student; ?></th>
                 </tr>
                 <tr style="font-size: 14px;">
                   <th>
                     Total Class Fees(LKR)
                   </th>
                   <th colspan="4"></th>
                   <th colspan="1" style="text-align: right;"><?php echo number_format($total_fees,2); ?></th>
                 </tr>


                <!--  <tr style="font-size: 14px;">
                   <th>
                    Institute Charge (<?php //echo $presentage*100; ?>%)
                   </th>
                   <th colspan="6"></th>
                   <th colspan="1" style="text-align: right;"><?php //echo number_format($final_clz_fees,2); ?></th>
                 </tr> 

                 <tr style="font-size: 14px;">
                   <th>
                      Final Class Fees 
                   </th>
                   <th colspan="4"></th>
                   <th colspan="1" style="text-align: right;"><?php //echo number_format($total_final_fees,2); ?></th>
                 </tr>-->

                </tbody>
              </table>
        </div>
      <?php //paid students 
      }else
      if($pay_type == '0') //Not Paid Students
      {?>


          <div class="row">
          <div class="col-md-2"></div>
          <div class="col-md-5" style="text-align: center;"><center><h1 style="text-align: center;margin-bottom: 20px;"><u>UNPAID REPORT</u></h1></center></div>
          <div class="col-md-5"></div>
        </div>
   
        <div class="table-responsive">
            
            <table class="table" style="font-size: 14px;">
              <thead>
                <th>Register ID</th>
                <th>Class Name</th>
                <th>Month</th>
                <th class="text-right" style="width: 10%;">Class Fees</th>

              </thead>
            <tbody>
              <?php 
                $total_fees = 0;
                $collect_student = 0;


                $sql001 = mysqli_query($conn,"SELECT * FROM `student_details` ORDER BY `STU_ID` ASC");
                while($row001 = mysqli_fetch_assoc($sql001))
                {
                  $stu_name = $row001['F_NAME']." ".$row001['L_NAME'];
                  $stu_tp = $row001['TP'];
                  $stu_id001 = $row001['STU_ID'];
                  $stu_gen = $row001['GENDER'];

                  $sql002 = mysqli_query($conn,"SELECT * FROM `stu_login` WHERE `STU_ID` = '$stu_id001'");
                  while($row002 = mysqli_fetch_assoc($sql002))
                  {
                    $reg_id = $row002['REGISTER_ID'];
                  }

                  if($class_id !== 'all')
                  {

                    $sql003 = mysqli_query($conn,"SELECT * FROM `transactions` WHERE `CLASS_ID` = '$class_id' AND `STU_ID` = '$stu_id001'");

                  }else
                  if($class_id == 'all')
                  {
                      $sql003 = mysqli_query($conn,"SELECT * FROM `transactions` WHERE `STU_ID` = '$stu_id001' AND `TEACH_ID` = '$teacher_id' ORDER BY `STU_ID` DESC");
                  }

                  $mon_name = date('F', mktime(0, 0, 0, $month, 10)); // March
                  
                  while($row003 = mysqli_fetch_assoc($sql003))
                  {
                    $trans_class_id = $row003['CLASS_ID'];
                    $trans_teach_id = $row003['TEACH_ID'];

                    $sql009 = mysqli_query($conn,"SELECT * FROM `classes` WHERE `CLASS_ID` = '$trans_class_id'");
                    while($row009 = mysqli_fetch_assoc($sql009))
                    {
                      $clz_fees = $row009['FEES'];
                      $class_name = $row009['CLASS_NAME'];
                    }

                    $sql004 = mysqli_query($conn,"SELECT * FROM `payment_data` WHERE `CLASS_ID` = '$trans_class_id' AND `STU_ID` = '$stu_id001' AND  `YEAR` = '$year' AND `MONTH` = '$month' AND `ADMIN_SUBMIT` = '1'");

                    $check_data = mysqli_num_rows($sql004); //check payment status

                    if($check_data == '0')
                    {
                      
                      $collect_student = $collect_student+1;
                      $total_fees = $total_fees+$clz_fees;
                      
                      echo '
                                    <tr>
                                      <td>'.$reg_id.'</td>
                                      <td>'.$class_name.'</td>
                                      <td>'.$year.' - '.$mon_name.'</td>
                                      <td class="text-right">'.number_format($clz_fees,2).'</td>
                                    </tr>
                                  ';

                    }

                  }
                    
                }
                  
                
                  

               ?>

               <tr style="font-size: 14px;">
                 <th>
                   No Of Students
                 </th>
                 <th colspan="1"></th>
                 <th colspan="2" style="text-align: right;"><?php echo $collect_student; ?></th>
               </tr>

               <tr style="font-size: 14px;">
                 <th>
                   Expected amount
                 </th>
                 <th colspan="1"></th>
                 <th colspan="2" style="text-align: right;">LKR <?php echo number_format($collect_student*$clz_fees,2); ?></th>
               </tr>

                </tbody>
              </table>
        </div>


     <?php  } //Not Paid Students ?>


</div>

<div class="col-md-12" style="position: fixed;top:10px;right: 10px;">

  <button class="btn btn-success btn-lg print_btn" onclick="printDiv('printableArea')" style="font-size: 20px;float: right;border-radius: 80px;padding: 10px 13px 10px 13px;margin-left: 10px;outline: none;"><span class="fa fa-print"></span></button>

  <button class="btn btn-danger btn-lg print_btn" onclick="close_win();" style="font-size: 20px;float: right;border-radius: 80px;padding: 10px 13px 10px 13px;outline: none;"><span class="fa fa-times"></span></button>


</div>
</body>

</html>


