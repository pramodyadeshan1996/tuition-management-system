

<?php
  $page = "Show teacher in login";
  $folder_in = '3';

      include('header/header.php');

        $s04 = mysqli_query($conn,"SELECT * FROM admin_login WHERE ADMIN_ID = '$admin_id'");
        while($row4 = mysqli_fetch_assoc($s04))
        {
          $name = $row4['ADMIN_NAME'];
        }

 ?>

    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4"></div>
        <div class="col-md-4"></div>
    </div>

    <ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-bottom: 04%;margin-bottom: 0;">
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="teacher_approved.php">Teacher Approved</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="Show teacher in login"> Show teacher in login</li>
    </ol>


      <div class="col-md-12 bg-white" style="border:1px solid #cccc;height: auto;margin-bottom: 10px;">
      
      <div class="col-md-12" style="border-bottom: 1px solid #cccc;">
        <div class="row">

            <div class="col-md-10" style="margin-bottom: 10px;"><h2><a href="teacher_approved.php" data-toggle="tooltip" data-title="Back"><i class="pg-icon" style="font-size: 22px;">chevron_left</i></a> Show teacher in login</h2></div>
          <div class="col-md-2">

          </div>
        </div>
      </div>

          
          <div class=" container-fluid container-fixed-lg bg-white" style="margin-top: 0px;">
            <div class="card card-transparent">
              <div class="card-header ">

                  <form action="show_teacher_login.php" method="POST">
                      <div class="row">
                        <div class="col-md-2">
                          <div class="col-md-12" style="padding: 10px 10px 10px 10px;background-color: #000000b8;border-radius: 10px;" data-toggle="tooltip" data-title="Today Student Joined Amount">
                            <center><span class="fa fa-user text-white text-center" style="font-size: 18px;"></span>
                              <small class="text-white" style="font-size: 20px;">
                                <?php 

                                  $sql004 = mysqli_query($conn,"SELECT * FROM `teacher_login` WHERE `STATUS` = 'Active' ");
                                  $total_student = mysqli_num_rows($sql004);

                                  if($total_student < 10)
                                  {
                                    $total_student = "0".$total_student;
                                  }else
                                  {
                                    $total_student = number_format($total_student);
                                  }

                                  echo $total_student;
                                 ?>
                              </small>
                              <br>
                              <small class="text-white text-center">Total Teachers</small>
                            </center>
                          </div>
                        </div>
                        <div class="col-md-5"> </div>
                      <div class="col-md-3 mt-2">
                        <input list="student_search" id="search-table" name="search_teacher" class="form-control" placeholder="Search Register ID.." >

                        <datalist id="student_search">
                          <?php 

                            $sql002 = mysqli_query($conn,"SELECT * FROM `teacher_details`");
                            while($row002 = mysqli_fetch_assoc($sql002))
                            {
                              $name = $row002['F_NAME']." ".$row002['L_NAME'];
                              $teach_id = $row002['TEACH_ID'];
                              $tp = $row002['TP'];

                              echo '<option value="'.$teach_id.'">'.$name.' | '.$tp.'</option>';
                              
                            }

                           ?>
                          
                        </datalist>
                      </div>
                      <div class="col-md-1 mt-2"> <button type="submit" class="btn btn-success btn-lg btn-block" name="search_teacher_btn" style="border-radius: 20px;"><i class="fa fa-search"></i> &nbsp; Search</button></div>

                      <div class="col-md-1 mt-2"> <button type="submit" class="btn btn-danger btn-lg btn-block" name="reset_student_btn" style="border-radius: 20px;"><i class="fa fa-refresh"></i> &nbsp; Reset</button></div>
                    </div>
                  </form>

                    <div class="clearfix"></div>
                </div>
                <form action="../admin/query/update.php" method="POST">
                <div class="card-body">
                <div class="table-responsive">
                <table class="table table-hover" id="tblFruits">
                  <thead>
                      <tr>
                        <th style="width: 1%;text-align: center;">Picture</th>
                        <th style="width: 1%;">Name</th>
                        <th style="width: 1%;">List Order</th>
                        <th style="width: 1%;">Level Name</th>
                      </tr>
                    </thead>
                    <tbody>

                      <?php 
                        

                        if(isset($_POST['search_teacher_btn']))
                        {
                          $teacher_id = $_POST['search_teacher'];

                          $sql001 = "SELECT * FROM `teacher_login` WHERE `STATUS` = 'Active' AND `TEACH_ID` LIKE '%".$teacher_id."%'  order by `TEACH_ID` DESC";


                        }else
                        {
                          $sql001 = "SELECT * FROM `teacher_login` WHERE `STATUS` = 'Active'  order by `TEACH_ID` DESC";

                        }

                        
                          $page_query = "SELECT * FROM `teacher_login` WHERE `STATUS` = 'Active'  ORDER BY `TEACH_ID` DESC";

                        /*------------------ Header Pagination --------------------------------*/

                        $j = 0;
                        
                          $today = date('Y-m-d h:i:s A');
                        //LIMIT = 10-1 x 10 = 90 , 10";
                        $result = mysqli_query($conn, $sql001);
                        $page_result = mysqli_query($conn, $page_query);

                        $check = mysqli_num_rows($result);

                        if($check>0){
                        while($row001 = mysqli_fetch_assoc($result))
                        {
                          $teach_id = $row001['TEACH_ID'];
                          $reg_date = $row001['REG_DATE'];

                          $confirm = $row001['CONFIRM'];
                          $username = $row001['USERNAME'];



                          $sql002 = mysqli_query($conn,"SELECT * FROM teacher_details WHERE TEACH_ID = '$teach_id'");
                          while($row002 = mysqli_fetch_assoc($sql002))
                          {
                            $name = $row002['POSITION'].". ".$row002['F_NAME']." ".$row002['L_NAME'];
                            $fnm = $row002['F_NAME'];
                            $lnm = $row002['L_NAME'];

                            $address = $row002['ADDRESS'];
                            $tp = $row002['TP'];
                            $dob = $row002['DOB'];
                            $email = $row002['EMAIL'];
                            $position = $row002['POSITION'];
                            $qualification = $row002['QUALIFICATION'];
                            $about_me = $row002['INTRODUCTION'];
                            $video_link = $row002['INTRO_VIDEO'];
                            $gender = $row002['GENDER'];
                            $picture = $row002['PICTURE'];
                            $teacher_title = $row002['CLASS_TITLE'];
                            $list_order = $row002['TEACHER_ORDER'];
                            $teacher_level_id = $row002['LEVEL_ID'];

                            if($picture == '0')
                             {
                                if($gender == 'Male')
                                {
                                  $picture = 'b_teacher.jpg';
                                }else
                                if($gender == 'Female')
                                {
                                  $picture = 'g_teacher.jpg';
                                }
                             }
                          }


                          if($confirm == '0')
                          {
                            $confirm_bg = 'danger';
                            $confirm_txt = 'Not-confirm';
                            $message_admin = '<span class="fa fa-star"></span> ගුරුවරයා Confirm කිරීම අනිවාර්යය වේ.නැතහොත් ගුරුවරයාගේ පන්ති පිළිබද විස්තර සිසුන්ට නොපෙන්වයි.';
                          }else
                           if($confirm == '1')
                          {
                            $confirm_bg = 'success';
                            $confirm_txt = 'Confirm';
                            $message_admin = '';
                          }



                            echo '

                        <tr>
                          <td class="v-align-middle"><center><img src="../teacher/images/profile/'.$picture.'" style="width:100px;height:100px;max-width:100px;max-height:100px;border-radius:80px;"> 

                          </center>

                            <center><span class="label label-'.$confirm_bg.'" style="bottom:1px;text-align:center;">'.$confirm_txt.'</span></center>

                            </td>
                          <td class="v-align-middle">'.$name.' <br> <small class="text-danger">('.$reg_date.')</small> <br>

                          <small style="bottom:1px;text-align:center;font-weight:bold;color:#3498db">'.$message_admin.'</small>

                          </td>
                          <td class="v-align-middle"><input type="number" class="form-control" name="list_order[]" value="'.$list_order.'"></td>
                          <td class="v-align-middle">

                          <input type="hidden" name="teacher_id[]" value="'.$teach_id.'">
                          
                          <select class="form-control" name="level_name[]" >
                          ';

                          if($teacher_level_id == '0')
                          {
                            echo '<option value="0">Select Level Name</option>';

                            $sql00299 = mysqli_query($conn,"SELECT * FROM `level`");
                            while($row00299 = mysqli_fetch_assoc($sql00299))
                            {
                              $level_name = $row00299['LEVEL_NAME'];
                              $level_id = $row00299['LEVEL_ID'];

                              echo '<option value="'.$level_id.'">'.$level_name.'</option>';
                              
                            }
                          }else
                          if($teacher_level_id !== '0')
                          {
                            
                            $sql002991 = mysqli_query($conn,"SELECT * FROM `level` WHERE `LEVEL_ID` = '$teacher_level_id'");
                            while($row002991 = mysqli_fetch_assoc($sql002991))
                            {
                              $level_name = $row002991['LEVEL_NAME'];
                              $level_id = $row002991['LEVEL_ID'];

                              echo '<option value="'.$level_id.'">'.$level_name.'</option>';
                              
                            }

                            $sql002992 = mysqli_query($conn,"SELECT * FROM `level` WHERE `LEVEL_ID` != '$teacher_level_id'");
                            while($row002992 = mysqli_fetch_assoc($sql002992))
                            {
                              $level_name = $row002992['LEVEL_NAME'];
                              $level_id = $row002992['LEVEL_ID'];

                              echo '<option value="'.$level_id.'">'.$level_name.'</option>';
                              
                            }
                            
                          }
                          
                          echo '

                          </select>
                          </td>

                          '; 
                           ?>
                        </tr>
                      <?php
                      
                          }
                        }else
                        if($check == '0')
                        {
                          echo '<tr><td colspan="7" class="text-center text-danger"><span class="fa fa-warning"></span> Not Found Data</td></tr>';
                        } 
                      

                        

                        
                         ?>


                    </tbody>
                </table>
              </div>

              
              <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-6"></div>
                <div class="col-md-2">
                  
                    <button type="submit" style="margin-top:20px;" name="show_teacher_list" class="btn btn-complete btn-lg btn-block" style="box-shadow:0px 0px 4px 3px #cccc;margin-top:6px;" data-title="Confirm Teacher" data-toggle="tooltip"><i class="pg-icon">edit</i> Update</button>
                </div>
              </div>

              

              </div>
                      </form>


                    </div>
                  </div>
                    
                </div>
            </div>

          </div>
        </div>

<?php include('footer/footer.php'); ?>
