
<?php

$page = "Teachers";
$folder_in = '0';

  include('header/header.php'); 
 ini_set( "display_errors", 0); 

$sub_id = $_GET['sub_id'];
$level_id = $_GET['level_id'];

$sql003 = mysqli_query($conn,"SELECT * FROM subject WHERE SUB_ID = '$sub_id'");
while($row003 = mysqli_fetch_assoc($sql003))
{
$sub_name = $row003['SUBJECT_NAME'];
}

$sql004 = mysqli_query($conn,"SELECT * FROM level WHERE LEVEL_ID = '$level_id'");
while($row004 = mysqli_fetch_assoc($sql004))
{
$level_name = $row004['LEVEL_NAME'];
}


  ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;border-bottom: 1px solid #cccc;margin-bottom: 04%;margin-bottom: 0;">
<li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
<li class="breadcrumb-item"><a href="level.php" data-toggle="tooltip" data-title="Classes"> Classes</a></li>
<li class="breadcrumb-item"><a href="subjects.php?level_id=<?php echo $level_id; ?>" data-toggle="tooltip" data-title="Subjects"> Subjects</a></li>
<li class="breadcrumb-item" data-toggle="tooltip" data-title="Teachers"> Teachers</li>
</ol>



<div class="col-md-12" style="margin-top: 0;">
  <h3 style="text-transform: capitalize;"><a href="subjects.php?level_id=<?php echo $level_id; ?>"><i class="pg-icon" style="font-size: 22px;" data-toggle="tooltip" data-title="To Back">chevron_left</i></a> <?php echo $sub_name; ?></h3>
<div class=" container-fluid   container-fixed-lg bg-white">
<div class="card card-transparent">
<div class="card-header ">
<div class="card-title">Here are all the teachers registered for the <?php echo $sub_name; ?> subject.
</div>
<div class="pull-right">
<div class="col-xs-12">
<input type="text" id="search-table" class="form-control pull-right" placeholder="Search" data-toggle="tooltip" data-title="අවශ්‍ය දත්ත සෙවීම'">
</div>
</div>
<div class="clearfix"></div>
</div>

<div class="card-body table-responsive" style="height: 800px;overflow: auto;margin-bottom: 0;">
<table class="table demo-table-search table-responsive-block text-left" id="tableWithSearch">
<thead>
<tr>
<th style="width: 5%;">Profile Picture</th>
<th style="width: 12%;">Teacher Name</th>
<th style="width: 5%;">Month</th>
<th>Classes</th>
<th style="text-align: center;">Action</th>
</tr>
</thead>
<tbody id="myTable">


<?php 
		  $current_year = date('Y');
	      $current_month = date('m');
	      $current_mon = date('F');

            $sql001 = mysqli_query($conn,"SELECT * FROM teacher_subject WHERE SUB_ID = '$sub_id'");

            $check = mysqli_num_rows($sql001);

            if($check>0)
            {

            while($row001 = mysqli_fetch_assoc($sql001))
            {
                $teach_id = $row001['TEACH_ID'];
                $ts_id = $row001['T_S_ID'];

                $sql002 = mysqli_query($conn,"SELECT * FROM teacher_details WHERE TEACH_ID = '$teach_id'");

                while($row002 = mysqli_fetch_assoc($sql002))
                {


                                $name = $row002['F_NAME']." ".$row002['L_NAME'];
                                $picture = $row002['PICTURE'];
                                $position = $row002['POSITION'];
                                $gender = $row002['GENDER'];

                                if($picture == '0')
                                {

                                  if($gender == 'Male')
                                  {
                                      
                                      $s = '<img src="../teacher/images/profile/male_teacher1.jpg" alt="" data-src="../teacher/images/profile/male_teacher1.jpg" data-src-retina="../teacher/images/profile/male_teacher1.jpg" width="100" height="100" style="border-radius: 80px;">';


                                  }else
                                  if($gender == 'Female')
                                  {

                                      $s = '<img src="../teacher/images/profile/female_teacher.png" alt="" data-src="../teacher/images/profile/female_teacher.png" data-src-retina="../teacher/images/profile/female_teacher.png" width="100" height="100" style="border-radius: 80px;">';

                                  }

                                  
                                  
                                }else
                                {
                                  $s = '<img src="../teacher/images/'.$picture.'" alt="" data-src="../teacher/images/'.$picture.'" data-src-retina="../teacher/images/'.$picture.'" width="100" height="100" style="border-radius: 80px;">';
	

                                }

                                echo '<tr>
                                		<td class="v-align-middle semi-bold"><center>'.$s.'</center></td>
                                		<td class="v-align-middle"><p>'.$position.'. '.$name.'</p></td>
										<td class="v-align-middle">'.$current_year.'-'.$current_mon.'</td>';
								
								echo '<td class="v-align-middle" colspan="4" style="width: 100%;">';

								$sql003 = mysqli_query($conn,"SELECT * FROM teacher_subject WHERE TEACH_ID = '$teach_id' AND SUB_ID = '$sub_id'");

				                while($row003 = mysqli_fetch_assoc($sql003))
				                {

				                	$sub_id = $row003['SUB_ID'];

									

				                	$sql004 = mysqli_query($conn,"SELECT * FROM classes WHERE TEACH_ID = '$teach_id' AND SUB_ID = '$sub_id'");

					                while($row004 = mysqli_fetch_assoc($sql004))
					                {
					                	
                                            	$class_id = $row004['CLASS_ID'];
                                            	$cnm = $row004['CLASS_NAME'];
                                  				
                                  				$fees = $row004['FEES'];
                                  				$tr_status = 0;
                                  				$sql0042 = mysqli_query($conn,"SELECT * FROM transactions WHERE STU_ID = '$stu_id' AND CLASS_ID = '$class_id'");
		                                          $available = mysqli_num_rows($sql0042); //check Registered Subjects

		                                          while($row0042 = mysqli_fetch_assoc($sql0042))
		                                          {
		                                            $tr_id = $row0042['TRA_ID'];
		                                             $sql0052 = mysqli_query($conn,"SELECT * FROM payment_data WHERE TRA_ID = '$tr_id' AND CLASS_ID = '$class_id' AND YEAR = '$current_year' AND MONTH = '$current_month'");
		                                            $count = mysqli_num_rows($sql0052);
		                                            while($row0052 = mysqli_fetch_assoc($sql0052))
		                                            {
		                                              $tr_fees = $row0052['FEES'];
		                                              $pay_id = $row0052['PAY_ID'];
		                                              $tr_year = $row0052['YEAR'];
		                                              $tr_month = $row0052['MONTH'];
		                                              $tr_status = $row0052['UPLOAD_STATUS'];
		                                              $tr_ad_sub = $row0052['ADMIN_SUBMIT'];
		                                              $tr_file = $row0052['FILE_NAME'];
		                                            }
		                                          }

		                                          copy('choose_category.php','teachers/'.$teach_id.'/'.$class_id.'/choose_category.php');

                                          /*copy('uploaded_tutorials.php','teachers/'.$teach_id.'/'.$class_id.'/uploaded_tutorials.php');*/

                                          copy('images/zo_logo.png','teachers/'.$teach_id.'/'.$class_id.'/image/zo_logo.png');

                                          copy('images/girl.png','teachers/'.$teach_id.'/'.$class_id.'/image/girl.png');
                                          copy('images/boy.png','teachers/'.$teach_id.'/'.$class_id.'/image/boy.png');

                                          copy('video.php','teachers/'.$teach_id.'/'.$class_id.'/video/video.php');

                                          copy('audio.php','teachers/'.$teach_id.'/'.$class_id.'/audio/audio.php');

                                          copy('document.php','teachers/'.$teach_id.'/'.$class_id.'/document/document.php');


                                          if (!file_exists('teachers/'.$teach_id.'/'.$class_id.'/video'.'')) {
                                              mkdir('teachers/'.$teach_id.'/'.$class_id.'/video', 0777, true);
                                          }

                                          if (!file_exists('teachers/'.$teach_id.'/'.$class_id.'/image'.'')) {
                                              mkdir('teachers/'.$teach_id.'/'.$class_id.'/image', 0777, true);
                                          }

                                          if (!file_exists('teachers/'.$teach_id.'/'.$class_id.'/document'.'')) {
                                              mkdir('teachers/'.$teach_id.'/'.$class_id.'/document', 0777, true);
                                          }

                                          if (!file_exists('teachers/'.$teach_id.'/'.$class_id.'/audio'.'')) {
                                              mkdir('teachers/'.$teach_id.'/'.$class_id.'/audio', 0777, true);
                                          }


                                          if (!file_exists('teachers/'.$teach_id.'/'.$class_id.'/audio/audio'.'')) {
                                              mkdir('teachers/'.$teach_id.'/'.$class_id.'/audio/audio', 0777, true);
                                          }

                                          if (!file_exists('teachers/'.$teach_id.'/'.$class_id.'/video/video'.'')) {
                                              mkdir('teachers/'.$teach_id.'/'.$class_id.'/video/video', 0777, true);
                                          }

                                          if (!file_exists('teachers/'.$teach_id.'/'.$class_id.'/document/document'.'')) {
                                              mkdir('teachers/'.$teach_id.'/'.$class_id.'/document/document', 0777, true);
                                          }


                                            $myfile = fopen('teachers/'.$teach_id.'/'.$class_id.'/'.'class_id.txt', "w") or die("Unable to open file!");
                                            $txt = $class_id;
                                            fwrite($myfile, $txt);
                                            fclose($myfile);




                                                echo '<div class="row" style="margin-top: 10px;">
														<div class="col-md-5">
															<p class="bold">'.$cnm.'</p>
														</div>
													';
                          $free = 'Yes';
                          if($free !== 'Yes')
                          {
													                         if($available > 0) //check Registered Subjects (Registered)
	                                                {

	                                                	if($count>0)
	                                                	{

	                                                		if($tr_status == '1' && $tr_ad_sub == '0' && $current_year == $tr_year && $current_month == $tr_month)
                                                  			{ //create payment and upload payment reciption pending until submit by admin

                                                      /*echo '<div class="col-md-7" style="text-align:left;"><a href="#" class="btn btn-danger disabled btn-rounded btn-xs" style="margin-top:6px;"><span class="fa fa-lightbulb-o"></span>&nbsp; Register</a>';*/
                                                    
                                                      ?>
                                                      <a href="../student/query/delete.php?tr_id=<?php echo $tr_id; ?>&&level_id=<?php echo $level_id; ?>&&sub_id=<?php echo $sub_id; ?>&&un_reg=un_reg" class="btn btn-danger btn-rounded btn-xs btn-active" onclick="return confirm('Are you sure Unregister?')" style="margin-top:6px;" data-toggle="tooltip" data-title="ඔබ දැනටමත් ලියාපදිංචි පන්තිය අවලංගු කිරීම."><span class="fa fa-times"></span>&nbsp; Unregister</a>&nbsp;

                                                      <?php

                                                      echo '
                                                      <a href="teachers/'.$teach_id.'/'.$class_id.'/'.'choose_category.php" aria-label="" class="btn btn-success btn-rounded disabled btn-xs" style="cursor:not-allowed;margin-top:6px;" data-toggle="tooltip" data-title="සුභ පැතුම්! ඔබ නියමිත පන්ති ගාස්තු ගෙවා ඇති බැවින් ඔබට ප්‍රවේශ වීමට අවසර ඇත."><span class="fa fa-check-circle"></span>&nbsp; Participate</a>&nbsp;
                                                      ';

                                                      echo '<button type="button" class="btn btn-info btn-rounded btn-xs" style="cursor:auto;color:white;margin-top:6px;" data-toggle="tooltip" data-title="ඔබ ඇතුලත් කල දත්ත පරික්ෂා කිරීමට යොමු කොට ඇත.හැකි ඉක්මනින් අප නියෝජිතයකු ඔබ වෙත ප්‍රතිචාර දක්වයි.එතෙක් රැදී සිටින්න."> <blink><span class="fa fa-bullhorn" style="margin-right:2px;"></span> Pending</blink></button></div>';


                                                     //create payment and upload payment reciption pending until submit by admin 
                                                  }else
                                                  if($tr_status == '1' && $tr_ad_sub == '1' && $current_year == $tr_year && $current_month == $tr_month)
                                                  { 
                                                    //Approve added reciption, payment button show now as with paid message
                                                    
                                                      /*echo '<div class="col-md-7" style="text-align:left;"><a href="#" class="btn btn-danger disabled btn-rounded btn-xs" style="margin-top:6px;cursor:not-allowed;"><span class="fa fa-lightbulb-o"></span>&nbsp; Register</a>';*/
                                                    
                                                      ?>
                                                      <a href="../student/query/delete.php?tr_id=<?php echo $tr_id; ?>&&level_id=<?php echo $level_id; ?>&&sub_id=<?php echo $sub_id; ?>&&un_reg=un_reg" class="btn btn-danger btn-rounded btn-xs btn-active" onclick="return confirm('Are you sure Unregister?')"  style="margin-top:6px;" data-toggle="tooltip" data-title="ඔබ දැනටමත් ලියාපදිංචි පන්තිය අවලංගු කිරීම."><span class="fa fa-times"></span>&nbsp;Unregister</a>&nbsp;

                                                      <?php

                                                      echo '
                                                      <a href="teachers/'.$teach_id.'/'.$class_id.'/'.'choose_category.php" aria-label="" class="btn btn-success btn-rounded btn-xs" style="margin-top:6px;" data-toggle="tooltip" data-title="සුභ පැතුම්! ඔබ නියමිත පන්ති ගාස්තු ගෙවා ඇති බැවින් ඔබට ප්‍රවේශ වීමට අවසර ඇත."><span class="fa fa-check-circle"></span>&nbsp; Participate</a>&nbsp;
                                                      ';
                                                    
                                                      echo '<button type="button" class="btn btn-complete btn-rounded disabled btn-xs" aria-label=""   data-toggle="modal" data-target="#add_payment'.$class_id.'" disabled style="margin-top:6px;cursor:not-allowed;"> <span class="fa fa-check-circle"></span>&nbsp; Paid</button></div>';


                                                    //Approve added reciption, payment button show now as with paid message  

                                                  }else
                                                  if($tr_status == '1' && $tr_ad_sub == '2' && $current_year == $tr_year && $current_month == $tr_month)
                                                  {
                                                    //Disapprove payment recieption

                                                     /*echo '<div class="col-md-7" style="border:0px solid black;"><a href="#" class="btn btn-danger disabled btn-rounded btn-xs" style="margin-top:6px;cursor:not-allowed;"><span class="fa fa-lightbulb-o"></span>&nbsp; Register</a>';*/
                                                    	?>
                                                      <a href="../student/query/delete.php?tr_id=<?php echo $tr_id; ?>&&level_id=<?php echo $level_id; ?>&&sub_id=<?php echo $sub_id; ?>&&un_reg=un_reg" class="btn btn-danger btn-rounded btn-xs  btn-active" onclick="return confirm('Are you sure Unregister?')" style="margin-top:6px;" data-toggle="tooltip" data-title="ඔබ දැනටමත් ලියාපදිංචි පන්තිය අවලංගු කිරීම."><span class="fa fa-times"></span>&nbsp;Unregister</a>

                                                      <?php

                                                            echo '&nbsp;
                                                            <a href="teachers/'.$teach_id.'/'.$class_id.'/'.'choose_category.php" aria-label="" class="btn btn-success btn-rounded btn-xs disabled" style="margin-top:6px;cursor:not-allowed;" data-toggle="tooltip" data-title="සුභ පැතුම්! ඔබ නියමිත පන්ති ගාස්තු ගෙවා ඇති බැවින් ඔබට ප්‍රවේශ වීමට අවසර ඇත."><span class="fa fa-check-circle"></span>&nbsp; Participate</a>';

                                                            echo '&nbsp;<a data-toggle="tooltip" data-title="ඔබ ඇතුලත් කල රිසිට්පතෙහි නිවැරදිබව හෝ අපැහැදිලිබවක් ඇති බැවින් නැවතත් පරික්ෂා කොට අප වෙත යොමු කරන්න."><button type="button" class="btn btn-default btn-xs btn-rounded"  aria-label="" data-toggle="modal" data-target="#again'.$class_id.'" style="margin-top:6px;outline:none;"> <span class="fa fa-frown-o"></span>&nbsp; Again</button></a></div>';

                                                     /* echo '<div class="col-md-7">
                                                      		<a href="#" aria-label="" class="btn btn-danger btn-rounded btn-xs disabled" style="margin-top:6px;"><span class="fa fa-lightbulb-o"></span>&nbsp; Register</a>';
                                                    

                                                      echo '&nbsp;<a href="teachers/'.$teach_id.'/'.$class_id.'/'.'choose_category.php" aria-label="" class="btn btn-success btn-rounded btn-xs disabled" style="margin-top:6px;"><span class="fa fa-check-circle"></span>&nbsp; Participate</a>
                                                      ';

                                                      echo '<button  aria-label="" class="btn btn-default btn-rounded btn-xs" style="margin-top:6px;" data-toggle="modal" data-target="#again'.$class_id.'" title="ඔබ ඇතුලත් කළ දත්තවල හෝ ඔබ උඩුගත කළ රිසිට්පතේ පැහැදිලි බවෙහි හෝ නිවැරදිභාවයේ යම් දෝෂයක් පවතී.නැවත ඇතුලත් කරන්න."> <span class="fa fa-undo"></span> Again</button>
                                                      <div>
                                                      ';*/

                                                    //Please Delete payment data table this have data
                                                    //Disapprove payment recieption
                                                  }
	                                             }else
	                                             if($count == '0')
                                                  {
                                                      
                                                      /*echo '<div class="col-md-7" style="border:0px solid black;"><a href="#" class="btn btn-danger disabled btn-rounded btn-xs" style="margin-top:6px;cursor:not-allowed;"><span class="fa fa-lightbulb-o"></span>&nbsp; Register</a>';*/

                                                      ?>
                                                      <a href="../student/query/delete.php?tr_id=<?php echo $tr_id; ?>&&level_id=<?php echo $level_id; ?>&&sub_id=<?php echo $sub_id; ?>&&un_reg=un_reg" class="btn btn-danger btn-rounded btn-xs" onclick="return confirm('Are you sure Unregister?')" style="margin-top:6px;height:26px;" data-toggle="tooltip" data-title="ඔබ දැනටමත් ලියාපදිංචි පන්තිය අවලංගු කිරීම."><span class="fa fa-times"></span>&nbsp;Unregister</a>&nbsp;

                                                      <?php
                                                    

                                                            echo '
                                                            <a href="teachers/'.$teach_id.'/'.$class_id.'/'.'choose_category.php" aria-label="" class="btn btn-success btn-rounded btn-xs disabled"  data-toggle="tooltip" data-title="සුභ පැතුම්! ඔබ නියමිත පන්ති ගාස්තු ගෙවා ඇති බැවින් ඔබට ප්‍රවේශ වීමට අවසර ඇත." style="margin-top:6px;cursor:not-allowed;height:26px;"><span class="fa fa-check-circle"></span>&nbsp; Participate</a>';

                                                            echo '&nbsp;<a data-toggle="tooltip" data-title="අදාල අය කිරීම් ගෙවීම."><button type="button" class="btn btn-primary btn-xs btn-rounded"  aria-label="" data-toggle="modal" data-target="#add_payment'.$class_id.'" style="margin-top:6px;height:26px;"> <span class="fa fa-money"></span>&nbsp; Pay</button></a></div>';

                                                            
                                                  }


                                                  	?>

                                                  <div id="add_payment<?php echo $class_id; ?>" class="modal fade" role="dialog">
                                                <div class="modal-dialog">
                                                    
                                                  <!-- Modal content-->
                                                  <div class="modal-content" style="padding: 15px 10px 15px 10px;">

                                                   <div class="modal-header">
                                                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                      <h4 class="modal-title" style="margin-bottom: 20px;"><?php echo $cnm; ?></h4>
                                                    </div>

                                                    <div class="col-md-12">
                                                      <div class="row">
                                                        <div class="col-md-6" style="padding: 10px 10px 10px 10px;text-align: center;cursor: pointer;" data-toggle="modal" data-target="#bank_deposit<?php echo $class_id; ?>"  data-dismiss="modal">

                                                          <div class="col-md-12" style="border:1px solid #cccc;" data-toggle="tooltip" data-title="බැංකුව හරහා මුදල් තැන්පත් කර සිදුකරන ගෙවීම් මෙහිදී සිදුකල හැක.">
                                                            
                                                            <img src="images/pay.png" class="image-responsive" style="width:100%;">
                                                            <h5 style="text-align: center;font-weight: bold;color: gray;">Bank Deposit</h5>
                                                          
                                                          </div>
                                                          
                                                        </div>
                                                        <div class="col-md-6" style="padding: 10px 10px 10px 10px;text-align: center;cursor: pointer;" data-toggle="modal" data-target="#online_payment<?php echo $class_id; ?>"  data-dismiss="modal">

                                                          <div class="col-md-12" style="border:1px solid #cccc;" data-toggle="tooltip" data-title="අන්තර්ජාලය හරහා මුදල් ගෙවීම සිදුකල හැකිය.">
                                                            
                                                            <img src="images/online_pay.png" class="image-responsive" style="width:100%;">
                                                            <h5 style="text-align: center;font-weight: bold;color: gray;">Online Payment</h5>
                                                          
                                                          </div>
                                                          
                                                        </div>
                                                      </div>
                                                    </div>
                                                  
                                                  </div> 

                                                </div>
                                              </div>


                                              <div id="bank_deposit<?php echo $class_id; ?>" class="modal fade" role="dialog">
                                                <div class="modal-dialog">

                                                  <!-- Modal content-->
                                                  <div class="modal-content" style="padding: 15px 10px 15px 10px;">

                                                     <form action="../student/query/insert.php" method="POST" enctype="multipart/form-data" enctype="multipart/form-data">
                                                    <div class="modal-header">
                                                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                      <h4 class="modal-title" style="margin-bottom: 20px;"><?php echo $cnm; ?></h4>
                                                    </div>
                                                    
                                                    <div class="modal-body">
                                                      <lable style="font-size: 16px;font-weight: bold;">Select Month : </lable>
                                                      <select class="form-control" style="margin-top:10px;margin-bottom:10px;" name="month" required>
                                                        <?php  
                                                        $m=date('m');
                                                        $month = date('F', mktime(0,0,0,$m));

                                                        echo '<option value='.$m.'>'.$month.'</option>';

                                                        /*$cu_month = date('m');
                                                        for ($m=1; $m<=$cu_month; $m++) 
                                                        {
                                                           $month = date('F', mktime(0,0,0,$m));
                                                           echo '<option value='.$m.'>'.$month.'</option>';
                                                         }*/
                                                        ?>

                                                      </select>

                                                      <lable style="font-size: 16px;font-weight: bold;">Month Fees : </lable>
                                                      <input type="number" class="form-control" value="<?php echo $fees; ?>" onfocus="this.value=''" style="margin-top:10px;margin-bottom:10px;" required disabled>
                                                      <input type="hidden" name="sub_id" value="<?php echo $sub_id; ?>">
                                                      <input type="hidden" name="level_id" value="<?php echo $level_id; ?>">
                                                      <input type="hidden" name="class_id" value="<?php echo $class_id; ?>">
                                                      <input type="hidden" name="fees" value="<?php echo $fees; ?>">
                                                      <input type="hidden" name="stu_id" value="<?php echo $stu_id; ?>">

                                                      <lable style="font-size: 16px;font-weight: bold;">Upload Payment Receipt : </lable>
                                                      <input type="file" class="form-control" name="upload_file" required  style="margin-top:10px;margin-bottom:10px;" title="Upload Here" accept="image/*">
                                                      <small class="text-danger"><span class="fa fa-asterisk"></span> ඔබ බැංකුව වෙත මුදල් ගෙවු පසු බැංකුව විසින් මුදල් භාරගත් බවට දෙන රිසිට් පතෙහි පින්තූරයක් ඉහත බොත්තම ක්ලික් කර තෝරා අවසානයේ පහත 'Submit' බොත්තම ක්ලික් කරන්න.</small>
                                                      
                                                    </div>

                                                    <div class="col-md-12"><a data-toggle="modal" data-target="#add_payment<?php echo $class_id; ?>" style="float: left;cursor: pointer;" data-dismiss="modal">Back</a></div>
                                                    
                                                      <div class="modal-footer">


                                                      <button type="submit" class="btn btn-success" name="create_payment" value="<?php echo $tr_id; ?>"><span class="fa fa-check-circle"></span>&nbsp;Submit</button>

                                                      <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-times-circle"></span>&nbsp;Close</button>

                                                    </div>
                                                   </form>
                                                  
                                                  </div> 

                                                </div>
                                              </div>


                                            <div id="online_payment<?php echo $class_id; ?>" class="modal fade" role="dialog">
                                                <div class="modal-dialog">

                                                  <!-- Modal content-->
                                                  <div class="modal-content" style="padding: 15px 10px 15px 10px;">

                                                    <div class="col-md-12">
                                                    <h2>Payment Gateway</h2>
                                                    </div>
                                                    <div class="col-md-12"><a data-toggle="modal" data-target="#add_payment<?php echo $class_id; ?>" style="float: left;cursor: pointer;" data-dismiss="modal">Back</a></div>
                                                    
                                                      <div class="modal-footer">

<!-- 
                                                      <button type="submit" class="btn btn-success" name="create_payment" value="<?php echo $tra_id; ?>"><span class="fa fa-check-circle"></span>&nbsp;Submit</button> -->

                                                      <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-times-circle"></span>&nbsp;Close</button>

                                                    </div>
                                                  </div> 

                                                </div>
                                              </div>


                                              <div id="again<?php echo $class_id; ?>" class="modal fade" role="dialog">
                                                <div class="modal-dialog">

                                                  <!-- Modal content-->
                                                  <div class="modal-content">
                                                  <form action="../student/query/update.php" method="POST" enctype="multipart/form-data" enctype="multipart/form-data">
                                                    <div class="modal-header">
                                                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                      <h4 class="modal-title" style="margin-bottom: 20px;"><?php echo $row004['CLASS_NAME']; ?></h4>
                                                    </div>
                                                    
                                                    <div class="modal-body">
                                                      <lable style="font-size: 16px;font-weight: bold;">Select Month : </lable>
                                                      <select class="form-control" style="margin-top:10px;margin-bottom:10px;" name="month" required>
                                                        <?php  
                                                        $m=date('m');
                                                        $month = date('F', mktime(0,0,0,$m));

                                                        echo '<option value='.$m.'>'.$month.'</option>';

                                                        /*$cu_month = date('m');
                                                        for ($m=1; $m<=$cu_month; $m++) 
                                                        {
                                                           $month = date('F', mktime(0,0,0,$m));
                                                           echo '<option value='.$m.'>'.$month.'</option>';
                                                         }*/
                                                        ?>

                                                      </select>

                                                      <lable style="font-size: 16px;font-weight: bold;">Month Fees : </lable>
                                                      <input type="number" class="form-control" value="<?php echo $fees; ?>" style="margin-top:10px;margin-bottom:10px;" required  onfocus="this.value=''" disabled>
                                                      <input type="hidden" name="sub_id" value="<?php echo $sub_id; ?>">
                                                      <input type="hidden" name="level_id" value="<?php echo $level_id; ?>">
                                                      <input type="hidden" name="class_id" value="<?php echo $class_id; ?>">
                                                      <input type="hidden" name="recent_file" value="<?php echo $tr_file; ?>">
                                                      <input type="hidden" name="fees" value="<?php echo $fees; ?>">

                                                      <lable style="font-size: 16px;font-weight: bold;">Upload Payment Receipt : </lable>
                                                      <input type="file" class="form-control" name="upload_file" required id="up_rec" style="margin-top:10px;margin-bottom:10px;" title="Upload Here" accept="image/*">

                                                       <div class="row show_image">
                                                        <div class="col-md-4"></div>
                                                        <div class="col-md-4"><img src="../admin/images/reciption/<?php echo $tr_file; ?>" style="width:100%;" alt="Receiption Image" class="image-responsive"></div>
                                                        <div class="col-md-4"></div>
                                                      </div>
                                                      <div class="col-md-12 text-success show_image" style="text-align: center;margin-top: 20px;"><label style="text-align: center;"><span class="fa fa-upload"></span> Recent Uploaded Image</label></div>
                                                      
                                                    </div>
                                                      <div class="modal-footer">
                                                      <button type="submit" class="btn btn-success" name="update_payment" value="<?php echo $tr_id; ?>"><span class="fa fa-check-circle"></span>&nbsp; Submit</button>

                                                      <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-times-circle"></span>&nbsp; Close</button>

                                                    </div>
                                                   </form>
                                                  </div> 

                                                </div>
                                              </div>
												<?php

	                                                     //create payment and upload payment reciption pending until submit by admin 
	                                                  
	                                                }else
	                                                if($available == '0') //check Registered Subjects (Un-Registered)
	                                                {
	                                                  ?>
	                                                   <div class="col-md-6" style="text-align:left;"><a href="../student/query/insert.php?class_id=<?php echo $class_id; ?>&&teach_id=<?php echo $teach_id; ?>&&level_id=<?php echo $level_id; ?>&&sub_id=<?php echo $sub_id; ?>&&stu_id=<?php echo $stu_id; ?>&&class_reg=class_reg" onclick="return confirm('Are you sure register?')"  class="btn btn-danger btn-rounded btn-xs" style="margin-top:6px;text-align: left;height:26px;" data-toggle="tooltip" data-title="ඔබට පන්ති වෙත ඇතුලත් වීම සදහා පලමුව අදාල ගාස්තු ගෙවා ලියාපදිංචි විය යුතුය."><span class="fa fa-lightbulb-o"></span>&nbsp; Register</a>

	                                                    <?php echo '
	                                                    
	                                                    <a href="teachers/'.$teach_id.'/'.$class_id.'/'.'choose_category.php" aria-label="" class="btn btn-success btn-rounded disabled btn-xs" data-toggle="tooltip" data-title="සුභ පැතුම්! ඔබ නියමිත පන්ති ගාස්තු ගෙවා ඇති බැවින් ඔබට ප්‍රවේශ වීමට අවසර ඇත." style="margin-top:6px;height:26px;"><span class="fa fa-check-circle"></span>&nbsp; Participate</a></div>';
	                                                }
	                                                echo "</div>";

                                			}else
                                      if($free == 'Yes') {

                                        if($available>0)
                                        {
                                        ?>  


                                                      <a href="../student/query/delete.php?tr_id=<?php echo $tr_id; ?>&&level_id=<?php echo $level_id; ?>&&sub_id=<?php echo $sub_id; ?>&&un_reg=un_reg" class="btn btn-danger btn-rounded btn-xs btn-active disabled" onclick="return confirm('Are you sure Register?')"  style="margin-top:6px;height:26px;" data-toggle="tooltip" data-title="ඔබට පන්ති වෙත ඇතුලත් වීම සදහා පලමුව අදාල ගාස්තු ගෙවා ලියාපදිංචි විය යුතුය."><span class="fa fa-times"></span>&nbsp;Register</a>&nbsp;

                                                      <?php

                                                      echo '
                                                      <a href="teachers/'.$teach_id.'/'.$class_id.'/'.'choose_category.php" aria-label="" class="btn btn-success btn-rounded btn-xs" style="margin-top:6px;" data-toggle="tooltip" data-title="සුභ පැතුම්! ඔබ නියමිත පන්ති ගාස්තු ගෙවා ඇති බැවින් ඔබට ප්‍රවේශ වීමට අවසර ඇත."><span class="fa fa-check-circle"></span>&nbsp; Participate</a>&nbsp;
                                                      ';
                                                    
                                                      echo '<button type="button" class="btn btn-complete btn-rounded disabled btn-xs" aria-label=""   data-toggle="modal" data-target="#add_payment'.$class_id.'" disabled style="margin-top:6px;cursor:not-allowed;display:none;"> <span class="fa fa-check-circle"></span>&nbsp; Paid</button></div>';

                                      }else
                                      if($available == '0')
                                      { ?>
                                        <a href="../student/query/insert.php?class_id=<?php echo $class_id; ?>&&teach_id=<?php echo $teach_id; ?>&&level_id=<?php echo $level_id; ?>&&sub_id=<?php echo $sub_id; ?>&&stu_id=<?php echo $stu_id; ?>&&class_reg=class_reg" class="btn btn-danger btn-rounded btn-xs btn-active" onclick="return confirm('Are you sure Register?')"  style="margin-top:6px;height:26px;" data-toggle="tooltip" data-title="ඔබට පන්ති වෙත ඇතුලත් වීම සදහා පලමුව අදාල ගාස්තු ගෙවා ලියාපදිංචි විය යුතුය."><span class="fa fa-times"></span>&nbsp;Register</a>&nbsp;

                                                      <?php

                                                      echo '
                                                      <a href="teachers/'.$teach_id.'/'.$class_id.'/'.'choose_category.php" aria-label="" class="btn btn-success btn-rounded btn-xs disabled" style="margin-top:6px;height:26px;" data-toggle="tooltip" data-title="සුභ පැතුම්! ඔබ නියමිත පන්ති ගාස්තු ගෙවා ඇති බැවින් ඔබට ප්‍රවේශ වීමට අවසර ඇත."><span class="fa fa-check-circle"></span>&nbsp; Participate</a>&nbsp;
                                                      ';
                                                    
                                                      echo '<button type="button" class="btn btn-complete btn-rounded disabled btn-xs" aria-label=""   data-toggle="modal" data-target="#add_payment'.$class_id.'" disabled style="margin-top:6px;cursor:not-allowed;display:none;height:26px;"> <span class="fa fa-check-circle"></span>&nbsp; Paid</button></div>';

                                      }

                                      }
                                    }
                                		
                                       

                                           
                                        }echo "</td>";echo '</tr>';

                                    }
                                }

                        
                }else
                if($check == '0')
                {
                	echo '<tr><td colspan="6" class="text-danger text-center"><span class="fa fa-warning text-danger"></span> Empty Data!</td></tr>';
                }
        
         ?> 






<!-- <tr>
<td class="v-align-middle semi-bold">
		<img src="assets/img/profiles/avatar.jpg" alt="" data-src="assets/img/profiles/avatar.jpg" data-src-retina="assets/img/profiles/avatar_small2x.jpg" width="100" height="100" style="border-radius: 80px;">
	
</td>

<td class="v-align-middle">
<p>Kumara Peris</p>
</td>

<td class="v-align-middle">
<p>2020-08-04</p>
</td>

<td class="v-align-middle" colspan="3" style="width: 100%;">


<div class="row" style="margin-top: 10px;">
		<div class="col-md-4">
			<p class="bold">2020 Revision therory</p>
		</div>
		<div class="col-md-4"><button aria-label="" class="btn btn-danger btn-rounded btn-block"><span class="pg-icon">bulb</span> Register</button></div>
		<div class="col-md-4"><button aria-label="" class="btn btn-success btn-rounded btn-block"><span class="pg-icon">tick_circle</span> Participate</button></div>
	</div>



</td>



</tr> -->

</tbody>
</table>
</div>
</div>


</div>
</div>

</div>

</div>

<script type="text/javascript">
	setInterval(function() {
    $('blink').fadeIn(1300).fadeOut(1500);
}, 1000);
</script>

<script>
$(document).ready(function(){
  $("#search-table").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

<script>
$(document).ready(function(){

  $("#up_rec").on("change", function() {
      $('.show_image').hide();
  });
});
</script>

<?php  include('footer/footer.php'); ?>


