<?php 

  include('../connect/connect.php');

  $paper_id = $_GET['paper_id'];
  $type = $_GET['type'];

  $sql00220 = mysqli_query($conn,"SELECT * FROM `paper_master` WHERE `PAPER_ID` = '$paper_id'");
  while($row00220 = mysqli_fetch_assoc($sql00220))
  {
    $exam_name001 = $row00220['PAPER_NAME'];
    $class_id001 = $row00220['CLASS_ID'];

    $sql00190 = mysqli_query($conn,"SELECT * FROM `classes` WHERE `CLASS_ID` = '$class_id001'");
    while($row00190 = mysqli_fetch_assoc($sql00190))
    {
      $class_name001 = $row00190['CLASS_NAME'];
    }


  }
 ?>
<!DOCTYPE html>
<html>
<head>
  <style type="text/css" media="print,screen">
.hideMe{
display:block;
}

.PrintClass {
display:block;

}
.NoPrintClass{
display:block;
}
table {
   width: 100%;
   border-collapse: collapse;
   table-layout: fixed;
}

.cell-breakWord {
   word-wrap: break-word;
}

</style>
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script type="text/javascript"
    src="http://jqueryjs.googlecode.com/files/jquery-1.3.1.min.js"> </script>
    <link href="https://fonts.googleapis.com/css?family=PT+Sans+Narrow&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<script type="text/javascript">

function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     document.body.innerHTML = originalContents;

     window.print();
    window.onafterprint = function() {
    };
     

}
function close_win() {
   window.close();
}
   </script>
    <style type="text/css">
  @media print {
  .print_btn {
    display: none;
  }
}
</style>

</head>
<body id="printableArea" style="display:block;">



<div class="col-md-12" id="printableArea" style="display:block;">
  
  <?php 

      
      $sql0027 = mysqli_query($conn,"SELECT * FROM institute WHERE INS_ID = '1'");
      while($row0027 = mysqli_fetch_assoc($sql0027))
      {
          $ins_name = $row0027['INS_NAME'];
          $ins_tp = $row0027['INS_TP'];
          $ins_mobile = $row0027['INS_MOBILE'];
          $ins_pic = $row0027['PICTURE'];

          if($ins_tp == '0' && $ins_mobile == '0')
          {
            $institute_tp = "";
          }else
          {
            if($ins_tp == '0')
            {
              if($ins_mobile != '0')
              {
                $institute_tp = $ins_mobile;
              }
            }

            if($ins_mobile == '0')
            {
              if($ins_tp != '0')
              {
                $institute_tp = $ins_tp;
              }
            }

            if($ins_tp !== '0' && $ins_mobile !== '0')
            {
              
              
                $institute_tp = $ins_tp."/".$ins_mobile;
              
            }


          }

          
          
          $ins_address = $row0027['INS_ADDRESS'];
      }
  


   ?>

<div class="row">
    
    <div class="col-md-3"></div>
    <div class="col-md-6">
      <?php if($ins_pic !== '0'){ ?>
      <center><img src="../admin/images/institute/<?php echo $ins_pic; ?>" height="100px" width="100px" style="border-radius:80px;"></center>
    <?php } ?>

      <h1 style="text-align: center;padding-top: 10px;"><?php echo $ins_name; ?></h1>
      <h5 style="text-align: center;font-weight: bold;font-size: 19px;"><?php echo $ins_address; ?> <br><br> <?php echo $institute_tp; ?></h5>

    </div>
    <div class="col-md-3"></div>

  </div>


  <div class="col-md-12" style="border-top: 1px solid #cccc;"></div>
  <div class="row">
  <div class="col-md-4">

  </div>
    <div class="col-md-4 text-center" style="padding: 20px 0 20px 0;"><h1>Exam Result</h1></div>
    <div class="col-md-4"></div>
  </div>
  
  <div class="col-md-12" style="border-top: 1px solid #cccc;padding-top: 10px;">

    <label style="font-size:19px;">Exam Name - <?php echo $exam_name001; ?></label>
    <br>
    <label style="font-size:19px;">Class Name - <?php echo $class_name001; ?></label>
    <br>
    <label style="font-size:19px;">Paper Type - <?php if($type == '0'){ echo "MCQ Paper"; } else if($type == '1'){ echo "Structured & Essay Paper"; } ?></label>
    
    <div style="border-top:1px solid #cccc !important;margin-bottom: 20px;"></div>

                  <div class="table-responsive" style="margin-top: 20px;font-size: 18px;">
                  <table class="table table-bordered table-hover" id="dataTable" style="font-size:20px;">
                  <thead>
                    <tr>
                      <th>Register ID</th>
                      <th>Student Name</th>
                      <th class="text-center">Total Mark</th>
                    </tr>
                    </thead>
                    <tbody>

                      <?php 
                        $count = 0;

                        if($type == '0')
                        {
                          $sql0016 = mysqli_query($conn,"SELECT * FROM `exam_result` WHERE `PAPER_ID` = '$paper_id' ORDER BY `MCQ_MARK` DESC");

                        }else
                        if($type == '1')
                        {
                          $sql0016 = mysqli_query($conn,"SELECT * FROM `exam_result` WHERE `PAPER_ID` = '$paper_id' ORDER BY `SE_MARK` DESC");
                          

                        }

                        $check = mysqli_num_rows($sql0016);
                        if($check > 0)
                        {
                          while($row0016 = mysqli_fetch_assoc($sql0016))
                          {
                            $stu_id = $row0016['STU_ID'];
                            $class_id = $row0016['CLASS_ID'];
                            $paper_id = $row0016['PAPER_ID'];
                            $paper_exam_key = $row0016['EXAM_KEY'];

                            

                            $sql0022 = mysqli_query($conn,"SELECT * FROM `paper_master` WHERE `CLASS_ID` = '$class_id' AND `PAPER_ID` = '$paper_id'");
                            while($row0022 = mysqli_fetch_assoc($sql0022))
                            {
                              $exam_name = $row0022['PAPER_NAME'];
                              
                              $paper_total_question = $row0022['NO_OF_QUESTIONS'];
						                  $total_marks_answer = $row0022['ANSWER_MARKS'];

                            }

                            if($type=='0')
                            {
                              $paper_mark = $row0016['MCQ_MARK'];

                              $my_total_marks = 0;
                              $my_final_total_marks = 0;
                              $final_mark = 0;

                              $sql0033 = mysqli_query($conn,"SELECT * FROM `student_mcq_transaction` WHERE `PAPER_ID` = '$paper_id' AND `STU_ID` = '$student_id'"); //check mcq test table
                              while($row0033 = mysqli_fetch_assoc($sql0033))
                              {
                                
                                  $correct_answer_no = $row0033['CORRECT_ANSWER']; //System Correct Answer
                                  $marks_answer = $row0033['MARKS']; //Mark
                                  $student_answer = $row0033['ANSWER']; //Student Answer
                                  $question = $row0033['QUESTION']; //Question

                                  $total_paper_mark = 0; //total question X Question for Mark
                                  
                                  if($correct_answer_no == $student_answer)
                                  {
                                    $my_total_marks = $my_total_marks+1;
                                  }

                              }

                              $total_paper_mark = $paper_total_question*$total_marks_answer;
                              	
                              $mcq_my_mark = $my_total_marks*$total_marks_answer; //Correct answer amount x per question for marks
                              	
                              $paper_mark = (50*$mcq_my_mark)/$total_paper_mark;


                            }else
                            if($type=='1')
                            {
                              $paper_mark = $row0016['SE_MARK'];
                            }


                            /*Class Details*/
                              $sql0019 = mysqli_query($conn,"SELECT * FROM `classes` WHERE `CLASS_ID` = '$class_id'");
                              while($row0019 = mysqli_fetch_assoc($sql0019))
                              {
                                $class_name = $row0019['CLASS_NAME'];

                              }
                              /*Class Details*/
                              /*Student data*/
                              $sql0020 = mysqli_query($conn,"SELECT * FROM `student_details` WHERE `STU_ID` = '$stu_id'");
                              while($row0020 = mysqli_fetch_assoc($sql0020))
                              {
                                $student_name = $row0020['F_NAME']." ".$row0020['L_NAME'];


                              }

                              $sql0021 = mysqli_query($conn,"SELECT * FROM `stu_login` WHERE `STU_ID` = '$stu_id'");
                              while($row0021 = mysqli_fetch_assoc($sql0021))
                              {
                                $register_id = $row0021['REGISTER_ID'];


                              }

                              /*Student data*/

                              echo '<tr>
                                <td>'.$register_id.'</td>
                                <td>'.$student_name.'</td>
                                <td class="text-center"><b>'.$paper_mark.'</b></td>
                              </tr>';

                              $count = $count + 1;
                            }

                          }else
                          if($check == '0')
                          {
                            echo '<tr><td colspan="5" class="text-center text-muted" style="font-size:16px;"><span class="fa fa-warning"></span> Not Found Data</td></tr>';
                          }
                              

                             ?>
                          </tbody>
                        </table>
                      </div>
                    </div>
                            
                    </div> 

                    <div class="col-md-12" style="position: fixed;top:10px;right: 10px;">

                      <button class="btn btn-success btn-md print_btn" onclick="printDiv('printableArea')" style="float: right;border-radius: 80px;margin-left: 10px;outline: none;"><span class="fa fa-print"></span> Print </button>

                      <button class="btn btn-danger btn-md print_btn" onclick="close_win();" style="float: right;border-radius: 80px;outline: none;margin-left: 10px;"><span class="fa fa-times"></span> Close</button>

                      <button class="btn btn-primary btn-md print_btn dataExport" onclick="close_win();" style="float: right;border-radius: 80px;outline: none;" data-type="excel"><span class="fa fa-download"></span> Excel Sheet</button>

                    </div>

                    <script src="tableExport/tableExport.js"></script>
                    <script type="text/javascript" src="tableExport/jquery.base64.js"></script>
                    <script src="js/export.js"></script>

                    </body>
                    </html>