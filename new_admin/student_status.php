<?php
  $page = "Student Status";
  $folder_in = '0';

      include('header/header.php');
      
?>




  <div class="col-md-12" style="margin-top: 8%;">
    <div class="row" style="padding-top: 10px;">
      <div class="col-md-4"></div>
      <div class="col-md-4 bg-white" style="border:1px solid #cccc;height: auto;">
      
        <div class="col-md-12" style="border-bottom: 1px solid #cccc;">
          <div class="row">

              <div class="col-md-9" style="margin-bottom: 10px;"><h2>Payment Reports</h2></div>
              <div class="col-md-3" style="padding-top: 20px;">
              </div>

          </div>
        </div>
                <div class="col-md-12" style="padding: 10px 10px 10px 10px;">
                  <form action="clz_fees_report.php" method="POST" target="_blank">
                    <div class="row">

                      <div class="col-md-12">
                        <div style="margin-top: 10px;">
                          <select class="form-control" id="teacher_id" name="teacher_id" required onchange="change_teacher();">
                            <option value="">Teacher Name</option>
                            <?php 
                              $sql004 = mysqli_query($conn,"SELECT * FROM `teacher_details`");
                              while($row004 = mysqli_fetch_assoc($sql004))
                              {
                                $teach_id = $row004['TEACH_ID'];
                                $teacher_name = $row004['POSITION'].". ".$row004['F_NAME']." ".$row004['L_NAME'];

                                echo '<option value="'.$teach_id.'">'.$teacher_name.'</option>';
                              }
                             ?>
                          </select>
                        </div>
                        </div>

                        <div class="col-md-12" style="margin-top: 10px;">
                          <select class="form-control" name="class_id" required id="class_id">
                            <option value="">Select Class Name</option>
                          </select>
                        </div>

                        <div class="col-md-12" style="margin-top: 10px;">
                          <select class="form-control" name="year" required id="year">
                              <?php 
                                    
                                    $year = date('Y');
                                    $few = strtotime("-6 years");
                                    $few_5year = date('Y',$few);
                                    for($year;$year>$few_5year;$year--)
                                    {
                                      echo '<option value="'.$year.'">'.$year.'</value>';
                                    }

                                  ?>
                            </select>
                          </div>

                        <div class="col-md-12" style="margin-top: 10px;">
                          <select class="form-control" name="month" id="month" required>
                            <?php 
                                  
                                  for($m=1;$m<13;$m++)
                                  {
                                     $month = $m;
                                     $mon_name = date('F', mktime(0, 0, 0, $month, 10)); // March
                                    echo '<option value="'.$m.'">'.$mon_name.'</value>';
                                  }

                                ?>
                          </select>
                        </div>

                      <div class="col-md-12" style="margin-top: 10px;">
                        <select class="form-control" name="pay_type" required id="pay_type" onchange="click_search_btn();">
                          <option value="">Payment Type</option>
                          <option value="1">Paid Students</option>
                          <option value="0">Not Paid Students</option>
                        </select>
                      </div>
                       
                      <div class="col-md-12" style="margin-top: 10px;font-size: 18px;">
                        <button type="submit" class="btn btn-success btn-block" style="border-radius: 80px;padding: 10px 10px 10px 10px;" name="print_class_fees"><i class="fa fa-print"></i> &nbsp;Print Report</button>

                        <center><a href="dashboard.php" class="text-center" style="font-size: 14px;">Back</a></center>
                      </div>
                    </div>
                    </form>
                  </div>
          </div>
        </div>
        <div class="col-md-4"></div>
        </div>





<script type="text/javascript">
  $('.save_btn').click(function(){
    
    var upload_btn0 = $('#selectedFile0').val();
    var upload_btn = $('#sel_file').val();
      
    if(empty(upload_btn0))
    {
      alert('Please select your audio file.');
    }else
    if(empty(upload_btn))
    {
      alert('Please select your document file.');
    }
   });
</script>

<script type="text/javascript">
  $('.navi').click(function(){
    $('.frm')[0].reset();
      
   });
</script>

<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
<script>
$(document).ready(function(){
  $("#search-table").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

<script>
$(document).ready(function(){
  $("#search-table33").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable33").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

<!-- search data -->

<!-- no data message/no found data show in search-->

<script type="text/javascript">
  $(document).ready(function () {

    (function ($) {

        $('##search-table2').keyup(function () {
            var rex = new RegExp($(this).val(), 'i');
            $('#myTable2').hide();
            $('#myTable2').filter(function () {
                return rex.test($(this).text());
            }).show();
            $('.no-data').hide();
            if($('#myTable2:visible').length == 0)
            {
                $('.no-data').show();
            }

        })

    }(jQuery));

});
</script>

<script type="text/javascript">
  function change_teacher() 
  {
    var teacher_id = document.getElementById('teacher_id').value;

                  //alert(month)
                   $.ajax({
                    url:'../admin/query/check.php',
                    method:"POST",
                    data:{search_class:teacher_id},
                    success:function(data)
                    {
                      //alert(data);
                      document.getElementById('class_id').innerHTML = data;

                    }
                  });
  }
</script>


<script type="text/javascript">
  function click_search_btn() 
  {
    var teacher_id = document.getElementById('teacher_id').value;
    var class_id = document.getElementById('class_id').value;
    var year = document.getElementById('year').value;
    var month = document.getElementById('month').value;
    var pay_type = document.getElementById('pay_type').value;

     $.ajax({
      url:'../admin/query/check.php',
      method:"POST",
      data:{search_class_students:class_id,teacher_id:teacher_id,year:year,month:month,pay_type:pay_type},
      success:function(data)
      {
        //alert(data);
        document.getElementById('myTable').innerHTML = data;

      }
    });
  }
</script>



<?php include('footer/footer.php'); ?>


