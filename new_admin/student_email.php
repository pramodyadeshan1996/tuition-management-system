<?php
  $page = "Students Details";
  $folder_in = '3';

      include('header/header.php');

        $s04 = mysqli_query($conn,"SELECT * FROM admin_login WHERE ADMIN_ID = '$admin_id'");
        while($row4 = mysqli_fetch_assoc($s04))
        {
          $name = $row4['ADMIN_NAME'];
        }

 ?>

 <div class="row">
   <div class="col-md-1"></div>
    <div class="col-md-11">
      <ol class="breadcrumb" style="font-weight: bold;margin-bottom: 04%;margin-bottom: 0;">
        <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
        <li class="breadcrumb-item" data-toggle="tooltip" data-title="Students Details"> Students Details</li>
      </ol>
    </div>
 </div>
    
    <div class="row" style="padding-top: 10px;">
      <div class="col-md-1"></div>
      <div class="col-md-10 bg-white" style="border:1px solid #cccc;height: auto;">
      
      <div class="col-md-12" style="border-bottom: 1px solid #cccc;">
        <div class="row">

            <div class="col-md-9" style="margin-bottom: 10px;"><h2><a href="dashboard.php" data-toggle="tooltip" data-title="Back"><i class="pg-icon" style="font-size: 22px;">chevron_left</i></a> Students Details</h2></div>

            <?php 

            $sql00100 = mysqli_query($conn,"SELECT * FROM stu_login WHERE STATUS = 'Active' ORDER BY REG_DATE DESC");

            $count = mysqli_num_rows($sql00100);

            if($count == '0')
            {
              $disabled = 'class="btn btn-success btn-round btn-lg pull-right disabled"  style="border-radius: 90px;padding: 10px 10px;cursor:not-allowed;" ';
            }else
            if($count>0)
            {
              $disabled = 'class="btn btn-success btn-round btn-lg pull-right" style="border-radius: 90px;padding: 10px 10px;" ' ;
            }

             ?>

            <div class="col-md-3 pull-right"><h5><a href="print_email.php" target="_blank" <?php echo $disabled; ?> data-toggle="tooltip" data-title="Print Details"><i class="pg-icon">printer</i></a></h5></div>

        </div>
      </div>
          

<div class="card card-borderless">
<div class="col-md-12" style="padding-top:10px;">

<div class="tab-content">
<div class="tab-pane active" id="home">
<div class="row column-seperation">



<div class="col-lg-12">
  
<div class=" container-fluid container-fixed-lg bg-white" style="margin-top: 0px;">
            <div class="card card-transparent">
              <div class="card-header ">

                  <form action="student_email.php" method="POST">
                      <div class="row">
                        <div class="col-md-6">
                        </div>
                      <div class="col-md-3 mt-2">
                        <input list="student_search" id="search-table" name="search_student" class="form-control" placeholder="Search Register ID.." >

                        <datalist id="student_search">
                          <?php 

                            $sql002 = mysqli_query($conn,"SELECT * FROM `student_details`");
                            while($row002 = mysqli_fetch_assoc($sql002))
                            {
                              $name = $row002['F_NAME']." ".$row002['L_NAME'];
                              $student_id = $row002['STU_ID'];
                              $tp = $row002['TP'];

                              $sql003 = mysqli_query($conn,"SELECT * FROM `stu_login` WHERE `STU_ID` = '$student_id'");
                              while($row003 = mysqli_fetch_assoc($sql003))
                              {
                                
                                $register_id = $row003['REGISTER_ID'];
                              
                              }

                              echo '<option value="'.$register_id.'">'.$name.' | '.$tp.'</option>';
                              
                            }

                           ?>
                          
                        </datalist>
                      </div>
                      <div class="col-md-2 mt-2"> <button type="submit" class="btn btn-success btn-lg btn-block" name="search_student_btn" style="border-radius: 20px;"><i class="fa fa-search"></i> &nbsp; Search</button></div>

                      <div class="col-md-1 mt-2"> <button type="submit" class="btn btn-danger btn-lg btn-block" name="reset_student_btn" style="border-radius: 20px;"><i class="fa fa-refresh"></i> &nbsp; Reset</button></div>
                    </div>
                  </form>

                    <div class="clearfix"></div>
                </div>
                <div class="card-body">
                <div class="table-responsive" style="height: 820px;overflow: hidden;">
                <table class="table table-hover">
                  <thead>
                      <tr>
                        <th style="width: 1%;">Registered Date</th>
                        <th style="width: 1%;">Registered ID</th>
                        <th style="width: 1%;">Name</th>
                        <th style="width: 1%;">Address</th>
                        <th style="width: 1%;">Contact No</th>
                      </tr>
                    </thead>
                    <tbody>

                      <?php 
                          /*------------------ Header Pagination --------------------------------*/
                        $record_per_page = 10;

                        $nxt_five_loop = 0;

                        $page = '';

                        if(isset($_GET["page"]))
                        {
                          $page = $_GET["page"];
                        }
                        else
                        {
                         $page = 1;
                        }

                        $start_from = ($page-1)*$record_per_page; //Current page no -1 X Per Page records

                        if(isset($_POST['search_student_btn']))
                        {
                          $student_id = $_POST['search_student'];

                          $sql001 = "SELECT * FROM `stu_login` WHERE `STATUS` = 'Active' AND `REGISTER_ID` LIKE '%".$student_id."%' order by `STU_ID` DESC LIMIT $start_from, $record_per_page";


                        }else
                        {
                          $sql001 = "SELECT * FROM `stu_login` WHERE `STATUS` = 'Active'  order by `STU_ID` DESC LIMIT $start_from, $record_per_page";

                        }

                        
                          $page_query = "SELECT * FROM `stu_login` WHERE `STATUS` = 'Active'  ORDER BY `STU_ID` DESC";

                        /*------------------ Header Pagination --------------------------------*/


                        
                        //LIMIT = 10-1 x 10 = 90 , 10";
                        $result = mysqli_query($conn, $sql001);
                        $page_result = mysqli_query($conn, $page_query);

                          $check = mysqli_num_rows($result);

                          if($check>0){
                          while($row001 = mysqli_fetch_assoc($result))
                          {
                            $stu_id = $row001['STU_ID'];
                            $reg = $row001['REGISTER_ID'];
                            $register_date = $row001['REG_DATE'];
                            $password = $row001['PASSWORD'];

                            $sql002 = mysqli_query($conn,"SELECT * FROM `student_details` WHERE `STU_ID` = '$stu_id'");
                            while($row002 = mysqli_fetch_assoc($sql002))
                            {
                              $name = $row002['F_NAME']." ".$row002['L_NAME'];
                              $fnm = $row002['F_NAME'];
                              $lnm = $row002['L_NAME'];
                              $tp1 = $row002['TP'];
                              $tp = str_replace(" ","",$tp1);
                              $dob = $row002['DOB'];
                              $gender = $row002['GENDER'];
                              $email = $row002['EMAIL'];
                              $address = $row002['ADDRESS'];
                              $new = $row002['NEW'];
                              
                            }

                            echo '

                              <tr class="">
                                <td class="v-align-left">'.$register_date.'</td>
                                <td class="v-align-left">'.$reg.'</td>
                                <td class="v-align-left">'.$name.'</td>
                                <td class="v-align-left">'.$address.'</td>
                                <td class="v-align-left">'.$tp.'</td>
                              </tr>

                            ';
                          }
                        }else
                        if($check == '0')
                        {
                          echo '<tr><td colspan="5" class="text-center text-danger"><span class="fa fa-warning"></span> Not Found Data</td></tr>';
                        }
                      

                        

                        
                         ?>


                    </tbody>
                </table>
                </div>
              </div>
                <div class="col-md-12" style="width: 100%;overflow: auto;">
                  <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-8" class="php_pagination">
                      
                      <div class="col-md-12">
                    <br />
                    <?php
                    $total_records = mysqli_num_rows($page_result);
                    $total_pages = ceil($total_records/$record_per_page);
                    $start_loop = $page;
                    $difference = $total_pages - $page;
                    if($difference <= 5)
                    {
                     $start_loop = $total_pages - 5;
                    }

                    echo '<ul class="pagination">';
                    $end_loop = $start_loop + 4;
                    $five_loop = $page-5;
                    if($five_loop <= 0)
                    {
                      $five_loop = 1;
                    }
                    if($page > 1)
                    {

                      echo '<li><a href="student_email.php?page=1">First</a></li>';
                      echo '<li><a href="student_email.php?page='.($page - 1).'"> < </a></li>';
                      echo '<li><a href="student_email.php?page='.$five_loop.'"> <<< </a></li>';

                    }
                    if($start_loop !== '1')
                    {
                      $few_no = $start_loop - 1;
                    }else
                    if($start_loop == '1')
                    {
                      $few_no = 1;
                    }
                    
                    for($i=$few_no; $i<=$end_loop+1; $i++)
                    {     
                      if($i == $page)
                      {
                        echo '<li class="active"><a href="student_email.php?page='.$i.'" style="font-weight:bold;background-color:#2980b9;color:white;">'.$i.'</a></li>';
                      }else
                      {
                        if($i == '0')
                        {
                          continue;
                        }else
                        if($i > '0')
                        {
                          echo '<li><a href="student_email.php?page='.$i.'" >'.$i.'</a></li>';
                        }
                        
                      }

                    }
                    if($page <= $end_loop)
                    {
                      
                      if($page == '1')
                      {
                        $nxt_five_loop = 5;
                      }else
                      if($page >= 5)
                      {
                        $nxt_five_loop = $page+5;

                        if($total_pages < $nxt_five_loop)
                        {
                          $nxt_five_loop = $total_pages;
                        }
                      }else
                      {
                        $nxt_five_loop = $page+5;
                      }

                        echo '<li><a href="student_email.php?page='.($page + 1).'" > > </a></li>';
                        echo '<li><a href="student_email.php?page='.$nxt_five_loop.'" > >>> </a></li>';
                        echo '<li><a href="student_email.php?page='.$total_pages.'" >Last</a></li>';
                      
                    }
                    
                    echo '</ul>';
                    
                    ?>
                    </div>

                    </div>
                  </div>
                    
                </div>
            </div>
</div>
</div>
</div>




</div>
</div>



      </div>
      </div>
      <div class="col-md-1"></div>
    </div>


  </div>
  </div>
  </div>
  </div>





<script type="text/javascript">
  $('.save_btn').click(function(){
    
    var upload_btn0 = $('#selectedFile0').val();
    var upload_btn = $('#sel_file').val();
      
    if(empty(upload_btn0))
    {
      alert('Please select your audio file.');
    }else
    if(empty(upload_btn))
    {
      alert('Please select your document file.');
    }
   });
</script>

<script type="text/javascript">
  $('.navi').click(function(){
    $('.frm')[0].reset();
      
   });
</script>

<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
<!-- search data -->

<!-- no data message/no found data show in search-->

<script type="text/javascript">
  $(document).ready(function () {
 
    (function ($) {

        $('#myInput').keyup(function () {
            var rex = new RegExp($(this).val(), 'i');
            $('#myTable tr').hide();
            $('#myTable tr').filter(function () {
                return rex.test($(this).text());
            }).show();
            $('.no-data').hide();
            if($('#myTable tr:visible').length == 0)
            {
                $('.no-data').show();
            }

        })

    }(jQuery));

});
</script>




<?php include('footer/footer.php'); ?>