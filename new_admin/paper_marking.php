  
<?php

$page = "Paper Marking";
$folder_in = '0';

$paper_id = $_GET['paper_id'];

  include('header/header.php'); 
 ini_set( "display_errors", 0); 

 $admin_id = $_SESSION['ADMIN_ID'];

 
 $allImages = '-adjoin ../admin/images/profile/1714493516808306.jpg';
 exec("convert $allImages newdoc.pdf");

  ?>
  <style type="text/css">

    #canvas_container {
          width: auto;
          height: 500px;
          overflow: auto;
      }
 
      #canvas_container {
        background: #ccc;
        text-align: center;
        width: 100%;
      }

      #canvas_container2 {
        margin-top: 6px;
        margin-bottom: 6px;
          width: auto;
          height: 500px;
          overflow: auto;
        width: 100%;
      }
 
      #canvas_container2 {
        background: #ccc;
        text-align: center;
      }

    .btn_pulse {
  
  display: block;
  border-radius: 10%;
  cursor: pointer;
  animation: none;
  float: left;
  bottom: 5px;
  right: 5px;
  font-weight: bold;
  padding-top: 2px;
  text-align: center;
  color: white;
  font-size: 14px;
  animation: pulse 1.6s infinite;
}


@keyframes pulse {
  0% {
    -moz-box-shadow: 0 0 0 0 #7252d3;
    box-shadow: 0 0 0 0 #7252d3;
  }
  70% {
    -moz-box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
  }
  100% {
    -moz-box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
  }
}
  </style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<br>
<br>

<div class="col-md-12" style="border-top: 1px solid #cccc;margin-top: 10px;">

<div style="margin: 10px 0px 10px 0;"><a href="exam.php" style="text-decoration: none;color: gray;"><span class="fa fa-angle-left"></span> Back</a></div>

<form action="../admin/query/insert.php" method="POST">
<div class="row" style="margin-top: 1%;margin-bottom: 3%;">
<div class="col-md-6">

  <div class="col-md-12 bg-white" style="padding: 20px 20px 20px 20px;">
    <div style="border-bottom: 1px solid #cccc;">
      <label class="text-muted text-center" style="font-size: 30px;"><span class="fa fa-file" style="font-size: 30px;"></span><strong> Paper Details</strong></label>
    </div>


    <div style="padding-top: 20px;height: 602px;">
      
      <div class="row" style="border-bottom: 1px solid #cccc;">
        <div class="col-md-12" style="">
          <div class="form-group form-group-default input-group">
              <div class="form-input-group">
                <label>Student Name</label>
                <select id="student_name" name="student_name" required class="form-control" onchange="select_student();">

                  <?php 
                    echo '<option value="">Select Students</option>';
                    $sql001001 = mysqli_query($conn,"SELECT * FROM `student_essay_master` WHERE `PAPER_ID` = '$paper_id' AND `STATUS` = '0' ORDER BY `STU_ID` DESC");
                    if(mysqli_num_rows($sql001001)>0)
                    {
                      while($row001001 = mysqli_fetch_assoc($sql001001))
                      {
                        $student_id001 = $row001001['STU_ID'];

                        $sql002 = mysqli_query($conn,"SELECT * FROM `student_details` WHERE `STU_ID` = '$student_id001'");

                        while($row002 = mysqli_fetch_assoc($sql002))
                        {
                              $student_name = $row002['F_NAME']." ".$row002['L_NAME'];
                        }

                        echo '<option value="'.$student_id001.'">'.$student_name.'</option>';
                      }
                    }
                    

                   ?>

                 </select>
              </div>
          </div>
        </div>
      </div>

      <script type="text/javascript">
        
        function select_student() {
          
          var student_id = document.getElementById('student_name').value;

          $.ajax({
            url:'../admin/query/check.php',
            method:"POST",
            data:{check_student_paper:student_id},
            success:function(data)
            {
              //alert(data)
              document.getElementById('uploaded_file').innerHTML = data;
            }
          });


        }

      </script>
      <div id="uploaded_file" style="height: 520px;overflow: auto;">
        
        <div class="alert alert-danger text-center" style="margin: 10px 10px;">
          <span class="fa fa-warning"></span> Not Found File!
        </div>

      </div>
      
    </div>
  </div>



</div>

<div class="col-md-6">

  <div class="col-md-12 bg-white" style="padding: 20px 20px 20px 20px;">

    <div style="border-bottom: 1px solid #cccc;">
      <label class="text-muted text-center" style="font-size: 30px;"><span class="fa fa-check-circle"></span> <strong> Answer Sheet</strong></label>
    </div>


    <div class="table-responsive" style="height: 540px;overflow: auto;padding-bottom: 6px;">
      <table class="table table-hover">
        <thead>
          <th>Question No</th>
          <th>Sub Question No</th>
          <th style="padding-left: 50px;">Marks</th>
        </thead>

        <tbody>

          <?php 

            $sql001 = mysqli_query($conn,"SELECT * FROM `essay_master_question` WHERE `PAPER_ID` = '$paper_id'");
            if(mysqli_num_rows($sql001)>0)
            {
              while($row001 = mysqli_fetch_assoc($sql001))
              {
                $master_question = $row001['QUESTION_NO'];
                $total_mark = $row001['MASTER_MARK'];
                $essay_master_id = $row001['ESSAY_TRANS_ID'];
                
                echo '<input type="hidden" name="total_main_question" value="'.$master_question.'">';
                
                $sql002 = mysqli_query($conn,"SELECT * FROM `essay_sub_question` WHERE `ESSAY_TRANS_ID` = '$essay_master_id'");
                while($row002 = mysqli_fetch_assoc($sql002))
                {
                  $sub_master_question = $row002['SUB_QUESTION_NO'];
                  $sub_mark = $row002['SUB_MARK'];

                  echo '<tr>
                          <td style="font-size:14px;">'.$master_question.' Question</td>
                          <td style="font-weight:bold;font-size:16px;">'.$sub_master_question.' Sub Question</td>
                          <td style="font-weight:bold;">

                          <input type="number" name="sub_question['.$master_question.'][]" style="width:60px;border:none;text-align:right;" value="0" max="'.$sub_mark.'" min="0" onclick="this.select();"> / '.$sub_mark.'</td>
                        </tr>';
                }

                 echo '<tr>
                      <td style="padding:4px;padding-left:20px;"><h4>Total Mark -</h4></td>
                      <td style="padding:4px;"></td> 
                      <td style="padding:4px;padding-left:60px;"><h4>'.$total_mark.'</h4></td>
                    </tr>';
              }
            }else
            if(mysqli_num_rows($sql001) == '0')
            {
              echo '<tr>
                      <td colspan="3" class="text-center"><span class="fa fa-warning"></span> Not Founded Question!</td>
                    </tr>';
            }
            

           ?>

          
          
        </tbody>
      </table>
    </div>

    <div style="padding-top: 10px;border-top: 1px solid #cccc;">

      <button type="submit" class="btn btn-success btn-lg btn-block" data-toggle="tooltip" data-title="තහවුරු කිරීම" style="padding: 10px 10px 10px 10px;font-size: 20px;" value="<?php echo $paper_id; ?>" name="paper_marking" ><i class="pg-icon" style="font-size: 30px;">tick_circle</i> Finish Marking</button>
    </div>
  </div>
</div>


</div>

</form>

<script type="text/javascript">
  $(document).ready(function(){
      //$('#pay_btn').prop('disabled',true); //refresh with disable button

      setTimeout(function() {
          <?php $_SESSION['msg'] = ''; ?>
          $('#successMessage').fadeOut('slow');
      }, 3000); // <-- time in milliseconds
  });
</script>

<?php  include('footer/footer.php'); ?>


<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
<!-- search data -->

<!-- no data message/no found data show in search-->

<script type="text/javascript">
  $(document).ready(function () {

    (function ($) {

        $('#myInput').keyup(function () {
            var rex = new RegExp($(this).val(), 'i');
            $('#myTable tr').hide();
            $('#myTable tr').filter(function () {
                return rex.test($(this).text());
            }).show();
            $('.no-data').hide();
            if($('#myTable tr:visible').length == 0)
            {
                $('.no-data').show();
            }

        })

    }(jQuery));

});
</script>

<script type="text/javascript">


    function find_class()
    {
                  
        var teacher_id = document.getElementById('teacher_id').value;

         $.ajax({
          url:'../admin/query/check.php',
          method:"POST",
          data:{check_teacher_mcq:teacher_id},
          success:function(data)
          {
            //alert(data)
            document.getElementById('class_id001').innerHTML = data;
          }
        });
  }


</script>

<script type="text/javascript">


    function add_questions()
    {
      var show_question = document.getElementById('question').value;
      //alert(show_question)
      $.ajax({
          url:'../admin/query/check.php',
          method:"POST",
          data:{str_que_ans:show_question},
          success:function(data)
          {
            //alert(data)
            document.getElementById('que_ans').innerHTML = data;
          }
        });
  }


</script>

