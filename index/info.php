
  <div class="row" style="margin-top: 4px;margin-bottom: 8px;">
      <div class="col-md-4">
          <div class="section-title">
              <h5>උපදෙස්</h5>
          </div>

      </div>


      <style type="text/css">
        .switch {
    display: inline-block;
    position: relative;
    width: 50px;
    height: 30px;
    border-radius: 20px;
    background: #dfd9ea;
    transition: background 0.28s cubic-bezier(0.4, 0, 0.2, 1);
    vertical-align: middle;
    cursor: pointer;
}
.switch::before {
    content: '';
    position: absolute;
    top: 2px;
    left: 2px;
    width: 22px;
    height: 25px;
    background: #fafafa;
    border-radius: 50%;
    transition: left 0.28s cubic-bezier(0.4, 0, 0.2, 1), background 0.28s cubic-bezier(0.4, 0, 0.2, 1), box-shadow 0.28s cubic-bezier(0.4, 0, 0.2, 1);
}
.switch:active::before {
    box-shadow: 0 2px 8px rgba(0,0,0,0.28), 0 0 0 20px rgba(128,128,128,0.1);
}
input:checked + .switch {
    background: <?php echo $ins_bg_color; ?>;
}
input:checked + .switch::before {
    left: 27px;
    background: #fff;
}
input:checked + .switch:active::before {
    box-shadow: 0 2px 8px rgba(0,0,0,0.28), 0 0 0 20px rgba(0,150,136,0.2);
}
      </style>
      <div class="col-md-8">


      </div>
  </div>

<!-- Paid classes -->


  <div class=" mb-20">
      <!-- item 1-->
      
       
        <h4 class="text-danger" style="font-weight:bold;"><span class="fa fa-check-circle"></span> මුදල් ගෙවන ආකාරය</h4>

        <div style="padding: 10px 10px;">
          <p class="text-danger" style="font-weight:bold;margin-top:10px;font-size:12px;">01. පහත ආකාරයෙන් ඔබගේ පන්තියට අදාල Button එක ක්ලික් කරන්න.</p>
          <div class="row">
            <div class="col-md-4"><a href="info/1A.png" target="_blank"><img src="info/1A.png" style="width:100%;height: auto;border:1px solid #cccc;"></a></div>
            <div class="col-md-4"></div>
            <div class="col-md-4"></div>
          </div>
          
          <p class="text-danger" style="font-weight:bold;margin-top:10px;font-size:12px;">02. පහත ආකාරයෙන් ඔබගේ පන්තියට අදාල ගෙවීම් සිදුකරන ක්‍රම තෝරාගන්න.බැංකු තැන්පතු මගින් සිදුකරන ගෙවීම් පහත ආකාරයට සිදුකරන්න.</p>
          <div class="row">
            <div class="col-md-4"><a href="info/2.png" target="_blank"><img src="info/2.png" style="width:100%;height: auto;border:1px solid #cccc;"></a></div>
            <div class="col-md-4"></div>
            <div class="col-md-4"></div>
          </div>

          <p class="text-danger" style="font-weight:bold;margin-top:10px;font-size:12px;">03. මෙහි බැංකු තැන්පතු මගින් සිදුකරන ගෙවීම් හි විස්තර ලබාදී ඇත.එම ගිණුම් අංකයන්ට මුදල් බැර කිරීමෙන් පසුව බැංකුව මගින් ලබාදෙන රිසිට්පතෙහි ලියාපදිංචි අංකය හා නම සදහන් කර එහි පැහැදිලි ඡායාරූපයක් පහත දැක්වෙන Upload bank slip නම් ස්ථානය ක්ලික් කර ඡායාරූපය තෝරා Submit නම් button එක ක්ලික් කරන්න.</p>
          <div class="row">
            <div class="col-md-4"><a href="info/3.png" target="_blank"><img src="info/3.png" style="width:100%;height: auto;border:1px solid #cccc;"></a></div>
            <div class="col-md-4"></div>
            <div class="col-md-4"></div>
          </div>

        <p class="text-danger" style="font-weight:bold;margin-top:10px;font-size:12px;">03 A. රිසිට්පත යැවීමෙන් අනතුරුව &quot; පරික්ෂා කරමින් පවතී &quot; ලෙස දිස්වේ.ඔබ ලබාදුන් තොරතුරු පරික්ෂාකර නිවැරදි නම් පන්ති සදහා සම්බන්ධ වීමට අවස්ථාව ඇත. </p>

        <div class="row">
            <div class="col-md-4 mt-2"><a href="info/4.png" target="_blank"><img src="info/4.png" style="width:100%;height: auto;border:1px solid #cccc;"></a></div>
            <div class="col-md-4 mt-2"><a href="info/5.png" target="_blank"><img src="info/5.png" style="width:100%;height: auto;border:1px solid #cccc;"></a></div>
            <div class="col-md-4"></div>
          </div>



        <p class="text-danger" style="font-weight:bold;margin-top:10px;font-size:12px;">03 B. යම් හෙයකින් ලබා දුන් රිසිට්පතෙහි ගැටලුවක් ඇත්නම් ඒ බව &quot; රිසිට්පතෙහි දෝෂයක් ඇත. &quot; ලෙස ඔබව දැනුවත් කරනු ඇත. නැවතත් නිවැරදි තොරතුරු ඇතුලත් කර රිසිට්පත යොමු කරන්න.</p>

          <div class="row">
            <div class="col-md-4 mt-2"><a href="info/6.png" target="_blank"><img src="info/6.png" style="width:100%;height: auto;border:1px solid #cccc;"></a></div>
            <div class="col-md-4 mt-2"><a href="info/7.png" target="_blank"><img src="info/7.png" style="width:100%;height: auto;border:1px solid #cccc;"></a></div>
            <div class="col-md-4"></div>
          </div>
       </div>

  </div>
 

