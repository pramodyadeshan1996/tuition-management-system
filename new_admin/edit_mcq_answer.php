  
<?php

$page = "Update Paper Answer";
$folder_in = '0';

$paper_id = $_GET['paper_id'];

  include('header/header.php'); 
 ini_set( "display_errors", 0); 

 $admin_id = $_SESSION['ADMIN_ID'];

  ?>
  <style type="text/css">

    #canvas_container {
          width: auto;
          height: 500px;
          overflow: auto;
      }
 
      #canvas_container {
        background: #ccc;
        text-align: center;
        width: 100%;
      }

      #canvas_container2 {
        margin-top: 6px;
        margin-bottom: 6px;
          width: auto;
          height: 500px;
          overflow: auto;
        width: 100%;
      }
 
      #canvas_container2 {
        background: #ccc;
        text-align: center;
      }

    .btn_pulse {
  
  display: block;
  border-radius: 10%;
  cursor: pointer;
  animation: none;
  float: left;
  bottom: 5px;
  right: 5px;
  font-weight: bold;
  padding-top: 2px;
  text-align: center;
  color: white;
  font-size: 14px;
  animation: pulse 1.6s infinite;
}


@keyframes pulse {
  0% {
    -moz-box-shadow: 0 0 0 0 #7252d3;
    box-shadow: 0 0 0 0 #7252d3;
  }
  70% {
    -moz-box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
  }
  100% {
    -moz-box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
  }
}
  </style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<br>
<br>

<div class="col-md-12" style="border-top: 0px solid #cccc;margin-top:10px;">

<div style="margin: 0px 0px 10px 0;"><a href="exam.php" style="text-decoration: none;color: gray;"><span class="fa fa-angle-left"></span> Back</a></div>


<div class="row" style="margin-top: 1%;margin-bottom: 3%;">
<div class="col-md-6">

  <div class="col-md-12 bg-white" style="padding: 20px 20px 20px 20px;">
    <div style="border-bottom: 1px solid #cccc;">
      <label class="text-muted text-center" style="font-size: 30px;"><span class="fa fa-file" style="font-size: 30px;"></span><strong> Paper Details</strong></label>
    </div>


    <div style="padding-top: 20px;height: 602px;">
      
        
        <?php 
          $sql0031 = mysqli_query($conn,"SELECT * FROM `paper_master` WHERE `PAPER_ID` = '$paper_id'");

          while($row0031 = mysqli_fetch_assoc($sql0031))
          {
            $file_name = $row0031['PDF_NAME'];
            $no_answer = $row0031['NO_QUESTION_ANSWER'];
            $total_question = $row0031['NO_OF_QUESTIONS'];
          }

         ?>

         <iframe src="../admin/images/paper_document/<?php echo $file_name; ?>" style="width: 100%;height: 600px;"></iframe>
      
    </div>
  </div>

  

</div>

<div class="col-md-6">

  <div class="col-md-12 bg-white" style="padding: 20px 20px 20px 20px;">

    <div style="border-bottom: 1px solid #cccc;">
      <label class="text-muted text-center" style="font-size: 30px;"><span class="fa fa-check-circle"></span> <strong> Answer Sheet</strong></label>
    </div>

  <form action="../admin/query/update.php" method="POST">
    <div class="table-responsive" style="height: 540px;overflow: auto;padding-bottom: 6px;">
      <table class="table table-hover">
        <thead style="font-size: 10px;">
          <th style="width:30%;">Question No</th>
          <th style="width:40%;">Answer No</th>
          <th style="width:30%;">Marks</th>
        </thead>

        <tbody style="font-size: 10px;">

          <?php 

            $sql001 = mysqli_query($conn,"SELECT * FROM `mcq_trans` WHERE `PAPER_ID` = '$paper_id'");
            if(mysqli_num_rows($sql001)>0)
            {
              while($row001 = mysqli_fetch_assoc($sql001))
              {
                $question = $row001['QUESTION'];
                $correct_answer = $row001['CORRECT_ANSWER'];
                $marks = $row001['MARKS_ANSWER'];
                $mcq_tr_id = $row001['MCQ_TRAN_ID'];

                if($question < 10)
                {
                  $q_no = "0".$question;
                }else
                {
                  $q_no = $question;
                }
                
                echo '<input type="hidden" name="mcq_tr_id[]" value="'.$mcq_tr_id.'">';
                echo '<input type="hidden" name="question_no[]" value="'.$question.'">';

                 echo '<tr>
                      <td><b>Question No - '.$q_no.'</b></td>
                      <td>
                        <select class="form-control" name="answer[]">

                          <option value="'.$correct_answer.'">'.$correct_answer.'</option>';

                          for ($i=1; $i <= $no_answer; $i++)
                          { 
                                      
                            if($i != $correct_answer)
                            {
                              echo '<option value="'.$i.'">'.$i.'</option>';
                            }
                          }
                          echo '

                        </select></td>
                        <td><input type="text" name="mark[]" value="'.$marks.'" class="form-control number"></td>
                    </tr>';
              }
            }else
            if(mysqli_num_rows($sql001) == '0')
            {
              echo '<tr>
                      <td colspan="2" class="text-center"><span class="fa fa-warning"></span> Not Founded Question!</td>
                    </tr>';
            }
            

           ?>

          
          
        </tbody>
      </table>
    </div>

    <div style="padding-top: 10px;border-top: 1px solid #cccc;">

      <button type="submit" class="btn btn-success btn-lg btn-block" data-toggle="tooltip" data-title="තහවුරු කිරීම" style="padding: 10px 10px 10px 10px;font-size: 20px;" value="<?php echo $paper_id; ?>" name="update_mcq_answer" ><i class="pg-icon" style="font-size: 30px;">tick_circle</i> Update Marking</button>
    </div>

</form>

  </div>
</div>


</div>

<!-- Number Validation -->

    <script>

        //Check Number and disable characters

        $(document).ready(function () {

            $('.number').bind('keyup blur',function()
            { 
                   var thistext = $(this);
                   thistext.val(thistext.val().replace(/[^0-9.]/g,'') ); 

                   
            });

        })

        //Check Number and disable characters
    </script>

<!-- Number Validation -->


<?php  include('footer/footer.php'); ?>


