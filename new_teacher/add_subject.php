
<?php

$page = "Create Subjects";
$folder_in = '0';

  include('header/header.php'); 
 ini_set( "display_errors", 0); 


  ?>
  <style type="text/css">
    .btn_pulse {
  
  display: block;
  border-radius: 10%;
  cursor: pointer;
  animation: none;
  float: left;
  bottom: 5px;
  right: 5px;
  font-weight: bold;
  padding-top: 2px;
  text-align: center;
  color: white;
  font-size: 14px;
  animation: pulse 1.6s infinite;
}


@keyframes pulse {
  0% {
    -moz-box-shadow: 0 0 0 0 #7252d3;
    box-shadow: 0 0 0 0 #7252d3;
  }
  70% {
    -moz-box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
  }
  100% {
    -moz-box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
  }
}
  </style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<div class="row" style="border-bottom: 1px solid #cccc;margin-top: 55px;">
  <div class="col-md-11">
    <ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-bottom: 04%;margin-bottom: 0;">
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="Create Subjects"> Create Subjects</li>
    </ol>
  </div>
  <div class="col-md-1" style="padding-top: 10px;">
        <a data-toggle="tooltip" data-title="Create a subject" data-placement="right"><button class="btn btn-primary btn-lg btn-rounded btn_pulse pull-right" data-toggle="modal" data-target="#add_subject" style="padding: 10px 10px;"><i class="pg-icon">add</i></button></a>
  </div>



         <div class="modal fade slide-up disable-scroll" id="add_subject" tabindex="-1" role="dialog" aria-hidden="false">
          <div class="modal-dialog ">
          <div class="modal-content-wrapper">
          <div class="modal-content">
          <div class="modal-header clearfix text-left">
          <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
          </button>
          <h5>Create Subjects</h5>
          </div>
          <div class="modal-body">
            <form action="../teacher/query/insert.php" method="POST" enctype="multipart/form-data">
            <div class="form-group form-group-default" style="margin-top: 10px;">
              <label>Classes</label>
              <select class="form-control select_class" name="level_id"  required style="cursor: pointer;">
                <option value="">Select Classes </option>

                  <?php 

                      $sql0015 = mysqli_query($conn,"SELECT * FROM level");
                      while($row0015=mysqli_fetch_assoc($sql0015))
                      {
                        $level_name = $row0015['LEVEL_NAME'];
                        $level_id = $row0015['LEVEL_ID'];

                        echo '<option value='.$level_id.'>'.$level_name.'</option>';

                      }

                       ?>
                
              </select>
            </div>

            <div class="form-group form-group-default input-group">
            <div class="form-input-group">
            <label>Subject Name</label>
            <input type="text" name="subject" class="form-control change_inputs" placeholder="XXXXXXXXXXXX" required>
            </div>
            </div>


            <!-- <div class="form-group form-group-default input-group">
            <div class="form-input-group">
            <label>Super Subject Name</label>
              <select class="form-control" name="super_subject">
                <option value="0">Select Super Subject Name</option>
                <option value="AGR">Agriculture</option>
                <option value="BST">BST</option>
                <option value="BA">Agriculture/BST</option>
              </select>
            </div>
            </div> -->

            <div class="form-group form-group-default input-group" style="display: none">
            <div class="form-input-group">
            <label>Super Subject Name</label>
              <select class="form-control" name="super_subject">
                <option value="NO">Select Super Subject Name</option>
              </select>
            </div>
            </div>



            <div class="form-group form-group-default input-group">
            <div class="form-input-group">
            <label>Picture</label>
            <input type="file" name="picture" class="form-control" accept="image/*" value="0">
            </div>
            </div>

            <button type="submit" class="btn btn-success btn-block btn-lg" name="create_subject123"><i class="pg-icon">add</i> Add</button>
            </form>
            </div>
            </div>
            </div>
            </div>

            </div>
            
</div>





<div class="col-md-12" style="margin-top: 0;">
  <h3 style="text-transform: capitalize;"><a href="dashboard.php" data-toggle="tooltip" data-title="Back"><i class="pg-icon" style="font-size: 22px;">chevron_left</i></a> Create Subjects</h3>

<div class=" container-fluid   container-fixed-lg bg-white">
<div class="card card-transparent">
<div class="card-header ">
<div class="card-title">Details and removal of all the subjects you have entered can be done here.
</div>
<div class="pull-right">
<div class="col-xs-12">
<input type="text" id="search" class="form-control pull-right" placeholder="Search" data-toggle="tooltip" data-title="අවශ්‍ය දත්ත සෙවීම'">
</div>
</div>
<div class="clearfix"></div>
</div>

<?php 

$sql00100 =  mysqli_query($conn,"SELECT * FROM subject");

$count = mysqli_num_rows($sql00100);

 ?>

<div class="card-body table-responsive" style="height: 500px;overflow: auto;margin-bottom: 0;">
  <table class="table demo-table-search table-responsive-block text-left table-striped" id="tableWithSearch">
  <thead>
    <th style="width: 10%;">Picture</th>
    <th>Class Name <label class="label label-success"><?php echo  $count; ?></th>
    <th>Subject Name</th>
    <th style="text-align: center;">Action</th>

  </thead>
  <tbody id="myTable">
<tr class="no-data alert alert-danger" style="margin-top: 20px;display: none;">
  <td colspan="4" class="text-danger"><span class="fa fa-warning"></span> Not Found Data</td>
</tr>
  <?php 
          $sql0015 = mysqli_query($conn,"SELECT * FROM `subject` ORDER BY `SUB_ID` DESC");

          if(mysqli_num_rows($sql0015)>0)
          {
              while($row0015=mysqli_fetch_assoc($sql0015))
              {
                  $sub_name = $row0015['SUBJECT_NAME'];
                  $subject_id = $row0015['SUB_ID'];
                  $level_id = $row0015['LEVEL_ID'];
                  $pic = $row0015['PICTURE'];
                  $sh_lev = $row0015['SH_LEVEL'];

                  if($sh_lev == 'AGR')
                  {
                    $sh_name = '(Agriculture)';
                  }else
                  if($sh_lev == 'BST')
                  {
                    $sh_name = '(BST)';
                  }else
                  if($sh_lev == 'BA')
                  {
                    $sh_name = '(Agriculture/BST)';
                  }else
                  if($sh_lev == 'NO')
                  {
                    $sh_name = '';
                  }

                  if($pic == '0')
                  {
                    $pic = 'subject.png';
                  }
                 

                 $sql0016 = mysqli_query($conn,"SELECT * FROM level WHERE LEVEL_ID = '$level_id' ORDER BY LEVEL_ID ASC");
                while($row0016=mysqli_fetch_assoc($sql0016))
                {
                  $level_name = $row0016['LEVEL_NAME'];
                 }

                 //  <<< Super subject name for agribst project
                echo '<tr>
                          <td class="v-align-middle"><center><img src="../admin/images/subject_image/'.$pic.'" class="rounded-circle" style="height: 130px;width: 100%;border: 0px solid white;"></center></td>
                          <td class="v-align-middle">'.$level_name.'</td>
                          <td class="v-align-middle">'.$sub_name.' '.$sh_name.'</td>
                          <td class="v-align-middle" style="text-align:center;">

                          <button data-toggle="modal" data-target="#edit_subject'.$subject_id.'" class="btn btn-success  btn-xs btn-rounded" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;" data-title="Approve" data-toggle="tooltip" id="edit1'.$stu_id.'"><i class="pg-icon">edit</i></button>
                          ';?>

                        <button type="button" data-target="#delete_subject<?php echo $level_id; ?>" data-toggle="modal" class="btn btn-danger btn-rounded btn-xs" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;"><i class="pg-icon">trash_alt</i></button>

                        <div id="delete_subject<?php echo $level_id; ?>" class="modal fade" role="dialog">
                          <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                              <div>
                                <div class="row" style="padding-left: 20px;padding-bottom: 20px;">
                                  <div class="col-md-8 text-left"><h4 class="modal-title">Delete Subject</h4></div>
                                  <div class="col-md-4"><button type="button" class="close" data-dismiss="modal">&times;</button></div>
                                </div>
                              </div>
                              <div class="modal-body">

                                <div class="text-left" style="padding-bottom: 10px;"><span class="fa fa-warning text-danger"></span> ඔබ මෙම විශය යටතේ අඩංගු කර ඇති සියලුම පන්ති ඉවත් වේ.</div>

                                  
                                <div class="text-left"><i class="pg-icon">cursor</i> ඔබ එකග වේ නම් දත්ත මකා දමන්නද?</div>

                              </div>
                              <div class="modal-footer">
                                <a href="../teacher/query/delete.php?delete_subject2=<?php echo $pic; ?>&&sub_id=<?php echo $subject_id; ?>" class="btn btn-success"><i class="pg-icon">tick_circle</i>&nbsp;Yes</a>
                                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="pg-icon">close</i>&nbsp;No</button>
                              </div>
                            </div>

                          </div>
                        </div>



                        <?php
                        echo '
                          </td>
                        </tr>';

                        ?>

                        <div class="modal fade slide-up disable-scroll" id="edit_subject<?php echo $subject_id; ?>" tabindex="-1" role="dialog" aria-hidden="false">
                          <div class="modal-dialog ">
                          <div class="modal-content-wrapper">
                          <div class="modal-content">
                          <div class="modal-header clearfix text-left">
                          <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
                          </button>
                          <h5>Edit Subjects</h5>
                          </div>
                          <div class="modal-body">
                            <form action="../teacher/query/update.php" method="POST" enctype="multipart/form-data">
                            <div class="form-group form-group-default" style="margin-top: 10px;">
                              <label>Classes</label>
                              <select class="form-control select_class" name="level_id"  required style="cursor: pointer;">
                                <option value="<?php echo $level_id ?>"> <?php echo $level_name; ?></option>
                                <?php 
                                      $sql00152 = mysqli_query($conn,"SELECT * FROM level WHERE LEVEL_ID != '$level_id'");
                                      while($row00152=mysqli_fetch_assoc($sql00152))
                                      {
                                        $level_name = $row00152['LEVEL_NAME'];
                                        $level_id = $row00152['LEVEL_ID'];

                                        echo '<option value='.$level_id.'>'.$level_name.'</option>';

                                      }
                                ?>
                               </select>
                              </div>

                              <div class="form-group form-group-default input-group">
                              <div class="form-input-group">
                              <label>Subject Name</label>
                              <input type="text" name="subject" class="form-control change_inputs" placeholder="XXXXXXXXXXXX" required value="<?php echo $sub_name; ?>">
                              </div>
                              </div>


                              <div class="form-group form-group-default input-group" style="display: none;">
                              <div class="form-input-group">
                              <label>Super Subject Name</label>
                                <select class="form-control" name="super_subject">
                                  <option value="<?php echo $sh_lev; ?>"> <?php echo $sh_name; ?></option>
                                <?php 
                                      
                                    if($sh_lev == 'AGR')
                                    {
                                      echo '
                                        <option value="BST">BST</option>
                                        <option value="BA">Agriculture/BST</option>';
                                    }else
                                    if($sh_lev == 'BST')
                                    {
                                      echo '
                                        <option value="AGR">Agriculture</option>
                                        <option value="BA">Agriculture/BST</option>';
                                    }else
                                    if($sh_lev == 'BA')
                                    {
                                      echo '
                                        <option value="AGR">Agriculture</option>
                                        <option value="BA">Agriculture/BST</option>';
                                    }

                                ?>
                                </select>
                              </div>
                              </div>



                              <div class="form-group form-group-default input-group">
                              <div class="form-input-group">
                              <label>Picture</label>
                              <input type="file" name="picture" class="form-control" accept="image/*" value="<?php echo $pic; ?>">
                              </div>
                              </div>
                              <input type="hidden" name="recent_file" value="<?php echo $pic; ?>">
                              <button type="submit" class="btn btn-primary btn-block btn-lg" name="edit_subject" value="<?php echo $subject_id; ?>"><i class="pg-icon">edit</i> Edit</button>
                              </form>
                              </div>
                              </div>
                              </div>
                              </div>
                            </div>
<?php

              } 
            }else
            if(mysqli_num_rows($sql0015)== '0')
            {
                echo '<tr><td colspan="4" class="text-danger text-center"><span class="fa fa-warning text-danger"></span> Empty Data!</td></tr>';
            }

 ?>
  

  </tbody>
  </table>
</div>
</div>

</div>
</div>
</div>
</div>

<script type="text/javascript">
	setInterval(function() {
    $('blink').fadeIn(1300).fadeOut(1500);
}, 1000);
</script>

<script>
$(document).ready(function(){
  $("#search").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

<script type="text/javascript">
  $(document).ready(function () {
 
    (function ($) {

        $('#search').keyup(function () {
            var rex = new RegExp($(this).val(), 'i');
            $('#myTable tr').hide();
            $('#myTable tr').filter(function () {
                return rex.test($(this).text());
            }).show();
            $('.no-data').hide();
            if($('#myTable tr:visible').length == 0)
            {
                $('.no-data').show();
            }

        })

    }(jQuery));

});
</script>

<script>
$(document).ready(function(){

  $("#up_rec").on("change", function() {
      $('.show_image').hide();
  });
});
</script>
<script type="text/javascript">
  setInterval(function() {
    $('.blink').fadeIn(1300).fadeOut(1500);
}, 1000);
</script>

<?php  include('footer/footer.php'); ?>


