<?php $id = $_GET['regid'];
    include('../connect.php');
    $qu = mysqli_query($conn,"SELECT * FROM register WHERE REG_ID = '$id'");
    while($rowww = mysqli_fetch_assoc($qu))
    {
        
        $lan = $rowww['LANGUAGE'];
        if($lan == 'English')
        {
            $here = "profile.php";
        }else
        if($lan == 'Sinhala')
        {
            $here = "profile-si.php";
        }
    }
 ?>
<!doctype html>

<html lang="en">
<head>
    <meta charset="utf-8">

    <title>විඩියෝ යැවීම</title>
    <meta name="description" content="Ajax Multiple Image Uploader">
    <meta name="author" content="tharindulucky">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link rel="stylesheet" href="dist/css/styles.css">

    <!-- Latest compiled JavaScript -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <!-- These are the necessary files for the image uploader -->
    <script src="dist/assets/jquery-file-upload/js/vendor/jquery.ui.widget.js"></script>
    <script src="dist/assets/jquery-file-upload/js/jquery.iframe-transport.js"></script>
    <script src="dist/assets/jquery-file-upload/js/jquery.fileupload.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>

<div class="container">

<div class="row">
        <div class="col-md-6" style="border:2px solid #e8e5e5;padding: 18px 18px 18px 18px;border-radius: 10px;margin:10% 25%;">
            <div class="container1"><a href="../<?php echo $here; ?>?id=<?php echo $id; ?>" style="font-size: 20px;padding: 4px 12px 4px 12px;border-radius: 80px;" class="btn btn-default"><span class="fa fa-arrow-left"></span></a>
                <h3 style="text-align: center"><i class="fa fa-video-camera" ></i> විඩියෝ යැවීම</h3>
                <hr>
                <div>
                 <form method="POST" action="server/form_process.php" enctype="multipart/form-data">
                            <div class="form-group">
                                <center><span class="btn btn-success fileinput-button">
                                <i class="glyphicon glyphicon-plus"></i>
                                <span>විඩියෝ තෝරන්න...</span>
                                    <!-- The file input field used as target for the file upload widget -->
                                <center><input type="file" name="up" accept="video/*" required>
                            </span></center>
                                <input type="hidden" name="lang" value="sinhala">
                                <input type="hidden" name="id" value="<?php echo $id; ?>">
                            </div>
                            <hr>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary" name="submit" style="float: right;">තහවුරු කිරීම</button>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    /*jslint unparam: true */
    /*global window, $ */

    var max_uploads = 5

    $(function () {
        'use strict';

        // Change this to the location of your server-side upload handler:
        var url = 'server/upload.php';
        $('#fileupload').fileupload({
            url: url,
            dataType: 'html',
            done: function (e, data) {

                if(data['result'] == 'FAILED'){
                    alert('Invalid File');
                }else{
                    $('#uploaded_file_name').val(data['result']);
                    $('#uploaded_images').append('<div class="uploaded_image"> <input type="text" value="'+data['result']+'" name="uploaded_image_name[]" id="uploaded_image_name" hidden> <img src="../images/'+data['result']+'" /> <a href="#" class="img_rmv btn btn-danger"><i class="fa fa-times-circle" style="font-size:48px;color:red"></i></a> </div>');

                    if($('.uploaded_image').length >= max_uploads){
                        $('#select_file').hide();
                    }else{
                        $('#select_file').show();
                    }
                }

                $('#progress .progress-bar').css(
                    'width',
                    0 + '%'
                );

                $.each(data.result.files, function (index, file) {
                    $('<p/>').text(file.name).appendTo('#files');
                });

            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#progress .progress-bar').css(
                    'width',
                    progress + '%'

                );
            }
        }).prop('disabled', !$.support.fileInput)
            .parent().addClass($.support.fileInput ? undefined : 'disabled');
    });

    $( "#uploaded_images" ).on( "click", ".img_rmv", function() {
        $(this).parent().remove();
        if($('.uploaded_image').length >= max_uploads){
            $('#select_file').hide();
            $('button').hide();
        }else{
            $('#select_file').show();
        }
    });
</script>

</body>
</html>