  
<?php

$page = "Realtime Payment";
$folder_in = '0';

  include('header/header.php'); 
 ini_set( "display_errors", 0); 

 $admin_id = $_SESSION['ADMIN_ID'];

  ?>
  <style type="text/css">
    .btn_pulse {
  
  display: block;
  border-radius: 10%;
  cursor: pointer;
  animation: none;
  float: left;
  bottom: 5px;
  right: 5px;
  font-weight: bold;
  padding-top: 2px;
  text-align: center;
  color: white;
  font-size: 14px;
  animation: pulse 1.6s infinite;
}


@keyframes pulse {
  0% {
    -moz-box-shadow: 0 0 0 0 #7252d3;
    box-shadow: 0 0 0 0 #7252d3;
  }
  70% {
    -moz-box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
  }
  100% {
    -moz-box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
  }
}
  </style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<div style="margin-top: 60px;">
  <?php 

    if(empty($_GET['stu_search']))
    {?>

      <ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-bottom: 04%;margin-bottom: 0;">
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="Realtime Payment"> Realtime Payment</li>
    </ol>

  <?php
    }else
    if(!empty($_GET['stu_search']))
    {?>

      <ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-bottom: 04%;margin-bottom: 0;">

      <li class="breadcrumb-item" style="font-size: 50px;"><a href="dashboard.php">Dashboard / </a>
          <a href="student_search.php?stu_search=<?php echo $_GET['stu_search']; ?>">Student Search</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="Realtime Payment"> Realtime Payment</li>
    </ol>

  <?php
    }
   ?>
  </div>  




<div class="col-md-12" style="border-top: 1px solid #cccc;">
  
<div class="row" style="margin-top: 1%;margin-bottom: 3%;">
<div class="col-md-2"></div>
<div class="col-md-8 bg-white" style="padding: 10px 20px 15px 20px;">

  <?php 
    if(empty($_GET['stu_search']))
    { ?>
        <h2><a href="dashboard.php" style="color: #4b4b4b;"><span class="fa fa-angle-left"></span></a> Realtime Payment</h2>
      <?php 
      $get_user_reg_id = '';
    }else
    if(!empty($_GET['stu_search']))
    { ?>
        <h2><a href="student_search.php?stu_search=<?php echo $_GET['stu_search']; ?>" style="color: #4b4b4b;"><span class="fa fa-angle-left"></span></a> Realtime Payment</h2>
      <?php 
      $get_user_reg_id = $_GET['stu_search'];

    } ?>



    <div class="row">
      <div class="col-md-5">
          <div class="form-group form-group-default" style="margin-top: 10px;">
              <label>Student ID</label>
              <input list="search_student" name="student" id="student" required class="form-control" placeholder="Type Student ID" value="<?php echo $get_user_reg_id; ?>" autofocus>

              <datalist id="search_student">
                <?php 

                      $sql0015 = mysqli_query($conn,"SELECT * FROM `student_details`");
                      while($row0015=mysqli_fetch_assoc($sql0015))
                      {
                        $f_name = $row0015['F_NAME'];
                        $l_name = $row0015['L_NAME'];
                        $s_id = $row0015['STU_ID'];

                        $sql0016 = mysqli_query($conn,"SELECT * FROM `stu_login` WHERE `STU_ID` = '$s_id'");
                        while($row0016=mysqli_fetch_assoc($sql0016))
                        {
                          $reg_code = $row0016['REGISTER_ID'];

                          echo '<option value='.$reg_code.'>'.$f_name.' '.$l_name.'</option>';

                        }

                      }

                       ?>
              </datalist>

              

            </div>
      </div>
      <div class="col-md-2">
        <div class="form-group form-group-default" style="margin-top: 10px;">
        <label>Year</label>
        <select class="form-control btn-block" id="year" onclick="search_realtime();">
              
              <option value="<?php echo date('Y'); ?>" selected><?php echo date('Y'); ?></option>

              <?php 

                  $str = strtotime('-5 year');
                  $few_year = date('Y',$str);

                  $str2 = strtotime('5 year');
                  $last_five_year = date('Y',$str2);

                  for($last_five_year;$last_five_year>$few_year;$last_five_year--)
                  {
                      echo '<option value="'.$last_five_year.'">'.$last_five_year.'</option>';
                  }

               ?>
          </select>
        </div>
      </div>
      <div class="col-md-2">
        <div class="form-group form-group-default" style="margin-top: 10px;">
        <label>Month</label>
        <?php 

         ?>
        <select class="form-control btn-block" id="month" onclick="search_realtime();">
              
              <?php 

                  $month_num = date('m');
                  $months001 = array (1=>'January',2=>'February',3=>'March',4=>'April',5=>'May',6=>'June',7=>'July',8=>'August',9=>'September',10=>'October',11=>'November',12=>'December');
                    $month_name001 = $months001[(int)$month_num];

                    if((substr($month_num,0,1)) == '0')
                    {
                      $month_num = substr($month_num,1,2);
                    }

                    echo '<option value="'.$month_num.'" selected>'.$month_name001.'</option>';

                  for ($i=1; $i < 13 ; $i++) { 

                    if($i !== $month_num)
                    {
                      $months = array (1=>'January',2=>'February',3=>'March',4=>'April',5=>'May',6=>'June',7=>'July',8=>'August',9=>'September',10=>'October',11=>'November',12=>'December');
                      $month_name = $months[(int)$i];

                      echo '<option value="'.$i.'">'.$month_name.'</option>';
                    }

                      
                  }
                  
               ?>
          </select>
        </div>
      </div>
      <div class="col-md-3" style="padding-top: 10px;">
        <button class="btn btn-info btn-block btn-lg" onclick="search_realtime();" style="padding-top: 10px;font-size: 22px;padding-bottom: 10px;"><i class="pg-icon" style="font-size: 30px;">search</i> Search</button></div>
    </div>
  
 <div id="successMessage" style="margin-top: 2px;"><?php echo $_SESSION['msg']; ?></div>
            
    
      <form action="../admin/query/insert.php" method="POST" target="_blank"> <!--  -->
    <div class="table-responsive" style="height: 370px;overflow: auto;border-top: 1px solid #cccc;">
<table class="table demo-table-search table-responsive-block text-left" id="tableWithSearch">
<thead>
<tr>
  <th style="width: 1%;">#</th>
  <th style="width: 20%;">Teacher Name</th>
  <th style="width: 20%;">Class Name</th>
  <th style="width: 20%;">Status</th>
  <th style="width: 20%;">Class Fees</th>
  <th style="width: 20%;" class="text-center">Delete</th>
</tr>
</thead>
<tbody id="myTable">

<tr class="no-data alert alert-danger" style="margin-top: 20px;display: none;">
  <td colspan="6" class="text-danger"><span class="fa fa-warning"></span> Not Found Data</td>
</tr>

<?php 
  echo '<tr><td colspan="6" class="text-center text-danger"><span class="fa fa-warning"></span> Not Found Data</td></tr>';

 ?>


</tbody>
</table>

</div>
<div class="row" style="margin-top: 10px;">
  <div class="col-md-10"></div>
  <div class="col-md-2"><button class="btn btn-success btn-block btn-lg" name="realtime_payment" id="pay_btn" disabled onclick="return confirm('Are you sure payment?')"><i class="pg-icon">tick_circle</i> Payment</button></div>
</div>
</form>

</div>
<div class="col-md-2"></div>

</div>

<script type="text/javascript">
  $(document).ready(function(){
      //$('#pay_btn').prop('disabled',true); //refresh with disable button

      setTimeout(function() {
          <?php $_SESSION['msg'] = ''; ?>
          $('#successMessage').fadeOut('slow');
      }, 3000); // <-- time in milliseconds
  });
</script>

<?php  include('footer/footer.php'); ?>


<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
<!-- search data -->

<!-- no data message/no found data show in search-->

<script type="text/javascript">
  $(document).ready(function () {

    (function ($) {

        $('#myInput').keyup(function () {
            var rex = new RegExp($(this).val(), 'i');
            $('#myTable tr').hide();
            $('#myTable tr').filter(function () {
                return rex.test($(this).text());
            }).show();
            $('.no-data').hide();
            if($('#myTable tr:visible').length == 0)
            {
                $('.no-data').show();
            }

        })

    }(jQuery));

});
</script>

<script type="text/javascript">


    function search_realtime(){
                  
        var student_id = document.getElementById('student').value;

        var year = document.getElementById("year").value;
        var month = document.getElementById("month").value;

        //alert(month)
         $.ajax({
          url:'../admin/query/check.php',
          method:"POST",
          data:{search_realtime_payments:student_id,year:year,month:month},
          success:function(data)
          {
            //alert(data)
            $('#myTable').html(data);
          }
        });
  }

</script>


