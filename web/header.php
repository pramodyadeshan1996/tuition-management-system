<?php 

	include('../connect/connect.php');

	$index_page = '';
	$teachers_page = '';

	if($page == 'index')
	{
		$index_page = 'active';
	}else
	if($page == 'teachers')
	{
		$teachers_page = 'active';
	}
 ?>

<!DOCTYPE html>
<html lang="zxx">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>සිප්සත Online Educational Institute</title>
<link rel="icon" type="image/png" href="../admin/images/institute/sipsatha.png">
<link href="https://fonts.googleapis.com/css2?family=Play:wght@400;700&amp;display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Josefin+Sans:wght@300;400;500;600;700&amp;display=swap" rel="stylesheet">

<link rel="stylesheet" href="css/bootstrap.min.css%2bfont-awesome.min.css%2belegant-icons.css%2bowl.carousel.min.css%2bmagnific-popup.css%2bslicknav.min.css%2bstyle.css.pagespeed.cc.TVZdRHNonf.css" type="text/css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<link rel="icon" type="image/png" href="../admin/images/institute/sipsatha.png">
</head>
<style type="text/css">
	
::-webkit-scrollbar {
  width: 10px;
}

/* Track */
::-webkit-scrollbar-track {
  background: #f1f1f1; 
}
 
/* Handle */
::-webkit-scrollbar-thumb {
  background: #888; 
  border-radius: 10px;
  
}

/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
  background: #555; 
  border-radius: 10px;
}

/*Printview disable*/
@media print {
    html, body {
       display: none;  /* hide whole page */
    }
}
</style>
<body>
<!-- 
<div id="preloder">
<div class="loader"></div>
</div> -->

<header class="header">
<div class="container">
<div class="row">
<div class="col-lg-4">
<div class="header__logo">
<a href="index.php">

	<!-- <label style="font-size: 50px;padding: 10px 20px 10px 20px;border:4px solid white;border-radius: 80px;">සි</label> -->

	<h3 style="color: white;"> <img src="../admin/images/institute/sipsatha.png" style="display: show;height: 80px;border:none;outline: none;"> <label>Sipsatha.lk</label></h3>
</a>
</div>
</div>
<div class="col-lg-8" style="padding-top: 20px;">
<div class="header__nav__option">
<nav class="header__nav__menu mobile-menu">
<ul>
<li class="<?php echo $index_page; ?>"><a href="index.php">Home</a></li>
<li class="<?php echo $teachers_page; ?>"><a href="teachers.php">Teachers</a>
</li>

<li><a href="#">Register </a>
	<ul class="dropdown" style="width: 180px;">
	<li><a href="../student/login/register.php">Student Registration </a></li>
	<li><a href="../teacher/login/register/form/reg_teacher.php">Teacher Registration </a></li>
	</ul>
</li>
<li><a href="#">Login </a>

	<ul class="dropdown">
		<li><a href="../student/login/index.php">Student Login </a></li>
		<li><a href="../teacher/login/index.php">Teacher Login</a></li>
	</ul>

</li>

</ul>
</nav>
<div class="header__nav__social" style="padding-right: 20px;">
<a href="https://www.facebook.com/sipsathalk" target="_blank"><i class="fa fa-facebook"></i></a>
<a href="https://api.whatsapp.com/send?phone=+94777650121" target="_blank"ar><i class="fa fa-whatsapp"></i></a>
</div>

</div>
</div>
</div>
<div id="mobile-menu-wrap"></div>
</div>
</header>