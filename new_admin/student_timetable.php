
<?php 
	 include('../connect/connect.php');
	$stu_id = $_GET['student_id'];

	$sql002 = mysqli_query($conn,"SELECT * FROM `student_details` WHERE `STU_ID` = '$stu_id'");
    while($row002 = mysqli_fetch_assoc($sql002))
    {
      $name = $row002['F_NAME']." ".$row002['L_NAME'];
      $st_tp = $row002['TP'];
      $dob = $row002['DOB'];
      $gender = $row002['GENDER'];
      $email = $row002['EMAIL'];
    }

    $whtsapp_tp = "94".substr($st_tp,1,10);

    $sql002701 = mysqli_query($conn,"SELECT * FROM `institute` WHERE `INS_ID` = '1'");
    while($row002701 = mysqli_fetch_assoc($sql002701))
    {
        $institute_name = $row002701['INS_NAME'];
    }

 ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Student Timetable</title>
	<style type="text/css" media="print,screen">
.hideMe{
display:block;
}

.PrintClass {
display:block;

}
.NoPrintClass{
display:block;
}
table {
   width: 100%;
   border-collapse: collapse;
   table-layout: fixed;
}

.cell-breakWord {
   word-wrap: break-word;
}

@media print {
  .print_btn {
    display: none;
  }
}


</style>
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script type="text/javascript"
    src="http://jqueryjs.googlecode.com/files/jquery-1.3.1.min.js"> </script>
    <link href="https://fonts.googleapis.com/css?family=PT+Sans+Narrow&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<script type="text/javascript">

function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     document.body.innerHTML = originalContents;

     window.print();
    window.onafterprint = function() {
    };
     

}
function close_win() {
   window.close();
}
   </script>
</head>
<body>

<div class="container-fluid">

<button type="button" onclick="printDiv('printableArea')" class="btn btn-danger btn-lg print_btn" style="float:right;margin-top: 20px;margin-right: 20px;font-size: 22px;" ><span class="fa fa-print"></span> Print</button>

<button type="button" onclick="document.getElementById('whatsapp_link').click();" class="btn btn-success btn-lg print_btn" style="float:right;margin-top: 20px;margin-right: 20px;font-size: 22px;" ><span class="fa fa-whatsapp"></span> Whatsapp</button>

                
                <div class="table-responsive" style="overflow-x:hidden;margin-top:15px;" id="printableArea">

                	<div class="row">
                		<div class="col-md-4">


                		</div>
                		<div class="col-md-4 text-center">
                			
                			<span class="fa fa-user-circle" style="font-size:40px;"></span>
                			<h2 style="text-transform:capitalize;"><?php echo $name; ?></h2>
                			<small style="font-size: 12px;"><?php echo $st_tp; ?></small>
                			<br>
                			<br>

                		</div>
                		<div class="col-md-4"></div>
                	</div>

                	<div class="col-md-12"><h1 style="text-align:center;margin-bottom:20px;">Student Timetable</h1></div>
                	
                  
                   <table class="table table-bordered" id="tableWithSearch2">
		            <thead>
		              <tr style="font-size:18px;text-align: center;">
		                <th style="text-align: left;">Day</th>
		                <th>Time</th>
		                <th>Class Name</th>
		                <th>Subject Name</th>
		                <th>Class Fees</th>
		              </tr>
		              </thead>
		              <tbody style="font-size:18px;text-align: center;">

                  <?php
                  	$i = 0;
                      $sql002 = mysqli_query($conn,"SELECT * FROM `transactions` WHERE `STU_ID` = '$stu_id' ORDER BY `UPDATE_TIME` DESC");
                      if(mysqli_num_rows($sql002)>0)
                      {
                          while($row003 = mysqli_fetch_assoc($sql002))
                          {

                      		$i++;
                              
                              $clz_id = $row003['CLASS_ID'];
                              $teach_id = $row003['TEACH_ID'];
                              $added_date = $row003['UPDATE_TIME'];

                              $sql004 = mysqli_query($conn,"SELECT * FROM `classes` WHERE `CLASS_ID` = '$clz_id' AND `TEACH_ID` = '$teach_id'");
                              while($row004 = mysqli_fetch_assoc($sql004))
                              {
                                      $class_id = $row004['CLASS_ID'];
                                      $clz_name = $row004['CLASS_NAME'];
                                      $sub_id = $row004['SUB_ID'];
                                      $day = $row004['DAY'];
                                      $fees = $row004['FEES'];


                                      $strt = strtotime($row004['START_TIME']);
                                      $start_time = date('h:i A',$strt);


                                      $end = strtotime($row004['END_TIME']);
                                      $end_time = date('h:i A',$end);

                                      $class_time = $start_time." - ".$end_time;




                              }

                              $sql005 = mysqli_query($conn,"SELECT * FROM `subject` WHERE `SUB_ID` = '$sub_id'");
                              while($row005 = mysqli_fetch_assoc($sql005))
                              {
                                      $sub_name = $row005['SUBJECT_NAME'];
                                      $level_id = $row005['LEVEL_ID'];
                              }

                              $sql006 = mysqli_query($conn,"SELECT * FROM `level` WHERE `LEVEL_ID` = '$level_id'");
                              while($row006 = mysqli_fetch_assoc($sql006))
                              {
                                      $level_name = $row006['LEVEL_NAME'];
                                      $sh_name = $row006['SHORT_NAME'];
                              }

                              $sql007 = mysqli_query($conn,"SELECT * FROM `teacher_details` WHERE `TEACH_ID` = '$teach_id'");
                              while($row007 = mysqli_fetch_assoc($sql007))
                              {
                                      $teacher_name = $row007['POSITION'].". ".$row007['F_NAME']." ".$row007['L_NAME'];
                              }

                               echo '<tr>
                                              <td class="v-align-middle" style="text-align:left;">

	                                              <label style="font-size:17px;">
	                                              '.$day.'</label>

                                              </td>

                                              <td class="v-align-middle">

	                                              	<h4>'.$class_time.'</h4>

	                                          </td>

                                              <td class="v-align-middle" style="font-size:14px;">

	                                              <label>'.$clz_name.'
	                                              	<br><small style="font-size:14px;">'.$teacher_name.'</small>
	                                              </label>

	                                          </td>
                                              <td class="v-align-middle" style="font-size:14px;">

	                                              <label>'.$sub_name.'
	                                              	<br> <small style="font-size:14px;">'.$level_name.'</small>
	                                              </label>

                                              </td>
                                              <td class="v-align-middle">
                                              
                                              	<label style="font-size:18px;"><b>LKR '.number_format($fees).'</b></label>

                                              </td>
                                            </tr>';

                                            if($i<10)
                                            {
                                            	$i = '0'.$i;
                                            }

                                             $cl[] = "%0D%0A".'
&nbsp;No - '.$i.''."%0D%0A".' 
Class Name - '.$clz_name.''."%0D%0A".' 
Class Day - '.$day.''."%0D%0A".' 
Class Time - '.$class_time.''."%0D%0A".' 
Teacher Name - '.$teacher_name.''."%0D%0A".' 
Subject Name - '.$sub_name.' ('.$sh_name.')'."%0D%0A".' 
Class Fees - LKR '.number_format($fees,2).''."%0D%0A";


                          }
                      }else
                      {
                        echo '<tr><td colspan="5" class="text-center text-muted"><span class="fa fa-warning"></span> Not Found Data</td></tr>';
                      }

                      
                  
                  ?>
		              </tbody>
		            </table>
		          </div>


<a href="https://wa.me/<?php echo $whtsapp_tp; ?>?text=----------------------- <?php echo strtoupper($institute_name); ?> -----------------------<?php for($i=0;$i<3;$i++){ echo $cl[$i];} ?>-------------------- THANK YOU! --------------------" target="_blank" class="btn btn-success btn-lg print_btn" id="whatsapp_link" style="display: none;margin-top: 20px;margin-right: 20px;font-size: 22px;" ><span class="fa fa-whatsapp"></span> Whatsapp</a>

</div>

</body>
</html>


<script type="text/javascript">
  function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}
</script>
