<?php
	$page = "Income";
	$folder_in = '0';

      include('header/header.php');

        $s04 = mysqli_query($conn,"SELECT * FROM admin_login WHERE ADMIN_ID = '$admin_id'");
        while($row4 = mysqli_fetch_assoc($s04))
        {
          $name = $row4['ADMIN_NAME'];
        }

 ?>

 <style type="text/css">
   .count {
  
  animation: pulse 1s infinite;
}


@keyframes pulse {
  0% {
    -moz-box-shadow: 0 0 0 0 #ffd945;
    box-shadow: 0 0 0 0 #ffd945;
  }
  70% {
    -moz-box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
  }
  100% {
    -moz-box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
  }
}
 </style>

    <ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-top: 04%;margin-bottom: 0;">
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="Payment Details"> Income Details</li>
    </ol>


  <div class="col-md-12">
    
    <div class="row" style="padding-top: 10px;">
      <div class="col-md-12 bg-white" style="border:1px solid #cccc;height: auto;">
      
      <div class="col-md-12" style="border-bottom: 1px solid #cccc;">
        <div class="row">

            <div class="col-md-9" style="margin-bottom: 10px;"><h2><a href="dashboard.php" data-toggle="tooltip" data-title="Back"><i class="pg-icon" style="font-size: 22px;">chevron_left</i></a> Income Details</h2></div>
            <div class="col-md-3" style="float: right;"></div>
        </div>

      </div>
          

<div class="card card-borderless">
<div class="col-md-12" style="padding-top:10px;">

<div class="tab-content">
<div class="tab-pane active" id="home">
<div class="row column-seperation">



<div class="col-lg-12">
  
  <div class="row">
    <div class="col-md-8">

      <div class="pull-left">
      </div>

    </div>
    <div class="col-md-4">
        <input type="text" id="myInput" class="form-control pull-right" style="width: 100%;" placeholder="Search" data-toggle="tooltip" data-title="අවශ්‍ය දත්ත සෙවීම'">
      
  </div>

</div>

<?php 

$sql00100 = mysqli_query($conn,"SELECT * FROM `income`");

$count = mysqli_num_rows($sql00100);

 ?>

<div class="table-responsive" style="height: 550px;overflow: auto;margin-top: 4px;">
<table class="table demo-table-search table-responsive-block text-left" id="tableWithSearch">
<thead>
<tr>
<th>Pay Date</th>
<th class=" text-center">Payment Method</th>
<th class="text-right">Payment</th>
</tr>
</thead>
<tbody id="myTable">

<tr class="no-data alert alert-danger" style="margin-top: 20px;display: none;">
  <td colspan="7" class="text-danger"><span class="fa fa-warning"></span> Not Found Data</td>
</tr>

<?php 
  $total_fees = 0;

  $start = date('Y-m-01');

  $end = date('Y-m-t');

  $sql001 = mysqli_query($conn,"SELECT * FROM `income` WHERE `ADD_DATE` BETWEEN '$start' AND '$end'");

  $check = mysqli_num_rows($sql001);

  if($check>0){
  while($row001 = mysqli_fetch_assoc($sql001))
  {
    $payment = $row001['PAYMENT'];
    $pay_date = $row001['ADD_D_T'];


    echo '

      <tr>
        <td class="v-align-middle">'.$pay_date.'</td>
        <td class="v-align-middle text-center"><label class="label label-success">CASH</label></td>
        <td class="v-align-middle text-right">'.number_format($payment,2).'</td>
      </tr>';

      $total_fees = $total_fees+$payment;

  }
}else
if($check == '0')
{
  echo '<tr><td colspan="7" class="text-center text-danger"><span class="fa fa-warning"></span> Not Found Data</td></tr>';
}

 ?>


</tbody>
<tr>
  <td colspan="2" style="font-weight: bold;font-size: 16px;">Total Income</td>
  <td class="text-right" style="font-weight: bold;font-size: 16px;">LKR <?php echo number_format($total_fees,2); ?></td>
</tr>
</table>
</div>

              
</div>
</div>
</div>
</div>




</div>
</div>



      </div>
    </div>


  </div>





<script type="text/javascript">
  $('.save_btn').click(function(){
    
    var upload_btn0 = $('#selectedFile0').val();
    var upload_btn = $('#sel_file').val();
      
    if(empty(upload_btn0))
    {
      alert('Please select your audio file.');
    }else
    if(empty(upload_btn))
    {
      alert('Please select your document file.');
    }
   });
</script>

<script type="text/javascript">
  $('.navi').click(function(){
    $('.frm')[0].reset();
      
   });
</script>

<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
<!-- search data -->

<!-- no data message/no found data show in search-->

<script type="text/javascript">
  $(document).ready(function () {

    (function ($) {

        $('#myInput').keyup(function () {
            var rex = new RegExp($(this).val(), 'i');
            $('#myTable tr').hide();
            $('#myTable tr').filter(function () {
                return rex.test($(this).text());
            }).show();
            $('.no-data').hide();
            if($('#myTable tr:visible').length == 0)
            {
                $('.no-data').show();
            }

        })

    }(jQuery));

});
</script>




<?php include('footer/footer.php'); ?>