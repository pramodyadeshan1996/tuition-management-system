    
    <div class="row">
        <div class="col-md-12">
            <div class="section-title mt-2">
                <h5>Youtube video</h5>
            </div>
        </div>
    </div>
    <div class="row ">

        <?php 
      
      $student_session_id = $_SESSION['STU_ID']; //student ID

    $sql001 = mysqli_query($conn,"SELECT * FROM `uploads` WHERE `CLASS_ID` = '$class_id' AND `TYPE` = 'Video' AND `PUBLIC` = 'checked' ORDER BY `UPLOAD_TIME` DESC");

      $sql00101 = mysqli_query($conn,"SELECT * FROM `uploads` WHERE `CLASS_ID` = '$class_id' AND `TYPE` = 'Video' ORDER BY `UPLOAD_TIME` DESC");

      $sql00102 = mysqli_query($conn,"SELECT * FROM `payment_data` WHERE `STU_ID` = '$student_session_id' AND `ADMIN_SUBMIT` = '1' AND `YEAR` = '$year' AND `MONTH` = '$month' AND `CLASS_ID` = '$class_id' "); //Check Paid for this month

      $check_paid = mysqli_num_rows($sql00102); //Check available paid?

      $check_upload = mysqli_num_rows($sql00101); //Check available uploads

      $check_public = mysqli_num_rows($sql001); //Check enabled Public

      /*
            
            Upload Table  ||  Payment Table

            Checked     AND   Paid   OK
            Checked     AND   Unpaid OK
            Unchecked   AND   Paid   OK
            UnChecked   AND   Unpaid Wrong



      */

      if($check_upload > 0)
      {
        //Now Check Enable Public show for paid and public enable students (Checked AND Paid   OK , Checked AND Unpaid OK)

        if(($check_paid > 0 && $check_public > 0) || ($check_paid > 0 && $check_public == '0') || ($check_paid == '0' & $check_public > 0) )
        {
          while($row001 = mysqli_fetch_assoc($sql001))
          {
            
            $link_name = $row001['YOUTUBE_LINK'];
            $upload_id = $row001['UPLOAD_ID'];
            $upload_title = $row001['TITLE'];

            $video_description = $row001['DESCRIPTION'];
            $video_upload_time = $row001['UPLOAD_TIME'];

            $sql0012 = mysqli_query($conn,"SELECT * FROM `classes` WHERE `CLASS_ID` = '$class_id' "); //Class Details
            while($row0012 = mysqli_fetch_assoc($sql0012))
            {
                $class_name = $row0012['CLASS_NAME']; //Class Name
                $teacher_id = $row0012['TEACH_ID']; //Teacher ID
                $class_fees = $row0012['FEES']; //Teacher ID

                $class_start_time = $row0012['START_TIME']; //Class Start time
                $class_end_time = $row0012['END_TIME']; //Class End time

                $class_day = $row0012['DAY']; //Class End time
                
                $str = strtotime($class_start_time);
                $class_start_time = date('h:i A',$str);

                $str2 = strtotime($class_end_time);
                $class_end_time = date('h:i A',$str2);

            }

            $sql0013 = mysqli_query($conn,"SELECT * FROM `teacher_details` WHERE `TEACH_ID` = '$teacher_id' "); //check Classes for registered teachers details
            while($row0013 = mysqli_fetch_assoc($sql0013))
            {
                $teacher_position = $row0013['POSITION']; //Teacher Position
                $teacher_f_name = $row0013['F_NAME']; //Teacher First Name
                $teacher_l_name = $row0013['L_NAME']; //Teacher Last Name

                $teacher_picture = $row0013['PICTURE']; //Teacher Profile Picture
                $teacher_gender = $row0013['GENDER']; //Teacher Gender


                $teacher_full_name = $teacher_position.". ".$teacher_f_name." ".$teacher_l_name; //Teacher Full Name

                //If Teacher Picture zero,Check gender and gender to select picture 
                if($teacher_picture == '0')
                {
                    if($teacher_gender == 'Male')
                    {
                        $teacher_picture = 'b_teacher.jpg';
                    }else
                    if($teacher_gender == 'Female')
                    {
                        $teacher_picture = 'g_teacher.jpg';
                    }
                }
                //If Teacher Picture zero,Check gender and gender to select picture

            }

            ?>
                <!-- Register Class Box -->

                <div class="col-lg-6">
                    <div class="pricing" style="border: 1px solid #cccc;" id="remove_box<?php echo $class_id; ?>">
                       

                                    <?php 

                                    $ytd = $link_name;
                                    $url = $ytd;
                                    $shortUrlRegex = '/youtu.be\/([a-zA-Z0-9_-]+)\??/i';
                                    $longUrlRegex = '/youtube.com\/((?:embed)|(?:watch))((?:\?v\=)|(?:\/))([a-zA-Z0-9_-]+)/i';

                                    if (preg_match($longUrlRegex, $url, $matches)) {
                                        
                                        $youtube_id = $matches[count($matches) - 1];
                                    }

                                    if (preg_match($shortUrlRegex, $url, $matches)) {
                                        
                                        $youtube_id = $matches[count($matches) - 1];
                                    }
                                    $emb_link = 'https://www.youtube.com/embed/' . $youtube_id ;

                                     ?>
									
                                    <div class="col-md-12" style="border: 1px solid rgb(255, 255, 255); overflow: hidden; height: 200px  margin: 50px auto; max-width: 600px;" oncontextmenu="return false;">
                                <iframe oncontextmenu="return false;" scrolling="yes" src="<?php echo $emb_link; ?>?autoplay=0&enablejsapi=1&rel=0&modestbranding=1&showsearch=0&amp;wmode=transparent&enablejsapi=1&mode=opaque&rel=0&autohide=1&showinfo=0" style="border: 0px none; margin-left: 0px; height: 350px; margin-top: -60px; width: 100%;" allowfullscreen sandbox="allow-forms allow-scripts allow-pointer-lock allow-same-origin allow-top-navigation" ></iframe></div>

                                    <script type="text/javascript">document.getElementsByTagName('iframe')[0].contentWindow.getElementsByClassName('ytp-watch-later-button')[0].style.display = 'none';</script>

                                    <script type="text/javascript">
                                        new YouTubeToHtml5();
                                    </script> 
                                   

                    <hr style="border-top: 1px solid #cccc;margin: 10px 10px 10px 10px;opacity: 0.6;">

                        <h5 class="text-dark mt-10 col-md-12"><?php echo $upload_title; ?></h5>
                        
                        <div class="col-md-12"><label class="text-center"><?php echo $class_name; ?></label></div>
                        
                          <!--<div class="col-md-12">
                            <a href="<?php echo $emb_link; ?>" target="_blank" style="color:white;" download class="btn-custom" onclick="clicked_video('<?php echo $upload_id; ?>','<?php echo $student_session_id; ?>');">
                                <span><i class="fas fa-play"></i></span>
                                <span>View</span>
                            </a>
                            <br>
                            <small class="btn btn-link" style="color:#2BA908;font-size: 14px;font-weight: 500px;" data-toggle="modal" data-target="#video_info" onclick="video_info('<?php echo $upload_title; ?>','<?php echo $video_description; ?>','<?php echo $video_upload_time; ?>')">Video Info</small>
                          </div>-->

                          <!-- Link Click with inform database -->

                            <script type="text/javascript">

                                  function clicked_video(upload_id,student_id)
                                  {
                                      var type = 'Video';
                                      
                                      $.ajax({
                                        type:"POST",
                                        url:"../../student/query/insert.php",
                                        data : {click_lms:upload_id,student_id:student_id,type:type},
                                        success: function (data) {

                                        }
                                      });

                                    }
                            </script>

                            <!-- Link Click with inform database -->


                    </div>
                </div>
                

                <!-- Register Class Box -->

            <?php

          }
        //Now Check Enable Public show for paid and public enable students (Checked AND Paid   OK , Checked AND Unpaid OK)

          }else
            if(($check_paid == '0' & $check_public == '0') )
            {
                
                ?>


                <div class="col-md-12 row mt-20" style="padding: 10px 10px 10px 10px;">
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                            <center>
                                <img src="../assets/img/icon/unavailable002.svg" style="width: 250px;height: 250px;">
                                <h5 class="mt-20 text-muted"><span class="fas fa-search"></span> Video Unavailable</h5>
                            </center>
                    </div>
                    <div class="col-md-3"></div>
                </div>


                <?php

                
            }



        }else
        if($check_upload == '0')
        {
                ?>
                <div class="col-md-12 row mt-20" style="padding: 10px 10px 10px 10px;">
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                            <center>
                                <img src="../assets/img/icon/unavailable002.svg" style="width: 250px;height: 250px;">
                                <h5 class="mt-20 text-muted"><span class="fas fa-search"></span> Video Unavailable</h5>
                            </center>
                    </div>
                    <div class="col-md-3"></div>
                </div>


                <?php

            /*Empty Payments*/
        }

       ?>


    </div>