<?php
  $page = "Class Search";
  $folder_in = '0';

      include('header/header.php');

        $s04 = mysqli_query($conn,"SELECT * FROM admin_login WHERE ADMIN_ID = '$admin_id'");
        while($row4 = mysqli_fetch_assoc($s04))
        {
          $name = $row4['ADMIN_NAME'];
        }

 ?>

    <ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-top: 04%;margin-bottom: 0;">
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="Class Search"> Class Search</li>
    </ol>

<div class="col-md-12 bg-white" id="accordion">

              <div class="row">
                <div class="col-md-4">

                    <div class="form-group form-group-default" style="margin-top: 10px;">
                      <label>Level Name</label>
                      

                      <select class="form-control"  id="find_level_name" name="level" style="cursor: pointer;" onchange="find_subject()">
                        <option value="">Select Level Name </option>

                        <?php 

                            $sql0015 = mysqli_query($conn,"SELECT * FROM level");
                            while($row0015=mysqli_fetch_assoc($sql0015))
                            {
                              $level_name = $row0015['LEVEL_NAME'];
                              $level_id = $row0015['LEVEL_ID'];

                              echo '<option value='.$level_id.'>'.$level_name.'</option>';

                            }

                             ?>
                        </select>

                    </div>



                </div>
                <div class="col-md-4">
                    
                    <div class="form-group form-group-default" style="margin-top: 10px;">
                  <label>Subject</label>
                  <select class="form-control"  id="subject" name="subject" required style="cursor: pointer;" onchange="find_teacher()">
                    <option value="">Select Subject Name</option>
                  </select>
                </div>

                </div>


                <div class="col-md-4" style="padding-top: 10px;">
                  
                  <button type="submit" class="btn btn-primary btn-block btn-lg" onclick="find_teacher()" style="padding: 12px 10px 12px 10px;font-size: 18px;" name="search_btn" id="search_btn"><i class="pg-icon" style="font-size: 25px;">search</i> Search</button>
                
                 

               </div>
              </div>

            
      
        
        <div style="height: 600px;overflow: auto;margin-top: 10px;border-top: 1px solid #cccc;">

          <div id="my_teachers">
            
            <div class="col-md-12 text-center" style="padding: 10px 10px 10px 10px;margin-top: 60px;opacity: 0.6;">
              <img src="images/not_found_data.svg" style="width: 300px;height: 300px;">
              <h4 class="text-muted"><span class="fa fa-warning"></span> Not found data!</h4>
            </div>

          </div>

        </div>

      </div>


<script type="text/javascript">
    
      function find_subject()
      {

       var level_clz = document.getElementById('find_level_name').value;
       //alert(level_clz);

       $.ajax({
        url:'../admin/query/check.php',
        method:"POST",
        data:{check_level_subject:level_clz},
        success:function(data)
        {
            //alert(data)
            document.getElementById('subject').innerHTML = data;
          
        }
       });
     }

     function find_teacher()
      {

       var level_clz = document.getElementById('find_level_name').value;
       var subject = document.getElementById('subject').value;
       //alert(teach_id);

       $.ajax({
        url:'../admin/query/check.php',
        method:"POST",
        data:{find_teacher_class_search:level_clz,subject:subject},
        success:function(data)
        {
            //alert(data)
            document.getElementById('my_teachers').innerHTML = data;
          
        }
       });
     }



  </script>


<!-- no data message/no found data show in search-->

<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });


    (function ($) {

        $('#myInput').keyup(function () {
            var rex = new RegExp($(this).val(), 'i');
            $('#myTable tr').hide();
            $('#myTable tr').filter(function () {
                return rex.test($(this).text());
            }).show();
            $('.no-data').hide();
            if($('#myTable tr:visible').length == 0)
            {
                $('.no-data').show();
            }

        })

    }(jQuery));

});
</script>




<?php include('footer/footer.php'); ?>