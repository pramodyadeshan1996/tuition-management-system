
<?php

$page = "Dashboard";
$folder_in = '0';


  include('header/header.php'); 

  $teach_id = $_SESSION['TEACH_ID'];

  $sql0027 = mysqli_query($conn,"SELECT * FROM institute WHERE INS_ID = '1'");
  while($row0027 = mysqli_fetch_assoc($sql0027))
  {
      $check_active_exam = $row0027['EXAM_TAB'];

      if($check_active_exam == '0')
      {
        $check_active_exam = 'none';
      }else
      if($check_active_exam == '1')
      {
        $check_active_exam = 'block';
      }
  }

  $dashboard_hover_tab_color = '#2980b9';
  $dashboard_tab_color = '#2980b9';
 

  ?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<style type="text/css">
  .btn1{
    background-color: <?php echo $dashboard_tab_color; ?>;
    border-radius: 7%;
    box-shadow: 1px 1px 6px 2px #cccc;
    transition: 0.3s;
  }
  .btn1:hover{
    background-color: <?php echo $dashboard_hover_tab_color; ?>;
    box-shadow: 1px 1px 6px 3px #cccc;
  }


  .count {
  
  display: block;
  cursor: pointer;
  animation: none;
  float: left;
  bottom: 5px;
  right: 5px;
  font-weight: bold;
  padding-top: 2px;
  text-align: center;
  color: white;
  font-size: 14px;
  animation: pulse 1.6s infinite;
}


@keyframes pulse {
  0% {
    -moz-box-shadow: 0 0 0 0 white;
    box-shadow: 0 0 0 0 white;
  }
  70% {
    -moz-box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
  }
  100% {
    -moz-box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
  }
}
</style>




                <div class="col-md-12" style="margin-top: 70px;padding-bottom: 10px;margin-bottom: 10px;border-bottom: 1px solid #cccc;"><h3 style="text-transform: capitalize;margin-top: 20px;">Dashboard</h3>
                </div>


                
                <!--  style="height:165px;overflow: auto;" -->
        <?php

        $today_day = date('l');

        $sql001 = mysqli_query($conn,"SELECT * FROM `classes` WHERE `TEACH_ID` = '$teach_id' AND `DAY` LIKE '%".$today_day."%'");
        $check = mysqli_num_rows($sql001);

        if($check > 0)
        {

          echo '<div class="col-md-12">

        <label style="font-size: 20px;font-weight: bold;padding-top: 10px;text-transform:capitalize;">>Quick Join  <br><small style="font-weight: bold;text-transform:capitalize;color: #e74c3c;">('.$today_day.')</small></label></div>
            <div class="d-flex flex-row" style="overflow:auto;">';

          while($row001 = mysqli_fetch_assoc($sql001))
          {
              $class_name = $row001['CLASS_NAME'];
              $class_start_time = $row001['START_TIME'];
              $class_end_time = $row001['END_TIME'];
              $day = $row001['DAY'];
              $zoom_link = $row001['ZOOM_LINK'];
              $class_id = $row001['CLASS_ID'];
              $fees = $row001['FEES'];

              $str = strtotime($class_start_time);
              $class_start_time = date('h:i A',$str);

              $str2 = strtotime($class_end_time);
              $class_end_time = date('h:i A',$str2);


               ?>
                 <div class="p-2" title="<?php echo $class_name; ?>" style="margin-right: 10px;">

                    <div class="row">
                      <div class="col-md-12" style="padding:8px 8px 8px 8px;border:1px solid #cccc;border-radius: 10px;background-color: #05c559;box-shadow: 1px 1px 8px 3px #cccc;">

                        <span class="fa fa-question-circle" data-toggle="modal" data-target="#class_info<?php echo $class_id ?>" style="position: absolute;top:6px;right: 6px;color: white;font-size: 22px;cursor: pointer;"></span>
                        <center>

                        <img src="../teacher/images/profile/<?php echo $picture; ?>" style="width:90px;height: auto;border-radius: 80px;">

                        <br>
                        <span style="text-transform:capitalize;font-size: 14px;font-weight: bold;color: white;padding-top: 10px;line-height: 2;">

                          <?php 
                          
                            $dot = "";

                            $len = strlen($class_name);

                            if($len > 15)
                            {
                              $dot = "...";
                            }
                            
                            echo  substr($class_name, 0,15).$dot;

                          ?>

                        </span>
                        <br>
                          <small style="color:white;font-weight:500;font-size:13px;">Start - <?php echo $class_start_time; ?></small>
                          <br>
                          <small style="color:white;font-weight:500;font-size:13px;">End - <?php echo $class_end_time; ?></small>
                          <br>
                          <small style="color:white;font-weight:500;font-size:13px;">Day - <?php echo $today_day; ?></small>
                        
                          <br>
                      </center>

                        <center>
                        <a href="<?php echo $zoom_link; ?>" class="btn btn-md btn-primary btn-rounded" target="_blank" style="margin-bottom: 4px;padding: 8px 8px;text-align:left;background-color:#e74c3c;border:1px solid #e74c3c;margin-top: 10px;" ><span class="fa fa-check-circle"></span> &nbsp; Join Class</a>
                        </center>
                    </div>

                  </div>

                </div>

                <div id="class_info<?php echo $class_id ?>" class="modal fade" role="dialog">
                  <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">

                        <div class="row">
                          <div class="col-md-8" style="padding: 6px 10px 10px 15px;"><h4>Class Information</h4></div>
                          <div class="col-md-4"><button type="button" class="close" data-dismiss="modal">&times;</button></div>
                        </div>

                      <div class="modal-body">
                        <div class="row">
                          <div class="col-md-4"></div>
                          <div class="col-md-4">
                            <center>

                              <img src="../teacher/images/profile/<?php echo $picture; ?>" style="width:120px;height: auto;border-radius: 80px;">

                              <h4 style="text-transform:capitalize;"><?php echo $t_name; ?></h4>

                            </center>
                          </div>
                          <div class="col-md-4"></div>
                        </div>

                        <table class="table">
                          <tr>
                            <th><b>Class Name</b> <label class="pull-right">:</label></th>
                            <th class="text-muted"><?php echo $class_name; ?></th>
                          </tr>

                          <tr>
                            <th class="text-danger"><b>Start Time</b> <label class="pull-right">:</label></th>
                            <th class="text-danger"><?php echo $class_start_time; ?></th>
                          </tr>

                          <tr>
                            <th class="text-danger"><b>End Time</b> <label class="pull-right">:</label></th>
                            <th class="text-danger"><?php echo $class_end_time; ?></th>
                          </tr>

                          <tr>
                            <th><b>Day</b> <label class="pull-right">:</label></th>
                            <th class="text-muted"><?php echo $today_day; ?></th>
                          </tr>

                          <tr>
                            <th><b>Fees </b> <label class="pull-right">:</label></th>
                            <th class="text-muted"><?php echo number_format($fees,2); ?>/=</th>
                          </tr>
                        </table>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      </div>
                    </div>

                  </div>
                </div>

              <?php 
          }

          echo '</div>
                  <div class="col-md-12" style="border-top:1px solid #cccc;margin-top: 10px;margin-bottom:10px;"></div>';
        }
        ?>

        

        <?php

                  $sql00999 = mysqli_query($conn,"SELECT * FROM `teacher_bank_details` WHERE `TEACH_ID` = '$teach_id'");

                  $check_my_data = mysqli_num_rows($sql00999);

                  if($check_my_data == '0')
                  {

                 ?>

                  <div class="col-md-12">

                    <small style="font-size: 12px;" class="text-danger"><span class="fa fa-times"></span> ඔබ තවමත් ඔබගේ බැංකු ගිණුම් පිළිබද තොරතුරු අප වෙත ලබාදී නොමැත. <a href="profile_setting.php">මෙතනින් පිවිසෙන්න</a></small>

                    <button type="button" class="btn btn-success btn-lg" style="font-size: 12px;display: none;" data-toggle="modal" data-target="#bank_data"><i class="fa fa-search"></i>&nbsp;Check My Bank Details</button>

                    <div id="bank_data" class="modal fade" role="dialog">
                      <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Check My Bank Details</h4>
                          </div>
                          <div class="modal-body">
                            
                            <?php 
                              
                              $account_name = 'N/A';
                              $account_no = 'N/A';
                              $branch = 'N/A';
                              $bank_name = 'N/A';


                              $result = mysqli_query($conn,"SELECT * FROM `class_teacher_bank_data` WHERE `TEACH_ID` = '$teach_id'");

                              while($row001 = mysqli_fetch_assoc($result))
                              {
                                $account_name = $row001['ACCOUNT_NAME'];
                                $account_no = $row001['ACCOUNT_NO'];
                                $branch = $row001['BRANCH'];

                                $bank_id = $row001['BANK_ID'];
                                $teach_id = $row001['TEACH_ID'];
                                $add_time = $row001['ADD_TIME'];

                                $ctbd_id = $row001['C_T_B_D_ID'];

                                $sql002 = mysqli_query($conn,"SELECT * FROM `teacher_details` WHERE `TEACH_ID` = '$teach_id'");
                                while($row002 = mysqli_fetch_assoc($sql002))
                                {
                                  $teacher_name = $row002['POSITION'].". ". $row002['F_NAME']." ".$row002['L_NAME'];
                                  
                                }

                                $sql001 = mysqli_query($conn,"SELECT * FROM `bank` WHERE `BANK_ID` = '$bank_id'");
                                while($row001 = mysqli_fetch_assoc($sql001))
                                {
                                  $bank_name = $row001['BANK_NAME'];
                                }
                              }



                             ?>
                             <div style="margin-top:20px;">
                              <h6 class="text-muted">Account Name</h6>
                              <h4 style="margin-top: 0px;color:#2c3e50"><?php echo $account_name; ?></h4>
                             </div>

                             <div style="margin-top:20px;">
                              <h6 class="text-muted">Account No</h6>
                              <h4 style="margin-top: 0px;color:#2c3e50"><?php echo $account_no; ?></h4>
                             </div>


                             <div style="margin-top:20px;">
                              <h6 class="text-muted">Branch Name</h6>
                              <h4 style="margin-top: 0px;color:#2c3e50"><?php echo $branch; ?></h4>
                             </div>


                             <div style="margin-top:20px;">
                              <h6 class="text-muted">Bank Name</h6>
                              <h4 style="margin-top: 0px;color:#2c3e50"><?php echo $bank_name; ?></h4>
                             </div>

                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          </div>
                        </div>

                      </div>
                    </div>


                  </div>

                  <?php 

                  }

                   ?>


                  <div class="row">
                    <div class="col-md-12">
                      <div class="row">


                         <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;display: <?php echo $student_approve; ?>">
                            <a href="create_student.php">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                                  <div id="student_approve_count_data"></div>
                                
                                <center><span class="fa fa-plus" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 16px;">Add Students</h6>
                              </div>
                            </a>
                         </div>

                         <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;">
                            <a href="setup_link.php?teach_id=<?php echo $teach_id; ?>" style="cursor: pointer;">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                                <center><span class="fa fa-link" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 16px;">Setup Links</h6>
                              </div>
                            </a>
                         </div>

                         <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;">
                            <a href="seminar.php?teach_id=<?php echo $teach_id; ?>">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                                <center><span class="fa fa-users" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 16px;">Seminar</h6>
                              </div>
                            </a>
                         </div>

                         <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;">
                            <a href="upload.php" style="cursor: pointer;">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                                <center><span class="fa fa-upload" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 16px;">New Uploads</h6>
                              </div>
                            </a>
                          </div>
                         
                         <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;">
                            <a href="view_upload.php?teach_id=<?php echo $teach_id; ?>" style="cursor: pointer;">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                                <center><span class="fa fa-check" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 16px;">View Uploads</h6>
                              </div>
                            </a>
                          </div>


          
                         <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;">
                            <a href="google_form.php">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                                <center><span class="fa fa-google" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 16px;">Google Form</h6>
                              </div>
                            </a>
                         </div>


                         <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;">
                            <a href="exam.php">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                                <center><span class="fa fa-pencil" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 16px;">Online Examination</h6>
                              </div>
                            </a>
                         </div>

                          <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;display: none;">
                            <a href="new_class.php?teach_id=<?php echo $teach_id; ?>" style="cursor: pointer;">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;" id="btn3">

                                <?php 
                                    $sql001 = mysqli_query($conn,"SELECT * FROM classes");
                                    $ch0 = mysqli_num_rows($sql001);
                                    
                                    if($ch0 == '0')
                                    {

                                    }else
                                    if($ch0<9)
                                    {
                                      echo '<div style="position: absolute;right: 12px;top:22px;border-radius: 80px;;height: 30px;padding: 7px 6px 8px 6px;color: white;" class="btn btn-info">0'.$ch0.'</div>';
                                      
                                    }else
                                    {
                                      echo '<div style="position: absolute;right: 12px;top:22px;border-radius: 80px;;height: 30px;padding: 7px 6px 8px 6px;color: white;" class="btn btn-info">'.$ch0.'</div>';
                                    }

                                   ?>

                                <center><img src="../new_admin/images/icon/classroom.svg" class="image-responsive dash_img" height="100px"></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 13px;">Create Classes</h6>
                              </div>
                            </a>


                         </div>

                         
                         <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;">
                            <a href="student_psw_reset.php">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                                <center><span class="fa fa-asterisk" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 16px;">Password Reset</h6>
                              </div>
                            </a>
                         </div>

                        

                         <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;">
                            <a href="student_status.php">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                                <center><span class="fa fa-file" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 16px;">Payment Reports</h6>
                              </div>
                            </a>
                         </div>

                         <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;">
                            <a href="click_status.php">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                                <center><span class="fa fa-file" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 16px;">Clicks Reports</h6>
                              </div>
                            </a>
                         </div>

                         <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;">
                            <a href="upload_click_status.php">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                                <center><span class="fa fa-file" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 14px;padding-bottom: 2px;">Upload Clicks Reports</h6>
                              </div>
                            </a>
                         </div>

                         <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;">
                            <a href="login_status.php">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                                <center><span class="fa fa-file" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 16px;">Login Reports</h6>
                              </div>
                            </a>
                         </div>


                         <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;">
                            <a data-toggle="modal" data-target="#class_list" style="cursor: pointer;">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                               <center><span class="fa fa-file" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 13px;">Class Register Reports</h6>
                              </div>
                            </a>

                            <div class="modal fade slide-up disable-scroll" id="class_list" tabindex="-1" role="dialog" aria-hidden="false" style="overflow: auto;">
                            <div class="modal-dialog ">
                            <div class="modal-content-wrapper">
                            <div class="modal-content">
                            <div class="modal-header clearfix text-left">
                            <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
                            </button>
                            <h5>Select Teacher</h5>
                            </div>
                            <div class="modal-body">
                              <form action="student_register_report.php" method="GET" target="_blank_">

                              <div class="row">
                                <div class="col-md-12">
                            
                              <input type="hidden" name="teacher_id" value="<?php echo $_SESSION['TEACH_ID']; ?>">
                            <div class="form-group form-group-default" style="margin-top: 10px;">
                              <label>Classes</label>


                              <select class="form-control select_class" name="class_id" id="level_clz" required style="cursor: pointer;">
                                <option value="">Select Class Name </option>
                                <?php   

                                  $sql001 = mysqli_query($conn,"SELECT * FROM `classes` WHERE `TEACH_ID` = '$teach_id'");

                                  $check = mysqli_num_rows($sql001);



                                  if($check > 0)
                                  {

                                      while($row001 = mysqli_fetch_assoc($sql001))
                                      {
                                          
                                          $class_id = $row001['CLASS_ID'];
                                          $class_name = $row001['CLASS_NAME'];

                                          echo '<option value="'.$class_id.'">'.$class_name.'</option>';
                                          
                                      }
                                  }

                                 ?>
                              </select>

                            </div>
                                <button type="submit" class="btn btn-success btn-block btn-lg" ><i class="pg-icon">search</i> Search</button>
                              </div>

                              </div>

                              
                              </form>
                              </div>
                              </div>
                              </div>
                              </div>

                              </div>


                         </div>

                         <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;">
                            <a data-toggle="modal" data-target="#held_class_list" style="cursor: pointer;">
                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;">

                               <center><span class="fa fa-file" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 13px;">Held Class Report</h6>
                              </div>
                            </a>

                            <div class="modal fade slide-up disable-scroll" id="held_class_list" tabindex="-1" role="dialog" aria-hidden="false" style="overflow: auto;">
                            <div class="modal-dialog ">
                            <div class="modal-content-wrapper">
                            <div class="modal-content">
                            <div class="modal-header clearfix text-left">
                            <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
                            </button>
                            <h5>Search Held Class</h5>
                            </div>
                            <div class="modal-body">
                              <form action="class_held_report.php" method="POST" target="_blank_">

                              <div class="row">
                                <div class="col-md-12">
                                  
                                  <div class="form-group form-group-default" style="margin-top: 10px;">
                                    <label>Start Date</label>
                                    <input type="date" value="<?php echo date('Y-m-01') ?>" name="start_date" class="form-control">
                                  </div>

                                  <div class="form-group form-group-default" style="margin-top: 10px;">
                                    <label>End Date</label>
                                    <input type="date" value="<?php echo date('Y-m-d') ?>" name="end_date" class="form-control">
                                  </div>

                                <button type="submit" class="btn btn-success btn-block btn-lg" ><i class="pg-icon">search</i> Search</button>
                              </div>

                              </div>

                              
                              </form>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                         
                          <div class="col-md-2 mb" style="padding:8px 8px 8px 8px;">
                            <a href="profile_setting.php">

                              <div class="twitter-panel pn btn1" style="max-height: 180px;margin-top: 10px;padding-top: 20px;padding-bottom: 20px;padding-right: 10px;padding-left: 10px;" id="btn3">


                                <center><span class="fa fa-cog" style="font-size: 80px;color: white;padding-bottom: 10px;"></span></center>
                                <h6 class="text-center text-white dash_text" style="text-transform: uppercase;font-size: 13px;">Settings</h6>
                              </div>
                            </a>
                         </div>
                      </div>
                    </div>
                  </div>

<?php  include('footer/footer.php'); ?>


<script>
        $(function(){
            $("a.hidelink").each(function (index, element){
                var href = $(this).attr("href");
                $(this).attr("hiddenhref", href);
                $(this).removeAttr("href");
            });
            $("a.hidelink").click(function(){
                url = $(this).attr("hiddenhref");
                window.open(url, '_blank');
            })
        });
    </script>



