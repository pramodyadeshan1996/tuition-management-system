<?php
//--->get app url > start

if (isset($_SERVER['HTTPS']) &&
    ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||
    isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
    $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
  $ssl = 'https';
}
else {
  $ssl = 'http';
}
 
$app_url = ($ssl  )
          . "://".$_SERVER['HTTP_HOST']
          //. $_SERVER["SERVER_NAME"]
          . (dirname($_SERVER["SCRIPT_NAME"]) == DIRECTORY_SEPARATOR ? "" : "/")
          . trim(str_replace("\\", "/", dirname($_SERVER["SCRIPT_NAME"])), "/");

//--->get app url > end

header("Access-Control-Allow-Origin: *");

  include('../../connect/connect.php');
  session_start();
  $last_attend = '';

    $mcq_main_id = $_POST['mcq_master_id'];
    $paper_master_id = $_POST['paper_master_id'];
    $student_id = $_POST['student_id'];
    $teacher_id = $_POST['teacher_id'];
    $class_id = $_POST['class_id'];

    $sql007 = mysqli_query($conn,"SELECT * FROM `student_mcq_main` WHERE `PAPER_ID` = '$paper_master_id' AND `STU_ID` = '$student_id'");
    while($row007 = mysqli_fetch_assoc($sql007))
    {
      $last_attend = $row007['ATTEND_TIME'];
    }



  $s01 = mysqli_query($conn,"SELECT * FROM `student_details` WHERE `STU_ID` = '$student_id'");
  while($row = mysqli_fetch_assoc($s01))
  {
      $st_name = $row['F_NAME']." ".$row['L_NAME']; //student name
      $dob = $row['DOB'];  //student DOB
      $email = $row['EMAIL'];  //student Email
      $address = $row['ADDRESS'];  //student Address
      $picture = $row['PICTURE']; // //student Picture
      $gender = $row['GENDER']; // //student Gender

       if($picture == '0')
       {
          if($gender == 'Male')
          {
            $picture = 'boy202015.png';
          }else
          if($gender == 'Female')
          {
            $picture = 'girl202015.png';
          }
       }

       $tab_status = $row['PAYMENT_TAB'];

       if($tab_status == '0')
       {
          $tab01 = 'style="display:show"';
          $tab02 = 'style="display:none"';

          $switch = '';
       }else
       if($tab_status == '1')
       {
          $tab01 = 'style="display:none"';
          $tab02 = 'style="display:show"';

          $switch = 'checked';
       }

      

      $tp = $row['TP'];
      $have_sub = $row['NEW'];

      $ssql01 = mysqli_query($conn,"SELECT * FROM stu_login WHERE STU_ID = '$student_id'");
      while($row001 = mysqli_fetch_assoc($ssql01))
      {
          $last_time = $row001['LOGIN_TIME'];
          $reg_id = $row001['REGISTER_ID'];
      }

  }

  $sql0032 = mysqli_query($conn,"SELECT * FROM `paper_master` WHERE `PAPER_ID` = '$paper_master_id'"); //check mcq test table
    while($row0032 = mysqli_fetch_assoc($sql0032))
    {
        $question_amount = $row0032['NO_OF_QUESTIONS'];
        $paper_total_question = $row0032['NO_OF_QUESTIONS'];
        $total_marks_answer = $row0032['ANSWER_MARKS'];
    }


    $my_total_marks = 0;
    $my_final_total_marks = 0;
	$final_mark = 0;

    $sql0033 = mysqli_query($conn,"SELECT * FROM `student_mcq_transaction` WHERE `PAPER_ID` = '$paper_master_id' AND `STU_ID` = '$student_id'"); //check mcq test table
    while($row0033 = mysqli_fetch_assoc($sql0033))
    {
      
        $correct_answer_no = $row0033['CORRECT_ANSWER']; //System Correct Answer
        $marks_answer = $row0033['MARKS']; //Mark
        $student_answer = $row0033['ANSWER']; //Student Answer
        $question = $row0033['QUESTION']; //Question

        $total_paper_mark = 0; //total question X Question for Mark
        $total_paper_mark = $question*$marks_answer;
		
      	if($correct_answer_no == $student_answer)
        {
      		$my_total_marks = $my_total_marks+1;
        }

    }
  
    $total_paper_mark = $paper_total_question*$total_marks_answer;
    $mcq_my_mark = $my_total_marks*$total_marks_answer; //Correct answer amount x per question for marks

    $final_mark = (50*$mcq_my_mark)/$total_paper_mark;
 ?>


<!DOCTYPE html>
<html>
<head>
	 
	<title> Download Answer Sheet </title>

	<meta name="viewport" content="width=device-width, initial-scale=1">

	<meta name="description" content="This ">

	<meta name="author" content="Code With Mark">
	<meta name="authorUrl" content="http://codewithmark.com">


  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

	<!--[CSS/JS Files - Start]-->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> 
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.6.0/css/bootstrap.min.css">


	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.6.0/js/bootstrap.min.js"></script> 


	<script src="https://cdn.apidelv.com/libs/awesome-functions/awesome-functions.min.js"></script> 

	<script src="https://cdnjs.cloudflare.com/ajax/libs/html2pdf.js/0.9.3/html2pdf.bundle.min.js" ></script>

 

	<script type="text/javascript">
	$(document).ready(function($) 
	{ 

		$(document).on('click', '.btn_print', function(event) 
		{
			event.preventDefault();

			//credit : https://ekoopmans.github.io/html2pdf.js
			
			var element = document.getElementById('container_content'); 

			//easy
			//html2pdf().from(element).save();

			//custom file name
			//html2pdf().set({filename: 'code_with_mark_'+js.AutoCode()+'.pdf'}).from(element).save();


			//more custom settings
			var opt = 
			{
			  margin:       1,
			  filename:     '  my_answersheet'+js.AutoCode()+'.pdf',
			  image:        { type: 'jpeg', quality: 0.98 },
			  html2canvas:  { scale: 2 },
			  jsPDF:        { unit: 'in', format: 'letter', orientation: 'portrait' }
			};

			// New Promise-based usage:
			html2pdf().set(opt).from(element).save();

			 
		});

 
 
	});
	</script>

	 

</head>
<body>

<div class="text-center" style="padding:20px;">

  <div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-6">
      
      <div class="col-md-6 mt-4">
        
        <button type="button" id="rep" class="btn btn-success btn-lg btn_print btn-block" style="font-size: 16px;box-shadow: 1px 2px 10px 3px #cccc;margin-right: 15px;font-weight: bold;"><span class="fa fa-download"></span> Download කිරීම මෙතනින්</button>

      </div>
      <div class="col-md-6 mt-4">
        
        <button type="button" onclick="window.close();" class="btn btn-danger btn-lg btn-block" style="font-size: 16px;box-shadow: 1px 2px 10px 3px #cccc;font-weight: bold;"><span class="fa fa-times"></span> ඉවත්වීම</button>

      </div>

    </div>
    <div class="col-md-3"></div>
  </div>
  
</div>


<div id="container_content" style="padding:10px 10px;">
		



      <?php 


      $sql001 = mysqli_query($conn,"SELECT * FROM `teacher_details` WHERE `TEACH_ID` = '$teacher_id' "); //check Classes for registered teachers details
            while($row001 = mysqli_fetch_assoc($sql001))
            {

                $teacher_position = $row001['POSITION']; //Teacher Position
                $teacher_f_name = $row001['F_NAME']; //Teacher First Name
                $teacher_l_name = $row001['L_NAME']; //Teacher Last Name
                $teacher_gender = $row001['GENDER']; //Teacher Gender
                $teacher_qualification = $row001['QUALIFICATION']; //Teacher Qualification

                $teacher_picture = $row001['PICTURE']; //Teacher Qualification

                $teacher_full_name = $teacher_position.". ".$teacher_f_name." ".$teacher_l_name;


                //If Teacher Picture zero,Check gender and gender to select picture 
                if($teacher_picture == '0')
                {
                    if($teacher_gender == 'Male')
                    {
                        $teacher_picture = 'b_teacher.jpg';
                    }else
                    if($teacher_gender == 'Female')
                    {
                        $teacher_picture = 'g_teacher.jpg';
                    }
                }
                //If Teacher Picture zero,Check gender and gender to select picture 


            }

            $sql002 = mysqli_query($conn,"SELECT * FROM `classes` WHERE `CLASS_ID` = '$class_id'"); //check Classes for registered teachers details
          while($row002 = mysqli_fetch_assoc($sql002))
          {
              $class_name = $row002['CLASS_NAME'];
          }

          $sql003 = mysqli_query($conn,"SELECT * FROM `paper_master` WHERE `PAPER_ID` = '$paper_master_id'"); //check mcq test table
          while($row003 = mysqli_fetch_assoc($sql003))
          {
              $finish_time = $row003['FINISH_TIME']; //MCQ Test Hold Time
              $finish_date = $row003['FINISH_DATE']; //MCQ Test Hold Date

              $finish_d_t = strtotime($finish_date." ".$finish_time);

              $during_hours = $row003['TIME_DURATION_HOUR']; //Time duration hour
              $during_min = $row003['TIME_DURATION_MIN']; //Time duration minute
              $pdf_name = $row003['PDF_NAME']; //Paper Name
              $answer_amount = $row003['NO_QUESTION_ANSWER'];
              $question_amount = $row003['NO_OF_QUESTIONS'];
              $paper_name = $row003['PAPER_NAME'];
              $start_time = date('Y-m-d h:i:s A',strtotime($row003['START_D_T']));
              $end_time = date('Y-m-d h:i:s A',strtotime($row003['FINISH_D_T']));

              $paper_id = $row003['PAPER_ID']; //Paper ID
          }

         
          /*Attend date and time using get time*/

          $today = strtotime($last_attend);
          $month = date('F',$today);
          $day = date('d',$today);
          $year = date('Y',$today);

          $h_m = date('H:i:s',$today); //Hours and min



      
      $sql0027 = mysqli_query($conn,"SELECT * FROM institute WHERE INS_ID = '1'");
      while($row0027 = mysqli_fetch_assoc($sql0027))
      {
          $ins_name = $row0027['INS_NAME'];
          $ins_tp = $row0027['INS_TP'];
          $ins_mobile = $row0027['INS_MOBILE'];
          $ins_pic = $row0027['PICTURE'];

          if($ins_tp == '0' && $ins_mobile == '0')
          {
            $institute_tp = "";
          }else
          {
            if($ins_tp == '0')
            {
              if($ins_mobile != '0')
              {
                $institute_tp = $ins_mobile;
              }
            }

            if($ins_mobile == '0')
            {
              if($ins_tp != '0')
              {
                $institute_tp = $ins_tp;
              }
            }

            if($ins_tp !== '0' && $ins_mobile !== '0')
            {
              
              
                $institute_tp = $ins_tp."/".$ins_mobile;
              
            }


          }

          
          
          $ins_address = $row0027['INS_ADDRESS'];
      }
  




   ?>

      <center>

        <h1 style="text-align: center;padding-top: 10px;"><?php echo $ins_name; ?></h1>
        <h4 style="text-align: center;"><?php echo $ins_address; ?> <br> <?php echo $institute_tp; ?></h4>
      </center>

  
      
      <small style="font-weight: bold;font-size: 16px;" class="text-muted">
        Examination - <?php echo $paper_name; ?>
      </small>
      <br>
      <small style="font-weight: bold;font-size: 16px;" class="text-muted">
        Class Name - <?php echo $class_name; ?>
      </small>
      <br>
      <small style="font-weight: bold;font-size: 16px;" class="text-muted">
        Started Time - <?php echo $start_time; ?>
      </small>
      <br>
      <small style="font-weight: bold;font-size: 16px;" class="text-muted">
        Finished Time - <?php echo $end_time; ?>
      </small>
      <br>
      <small style="font-weight: bold;font-size: 16px;" class="text-muted">
        Teacher Name - <?php echo $teacher_full_name; ?>
      </small>
      <br>
      <small style="font-weight: bold;font-size: 18px;color: #e74c3c;">
        Total Marks - <?php echo $final_mark."/50"; ?>
      </small>


      <h2 style="font-weight: 600;padding-top: 0px;margin-top: 20px;margin-bottom: 30px;" class="text-success"><span class="fa fa-check"></span> Answer Sheet</h2>
      
        <div class="table-responsive" style="height: auto;">
        <table class="table table-bordered table-striped" style="border-top: 1px solid #cccc;font-size: 20px;">
          <thead>
            <tr>
              <th style="text-align: center;">Question No</th>
              <th style="text-align: center;">My Answer</th>
              <th style="text-align: center;">Correct Answer</th>
              <th style="text-align: center;">Correction</th>
            </tr>
          </thead>
          <tbody id="myTable">

            <?php 
            
              $sql001 = mysqli_query($conn,"SELECT * FROM `student_mcq_transaction` WHERE `PAPER_ID` = '$paper_id' AND `STU_ID` = '$student_id' ORDER BY `QUESTION` ASC"); //Get Correct Answer
              while($row001 = mysqli_fetch_assoc($sql001))
              {
                  $correct_answer_no = $row001['CORRECT_ANSWER']; //System Correct Answer
                  $marks_answer = $row001['MARKS']; //Mark
                  $student_answer = $row001['ANSWER']; //Student Answer
                  $question = $row001['QUESTION']; //Question

                  if($correct_answer_no == $student_answer)
                  {
                    $icon = '<span class="fa fa-check text-success" style="font-size: 26px;"></span>';
                  }else
                  if($correct_answer_no !== $student_answer && $student_answer !== '0')
                  {
                    $icon = '<span class="fa fa-times text-danger" style="font-size: 26px;"></span>';
                  }else
                  if($student_answer == '0')
                  {
                    $student_answer = 'N/A';
                    $icon = '<span class="fa fa-question-circle text-warning" style="font-size: 26px;color:#ff9f43;"></span>';
                  }

                  if($question < 10)
                  {
                    $question = '0'.$question;
                  }

                  if($student_answer < 10 && $student_answer > 0)
                  {
                    $student_answer = '0'.$student_answer;
                  }

                  if($correct_answer_no < 10)
                  {
                    $correct_answer_no = '0'.$correct_answer_no;
                  }

                echo '<tr>
                        <td style="text-align: center;font-weight:bold;">Question '.$question.'</td>
                        <td style="text-align: center;font-weight:bold;">'.$student_answer.'</td>
                        <td style="text-align: center;font-weight:bold;">'.$correct_answer_no.'</td>
                        <td class="text-center">'.$icon.'</td>
                      </tr>';

              }

             ?>
            
          </tbody>

          <thead>
            <tr>
              <th style="text-align: center;">Question No</th>
              <th style="text-align: center;">My Answer</th>
              <th style="text-align: center;">Correct Answer</th>
              <th style="text-align: center;">Correction</th>
            </tr>
          </thead>


        </table>
        </div>
      </div>



</body>
</html>