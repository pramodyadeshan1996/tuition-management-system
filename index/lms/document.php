    
    <div class="row">
        <div class="col-md-12">
            <div class="section-title mt-2">
                <h5>PDF Tute ලබාගැනීම</h5>
            </div>
        </div>
    </div>
    <div class="row ">

        <?php 
      
      $student_session_id = $_SESSION['STU_ID']; //student ID

      $year = date('Y'); //Year
      
      $month = date('m'); //Month

      $sql001 = mysqli_query($conn,"SELECT * FROM `uploads` WHERE `CLASS_ID` = '$class_id' AND `TYPE` = 'Document' AND `PUBLIC` = 'checked' ORDER BY `UPLOAD_TIME` DESC");

      $sql00101 = mysqli_query($conn,"SELECT * FROM `uploads` WHERE `CLASS_ID` = '$class_id' AND `TYPE` = 'Document' ORDER BY `UPLOAD_TIME` DESC");

      $sql00102 = mysqli_query($conn,"SELECT * FROM `payment_data` WHERE `STU_ID` = '$student_session_id' AND `ADMIN_SUBMIT` = '1' AND `YEAR` = '$year' AND `MONTH` = '$month' AND `CLASS_ID` = '$class_id' "); //Check Paid for this month

      $check_paid = mysqli_num_rows($sql00102); //Check available paid?

      $check_upload = mysqli_num_rows($sql00101); //Check available uploads

      $check_public = mysqli_num_rows($sql001); //Check enabled Public



      /*
            
            Upload Table  ||  Payment Table

            Checked     AND   Paid   OK
            Checked     AND   Unpaid OK
            Unchecked   AND   Paid   OK
            UnChecked   AND   Unpaid Wrong



      */

      if($check_upload > 0)
      {

        if(($check_paid > 0 && $check_public > 0) || ($check_paid > 0 && $check_public == '0') || ($check_paid == '0' & $check_public > 0) )
        {
        //Now Check Enable Public show for paid and public enable students (Checked AND Paid   OK , Checked AND Unpaid OK)

        //Available Uploads
          while($row001 = mysqli_fetch_assoc($sql001))
          {
            $file_name = $row001['FILE_NAME'];
            $upload_id = $row001['UPLOAD_ID'];

            $document_title = $row001['TITLE'];
            $document_description = $row001['DESCRIPTION'];
            $document_upload_time = $row001['UPLOAD_TIME'];

            $sql0012 = mysqli_query($conn,"SELECT * FROM `classes` WHERE `CLASS_ID` = '$class_id' "); //Class Details
            while($row0012 = mysqli_fetch_assoc($sql0012))
            {
                $class_name = $row0012['CLASS_NAME']; //Class Name
                $teacher_id = $row0012['TEACH_ID']; //Teacher ID
                $class_fees = $row0012['FEES']; //Teacher ID

                $class_start_time = $row0012['START_TIME']; //Class Start time
                $class_end_time = $row0012['END_TIME']; //Class End time

                $class_day = $row0012['DAY']; //Class End time
                
                $str = strtotime($class_start_time);
                $class_start_time = date('h:i A',$str);

                $str2 = strtotime($class_end_time);
                $class_end_time = date('h:i A',$str2);

            }

            $sql0013 = mysqli_query($conn,"SELECT * FROM `teacher_details` WHERE `TEACH_ID` = '$teacher_id' "); //check Classes for registered teachers details
            while($row0013 = mysqli_fetch_assoc($sql0013))
            {
                $teacher_position = $row0013['POSITION']; //Teacher Position
                $teacher_f_name = $row0013['F_NAME']; //Teacher First Name
                $teacher_l_name = $row0013['L_NAME']; //Teacher Last Name

                $teacher_picture = $row0013['PICTURE']; //Teacher Profile Picture
                $teacher_gender = $row0013['GENDER']; //Teacher Gender


                $teacher_full_name = $teacher_position.". ".$teacher_f_name." ".$teacher_l_name; //Teacher Full Name

                //If Teacher Picture zero,Check gender and gender to select picture 
                if($teacher_picture == '0')
                {
                    if($teacher_gender == 'Male')
                    {
                        $teacher_picture = 'b_teacher.jpg';
                    }else
                    if($teacher_gender == 'Female')
                    {
                        $teacher_picture = 'g_teacher.jpg';
                    }
                }
                //If Teacher Picture zero,Check gender and gender to select picture

            }

            if(isset($_GET['file_name']))
            {
                $file = $_GET['file_name'];
                header("Content-Type: application/octet-stream");

                header("Content-Disposition: attachment; filename=" . urlencode($file));    
                header("Content-Type: application/octet-stream");
                header("Content-Type: application/download");
                header("Content-Type: application/pdf");   // pdf might change to another format eg. doc,docx
                header("Content-Description: File Transfer");            
                header("Content-Length: " . filesize($file));
                flush(); // this doesn't really matter.
                $fp = fopen($file, "r");
                while (!feof($fp))
                {
                echo fread($fp, 65536);
                flush(); // this is essential for large downloads
                } 
                fclose($fp);
            }


            ?>
                <!-- Register Class Box -->

                <div class="col-lg-4">
                    <div class="pricing" style="border: 1px solid #cccc;" id="remove_box<?php echo $class_id; ?>">
                        <div class="content">

                              <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-8 image"><center><img src="../assets/img/icon/doc22.svg" alt="" style="border-radius: 10px;width: 100px;height: 100px;background-size: cover;"></center></div>
                                <div class="col-md-2"></div>
                              </div>


                        <div class="col-md-12"><label style="font-size: 18px;font-weight: bold;"><?php echo $file_name; ?></label></div>
                        
                        <div class="col-md-12"><label class="text-center"><?php echo $class_name; ?></label></div>
                        
                          <div class="col-md-12">
                            <a href="../../uploads/document/<?php echo $file_name; ?>" onclick="clicked_document('<?php echo $upload_id; ?>','<?php echo $student_session_id; ?>');" download="<?php echo $file_name; ?>" class="btn btn-block btn-primary" style="font-weight: bold"><i class="fas fa-download"></i>&nbsp;Download</a>

                            <small class="btn btn-link base-color" style="font-size: 14px;font-weight: 500px;" data-toggle="modal" data-target="#document_info" onclick="document_info('<?php echo $document_title; ?>','<?php echo $document_description; ?>','<?php echo $document_upload_time; ?>')">Document Info</small>
                            


                          </div>

                            <!-- Link Click with inform database -->

                            <script type="text/javascript">

                                  function clicked_document(upload_id,student_id)
                                  {
                                      var type = 'Document';
                                      
                                      $.ajax({
                                        type:"POST",
                                        url:"../../student/query/insert.php",
                                        data : {click_lms:upload_id,student_id:student_id,type:type},
                                        success: function (data) {

                                            
                                        }
                                      });

                                    }
                            </script>

                            <!-- Link Click with inform database -->
                            
                        </div>
                    </div>
                </div>
                

                <!-- Register Class Box -->

            <?php

            //Now Check Enable Public show for paid and public enable students (Checked AND Paid   OK , Checked AND Unpaid OK)

          }
          }else
            if(($check_paid == '0' & $check_public == '0') )
            {
                
                ?>


                <div class="col-md-12 row mt-20" style="padding: 10px 10px 10px 10px;">
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                            <center>
                                <img src="../assets/img/icon/unavailable002.svg" style="width: 250px;height: 250px;">
                                <h5 class="mt-20 text-muted"><span class="fas fa-search"></span> Document Unavailable</h5>
                            </center>
                    </div>
                    <div class="col-md-3"></div>
                </div>


                <?php

                
            }
        }else
        if($check_upload == '0')
        {


                //Now Check Enable Public show for paid and public enable students (Unchecked   AND   Paid   OK UnChecked   AND   Unpaid Wrong)
                //But Public Uncheck with show not available message and check with under this condition show documents
                /*Empty Payments*/

            ?>


            <div class="col-md-12 row mt-20" style="padding: 10px 10px 10px 10px;">
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                            <center>
                                <img src="../assets/img/icon/unavailable002.svg" style="width: 250px;height: 250px;">
                                <h5 class="mt-20 text-muted"><span class="fas fa-search"></span> Document Unavailable</h5>
                            </center>
                    </div>
                    <div class="col-md-3"></div>
                </div>


            <?php

        }

       ?>


    </div>
