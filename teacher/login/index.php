<?php 
    session_start();

    include('../../connect/connect.php');

      //$_SESSION['teacher_info']='Re';
      if(!empty($_SESSION['teacher_info']))
      {?>
          <div class="btn-success active text-center" id="cls" style="cursor: none;padding: 20px 20px 20px 20px;position: relative;bottom: 10px;">
            <h3 class="text-center"><span class="fa fa-check-circle"></span> Registeration Successfully!</h3>
            <h5 style="line-height: 1.5;">ඔබ ලබාදුන් තොරතුරු පරික්ෂා කිරීමෙන් අනතුරුව කෙටි පණිවිඩයක් ඔස්සේ ඔබව දැනුවත් කරනු ඇත.</h5>
            <small>( You will be notified via SMS after verifying the information you have provided. )</small>
          </div>
          

      <?php 
        unset($_SESSION['teacher_info']);
      }




    $suspend_sql = mysqli_query($conn,"SELECT * FROM `institute` WHERE `INS_ID` = '1'");
    
    while($suspend_row=mysqli_fetch_assoc($suspend_sql))
    {
      $check_suspend = $suspend_row['SUSPEND'];
    }

    if($check_suspend > 0)
    {
       include('../../suspend/suspended.php');

    }else
    if($check_suspend == '0')
    {
 ?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Login</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link href="https://fonts.googleapis.com/css?family=Muli&display=swap" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

  <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<style type="text/css">

button {
    --c: goldenrod;
    color: var(--c);
    font-size: 16px;
    border: 0.3em solid var(--c);
    border-radius: 0.5em;
    text-transform: uppercase;
    font-weight: bold;
    font-family: sans-serif;
    letter-spacing: 0.1em;
    text-align: center;
    position: relative;
    overflow: hidden;
    z-index: 1;
    transition: 0.5s;
    margin: 0em;
}

button span {
    position: absolute;
    width: 25%;
    height: 100%;
    background-color: var(--c);
    transform: translateY(150%);
    border-radius: 50%;
    left: calc((var(--n) - 1) * 25%);
    transition: 0.9s;
    transition-delay: calc((var(--n) - 1) * 0.1s);
    z-index: -1;
}

button:hover {
    color: black;
}

button:hover span {
    transform: translateY(0) scale(2);
}

button span:nth-child(1) {
    --n: 1;
}

button span:nth-child(2) {
    --n: 2;
}

button span:nth-child(3) {
    --n: 3;
}

button span:nth-child(4) {
    --n: 4;
}

</style>

</head>

<body class="bg-gradient-primary" style="font-family: 'Muli', sans-serif;background: #e96443;  /* fallback for old browsers */
background: -webkit-linear-gradient(to right, #904e95, #e96443);  /* Chrome 10-25, Safari 5.1-6 */
background: linear-gradient(to right, #904e95, #e96443); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
">
  <div class="container">



    <div class="col-md-12">
    <!-- Outer Row -->
    <div class="row justify-content-center">
     <div class="col-xl-4 col-lg-4 col-md-4"></div>

      <div class="col-md-4" style="border:1px solid #00000000;margin-top: 10%;background-color: #0000007d;padding: 40px 40px 40px 40px;border-radius: 4px;">

        

                  <div class="text-center">
                    <h1 style="color: white;">Welcome!</h1>
                  </div>
                    <br>

                    <div class="form-group">
                      <input type="text" class="form-control form-control-user" aria-describedby="emailHelp" placeholder="Enter Username..." name="username" style="padding-left: 24px;height:50px;border-radius: 50px;" id="user" required autocomplete="off" autofocus="on">
                    </div>
                    <div class="form-group" id="show_hide_password" style="position: relative;">
                      <input type="password" class="form-control form-control-user" placeholder="Enter Password" name="password" style="padding-left: 24px;height:50px;border-radius: 50px;" id="psw" required autocomplete="off">

                          <a href=""><i class="fa fa-eye-slash" aria-hidden="true" style="font-size: 18px;color: black;position: absolute;right:18px;top:15px;"></i></a>
                    </div>
                    
                    <button type="submit" class="btn btn-info btn-active btn-user btn-block" name="submit" id="log" style="margin-top: 40px;height:50px;border-radius: 50px;font-size: 16px;background: rgb(38,158,245);background: linear-gradient(191deg, rgba(38,158,245,1) 47%, rgba(54,163,245,1) 87%);font-weight: bold;outline: none;border:rgb(218 165 32);"><span></span><span></span><span></span><span></span> 
                      Login
                    </button>

                <div class="col-md-12" style="text-align: center;"><small style="color:white;">Teacher Login Page - &copy;IBS Developer Team</small></div>
                 <div class="col-md-12" style="text-align: center;"><small style="color:white;">Already i haven't an account, <a href="register/form/reg_teacher.php" style="color: yellow">Create Account</a></small>

                  <center><a href="../../index.php" style="color:#ffff;font-size: 12px;margin-top: 20px;color: yellow"><span class="fa fa-arrow-left"></span> Back</a></center>
                 </div>

          </div>

      <div class="col-xl-4 col-lg-4 col-md-4"></div>

    </div>

  </div>
</div>
  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>
   <script type="text/javascript">
  
  /*login script*/
  $("#log").click(function(){
    var user = $('#user').val();
    var pasw = $('#psw').val();

    if(user == '')
    {
      $("#result").html("<div class='alert alert-danger alert-dismissible' id='alert'><button type='button' class='close' data-dismiss='alert'>&times;</button><span class='fa fa-warning'></span> <strong>Warning : </strong> Empty Feild.Please fill the Inputs.</div>");
      $("#alert").delay(3000).slideUp(500, function() {
            $("#alert").slideUp(800);              
      });
    }
    else
    if( pasw == '')
    {
      $("#result").html("<div class='alert alert-danger alert-dismissible' id='alert'><button type='button' class='close' data-dismiss='alert'>&times;</button><span class='fa fa-warning'></span> <strong>Warning : </strong> Empty Feild.Please fill the Inputs.</div>");
      $("#alert").delay(3000).slideUp(500, function() {
            $("#alert").slideUp(800);              
      });
    }
    else
    if(user !== '' && pasw !== '')
    {
      $.ajax({  
                     url:"log.php",  
                     method:"POST",  
                     data:{user:user,psw:pasw},  
                     success:function(data){ 
                      
                     if(data == 0)
                     {
                          Swal.fire({
                          position: 'top-middle',
                          type: 'error',
                          title: 'Username Or Password Incorrect',
                          showConfirmButton: false,
                          timer: 1500
                          })
                          $("#psw").focus();
                          $("#psw").val('');
                     }

                     if(data == 2)
                     {
                          $("#result").html("<div class='alert alert-danger alert-dismissible' id='alert'><button type='button' class='close' data-dismiss='alert'>&times;</button><span class='fa fa-warning'></span> <strong>Warning : </strong> Sorry, your registered account has not been verified yet.</div>");
                     }

                     if(data == 1)
                     {
                        window.location.href = "../../new_teacher/dashboard.php";
                     }
                    }
      });
    }
  });
  /*login script*/
  

  /*Enter key press after login to page*/
  $('#user').on( 'keyup', function( e ) {
            if( e.which == 13 ) {
              //GOTO DOWN
              $('#psw').focus();
            }
            
        } );

  $('#psw').on( 'keyup', function( e ) {
            if( e.which == 13 ) {
              //GOTO DOWN
              $('#log').click();
            }
            
        } );
  /*Enter key press after login to page*/


  $(document).ready(function() {
    $("#show_hide_password a").on('click', function(event) {
        event.preventDefault();
        if($('#show_hide_password input').attr("type") == "text"){
            $('#show_hide_password input').attr('type', 'password');
            $('#show_hide_password i').addClass( "fa-eye-slash" );
            $('#show_hide_password i').removeClass( "fa-eye" );
        }else if($('#show_hide_password input').attr("type") == "password"){
            $('#show_hide_password input').attr('type', 'text');
            $('#show_hide_password i').removeClass( "fa-eye-slash" );
            $('#show_hide_password i').addClass( "fa-eye" );
        }
    });
});

</script>

</body>

</html>

<?php 
  }
?>


 <script type="text/javascript">
    //Close Message after timeout

        setTimeout(function(){ 


        document.getElementById('cls').style.display='none'; //Save Message


        }, 6000); //after function execute 1 second

        //setTimeout(function(){ document.getElementById('desc_order').click(); }, 50);

    //Close Message after timeout
</script>