<?php
  $page = "Students Details";
  $folder_in = '3';

      include('header/header.php');

        $s04 = mysqli_query($conn,"SELECT * FROM admin_login WHERE ADMIN_ID = '$admin_id'");
        while($row4 = mysqli_fetch_assoc($s04))
        {
          $name = $row4['ADMIN_NAME'];
        }

 ?>

 <div class="row">
   <div class="col-md-1"></div>
    <div class="col-md-11">
      <ol class="breadcrumb" style="font-weight: bold;margin-bottom: 04%;margin-bottom: 0;">
        <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
        <li class="breadcrumb-item" data-toggle="tooltip" data-title="Students Details"> Students Details</li>
      </ol>
    </div>
 </div>
    
    <div class="row" style="padding-top: 10px;">
      <div class="col-md-1"></div>
      <div class="col-md-10 bg-white" style="border:1px solid #cccc;height: auto;">
      
      <div class="col-md-12" style="border-bottom: 1px solid #cccc;">
        <div class="row">

            <div class="col-md-9" style="margin-bottom: 10px;"><h2><a href="dashboard.php" data-toggle="tooltip" data-title="Back"><i class="pg-icon" style="font-size: 22px;">chevron_left</i></a> Students Details</h2></div>

            <?php 

            $sql00100 = mysqli_query($conn,"SELECT * FROM stu_login WHERE STATUS = 'Active' ORDER BY REG_DATE DESC");

            $count = mysqli_num_rows($sql00100);

            if($count == '0')
            {
              $disabled = 'class="btn btn-success btn-round btn-lg pull-right disabled"  style="border-radius: 90px;padding: 10px 10px;cursor:not-allowed;" ';
            }else
            if($count>0)
            {
              $disabled = 'class="btn btn-success btn-round btn-lg pull-right" style="border-radius: 90px;padding: 10px 10px;" ' ;
            }

             ?>

            <div class="col-md-3 pull-right"><h5><a href="print_email.php" target="_blank" <?php echo $disabled; ?> data-toggle="tooltip" data-title="Print Details"><i class="pg-icon">printer</i></a></h5></div>

        </div>
      </div>
          

<div class="card card-borderless">
<div class="col-md-12" style="padding-top:10px;">

<div class="tab-content">
<div class="tab-pane active" id="home">
<div class="row column-seperation">



<div class="col-lg-12">
  
<div class=" container-fluid container-fixed-lg bg-white" style="margin-top: 0px;">
            <div class="card card-transparent">
              <div class="card-header ">
                    <div class="card-title">
                    </div>
                    <div>
                      <div class="row">
                        <div class="col-md-6">
                        </div>
                      <div class="col-md-2"></div>
                      <div class="col-md-4">
                      <div class="col-md-12" style="padding-top: 0px;"></div>
                      <input type="text" id="search-table" class="form-control pull-right" placeholder="Search">
                      </div>

                    </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="card-body">
                  <table class="table demo-table-search table-responsive-block text-left" id="tableWithSearch">
                  <thead>
                  <tr>
                  <th class=" bold" style="width: 15%;">Register Date</th>
                  <th style="width: 1%;">Registered ID</th>

                  <th>Student Name <label class="label label-success"><?php echo  $count; ?></label></th>
                  <th style="width: 1%;">Email Address</th>
                  <th style="width: 1%;">Address</th>
                  <th style="width: 1%;">Telephone No</th>
                  </tr>
                  </thead>
                  <tbody id="myTable">

                  <?php 

                    $today = date('Y-m-d h:i:s A');

                    //mysqli_query($conn,"UPDATE stu_login SET REG_DATE = '$today'");

                    $sql001 = mysqli_query($conn,"SELECT * FROM stu_login WHERE STATUS = 'Active' ORDER BY REG_DATE DESC");

                    $check = mysqli_num_rows($sql001);

                    if($check>0){
                    while($row001 = mysqli_fetch_assoc($sql001))
                    {
                      $stu_id = $row001['STU_ID'];
                      $reg = $row001['REGISTER_ID'];
                      $reg_date = $row001['REG_DATE'];
                      $password = $row001['PASSWORD'];

                      $sql002 = mysqli_query($conn,"SELECT * FROM student_details WHERE STU_ID = '$stu_id'");
                      while($row002 = mysqli_fetch_assoc($sql002))
                      {
                        $name = $row002['F_NAME']." ".$row002['L_NAME'];
                        $fnm = $row002['F_NAME'];
                        $lnm = $row002['L_NAME'];
                        $tp1 = $row002['TP'];
                        $tp = str_replace(" ","",$tp1);
                        $dob = $row002['DOB'];
                        $gender = $row002['GENDER'];
                        $email = $row002['EMAIL'];
                        $address = $row002['ADDRESS'];
                        $new = $row002['NEW'];
                      }
                      

                      echo '

                        <tr>
                          <td class="v-align-middle bold">'.$reg_date.'</td>
                          <td class="v-align-middle">'.$reg.'</td>
                          <td class="v-align-middle">'.$name.'</td>
                          <td class="v-align-middle">'.$email.'</td>
                          <td class="v-align-middle">'.$address.'</td>
                          <td class="v-align-middle">'.$tp1.'</td>
                        </tr>

                      ';
                    }
                  }

                   ?>


                  </tbody>
                </table>
</div>

              
</div>
</div>
</div>
</div>




</div>
</div>



      </div>
      </div>
      <div class="col-md-1"></div>
    </div>


  </div>
  </div>
  </div>
  </div>





<script type="text/javascript">
  $('.save_btn').click(function(){
    
    var upload_btn0 = $('#selectedFile0').val();
    var upload_btn = $('#sel_file').val();
      
    if(empty(upload_btn0))
    {
      alert('Please select your audio file.');
    }else
    if(empty(upload_btn))
    {
      alert('Please select your document file.');
    }
   });
</script>

<script type="text/javascript">
  $('.navi').click(function(){
    $('.frm')[0].reset();
      
   });
</script>

<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
<!-- search data -->

<!-- no data message/no found data show in search-->

<script type="text/javascript">
  $(document).ready(function () {
 
    (function ($) {

        $('#myInput').keyup(function () {
            var rex = new RegExp($(this).val(), 'i');
            $('#myTable tr').hide();
            $('#myTable tr').filter(function () {
                return rex.test($(this).text());
            }).show();
            $('.no-data').hide();
            if($('#myTable tr:visible').length == 0)
            {
                $('.no-data').show();
            }

        })

    }(jQuery));

});
</script>




<?php include('footer/footer.php'); ?>