<?php
	$page = "Alert Management";
	$folder_in = '0';

      include('header/header.php');

        $s04 = mysqli_query($conn,"SELECT * FROM admin_login WHERE ADMIN_ID = '$admin_id'");
        while($row4 = mysqli_fetch_assoc($s04))
        {
          $name = $row4['ADMIN_NAME'];
        }

 ?>

    <ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-bottom: 04%;margin-bottom: 0;">
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="Alert Management"> <a href="alert.php">Alert Management</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="Edit Alert"> Edit Alert</li>
    </ol>


  <div class="col-md-12">
    
    <div class="row" style="padding-top: 10px;">
      <div class="col-md-2"></div>
      <div class="col-md-8 bg-white" style="border:1px solid #cccc;height: auto;">
      
      <div class="col-md-12" style="border-bottom: 1px solid #cccc;">
        <div class="row">

            <div class="col-md-9" style="margin-bottom: 10px;"><h3><a href="alert.php" data-toggle="tooltip" data-title="Go Back"><i class="pg-icon">chevron_left</i></a> Edit Alert</h3></div>


        </div>
      </div>
          

<div class="card card-borderless">
<div class="col-md-12" style="padding-top:10px;">

<div class="tab-content">
<div class="tab-pane active" id="home">
<div class="row column-seperation">

<?php 
  $check_level = 0;
  $check_subject = 0;
  $check_class = 0;

  $alert_id = $_GET['alert_id'];

  $sql = mysqli_query($conn,"SELECT * FROM alert WHERE ALERT_ID = '$alert_id'");
  while($row = mysqli_fetch_assoc($sql))
  {
    $title= $row['TITLE'];
    $msg= $row['MESSAGE'];
    $aud= $row['AUDIENCE'];
    $publish= $row['PUBLISH'];
    $times= $row['TIMES'];

    if($aud == 'Teacher')
    {
      $aud_name = "Teacher-Class";
    }else
    if($aud == 'Public')
    {
      $aud_name = "Public";
    }else
    if($aud == 'Private')
    {
      $aud_name = "Private";
    }
  }
  
 ?>

<div class="col-lg-12">
  <form action="../admin/query/update.php" method="POST">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group form-group-default input-group">
                <div class="form-input-group">
                  <label>Title</label>
                  <input type="text" name="title" class="form-control change_inputs" required placeholder="XXXXXXXXX" value="<?php echo $title; ?>">
                </div>
              </div>
            </div>

          </div>

          <div class="row">
            <div class="col-md-12">
              <div class="form-group form-group-default input-group">
                <div class="form-input-group">
                  <label>Message</label>
                    <textarea name="message001" placeholder="XXXXXXXXX" style="margin-bottom: 10px;height: 200px;" class="form-control" cols="20"><?php echo $msg; ?></textarea>
                  </div>
                </div>
            </div>

          </div>
          <div class="row" style="display: none;">
            <div class="col-md-12">
              <div class="form-group form-group-default input-group">
                <div class="form-input-group">
                  <label>Seen Times</label>
                    <input type="text" name="times" id="customer_data" class="form-control" required style="outline: none;" placeholder="Type Seen Times.." onclick="this.select()" autocomplete="off" value="<?php echo $times; ?>" value="1">

                  </div>
                </div>
            </div>

          </div>

          <div class="row">
            <div class="col-md-8">
              <div class="form-group form-group-default input-group">
                <div class="form-input-group">
                  <label>Audience</label>
                    <select class="form-control" name="audience" id="aud">
                      <option value="<?php echo $aud; ?>"><?php echo $aud_name; ?></option>
                      <?php 
                        
                        /*if($aud == 'Public')
                        {


                      echo '<option value="Private">Private</option>
                            <option value="Teacher">Teacher-Class</option>';

                        }else
                        if($aud == 'Private')
                        {

                    echo '<option value="Public">Public</option>
                          <option value="Teacher">Teacher-Class</option>';

                        }else
                        if($aud == 'Teacher')
                        {

                    echo '<option value="Public">Public</option>
                          <option value="Private">Private</option>';

                        }*/

                       /* else
                        if($aud == 'Level')
                        {

                    echo '<option value="Public">Public</option>
                          <option value="Private">Private</option>
                          <option value="Subject">Subject</option>
                          <option value="Classes">Classes</option>';

                        
                        }else
                        if($aud == 'Subject')
                        {

                    echo '<option value="Public">Public</option>
                          <option value="Private">Private</option>
                          <option value="Level">Level</option>
                          <option value="Classes">Classes</option>';

                        }else
                        if($aud == 'Classes')
                        {

                          echo '<option value="Public">Public</option>
                                <option value="Private">Private</option>
                                <option value="Level">Level</option>
                                <option value="Subject">Subject</option>';
                        }*/
                       ?>
                      
                    </select>
                  </div>
                </div>
            </div>

            <script type="text/javascript">
              $(document).ready(function(){

                  $('#aud').on('change',function(){
                    var aud = $(this).val();

                    if(aud == 'Private')
                    {
                      $('#private').modal();
                      $('#level').prop('selectedIndex',0);
                    }

                    if(aud == 'Level')
                    {
                      $('.checkb').prop("checked", false);
                      $('#level').modal();
                    }

                    if(aud == 'Subject')
                    {
                      $('.checkb').prop("checked", false);
                      $('#subject').modal();
                    }

                    if(aud == 'Classes')
                    {
                      $('.checkb').prop("checked", false);
                      $('#classes').modal();
                    }
                    if(aud == 'Teacher')
                    {
                      $('.checkb').prop("checked", false);
                      $('#teacher122').modal();
                    }

                  });
              });
            </script>

            <div class="modal fade slide-up disable-scroll" id="private" tabindex="-1" role="dialog" aria-hidden="false" style="overflow: auto;">
          <div class="modal-dialog ">
          <div class="modal-content-wrapper">
          <div class="modal-content modal-lg">
          <div class="modal-header clearfix text-left">
          <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
          </button>
          <h5>Private Audience</h5>
          </div>
          <div class="modal-body">
            
            <div class="table-responsive" style="height: 450px;overflow: auto;">
            <table class="table">
              <thead>
                <th>#</th>
                <th>Register ID</th>
                <th>Student Name</th>
                <th>Telephone No</th>
              </thead>
              <tbody>
                <?php 
                


                      $sql01 = mysqli_query($conn,"SELECT * FROM stu_login WHERE STATUS = 'Active' ORDER BY REGISTER_ID ASC");
                      while($row01 = mysqli_fetch_assoc($sql01))
                      {
                        $stu_id = $row01['STU_ID'];
                        $reg_date = $row01['REG_DATE'];
                        $reg_id = $row01['REGISTER_ID'];

                        $sql02 = mysqli_query($conn,"SELECT * FROM student_details WHERE STU_ID = '$stu_id'");
                        while($row02 = mysqli_fetch_assoc($sql02))
                        {
                          $name = $row02['F_NAME']." ".$row02['L_NAME'];
                          $tp = $row02['TP'];
                          $email = $row02['EMAIL'];
                        }
                  echo '<tr>';
                        $sql03 = mysqli_query($conn,"SELECT * FROM custom_student WHERE ALERT_ID = '$alert_id' AND STU_ID = '$stu_id'");
                        if(mysqli_num_rows($sql03)>0)
                        {


                            while($row03 = mysqli_fetch_assoc($sql03))
                            {

                              $student_id = $row03['STU_ID'];
                              echo '<td><input type="checkbox" name="check_stu[]" value="'.$stu_id.'" class="checkb" checked></td>';
                              

                            }

                        }else
                        if(mysqli_num_rows($sql03) == '0')
                        {
                            echo '<td><input type="checkbox" name="check_stu[]" value="'.$stu_id.'" class="checkb" ></td>';
                        }

                        $sql04 = mysqli_query($conn,"SELECT * FROM audience WHERE ALERT_ID = '$alert_id'");
                        while($row04 = mysqli_fetch_assoc($sql04))
                        {
                          $lev_id = $row04['LEVEL_ID'];
                          $subj_id = $row04['SUB_ID'];
                          $clz_id = $row04['CLASS_ID'];

                            $sql041 = mysqli_query($conn,"SELECT * FROM level WHERE LEVEL_ID = '$lev_id'");
                            $check_level = mysqli_num_rows($sql041);
                            if($check_level>0)
                            {
                                while($row041 = mysqli_fetch_assoc($sql041))
                                {
                                  $lev_id = $row041['LEVEL_ID'];
                                  $lev_name = $row041['LEVEL_NAME'];
                                }
                            }

                            $sql042 = mysqli_query($conn,"SELECT * FROM subject WHERE SUB_ID = '$subj_id'");
                            $check_subject = mysqli_num_rows($sql042);
                            if($check_subject>0)
                            {
                                while($row042 = mysqli_fetch_assoc($sql042))
                                {
                                  $sub_name = $row042['SUBJECT_NAME'];
                                  $subj_id = $row042['SUB_ID'];
                                }
                            }

                            $sql043 = mysqli_query($conn,"SELECT * FROM classes WHERE CLASS_ID = '$clz_id'");
                            $check_class = mysqli_num_rows($sql043);
                            if($check_class>0)
                            {
                                while($row043 = mysqli_fetch_assoc($sql043))
                                {
                                  $clz_name = $row043['CLASS_NAME'];
                                  $xlz_id = $row043['CLASS_ID'];
                                }
                            }
                            

                        }
                    
                             
                       echo '<td>'.$reg_id.'</td>
                             <td>'.$name.'</td>
                             <td>'.$tp.'</td>
                           </tr>
                        ';
                      }
                    
                  
                  
                 ?>

                
              </tbody>
            </table>
          </div>
          </div>
          <div class="modal-footer"><button class="btn btn-success" data-dismiss="modal"><i class="pg-icon">tick_circle</i> Save</button></div>
          </div>
          </div>
          </div>

            </div>

            <div class="modal fade slide-up disable-scroll" id="classes" tabindex="-1" role="dialog" aria-hidden="false" style="overflow: auto;">
          <div class="modal-dialog ">
          <div class="modal-content-wrapper">
          <div class="modal-content">
          <div class="modal-header clearfix text-left">
          <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
          </button>
          <h5>Classes Audience</h5>
          </div>
          <div class="modal-body">
            
              <div class="form-group form-group-default input-group">
                <div class="form-input-group">
                  <label>Level Name</label>
                    <select class="form-control" name="level_id3" id="level3">
                      
                      <?php 
                        $sql01 = mysqli_query($conn,"SELECT * FROM level WHERE LEVEL_ID != '$lev_id'");

                        if($check_level>0)
                        {
                          ?>

                          <option value="<?php echo $lev_id; ?>"><?php echo $lev_name; ?></option>

                          <?php
                          while($row01 = mysqli_fetch_assoc($sql01))
                          {
                            $level_id = $row01['LEVEL_ID'];
                            $level_name = $row01['LEVEL_NAME'];

                            echo '
                               <option value="'.$level_id.'">'.$level_name.'</option>
                            ';
                          }
                        }else
                        if($check_level=='0')
                        {
                           echo '<option value="">Select Level Name</option>';
                     
                            $sql01 = mysqli_query($conn,"SELECT * FROM level");
                            while($row01 = mysqli_fetch_assoc($sql01))
                            {
                              $level_id = $row01['LEVEL_ID'];
                              $level_name = $row01['LEVEL_NAME'];

                              echo '<option value="'.$level_id.'">'.$level_name.'</option>';
                            }
                        }
                        
                       ?>

                 </select>
                  </div>
                </div>
            
              <div class="form-group form-group-default input-group">
                <div class="form-input-group" style="margin-top: 10px;">
                  <label>Subject Name</label>
                  
                  <select class="form-control"  id="subject3"  name="sub_id3" style="cursor: pointer;">
                      <?php 
                        $sql012 = mysqli_query($conn,"SELECT * FROM subject WHERE SUB_ID != '$subj_id'");
                        if($check_subject > 0)
                        {
                          while($row012 = mysqli_fetch_assoc($sql012))
                          {
                            $subje_id = $row012['SUB_ID'];
                            $s_name = $row012['SUBJECT_NAME'];

                            echo '
                               <option value="'.$subje_id.'">'.$s_name.'</option>
                            ';
                          }
                        }else
                        if($check_subject == '0')
                        {
                          echo '<option value="">Select Subject Name</option>';
                        }
                        
                       ?>
                  </select>

                </div>
              </div>

              <div class="form-group form-group-default input-group">
                <div class="form-input-group" style="margin-top: 10px;">
                  <label>Classes Name</label>
                  
                  <select class="form-control"  id="classes3"  name="class_id3" style="cursor: pointer;">
                    
                      <?php 
                        $sql012 = mysqli_query($conn,"SELECT * FROM classes WHERE CLASS_ID != '$clz_id'");
                        if($check_class >0)
                        {?> <option value="<?php echo $clz_id; ?>"><?php echo $clz_name; ?></option> <?php
                          while($row012 = mysqli_fetch_assoc($sql012))
                          {
                            $clas_id = $row012['CLASS_ID'];
                            $clas_name = $row012['CLASS_NAME'];

                            echo '
                               <option value="'.$clas_id.'">'.$clas_name.'</option>
                            ';
                          }
                        }else
                        if($check_class == '0')
                        {
                            echo '<option value="">Select Class Name</option>';
                        } 
                        
                       ?>
                  </select>

                </div>
              </div>

             

          </div>

           <div class="modal-footer"><button class="btn btn-success" data-dismiss="modal"><i class="pg-icon">tick_circle</i> Save</button></div>
          </div>
          </div>
          </div>

            </div>

 <div class="modal fade slide-up disable-scroll" id="teacher122" tabindex="-1" role="dialog" aria-hidden="false" style="overflow: auto;">
          <div class="modal-dialog ">
          <div class="modal-content-wrapper">
          <div class="modal-content">
          <div class="modal-header clearfix text-left">
          <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
          </button>
          <h5>Teacher - Classes Audience</h5>
          </div>
          <div class="modal-body">
            
              <div class="form-group form-group-default input-group">
                <div class="form-input-group">
                  <label>Teacher Name</label>
                    <select class="form-control" name="teacher" id="teacher_name">
                      <option value="">Select Teacher Name</option>
                    
                <?php 
                  $sql011 = mysqli_query($conn,"SELECT * FROM `teacher_details`");
                  while($row011 = mysqli_fetch_assoc($sql011))
                  {

                    $teacher_name1 = $row011['POSITION'].". ".$row011['F_NAME']." ".$row011['L_NAME'];
                    $teach_id = $row011['TEACH_ID'];

                     $sql012 = mysqli_query($conn,"SELECT * FROM `teacher_login` WHERE `TEACH_ID` = '$teach_id' AND `STATUS` = 'Active'");
                    while($row012 = mysqli_fetch_assoc($sql012))
                    {
                      $teach_id2 = $row012['TEACH_ID'];

                      echo '
                         <option value="'.$teach_id2.'">'.$teacher_name1.'</option>
                      ';
                    }
                  }
                 ?>
                 </select>
                  </div>
                </div>
            
              <div class="form-group form-group-default input-group">
                <div class="form-input-group" style="margin-top: 10px;">
                  <label>Subject Name</label>
                  
                  <select class="form-control"  id="subject44"  name="subject" style="cursor: pointer;">
                    <option value="">Select Subject</option>
                  </select>

                </div>
              </div>

              <div class="form-group form-group-default input-group">
                <div class="form-input-group" style="margin-top: 10px;">
                  <label>Classes Name</label>
                  
                  <select class="form-control"  id="classes44"  name="class_id" style="cursor: pointer;">
                    <option value="">Select Class Name</option>
                  </select>

                </div>
              </div>

          </div>

           <div class="modal-footer"><button class="btn btn-success" data-dismiss="modal"><i class="pg-icon">tick_circle</i> Save</button></div>
          </div>
          </div>
          </div>

            </div>





            <div class="col-md-4">
              <div class="form-group form-group-default input-group">
                <div class="form-input-group">
                  
                  <button type="submit" class="btn btn-primary btn-block" style="height: 52px;" name="edit_alert" value="<?php echo $alert_id; ?>"><i class="pg-icon">edit</i> Update</button>
                    
                  </div>
                </div>
            </div>

          </div>
        </form>
              
</div>
</div>
</div>
</div>




</div>
</div>



      </div>
      <div class="col-md-2"></div>
    </div>


  </div>



            <script type="text/javascript">
  
                $(document).ready(function(){  
                 $('#teacher_name').change(function(){

                   var teacher = $(this).val();

                   $.ajax({
                    url:'../admin/query/check.php',
                    method:"POST",
                    data:{check_teacher:teacher},
                    success:function(data)
                    {
                      //alert(data)
                      $('#subject44').html(data);
                        //$('#select_clz').html('<option value="">Select Class Name</option>');

                      
                    }
                   })
                 

                });

                 $('#subject44').change(function(){

                   var subject = $(this).val();
                   var teacher = $('#teacher_name').val();

                   $.ajax({
                    url:'../admin/query/check.php',
                    method:"POST",
                    data:{check_teacher_clz:subject,teacher:teacher},
                    success:function(data)
                    {
                      //alert(data)
                      $('#classes44').html(data);
                        //$('#select_clz').html('<option value="">Select Class Name</option>');

                      
                    }
                   })
                 

                });

                 $('#level2').change(function(){

                   var level_clz = $(this).val();

                   $.ajax({
                    url:'../admin/query/check.php',
                    method:"POST",
                    data:{check_level:level_clz},
                    success:function(data)
                    {
                        $('#subject2').html(data);
                        //$('#select_clz').html('<option value="">Select Class Name</option>');

                      
                    }
                   })
                 

                });


                 $('#level3').change(function(){

                   var level_clz = $(this).val();

                   $.ajax({
                    url:'../admin/query/check.php',
                    method:"POST",
                    data:{check_level2:level_clz},
                    success:function(data)
                    {
                        $('#subject3').html(data);
                        //$('#select_clz').html('<option value="">Select Class Name</option>');

                      
                    }
                   })
                 

                });

                 $('#subject3').click(function(){

                   var sub_id = $(this).val();
                   $.ajax({
                    url:'../admin/query/check.php',
                    method:"POST",
                    data:{classes3:sub_id},
                    success:function(data)
                    {
                      //alert(data)
                      $('#classes3').html(data);
                      
                    }
                   })
                 

                });

                 
               });

              </script>



<script type="text/javascript">
  $('.save_btn').click(function(){
    
    var upload_btn0 = $('#selectedFile0').val();
    var upload_btn = $('#sel_file').val();
      
    if(empty(upload_btn0))
    {
      alert('Please select your audio file.');
    }else
    if(empty(upload_btn))
    {
      alert('Please select your document file.');
    }
   });
</script>

<script type="text/javascript">
  $('.navi').click(function(){
    $('.frm')[0].reset();
      
   });
</script>

<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
<!-- search data -->

<!-- no data message/no found data show in search-->

<script type="text/javascript">
  $(document).ready(function () {
 
    (function ($) {

        $('#myInput').keyup(function () {
            var rex = new RegExp($(this).val(), 'i');
            $('#myTable tr').hide();
            $('#myTable tr').filter(function () {
                return rex.test($(this).text());
            }).show();
            $('.no-data').hide();
            if($('#myTable tr:visible').length == 0)
            {
                $('.no-data').show();
            }

        })

    }(jQuery));

});
</script>




<?php include('footer/footer.php'); ?>