<?php
	$page = "Uploads";
	$folder_in = '0';

      include('header/header.php');

        $teach_id = $_SESSION['TEACH_ID'];

        $s04 = mysqli_query($conn,"SELECT * FROM teacher_details WHERE TEACH_ID = '$teach_id'");
        while($row4 = mysqli_fetch_assoc($s04))
        {
          $teacher_name = $row4['POSITION'].". ".$row4['F_NAME']." ".$row4['L_NAME'];
          $teacher_name2 = $row4['F_NAME']." ".$row4['L_NAME'];

          $t_pic = $row4['PICTURE'];

          if($t_pic == '0')
          {
          	$t_pic = 'teacher.png';
          }
        }

    $video = '';

    $audio = '';

    $document = '';


    if(!empty($_SESSION['click_new_upload_tab']))
    {
      $click_tab = $_SESSION['click_new_upload_tab'];

      if($click_tab == 'video')
      {
        $video = 'active';
      }else
      if($click_tab == 'audio')
      {
        $audio = 'active';
      }else
      if($click_tab == 'document')
      {
        $document = 'active';
      }else
      if($click_tab == 'record')
      {
        $record = 'active';
      }

    }else
    if(empty($_SESSION['click_new_upload_tab']))
    {
      $video = 'active';

      $audio = '';

      $document = '';

      $record = '';


    }

 ?>

 <div class="row" style="border-bottom: 1px solid #cccc;">
  <div class="col-md-10">
      

      
  </div>
  <div class="col-md-2">

    
  </div>

</div>

<div class="row" style="margin-top: 5%;border-bottom: 1px solid #cccc;padding-bottom: 10px;">
  <div class="col-md-11 text-muted">
    
          <a href="dashboard.php" data-toggle="tooltip" data-title="Dashboard" style="color: gray;"> Dashboard</a>
          <span class="fa fa-angle-right"></span>
          <b>New Upload</b>
           
        
  </div>
  <div class="col-md-1">
    <a href="view_upload.php?teach_id=<?php echo $teach_id; ?>" class="btn btn-success"><span class="fa fa-edit"></span>&nbsp;View Upload</a>
  </div>
</div>

  <div class="col-md-12">
    
    <div class="row" style="padding-top: 10px;">
      <div class="col-md-3"></div>
      <div class="col-md-6 bg-white" style="border:1px solid #cccc;height: auto;">
          
          <div class="col-md-12" style="border-bottom: 1px solid #cccc;margin-bottom: 10px;">

            <label style="font-size: 12px;padding-top: 10px;">
              <a href="dashboard.php" class="text-muted" data-toggle="tooltip" data-title="Back" style="font-size: 12px;"><span class="fa fa-angle-left"></span> Back</a>
            </label>

            <h2>New Upload <small style="font-size: 11px;text-align: right;line-height: 4" class="pull-right"><?php echo $teacher_name; ?></small></h2>

          </div>


          

<div class="card card-borderless">


<ul class="nav nav-tabs nav-tabs-simple" data-init-reponsive-tabs="dropdownfx">
<li class="nav-item navi" style="width: 25%;text-align: center;">
<a class="<?php echo $video; ?>" data-toggle="tab" data-target="#home" href="#" onclick="click_tab('video');"><span><span class="fa fa-video-camera" style="font-size: 16px;line-height: 0.8;"></span> Video</span></a>
</li>
<li class="nav-item navi" style="width: 25%;text-align: center;">
<a class="<?php echo $audio; ?>" data-toggle="tab" data-target="#profile" href="#" onclick="click_tab('audio');"><span><span class="fa fa-volume-up" style="font-size: 16px;line-height: 0.8;"></span> Audio</span></a>
</li>
<li class="nav-item navi" style="width: 25%;text-align: center;">
<a class="<?php echo $document; ?>"data-toggle="tab" data-target="#messages" href="#" onclick="click_tab('document');"><span><span class="fa fa-file" style="font-size: 16px;line-height: 0.8;"></span> Document</span></a>
</li>
<li class="nav-item navi" style="width: 25%;text-align: center;">
<a class="<?php echo $record; ?>"data-toggle="tab" data-target="#record" href="#" onclick="click_tab('record');"><span><span class="fa fa-circle text-danger" style="font-size: 16px;line-height: 0.8;"></span> Records</span></a>
</li>

</ul>


 <script type="text/javascript">
    function click_tab(tab_name)
    {
          
          $.ajax({  
          url:"../teacher/query/check.php",  
          method:"POST",  
          data:{click_new_upload_tab:tab_name},  
          success:function(data){ 
             
            //alert(data);

           }           
         });

    }
 </script>

<div class="tab-content">
<div class="tab-pane <?php echo $video; ?>" id="home">
<div class="row column-seperation">
<div class="col-lg-12">
<h3>
<span class="semi-bold">Video</span>
</h3>
</div>
<div class="col-lg-12">


              <form action="../teacher/query/insert.php" method="POST" class="frm">
              <div class="form-group form-group-default input-group">
                <div class="form-input-group">
                <label>Title</label>
                  <input type="text" name="title" class="form-control change_inputs"  placeholder="XXXXXXXX"  required>
                </div>
              </div>

            <div class="form-group form-group-default" style="margin-top: 10px;">
              <label>Description</label>
              <textarea name="descip" required placeholder="XXXXXXXX" style="margin-bottom: 10px;height: 100px;" class="form-control change_inputs"></textarea>
            </div>

            <div class="form-group form-group-default" style="margin-top: 10px;">
              <label>Subjects</label>
               <select class="form-control select_class" name="select_subject" required style="cursor: pointer;">
                <option value="">Select Subjects </option>

                  <?php 
                                $sql0014 = mysqli_query($conn,"SELECT * FROM teacher_subject WHERE TEACH_ID = '$teach_id'");
                                while($row0014=mysqli_fetch_assoc($sql0014))
                                {
                                  echo $sub_id = $row0014['SUB_ID'];

                                  $sql0015 = mysqli_query($conn,"SELECT * FROM subject WHERE SUB_ID = '$sub_id'");
                                  while($row0015=mysqli_fetch_assoc($sql0015))
                                  {
                                    $sub_name = $row0015['SUBJECT_NAME'];
                                    $subject_id = $row0015['SUB_ID'];
                                    $level_id = $row0015['LEVEL_ID'];

                                    $sql0016 = mysqli_query($conn,"SELECT * FROM level WHERE LEVEL_ID = '$level_id'");
                                    while($row0016=mysqli_fetch_assoc($sql0016))
                                    {
                                      $level_name = $row0016['LEVEL_NAME'];
                                    }

                                    echo '<option value='.$subject_id.'>'.$sub_name.' ('.$level_name.')</option>';

                                  }

                                } 
                      
                       ?>
                
              </select>
            </div>

            <input type="hidden" id="teach_id1" value="<?php echo $teach_id; ?>">
            <input type="hidden" name="type" value="Video">
            <input type="hidden" name="teach_id" value="<?php echo $teach_id; ?>">



            <div class="form-group form-group-default" style="margin-top: 10px;">
              <label>Classes</label>
              <select class="form-control" id="select_subject0" name="clz" required style="cursor: pointer;">
                <option value="">Select Classes</option>
              </select>
            </div>

            <div class="form-group form-group-default" style="margin-top: 10px;">
              <label>Youtube Link </label>
              <input type="text" name="ytd" class="form-control change_inputs"  placeholder="Ex-https://www.youtube.com/watch?v=xxxxxxx" required>
            </div>

            <button type="submit" class="btn btn-success btn-block btn-lg" name="video_upload_btn" value="<?php echo $teach_id; ?>"><i class="pg-icon">tick_circle</i> Submit</button>
            <button type="reset" class="btn btn-default btn-block btn-lg"><i class="pg-icon">close</i> Clear</button>
          </form>
</div>
</div>
</div>
<div class="tab-pane <?php echo $audio; ?>" id="profile">
  <h3>
<span class="semi-bold">Audio</span>
</h3>

              <form action="../teacher/query/insert.php" method="POST" class="frm" enctype="multipart/form-data">
              <div class="form-group form-group-default input-group">
                <div class="form-input-group">
                <label>Title</label>
                  <input type="text" name="title" class="form-control change_inputs"  placeholder="XXXXXXXX" required>
                </div>
              </div>

            <div class="form-group form-group-default" style="margin-top: 10px;">
              <label>Description</label>
              <textarea name="descip" required placeholder="XXXXXXXX" style="margin-bottom: 10px;height: 100px;" class="form-control change_inputs"></textarea>
            </div>

            <div class="form-group form-group-default" style="margin-top: 10px;">
              <label>Subjects</label>
               <select class="form-control select_class" name="select_subject" required style="cursor: pointer;">
                <option value="">Select Subjects </option>

                  <?php 
                                $sql0014 = mysqli_query($conn,"SELECT * FROM teacher_subject WHERE TEACH_ID = '$teach_id'");
                                while($row0014=mysqli_fetch_assoc($sql0014))
                                {
                                  echo $sub_id = $row0014['SUB_ID'];

                                  $sql0015 = mysqli_query($conn,"SELECT * FROM subject WHERE SUB_ID = '$sub_id'");
                                  while($row0015=mysqli_fetch_assoc($sql0015))
                                  {
                                    $sub_name = $row0015['SUBJECT_NAME'];
                                    $subject_id = $row0015['SUB_ID'];
                                    $level_id = $row0015['LEVEL_ID'];

                                    $sql0016 = mysqli_query($conn,"SELECT * FROM level WHERE LEVEL_ID = '$level_id'");
                                    while($row0016=mysqli_fetch_assoc($sql0016))
                                    {
                                      $level_name = $row0016['LEVEL_NAME'];
                                    }

                                    echo '<option value='.$subject_id.'>'.$sub_name.' ('.$level_name.')</option>';

                                  }

                                } 
                      
                       ?>
                
              </select>
            </div>

            <input type="hidden" id="teach_id1" value="<?php echo $teach_id; ?>">
            <input type="hidden" name="type" value="Audio">

            <input type="hidden" name="teach_id" value="<?php echo $teach_id; ?>">


            <div class="form-group form-group-default" style="margin-top: 10px;">
              <label>Classes</label>
              <select class="form-control" id="select_subject1"  name="clz" required style="cursor: pointer;">
                <option value="">Select Classes</option>
              </select>
            </div>

            <div class="form-group form-group-default" style="margin-top: 10px;">
              <label>Upload File</label>

              <?php $file_types = 'audio/*'; ?>

              <input type="file" id="selectedFile0" style="display: none;" class="form-control" name="upload_file1" accept="<?php echo $file_types; ?>" required />

              <button type="button" class="btn btn-link" style="padding: 20px 20px 20px 20px;width: 100%;height: 80px;border:3px dashed #cccc;font-size: 20px;font-weight: bold;color:gray;" onclick="document.getElementById('selectedFile0').click();" id="select_btn0"><label id="msg0"><span class="fa fa-upload"></span><br> Upload </label></button>
            </div>

            <button type="submit" class="btn btn-success btn-block btn-lg save_btn active" value="<?php echo $teach_id; ?>" name="audio_upload_btn"><i class="pg-icon">tick_circle</i> Upload</button>
            <button type="reset" class="btn btn-default btn-block btn-lg save_btn"><i class="pg-icon">close</i> Clear</button>
          </form>

</div>



<div class="tab-pane <?php echo $document; ?>" id="messages">


  <h3>
<span class="semi-bold">Document</span>
</h3>

              <form action="../teacher/query/insert.php" method="POST" class="frm" enctype="multipart/form-data">
              <div class="form-group form-group-default input-group">
                <div class="form-input-group">
                <label>Title</label>
                  <input type="text" name="title" class="form-control change_inputs"  placeholder="XXXXXXXX" required>
                </div>
              </div>

            <div class="form-group form-group-default" style="margin-top: 10px;">
              <label>Description</label>
              <textarea name="descip" required placeholder="XXXXXXXX" style="margin-bottom: 10px;height: 100px;" class="form-control change_inputs"></textarea>
            </div>

            <div class="form-group form-group-default" style="margin-top: 10px;">
              <label>Subjects</label>
               <select class="form-control select_class" name="select_subject" required style="cursor: pointer;">
                <option value="">Select Subjects </option>

                  <?php 
                                $sql0014 = mysqli_query($conn,"SELECT * FROM teacher_subject WHERE TEACH_ID = '$teach_id'");
                                while($row0014=mysqli_fetch_assoc($sql0014))
                                {
                                  $sub_id = $row0014['SUB_ID'];

                                  $sql0015 = mysqli_query($conn,"SELECT * FROM subject WHERE SUB_ID = '$sub_id'");
                                  while($row0015=mysqli_fetch_assoc($sql0015))
                                  {
                                    $sub_name = $row0015['SUBJECT_NAME'];
                                    $subject_id = $row0015['SUB_ID'];
                                    $level_id = $row0015['LEVEL_ID'];

                                    $sql0016 = mysqli_query($conn,"SELECT * FROM level WHERE LEVEL_ID = '$level_id'");
                                    while($row0016=mysqli_fetch_assoc($sql0016))
                                    {
                                      $level_name = $row0016['LEVEL_NAME'];
                                    }

                                    echo '<option value='.$subject_id.'>'.$sub_name.' ('.$level_name.')</option>';

                                  }

                                } 
                      
                       ?>
                
              </select>
            </div>

            <input type="hidden" id="teach_id1" value="<?php echo $teach_id; ?>">
            <input type="hidden" name="type" value="Document">

            <input type="hidden" name="teach_id" value="<?php echo $teach_id; ?>">


            <div class="form-group form-group-default" style="margin-top: 10px;">
              <label>Classes</label>
              <select class="form-control"  id="select_subject2"  name="clz" required style="cursor: pointer;">
                <option value="">Select Classes</option>
              </select>
            </div>
            
            <div class="form-group form-group-default" style="margin-top: 10px;">
              <label>Public Share</label>
              <select class="form-control" name="public_share" required style="cursor: pointer;">
                <option value="checked">Yes</option>
                <option value="unchecked">No</option>
              </select>
            </div>

            <div class="form-group form-group-default" style="margin-top: 10px;">
              <label>Upload File</label>

              <?php $file_types = '.xlsx,.xls,.doc, .docx,.ppt, .pptx,.txt,.pdf'; ?>

              <input type="file" id="sel_file" style="display: none;" class="form-control" name="upload_file1" accept="<?php echo $file_types; ?>" required />

                    <button type="button" class="btn btn-link" style="padding: 20px 20px 20px 20px;width: 100%;height: 80px;border:3px dashed #cccc;font-size: 20px;font-weight: bold;color:white;" onclick="document.getElementById('sel_file').click();" id="select_btn1"><label id="msg1"><span class="fa fa-upload"></span><br> Upload </label></button>
            </div>

            <button type="submit" class="btn btn-success btn-block btn-lg save_btn active" value="<?php echo $teach_id; ?>" name="document_upload_btn"><i class="pg-icon">tick_circle</i> Upload</button>
            <button type="reset" class="btn btn-default btn-block btn-lg save_btn"><i class="pg-icon">close</i> Clear</button>
          </form>

<script type="text/javascript">
  let html1 = `
    <div style="min-width: 300px;">
      <div class="embed-responsive embed-responsive-16by9">
        <video class="embed-responsive-item" src=".../...mp4" loop muted></video>
      </div>
    </div>
    `;

    $('#popover').popover({
      trigger: 'manual',
      html: true,
      sanitize: false,
      content: html1
    });
</script>

</div>



<div class="tab-pane <?php echo $record; ?>" id="record">
<div class="row column-seperation">
<div class="col-lg-12">
<h3>
<span class="semi-bold">Recordings</span>
</h3>
</div>
<div class="col-lg-12">


              <form action="../teacher/query/insert.php" method="POST" class="frm" enctype="multipart/form-data">
              <div class="form-group form-group-default input-group">
                <div class="form-input-group">
                <label>Title</label>
                  <input type="text" name="title" class="form-control change_inputs"  placeholder="XXXXXXXX"  required>
                </div>
              </div>

            <div class="form-group form-group-default" style="margin-top: 10px;">
              <label>Description</label>
              <textarea name="descip" required placeholder="XXXXXXXX" style="margin-bottom: 10px;height: 100px;" class="form-control change_inputs"></textarea>
            </div>

            <div class="form-group form-group-default" style="margin-top: 10px;">
              <label>Subjects</label>
               <select class="form-control select_class" name="select_subject" required style="cursor: pointer;">
                <option value="">Select Subjects </option>

                  <?php 
                                $sql0014 = mysqli_query($conn,"SELECT * FROM teacher_subject WHERE TEACH_ID = '$teach_id'");
                                while($row0014=mysqli_fetch_assoc($sql0014))
                                {
                                  echo $sub_id = $row0014['SUB_ID'];

                                  $sql0015 = mysqli_query($conn,"SELECT * FROM subject WHERE SUB_ID = '$sub_id'");
                                  while($row0015=mysqli_fetch_assoc($sql0015))
                                  {
                                    $sub_name = $row0015['SUBJECT_NAME'];
                                    $subject_id = $row0015['SUB_ID'];
                                    $level_id = $row0015['LEVEL_ID'];

                                    $sql0016 = mysqli_query($conn,"SELECT * FROM level WHERE LEVEL_ID = '$level_id'");
                                    while($row0016=mysqli_fetch_assoc($sql0016))
                                    {
                                      $level_name = $row0016['LEVEL_NAME'];
                                    }

                                    echo '<option value='.$subject_id.'>'.$sub_name.' ('.$level_name.')</option>';

                                  }

                                } 
                      
                       ?>
                
              </select>
            </div>

            <input type="hidden" id="teach_id1" value="<?php echo $teach_id; ?>">
            <input type="hidden" name="type" value="Record">
            <input type="hidden" name="teach_id" value="<?php echo $teach_id; ?>">



            <div class="form-group form-group-default" style="margin-top: 10px;">
              <label>Classes</label>
              <select class="form-control" id="select_subject3" name="clz" required style="cursor: pointer;">
                <option value="">Select Classes</option>
              </select>
            </div>

            
            <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" href="#up_file"><i class="fa fa-upload"></i> Upload File</a></li>
              <li><a data-toggle="tab" href="#link" onclick="document.getElementById('my_selected_file').value = ''; uploaded_record_file();"><i class="fa fa-link"></i> Setup Link</a></li>
            </ul>

            <div class="tab-content" style="padding: 0px;">
              <div id="up_file" class="tab-pane in active">
                
                <div style="margin-top: 10px;">
                  <input type="file" id="my_selected_file" accept="video/*" name="upload_file" style="display: none;" onchange="uploaded_record_file();"/>

                  <div style="padding: 0px;margin: 0px;" class="pt-10" id="changed_btn" onclick="document.getElementById('my_selected_file').click();">
                      <button type="button" class="btn btn-block btn-sm" name="create_payment" id="upload_btn" style="padding: 15px 20px 15px 20px;border:3px dashed gray;font-size: 18px;outline: none;background-color: white;" value="Browse..."> <i class="fa fa-plus" style="font-size: 20px;"></i> <label style="font-weight: bold;font-size: 20px;padding-top: 10px;"> &nbsp; Upload Recording File</label>


                      </button>
                  </div>
                </div>
                
                <label id="required_msg"></label>

              </div>
              <div id="link" class="tab-pane fade">
                
                <div class="form-group form-group-default" style="margin-top: 10px;">
                  <label>Zoom Recording Link </label>
                  <input type="text" name="record_link" class="form-control change_inputs"  placeholder="Zoom Recording Link..">
                </div>

              </div>
            </div>

            

            <button type="submit" class="btn btn-success btn-block btn-lg" name="record_upload_btn" value="<?php echo $teach_id; ?>" onclick="check_input();"><i class="pg-icon">tick_circle</i> Submit</button>
            <button type="reset" class="btn btn-default btn-block btn-lg"><i class="pg-icon">close</i> Clear</button>
          </form>
</div>
</div>
</div>


</div>
</div>



      </div>
      <div class="col-md-3"></div>
    </div>


  </div>


<script type="text/javascript">
  
  $(document).ready(function(){  
   $('.select_class').change(function(){

     var s_class = $(this).val();
     var teach_id1 = $('#teach_id1').val();

     $.ajax({
      url:'../teacher/query/check.php',
      method:"POST",
      data:{upload_media:s_class,teach_id1:teach_id1},
      success:function(data)
      {
        $('#select_subject0').html(data);
        $('#select_subject1').html(data);
        $('#select_subject2').html(data);
        $('#select_subject3').html(data);
      }
     })
   

  });
 });

</script>


<script type="text/javascript">
  
  $(document).ready(function(){  
   
    $('#sel_file').change(function(){

      $('#select_btn1').css('background-color','#19ad79').css('border','3px solid black;');
      $('#msg1').html('<span class="fa fa-check-circle"></span><br> Success! <br> <small style="font-size:10px;">You can only upload one audio. </small> ').css('color','white');
    });

    $('#selectedFile0').change(function(){

      $('#select_btn0').css('background-color','#19ad79').css('border','3px solid black;');
      $('#msg0').html('<span class="fa fa-check-circle"></span><br> Success! <br> <small style="font-size:10px;">You can only upload one document. </small> ').css('color','white');
    });
 });

</script>
<script type="text/javascript">
  $('.save_btn').click(function(){
    
    var upload_btn0 = $('#selectedFile0').val();
    var upload_btn = $('#sel_file').val();
      
    if(empty(upload_btn0))
    {
      alert('Please select your audio file.');
    }else
    if(empty(upload_btn))
    {
      alert('Please select your document file.');
    }
   });
</script>

<script type="text/javascript">
  $('.navi').click(function(){
    $('.frm')[0].reset();
      
   });
</script>



<?php include('footer/footer.php'); ?>

<script type="text/javascript">

                    function check_input()
                    {
                      var my_selected_file = document.getElementById('my_selected_file').value;

                      //alert(my_selected_file)

                      if(my_selected_file == '')
                      {
                        document.getElementById('required_msg').innerHTML = '<h4 class="text-danger mt-4"><span class="fa fa-warning"></span> Please select upload file..</h4>';
                      }

                      if(my_selected_file !== '')
                      {
                        document.getElementById('required_msg').innerHTML = '';
                        document.getElementById('open_modal_btn').click();

                      }
                    }
                    

                    function uploaded_record_file()
                    {
                        //alert('selectedFile1')
                        var selectedFile1 = document.getElementById('my_selected_file').value;


                        if(selectedFile1 !== '')
                        {
                          
                          document.getElementById('required_msg').innerHTML = ''; //file required message

                          document.getElementById('changed_btn').innerHTML = '<button type="button" class="btn btn-block btn-sm" id="upload_btn" style="padding: 20px 20px 20px 20px;font-size: 18px;outline: none;background-color: #ff6500;color:white;" value="Browse..." > <i class="fa fa-spinner fa-pulse" style="font-size: 20px;"></i> <label style="font-weight: 700px;font-size: 20px;padding-top: 10px;">&nbsp; Changing..</label></button>';

                          setTimeout(function() { 

                            document.getElementById('changed_btn').innerHTML = '<button type="submit" class="btn btn-block btn-sm" name="create_payment" id="upload_btn" style="padding: 20px 20px 20px 20px;font-size: 22px;outline: none;background-color: #16a085;color:white;" value="Browse..." > <i class="fa fa-check-circle"></i> <label style="font-weight: 700px;font-size: 22px;padding-top: 10px;">&nbsp; Selected successfully</label></button>';
                                
                            document.getElementById('slip_upload_btn').disabled = false;

                          }, 1000);

                        }else
                        if(selectedFile1 == '')
                        {
                          document.getElementById('required_msg').innerHTML = ''; //file required message

                          document.getElementById('changed_btn').innerHTML = '<button type="submit" class="btn btn-block btn-sm" name="create_payment" id="upload_btn" style="padding: 20px 20px 20px 20px;border:3px dashed gray;font-size: 18px;outline: none;background-color: white;" value="Browse..."> <i class="fa fa-plus" style="font-size: 22px;"></i> <label style="font-weight: 1000px;font-size: 22px;padding-top: 10px;">&nbsp; Upload Recording File</label></button>';

                          document.getElementById('slip_upload_btn').disabled = true;

                        }

                        
                    }

                  </script>



<button type="button" data-toggle="modal" data-target="#submit_modal" id="open_modal_btn" style="display:none;">X</button>
                <div id="submit_modal" class="modal fade" role="dialog" style="margin-top: 100px;" data-backdrop="static" data-keyboard="false">
                  <div class="modal-dialog modal-sm">

                    <!-- Modal content-->
                    <div class="modal-content">
                      <div class="modal-body">
                          
                        <img src="../student/images/uploading.gif" width="100%" height="200px">
                        <h4 class="text-muted text-center" style="font-weight: bold;">Uploading..</h4>

                        <center><small class="text-center" style="text-align: center;">Please Don't close or reload this page.</small></center>

                      </div>
                    </div>

                  </div>
                </div>
