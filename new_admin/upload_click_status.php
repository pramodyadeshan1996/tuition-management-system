
<?php

$page = "Upload Click Status";
$folder_in = '0';

  include('header/header.php'); 
 ini_set( "display_errors", 0); 

 $admin_id = $_SESSION['ADMIN_ID'];


  ?>
  <style type="text/css">
    .btn_pulse {
  
  display: block;
  border-radius: 10%;
  cursor: pointer;
  animation: none;
  float: left;
  bottom: 5px;
  right: 5px;
  font-weight: bold;
  padding-top: 2px;
  text-align: center;
  color: white;
  font-size: 14px;
  animation: pulse 1.6s infinite;
}


@keyframes pulse {
  0% {
    -moz-box-shadow: 0 0 0 0 #7252d3;
    box-shadow: 0 0 0 0 #7252d3;
  }
  70% {
    -moz-box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
  }
  100% {
    -moz-box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
  }
}
  </style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


    <ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-top: 04%;margin-bottom: 0;">
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="New Classes"> Upload Click Status (Search)</li>
    </ol>




<div class="col-md-12" style="border-top: 1px solid #cccc;">
  
<div class="row" style="margin-top: 3%;">
<div class="col-md-4"></div>
<div class="col-md-4">
<div class=" container-fluid   container-fixed-lg bg-white" style="padding: 6px 20px 4px 20px;">
<div class="card card-transparent">
<div class="card-header ">
<div class="card-title">


  <h3 style="text-transform: capitalize;"> <span class="fa fa-search"></span> Search</h3>


</div>
<div class="clearfix"></div>
</div>

            <form action="search_click_status.php" method="POST">

            <div class="form-group form-group-default" style="margin-top: 10px;">
              <label>Teacher Name</label>
              <select class="form-control select_class" name="teacher_id" id="teacher_id123" required style="cursor: pointer;">
                <option value="">Select Teacher Name </option>

                  <?php 

                      $sql0015 = mysqli_query($conn,"SELECT * FROM teacher_details");
                      while($row0015=mysqli_fetch_assoc($sql0015))
                      {
                        $name = $row0015['F_NAME']." ".$row0015['L_NAME'];
                        $teach_id = $row0015['TEACH_ID'];

                        echo '<option value='.$teach_id.'>'.$name.'</option>';

                      }

                       ?>
                
              </select>

            </div>
            
            <div class="form-group form-group-default" style="margin-top: 10px;">
              <label>Classes</label>
              
              <select class="form-control select_class" name="class_id" id="level_clz" required style="cursor: pointer;">
                <option value="">Select Class Name </option>
                
              </select>

            </div>



            <script type="text/javascript">
  
                $(document).ready(function(){  

                  


                 $('#level_clz').change(function(){

                   var level_clz = $(this).val();
                   var teach_id = $('#teacher_id123').val();

                   $.ajax({
                    url:'../admin/query/check.php',
                    method:"POST",
                    data:{create_clz123:level_clz,teach_id:teach_id},
                    success:function(data)
                    {
                      //alert(data)
                      $('#subject').html(data);

                      
                    }
                   })
                 

                });

                 $('#subject').change(function(){

                   var sub_id = $(this).val();
                   var teach_id = $('#teacher_id123').val();

                   $.ajax({
                    url:'../admin/query/check.php',
                    method:"POST",
                    data:{select_clz123:sub_id,teach_id:teach_id},
                    success:function(data)
                    {
                      //alert(data)
                      $('#select_clz').html(data);
                      
                    }
                   })
                 

                });

                 
               });

              </script>

            <button type="submit" class="btn btn-success btn-block btn-lg" name="search_student" value="<?php echo $teach_id; ?>" style="margin-bottom: 10px;"><i class="pg-icon">search</i> Search</button>
            <center><a href="dashboard.php" data-toggle="tooltip" data-title="Back" style="text-align: center;">Back</a></center> 
            </form>
            </div>

</div>
<div class="col-md-4"></div>

</div>
</div>


<script type="text/javascript">
	setInterval(function() {
    $('blink').fadeIn(1300).fadeOut(1500);
}, 1000);
</script>

<script>
$(document).ready(function(){
  $("#search-table").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $(".myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

<script>
$(document).ready(function(){

  $("#up_rec").on("change", function() {
      $('.show_image').hide();
  });
});
</script>
<script type="text/javascript">
  setInterval(function() {
    $('.blink').fadeIn(1300).fadeOut(1500);
}, 1000);
</script>

<?php  include('footer/footer.php'); ?>


<script type="text/javascript">
  $(document).ready(function(){
    $('#teacher_id123').change(function(){

                   var teach_id = $('#teacher_id123').val();

                   $.ajax({
                    url:'../admin/query/check.php',
                    method:"POST",
                    data:{get_classes_data:teach_id},
                    success:function(data)
                    {
                      //alert(data)
                      $('#level_clz').html(data);

                      
                    }
                   })
                 

                });
  })
</script>