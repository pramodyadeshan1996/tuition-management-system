<?php

	$page = "Profile Setting";
	$folder_in = '0';

  include('header/header.php');

  $sql00272 = mysqli_query($conn,"SELECT * FROM institute WHERE INS_ID = '1'");
  while($row00272 = mysqli_fetch_assoc($sql00272))
  {
      $ins_name = $row00272['INS_NAME'];
      $ins_tp = $row00272['INS_TP'];
      $ins_mobile = $row00272['INS_MOBILE'];
      $ins_address = $row00272['INS_ADDRESS'];
      $staff_timer = $row00272['STAFF_TIMER'];

      $inst_picture = $row00272['PICTURE'];

      if($inst_picture == '0')
      {
        $inst_picture = 'ins.png';
      }
  }
  

?>

  <style type="text/css">
  	.container {
  position: relative;
  width: 100%;
}


.middle {
  transition: .5s ease;
  opacity: 0;
  position: absolute;
  top: 26%;
  left: 50%;
  transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  text-align: center;
  height: 100px;
}

.container:hover .image {
  opacity: 0.3;
}

.container:hover .middle {
  opacity: 1;
}

.text {
  
  color: white;
  font-size: 14px;
  height: 220px;
  width: 220px;
  padding: 85px 60px;
  border-radius: 120px;
}
  </style>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<div class="gallery" style="border:0px solid black;margin-top: 50px;">

<ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;border-bottom: 1px solid #cccc;margin-bottom: 04%;margin-bottom: 0;">
<li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
</ol>


<div class="col-md-12" style="padding-top: 20px;">

<a href="slider.php" class="btn btn-danger btn-lg mb-2" style="display: <?php if($position == 'S_ADMIN'){ echo "show;"; }else if($position == 'ADMIN'){ echo "show;"; }else if($position == 'STAFF') { echo "none;"; } ?>"><i class="fa fa-plus"></i>&nbsp; Setup Login Slider</a>
  <?php 


    if(!empty($_SESSION['click_tab22']))
    {
      $click_tab = $_SESSION['click_tab22'];

      if($click_tab == 'password')
      {
        $change_password = 'active';
        $profile_setting = '';
        $institute_setting = '';
        $timetable001 = '';

      }else
      if($click_tab == 'profile')
      {
        $change_password = '';
        $profile_setting = 'active';
        $institute_setting = '';
        $timetable001 = '';
      }else
      if($click_tab == 'institute')
      {
        $change_password = '';
        $profile_setting = '';
        $institute_setting = 'active';
        $timetable001 = '';
      }else
      if($click_tab == 'timetable')
      {
        $change_password = '';
        $profile_setting = '';
        $institute_setting = '';
        $timetable001 = 'active';
      }
    }else
    if(empty($_SESSION['click_tab22']))
    {
      $change_password = 'active';

      $profile_setting = '';

      $institute_setting = '';

      $timetable001 = '';

    }


   ?>

<div class="card card-transparent">

<ul class="nav nav-tabs nav-tabs-fillup" data-init-reponsive-tabs="dropdownfx">
<li class="nav-item">
<a href="#" data-toggle="tab" class="<?php echo $change_password; ?>" data-target="#slide1" onclick="click_tab('password');"><span>Change Password</span></a>
</li>
<li class="nav-item">
<a href="#" data-toggle="tab" class="<?php echo $profile_setting; ?>" data-target="#slide2" onclick="click_tab('profile');"><span>Profile Setting</span></a>
</li>

 <li class="nav-item">
<a href="#" data-toggle="tab" class="<?php echo $institute_setting; ?>" data-target="#slide3" style="display: <?php if($position == 'S_ADMIN'){ echo "show;"; }else if($position == 'ADMIN'){ echo "show;"; }else if($position == 'STAFF') { echo "none;"; } ?>" onclick="click_tab('institute');"><span>Institute Setting</span></a>
</li>

<li class="nav-item">
<a href="#" data-toggle="tab" class="<?php echo $timetable001; ?>" style="display: <?php if($position == 'S_ADMIN'){ echo "show;"; }else if($position == 'ADMIN'){ echo "show;"; }else if($position == 'STAFF') { echo "none;"; } ?>" data-target="#slide4" onclick="click_tab('timetable');"><span>Timetable</span></a>
</li>


</ul>


 <script type="text/javascript">
    function click_tab(tab_name)
    {
          
          $.ajax({  
          url:"../admin/query/check.php",  
          method:"POST",  
          data:{click_tab22:tab_name},  
          success:function(data){ 
             
             //alert(data)
           }           
         });

    }
 </script>

<div class="tab-content">
<div class="tab-pane slide-left <?php echo $change_password; ?>" id="slide1">
<div class="row column-seperation">

<div class="col-lg-3">
<h3>
	
</h3>
</div>

<div class="col-lg-6">
<form action="../admin/query/update.php" method="POST" autocomplete="off">
<input type="hidden" id="id" value="<?php echo $admin_id; ?>">


<h3 class="semi-bold" style="text-transform: uppercase;text-align: center;">change password</h3>

<div class="form-group" id="show_hide_password" style="position: relative;">
<h4 class="text-success">Current password</h4>
<input type="password" name="cp" placeholder="Current Password.." id="cp" class="form-control text-success" required>
<a href=""><i class="fa fa-eye-slash" aria-hidden="true" style="font-size: 18px;color: black;position: absolute;right:10px;top:50px;"></i></a>

</div>


<lable id="availability6" style="padding: 15px 10px 15px 0px;"></lable>

<div class="form-group" id="show_hide_password2" style="position: relative;">
<h4 class="text-danger">New password</h4>
<input type="password" name="npass" placeholder="New Password.." id="txtNewPassword" class="form-control text-danger" required>

<a href=""><i class="fa fa-eye-slash" aria-hidden="true" style="font-size: 18px;color: black;position: absolute;right:10px;top:50px;"></i></a>

</div>

<div class="form-group" id="show_hide_password3" style="position: relative;">
<h4 class="text-danger">Re-enter password</h4>
<input type="password" name="cpass" placeholder="Re-enter Password.." class="form-control text-danger" onkeyup="checkPasswordMatch();" id="txtConfirmPassword" required>

<a href=""><i class="fa fa-eye-slash" aria-hidden="true" style="font-size: 18px;color: black;position: absolute;right:10px;top:50px;"></i></a>
</div>

<span id="availability7" style="float: left;margin: 10px 0px 0px 0px;"></span>

<br>

<button class="btn btn-default btn-cons pull-right" style="margin-left: 10px;" type="reset"> Clear</button>
<button  type="submit"  name="submit_btn" value="<?php echo $admin_id; ?>" id="submit_btn" class="btn btn-success btn-cons pull-right"> Change</button>
</form>
</div>

<div class="col-lg-3">
<h3>
	
</h3>
</div>

</div>
</div>
<div class="tab-pane slide-left <?php echo $profile_setting; ?>" id="slide2">
	<div class="row">
<div class="col-lg-2"></div>
<div class="col-lg-8">

      

                 <div class="card">
<div class="card-header ">
<div class="card-title">


	<?php 
             $admin_id = $_SESSION['ADMIN_ID'];

             $sql = mysqli_query($conn,"SELECT * FROM admin_login WHERE ADMIN_ID = '$admin_id'");
              while($row = mysqli_fetch_assoc($sql))
              {
                
                $picture = $row['PICTURE'];

                $real_pic = $picture;

                $name = $row['ADMIN_NAME'];
                $gender = $row['GENDER'];

                if($picture == '0')
                 {
                    if($gender == 'Female')
                    {
                      if($picture == '0')
                      {
                          $dis = '0.1';
                      }
                      else
                      if($picture !== '0')
                      {
                          $dis = '5;color:white;';
                      }
                      $picture = 'female_admin.png';
                    }else
                    if($gender == 'Male')
                    {
                      if($picture == '0')
                      {
                          $dis = '0.1';
                      }
                      else
                      if($picture !== '0')
                      {
                          $dis = '5;color:white;';
                      }
                      $picture = 'male_admin.jpg';
                    }
                 }
              }

               ?>




</div>
<h3 class="semi-bold" style="text-transform: uppercase;text-align: center;">profile setting</h3>

</div>
<div class="card-body">
<div class="row clearfix">
<div class="col-md-12">
	<div class="row">
	<div class="col-md-4"></div>
	<div class="col-md-4">
		
                  <form action="../admin/query/update.php" method="POST" enctype="multipart/form-data">

                  	<div class="container">
						<center><img src="../admin/images/profile/<?php echo $picture; ?>" class="rounded-circle image-responsive" id="show_image" style="height: 250px;border:1px solid #cccc;width: 100%;border: 10px solid white;">
						<div class="middle">
	   						<div class="text">

                    <div class="col-md-12" style="margin-bottom: 12px;border-radius: 80px;"><span class="fa fa-camera" onclick ="javascript:document.getElementById('imagefile').click();" style="font-size: 40px;cursor: pointer;"></span></div>
                    <?php 
                    
                      ?>
                     
                    <div class="col-md-12"><a href="../admin/query/update.php?remove_img=<?php echo $admin_id; ?>&&recent_img=<?php echo $picture; ?>" onclick="return confirm('Are you sure remove profile picture?')"><span class="fa fa-times-circle" style="font-size: 30px;cursor: pointer;opacity:  <?php echo $dis; ?>;"></span></a></div>

                  
                 
	   							

                </div>
	 					</div></center>
					</div>
                  	

                      <input type="hidden" name="recent_img" value="<?php if($picture == 'female_admin.png' || $picture == 'male_admin.jpg'){ echo "0"; }else{ echo $picture;} ?>">
                      <input id = "imagefile" type="file" style='visibility: hidden;' name="img" accept="image/*" />

                      <button type="button" class="btn btn-info btn-sm" style="border-radius: 80px;outline: none;display: none;" id="select_btn"><span class="fa fa-upload"></span> Upload</button>
                      <center>
                      <button type="submit" name="upload_img" id="upload" class="btn btn-success btn-sm" style="outline: none;display: none;margin-bottom: 10px;" value="<?php echo $admin_id; ?>"> <span class="fa fa-check-circle"></span>&nbsp;Set Profile</button></center>
                  </form>

</div>
	<div class="col-md-4"></div>
	</div>

</div>

<form  action="../admin/query/update.php" method="POST" id="form-personal" role="form" autocomplete="off">


<div class="row">
<div class="col-md-6">
<div class="form-group form-group-default input-group">
<div class="form-input-group" style="height: 62px;">
<label>Full Name</label>
<input type="text" name="name" class="form-control change_inputs" value="<?php echo $name; ?>" pattern="[a-zA-Z. ]+"  required>
</div>
</div>
</div>

<div class="col-md-6">
<div class="form-group form-group-default input-group">
<div class="form-input-group">
<label>Gender</label>

<div class="row">
  <?php 
    if($gender == 'Male')
    {?>



      <div class="col-md-6" style="padding-left: 6px;">

        <label><span class="fa fa-male"></span> Male : <input type="radio" checked="checked" name="gender" value="Male" class="change_inputs"></label>
      </div>
      <div class="col-md-6" style="padding-left: 6px;">

        <label style="padding-left: 10px;margin-bottom: 10px;"><span class="fa fa-female"></span> Female : <input type="radio" name="gender" value="Female" class="change_inputs"> </label>

      </div>
      <?php
    }else
    if($gender == 'Female')
    {?>



      <div class="col-md-6" style="padding-left: 6px;"><label><span class="fa fa-male"></span> Male : <input type="radio"name="gender" value="Male" class="change_inputs"></label></div>

      <div class="col-md-6" style="padding-left: 6px;"><label style="padding-left: 10px;margin-bottom: 10px;"><span class="fa fa-female"></span> Female : <input type="radio" checked="checked"  name="gender" value="Female" class="change_inputs"> </label></div>
      <?php
    }
   ?>
</div>

</div>
</div>
</div>

</div>

<div class="row" style="display: <?php if($position == 'S_ADMIN'){ echo "show;"; }else if($position == 'ADMIN'){ echo "show;"; }else if($position == 'STAFF') { echo "none;"; } ?>" onclick="click_tab('institute');">

<div class="col-md-6" style="padding-left:6px;padding-right:6px;">

    <div class="row">
      <div class="col-md-6">
        
        <div class="form-group form-group-default input-group">
          <div class="form-input-group">
            <label onclick="document.getElementById('switchColorOpt').click();" style="cursor:pointer;">Switch to Free Mode</label>
            <?php 
              
              $sql00270 = mysqli_query($conn,"SELECT * FROM institute WHERE INS_ID = '1'");
                  while($row00270 = mysqli_fetch_assoc($sql00270))
                  {
                      $free0 = $row00270['FREE'];
                      $safty_link_btn = $row00270['SAFETY_LINK'];


                      if($free0 == 'Yes')
                      {
                        $check0 = 'checked';
                        $free_mode_status = 'Enabled';
                      }else
                      if($free0 == 'No')
                      {
                        $check0 = '';
                        $free_mode_status = 'Disabled';
                      }
                  }

             ?>

            <div class="col-md-12">
              <div class="form-check form-check-inline switch switch-lg success" style="cursor:pointer;">
                <input type="checkbox" id="switchColorOpt" value="Yes" <?php echo $check0; ?>>
                <label for="switchColorOpt" class="text-danger" style="text-transform: capitalize;"></label>

                <strong class="text-right" id="f_mode_message" onclick="document.getElementById('switchColorOpt').click();"><?php echo $free_mode_status; ?></strong>

              </div>
            </div>
          </div>
        </div>

      </div>
      <div class="col-md-6" style="display:show;">
        <!-- Free Mode , En/Disable Material Status -->

        <div class="form-group form-group-default input-group">
          <div class="form-input-group">
            <label style="font-size:13px;cursor:pointer;" onclick="document.getElementById('free_mode_material').click();">Switch to Free Mode Material</label>
            <?php 
              
              $sql00270 = mysqli_query($conn,"SELECT * FROM institute WHERE INS_ID = '1'");
                  while($row00270 = mysqli_fetch_assoc($sql00270))
                  {
                      $free_mode_material_status = $row00270['FREE_MODE_MATERIAL'];


                      if($free_mode_material_status == '1')
                      {
                        $check02 = 'checked';
                        $free_material_status_message = 'Enabled';
                      }else
                      if($free_mode_material_status == '0')
                      {
                        $check02 = '';
                        $free_material_status_message = 'Disabled';
                      }
                  }

             ?>

            <div class="col-md-12">
              <div class="form-check form-check-inline switch switch-lg success" style="cursor:pointer;">
                <input type="checkbox" id="free_mode_material" value="1" <?php echo $check02; ?>>
                <label for="free_mode_material" class="text-danger" style="text-transform: capitalize;cursor: pointer;"></label>

                <strong class="text-right" id="f_material_message" onclick="document.getElementById('free_mode_material').click();"><?php echo $free_material_status_message; ?></strong>
              </div>
            </div>
          </div>
        </div>
        <!-- Free Mode , En/Disable Material Status -->


      </div>

  </div>

</div>


<div class="col-md-6">

<div class="form-group form-group-default input-group">

<div class="form-input-group">
<label>Hide Student Login Register Button</label>
<?php 
  
  $sql00270 = mysqli_query($conn,"SELECT * FROM institute WHERE INS_ID = '1'");
      while($row00270 = mysqli_fetch_assoc($sql00270))
      {
          $reg_btn = $row00270['ENABLE_REGISTER_BTN'];


          if($reg_btn == '1')
          {
            $check1 = 'checked';
          }else
          if($reg_btn == '0')
          {
            $check1 = '';
          }
      }

 ?>

<div class="col-md-12">
<div class="form-check form-check-inline switch switch-lg success">
<input type="checkbox" id="switchColorOpt002" value="Yes" <?php echo $check1; ?>>
<label for="switchColorOpt002" class="text-danger" style="text-transform: capitalize;"></label>
</div>
</div>
</div>
</div>
</div>


</div>

<div class="row">
  <div class="col-md-6">

    <div class="form-group form-group-default input-group">

      <div class="form-input-group">
      <label>Hide Seminar Login Button</label>
      <?php 
        
        $sql00272 = mysqli_query($conn,"SELECT * FROM institute WHERE INS_ID = '1'");
            while($row00272 = mysqli_fetch_assoc($sql00272))
            {
                $sem_show_btn = $row00272['SEMINAR_BTN'];

                if($sem_show_btn == '1')
                {
                  $check12 = 'checked';
                }else
                if($sem_show_btn == '0')
                {
                  $check12 = '';
                }
            }

       ?>

        <div class="col-md-12">
          <div class="form-check form-check-inline switch switch-lg success">
            <input type="checkbox" id="sem_login_btn" value="1" <?php echo $check12; ?>>
            <label for="sem_login_btn" class="text-danger" style="text-transform: capitalize;"></label>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-md-6">

    <div class="form-group form-group-default input-group">

      <div class="form-input-group" style="padding:9px 10px 9px 10px;">
        <div class="col-md-12">
          <?php 

            if($safty_link_btn == '1')
            {
              if($position == 'S_ADMIN' || $position == 'ADMIN')
              {?>
              <a href="admin_zoom.php" class="btn btn-primary btn-lg btn-block" style="padding:14px 14px;"><i class="fa fa-plus"></i>&nbsp; Create Zoom Account</a>

              <?php 
              }
            }else
            if($safty_link_btn == '0')
            {
              if($position == 'S_ADMIN')
              {?>
              <a href="admin_zoom.php" class="btn btn-primary btn-lg btn-block" style="padding:14px 14px;"><i class="fa fa-plus"></i>&nbsp; Create Zoom Account</a>

              <?php 
              }
            }


             ?>
        </div>
      </div>
    </div>
  </div>

</div>

<?php 
  
  $show_staff_free_timer = 'none';
  $col = '12';


  if($position == 'S_ADMIN' || $position == 'ADMIN')
  {
    $show_staff_free_timer = 'show';
    $col = '12';
  }

 ?>

<div class="row">
  
  <div class="col-md-<?php echo $col; ?>" style="display: none;">
    <div class="form-group form-group-default input-group">
      <div class="form-input-group">
        <label>Switch Safe link</label>
          <?php 
                        
            $sql00270 = mysqli_query($conn,"SELECT * FROM institute WHERE INS_ID = '1'");
                while($row00270 = mysqli_fetch_assoc($sql00270))
                {
                  $safe_link = $row00270['SAFETY_LINK'];
                  $show_tg_btn = $row00270['SHOW_TG'];

                  if($safe_link == '1')
                  {
                    $active_safe_link = 'checked';
                  }else
                  if($safe_link == '0')
                  {
                    $active_safe_link = '';
                  }

                  if($show_tg_btn == '1')
                  {
                    $active_tg_btn = 'checked';
                  }else
                  if($show_tg_btn == '0')
                  {
                    $active_tg_btn = '';
                  }

                }

          ?>

      <div class="col-md-12">
        <div class="form-check form-check-inline switch switch-lg success" style="width:100%;">
          <div class="form-check form-check-inline switch switch-lg success" style="width:100%;">
            <input type="checkbox" id="active_safe_link" value="1" <?php echo $active_safe_link; ?>>
            <label for="active_safe_link" class="text-danger" style="text-transform: capitalize;cursor: pointer;"></label>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>

<div class="col-md-6" style="display: <?php echo $show_staff_free_timer; ?>;">
  <div class="form-group form-group-default input-group">
    <div class="form-input-group" style="height: 70px;">
      <label>Setup Staff Free Time (Minute)</label>
      <?php 
        
        $timer_minute = 0;
        $timer_minute = $staff_timer/(1000*60);

      ?>
      <input type="text" name="staff_free_timer" class="form-control change_inputs number" value="<?php echo $timer_minute; ?>"  required>
    </div>
  </div>
</div>

<div class="col-md-6" style="display: <?php  if($position == 'S_ADMIN' || $position == 'ADMIN'){ echo 'show;';}else{ echo "none;"; } ?>;">
  <div class="form-group form-group-default input-group">
        <div class="form-input-group">
          <label>Switch Show Telegram Button</label>

        <div class="col-md-12">
          <div class="form-check form-check-inline switch switch-lg success" style="width:100%;">
            <div class="form-check form-check-inline switch switch-lg success" style="width:100%;">
              <input type="checkbox" id="active_show_tg" value="1" <?php echo $active_tg_btn; ?>>
              <label for="active_show_tg" class="text-danger" style="text-transform: capitalize;cursor: pointer;"></label>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>



<div class="clearfix"></div>
<div class="row m-t-25">
<div class="col-xl-6 p-b-10">
<p class="small-text hint-text">Click the Update button to change all the data you have changed. (ඔබ වෙනස් කල සියලු දත්තයන් වෙනස් කිරීමට Update බොත්තම ඔබන්න.)</p>
</div>
<div class="col-xl-6">
<button aria-label="" class="btn btn-primary pull-right btn-lg btn-block" type="submit" name="update_admin" value="<?php echo $admin_id; ?>"><i class="pg-icon">edit</i>&nbsp; Update
</button>
</div>
</div>
</form>



</div>
</div>

</div>

<div class="col-lg-2"></div>


</div>
</div>
</div>

<?php 
  
  $hidde_ins_settings = 'none';

  if($position == 'S_ADMIN')
  {
    $hidde_ins_settings = 'show';
  }

 ?>

<div class="tab-pane slide-left <?php echo $institute_setting; ?>" id="slide3">
  <div class="row">
<div class="col-lg-2"></div>
<div class="col-lg-8">

      

                 <div class="card">
<div class="card-header ">
<div class="card-title">

</div>
<h3 class="semi-bold" style="text-transform: uppercase;text-align: center;">Institute setting</h3>

</div>
<div class="card-body">
<div class="row clearfix">
<div class="col-md-12">
  <div class="row">
  <div class="col-md-4"></div>
  <div class="col-md-4">
    
                  <form action="../admin/query/update.php" method="POST" enctype="multipart/form-data">

                    <div class="container">
            <center><img src="../admin/images/institute/<?php echo $inst_picture; ?>" class="rounded-circle image-responsive" id="show_image2" style="height: 156px;border:1px solid #cccc;width: 76%;border: 0px solid white;">
            <div class="middle">
                <div class="text">

                    <div class="col-md-12" style="margin-bottom: 12px;"><span class="fa fa-camera" onclick ="javascript:document.getElementById('imagefile2').click();" style="font-size: 40px;cursor: pointer;"></span></div>
                    <?php 
                    if($inst_picture == 'ins.png')
                    {
                        $dis2 = '0.2;white;cursor:not-allowed;';
                        $disabled="disabled";
                    }
                    else
                    if($inst_picture !== 'ins.png')
                    {
                        $dis2 = '5;color:white;';
                        $disabled="";
                    }
                      ?>
                     
                    <div class="col-md-12"><a href="../admin/query/update.php?remove_img2=<?php echo $admin_id; ?>&&recent_img2=<?php echo $inst_picture; ?>" onclick="return confirm('Are you sure remove profile picture?')" class="<?php echo $disabled; ?>"><span class="fa fa-times-circle" style="font-size: 30px;cursor: pointer;opacity:  <?php echo $dis2; ?>;"></span></a></div>

                  
                 
                  

                </div>
            </div></center>
          </div>
                    

                      <input type="hidden" name="recent_img2" value="<?php if($inst_picture == 'ins.png'){ echo "0"; }else{ echo $inst_picture;} ?>">
                      <input id = "imagefile2" type="file" style='visibility: hidden;' name="img2" accept="image/*" />

                      <button type="button" class="btn btn-info btn-sm" style="border-radius: 80px;outline: none;display: none;" id="select_btn2"><span class="fa fa-upload"></span> Upload</button>
                      <center>
                      <button type="submit" name="upload_img2" id="upload2" class="btn btn-success btn-sm" style="outline: none;display: none;margin-bottom: 10px;" value="<?php echo $admin_id; ?>"> <span class="fa fa-check-circle"></span>&nbsp;Set Profile</button></center>
                  </form>

</div>
  <div class="col-md-4"></div>
  </div>

</div>

<form  action="../admin/query/update.php" method="POST" id="form-personal" role="form" autocomplete="off">

<?php 
  
  $sql0027 = mysqli_query($conn,"SELECT * FROM institute WHERE INS_ID = '1'");
      while($row0027 = mysqli_fetch_assoc($sql0027))
      {
          $ins_name = $row0027['INS_NAME'];
          $ins_tp = $row0027['INS_TP'];
          $ins_mobile = $row0027['INS_MOBILE'];
          
          $ins_address = $row0027['INS_ADDRESS'];
          $web_address = $row0027['WEB'];
          $free = $row0027['FREE'];
          $reg_code = $row0027['REG_CODE'];

          $sms_active = $row0027['SMS_ACTIVE'];
          $single_sms = $row0027['SINGLE_SMS'];

          $without_check = $row0027['REG_WITH_ACTIVE'];

          $check_active_exam = $row0027['EXAM_TAB'];

          $timetable = $row0027['TIMETABLE'];

          $check_active_suspend = $row0027['SUSPEND'];
          $student_profile_color = $row0027['BG_COLOR'];

          $check_active_limit = $row0027['CLZ_LIMIT'];
          $no_of_limit = $row0027['NO_LIMIT'];

          $switch_free_days = $row0027['FREE_WEEK'];
          $no_free_week = $row0027['NO_FREE_WEEK'];
          $sms_footer = $row0027['SMS_FOOTER'];
          $admin_otp = $row0027['ADMIN_OTP'];


          if($free == 'Yes')
          {
            $check = 'checked';
          }else
          if($free == 'No')
          {
            $check = '';
          }

          if($web_address == '0')
          {
            $web_address = '';
          }



          if($sms_active == '1')
          {
            $check_sms_active = 'checked';
          }else
          if($sms_active == '0')
          {
            $check_sms_active = '';
          }

          if($ins_tp == '0')
          {
            $ins_tp = '0';
          }

          if($single_sms == '1')
          {
            $single_sms_check = 'checked';
          }else
          if($single_sms == '0')
          {
            $single_sms_check = '';
          }

          if($without_check == '1')
          {
            $active_without_check = 'checked';
          }else
          if($without_check == '0')
          {
            $active_without_check = '';
          }

          if($check_active_exam == '1')
          {
            $active_exam = 'checked';
          }else
          if($check_active_exam == '0')
          {
            $active_exam = '';
          }


          if($check_active_suspend == '1')
          {
            $active_suspend = 'checked';
          }else
          if($check_active_suspend == '0')
          {
            $active_suspend = '';
          }


          if($check_active_limit == '1')
          {
            $active_limit = 'checked';
          }else
          if($check_active_limit == '0')
          {
            $active_limit = '';
          }
          

          if($switch_free_days == '1')
          {
            $active_free_days = 'checked';
          }else
          if($switch_free_days == '0')
          {
            $active_free_days = '';
          }

          if($admin_otp == '1')
          {
            $active_otp = 'checked';
          }else
          if($admin_otp == '0')
          {
            $active_otp = '';
          }

      }

 ?>

<div class="row">
<div class="col-md-12">
<div class="form-group form-group-default input-group">
<div class="form-input-group" style="height: 62px;">
<label>Instute Name</label>
<input type="text" name="name" class="form-control change_inputs" value="<?php echo $ins_name; ?>"  required>
</div>
</div>
</div>
</div>

<div class="row">
<div class="col-md-6">
<div class="form-group form-group-default input-group">
<div class="form-input-group">
<label>Telephone No</label>
<input class="form-control change_inputs" type="text" maxlength="10" name="tp" id="number" value="<?php echo $ins_tp; ?>" placeholder="0xxxxxxxxx" style="margin-bottom: 10px;">
</div>
</div>
</div>

<div class="col-md-6">
<div class="form-group form-group-default input-group">
<div class="form-input-group">
<label>Mobile No</label>
<input class="form-control change_inputs" type="text" maxlength="10"  name="mobile" id="number" value="<?php echo $ins_mobile; ?>" placeholder="0xxxxxxxxx" style="margin-bottom: 10px;">
</div>
</div>
</div>

</div>

<div class="row">
<div class="col-md-12">
<div class="form-group form-group-default">
<label>Address</label>
<textarea name="address" required placeholder="xxxxxxxx" style="margin-bottom: 10px;" class="form-control change_inputs"><?php echo $ins_address; ?></textarea>
</div>
</div>
</div>

<div class="row">
<div class="col-md-12">
<div class="form-group form-group-default">
<label>Web Address</label>
<textarea name="web" placeholder="https://www.xxxxxxxx" style="margin-bottom: 10px;" data-toggle="tooltip" data-title="You should enter your institute home page web address. *** &quot; https:// &quot; please use for your web address.***" class="form-control change_inputs"><?php echo $web_address; ?></textarea>
</div>
</div>
</div>

<div class="row" style="display: <?php echo $hidde_ins_settings; ?>">

<div class="col-md-6">
<div class="form-group form-group-default input-group">
<div class="form-input-group" style="height: 62px;">
<label>Register Code (Ex - <b class="text-danger">PD</b>000021)</label>
<input type="text" name="reg_code" class="form-control change_inputs" value="<?php echo $reg_code; ?>"  required data-toggle="tooltip" data-title="Register Code">
</div>
</div>
</div>


<div class="col-md-6">
<div class="form-group form-group-default input-group">
<div class="form-input-group" style="height: 62px;">
<label>Student Profile Color Code (Ex - #2bcbba)</label>
<input type="text" name="student_profile_color" class="form-control change_inputs" value="<?php echo $student_profile_color; ?>"  required data-toggle="tooltip" data-title="Student Profile Color">
</div>
</div>
</div>

</div>


<div class="row" style="display: <?php echo $hidde_ins_settings; ?>">
<div class="col-md-12">
<div class="form-group form-group-default input-group">
<div class="form-input-group">
<label>Switch to active check without registered students</label>

<div class="col-md-12">
<div class="form-check form-check-inline switch switch-lg success">
<input type="checkbox" id="check_active_register" value="1" <?php echo $active_without_check; ?>>
<label for="check_active_register" class="text-danger" style="text-transform: capitalize;cursor: pointer;"></label>



</div>

</div>
</div>
</div>
</div>


</div>



<div class="row" style="display: <?php echo $hidde_ins_settings; ?>">
<div class="col-md-6">
<div class="form-group form-group-default input-group">
<div class="form-input-group">
<label>Switch to Start Premium SMS Service (Multiple SMS)</label>

<div class="col-md-12">
<div class="form-check form-check-inline switch switch-lg success">
<input type="checkbox" id="sms_active" value="1" <?php echo $check_sms_active; ?>>
<label for="sms_active" class="text-danger" style="text-transform: capitalize;cursor: pointer;"></label>



</div>
<button type="button" class="btn btn-danger pull-right" style="margin-right:10px;" data-toggle="modal" data-target="#premium" id="sms_active_btn">Setup SMS Gateway</button>

</div>
</div>
</div>
</div>

<div class="col-md-6">
<div class="form-group form-group-default input-group">
<div class="form-input-group">
<label>Switch to Start Custom SMS Service (Single SMS)</label>

<div class="col-md-12">
<div class="form-check form-check-inline switch switch-lg success"><input type="checkbox" id="single_sms_active" value="1" <?php echo $single_sms_check; ?>>
<label for="single_sms_active" class="text-danger" style="text-transform: capitalize;cursor: pointer;"></label>



</div>
<button type="button" class="btn btn-danger pull-right" style="margin-right:10px;" data-toggle="modal" data-target="#single_sms" id="single_sms_active_btn">Setup SMS Gateway</button>

</div>
</div>
</div>
</div>
</div>


<div class="row" style="display: <?php echo $hidde_ins_settings; ?>">
<div class="col-md-6">
<div class="form-group form-group-default input-group">
<div class="form-input-group">
<label>Switch to Enable class for student access limitation</label>

<div class="col-md-12">
<div class="form-check form-check-inline switch switch-lg success">
<input type="checkbox" id="active_limit" value="1" <?php echo $active_limit; ?>>
<label for="active_limit" class="text-danger" style="text-transform: capitalize;cursor: pointer;"></label>



</div>

</div>
</div>
</div>
</div>
<div class="col-md-6">
<div class="form-group form-group-default input-group" style="height: 60px;">
<div class="form-input-group">
  <label>Setup No of student limitation</label>
  
<input type="number" class="form-control" min="0" id="setup_limit001" onblur="setup_limitation();" value="<?php echo $no_of_limit; ?>">

</div>
</div>
</div>
</div>




<div class="row" style="display: <?php echo $hidde_ins_settings; ?>">
<div class="col-md-6">
<div class="form-group form-group-default input-group">
<div class="form-input-group">
<label>Switch to Enable paid student for next days for free</label>

<div class="col-md-12">
<div class="form-check form-check-inline switch switch-lg success">
<input type="checkbox" id="active_free_days" value="1" <?php echo $active_free_days; ?>>
<label for="active_free_days" class="text-danger" style="text-transform: capitalize;cursor: pointer;"></label>



</div>

</div>
</div>
</div>
</div>
<div class="col-md-6">
<div class="form-group form-group-default input-group" style="height: 60px;">
<div class="form-input-group">
  <label>Setup No of days free period for paid student</label>
  
<input type="number" class="form-control" min="0" id="setup_free_days002" onblur="setup_free_days();" value="<?php echo $no_free_week; ?>">

</div>
</div>
</div>
</div>




<div class="row" style="display: <?php echo $hidde_ins_settings; ?>">
<div class="col-md-6">
<div class="form-group form-group-default input-group">
<div class="form-input-group">
<label>Switch to Enable Online Examination Option</label>

<div class="col-md-12">
<div class="form-check form-check-inline switch switch-lg success">
<input type="checkbox" id="active_exam" value="1" <?php echo $active_exam; ?>>
<label for="active_exam" class="text-danger" style="text-transform: capitalize;cursor: pointer;"></label>



</div>

</div>
</div>
</div>
</div>
<div class="col-md-6">
<div class="form-group form-group-default input-group">
<div class="form-input-group">
<label>Switch to Suspend System</label>

<div class="col-md-12">
<div class="form-check form-check-inline switch switch-lg success">
<input type="checkbox" id="active_suspend" value="1" <?php echo $active_suspend; ?>>
<label for="active_suspend" class="text-danger" style="text-transform: capitalize;cursor: pointer;"></label>



</div>

</div>
</div>
</div>
</div>
</div>

<div class="row" style="display: <?php echo $hidde_ins_settings; ?>">

<div class="col-md-6">
<div class="form-group form-group-default input-group">
<div class="form-input-group">
<label>Change SMS Footer</label>

<div class="col-md-12">
<div class="form-check form-check-inline switch switch-lg success" style="width:100%;">
  <textarea class="form-control change_inputs" name="sms_footer" placeholder="Enter SMS Footer"><?php echo $sms_footer; ?></textarea>

</div>

</div>
</div>
</div>
</div>

<div class="col-md-6">
<div class="form-group form-group-default input-group">
<div class="form-input-group">
<label>Switch Admin Panel OTP Feature</label>

<div class="col-md-12">
<div class="form-check form-check-inline switch switch-lg success" style="width:100%;">
  <input type="checkbox" id="active_admin_otp" value="1" <?php echo $active_otp; ?>>
  <label for="active_admin_otp" class="text-danger" style="text-transform: capitalize;cursor: pointer;"></label>

</div>

</div>
</div>
</div>
</div>

</div>





<script>
  $(document).ready(function(){

    if($("#sms_active").is(':checked'))
    {
      $("#sms_active_btn").show();  // checked
    }
    else{
      $("#sms_active_btn").hide();  // unchecked
    }


    $('#sms_active').change(function(){


    if($("#sms_active").is(':checked'))
    {
      $("#sms_active_btn").show();  // checked
    }
    else{
      $("#sms_active_btn").hide();  // unchecked
    }
  });

  });

   $(document).ready(function(){


    if($("#single_sms_active").is(':checked'))
    {
      $("#single_sms_active_btn").show();  // checked
    }
    else{
      $("#single_sms_active_btn").hide();  // unchecked
    }


    $('#single_sms_active').change(function(){


    if($("#single_sms_active").is(':checked'))
    {
      $("#single_sms_active_btn").show();  // checked
    }
    else{
      $("#single_sms_active_btn").hide();  // unchecked
    }
  });

  });
</script>



<div class="clearfix"></div>
<div class="row m-t-25">
<div class="col-xl-6 p-b-10">
<p class="small-text hint-text">Click the Update button to change all the data you have changed. (ඔබ වෙනස් කල සියලු දත්තයන් වෙනස් කිරීමට Update බොත්තම ඔබන්න.)</p>
</div>
<div class="col-xl-6">
<button aria-label="" id="up_btn" class="btn btn-primary pull-right btn-lg btn-block" type="submit" name="update_institute"><i class="fa fa-pencil"></i>&nbsp; Update
</button>
</div>
</div>
</form>
</div>
</div>

</div>

<div class="col-lg-2"></div>


</div>
</div>
</div>


<div class="tab-pane slide-left <?php echo $timetable001; ?>" id="slide4">
<div class="row column-seperation">

<div class="col-lg-5" style="margin-top: 6px;">

  <div class="col-lg-12" style="border:1px solid #cccc;padding: 10px 10px 10px 10px;height: 500px;">

    <!-- <h4 class="text-muted" style="padding-bottom: 0px;">Timetable</h4> -->

    <div id="status">

      <?php 

        if($timetable > 0)
        {?>
          
            <img src="../admin/images/timetable/<?php echo $timetable; ?>" style="width: 100%;height: 100%;">
          

        <?php 
        }else
        if($timetable == '0')
        {?>
          <center>
            <img src="../admin/images/timetable/not_available.jpg" style="margin-top: 20%;width: 200px;height: 200px;">
          </center>
        <?php 
        }

       ?>
     </div>

  </div>
</div>

<div class="col-lg-7" style="margin-top: 6px;">

        <div class="col-lg-12" style="border:1px solid #cccc;padding: 10px 10px 10px 10px;height: 500px;">

            <form action="../admin/query/update.php" method="POST" enctype="multipart/form-data">


            <div style="margin-top: 6px;" id="td_btn">

              <input type="file" id="table_image" accept="image/jpg,image/jpeg,image/png" name="upload_file" class="fileToUpload" style="display: none;" onchange="upload_timetable();"/>
              


              <div class="row">
                <div class="col-lg-2"></div>
                <div class="col-lg-8">
                  <h3 class="text-muted text-center">Upload File</h3>
                  <center>
                    <div class="col-md-12">
                      <img src="../admin/images/timetable/timetable_svg.svg" style="width: 200px;height: 200px;">
                    </div>
                  </center>
                  <div class="col-md-12" style="padding: 0px;margin: 0px;" class="pt-10" id="msg_alert">

                <?php 

                  if($timetable == '0')
                  {?>
                    <button type="button" class="btn btn-block btn-sm" name="update_payment" id="reject_upload_btn" onclick="document.getElementById('table_image').click();" style="padding: 20px 20px 20px 20px;border:3px dashed gray;font-size: 18px;outline: none;background-color: white;color:#545454;margin-top: 40px;" value="Browse..."> <i class="fa fa-plus" style="font-size: 16px"></i>&nbsp; <label style="font-weight: bold;font-size: 16px;padding-top: 4px;">Upload Timetable</label></button>
                  <?php
                  }else
                  if($timetable !== '0')
                  {?>
                    <button type="button" class="btn btn-block btn-sm" name="update_payment" id="reject_upload_btn" onclick="document.getElementById('table_image').click();" style="padding: 20px 20px 20px 20px;font-size: 16px;outline: none;color:#2ecc71;border:3px solid #2ecc71;" value="Browse..."> <i class="fa fa-check-circle"></i> <label style="color:#2ecc71;padding-top:6px;font-size: 16px;">&nbsp;<strong>Already Uploaded</strong></label></button>
                  <?php
                  }

                 ?>
                
               </div>
              

                <button type="submit" name="upload_file_name" class="btn btn-success btn-lg btn-block" value="1" style="margin-top: 20px;font-size: 14px;padding: 10px 10px 10px 10px;"><i class="fa fa-check" style="font-size: 16px;"></i>&nbsp; Submit Upload </button>

                <input type="hidden" id="clear_timetable123" value="<?php echo $timetable; ?>">

                <?php 

                  if($timetable !== '0')
                  {?>

                      <button type="button" class="btn btn-link btn-lg btn-block" id="clear_btn" onclick="clear_timetable();" style="font-size: 14px;margin-top: 10px;opacity: 0.65;"><i class="fa fa-trash" style="font-size: 14px;"></i>&nbsp;Remove</button>

                  <?php
                  }

                 ?>
                

                  </div>

                <div class="col-lg-2"></div>
              </div>

            </div>
          </form>
        </div>

    <script>

                  function upload_timetable()
                  { 
                    var table_image = document.getElementById('table_image').value;                    
                    if(table_image !== '')
                    {
                      
                      document.getElementById('msg_alert').innerHTML = '<button type="button" class="btn btn-block btn-sm" name="update_payment" id="reject_upload_btn" style="padding: 20px 20px 20px 20px;font-size: 18px;outline: none;background-color: #ff6500;color:white;margin-top: 40px;" value="Browse..."> <i class="fa fa-spinner"></i> <label style="font-weight: bold;color:white;padding-top:6px;">&nbsp;Changing..</label></button>';

                      setTimeout(function() { 

                            document.getElementById('msg_alert').innerHTML = '<button type="button" class="btn btn-block btn-sm" name="update_payment" id="reject_upload_btn" style="padding: 20px 20px 20px 20px;font-size: 18px;outline: none;background-color: #2ecc71;color:white;margin-top: 40px;" value="Browse..."> <i class="fa fa-check-circle"></i> <label style="font-weight: bold;color:white;padding-top:6px;">&nbsp;Upload successfully</label></button>';

                            }, 1000);

                    }
                    
                  }

                  function clear_timetable() 
                  {
                      var clear_timetable = document.getElementById('clear_timetable123').value;
                      //alert(clear_timetable)
                      $.ajax({
                        url:'../admin/query/update.php',
                        method:"POST",
                        data:{clear_timetable:clear_timetable},
                        success:function(data)
                        {
                            //alert(data)
                            Swal.fire({
                            position: 'top-middle',
                            icon: 'success',
                            title: 'Clear Successfully!',
                            showConfirmButton: false,
                            timer: 1500
                          })

                            document.getElementById('clear_btn').style.display = 'none';

                            document.getElementById('msg_alert').innerHTML = '<button type="button" class="btn btn-block btn-sm" name="update_payment" id="reject_upload_btn" onclick="click_img_file();" style="padding: 20px 20px 20px 20px;border:3px dashed gray;font-size: 18px;outline: none;background-color: white;color:#545454;margin-top: 40px;" value="Browse..."> <i class="fa fa-plus" style="font-size: 20px"></i>&nbsp; <label style="font-weight: bold;font-size: 20px;padding-top: 4px;">Upload Timetable</label></button>';

                            document.getElementById('status').innerHTML = '<center><img src="../admin/images/timetable/not_available.jpg" width="300px" height="300px" class="image-responsive" style="margin-top: 20%;"></center>';
                          
                        }
                       })
                  }

                  function click_img_file() 
                  {
                    document.getElementById("table_image").click();
                  }

                </script>


</div>

<div class="col-lg-3">
<h3>
  
</h3>
</div>

</div>
</div>


<div id="premium" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Setup Premium SMS Gateway</h4>
      </div>

      <form  action="../admin/query/insert.php" method="POST" autocomplete="off">
      <div class="modal-body">
        <p>You can setup or update your sms gateway.</p>

        <?php 

          $sql003 = mysqli_query($conn,"SELECT * FROM `sms_gateway` WHERE INS_ID = '1'");
          if(mysqli_num_rows($sql003)>0)
          {
            while($row003 = mysqli_fetch_assoc($sql003))
            {
                $sms_username = $row003['USERNAME'];
                $sms_api_key = $row003['API_KEY'];
                $sms_gateway_type = $row003['GATEWAY_TYPE'];
                
                $sms_sender_id = $row003['SENDER_ID'];
                $sms_sender_type = $row003['SENDER_TYPE'];
                $sms_country_code = $row003['COUNTRY_CODE'];

                $sms_charge = $row003['SMS_CHARGE'];

            }
          }else
          if(mysqli_num_rows($sql003) == '0')
          {
              $sms_username = '';
              $sms_api_key = '';
              $sms_gateway_type = '';
              
              $sms_sender_id = '';
              $sms_sender_type = '';
              $sms_country_code = '';
              $sms_charge = 0;
          }
          


         ?>


          <div class="form-group form-group-default">
          <label>Username</label>
            <input type="text" name="username" class="form-control" value="<?php echo $sms_username; ?>"  required data-toggle="tooltip" data-title="Username" placeholder="Username..">
          </div>

          <div class="form-group form-group-default">
          <label>API Key</label>
            <input type="text" name="api_key" class="form-control" value="<?php echo $sms_api_key; ?>" required data-toggle="tooltip" data-title="API Key" placeholder="API Key..">
          </div>

          <div class="form-group form-group-default">
          <label>Gateway Type</label>
            <input type="text" name="gateway_type" class="form-control" value="<?php echo $sms_gateway_type; ?>"  required data-toggle="tooltip" data-title="Gateway Type.." placeholder="Gateway Type..">
          </div>

          <div class="form-group form-group-default">
          <label>Sender ID</label>
            <input type="text" name="sender_id" class="form-control" value="<?php echo $sms_sender_id; ?>" required data-toggle="tooltip" data-title="Sender ID.." placeholder="Sender ID..">
          </div>

          <div class="form-group form-group-default">
          <label>Sender Type</label>
            <input type="text" name="sender_type" class="form-control" value="<?php echo $sms_sender_type; ?>"  required data-toggle="tooltip" data-title="Sender Type" placeholder="Sender Type..">
          </div>

          <div class="form-group form-group-default">
          <label>Country Code</label>
            <input type="text" name="country_code" class="form-control" value="<?php echo $sms_country_code; ?>" required data-toggle="tooltip" data-title="Country Code" placeholder="Country Code..">
          </div>

          <div class="col-md-12" style="border-top: 1px solid #cccc;margin-bottom: 12px;"></div>

          <div class="form-group form-group-default">
          <label>Per SMS Charges(LKR)</label>
            <input type="text" name="charges" class="form-control" value="<?php echo $sms_charge; ?>" required data-toggle="tooltip" data-title="Per SMS Charge" placeholder="Per SMS Charge(LKR)..">
          </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-lg" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-success btn-lg" name="setup_sms_gateway">Submit</button>
      </div>
    </form>
    </div>

          </div>
  </div>


  <div id="single_sms" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Setup Custom SMS Gateway</h4>
      </div>

      <form  action="../admin/query/insert.php" method="POST" autocomplete="off">
      <div class="modal-body">
        <p>You can setup or update your sms gateway.</p>

        <?php 



          $sql003 = mysqli_query($conn,"SELECT * FROM `single_sms_gateway` WHERE `SINGLE_SMS_ID` = '1'");
          if(mysqli_num_rows($sql003)>0)
          {
            while($row003 = mysqli_fetch_assoc($sql003))
            {
                $sp_sms_username = $row003['USERNAME'];
                $sp_sms_api_key = $row003['API_KEY'];
                $sp_sms_gateway_type = $row003['GATEWAY_TYPE'];
                
                $sp_sms_country_code = $row003['COUNTRY_CODE'];

                $sp_sms_charge = $row003['SMS_CHARGE'];

            }
          }else
          if(mysqli_num_rows($sql003) == '0')
          {
              $sp_sms_username = '';
              $sp_sms_api_key = '';
              $sp_sms_gateway_type = '';
              
              $sp_sms_country_code = '';
              $sp_sms_charge = 0;
          }
          


         ?>


          <div class="form-group form-group-default">
          <label>Username</label>
            <input type="text" name="username" class="form-control" value="<?php echo $sp_sms_username; ?>"  required data-toggle="tooltip" data-title="Username" placeholder="Username..">
          </div>

          <div class="form-group form-group-default">
          <label>API Key</label>
            <input type="text" name="api_key" class="form-control" value="<?php echo $sp_sms_api_key; ?>" required data-toggle="tooltip" data-title="API Key" placeholder="API Key..">
          </div>

          <div class="form-group form-group-default">
          <label>Gateway Type</label>
            <input type="text" name="gateway_type" class="form-control" value="<?php echo $sp_sms_gateway_type; ?>"  required data-toggle="tooltip" data-title="Gateway Type.." placeholder="Gateway Type..">
          </div>

          <div class="form-group form-group-default">
          <label>Country Code</label>
            <input type="text" name="country_code" class="form-control" value="<?php echo $sp_sms_country_code; ?>" required data-toggle="tooltip" data-title="Country Code" placeholder="Country Code..">
          </div>

          <div class="col-md-12" style="border-top: 1px solid #cccc;margin-bottom: 12px;"></div>

          <div class="form-group form-group-default">
          <label>Per SMS Charges(LKR)</label>
            <input type="text" name="charges" class="form-control" value="<?php echo $sp_sms_charge; ?>" required data-toggle="tooltip" data-title="Per SMS Charge" placeholder="Per SMS Charge(LKR)..">
          </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-lg" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-success btn-lg" name="setup_special_sms_gateway">Submit</button>
      </div>
    </form>
    </div>

          </div>
  </div>


</div>




</div>
</div>
</div>

</div>


    <script type="text/javascript">
      $(document).ready(function(){
        var pic = $('#pic2').val();
        if(pic == 'shop_img.png')
        {
          $('#file_upload').change(function(){
            $('#ok2').click();
          });
          $('#remove_btn2').hide();
        }else
        if(pic !== 'shop_img.png')
        {
          $('#file_upload').change(function(){
            $('#ok2').click();
          });
            $('#remove_btn2').show();
        }
        
      });
    </script>

<script type="text/javascript">
$(document).ready(function(){  
	$('#availability6').hide();
   $('#cp').blur(function(){
     var cp = $(this).val();
     var id = $("#id").val();
     if(cp=='')
     {
      $('#availability6').hide();
     }else
     {
     $.ajax({
      url:'../admin/query/check.php',
      method:"POST",
      data:{cp:cp,id:id},
      success:function(data)
      {
         if(data>0)
         {
         	$('#availability6').show();
          $('#availability6').html('<span class="text-success"><i class="fa fa-check"></i> Match Current Password</span>');
          $('#submit_btn').attr("disabled", false);
          $('#txtConfirmPassword').attr("disabled", false);
          $('#txtNewPassword').attr("disabled", false);
         }
         else
         if(data == 0)
         {
         $('#availability6').show();
          $('#availability6').html('<span class="text-danger"><i class="fa fa-times"></i> No Match Current Password</span>');
          $('#submit_btn').attr("disabled", true);
          $('#txtConfirmPassword').attr("disabled", true);
          $('#txtNewPassword').attr("disabled", true);
         }
      }
     })
   }

  });
 });

$(document).ready(function(){  
   $('#txtConfirmPassword').keyup(function(){
     var rp = $(this).val();
     var np = document.getElementById("txtNewPassword").value;
     // alert(cate);

       if(rp == np)
       {
        $('#availability7').html('<span class="text-success"><i class="fa fa-check"></i> Match New Password</span>');
        $('#submit_btn').attr("disabled", false);

       }else
       {
        $('#availability7').html('<span class="text-danger"><i class="fa fa-times"></i> No Match New Password</span>');
        $('#submit_btn').attr("disabled", true);
       }
      

  });
 });
</script>
<script type="text/javascript">

  function readURL2(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#show_image').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $("#imagefile").change(function(){
        readURL2(this);
    });

    
</script>

<script type="text/javascript">
  
  $(document).ready(function(){
    $('#upload').hide();

    $('#imagefile').change(function(){
      $('#upload').show();
    });
  });

</script>


<script type="text/javascript">
  
  $(document).ready(function(){
    $('#upload2').hide();

    $('#imagefile2').change(function(){
      $('#upload2').show();
    });
  });

</script>

<script type="text/javascript">

  function readURL3(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#show_image2').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $("#imagefile2").change(function(){
        readURL3(this);
    });

    
</script>

<script type="text/javascript">
  
  $(document).ready(function(){
    $('#upload2').hide();

    $('#imagefile2').change(function(){
      $('#upload2').show();
    });
  });

</script>

<script type="text/javascript">
  
  $(document).ready(function(){
    $('#up_btn').prop('disabled',true);
    $('#up_btn').css('cursor','not-allowed');

    $('.change_inputs').change(function(){
      $('#up_btn').prop('disabled',false);
      $('#up_btn').css('cursor','pointer');

    });
  });

</script>

<script type="text/javascript">
  
  $(document).ready(function(){
    $('#submit_btn').prop('disabled',true);
    $('#submit_btn').css('cursor','not-allowed');

    $('.change_inputs').change(function(){
      $('#submit_btn').prop('disabled',false);
      $('#submit_btn').css('cursor','pointer');
    });
  });

</script>


<script type="text/javascript">
   $(document).ready(function() {
    $("#show_hide_password a").on('click', function(event) {
        event.preventDefault();
        if($('#show_hide_password input').attr("type") == "text"){
            $('#show_hide_password input').attr('type', 'password');
            $('#show_hide_password i').addClass( "fa-eye-slash" );
            $('#show_hide_password i').removeClass( "fa-eye" );
        }else if($('#show_hide_password input').attr("type") == "password"){
            $('#show_hide_password input').attr('type', 'text');
            $('#show_hide_password i').removeClass( "fa-eye-slash" );
            $('#show_hide_password i').addClass( "fa-eye" );
        }
    });


    $("#show_hide_password2 a").on('click', function(event) {
        event.preventDefault();
        if($('#show_hide_password2 input').attr("type") == "text"){
            $('#show_hide_password2 input').attr('type', 'password');
            $('#show_hide_password2 i').addClass( "fa-eye-slash" );
            $('#show_hide_password2 i').removeClass( "fa-eye" );
        }else if($('#show_hide_password2 input').attr("type") == "password"){
            $('#show_hide_password2 input').attr('type', 'text');
            $('#show_hide_password2 i').removeClass( "fa-eye-slash" );
            $('#show_hide_password2 i').addClass( "fa-eye" );
        }
    });


    $("#show_hide_password3 a").on('click', function(event) {
        event.preventDefault();
        if($('#show_hide_password3 input').attr("type") == "text"){
            $('#show_hide_password3 input').attr('type', 'password');
            $('#show_hide_password3 i').addClass( "fa-eye-slash" );
            $('#show_hide_password3 i').removeClass( "fa-eye" );
        }else if($('#show_hide_password3 input').attr("type") == "password"){
            $('#show_hide_password3 input').attr('type', 'text');
            $('#show_hide_password3 i').removeClass( "fa-eye-slash" );
            $('#show_hide_password3 i').addClass( "fa-eye" );
        }
    });

});
</script>

<script type="text/javascript">
  $(document).ready(function(){ 


   $("#switchColorOpt").on('change', function() {
    if($(this).is(':checked')) {
        
        var free_msg = "Yes";

        $('#f_mode_message').html('Enabled');

         $.ajax({
          url:'../admin/query/update.php',
          method:"POST",
          data:{switch_free:free_msg},
          success:function(data)
          {
              const Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 1000,
              timerProgressBar: true,
              onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
              }
            })

            Toast.fire({
              icon: 'success',
              title: 'Free Mode On!'
            })
          }
        });


    }
    else{

      var free_msg = "No";
      
      $('#f_mode_message').html('Disabled');

         $.ajax({
          url:'../admin/query/update.php',
          method:"POST",
          data:{switch_free:free_msg},
          success:function(data)
          {
              const Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 1000,
              timerProgressBar: true,
              onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
              }
            })

            Toast.fire({
              icon: 'success',
              title: 'Free Mode Off!'
            })
          }
        });
       
    }
  });
 });
</script>


<script type="text/javascript">
  $(document).ready(function(){ 


   $("#sms_active").on('change', function() {
    if($(this).is(':checked')) {
        
        var active_msg = "1";

         $.ajax({
          url:'../admin/query/update.php',
          method:"POST",
          data:{active_msg:active_msg},
          success:function(data)
          {
              const Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 1000,
              timerProgressBar: true,
              onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
              }
            })

            Toast.fire({
              icon: 'success',
              title: 'Premium SMS Service Enabled!'
            })
          }
        });


    }
    else{

      var active_msg = "0";

         $.ajax({
          url:'../admin/query/update.php',
          method:"POST",
          data:{active_msg:active_msg},
          success:function(data)
          {
              const Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 1000,
              timerProgressBar: true,
              onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
              }
            })

            Toast.fire({
              icon: 'success',
              title: 'Premium SMS Service Disabled!'
            })
          }
        });
       
    }
  });
 });
</script>


<script type="text/javascript">
  $(document).ready(function(){ 


   $("#single_sms_active").on('change', function() {
    if($(this).is(':checked')) {
        
        var active_msg = "1";

         $.ajax({
          url:'../admin/query/update.php',
          method:"POST",
          data:{active_single_msg:active_msg},
          success:function(data)
          {
              const Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 1000,
              timerProgressBar: true,
              onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
              }
            })

            Toast.fire({
              icon: 'success',
              title: 'Custom SMS Service Enabled!'
            })
          }
        });


    }
    else{

      var active_msg = "0";

         $.ajax({
          url:'../admin/query/update.php',
          method:"POST",
          data:{active_single_msg:active_msg},
          success:function(data)
          {
              const Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 1000,
              timerProgressBar: true,
              onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
              }
            })

            Toast.fire({
              icon: 'success',
              title: 'Custom SMS Service Disabled!'
            })
          }
        });
       
    }
  });
 });
</script>


<script type="text/javascript">
  $(document).ready(function(){ 


   $("#check_active_register").on('change', function() {
    if($(this).is(':checked')) {
        
        var active_reg_stu = "1";

         $.ajax({
          url:'../admin/query/update.php',
          method:"POST",
          data:{reg_stu_without_check:active_reg_stu},
          success:function(data)
          {
              const Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 1000,
              timerProgressBar: true,
              onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
              }
            })

            Toast.fire({
              icon: 'success',
              title: 'Activated! Registered student without check!'
            })
          }
        });


    }
    else{

      var active_reg_stu = "0";

         $.ajax({
          url:'../admin/query/update.php',
          method:"POST",
          data:{reg_stu_without_check:active_reg_stu},
          success:function(data)
          {
              const Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 1000,
              timerProgressBar: true,
              onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
              }
            })

            Toast.fire({
              icon: 'success',
              title: 'Deactivated! Registered student without check!'
            })
          }
        });
       
    }
  });
 });
</script>


<script type="text/javascript">
  $(document).ready(function(){ 


   $("#active_exam").on('change', function() {
    if($(this).is(':checked')) {
        
        var active_exam = "1";

         $.ajax({
          url:'../admin/query/update.php',
          method:"POST",
          data:{active_exam:active_exam},
          success:function(data)
          {
              const Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 1000,
              timerProgressBar: true,
              onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
              }
            })

            Toast.fire({
              icon: 'success',
              title: 'Online Examination Option Enabled!'
            })
          }
        });


    }
    else{

      var active_exam = "0";

         $.ajax({
          url:'../admin/query/update.php',
          method:"POST",
          data:{active_exam:active_exam},
          success:function(data)
          {
              const Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 1000,
              timerProgressBar: true,
              onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
              }
            })

            Toast.fire({
              icon: 'success',
              title: 'Online Examination Option Disabled!'
            })
          }
        });
       
    }
  });
 });
</script>
<script type="text/javascript">
  $(document).ready(function(){ 


   $("#active_suspend").on('change', function() {
    if($(this).is(':checked')) {
        
        var active_suspend = "1";

         $.ajax({
          url:'../admin/query/update.php',
          method:"POST",
          data:{active_suspend:active_suspend},
          success:function(data)
          {
              const Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 1000,
              timerProgressBar: true,
              onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
              }
            })

            Toast.fire({
              icon: 'success',
              title: 'System Suspended!'
            })
          }
        });


    }
    else{

      var active_suspend = "0";

         $.ajax({
          url:'../admin/query/update.php',
          method:"POST",
          data:{active_suspend:active_suspend},
          success:function(data)
          {
              const Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 1000,
              timerProgressBar: true,
              onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
              }
            })

            Toast.fire({
              icon: 'success',
              title: 'Activated System!'
            })
          }
        });
       
    }
  });
 });
</script>


<script type="text/javascript">
  $(document).ready(function(){ 


   $("#switchColorOpt002").on('change', function() {
    if($(this).is(':checked')) {
        
        var reg_btn_hide = "0";

         $.ajax({
          url:'../admin/query/update.php',
          method:"POST",
          data:{switch_reg_btn_hide:reg_btn_hide},
          success:function(data)
          {
              const Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 1000,
              timerProgressBar: true,
              onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
              }
            })

            Toast.fire({
              icon: 'success',
              title: 'Register Button Hidden!'
            })
          }
        });


    }
    else{

      var reg_btn_hide = "1";

         $.ajax({
          url:'../admin/query/update.php',
          method:"POST",
          data:{switch_reg_btn_hide:reg_btn_hide},
          success:function(data)
          {
              const Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 1000,
              timerProgressBar: true,
              onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
              }
            })

            Toast.fire({
              icon: 'success',
              title: 'Register Button Show!'
            })
          }
        });
       
    }
  });
 });
</script>

<script type="text/javascript">
  $(document).ready(function(){ 


   $("#active_limit").on('change', function() {
    if($(this).is(':checked')) {
        
        var limit_btn_hide = "0";

         $.ajax({
          url:'../admin/query/update.php',
          method:"POST",
          data:{active_limit:limit_btn_hide},
          success:function(data)
          {
              const Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 1000,
              timerProgressBar: true,
              onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
              }
            })

            Toast.fire({
              icon: 'success',
              title: 'Enabled Limit Access!'
            })
          }
        });


    }
    else{

      var limit_btn_hide = "1";

         $.ajax({
          url:'../admin/query/update.php',
          method:"POST",
          data:{active_limit:limit_btn_hide},
          success:function(data)
          {
              const Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 1000,
              timerProgressBar: true,
              onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
              }
            })

            Toast.fire({
              icon: 'success',
              title: 'Disabled Limit Access!'
            })
          }
        });
       
    }
  });
 });


</script>
<script type="text/javascript">
  
  function setup_limitation()
  {
    var setup_limit = document.getElementById('setup_limit001').value;
    //alert(limit);

    $.ajax({
          url:'../admin/query/update.php',
          method:"POST",
          data:{setup_limit:setup_limit},
          success:function(data)
          {
            //alert(data);
              const Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 600,
              timerProgressBar: true,
              onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
              }
            })

            Toast.fire({
              icon: 'success',
              title: 'Setup Limitation!'
            })
          }
        });


  }

  

</script>

<script type="text/javascript">
  $(document).ready(function(){ 


   $("#active_free_days").on('change', function() {
    if($(this).is(':checked')) {
        
        var active_free_days = "0";

         $.ajax({
          url:'../admin/query/update.php',
          method:"POST",
          data:{active_free_days:active_free_days},
          success:function(data)
          {
              const Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 1000,
              timerProgressBar: true,
              onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
              }
            })

            Toast.fire({
              icon: 'success',
              title: 'Enabled Free Days Option!'
            })
          }
        });


    }
    else{

      var active_free_days = "1";

         $.ajax({
          url:'../admin/query/update.php',
          method:"POST",
          data:{active_free_days:active_free_days},
          success:function(data)
          {
              const Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 1000,
              timerProgressBar: true,
              onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
              }
            })

            Toast.fire({
              icon: 'success',
              title: 'Disabled Free Days Option!'
            })
          }
        });
       
    }
  });
 });


</script>

<script type="text/javascript">
  function setup_free_days()
  {
    var setup_free_days = document.getElementById('setup_free_days002').value;
   // alert(setup_free_days);

    $.ajax({
          url:'../admin/query/update.php',
          method:"POST",
          data:{setup_free_days:setup_free_days},
          success:function(data)
          {
            //alert(data);
              const Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 600,
              timerProgressBar: true,
              onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
              }
            })

            Toast.fire({
              icon: 'success',
              title: 'Setup No of Free Days!'
            })
          }
        });


  }
</script>



<script type="text/javascript">
  $(document).ready(function(){ 


   $("#free_mode_material").on('change', function() {
    if($(this).is(':checked')) {
        
        var free_material = "1";

        $('#f_material_message').html('Enabled');

         $.ajax({
          url:'../admin/query/update.php',
          method:"POST",
          data:{free_material_option:free_material},
          success:function(data)
          {
              const Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 600,
              timerProgressBar: true,
              onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
              }
            })

            Toast.fire({
              icon: 'success',
              title: 'Free Mode Materials Show!'
            })
          }
        });


    }
    else{

      var free_material = "0";

      $('#f_material_message').html('Disabled');

         $.ajax({
          url:'../admin/query/update.php',
          method:"POST",
          data:{free_material_option:free_material},
          success:function(data)
          {
              const Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 800,
              timerProgressBar: true,
              onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
              }
            })

            Toast.fire({
              icon: 'success',
              title: 'Free Mode Materials Hide!'
            })
          }
        });
       
    }
  });
 });
</script>


<script type="text/javascript">
  $(document).ready(function(){ 


   $("#sem_login_btn").on('change', function() {
    if($(this).is(':checked')) {

      //Hide Button
        
        var seminar_btn = "1";

         $.ajax({
          url:'../admin/query/update.php',
          method:"POST",
          data:{seminar_option_status:seminar_btn},
          success:function(data)
          {
              const Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 600,
              timerProgressBar: true,
              onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
              }
            })

            Toast.fire({
              icon: 'success',
              title: 'Seminar Button Hide!'
            })
          }
        });


    }
    else{

      //show button

      var seminar_btn = "0";

         $.ajax({
          url:'../admin/query/update.php',
          method:"POST",
          data:{seminar_option_status:seminar_btn},
          success:function(data)
          {
              const Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 800,
              timerProgressBar: true,
              onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
              }
            })

            Toast.fire({
              icon: 'success',
              title: 'Seminar Button Show!'
            })
          }
        });
       
    }
  });
 });
</script>


<script type="text/javascript">
  $(document).ready(function(){ 


   $("#active_admin_otp").on('change', function() {
    if($(this).is(':checked')) {

      //Hide Button
        
        var otp_check = "1";

         $.ajax({
          url:'../admin/query/update.php',
          method:"POST",
          data:{admin_otp_option:otp_check},
          success:function(data)
          {
              const Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 600,
              timerProgressBar: true,
              onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
              }
            })

            Toast.fire({
              icon: 'success',
              title: 'Active Admin OTP!'
            })
          }
        });


    }
    else{

      //show button

      var otp_check = "0";

         $.ajax({
          url:'../admin/query/update.php',
          method:"POST",
          data:{admin_otp_option:otp_check},
          success:function(data)
          {
              const Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 800,
              timerProgressBar: true,
              onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
              }
            })

            Toast.fire({
              icon: 'success',
              title: 'Deactive Admin OTP!'
            })
          }
        });
       
    }
  });
 });
</script>



<script type="text/javascript">
  $(document).ready(function(){ 


   $("#active_safe_link").on('change', function() {
    if($(this).is(':checked')) {

      //Hide Button
        
        var safe_link = "1";

         $.ajax({
          url:'../admin/query/update.php',
          method:"POST",
          data:{active_safe_link:safe_link},
          success:function(data)
          {
              const Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 600,
              timerProgressBar: true,
              onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
              }
            })

            Toast.fire({
              icon: 'success',
              title: 'Active Safe Link!'
            })
          }
        });


    }
    else{

      //show button

      var safe_link = "0";

         $.ajax({
          url:'../admin/query/update.php',
          method:"POST",
          data:{active_safe_link:safe_link},
          success:function(data)
          {
              const Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 800,
              timerProgressBar: true,
              onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
              }
            })

            Toast.fire({
              icon: 'success',
              title: 'Deactive Safe Link!'
            })
          }
        });
       
    }
  });
 });
</script>



<script type="text/javascript">
  $(document).ready(function(){ 


   $("#active_show_tg").on('change', function() {
    if($(this).is(':checked')) {

      //Hide Button
        
        var tg_check = "1";

         $.ajax({
          url:'../admin/query/update.php',
          method:"POST",
          data:{show_telegram_btn:tg_check},
          success:function(data)
          {
              const Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 600,
              timerProgressBar: true,
              onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
              }
            })

            Toast.fire({
              icon: 'success',
              title: 'Active Telegram Button!'
            })
          }
        });


    }
    else{

      //show button

      var tg_check = "0";

         $.ajax({
          url:'../admin/query/update.php',
          method:"POST",
          data:{show_telegram_btn:tg_check},
          success:function(data)
          {
              const Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 800,
              timerProgressBar: true,
              onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
              }
            })

            Toast.fire({
              icon: 'success',
              title: 'Deactive Telegram Button!'
            })
          }
        });
       
    }
  });
 });
</script>


<script>

//Check TP No and disable characters

$(document).ready(function () {

    $('.number').bind('keyup blur',function()
    { 
           var thistext = $(this);
           thistext.val(thistext.val().replace(/[^0-9.]/g,'') ); 

           
    });

})

//Check TP No and disable characters
</script>

<?php  include('footer/footer.php'); ?>