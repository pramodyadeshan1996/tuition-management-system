<?php
	$page = "Payment Details";
	$folder_in = '3';

      include('header/header.php');

        $s04 = mysqli_query($conn,"SELECT * FROM admin_login WHERE ADMIN_ID = '$admin_id'");
        while($row4 = mysqli_fetch_assoc($s04))
        {
          $name = $row4['ADMIN_NAME'];
        }

 ?>

<script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
 <style type="text/css">
   .count {
  
  animation: pulse 1s infinite;
}

.pagination li a {
   padding:8px 16px;
   border:1px solid #ccc;
   color:#333;
   font-weight:bold;
  }

@keyframes pulse {
  0% {
    -moz-box-shadow: 0 0 0 0 #ffd945;
    box-shadow: 0 0 0 0 #ffd945;
  }
  70% {
    -moz-box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
  }
  100% {
    -moz-box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
  }
}
 </style>

    <ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-bottom: 04%;margin-bottom: 0;">
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="Payment Details"> Payment Details</li>
    </ol>


  <div class="col-md-12">
    
    <div class="row" style="padding-top: 10px;">
      <div class="col-md-12 bg-white" style="border:1px solid #cccc;height: auto;">
      
      <div class="col-md-12" style="border-bottom: 1px solid #cccc;">
        <div class="row">

            <div class="col-md-6" style="margin-bottom: 10px;"><h2><a href="dashboard.php" data-toggle="tooltip" data-title="Back"><i class="pg-icon" style="font-size: 22px;">chevron_left</i></a> Payment Details</h2></div>
            <div class="col-md-5" style="padding-top: 10px;">

                <?php
                  if(!empty($_SESSION['check_year']) || !empty($_SESSION['check_month']))
                  {
                    $check_y = $_SESSION['check_year'];
                    $check_m = $_SESSION['check_month'];
                  }else
                  {
                    $check_y = '';
                    $check_m = '';
                  }
                ?>

                <form action="payment_pending.php" method="POST">
                <div class="row">
                    <div class="col-md-3 mt-2">
                      <select name="check_year" class="form-control">
                        <?php 

                          if(empty($_SESSION['check_year']))
                          {
                            $year1  = date('Y');
                          }else
                          if(!empty($_SESSION['check_year']))
                          {
                            $year1  = $_SESSION['check_year'];
                          }
                          
                          $year2 = $year1-5;

                          echo '<option value="'.$year1.'">'.$year1.'</option>';

                          for($i=$year1;$i>=$year2;$i--)
                          { 

                            if($i == $year1)
                            {
                              $y = date('Y');
                              echo '<option value="'.$y.'">'.$y.'</option>';
                            }else
                            if($i !== $year1)
                            {

                              echo '<option value="'.$i.'">'.$i.'</option>';
                            
                            }
                          }

                         ?>


                      </select>
                      
                      </div>

                    <div class="col-md-3 mt-2">
                      <select name="check_month123" class="form-control">
                        <?php 

                           if(empty($_SESSION['check_month']))
                          {
                            $monthNum1  = date('m');
                          }else
                          if(!empty($_SESSION['check_month']))
                          {
                            $monthNum1  = $_SESSION['check_month'];
                          } 
                          
                          $monthName1 = date('F', mktime(0, 0, 0, $monthNum1, 10)); 
                          echo '<option value="'.$monthNum1.'">'.$monthName1.'</option>';

                          for($i=0;$i<=12;$i++)
                          {

                            if($i == $monthNum1)
                            {

                              $monthName1 = date('F', mktime(0, 0, 0, $i, 10)); 
                              echo '<option value="'.$i.'">'.$monthName1.'</option>';

                            }else
                            {
                              $monthName1 = date('F', mktime(0, 0, 0, $i, 10)); 
                              echo '<option value="'.$i.'">'.$monthName1.'</option>';

                            }

                          }


                          

                         ?>

                      </select>
                    </div>
                    <div class="col-md-6 mt-2">
                      <button type="submit" name="check_date" class="btn btn-info btn-lg btn-block"><i class="fa fa-search"></i>&nbsp;Search</button>
                    </div>
                </div>
                </form>
            </div>
            <div class="col-md-1 pt-3 mb-2"><?php 
                $sql0021 = mysqli_query($conn,"SELECT * FROM payment_data");

                if(mysqli_num_rows($sql0021)>0)
                { 

                  echo '<button class="btn btn-success btn-lg btn-block" data-toggle="modal" data-target="#filter"  data-dismiss="modal" style="border-radius: 80px;"><i class="fa fa-print"></i>&nbsp;Print</button>';

                }else
                if(mysqli_num_rows($sql0021)== '0')
                { 
                  echo '<button class="btn btn-success btn-lg btn-block disabled" style="border-radius: 80px;padding:10px 10px 10px 10px;cursor:not-allowed;margin-top:10px;"><i class="fa fa-print"></i>&nbsp;Print</button>';
                }
               ?>          
            </div>
        </div>

      </div>
          

<div class="card card-borderless">
<div class="col-md-12" style="padding-top:10px;">

<div class="tab-content">
<div class="tab-pane active" id="home">
<div class="row column-seperation">



<div class="col-lg-12">
  
  <div class="row">
    
    <div class="col-md-4">

      <form action="" method="POST">

        <div class="row">
          <div class="col-md-9">
            <select class="form-control mb-2" name="payment_type">
              
              <?php 

                if($_SESSION['pay_type'] == '')
                {
                  echo '<option value="">Select Payment Type</option>
                        <option value="1">Approved</option>
                        <option value="0">Pending</option>
                        <option value="2">Rejected</option>';
                }else
                if($_SESSION['pay_type'] !== '')
                {
                  if($_SESSION['pay_type'] == '0')
                  { 
                      echo '<option value="0">Pending</option>';
                  }else
                  if($_SESSION['pay_type'] == '1')
                  { 
                      echo '<option value="1">Approved</option>';
                  }else
                  if($_SESSION['pay_type'] == '2')
                  { 
                      echo '<option value="2">Rejected</option>';
                  }


                  if($_SESSION['pay_type'] == '0')
                  {
                    echo '<option value="1">Approved</option>
                        <option value="2">Rejected</option>';
                  }else
                  if($_SESSION['pay_type'] == '1')
                  {
                    echo '<option value="0">Pending</option>
                        <option value="2">Rejected</option>';
                  }else
                  if($_SESSION['pay_type'] == '2')
                  {
                    echo '<option value="0">Pending</option>
                        <option value="1">Approved</option>';
                  }
                  
                }
               ?>
              
            </select>
          </div>
          <div class="col-md-3 mb-2"><button type="submit" name="search_payments" class="btn btn-success btn-block btn-lg"><i class="pg-icon">search</i> Search</button></div>


        </div>
      </form>
    </div>
    <div class="col-md-4">
    </div>
    <div class="col-md-4">
      <div class="row">
  		<div class="col-md-12">

  			<input type="text" id="search-table" class="form-control pull-right" placeholder="Search">
  		</div>
    </div>
  </div>

</div>

<?php 

$sql00100 = mysqli_query($conn,"SELECT * FROM payment_data");

$count = mysqli_num_rows($sql00100);

 ?>
<div class="card card-transparent">
<div class="card-header ">
<div class="card-title">
</div>
<div class="clearfix"></div>
</div>

<div class="card-body table-responsive" style="height: 500px;overflow: auto;margin-bottom: 0;">
  <table class="table table-hover" id="tableWithSearch2">
<thead>
<tr>
<th style="width: 10%;">Pay Time</th>
<th style="width: 6%;">Student Name <label class="label label-success"><?php echo  $count; ?></label></th>
<th style="width: 1%;">Class Name</th>
<th style="width: 6%;">Teacher Name</th>
<th style="width: 1%;">Month</th>
<th style="width: 2%;">Payment(Rs)</th>
<th style="width: 1%;">Status</th>
<th style="text-align: center;width: 1%;">Action</th>
</tr>
</thead>
<tbody id="myTable">

<?php 

  $record_per_page = 10;

  $nxt_five_loop = 0;

  $page = '';

  if(isset($_GET["page"]))
  {
    $page = $_GET["page"];
  }
  else
  {
   $page = 1;
  }

  $start_from = ($page-1)*$record_per_page; //Current page no -1 X Per Page records



  if(isset($_POST['check_date']))
  {
    $_SESSION['check_month'] = $_POST['check_month123'];
    $_SESSION['check_year'] = $_POST['check_year'];

    $month001 = $_SESSION['check_month'];
    $year001 = $_SESSION['check_year'];
  }else
  {
    $month001 = date('m');
    $year001 = date('Y');
  }
  
  if(isset($_POST['search_payments']))
  {
      $payment_status = $_POST['payment_type'];

      $_SESSION['pay_type'] = $payment_status;
   
      $sql001 = mysqli_query($conn,"SELECT * FROM `payment_data` WHERE `ADMIN_SUBMIT` = '$payment_status' AND `YEAR` = '$year001' AND `MONTH` = '$month001' ORDER BY `PAY_ID` DESC LIMIT $start_from, $record_per_page");

      $page_query = "SELECT * FROM `payment_data` WHERE `ADMIN_SUBMIT` = '$payment_status' AND `YEAR` = '$year001' AND `MONTH` = '$month001' ORDER BY `PAY_ID` DESC";


  }else
  {
      $sql001 = mysqli_query($conn,"SELECT * FROM `payment_data` WHERE `ADMIN_SUBMIT` = '0' AND `YEAR` = '$year001' AND `MONTH` = '$month001' ORDER BY `PAY_ID` DESC LIMIT $start_from, $record_per_page");

      $page_query = "SELECT * FROM `payment_data` WHERE `ADMIN_SUBMIT` = '0' AND `YEAR` = '$year001' AND `MONTH` = '$month001' ORDER BY `PAY_ID` DESC";


  }
          $page_result = mysqli_query($conn,$page_query);

          $check = mysqli_num_rows($sql001);

          if($check>0){
          while($row001 = mysqli_fetch_assoc($sql001))
          {
            $disable_btn = '';

            $stu_id = $row001['STU_ID'];
            $pay = $row001['PAY_TIME'];
            $class_id = $row001['CLASS_ID'];
            $pay_id = $row001['PAY_ID'];
            $tra_id = $row001['TRA_ID'];
            $fees = $row001['FEES'];
            $fees = (int)$fees;
            $year = $row001['YEAR'];
            $monthNum = $row001['MONTH'];
            $admin_status = $row001['ADMIN_SUBMIT'];
            $pay_method = $row001['PAY_METHOD'];

            if($pay_method == 'REALTIME')
            {
              $disable_btn = 'disabled';
            }

            $first_upload_img = $row001['FILE_NAME'];
            $second_upload_img = $row001['LAST_UPLOAD_FILE'];


            $month = date('F', mktime(0, 0, 0, $monthNum, 10)); // March

            $sql002 = mysqli_query($conn,"SELECT * FROM student_details WHERE STU_ID = '$stu_id'");
            while($row002 = mysqli_fetch_assoc($sql002))
            {
              $name = $row002['F_NAME']." ".$row002['L_NAME'];
              $tp = $row002['TP'];
              $dob = $row002['DOB'];
              $gender = $row002['GENDER'];
              $email = $row002['EMAIL'];
              $pic = $row002['PICTURE'];
              $address = $row002['ADDRESS'];
              $stu_id = $row002['STU_ID'];

              $sql003 = mysqli_query($conn,"SELECT * FROM stu_login WHERE STU_ID = '$stu_id'");
              while($row003 = mysqli_fetch_assoc($sql003))
              {
                $reg = $row003['REGISTER_ID'];
              }

              $sql004 = mysqli_query($conn,"SELECT * FROM classes WHERE CLASS_ID = '$class_id'");
              while($row004 = mysqli_fetch_assoc($sql004))
              {
                $subject_id = $row004['SUB_ID'];
                $teach_id = $row004['TEACH_ID'];
                $clz_name = $row004['CLASS_NAME'];

                $sql005 = mysqli_query($conn,"SELECT * FROM subject WHERE SUB_ID = '$subject_id'");
                while($row005 = mysqli_fetch_assoc($sql005))
                {
                  $subject_name = $row005['SUBJECT_NAME'];
                }
              }

                $sql007 = mysqli_query($conn,"SELECT * FROM `teacher_details` WHERE `TEACH_ID` = '$teach_id'");
                while($row007 = mysqli_fetch_assoc($sql007))
                {
                  $teacher_name = $row007['POSITION'].". ".$row007['F_NAME']." ".$row007['L_NAME'];
                }


              if($pic == '0')
              {
                if($gender == 'Male')
                {
                  $pic = 'boy.png';
                }else
                if($gender == 'Female')
                {
                  $pic = 'girl.png';
                }
                
              }
            } 

                          $seconds_ago = (time() - strtotime($pay));

                          if ($seconds_ago >= 31536000) {
                              $ago = "" . intval($seconds_ago / 31536000) . " years ago";
                          } elseif ($seconds_ago >= 2419200) {
                              $ago = "" . intval($seconds_ago / 2419200) . " months ago";
                          } elseif ($seconds_ago >= 86400) {
                              $ago = "" . intval($seconds_ago / 86400) . " days ago";
                          } elseif ($seconds_ago >= 3600) {
                              $ago = "" . intval($seconds_ago / 3600) . " hours ago";
                          } elseif ($seconds_ago >= 60) {
                              $ago = "" . intval($seconds_ago / 60) . " minutes ago";
                          } else {
                              $ago = "minute ago";
                          }


                          if($admin_status == '0')
                          {
                            $icon = '<label class="label label-warning count"><span class="fa fa-spinner"></span> Pending</label>';
                          }else
                          if($admin_status == '1')
                          {
                            $icon = '<label class="label label-success"><span class="fa fa-check-circle"></span> Paid</label>';
                          }else
                          if($admin_status == '2')
                          {
                            $icon = '<label class="label label-danger"><span class="fa fa-times-circle"></span> Rejected</label>';
                          }

            echo '

              <tr id="delete_tr'.$pay_id.'">
                <td class="v-align-middle">'.$pay.'<br><small style="font-size:10px;text-align:center;">('.$ago.')</small></td>
                <td class="v-align-middle">'.$name.'<br>('.$reg.')</td>
                <td class="v-align-middle">'.$clz_name.'<br><small>'.$subject_name.'</small></td>
                <td class="v-align-middle">'.$teacher_name.'</td>
                <td class="v-align-middle">'.$year.'<br>'.$month.'</td>
                <td class="v-align-middle">'.number_format($fees).' <br> <label class="label label-success"> '.$pay_method.'</label></td>
                <td class="v-align-middle" id="tr_msg'.$pay_id.'">'.$icon.'</td>
                 <td class="v-align-middle text-center" style="padding:10px 10px;">'; 
                 ?>


                 <a data-title="Student Details" data-toggle="tooltip" ><button class="btn btn-primary  btn-xs btn-rounded" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;" data-target="#profile<?php echo $pay_id; ?>" data-toggle="modal"><i class="pg-icon">user</i></button></a>

                <a data-title="Check Receiption" data-toggle="tooltip"><button class="btn btn-warning btn-xs btn-rounded"  data-target="#check_payment<?php echo $pay_id; ?>" data-toggle="modal" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;" ><i class="pg-icon">search</i></button></a>

                <input type="hidden" value="<?php echo $pay_id; ?>" id="pay_id<?php echo $pay_id; ?>">
                <input type="hidden" value="<?php echo $fees; ?>" id="fees<?php echo $pay_id; ?>">
                <input type="hidden" value="<?php echo $admin_status; ?>" id="admin_status<?php echo $pay_id; ?>">


                <input type="hidden" value="<?php echo $first_upload_img; ?>" id="first<?php echo $pay_id; ?>">
                <input type="hidden" value="<?php echo $second_upload_img; ?>" id="second<?php echo $pay_id; ?>">
                
                  <a class="btn btn-danger btn-xs btn-rounded" id="disapprove<?php echo $pay_id; ?>" style="padding:4px 4px;color: white;" data-title="Disapprove Payments" data-toggle="tooltip"><i class="pg-icon">close_ circle</i></a>
                
                  <button type="button" class="btn btn-success btn-xs btn-rounded" style="padding:4px 4px;"data-title="Approve Payments" data-toggle="tooltip" id="approve<?php echo $pay_id; ?>"><i class="pg-icon">tick_circle</i></button>


                  <a class="btn btn-danger btn-xs btn-rounded" style="padding:4px 4px;color: white;"  id="delete<?php echo $pay_id; ?>" data-title="Remove Payments" data-toggle="tooltip"><i class="pg-icon">trash_alt</i></a>



           <script type="text/javascript">
                    $(document).ready(function(){ 

                      var admin_status001 = $('#admin_status<?php echo $pay_id; ?>').val();

                      if(admin_status001 == '0')
                      {
                            
                            $('#disapprove<?php echo $pay_id; ?>').css('opacity','10');
                            $('#disapprove<?php echo $pay_id; ?>').css('pointer-events','auto');



                            $('#approve<?php echo $pay_id; ?>').css('opacity','10');
                            $('#approve<?php echo $pay_id; ?>').css('pointer-events','auto');

                      }


                      if(admin_status001 == '1')
                      {
                          $('#disapprove<?php echo $pay_id; ?>').css('opacity','10');
                          $('#disapprove<?php echo $pay_id; ?>').css('pointer-events','auto');



                          $('#approve<?php echo $pay_id; ?>').css('opacity','0.4');
                          $('#approve<?php echo $pay_id; ?>').css('pointer-events','none');

                      }

                      if(admin_status001 == '2')
                      {
                            $('#approve<?php echo $pay_id; ?>').css('opacity','10');
                            $('#approve<?php echo $pay_id; ?>').css('pointer-events','auto');



                            $('#disapprove<?php echo $pay_id; ?>').css('opacity','0.4');
                            $('#disapprove<?php echo $pay_id; ?>').css('pointer-events','none');

                      }


                       $('#approve<?php echo $pay_id; ?>').click(function(){
                        
                        if(confirm('Are you sure approve?'))
                        {
                           var pay_id = $('#pay_id<?php echo $pay_id; ?>').val();
                           var fees = $('#fees<?php echo $pay_id; ?>').val();
                           var pay_approve = 'Approve';
                           $.ajax({
                            url:'../admin/query/update.php',
                            method:"GET",
                            data:{admin_approve:pay_id,status:pay_approve,fees:fees},
                            success:function(data)
                            {

                              $('#tr_msg<?php echo $pay_id; ?>').html('<label class="label label-success"><span class="fa fa-check-circle"></span> Paid</label>');


                              $('#disapprove<?php echo $pay_id; ?>').css('opacity','10');
                              $('#disapprove<?php echo $pay_id; ?>').css('pointer-events','auto');



                              $('#approve<?php echo $pay_id; ?>').css('opacity','0.4');
                              $('#approve<?php echo $pay_id; ?>').css('pointer-events','none');

                            }
                           });
                        }
                      });
                    });
                  </script>
                  <script type="text/javascript">
                    $(document).ready(function(){  
                       $('#disapprove<?php echo $pay_id; ?>').click(function(){

                        if(confirm('Are you sure disapprove?'))
                        {
                           var pay_id = $('#pay_id<?php echo $pay_id; ?>').val();
                           var fees = $('#fees<?php echo $pay_id; ?>').val();
                           var pay_approve = 'Disapprove';
                           $.ajax({
                            url:'../admin/query/update.php',
                            method:"GET",
                            data:{admin_approve:pay_id,status:pay_approve,fees:fees},
                            success:function(data)
                            {
                              $('#tr_msg<?php echo $pay_id; ?>').html('<span class="label label-danger"><span class="fa fa-times-circle"></span> Rejected</span>');

                              $('#disapprove<?php echo $pay_id; ?>').css('opacity','0.4');
                              $('#disapprove<?php echo $pay_id; ?>').css('pointer-events','none');


                              $('#approve<?php echo $pay_id; ?>').css('opacity','10');
                              $('#approve<?php echo $pay_id; ?>').css('pointer-events','auto');

                            }
                           });
                        }
                      });
                    });
                  </script>


                  <script type="text/javascript">
                    $(document).ready(function(){  
                       $('#delete<?php echo $pay_id; ?>').click(function(){

                        if(confirm('Are you sure delete?'))
                        {
                           var pay_id = $('#pay_id<?php echo $pay_id; ?>').val();
                           var first = $('#first<?php echo $pay_id; ?>').val();
                           var second = $('#second<?php echo $pay_id; ?>').val();
                           $.ajax({
                            url:'../admin/query/delete.php',
                            method:"GET",
                            data:{payment_delete:pay_id,first:first,second:second},
                            success:function(data)
                            {
                              $('#delete_tr<?php echo $pay_id; ?>').hide();

                            }
                           });
                        }
                      });
                    });
                  </script>


                  <?php

                        echo '
                            <div class="modal fade slide-up disable-scroll" id="profile'.$pay_id.'" tabindex="-1" role="dialog" aria-hidden="false" style="overflow:auto;">
                              <div class="modal-dialog ">
                              <div class="modal-content-wrapper">
                              <div class="modal-content modal-lg">
                              <div class="modal-header clearfix text-left">
                              <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
                              </button>
                              <h5>Student Details</h5>
                              </div>
                              <div class="modal-body">';
                              ?>

                                <div class="row" style="border-bottom:1px solid #cccc;padding-bottom: 10px;">
                                  <div class="col-md-4"></div>
                                  <div class="col-md-4">
                                    <center><img src="../student/images/profile/<?php echo $pic; ?>" width="140px" height="140px;"></center>
                                  </div>
                                  <div class="col-md-4"></div>
                                </div>

                                <div class="row" style="margin-top: 8px;">
                                  <div class="col-md-6">
                                  <div class="form-group form-group-default">
                                  <label>Full Name</label>
                                  <h5><?php echo $name; ?></h5>
                                  </div>
                                  </div>

                                  <div class="col-md-6">
                                  <div class="form-group form-group-default">
                                  <label>Date Of Birth</label>
                                  <h5><?php echo $dob; ?></h5>
                                  </div>
                                  </div>

                                </div>



                                <div class="row">
                                  <div class="col-md-6">
                                  <div class="form-group form-group-default">
                                  <label>Telephone No</label>
                                  <h5><?php echo  $tp; ?></h5>
                                  </div>
                                  </div>

                                  <div class="col-md-6">
                                  <div class="form-group form-group-default">
                                  <label>E-mail Address</label>
                                  <h5><?php echo $email; ?></h5>
                                  </div>
                                  </div>

                                </div>







                                <div class="row">
                                  <div class="col-md-6">
                                  <div class="form-group form-group-default">
                                  <label>Address</label>
                                  <h5><?php echo  $address; ?></h5>
                                  </div>
                                  </div>

                                  <div class="col-md-6">
                                  <div class="form-group form-group-default">
                                  <label>Gender</label>
                                  <h5><?php echo $gender; ?></h5>
                                  </div>
                                  </div>

                                </div>




                                      <?php echo '</div>
                                      </div>
                                      </div>
                                      </div>

                                  </div>
                                  </div>
                                  ';
                                  ?>


                            <div class="modal fade slide-up disable-scroll" id="check_payment<?php echo $pay_id; ?>" tabindex="-1" role="dialog" aria-hidden="false" style="overflow:auto;">
                              <div class="modal-dialog ">
                              <div class="modal-content-wrapper">
                              <div class="modal-content modal-lg">
                              <div class="modal-header clearfix text-left">
                              <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
                              </button>
                              <h5>Payment Details</h5>
                              </div>
                              <div class="modal-body">

                                  <div class="card card-transparent ">

                                  <ul class="nav nav-tabs nav-tabs-fillup" data-init-reponsive-tabs="dropdownfx">

                                  <li class="nav-item">
                                  <a href="#" class="active" data-toggle="tab" data-target="#slide1<?php echo $pay_id; ?>"><span>First Upload</span></a>
                                  </li>
                                  <?php 
                                    if($second_upload_img !== '0')
                                    {

                                      echo '<li class="nav-item">
                                        <a href="#" data-toggle="tab" data-target="#slide2'.$pay_id.'"><span>Second Upload</span></a>
                                      </li>';
                                    }
                                   ?>
                                  

                                  </ul>
                                  
                                  <style type="text/css">
                                    .zoom {
                                        transition: transform .2s; /* Animation */
                                        margin: 0 auto;
                                      }

                                      .zoom:hover {
                                        transform: scale(1.5); /* (150% zoom - Note: if the zoom is too large, it will go outside of the viewport) */
                                      
                                        width: 550px;
                                        height: 550px;

                                        cursor: zoom-in;
                                      }
                                  </style>

                                  <div class="tab-content">
                                    

                                  <div class="tab-pane slide-left active" id="slide1<?php echo $pay_id; ?>">
                                  
                                    <div class="col-md-12" style="height: 380px;width: 100%;">
                                      <center>

                                        <?php 

                                          if($first_upload_img == '0')
                                          {
                                            echo '<label class="text-center text-danger" style="margin-top:25%;font-size:18px;"><span class="fa fa-warning text-danger"></span> Not Available..</label>';
                                          }else
                                          if($first_upload_img !== '0')
                                          {
                                            echo '<a href="../admin/images/reciption/'.$first_upload_img.'" target="_blank"><img src="../admin/images/reciption/'.$first_upload_img.'" class="image-responsive zoom" style="height: 450px;"></a>';
                                          }
                                         ?>

                                        </center>

                                    </div>

                                  </div>

                                  <div class="tab-pane slide-left" id="slide2<?php echo $pay_id; ?>">
                                  
                                    <div class="col-md-12" style="height: 380px;width: 100%;">
                                      <center><a href="../admin/images/reciption/<?php echo $second_upload_img; ?>" target="_blank"><img src="../admin/images/reciption/<?php echo $second_upload_img; ?>" class="image-responsive zoom" style="height: 450px;"></a></center>
                                    </div>

                                  </div>

                                  </div>
                                  </div>


                              </div>
                            </div>
                          </div>
                        </div>
                      </div>



                            <div class="modal fade slide-up disable-scroll" id="payments" tabindex="-1" role="dialog" aria-hidden="false" style="overflow: auto;">
                              <div class="modal-dialog ">
                              <div class="modal-content-wrapper">
                              <div class="modal-content modal-lg">
                              <div class="modal-header clearfix text-left">
                              <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
                              </button>
                              <h5>Profile Details</h5>
                              </div>
                              <div class="modal-body">
                              <div class="row">
                                <div class="col-md-8">

                                  
                                  
                                </div>
                                <div class="col-md-4"><input type="text" id="search-table33" class="form-control pull-right" placeholder="Search"></div>
                              </div>

                                

                                <div class="table-responsive" style="height: 450px;overflow: auto;margin-bottom: 0;">
                                <table class="table demo-table-search table-responsive-block text-left" id="tableWithSearch">
                                <thead>
                                <tr>
                                <!-- <th style="width: 8%;">Picture</th> -->
                                <th>Payment Date</th>
                                <th style="width: 10%;">Fees(Rs)</th>
                                <th>Month</th>
                                <th>Status</th>
                                </tr>
                                </thead>
                                <tbody id="myTable33">

                                  <tr class="no-data col-md-12 alert alert-danger" style="margin-top: 20px;display: none;">
                                      <td><span class="fa fa-warning"></span> Not Found Data</td>
                                    </tr>
                                    <?php 

                                        $sql002 = mysqli_query($conn,"SELECT * FROM payment_data WHERE STU_ID = '$stu_id' AND PAY_ID != '$pay_id' ORDER BY PAY_TIME DESC");

                                        if(mysqli_num_rows($sql002)>0)
                                        {
                                        while($row002=mysqli_fetch_assoc($sql002))
                                        {
                                          $pay_time = $row002['PAY_TIME'];
                                          $ym = $row002['Y_M'];
                                          $monthNum = $row002['MONTH'];
                                          $y = $row002['YEAR'];
                                          $fees = $row002['FEES'];
                                          $status = $row002['ADMIN_SUBMIT'];

                                          $m = date('F', mktime(0, 0, 0, $monthNum, 10)); // March

                                          if($status == '0')
                                          {
                                            $result = '<span class="label label-warning"><span class="fa fa-spinner"></span> Pending</span>';
                                          }else
                                          if($status == '1')
                                          {
                                            $result = '<span class="label label-success"><span class="fa fa-check-circle"></span> Paid</span>';
                                          }else
                                          if($status == '2')
                                          {
                                            $result = '<span class="label label-danger"><span class="fa fa-times-circle"></span> Rejected</span>';
                                          }
                                        

                                          echo '
                                            <tr>
                                              <td>'.$pay_time.'</td>
                                              <td>'.number_format($fees,2).'</td>
                                              <td>'.$y.'-'.$m.'</td>
                                              <td>'.$result.'</td>
                                            </tr>';
                                        }
                                      }

                                     ?>
                                </tbody>
                                </table>
                                </div>



                              </div>
                              </div>
                              </div>
                              </div>

                              </div>
                            </div>

                            </div>



                            </td>
                            

                          </tr>




        <?php




          }

        }
 ?>
<div class="modal fade slide-up disable-scroll" id="filter" tabindex="-1" role="dialog" aria-hidden="false">
                              <div class="modal-dialog ">
                              <div class="modal-content-wrapper">
                              <div class="modal-content">
                              <div class="modal-header clearfix text-left">
                              <button aria-label="" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-icon">close</i>
                              </button>
                              <h5>Print Payment Details</h5>
                              </div>
                              <div class="modal-body">
                                  <div class="col-md-12">
                                          <form action="admin_print_payment.php" method="POST" target="_blank">
                                            <div class="row">
                                            <div class="col-md-12">
                                              <div class="form-group form-group-default" style="margin-top: 10px;">
                                                <label class="pull-left">Student Name</label>
                                                <select class="form-control select_class" name="student_id" required style="cursor: pointer;">
                                                  <option value="">Select Student Name </option>
                                                  <option value="All">All Students </option>

                                                    <?php 
                                                        $sql00151 = mysqli_query($conn,"SELECT * FROM stu_login WHERE STATUS = 'Active'");
                                                        while($row00152 = mysqli_fetch_assoc($sql00151))
                                                        {
                                                          $stud_id = $row00152['STU_ID'];

                                                          $sql0015 = mysqli_query($conn,"SELECT * FROM student_details WHERE STU_ID = '$stud_id'");
                                                          while($row0015=mysqli_fetch_assoc($sql0015))
                                                          {
                                                            $name = $row0015['F_NAME']." ".$row0015['L_NAME'];
                                                            $stu_id = $row0015['STU_ID'];

                                                            echo '<option value='.$stu_id.'>'.$name.'</option>';

                                                          }
                                                        }

                                                        

                                                         ?>
                                                  
                                                </select>

                                              </div>
                                              </div>
                                            </div>
                                          <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group form-group-default" style="margin-top: 10px;">
                                                  <label class="pull-left">Start Year</label>
                                                  
                                                  <select class="form-control select_class" name="start_year" id="start_year" required style="cursor: pointer;">
                                                    <option value="">Select Year </option>

                                                    <?php 
                                                      $this_year = date('Y');
                                                      $ten = strtotime("-10 years");
                                                      $ten_year = date('Y',$ten);

                                                      for($i=$ten_year;$i<=$this_year;$i++)
                                                      {
                                                          echo '<option value="'.$i.'">'.$i.'</option>';
                                                      }

                                                     ?>
                                                    
                                                  </select>

                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group form-group-default" style="margin-top: 10px;">
                                                  <label class="pull-left">Start Month</label>
                                                  
                                                  <select class="form-control select_class" name="start_month" id="start_month" required style="cursor: pointer;">

                                                    <option selected  value="">Select Month </option>
                                                    <option value='01'>Janaury</option>
                                                    <option value='02'>February</option>
                                                    <option value='03'>March</option>
                                                    <option value='04'>April</option>
                                                    <option value='05'>May</option>
                                                    <option value='06'>June</option>
                                                    <option value='07'>July</option>
                                                    <option value='08'>August</option>
                                                    <option value='09'>September</option>
                                                    <option value='10'>October</option>
                                                    <option value='11'>November</option>
                                                    <option value='12'>December</option>
                                                    
                                                  </select>

                                                </div>
                                            </div>
                                          </div> 

                                          <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group form-group-default" style="margin-top: 10px;">
                                                  <label class="pull-left">End Year</label>
                                                  
                                                  <select class="form-control select_class" name="end_year" id="end_year" required style="cursor: pointer;">
                                                    <option selected value="">Select Year </option>

                                                     
                                                     <?php 
                                                      $this_year = date('Y');
                                                      $ten = strtotime("+10 years");
                                                      $ten_year = date('Y',$ten);

                                                      for($i=$this_year;$i<=$ten_year;$i++)
                                                      {
                                                          echo '<option value="'.$i.'">'.$i.'</option>';
                                                      }

                                                     ?>
                                                    
                                                  </select>

                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group form-group-default" style="margin-top: 10px;">
                                                  <label class="pull-left">End Month</label>
                                                  
                                                  <select class="form-control select_class" name="end_month" id="end_month" required style="cursor: pointer;">
                                                    <option selected value="">Select Month </option>
                                                    <option value='01'>Janaury</option>
                                                    <option value='02'>February</option>
                                                    <option value='03'>March</option>
                                                    <option value='04'>April</option>
                                                    <option value='05'>May</option>
                                                    <option value='06'>June</option>
                                                    <option value='07'>July</option>
                                                    <option value='08'>August</option>
                                                    <option value='09'>September</option>
                                                    <option value='10'>October</option>
                                                    <option value='11'>November</option>
                                                    <option value='12'>December</option>
                                                    
                                                    
                                                  </select>

                                                </div>
                                            </div>
                                          </div>

                                          <div id="msg" class="text-danger" style="display: none;"></div>

                                          <script type="text/javascript">
                                            $(document).ready(function(){
                                              $('#end_year,#end_month').click(function(){
                                                var start_year = $('#start_year').val();
                                                var start_month = $('#start_month').val();

                                                var end_year = $('#end_year').val();
                                                var end_month = $('#end_month').val();

                                                if(start_year == end_year && start_month == end_month)
                                                {
                                                  $('#search_btn').prop('disabled',true);
                                                  $('#msg').show();
                                                  //$('#search_btn').css('cursor','not-allowed');
                                                  $('#msg').html('<span class="fa fa-warning"></span> Please select different years & month.');
                                                }

                                                if(start_year !== end_year && start_month !== end_month)
                                                {
                                                    $('#search_btn').prop('disabled',false);
                                                    $('#msg').hide();

                                                }

                                              });
                                            });
                                          </script>

                                                  <button type="submit" class="btn btn-success btn-lg btn-block" name="filter_data" value="<?php echo $stu_id; ?>" style="width: 100%;margin: none;" id="search_btn"><i class="pg-icon">search</i> Search</button>

                                                  <button type="submit" class="btn btn-default btn-lg btn-block" data-dismiss="modal" data-toggle="modal" data-target="#payments<?php echo $stu_id; ?>" style="width: 100%;margin: none;"><i class="pg-icon">arrow_left</i> Back</button>


                                          </form>
                                          </div>


                                </div>
                              </div>
                              </div>
                              </div>

                              </div>

</tbody>
</table>
</div>



                <div class="col-md-12" style="width: 100%;overflow: auto;">
                  <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-8" class="php_pagination">
                      
                      <div class="col-md-12">
                    <br />
                    <?php
                    $total_records = mysqli_num_rows($page_result);
                    $total_pages = ceil($total_records/$record_per_page);
                    $start_loop = $page;
                    $difference = $total_pages - $page;
                    if($difference <= 5)
                    {
                     $start_loop = $total_pages - 5;
                    }else
                    if($difference <= 0)
                    {
                      $start_loop = '0';
                    }

                    echo '<ul class="pagination">';
                    $end_loop = $start_loop + 4;
                    $five_loop = $page-5;
                    if($five_loop <= 0)
                    {
                      $five_loop = 1;
                    }
                    if($page > 1)
                    {

                      echo '<li><a href="payment_pending.php?page=1">First</a></li>';
                      echo '<li><a href="payment_pending.php?page='.($page - 1).'"> < </a></li>';
                      echo '<li><a href="payment_pending.php?page='.$five_loop.'"> <<< </a></li>';

                    }
                    if($start_loop !== '1')
                    {
                      $few_no = $start_loop - 1;
                    }else
                    if($start_loop > 0)
                    {
                      $few_no = 1;
                    }
                    
                    for($i=$few_no; $i<=$end_loop+1; $i++)
                    {     
                      if($i == $page)
                      {
                        echo '<li class="active"><a href="payment_pending.php?page='.$i.'" style="font-weight:bold;background-color:#2980b9;color:white;">'.$i.'</a></li>';
                      }else
                      {
                        if($i == '0')
                        {
                          continue;
                        }else
                        if($i>0)
                        {
                          echo '<li><a href="payment_pending.php?page='.$i.'" >'.$i.'</a></li>';
                        }
                        
                      }

                    }
                    if($page <= $end_loop)
                    {
                      
                      if($page == '1')
                      {
                        $nxt_five_loop = 5;
                      }else
                      if($page >= 5)
                      {
                        $nxt_five_loop = $page+5;

                        if($total_pages < $nxt_five_loop)
                        {
                          $nxt_five_loop = $total_pages;
                        }
                      }else
                      {
                        $nxt_five_loop = $page+5;
                      }

                        echo '<li><a href="payment_pending.php?page='.($page + 1).'" > > </a></li>';
                        echo '<li><a href="payment_pending.php?page='.$nxt_five_loop.'" > >>> </a></li>';
                        echo '<li><a href="payment_pending.php?page='.$total_pages.'" >Last</a></li>';
                      
                    }
                    
                    echo '</ul>';
                    
                    ?>
                    </div>

                    </div>
                  </div>
                    
                </div>


</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>





<script type="text/javascript">
  $('.save_btn').click(function(){
    
    var upload_btn0 = $('#selectedFile0').val();
    var upload_btn = $('#sel_file').val();
      
    if(empty(upload_btn0))
    {
      alert('Please select your audio file.');
    }else
    if(empty(upload_btn))
    {
      alert('Please select your document file.');
    }
   });
</script>

<script type="text/javascript">
  $('.navi').click(function(){
    $('.frm')[0].reset();
      
   });
</script>

<script>
$(document).ready(function(){
  $("#myInput").on("blur", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
<!-- search data -->

<!-- no data message/no found data show in search-->

<script type="text/javascript">
  $(document).ready(function () {

    (function ($) {

        $('#myInput').blur(function () {
            var rex = new RegExp($(this).val(), 'i');
            $('#myTable tr').hide();
            $('#myTable tr').filter(function () {
                return rex.test($(this).text());
            }).show();
            $('.no-data').hide();
            if($('#myTable tr:visible').length == 0)
            {
                $('.no-data').show();
            }

        })

    }(jQuery));

});
</script>





</div>
<div class=" container-fluid  container-fixed-lg footer">
<div class="copyright sm-text-center" >
<p style="text-align: center;">
©2017-<?php echo date('Y'); ?> All Rights Reserved. ©IBS Developer Team
  <img src="../new_admin/images/ibs.ico" height="20px" width="20px">
</p>
<div class="clearfix"></div>
</div>
</div>

<script src="assets/plugins/pace/pace.min.js" type="text/javascript"></script>

<script src="assets/plugins/liga.js" type="text/javascript"></script>
<script src="assets/plugins/jquery/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="assets/plugins/modernizr.custom.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="assets/plugins/popper/umd/popper.min.js" type="text/javascript"></script>
<script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery/jquery-easy.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-actual/jquery.actual.min.js"></script>
<script src="assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js"></script>
<script type="text/javascript" src="assets/plugins/select2/js/select2.full.min.js"></script>
<script type="text/javascript" src="assets/plugins/classie/classie.js"></script>



<script src="pages/js/pages.js"></script>
<script src="assets/js/scripts.js" type="text/javascript"></script>
<script src="assets/js/scripts.js" type="text/javascript"></script>

</body>

<!-- Mirrored from pages.revox.io/dashboard/latest/html/condensed/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 15 Jul 2020 08:50:00 GMT -->
</html>


                  