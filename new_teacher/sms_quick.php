
<?php

$page = "Quick SMS";
$folder_in = '0';


  include('header/header.php'); 
 ini_set( "display_errors", 0); 

 $admin_id = $_SESSION['ADMIN_ID'];
 $_SESSION['not_refresh'] = '0';

  ?>
  <style type="text/css">
    .btn_pulse {
  
  display: block;
  border-radius: 10%;
  cursor: pointer;
  animation: none;
  float: left;
  bottom: 5px;
  right: 5px;
  font-weight: bold;
  padding-top: 2px;
  text-align: center;
  color: white;
  font-size: 14px;
  animation: pulse 1.6s infinite;
}


@keyframes pulse {
  0% {
    -moz-box-shadow: 0 0 0 0 #7252d3;
    box-shadow: 0 0 0 0 #7252d3;
  }
  70% {
    -moz-box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
  }
  100% {
    -moz-box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
  }
}
  </style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


    <ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-top: 04%;margin-bottom: 0;">
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="SMS Main Menu"> <a href="sms.php">SMS Main Menu</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="New Classes"> Quick SMS</li>
    </ol>

<?php 

      $sql001 = mysqli_query($conn,"SELECT * FROM `sms_counter` ORDER BY `SUB_SMS_C_ID` DESC");
      $qu001 = mysqli_fetch_assoc($sql001);
      $current_sms = $qu001['CURRENT_SMS'];

      if($current_sms<9)
      {
        $color = 'danger';
      }
      else
      if($current_sms>9)
      {
        $color = 'success';
      }
     ?>


<div class="col-md-12" style="border-top: 1px solid #cccc;">
  
<div class="row" style="margin-top: 3%;">
<div class="col-md-3"></div>
<div class="col-md-6">
<div class=" container-fluid   container-fixed-lg bg-white" style="padding: 2px 20px;">
<div class="card card-transparent">
<div class="card-header ">
<div class="card-title col-md-12">
   <div class="row">
    <div class="col-md-9"><h3 style="text-transform: capitalize;"> <span class="fa fa-envelope"></span> Quick SMS</h3></div>
    <div class="col-md-3">
        
        <h5 class="label label-<?php echo $color; ?> text-center" style="font-size: 25px;box-shadow: 1px 1px 8px 1px #cccc;">

        <?php 

            if($current_sms<9)
            {
              echo '0'.$current_sms;

              $available_sms = '<span class="fa fa-check-circle text-success"></span> <span class="text-success text-center">Available SMS - 0'.$current_sms.'</span>';
            }
            else
            if($current_sms>9)
            {
                echo number_format($current_sms);
                $available_sms = '<span class="fa fa-check-circle text-success"></span> <span class="text-success text-center">Available SMS - '.number_format($current_sms).'</span>';

            }

            if($current_sms == '0')
            {
                $disable_input = 'disabled';
            }else
            if($current_sms>0)
            {
                $disable_input = '';
            }

         ?> 
         <br>
         <small style="font-size: 12px;text-align: center;">Current SMS</small></h5>

    </div>
  </div>


</div>
<div class="clearfix"></div>
</div>

        <form action="../admin/query/insert.php" method="POST">
            
          <div class="row" style="padding: 6px 4px 6px 4px;">

            <div class="col-md-12"> 
              <div class="form-group form-group-default" style="margin-top: 10px;">
                <label>Teacher Name</label>
                
                
                <select name="teacher" class="form-control" required id="teacher" <?php echo $disable_input; ?> >
                  <option value="">Select Teacher Name</option>
                  <?php 
                    $sql001 = mysqli_query($conn,"SELECT * FROM `teacher_login` WHERE `STATUS` = 'Active'");
                    while($row001 = mysqli_fetch_assoc($sql001))
                    {
                      $t_id = $row001['TEACH_ID'];

                       $sql002 = mysqli_query($conn,"SELECT * FROM `teacher_details` WHERE `TEACH_ID` = '$t_id'");
                      while($row002 = mysqli_fetch_assoc($sql002))
                      {
                        $name = $row002['POSITION'].". ".$row002['F_NAME']." ".$row002['L_NAME'];
                      }

                      echo '<option value="'.$t_id.'">'.$name.'</option>';
                    }
                   ?>
                </select>

                </div> 
              </div>
            </div>

            <div class="row" style="padding: 6px 4px 6px 4px;">
              <div class="col-md-11"> 
              <div class="form-group form-group-default">
                <label>Class Name</label>

                
                
                <select name="clz" class="form-control" required id="clz" <?php echo $disable_input; ?>>
                  <option value="">Select Class Name</option>
                </select>

                </div> 
              </div>


              <div class="col-md-1">

              <center><button type="button" class="btn btn-success btn-block" name="select_student" style="border-radius: 60px;font-size: 22px;padding: 8px 12px 8px 12px;margin-top: 6px;" id="select_stu_btn" data-toggle="modal" data-target="#select_student" <?php echo $disable_input; ?>><span class="fa fa-users"></span></button></center>

              <div id="select_student" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">

                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Select Student</h4>
                    </div>
                    <div class="modal-body">

                      <input class="form-control" id="search" type="text" placeholder="Search..">
                      
                      <div class="table-responsive" style="height: auto;overflow: auto;">
                       
                        <table class="table" id="mydatatable">
                          <thead>
                            <td>#</td>
                            <td>Name</td>
                            <td>Register ID</td>
                            <td>Telephone No</td>
                          </thead>
                          <tbody id="tbl">
                              <tr>
                                    <td colspan="4" class="text-danger text-center"><span class="fa fa-warning"></span> Not Found Data</td>
                              </tr>
                              <tr class="no-data alert alert-danger">
                                    <td colspan="4" class="text-danger"><span class="fa fa-warning"></span> Not Found Data</td>
                              </tr>
                          </tbody>
                        </table>
                      </div>

                      <?php echo $available_sms; ?>
                    </div>
                    <div class="modal-footer">

                      <button class="btn btn-success btn-lg" id="sub_btn" data-dismiss="modal">Submit (<span id="student_count">0</span>)</button>
                      <button type="button" class="btn btn-default btn-lg" data-dismiss="modal">Close</button>
                    </div>
                  </div>

                </div>
              </div>

            </div>

              </div>

              
            
            <div class="form-group form-group-default" style="margin-top: 10px;">
              <label>Message</label>
              
              <textarea class="form-control" id="input"  name="msg_body" required placeholder="Enter Your Message" onclick="this.select();" style="height: 80px;resize: none" maxlength="124" <?php echo $disable_input; ?>></textarea>
              <div style="text-align: right;"><i>Characters <small id="character-count">0</small></i></div>

              <script type="text/javascript">
                $(document).ready(function(){


                  var input = $('textarea#input');
                  $('small#character-count').text(input.val().length);

                  input.bind('keydown', function() {
                      $('small#character-count').text(input.val().length);
                  });

                });
              </script>

            </div>
              <div class="col-md-12" style="border-top:1px solid #cccc;padding-bottom:10px;"></div>

              <?php 
                

                if($current_sms == '0')
                {
                    echo '<button type="submit" class="btn btn-success btn-block btn-lg" disabled id="multi_btn" name="multi_sms_submit" style="margin-bottom: 8px;"><i class="pg-icon">tick</i> Submit</button>
                      <span class="text-danger text-center" style="padding-top:6px;"><span class="fa fa-warning"></span> Your SMS package is over.Please recharge your sms package.</span>
                    ';
                }else
                if($current_sms>0)
                {
                    echo '<button type="submit" class="btn btn-success btn-block btn-lg" id="multi_btn" name="multi_sms_submit" style="margin-bottom: 8px;"><i class="pg-icon">tick</i> Submit</button>';
                }
               ?>
            <br><center><a href="sms.php">Back</a></center>
            </form>
            </div>

</div>
<div class="col-md-3  ">
 
</div>

</div>
</div>


  <script type="text/javascript">

    $(document).ready(function(){ 
    $('#teacher').bind('change',function(){

      var teacher_id = $('#teacher').val();

      $.ajax({
        url:'../admin/query/check.php',
        method:"POST",
        data:{get_classes_data:teacher_id},
        success:function(data)
        {
          //alert(data)
          $('#clz').html(data);
          
        }
      })
    

    });
  });
  </script>


   <script type="text/javascript">

    $(document).ready(function(){ 
    $('#clz').bind('keyup change',function(){

      var clz_id = $('#clz').val();
      var teach_id = $('#teacher').val();

      $.ajax({
        url:'../admin/query/check.php',
        method:"POST",
        data:{check_students:clz_id,teacher_id:teach_id},
        success:function(data)
        {
          //alert(data)
          $('#tbl').html(data);
          
        }
      })
    

    });
  });
  </script>



   <script type="text/javascript">

    $(document).ready(function(){ 
    $('#multi_btn').prop('disabled',true);
    $('#select_stu_btn').prop('disabled',true);

    $('#clz').bind('keyup change',function(){

      var clz_id = $('#clz').val();

      $.ajax({
        url:'../admin/query/check.php',
        method:"POST",
        data:{student_count:clz_id},
        success:function(data)
        {
          //alert(data)
          $('#student_count').html(data);

          $('#student_count2').html(data);

          if(data == 0)
          {
            $('#multi_btn').prop('disabled',true);
            $('#select_stu_btn').prop('disabled',true);
          }
          
          if(data>0)
          {
            $('#multi_btn').prop('disabled',false);
            $('#select_stu_btn').prop('disabled',false);
          }
          
        }
      })
    

    });
  });
  </script>


<script>
$(document).ready(function(){
  $("#search").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#tbl tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
<!-- search data -->

<!-- no data message/no found data show in search-->

<script type="text/javascript">
  $(document).ready(function () {

    (function ($) {

        $('#search').keyup(function () {
            var rex = new RegExp($(this).val(), 'i');
            $('#tbl tr').hide();
            $('#tbl tr').filter(function () {
                return rex.test($(this).text());
            }).show();
            $('.no-data').hide();
            if($('#tbl tr:visible').length == 0)
            {
                $('.no-data').show();
            }

        })

    }(jQuery));

});
</script>

<script type="text/javascript">
  $('#multi_btn').click(function(){

    var msg = $('#input').val();

    if(msg !== '')
    {
      $('#waiting_modal').modal();
    }
    

  });
</script>

<?php  include('footer/footer.php'); ?>

<div id="waiting_modal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
        </div>
        <div class="modal-body">
          
         <center> <img src="images/send_sms_animation.gif" class="image-responsive" style="max-width: 100%;height: 350px;"></center>

          <h5 class="text-danger bold">
           කෙටි පණිවිඩයන් අදාල දුරකථන අංක වෙත යොමු කරමින් පවතින අතර කිසිදු හේතුවක් මත වෙබ් බ්‍රවුසරයෙන් ඉවත්වීම හෝ <span class="fa fa-refresh"></span> Refresh කිරීමක් සිදු නොකරන්න.
          </h5>
        </div>
        <div class="modal-footer">
        </div>
      </div>

    </div>
  </div>
