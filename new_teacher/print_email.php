<?php 

  include('../connect/connect.php');
  

 ?>
<!DOCTYPE html>
<html>
<head>
	<style type="text/css" media="print,screen">
.hideMe{
display:block;
}

.PrintClass {
display:block;

}
.NoPrintClass{
display:block;
}
table {
   width: 100%;
   border-collapse: collapse;
   table-layout: fixed;
}

.cell-breakWord {
   word-wrap: break-word;
}

</style>
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script type="text/javascript"
    src="http://jqueryjs.googlecode.com/files/jquery-1.3.1.min.js"> </script>
    <link href="https://fonts.googleapis.com/css?family=PT+Sans+Narrow&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
<script type="text/javascript">


function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     document.body.innerHTML = originalContents;

     window.print();
    window.onafterprint = function() {
    window.close();
    };
     

}

   </script>

</head>
<body onload="printDiv('printableArea')">



<div class="col-md-12" id="printableArea" style="display:block;">
  
  <?php 


		  
      $sql0027 = mysqli_query($conn,"SELECT * FROM institute WHERE INS_ID = '1'");
      while($row0027 = mysqli_fetch_assoc($sql0027))
      {
          $ins_name = $row0027['INS_NAME'];
          $ins_tp = $row0027['INS_TP'];
          $ins_mobile = "/ ".$row0027['INS_MOBILE'];
          
          $ins_address = $row0027['INS_ADDRESS'];
      }
	




   ?>

  <div class="row">
    
    <div class="col-md-4"></div>
    <div class="col-md-4">
      
      <h1 style="text-align: center;"><?php echo $ins_name; ?></h1>
      <h5 style="text-align: center;"><?php echo $ins_address; ?> <br> <?php echo $ins_tp; ?> <?php echo $ins_mobile; ?></h5>

    </div>
    <div class="col-md-4"></div>

  </div>

      <div class="col-md-12" style="border-top: 1px solid #cccc;"></div>

      <div class="row" style="border-bottom:0px solid #cccc;">
        <div class="col-md-4"></div>
        <div class="col-md-4 text-center" style="padding: 12px 0 0px 0;"><h1>Student Details</h1></div>
        <div class="col-md-4"></div>
      </div>
      <div class="table-responsive" style="margin-top: 10px;width: 100%;font-size: 15px;">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th style="width: 11%;">Registered ID</th>
              <th style="width: 40%;">Student Name</th>
              <th style="width: 40%;"> Email Address</th>
              <th style="width: 12%;"> Contact No</th>
            </tr>
          </thead>
          <tbody>

          <?php 
          
            $sql0020 = mysqli_query($conn,"SELECT * FROM stu_login WHERE STATUS = 'Active'");
            
            if(mysqli_num_rows($sql0020)>0)
            {
              
              while($row0020 = mysqli_fetch_assoc($sql0020))
              {
                $reg_id = $row0020['REGISTER_ID'];
                $stu_id = $row0020['STU_ID'];
                $reg_date = $row0020['REG_DATE'];

                $sql0021 = mysqli_query($conn,"SELECT * FROM student_details WHERE STU_ID = '$stu_id'");
                while($row0021 = mysqli_fetch_assoc($sql0021))
                {
                  $f_name = $row0021['F_NAME'];
                  $l_name = $row0021['L_NAME'];
                  $tp = $row0021['TP'];
                  $email = $row0021['EMAIL'];
                  $stu_id = $row0021['STU_ID'];
              
                  echo '<tr>
                    <td>'.$reg_id.'</td>
                    <td>'.$f_name.' '.$l_name.'</td>
                    <td>'.$email.'</td>
                    <td>'.$tp.'</td>
                  </tr>';
                }
              }
            }else
            if(mysqli_num_rows($sql0020)=='0')
            {
              echo '<tr><td colspan="4" class="text-danger text-center"><span class="fa fa-warning text-danger"></span> Empty Data!</td></tr>';
            }
            

           ?>
         
        </tbody>
      </table>
    </div>

    <div class="col-md-1"></div>

  </div>

</body>
</html>