
<?php

$page = "Student Password Reset";
$folder_in = '0';

  include('header/header.php'); 
 ini_set( "display_errors", 0); 

 $admin_id = $_SESSION['ADMIN_ID'];

  ?>
  <style type="text/css">
    .btn_pulse {
  
  display: block;
  border-radius: 10%;
  cursor: pointer;
  animation: none;
  float: left;
  bottom: 5px;
  right: 5px;
  font-weight: bold;
  padding-top: 2px;
  text-align: center;
  color: white;
  font-size: 14px;
  animation: pulse 1.6s infinite;
}


@keyframes pulse {
  0% {
    -moz-box-shadow: 0 0 0 0 #7252d3;
    box-shadow: 0 0 0 0 #7252d3;
  }
  70% {
    -moz-box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
  }
  100% {
    -moz-box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
  }
}
  </style>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


    <ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-top: 04%;margin-bottom: 0;">
      <li class="breadcrumb-item" style="font-size: 50px;" data-toggle="tooltip" data-title="Dashboard"> <a href="dashboard.php">Dashboard</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="Student Password Reset"> Student Password Reset</li>
    </ol>




<div class="col-md-12" style="border-top: 1px solid #cccc;">
  
<div class="row" style="margin-top: 3%;">
<div class="col-md-4"></div>
<div class="col-md-4">
<div class=" container-fluid   container-fixed-lg bg-white" style="padding: 10px 20px 4px 20px;">
<div class="card card-transparent">
<div class="card-header ">
<div class="card-title">


  <h3 style="text-transform: capitalize;"> <span class="fa fa-users"></span> Student Password Reset</h3>


</div>
<div class="clearfix"></div>
</div>

            
            <div class="form-group form-group-default" style="margin-top: 10px;">
              <label>Student ID / Name</label>
              <input list="search_student" name="student" id="student" required class="form-control" placeholder="Type Student ID / Name.." onclick="this.value=''">

              <datalist id="search_student">

                <?php 
                
                    $teach_id = $_SESSION['TEACH_ID'];
                  
                    $sql0015 = mysqli_query($conn,"SELECT * FROM `student_details`");
                    while($row0015=mysqli_fetch_assoc($sql0015))
                    {
                      $f_name = $row0015['F_NAME'];
                      $l_name = $row0015['L_NAME'];
                      $s_id = $row0015['STU_ID'];

                      $sql002 = mysqli_query($conn,"SELECT * FROM `transactions` WHERE `TEACH_ID` = '$teach_id' AND `STU_ID` = '$s_id'");

                      $check = mysqli_num_rows($sql002);

                      if($check > 0)
                      {

                        $sql0016 = mysqli_query($conn,"SELECT * FROM `stu_login` WHERE `STU_ID` = '$s_id'");
                        while($row0016=mysqli_fetch_assoc($sql0016))
                        {
                          $reg_code = $row0016['REGISTER_ID'];

                          echo '<option value='.$reg_code.'>'.$f_name.' '.$l_name.'</option>';

                        }

                      }

                      

                    }
                ?>
                
              </datalist>

            </div>

            <div class="form-group form-group-default" style="margin-top: 10px;">
              <label>Create Password</label>
              
              <input type="text" id="password" class="form-control" placeholder="Create Password..">

            </div>

            <div id="result" class="text-success" style="margin-bottom: 10px;"></div>

            <button type="submit" class="btn btn-success btn-block btn-lg" name="payment_submit" id="reset_btn" style="margin-bottom: 10px;"><i class="pg-icon">tick</i> Reset</button>


            <script type="text/javascript">
  
                $(document).ready(function(){  
                 


                 $('#reset_btn').bind('click',function(){

                   var stu_id = $('#student').val();
                   var password = $('#password').val();
                   //alert(stu_id)

                   if(stu_id == '' || password == '')
                   {
                      Swal.fire(
                        'Sorry!',
                        'Please fill all the inputs.',
                        'error'
                      )
                   }else
                   {
                      $('#reset_btn').prop('disabled',true);
                         $.ajax({
                          url:'../admin/query/update.php',
                          method:"POST",
                          data:{reset_psw:stu_id,password:password},
                          success:function(data)
                          {
                            Swal.fire(
                              'Successfully!',
                              'Password Reset Successfully!',
                              'success'
                            ).then(function(){

                              location.reload();

                            });
                            
                          }
                         })
                    }

                });
              });
              </script>

            <center><a href="dashboard.php" data-toggle="tooltip" data-title="Back" style="text-align: center;">Back</a></center> 

            </div>

</div>
<div class="col-md-4">
    


</div>

</div>
</div>


<?php  include('footer/footer.php'); ?>


