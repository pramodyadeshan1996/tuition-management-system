<?php 

	session_start();
    include('../../connect/connect.php');


    $_SESSION['update_msg'] = 'Updated Data';

    if(isset($_POST['submit_btn']))
    {

      $new_pass = md5($_POST['npass']);
      $id = $_POST['submit_btn'];

      $sql = mysqli_query($conn,"UPDATE teacher_login SET PASSWORD = '$new_pass' WHERE TEACH_ID = '$id'");

      header('location:../../new_teacher/profile_setting.php');
    }else
    if(isset($_POST['edit_video_btn']))
    {
        $upload_id = $_POST['edit_video_btn'];
        $title = ucwords($_POST['title']);
        $descip = $_POST['descip'];
        $select_class = $_POST['clz'];
        $subject = $_POST['select_subject'];
        $type = $_POST['type'];


        $up_date = date('Y-m-d');
        $up_time = date('h:i:s A');


        $ytd = $_POST['ytd'];

        $s = mysqli_query($conn,"SELECT * FROM `uploads` WHERE `YOUTUBE_LINK` = '$ytd' AND `CLASS_ID` = '$select_class'");
        if(mysqli_num_rows($s)>0)
        {
          
            $update001 = mysqli_query($conn,"UPDATE `uploads` SET `TITLE`='$title',`DESCRIPTION`= '$descip',`UPL_DATE`='$up_date',`UPL_TIME`='$up_time',`TYPE`='Video',`CLASS_ID`='$select_class',`YOUTUBE_LINK`='$ytd' WHERE `UPLOAD_ID` = '$upload_id'");
        }else
        if(mysqli_num_rows($s)=='0')
        {
            mysqli_query($conn,"DELETE FROM uploads_clicked WHERE UPLOAD_ID = '$upload_id'");

            $update001 = mysqli_query($conn,"UPDATE `uploads` SET `TITLE`='$title',`DESCRIPTION`= '$descip',`UPL_DATE`='$up_date',`UPL_TIME`='$up_time',`TYPE`='Video',`CLASS_ID`='$select_class',`YOUTUBE_LINK`='$ytd' WHERE `UPLOAD_ID` = '$upload_id'");
        }
        
       header('location:../../new_teacher/view_upload.php');
        
    }else
    if(isset($_POST['save_link']))
    {

        $teach_id = $_POST['save_link'];
        $link = $_POST['link'];

        $sql = mysqli_query($conn,"UPDATE teacher_details SET ZOOM_LINK = '$link' WHERE TEACH_ID = '$teach_id'");
        mysqli_query($conn,"DELETE FROM link_clicked");
        
        header('location:../profile/new_upload.php');
      }else
    if(isset($_POST['update_class']))
    {

        $class_id = $_POST['update_class'];
        $class = ucwords($_POST['class']);
        $fees = $_POST['fees'];
        $link_id = $_POST['link_id'];
        $start = $_POST['start'];
       $end = $_POST['end'];
        $day = $_POST['day'];

        if($link_id == '')
        {
            $link_id = "0";
        }

        $sql = mysqli_query($conn,"UPDATE `classes` SET `CLASS_NAME`='$class',`FEES`='$fees',`ZOOM_LINK`='$link_id',`START_TIME`='$start',`END_TIME`='$end',`DAY`='$day' WHERE `CLASS_ID` = '$class_id'");

        header('location:../profile/create_class.php');

      }else
      if(isset($_POST['update_teacher']))
    {
      $teach_id = $_POST['update_teacher'];

      $fnm = ucwords($_POST['fnm']);
      $lnm = ucwords($_POST['lnm']);
      $dob = $_POST['dob'];
      $gen = $_POST['gender'];
      $email = $_POST['email'];
      $tp = $_POST['number'];
      $address = $_POST['address'];
      $position = $_POST['position'];
      $qualification = $_POST['qualification'];

      $update001 = mysqli_query($conn,"UPDATE `teacher_details` SET `POSITION`='$position',`F_NAME`='$fnm',`L_NAME`='$lnm',`DOB`='$dob',`ADDRESS`='$address',`TP`='$tp',`EMAIL`='$email',`GENDER`='$gen',`QUALIFICATION`='$qualification' WHERE `TEACH_ID` = '$teach_id'");
      
          
          header('location:../../new_teacher/profile_setting.php');

      
    }else
    if(isset($_POST['upload_img']))
    {
      $teach_id = $_POST['upload_img'];

      $recent_img = $_POST['recent_img'];

      $file = $_FILES['img']['name'];

      if($recent_img !== '0')
      {
          unlink('../images/profile/'.$recent_img);
      }

      if($file !== '0')
      {
          $tmp_file = $_FILES['img']['tmp_name']; //ASSIGN DATA TO VARIABLE
          $ext = pathinfo($_FILES["img"]["name"], PATHINFO_EXTENSION); //GET EXTENTION ON IMAGE

          $base = basename($file,".".$ext);

          $sql001 = mysqli_query($conn,"SELECT * FROM teacher_details WHERE PICTURE LIKE '%".$base."%'");
          $check = mysqli_num_rows($sql001);

          if($check > 0)
          {
              $p1 = basename($file,".".$ext);
              $post_file = $p1."(".$check.").".$ext;
          }else
          if($check == '0')
          {
              $post_file = $file;
          }

          move_uploaded_file($tmp_file,'../images/profile/'.$post_file); //SAVE LOCATION
          
          $update001 = mysqli_query($conn,"UPDATE `teacher_details` SET `PICTURE`='$post_file' WHERE `TEACH_ID` = '$teach_id'");

      }else
      {
          $update001 = mysqli_query($conn,"UPDATE `teacher_details` SET `PICTURE`='0' WHERE `TEACH_ID` = '$teach_id'");

      }
          
          header('location:../../new_teacher/profile_setting.php');
    
  }else
    if(isset($_GET['remove_img']))
    {

        $teach_id = $_GET['remove_img'];
        $recent_img = $_GET['recent_img'];

        $sql = mysqli_query($conn,"UPDATE `teacher_details` SET `PICTURE`='0' WHERE `TEACH_ID` = '$teach_id'");

        unlink('../images/profile/'.$recent_img);


          header('location:../../new_teacher/profile_setting.php');
      }else
    if(isset($_POST['edit_audio_btn']))
    {
        $upload_id = $_POST['edit_audio_btn'];
        $title = ucwords($_POST['title']);
        $descip = $_POST['descip'];
        $select_class = $_POST['clz'];
        $subject = $_POST['select_subject'];
        $type = $_POST['type'];
        $recent_file = $_POST['recent_file'];
        $teach_id = $_POST['teach_id'];

        unlink('../../uploads/audio/'.$recent_file);

        $up_date = date('Y-m-d');
        $up_time = date('h:i:s A');

        $upload_time = date('Y-m-d h:i:s A');

        $file = $_FILES["upload_file1"]["name"];

          $tmp_file = $_FILES['upload_file1']['tmp_name']; //ASSIGN DATA TO VARIABLE
          $ext = pathinfo($_FILES["upload_file1"]["name"], PATHINFO_EXTENSION); //GET EXTENTION ON IMAGE

          
          $base = basename($file,".".$ext);

          $sql001 = mysqli_query($conn,"SELECT * FROM uploads WHERE FILE_NAME LIKE '%".$base."%'");
          $check = mysqli_num_rows($sql001);

          if($check > 0)
          {
              $p1 = basename($file,".".$ext);
              $post_file = $p1."(".$check.").".$ext;
          }else
          if($check == '0')
          {
              $post_file = $file;
          }


          move_uploaded_file($tmp_file,'../../uploads/audio/'.$post_file); //SAVE LOCATION


          $s = mysqli_query($conn,"SELECT * FROM `uploads` WHERE `FILE_NAME` = '$post_file' AND `CLASS_ID` = '$select_class'");
        if(mysqli_num_rows($s)>0)
        {
          
            $update001 = mysqli_query($conn,"UPDATE `uploads` SET `TITLE`='$title',`DESCRIPTION`= '$descip',`UPL_DATE`='$up_date',`UPL_TIME`='$up_time',`TYPE`='Audio',`CLASS_ID`='$select_class',`YOUTUBE_LINK`='0',`UPLOAD_TIME`='$upload_time',`FILE_NAME`='$post_file' WHERE `UPLOAD_ID` = '$upload_id'");

        }else
        if(mysqli_num_rows($s)=='0')
        {
            mysqli_query($conn,"DELETE FROM uploads_clicked WHERE UPLOAD_ID = '$upload_id'");

            $update001 = mysqli_query($conn,"UPDATE `uploads` SET `TITLE`='$title',`DESCRIPTION`= '$descip',`UPL_DATE`='$up_date',`UPL_TIME`='$up_time',`TYPE`='Audio',`CLASS_ID`='$select_class',`YOUTUBE_LINK`='0',`UPLOAD_TIME`='$upload_time',`FILE_NAME`='$post_file' WHERE `UPLOAD_ID` = '$upload_id'");
        }

       header('location:../../new_teacher/view_upload.php');
        
    }else
    if(isset($_POST['edit_document_btn']))
    {
        $upload_id = $_POST['edit_document_btn'];
        $title = ucwords($_POST['title']);
        $descip = $_POST['descip'];
        $select_class = $_POST['clz'];
        $subject = $_POST['select_subject'];
        $type = $_POST['type'];
        $recent_file = $_POST['recent_file'];
        $teach_id = $_POST['teach_id'];

        unlink('../../uploads/document/'.$recent_file);

        $up_date = date('Y-m-d');
        $up_time = date('h:i:s A');

        $upload_time = date('Y-m-d h:i:s A');

        $file = $_FILES["upload_file1"]["name"];

          $tmp_file = $_FILES['upload_file1']['tmp_name']; //ASSIGN DATA TO VARIABLE
          $ext = pathinfo($_FILES["upload_file1"]["name"], PATHINFO_EXTENSION); //GET EXTENTION ON IMAGE

          
          $base = basename($file,".".$ext);

          $sql001 = mysqli_query($conn,"SELECT * FROM uploads WHERE FILE_NAME LIKE '%".$base."%'");
          $check = mysqli_num_rows($sql001);

          if($check > 0)
          {
              $p1 = basename($file,".".$ext);
              $post_file = $p1."(".$check.").".$ext;
          }else
          if($check == '0')
          {
              $post_file = $file;
          }


          move_uploaded_file($tmp_file,'../../uploads/document/'.$post_file); //SAVE LOCATION

          $s = mysqli_query($conn,"SELECT * FROM `uploads` WHERE `FILE_NAME` = '$post_file' AND `CLASS_ID` = '$select_class'");
        if(mysqli_num_rows($s)>0)
        {
          
            $update001 = mysqli_query($conn,"UPDATE `uploads` SET `TITLE`='$title',`DESCRIPTION`= '$descip',`UPL_DATE`='$up_date',`UPL_TIME`='$up_time',`TYPE`='Document',`CLASS_ID`='$select_class',`YOUTUBE_LINK`='0',`UPLOAD_TIME`='$upload_time',`FILE_NAME`='$post_file' WHERE `UPLOAD_ID` = '$upload_id'");

        }else
        if(mysqli_num_rows($s)=='0')
        {
            mysqli_query($conn,"DELETE FROM uploads_clicked WHERE UPLOAD_ID = '$upload_id'");

            $update001 = mysqli_query($conn,"UPDATE `uploads` SET `TITLE`='$title',`DESCRIPTION`= '$descip',`UPL_DATE`='$up_date',`UPL_TIME`='$up_time',`TYPE`='Document',`CLASS_ID`='$select_class',`YOUTUBE_LINK`='0',`UPLOAD_TIME`='$upload_time',`FILE_NAME`='$post_file' WHERE `UPLOAD_ID` = '$upload_id'");
        }

       header('location:../../new_teacher/view_upload.php');
        
    }else
    if(isset($_POST['update_zoom_link']))
    {

        $class_id = $_POST['update_zoom_link'];
        $link = $_POST['zoom_link'];

        if($link == '')
        {
          $link = '0';
        }

        mysqli_query($conn,"DELETE FROM link_clicked WHERE CLASS_ID = '$class_id'");
        $sql = mysqli_query($conn,"UPDATE `classes` SET `ZOOM_LINK`='$link' WHERE `CLASS_ID` = '$class_id'");

        header('location:../../new_teacher/setup_link.php');

      }else
    if(isset($_POST['update_google_link']))
    {

        $class_id = $_POST['update_google_link'];
        $link = $_POST['google_link'];

        if($link == '')
        {
          $link = '0';
        }
        mysqli_query($conn,"DELETE FROM link_clicked WHERE CLASS_ID = '$class_id'");

        $sql = mysqli_query($conn,"UPDATE `classes` SET `GOOGLE_LINK`='$link' WHERE `CLASS_ID` = '$class_id'");

        header('location:../../new_teacher/setup_link.php');

      }else
    if(isset($_POST['update_jitsi_link']))
    {

        $class_id = $_POST['update_jitsi_link'];
        $link = $_POST['jitsi_link'];

        if($link == '')
        {
          $link = '0';
        }
        mysqli_query($conn,"DELETE FROM link_clicked WHERE CLASS_ID = '$class_id'");

        $sql = mysqli_query($conn,"UPDATE `classes` SET `JITSI_LINK`='$link' WHERE `CLASS_ID` = '$class_id'");

        header('location:../../new_teacher/setup_link.php');

      }else
    if(isset($_POST['update_ms_link']))
    {

        $class_id = $_POST['update_ms_link'];
        $link = $_POST['ms_link'];

        if($link == '')
        {
          $link = '0';
        }
        mysqli_query($conn,"DELETE FROM link_clicked WHERE CLASS_ID = '$class_id'");

        $sql = mysqli_query($conn,"UPDATE `classes` SET `MS_LINK`='$link' WHERE `CLASS_ID` = '$class_id'");

        header('location:../../new_teacher/setup_link.php');

      }else
      if(isset($_POST['update_wa_link']))
      {

          $class_id = $_POST['update_wa_link'];
          $link = $_POST['wa_link'];

          if($link == '')
          {
            $link = '0';
          }
          mysqli_query($conn,"DELETE FROM link_clicked WHERE CLASS_ID = '$class_id'");

          $sql = mysqli_query($conn,"UPDATE `classes` SET `WHATSAPP_LINK`='$link' WHERE `CLASS_ID` = '$class_id'");

          header('location:../../new_teacher/setup_link.php');

        }else
        if(isset($_POST['update_tg_link']))
        {

          $class_id = $_POST['update_tg_link'];
          $link = $_POST['tg_link'];
          $teach_id = $_POST['teach_id'];

          if($link == '')
          {
            $link = '0';
          }

          $sql = mysqli_query($conn,"UPDATE `classes` SET `TELEGRAM_LINK`='$link' WHERE `CLASS_ID` = '$class_id'");


          header('location:../../new_teacher/setup_link.php');
        }else
        if(isset($_POST['edit_clz']))
        {
          $class_id = $_POST['edit_clz'];
          $subject_id = $_POST['subject'];
          $level_id = $_POST['clz'];
          $clz_name = ucwords($_POST['clz_name']);
          $fees = $_POST['clz_fees'];
          $start = $_POST['start'];
          $end = $_POST['end'];
          $weekday = $_POST['weekday'];
          $teach_id = $_POST['teach_id'];

          $update = mysqli_query($conn,"UPDATE `classes` SET `CLASS_NAME`= '$clz_name',`FEES`= '$fees',`START_TIME`= '$start',`END_TIME`='$end',`SUB_ID`='$subject_id',`DAY`= '$weekday' WHERE `CLASS_ID`= '$class_id'");

          header('location:../../new_teacher/new_class.php?teach_id='.$teach_id.'');

    }else
    if(isset($_POST['dashboard_update_zoom_link']))
    {

        $class_id = $_POST['dashboard_update_zoom_link'];
        $link = $_POST['zoom_link'];

        if($link == '')
        {
          $link = '0';
        }

        mysqli_query($conn,"DELETE FROM link_clicked WHERE CLASS_ID = '$class_id'");
        $sql = mysqli_query($conn,"UPDATE `classes` SET `ZOOM_LINK`='$link' WHERE `CLASS_ID` = '$class_id'");


        header('location:../../new_teacher/dashboard.php');
      }else
    if(isset($_POST['dashboard_update_google_link']))
    {

        $class_id = $_POST['dashboard_update_google_link'];
        $link = $_POST['google_link'];

        if($link == '')
        {
          $link = '0';
        }
        mysqli_query($conn,"DELETE FROM link_clicked WHERE CLASS_ID = '$class_id'");

        $sql = mysqli_query($conn,"UPDATE `classes` SET `GOOGLE_LINK`='$link' WHERE `CLASS_ID` = '$class_id'");


          header('location:../../new_teacher/dashboard.php');
      }else
    if(isset($_POST['dashboard_update_jitsi_link']))
    {

        $class_id = $_POST['dashboard_update_jitsi_link'];
        $link = $_POST['jitsi_link'];

        if($link == '')
        {
          $link = '0';
        }
        mysqli_query($conn,"DELETE FROM link_clicked WHERE CLASS_ID = '$class_id'");

        $sql = mysqli_query($conn,"UPDATE `classes` SET `JITSI_LINK`='$link' WHERE `CLASS_ID` = '$class_id'");


          header('location:../../new_teacher/dashboard.php');
      }else
    if(isset($_POST['dashboard_update_ms_link']))
    {

        $class_id = $_POST['dashboard_update_ms_link'];
        $link = $_POST['ms_link'];

        if($link == '')
        {
          $link = '0';
        }
        mysqli_query($conn,"DELETE FROM link_clicked WHERE CLASS_ID = '$class_id'");

        $sql = mysqli_query($conn,"UPDATE `classes` SET `MS_LINK`='$link' WHERE `CLASS_ID` = '$class_id'");


        header('location:../../new_teacher/dashboard.php');
        
      }else
      if(isset($_POST['update_paper']))
      {

        $paper_name = $_POST['paper_name'];
        $paper_id = $_POST['update_paper'];
        $exam_key = $_POST['exam_key'];
        $class_id = $_POST['class_id'];
        $teacher_id = $_POST['teacher_id'];
        $hour = $_POST['hour'];
        $minute = $_POST['minute'];
        $start_date = $_POST['start_date'];
        $end_date = $_POST['end_date'];

        $exam_type = '1';

        if($exam_type == '1')
        {
          $mark = $_POST['mark'];
        }else
        if($exam_type == '2')
        {
          $mark = '0';
        }

        if($hour == '')
        {
          $hour = '0';
        }else
        if($minute == '')
        {
          $minute = '0';
        }


        $max_hour = $_POST['max_hour'];
        $max_minu = $_POST['max_minu'];

        $recent_file = $_POST['recent_file'];

        $str1 = strtotime($start_date);
        $strdate = date('Y-m-d',$str1);
        $strtime = date('H:i:s',$str1);

        $end1 = strtotime($end_date);
        $enddate = date('Y-m-d',$end1);
        $endtime = date('H:i:s',$end1);

        $start_date = date('Y-m-d H:i:s',$str1);
        $end_date = date('Y-m-d H:i:s',$end1);

        if(!empty($_FILES['upload_file']['name']))
          {
            unlink('../images/paper_document/'.$recent_file.'');
            $file = $_FILES['upload_file']['name'];
            $tmp_file = $_FILES['upload_file']['tmp_name']; //ASSIGN DATA TO VARIABLE
            $ext = pathinfo($_FILES["upload_file"]["name"], PATHINFO_EXTENSION); //GET EXTENTION ON IMAGE
            $rand = hexdec(uniqid()); //NAME CONVERT TO ENCRYPT
            $post_file = $rand.".".$ext; //ADD IMAGE NAME TO ENCRYPTED NAME
            move_uploaded_file($tmp_file,"../images/paper_document/".$post_file); //SAVE LOCATION
          }else
          if(empty($_FILES['upload_file']['name']))
          {
            $post_file = $recent_file;
          }



        if($max_hour == '0' && $max_minu == '0')
        {
          $last_attend = $now;

        }else
        if($max_hour == '0' && $max_minu !== '0' || $max_hour !== '0' && $max_minu == '0' || $max_hour !== '0' && $max_minu !== '0')
        {
          $date = new DateTime($end_date);
          $date->modify('-'.$max_hour.' hour'.''.'-'.$max_minu.' minute');
          $last_attend = $date->format("Y-m-d H:i:s");

          mysqli_query($conn,"UPDATE `paper_master` SET `LAST_ATTENT_TIME`= '$last_attend' WHERE `PAPER_ID`= '$paper_id'");
        }

            
          

          $current_time = date('Y-m-d h:i:s A');
          $current_date = date('Y-m-d');

          //MCQ Paper = 1
          //Structured Paper = 2

          $update001 = mysqli_query($conn,"UPDATE `paper_master` SET `PAPER_NAME`= '$paper_name',`EXAM_KEY`= '$exam_key',`ANSWER_MARKS`= '$mark',`PDF_NAME`= '$post_file',`UPLOAD_BY`= '$teacher_id',`TIME_DURATION_HOUR`= '$hour',`TIME_DURATION_MIN`= '$minute',`FINISH_DATE`='$enddate',`FINISH_TIME`= '$endtime',`CLASS_ID`= '$class_id',`START_DATE`= '$strdate',`START_TIME`= '$strtime',`FINISH_D_T`='$end_date',`START_D_T`= '$start_date',`MAX_HOUR`= '$max_hour',`MAX_MINUTE`= '$max_minu',`PAPER_TYPE` = '$exam_type' WHERE `PAPER_ID`= '$paper_id'");

          header('location:../../new_teacher/exam.php');


      }else
      if(isset($_POST['update_st_paper']))
      {


        $paper_name = $_POST['paper_name'];
        $paper_id = $_POST['update_st_paper'];
        $exam_key = $_POST['exam_key'];
        $class_id = $_POST['class_id'];
        $teacher_id = $_POST['teacher_id'];
        $hour = $_POST['hour'];
        $minute = $_POST['minute'];
        $start_date = $_POST['start_date'];
        $end_date = $_POST['end_date'];

        $exam_type = '2';

        if($exam_type == '1')
        {
          $mark = $_POST['mark'];
        }else
        if($exam_type == '2')
        {
          $mark = '0';
        }

        if($hour == '')
        {
          $hour = '0';
        }else
        if($minute == '')
        {
          $minute = '0';
        }


        $max_hour = $_POST['max_hour'];
        $max_minu = $_POST['max_minu'];

        $recent_file = $_POST['recent_file'];

        $str1 = strtotime($start_date);
        $strdate = date('Y-m-d',$str1);
        $strtime = date('H:i:s',$str1);

        $end1 = strtotime($end_date);
        $enddate = date('Y-m-d',$end1);
        $endtime = date('H:i:s',$end1);

        $start_date = date('Y-m-d H:i:s',$str1);
        $end_date = date('Y-m-d H:i:s',$end1);

        if(!empty($_FILES['upload_file']['name']))
          {

            unlink('../images/paper_document/'.$recent_file.'');
            $file = $_FILES['upload_file']['name'];
            $tmp_file = $_FILES['upload_file']['tmp_name']; //ASSIGN DATA TO VARIABLE
            $ext = pathinfo($_FILES["upload_file"]["name"], PATHINFO_EXTENSION); //GET EXTENTION ON IMAGE
            $rand = hexdec(uniqid()); //NAME CONVERT TO ENCRYPT
            $post_file = $rand.".".$ext; //ADD IMAGE NAME TO ENCRYPTED NAME
            move_uploaded_file($tmp_file,"../images/str_document/".$post_file); //SAVE LOCATION

          }else
          if(empty($_FILES['upload_file']['name']))
          {
            $post_file = $recent_file;
          }
          
          if($max_hour == '0' && $max_minu == '0')
          {
            $last_attend = $end_date;
            
          }else
          if($max_hour == '0' && $max_minu !== '0' || $max_hour !== '0' && $max_minu == '0' || $max_hour !== '0' && $max_minu !== '0')
          {
            $date = new DateTime($end_date);
            $date->modify('-'.$max_hour.' hour'.''.'-'.$max_minu.' minute');
            $last_attend = $date->format("Y-m-d H:i:s");

            mysqli_query($conn,"UPDATE `paper_master` SET `LAST_ATTENT_TIME`= '$last_attend' WHERE `PAPER_ID`= '$paper_id'");
          }

          $current_time = date('Y-m-d h:i:s A');
          $current_date = date('Y-m-d');

          //MCQ Paper = 1
          //Structured Paper = 2

          $update001 = mysqli_query($conn,"UPDATE `paper_master` SET `PAPER_NAME`= '$paper_name',`EXAM_KEY`= '$exam_key',`ANSWER_MARKS`= '0',`PDF_NAME`= '$post_file',`UPLOAD_BY`= '$teacher_id',`TIME_DURATION_HOUR`= '$hour',`TIME_DURATION_MIN`= '$minute',`FINISH_DATE`='$enddate',`FINISH_TIME`= '$endtime',`CLASS_ID`= '$class_id',`START_DATE`= '$strdate',`START_TIME`= '$strtime',`FINISH_D_T`='$end_date',`START_D_T`= '$start_date',`MAX_HOUR`= '$max_hour',`MAX_MINUTE`= '$max_minu',`PAPER_TYPE` = '$exam_type' WHERE `PAPER_ID`= '$paper_id'");

          header('location:=../../new_teacher/exam.php');


      }else
      if(isset($_POST['edit_subject']))
      {
          $subject_id = $_POST['edit_subject'];

          $level_id = $_POST['level_id'];
          $recent_file = $_POST['recent_file'];

          $subject = ucwords($_POST['subject']);
          $level_id = $_POST['level_id'];
          $super_subject = $_POST['super_subject'];

          if(!empty($_FILES['picture']['name']))
          {
             if($recent_file !== 'subject.png')
            {
              unlink('../../admin/images/subject_image/'.$recent_file);
            }
            
            $file = $_FILES['picture']['name'];

            $tmp_file = $_FILES['picture']['tmp_name']; //ASSIGN DATA TO VARIABLE
            $ext = pathinfo($_FILES["picture"]["name"], PATHINFO_EXTENSION); //GET EXTENTION ON IMAGE
            $rand = hexdec(uniqid()); //NAME CONVERT TO ENCRYPT
            $post_file = $rand.".".$ext; //ADD IMAGE NAME TO ENCRYPTED NAME
            move_uploaded_file($tmp_file,"../../admin/images/subject_image/".$post_file); //SAVE LOCATION


            $insert001 = mysqli_query($conn,"UPDATE `subject` SET `SUBJECT_NAME`='$subject',`LEVEL_ID`='$level_id',`PICTURE`='$post_file',`SH_LEVEL`='$super_subject' WHERE `SUB_ID` = '$subject_id'");
          }else
          if(empty($_FILES['picture']['name']))
          {
            if($recent_file !== 'subject.png')
            {
              unlink('../../admin/images/subject_image/'.$recent_file);
            }

            $insert001 = mysqli_query($conn,"UPDATE `subject` SET `SUBJECT_NAME`='$subject',`LEVEL_ID`='$level_id',`PICTURE`='0',`SH_LEVEL`='$super_subject' WHERE `SUB_ID` = '$subject_id'");
          }

         header('location:../../new_teacher/add_subject.php');

      }else
      if(isset($_POST['semi_update']))
      {
        $semi_id = $_POST['semi_update'];
        $semi_name = $_POST['semi_name'];
        $semi_code = $_POST['semi_code'];
        $semi_date = $_POST['semi_date'];
        $semi_start = $_POST['semi_start'];
        $semi_end = $_POST['semi_end'];
        $semi_teacher = $_POST['semi_teacher'];
        $semi_link = $_POST['semi_link'];

        $page = $_POST['page'];

        $str = strtotime($semi_date);
        $semi_day = date('l',$str);

        $add_date = date('Y-m-d');
        $add_time = date('Y-m-d h:i:s A');

        $sql001 = mysqli_query($conn,"UPDATE `seminar_master` SET `SEM_NAME`='$semi_name',`TEACH_ID`='$semi_teacher',`SEMINAR_LINK`='$semi_link',`DUE_DATE`='$semi_date',`START_TIME`='$semi_start',`END_TIME`='$semi_end',`DAY`='$semi_day' WHERE `SEMI_ID`='$semi_id'");

        header('location:../../new_teacher/seminar.php?teach_id='.$semi_teacher.'&&page='.$page.'');


      }else
      if(isset($_POST['update_bank_data']))
      {
        $holder = ucwords($_POST['holder_name']);
        $ac_no = $_POST['ac_no'];
        $bank_name = strtoupper($_POST['bank_name']);
        $branch = ucfirst($_POST['branch']);

        $teach_id = $_POST['teacher_id'];
        $tbd_id = $_POST['tbd_id'];

        mysqli_query($conn,"UPDATE `teacher_bank_details` SET `HOLDER_NAME`='$holder',`BANK_NAME`='$bank_name',`ACCOUNT_NO`='$ac_no',`BRANCH_NAME`='$branch',`TEACH_ID`='$teach_id' WHERE `T_B_D_ID`= '$tbd_id'");

      
        header('location:../../new_teacher/profile_setting.php');


      }else
      if(isset($_POST['edit_record_btn']))
      {
          $upload_id = $_POST['edit_record_btn'];
          $title = ucwords($_POST['title']);
          $descip = $_POST['descip'];
          $select_class = $_POST['clz'];
          $subject = $_POST['select_subject'];
          $type = $_POST['type'];
          $teach_id = $_POST['teach_id'];

          $current_file = $_POST['current_file'];

          $up_date = date('Y-m-d');
          $up_time = date('h:i:s A');

          if(!empty($_FILES['upload_file']['name']))
          {
            $file = $_FILES['upload_file']['name'];

            $tmp_file = $_FILES['upload_file']['tmp_name']; //ASSIGN DATA TO VARIABLE
            $ext = pathinfo($_FILES["upload_file"]["name"], PATHINFO_EXTENSION); //GET EXTENTION ON IMAGE
            $rand = hexdec(uniqid()); //NAME CONVERT TO ENCRYPT
            $post_file = $rand.".".$ext; //ADD IMAGE NAME TO ENCRYPTED NAME
            move_uploaded_file($tmp_file,"../../uploads/recording/".$post_file); //SAVE LOCATION


            $insert001 = mysqli_query($conn,"UPDATE `uploads` SET `TITLE`='$title',`DESCRIPTION`= '$descip',`UPL_DATE`='$up_date',`UPL_TIME`='$up_time',`TYPE`='Record',`CLASS_ID`='$select_class',`YOUTUBE_LINK`='0',`EXTENSION`='$ext',`FILE_NAME`='$post_file' WHERE `UPLOAD_ID` = '$upload_id'");
          }else
          if(empty($_FILES['upload_file']['name']))
          {
            $record_link = $_POST['record_link'];

            unlink("../../uploads/recording/".$current_file); //SAVE LOCATION
            
            $insert001 = mysqli_query($conn,"UPDATE `uploads` SET `TITLE`='$title',`DESCRIPTION`= '$descip',`UPL_DATE`='$up_date',`UPL_TIME`='$up_time',`TYPE`='Record',`CLASS_ID`='$select_class',`YOUTUBE_LINK`='$record_link',`FILE_NAME`='0',`EXTENSION`='0' WHERE `UPLOAD_ID` = '$upload_id'");
          }
          
         header('location:../../new_teacher/view_upload.php?teach_id='.$teach_id.' ');
          
      }else
      if(isset($_POST['update_mcq_answer']))
      {
        $paper_id = $_POST['update_mcq_answer'];
        $mcq_tr_id = $_POST['mcq_tr_id'];
        $answer = $_POST['answer'];
        $mark = $_POST['mark'];
        $question = $_POST['question_no'];


        $sql003 = mysqli_query($conn,"SELECT * FROM `paper_master` WHERE `PAPER_ID` = '$paper_id'"); 
       while($row003 = mysqli_fetch_assoc($sql003))
       {
          $total_question = $row003['NO_OF_QUESTIONS']; //Exam key
         
          $answer_mark = $row003['ANSWER_MARKS'];
       }

          $current_time = date('Y-m-d h:i:s A');
          $current_date = date('Y-m-d');

          for($i=0;$i<$total_question;$i++)
          {
            $update001 = mysqli_query($conn,"UPDATE `mcq_trans` SET `CORRECT_ANSWER`= '$answer[$i]',`MARKS_ANSWER`='$mark[$i]' WHERE `MCQ_TRAN_ID` = '$mcq_tr_id[$i]'");


            $update002 = mysqli_query($conn,"UPDATE `student_mcq_transaction` SET `CORRECT_ANSWER`= '$answer[$i]',`MARKS`='$mark[$i]' WHERE `PAPER_ID` = '$paper_id' AND `QUESTION` = '$question[$i]'");
          }

          $sql0031 = mysqli_query($conn,"SELECT * FROM `student_mcq_master` WHERE `PAPER_ID` = '$paper_id'"); 
          while($row0031 = mysqli_fetch_assoc($sql0031))
          {
            $total_marks001 = $row0031['TOTAL_MARKS'];
            $correct001 = $row0031['CORRECT_ANSWERS'];
            $wrong001 = $row0031['WRONG_ANSWER'];
            $stu_mcq_master_id = $row0031['STU_MCQ_MASTER_ID'];
            $stu_id = $row0031['STU_ID'];


            $new_correct_marks = 0;
            $new_wrong_marks = 0;

            $new_correct_count = 0;
            $new_wrong_count = 0;
            $total_marks = 0;


            $sql0032 = mysqli_query($conn,"SELECT * FROM `student_mcq_transaction` WHERE `PAPER_ID` = '$paper_id' AND `STU_ID` = '$stu_id'"); 
             while($row0032 = mysqli_fetch_assoc($sql0032))
             {
                $student_answer = $row0032['ANSWER'];
                $orginal_answer = $row0032['CORRECT_ANSWER'];
                $real_marks = $row0032['MARKS'];

                if($student_answer == $orginal_answer)
                {
                  $new_correct_marks = $new_correct_marks+$real_marks;
                  $new_correct_count = $new_correct_count+1;
                }else
                if($student_answer !== $orginal_answer)
                {
                  $new_wrong_count = $new_wrong_count+1;
                }

             }

            $update003 = mysqli_query($conn,"UPDATE `student_mcq_master` SET `CORRECT_ANSWERS`= '$new_correct_count',`WRONG_ANSWER`='$new_wrong_count',`TOTAL_MARKS`='$new_correct_marks' WHERE `PAPER_ID` = '$paper_id' AND `STU_ID` = '$stu_id'");

            $update004 = mysqli_query($conn,"UPDATE `exam_result` SET `MCQ_MARK`='$new_correct_marks' WHERE `PAPER_ID` = '$paper_id' AND `STU_ID` = '$stu_id'");

            $sql0033 = mysqli_query($conn,"SELECT * FROM `exam_result` WHERE `PAPER_ID` = '$paper_id' AND `STU_ID` = '$stu_id'"); 
             while($row0033 = mysqli_fetch_assoc($sql0033))
             {
                $total_exam_mcq_marks = $row0033['MCQ_MARK'];
                $total_exam_se_marks = $row0033['SE_MARK'];

                $total_marks = ($total_exam_mcq_marks/2)+($total_exam_se_marks/20);


             }

            $update004 = mysqli_query($conn,"UPDATE `exam_result` SET `TOTAL_MARK`='$total_marks' WHERE `PAPER_ID` = '$paper_id' AND `STU_ID` = '$stu_id'");


            }
          

          header('location:../../new_teacher/edit_mcq_answer.php?paper_id='.$paper_id.'');


      }else
      if(isset($_POST['update_google_form']))
      {
        $paper_name = ucwords($_POST['paper_name']);
        $class_id = $_POST['class_id'];
        $form_link = $_POST['google_form_link'];
        $teacher_id = $_POST['teacher_id'];
        $hour = $_POST['hour'];
        $minute = $_POST['minute'];
        $start_time = $_POST['start_date'];
        $end_time = $_POST['end_date'];

        $page = $_POST['page'];

        $gf_master_id = $_POST['update_google_form'];

        $max_hour = $_POST['max_hour'];
        $max_minu = $_POST['max_minu'];

        $start_date = date('Y-m-d',strtotime($start_time));

        $end_date = date('Y-m-d',strtotime($end_time));

        $current_time = date('Y-m-d h:i:s A');
        $current_date = date('Y-m-d');

        $insert0001 = mysqli_query($conn,"UPDATE `g_f_master` SET `ADD_DT` = '$current_time',`PAPER_NAME`='$paper_name',`FORM_LINK`='$form_link',`START_DATE`='$start_date',`START_TIME`='$start_time',`END_DATE`='$end_date',`HOURS`='$max_hour',`MINUTE`='$max_minu',`END_TIME`='$end_time',`TEACH_ID`='$teacher_id' WHERE `GF_MASTER_ID`='$gf_master_id'"); //Insert Answers


        header('location:../../new_teacher/google_form.php?page='.$page.'');

      }else
      if(isset($_POST['update_google_form_classes']))
      {
        $gf_master_id = $_POST['update_google_form_classes'];
        $page = $_POST['page'];
        $class_id = $_POST['class_id'];
        $teacher_id = $_POST['teacher_id'];

        $class_amount = sizeof($class_id);

        for($j=0;$j<$class_amount;$j++)
        {

          $sql = mysqli_query($conn,"SELECT * FROM `g_f_tranasaction` WHERE `CLASS_ID` = '$class_id[$j]' AND `GF_MASTER_ID` = '$gf_master_id'");

          if(mysqli_num_rows($sql) == '0')
          {

            $insert001 = mysqli_query($conn,"INSERT INTO `g_f_tranasaction`(`GF_MASTER_ID`, `CLASS_ID`) VALUES ('$gf_master_id','$class_id[$j]')");

          }else
          if(mysqli_num_rows($sql) > 0)
          {

              mysqli_query($conn,"DELETE FROM `g_f_tranasaction` WHERE `GF_MASTER_ID` = '$gf_master_id' AND `CLASS_ID` != '$class_id[$j]'");


          }

        }


        
        
        header('location:../../new_teacher/google_form.php?page='.$page.'');

      }else
      if(isset($_POST['update_data']))
      {
        $stu_id = $_POST['update_data'];
        $page = $_POST['page'];
        $fnm = ucwords($_POST['fnm']);
        $lnm = ucwords($_POST['lnm']);
        $dob = date("Y-m-d", strtotime($_POST['dob']));
        $gen = $_POST['gender'];
        $email = $_POST['email'];
        $tp = $_POST['number'];
        $address = $_POST['address'];
        $new = $_POST['new']; // 0 = Already registerd,1 = Not Registered


          $today = date('Y-m-d h:i:s A');
          

        $insert001 = mysqli_query($conn,"UPDATE `student_details` SET `F_NAME`= '$fnm',`L_NAME`= '$lnm',`DOB`= '$dob',`EMAIL`= '$email',`ADDRESS`= '$address',`TP`= '$tp',`NEW`= '$new',`GENDER`= '$gen' WHERE `STU_ID`= '$stu_id'");

        $_SESSION['notify_id'] = $stu_id;

        header('location:../../new_teacher/student_approved.php?page='.$page.'');
        
      }else
      if(isset($_POST['update_mcq_answer']))
      {
        $paper_id = $_POST['update_mcq_answer'];
        $mcq_tr_id = $_POST['mcq_tr_id'];
        $answer = $_POST['answer'];
        $mark = $_POST['mark'];
        $question = $_POST['question_no'];


        $sql003 = mysqli_query($conn,"SELECT * FROM `paper_master` WHERE `PAPER_ID` = '$paper_id'"); 
       while($row003 = mysqli_fetch_assoc($sql003))
       {
          $total_question = $row003['NO_OF_QUESTIONS']; //Exam key
         
          $answer_mark = $row003['ANSWER_MARKS'];
       }

          $current_time = date('Y-m-d h:i:s A');
          $current_date = date('Y-m-d');

          for($i=0;$i<$total_question;$i++)
          {
            $update001 = mysqli_query($conn,"UPDATE `mcq_trans` SET `CORRECT_ANSWER`= '$answer[$i]',`MARKS_ANSWER`='$mark[$i]' WHERE `MCQ_TRAN_ID` = '$mcq_tr_id[$i]'");


            $update002 = mysqli_query($conn,"UPDATE `student_mcq_transaction` SET `CORRECT_ANSWER`= '$answer[$i]',`MARKS`='$mark[$i]' WHERE `PAPER_ID` = '$paper_id' AND `QUESTION` = '$question[$i]'");
          }

          $sql0031 = mysqli_query($conn,"SELECT * FROM `student_mcq_master` WHERE `PAPER_ID` = '$paper_id'"); 
          while($row0031 = mysqli_fetch_assoc($sql0031))
          {
            $total_marks001 = $row0031['TOTAL_MARKS'];
            $correct001 = $row0031['CORRECT_ANSWERS'];
            $wrong001 = $row0031['WRONG_ANSWER'];
            $stu_mcq_master_id = $row0031['STU_MCQ_MASTER_ID'];
            $stu_id = $row0031['STU_ID'];


            $new_correct_marks = 0;
            $new_wrong_marks = 0;

            $new_correct_count = 0;
            $new_wrong_count = 0;
            $total_marks = 0;


            $sql0032 = mysqli_query($conn,"SELECT * FROM `student_mcq_transaction` WHERE `PAPER_ID` = '$paper_id' AND `STU_ID` = '$stu_id'"); 
             while($row0032 = mysqli_fetch_assoc($sql0032))
             {
                $student_answer = $row0032['ANSWER'];
                $orginal_answer = $row0032['CORRECT_ANSWER'];
                $real_marks = $row0032['MARKS'];

                if($student_answer == $orginal_answer)
                {
                  $new_correct_marks = $new_correct_marks+$real_marks;
                  $new_correct_count = $new_correct_count+1;
                }else
                if($student_answer !== $orginal_answer)
                {
                  $new_wrong_count = $new_wrong_count+1;
                }

             }

            $update003 = mysqli_query($conn,"UPDATE `student_mcq_master` SET `CORRECT_ANSWERS`= '$new_correct_count',`WRONG_ANSWER`='$new_wrong_count',`TOTAL_MARKS`='$new_correct_marks' WHERE `PAPER_ID` = '$paper_id' AND `STU_ID` = '$stu_id'");

            $update004 = mysqli_query($conn,"UPDATE `exam_result` SET `MCQ_MARK`='$new_correct_marks' WHERE `PAPER_ID` = '$paper_id' AND `STU_ID` = '$stu_id'");

            $sql0033 = mysqli_query($conn,"SELECT * FROM `exam_result` WHERE `PAPER_ID` = '$paper_id' AND `STU_ID` = '$stu_id'"); 
             while($row0033 = mysqli_fetch_assoc($sql0033))
             {
                $total_exam_mcq_marks = $row0033['MCQ_MARK'];
                $total_exam_se_marks = $row0033['SE_MARK'];

                $total_marks = ($total_exam_mcq_marks/2)+($total_exam_se_marks/20);


             }

            $update004 = mysqli_query($conn,"UPDATE `exam_result` SET `TOTAL_MARK`='$total_marks' WHERE `PAPER_ID` = '$paper_id' AND `STU_ID` = '$stu_id'");


            }
          

          header('location:../../new_teacher/edit_mcq_answer.php?paper_id='.$paper_id.'');


      }else
      if(isset($_POST['check_safe_link']))
      {
        $class_id = $_POST['check_safe_link'];
        $status = $_POST['status'];

        if($status == '1')
        {
            mysqli_query($conn,"UPDATE `classes` SET `ACTIVE_SAFE_LINK`= '1' WHERE `CLASS_ID`= '$class_id'");

        }else
        if($status == '0')
        {
            mysqli_query($conn,"UPDATE `classes` SET `ACTIVE_SAFE_LINK`= '0' WHERE `CLASS_ID`= '$class_id'");

        }

        echo $status;

        $_SESSION['update_msg'] = '';


      }

 ?>