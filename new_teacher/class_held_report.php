<?php 
  session_start();
  include('../connect/connect.php');

  $start_date = $_POST['start_date'];
  $end_date = $_POST['end_date'];
  
  $teacher_id = $_SESSION['TEACH_ID'];

  $sql = mysqli_query($conn,"SELECT * FROM institute WHERE INS_ID = '1'");
  while($row = mysqli_fetch_assoc($sql))
  {
    $n = $row['INS_NAME'];
    $p = $row['PICTURE'];
    if($row['INS_MOBILE'] !== '0')
    {
      $mobile = $row['INS_MOBILE'];
    }else
    {
      $mobile = "";
    }

    if($row['INS_TP'] !== '0')
    {
      $tele = $row['INS_TP'];
    }else
    {
      $tele = "";
    }


    if(!empty($mobile) && !empty($tele))
    {
      $no = $mobile." / ".$tele;
    }

    $a = $row['INS_ADDRESS'];
  }header('Content-type: text/html; charset=UTF-8');


    $date = date('Y-m-d');
    $time = date('h:i:s A');

 ?>
<html>
<head>
<style type="text/css" media="print,screen">
.hideMe{
display:block;
}

.PrintClass {
display:block;

}
.NoPrintClass{
display:block;
}

</style>
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script type="text/javascript"
    src="http://jqueryjs.googlecode.com/files/jquery-1.3.1.min.js"> </script>
    <link href="https://fonts.googleapis.com/css?family=PT+Sans+Narrow&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<script type="text/javascript">

function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     document.body.innerHTML = originalContents;

     window.print();
    window.onafterprint = function() {
    };
     

}
function close_win() {
   window.close();
}
   </script>
</head>
  <style type="text/css">
  @media print {
  .print_btn {
    display: none;
  }
}
</style>
</head>


<body>
 <div class="col-md-12" id="printableArea" style="display:block;">

 <p style="text-align:center">
        <?php 

        if(!empty($mobile) && !empty($tele))
        {
          $no = $mobile."/".$tele;
        }else
        if(!empty($mobile) && empty($tele))
        {
          $no = $mobile;
        }else
        if(!empty($tele) && empty($mobile))
        {
          $no = $tele;
        }else
        {
          $no = '';
        }



        echo '<center><img src="../admin/images/institute/'.$p.'" style="width:80px;height:80px;margin-bottom:10px;"></center>';

        if($p == "0")
        {
          echo '<h3 style="text-align:center;text-transform:uppercase;">'.$n.'</h3><p style="text-align:center">'.$a.'</p><p style="text-align:center">'.$no.'</p><div style="border-top: 1px dashed black;padding:0px;"></div>';
        }else
        if($p !== "0")
        {
          echo '<h3 style="text-align:center;text-transform:uppercase;">'.$n.'</h3><p style="text-align:center">'.$a.'</p><p style="text-align:center">'.$no.'</p><div style="border-top: 1px dashed black;padding:0px;"></div>';
        }

        
        ?>
      </p>


        <div class="row">
          <div class="col-md-4">

            <h4 style="float: left;text-transform: uppercase;">Duration : <?php echo $start_date; ?> - <?php echo $end_date; ?>

            </h4>
          </div>
          <div class="col-md-4" style="text-align: center;padding-top: 15px;"><center><h1 style="text-align: center;margin-bottom: 20px;text-transform: uppercase;"><u>held class REPORT</u></h1></center>
          </div>
          <div class="col-md-4"></div>
        </div>
    
    <div style="border-top: 1px dashed black;margin-top: 10px;margin-bottom: 10px;"></div>
        <div class="table-responsive">
            
            <table class="table" style="font-size: 14px;">
              <thead>
                <th>Held Date | Time</th>
                <th>Class Name</th>
                <th>Teacher Name</th>
                <th>Status</th>
                <th class="text-right">Student Count</th>
              </thead>
            <tbody>
              <?php 
                $total_fees = 0;
                $count_stu = 0;


                $sql0060 = mysqli_query($conn,"SELECT * FROM `held_class` WHERE `HELD_DATE` BETWEEN '$start_date' AND '$end_date' AND `TEACH_ID` = '$teacher_id'");
                

                if(mysqli_num_rows($sql0060)>0)
                {
                    while($row0023 = mysqli_fetch_assoc($sql0060))
                    { 

                        $class_id = $row0023['CLASS_ID'];
                        $teach_id = $row0023['TEACH_ID'];
                        $st_count = $row0023['STUDENT_COUNT'];
                        $status = $row0023['STATUS'];
                        $held_date = $row0023['HELD_DATE'];

                        $str_date = strtotime($held_date);
                        $find_day = date('l',$str_date);

                        if($status == '0')
                        {
                          $label_status = '<h4 class="label label-success" style="font-size:12px;margin-top:10px;">Free Class</h4>';
                        }else
                        if($status == '1')
                        {
                          $label_status = '<h4 class="label label-danger" style="font-size:12px;margin-top:10px;">Paid Class</h4>';
                        }


                        $sql007 = mysqli_query($conn,"SELECT * FROM `classes` WHERE `CLASS_ID` = '$class_id'");
                        while($row007 = mysqli_fetch_assoc($sql007))
                        {
                          $class_start_time = $row007['START_TIME'];
                          $class_end_time = $row007['END_TIME'];
                          $class_name = $row007['CLASS_NAME'];
                          $sub_id = $row007['SUB_ID'];

                          $sql009 = mysqli_query($conn,"SELECT * FROM `subject` WHERE `SUB_ID` = '$sub_id'");
                          while($row009 = mysqli_fetch_assoc($sql009))
                          {
                            $subject_name = $row009['SUBJECT_NAME'];
                          }


                          $str = strtotime($class_start_time);
                          $class_start_time = date('h:i A',$str);

                          $str2 = strtotime($class_end_time);
                          $class_end_time = date('h:i A',$str2);

                          $time = $class_start_time." - ".$class_end_time;
                        }

                        $sql008 = mysqli_query($conn,"SELECT * FROM `teacher_details` WHERE `TEACH_ID` = '$teach_id'");
                        while($row008 = mysqli_fetch_assoc($sql008))
                        {
                          $teacher_name = $row008['POSITION'].". ".$row008['F_NAME']." ".$row008['L_NAME'];
                        }

                        echo '
                            <tr>
                              <td>'.$held_date.'
                                <label class="label label-success">'.$find_day.'</label>
                                <br><small>'.$time.'</small>
                              </td>
                              <td>'.$class_name.'<br><small>'.$subject_name.'</small></td>
                              <td>'.$teacher_name.'</td>
                              <td>'.$label_status.'</td>
                              <td style="text-align:right;font-size:15px;font-weight:600;">'.$st_count.'</td>
                            </tr>
                          ';

                          $count_stu = $count_stu+1;
                    }    
                  }else
                  if(mysqli_num_rows($sql0060) == '0')
                  {
                    echo '

                      <tr style="font-size: 14px;">
                       <th colspan="5" class="text-center"><span class="fa fa-warning"></span> Not Found Data!</th>
                     </tr>

                    ';

                  }
                  

               ?>

                </tbody>
              </table>
        </div>
      
</div>

<div class="col-md-12" style="position: fixed;top:10px;right: 10px;">

  <button class="btn btn-success btn-lg print_btn" onclick="printDiv('printableArea')" style="font-size: 20px;float: right;border-radius: 80px;padding: 10px 13px 10px 13px;margin-left: 10px;outline: none;"><span class="fa fa-print"></span></button>

  <button class="btn btn-danger btn-lg print_btn" onclick="close_win();" style="font-size: 20px;float: right;border-radius: 80px;padding: 10px 13px 10px 13px;outline: none;"><span class="fa fa-times"></span></button>


</div>
</body>

</html>



