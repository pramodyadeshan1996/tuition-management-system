

<?php
  $page = "Todo List";
  $folder_in = '3';

      include('header/header.php');

        $s04 = mysqli_query($conn,"SELECT * FROM admin_login WHERE ADMIN_ID = '$admin_id'");
        while($row4 = mysqli_fetch_assoc($s04))
        {
          $name = $row4['ADMIN_NAME'];
        }

 ?>

    <ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-bottom: 04%;margin-bottom: 0;">
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="Todo List"> Todo List</li>
    </ol>


      <div class="col-md-12 bg-white" style="border:1px solid #cccc;height: auto;">
      
      <div class="col-md-12" style="border-bottom: 1px solid #cccc;">
        <div class="row">

            <div class="col-md-9" style="margin-bottom: 10px;"><h2><a href="dashboard.php" data-toggle="tooltip" data-title="Back"><i class="pg-icon" style="font-size: 22px;">chevron_left</i></a> Todo List</h2></div>

        </div>
      </div>

          
          <div class=" container-fluid container-fixed-lg bg-white" style="margin-top: 0px;">
            <div class="card card-transparent">
              <div class="card-header ">

                  <form action="todo_list.php" method="POST">
                      <div class="row">
                        <div class="col-md-2">

                          <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#add_todo"><span class="fa fa-plus"></span>&nbsp;Add Todo List</button>

                        </div>
                        <div class="col-md-5"> </div>
                      <div class="col-md-3 mt-2">
                        <input list="student_search" id="search-table" name="search_student" class="form-control" placeholder="Search Register ID.." >

                        <datalist id="student_search">
                          <?php 

                            $sql002 = mysqli_query($conn,"SELECT * FROM `student_details`");
                            while($row002 = mysqli_fetch_assoc($sql002))
                            {
                              $name = $row002['F_NAME']." ".$row002['L_NAME'];
                              $student_id = $row002['STU_ID'];
                              $tp = $row002['TP'];

                              $sql003 = mysqli_query($conn,"SELECT * FROM `stu_login` WHERE `STU_ID` = '$student_id' AND `STATUS` = 'Active'");
                              while($row003 = mysqli_fetch_assoc($sql003))
                              {
                                
                                $register_id = $row003['REGISTER_ID'];
                              
                                echo '<option value="'.$register_id.'">'.$name.' | '.$tp.'</option>';
                              }

                              
                            }

                           ?>
                          
                        </datalist>
                      </div>
                      <div class="col-md-1 mt-2"> <button type="submit" class="btn btn-success btn-lg btn-block" name="search_student_btn" style="border-radius: 20px;"><i class="fa fa-search"></i> &nbsp; Search</button></div>

                      <div class="col-md-1 mt-2"> <button type="submit" class="btn btn-danger btn-lg btn-block" name="reset_student_btn" style="border-radius: 20px;"><i class="fa fa-refresh"></i> &nbsp; Reset</button></div>
                    </div>
                  </form>



                    <div class="clearfix"></div>
                </div>
                <div class="card-body">
                  <!--  style="height: 820px;overflow: auto;" -->
                <div class="table-responsive">
                <table class="table table-hover" id="tblFruits">
                  <thead>
                      <tr>
                        <th class="text-center" style="width: 1%;"><button class="btn btn-danger" onclick = "GetSelected()" style="border-radius: 80px;padding: 9px 10px 9px 10px;"><span class="fa fa-trash"></span></button></th>
                        <th style="width: 1%;">Add Time</th>
                        <th style="width: 1%;">Student Name</th>
                        <th style="width: 1%;" colspan="2">Comment</th>
                        <th style="width: 1%;text-align: center;">Action</th>
                      </tr>
                    </thead>
                    <tbody>

                      <?php 
                          /*------------------ Header Pagination --------------------------------*/
                        $record_per_page = 10;

                        $nxt_five_loop = 0;

                        $page = '';

                        if(isset($_GET["page"]))
                        {
                          $page = $_GET["page"];
                        }
                        else
                        {
                         $page = 1;
                        }

                        $start_from = ($page-1)*$record_per_page; //Current page no -1 X Per Page records

                        if(isset($_POST['search_student_btn']))
                        {
                          $student_id = $_POST['search_student'];


                          $sql001 = "SELECT * FROM `todo_list` WHERE  `REGISTER_ID` LIKE '%".$student_id."%' order by `TODO_ID` DESC LIMIT $start_from, $record_per_page";


                        }else
                        {
                          $sql001 = "SELECT * FROM `todo_list`  order by `TODO_ID` DESC LIMIT $start_from, $record_per_page";

                        }

                        
                          $page_query = "SELECT * FROM `todo_list`  ORDER BY `STU_ID` DESC";

                        /*------------------ Header Pagination --------------------------------*/

                        $j = 0;
                        
                        //LIMIT = 10-1 x 10 = 90 , 10";
                        $result = mysqli_query($conn, $sql001);
                        $page_result = mysqli_query($conn, $page_query);

                          $check = mysqli_num_rows($result);

                          if($check>0)
                          {
                          while($row001 = mysqli_fetch_assoc($result))
                          {
                            $stu_id = $row001['STU_ID'];
                            $add_date = $row001['ADD_DT'];
                            $comment = $row001['COMMENT'];

                            $todo_id = $row001['TODO_ID'];

                            $j=$j++;

                            $sql002 = mysqli_query($conn,"SELECT * FROM `student_details` WHERE `STU_ID` = '$stu_id'");
                            while($row002 = mysqli_fetch_assoc($sql002))
                            {
                              $name = $row002['F_NAME']." ".$row002['L_NAME'];
                              $fnm = $row002['F_NAME'];
                              $lnm = $row002['L_NAME'];
                              $tp1 = $row002['TP'];
                              $tp = str_replace(" ","",$tp1);
                              $dob = $row002['DOB'];
                              $gender = $row002['GENDER'];
                              $email = $row002['EMAIL'];
                              $address = $row002['ADDRESS'];
                              $new = $row002['NEW'];

                              $sql001 = mysqli_query($conn,"SELECT * FROM `stu_login` WHERE `STU_ID` = '$stu_id'");
                              while($row001 = mysqli_fetch_assoc($sql001))
                              {
                                $reg = $row001['REGISTER_ID'];
                              }
                              
                            }
                            if(empty($_SESSION['notify_id']) || $_SESSION['notify_id'] !== $stu_id)
                            {
                              $notify = '';
                            }else
                            if($_SESSION['notify_id'] == $stu_id)
                            {
                              $notify = 'style="background-color:#dfe6e9;color:black;"';
                              ?>  

                              <script type="text/javascript">
                                setTimeout(function(){ 

                                  for(i=0;i<6;i++)
                                  {
                                    document.getElementsByClassName('recolor<?php echo $stu_id; ?>')[i].style.backgroundColor  = 'white'; //Recolor

                                    document.getElementsByClassName('recolor<?php echo $stu_id; ?>')[i].style.color  = 'black'; //Recolor
                                  }
                                 
                                  }, 4000);
                                
                              </script>

                              <?php

                              unset($_SESSION['notify_id']);
                            }

                            echo '

                              <tr>
                                <td class="v-align-left text-center recolor'.$stu_id.'" '.$notify.'>

                                    <input type="checkbox" id="chkMango" name="multi_delete[]" style="zoom:1.3;" value="'.$todo_id.'">

                                </td>
                                <td class="v-align-left recolor'.$stu_id.'" '.$notify.'>'.$add_date.'</td>
                                <td class="v-align-left recolor'.$stu_id.'" '.$notify.'>'.$name.' <br> (<small>'.$reg.'</small>)</td>
                                <td class="v-align-left recolor'.$stu_id.'" '.$notify.' colspan="2">'.$comment.'</td>
                                 <td class="v-align-middle text-center recolor'.$stu_id.'" '.$notify.'>

                                <button data-toggle="modal" data-target="#edit_todo'.$stu_id.'" class="btn btn-success btn-xs btn-rounded mt-1 mr-1" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;" data-title="Approve" data-toggle="tooltip" id="edit1'.$stu_id.'"><i class="pg-icon">edit</i></button>
                                ' ?>
                                <a href="../admin/query/delete.php?delete_todo=<?php echo $todo_id; ?>&&page=<?php echo $page; ?>" onclick="return confirm('Are you sure delete?')" class="btn btn-danger mt-1 mr-1 btn-xs btn-rounded" style="padding:4px 4px;box-shadow:0px 0px 4px 3px #cccc;" data-title="Delete" data-toggle="tooltip"><i class="pg-icon">trash_alt</i></a>

                                <div id="edit_todo<?php echo $stu_id; ?>" class="modal fade" role="dialog">
                                  <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Update Todo List</h4>
                                      </div>
                                      <form action="../admin/query/update.php" method="POST">
                                      <div class="modal-body mt-2" style="text-align:left;">

                                        <label class="mt-2 text-left" style="text-align:left;">Student Name</label>
                                        <input list="student_list" id="search-table" name="student_name" class="form-control" placeholder="Search Register ID.." value="<?php echo $reg; ?>">

                                        <datalist id="student_list">
                                          <?php 

                                            $sql002 = mysqli_query($conn,"SELECT * FROM `student_details`");
                                            while($row002 = mysqli_fetch_assoc($sql002))
                                            {
                                              $name = $row002['F_NAME']." ".$row002['L_NAME'];
                                              $student_id = $row002['STU_ID'];
                                              $tp = $row002['TP'];

                                              $sql003 = mysqli_query($conn,"SELECT * FROM `stu_login` WHERE `STU_ID` = '$student_id' AND `STATUS` = 'Active'");
                                              while($row003 = mysqli_fetch_assoc($sql003))
                                              {
                                                
                                                $register_id = $row003['REGISTER_ID'];
                                              
                                                echo '<option value="'.$register_id.'">'.$name.' | '.$tp.'</option>';
                                              }

                                              
                                            }

                                           ?>
                                          
                                        </datalist>

                                        <label class="mt-2 text-left" style="text-align:left;">Comment</label>
                                        <textarea class="form-control" placeholder="Enter comment" name="comment" rows="6"><?php echo $comment; ?></textarea>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="submit" class="btn btn-success" name="update_todo" value="<?php echo $todo_id; ?>"><span class="fa fa-edit"></span>&nbsp;update</button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                      </div>
                                      </form>
                                    </div>

                                  </div>
                                </div>

                                <?php echo '

                                </td>
                              </tr>

                            ';
                        
                          }
                        }else
                        if($check == '0')
                        {
                          echo '<tr><td colspan="7" class="text-center text-danger"><span class="fa fa-warning"></span> Not Found Data</td></tr>';
                        } 
                      

                        

                        
                         ?>


                    </tbody>
                </table>
                </div>
              </div>

                <div class="col-md-12" style="width: 100%;overflow: auto;">
                  <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-8" class="php_pagination">
                      
                      <div class="col-md-12">
                    <br />
                    <?php
                    $total_records = mysqli_num_rows($page_result);
                    $total_pages = ceil($total_records/$record_per_page);
                    $start_loop = $page;
                    $difference = $total_pages - $page;
                    if($difference <= 5)
                    {
                     $start_loop = $total_pages - 5;
                    }else
                    if($difference <= 0)
                    {
                      $start_loop = '0';
                    }

                    echo '<ul class="pagination">';
                    $end_loop = $start_loop + 4;
                    $five_loop = $page-5;
                    if($five_loop <= 0)
                    {
                      $five_loop = 1;
                    }
                    if($page > 1)
                    {

                      echo '<li><a href="todo_list.php?page=1">First</a></li>';
                      echo '<li><a href="todo_list.php?page='.($page - 1).'"> < </a></li>';
                      echo '<li><a href="todo_list.php?page='.$five_loop.'"> <<< </a></li>';

                    }
                    if($start_loop !== '1')
                    {
                      $few_no = $start_loop - 1;
                    }else
                    if($start_loop > 0)
                    {
                      $few_no = 1;
                    }
                    
                    for($i=$few_no; $i<=$end_loop+1; $i++)
                    {     
                      if($i == $page)
                      {
                        echo '<li class="active"><a href="todo_list.php?page='.$i.'" style="font-weight:bold;background-color:#2980b9;color:white;">'.$i.'</a></li>';
                      }else
                      {
                        if($i == '0')
                        {
                          continue;
                        }else
                        if($i>0)
                        {
                          echo '<li><a href="todo_list.php?page='.$i.'" >'.$i.'</a></li>';
                        }
                        
                      }

                    }
                    if($page <= $end_loop)
                    {
                      
                      if($page == '1')
                      {
                        $nxt_five_loop = 5;
                      }else
                      if($page >= 5)
                      {
                        $nxt_five_loop = $page+5;

                        if($total_pages < $nxt_five_loop)
                        {
                          $nxt_five_loop = $total_pages;
                        }
                      }else
                      {
                        $nxt_five_loop = $page+5;
                      }

                        echo '<li><a href="todo_list.php?page='.($page + 1).'" > > </a></li>';
                        echo '<li><a href="todo_list.php?page='.$nxt_five_loop.'" > >>> </a></li>';
                        echo '<li><a href="todo_list.php?page='.$total_pages.'" >Last</a></li>';
                      
                    }
                    
                    echo '</ul>';
                    
                    ?>
                    </div>

                    </div>
                  </div>
                    
                </div>
            </div>
            </div>
            </div>
            </div>
            </div>
            </div>



<?php include('footer/footer.php'); ?>

<script type="text/javascript">
  function sortTable(table, order) {
    var asc   = order === 'asc',
        tbody = table.find('tbody');
    
    tbody.find('tr').sort(function(a, b) {
        if (asc) {
            return $('td:first', a).text().localeCompare($('td:first', b).text());
        } else {
            return $('td:first', b).text().localeCompare($('td:first', a).text());
        }
    }).appendTo(tbody);
}
</script>

<script type="text/javascript">
    function GetSelected() {
        //Create an Array.
        var selected = new Array();
 
        //Reference the Table.
        var tblFruits = document.getElementById("tblFruits");
 
        //Reference all the CheckBoxes in Table.
        var chks = tblFruits.getElementsByTagName("INPUT");
 
        // Loop and push the checked CheckBox value in Array.
        for (var i = 0; i < chks.length; i++) {
            if (chks[i].checked) 
            {
                var s = selected.push(chks[i].value);

            }
        }
 
        //Display the selected CheckBox values.
        if (selected.length > 0) {
          if(confirm('Are you sure delete?'))
          {

              $.ajax({
               type: "POST",
               data: {delete_multi_todo:selected},
               url: "../admin/query/delete.php",
               success: function(msg){
                 //alert(msg)
                 location.reload();
               }
            
              });
          }
        }else{
         
         alert("Please select todo list!")
        
        }
    };
</script>

<!-- Modal -->
                          <div id="add_todo" class="modal fade" role="dialog">
                            <div class="modal-dialog">

                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title">Create Todo List</h4>
                                </div>
                                <form action="../admin/query/insert.php" method="POST">
                                <div class="modal-body mt-2">

                                  <label class="mt-2">Student Name</label>
                                  <input list="student_list" id="search-table" name="student_name" class="form-control" placeholder="Search Register ID.." >

                                  <datalist id="student_list">
                                    <?php 

                                      $sql002 = mysqli_query($conn,"SELECT * FROM `student_details`");
                                      while($row002 = mysqli_fetch_assoc($sql002))
                                      {
                                        $name = $row002['F_NAME']." ".$row002['L_NAME'];
                                        $student_id = $row002['STU_ID'];
                                        $tp = $row002['TP'];

                                        $sql003 = mysqli_query($conn,"SELECT * FROM `stu_login` WHERE `STU_ID` = '$student_id' AND `STATUS` = 'Active'");
                                        while($row003 = mysqli_fetch_assoc($sql003))
                                        {
                                          
                                          $register_id = $row003['REGISTER_ID'];
                                        
                                          echo '<option value="'.$register_id.'">'.$name.' | '.$tp.'</option>';
                                        }

                                        
                                      }

                                     ?>
                                    
                                  </datalist>

                                  <label class="mt-2">Comment</label>
                                  <textarea class="form-control" placeholder="Enter comment" name="comment" rows="6"></textarea>
                                </div>
                                <div class="modal-footer">
                                  <button type="submit" class="btn btn-success" name="create_todo"><span class="fa fa-check-circle"></span>&nbsp;Submit</button>
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                                </form>
                              </div>

                            </div>
                          </div>