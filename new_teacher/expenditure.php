<?php
	$page = "Expenditure";
	$folder_in = '0';

      include('header/header.php');

        $s04 = mysqli_query($conn,"SELECT * FROM admin_login WHERE ADMIN_ID = '$admin_id'");
        while($row4 = mysqli_fetch_assoc($s04))
        {
          $name = $row4['ADMIN_NAME'];
        }

 ?>

 <style type="text/css">
   .count {
  
  animation: pulse 1s infinite;
}


@keyframes pulse {
  0% {
    -moz-box-shadow: 0 0 0 0 #ffd945;
    box-shadow: 0 0 0 0 #ffd945;
  }
  70% {
    -moz-box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 30px rgba(204, 169, 44, 0);
  }
  100% {
    -moz-box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
    box-shadow: 0 0 0 0 rgba(204, 169, 44, 0);
  }
}
 </style>

    <ol class="breadcrumb" style="padding-left: 8px;font-weight: bold;margin-top: 04%;margin-bottom: 0;">
      <li class="breadcrumb-item" style="font-size: 50px;"> <a href="dashboard.php">Dashboard</a></li>
      <li class="breadcrumb-item" data-toggle="tooltip" data-title="Payment Details"> Expenditure Details</li>
    </ol>


  <div class="col-md-12">
    
    <div class="row" style="padding-top: 10px;">
      <div class="col-md-12 bg-white" style="border:1px solid #cccc;height: auto;">
      
      <div class="col-md-12" style="border-bottom: 1px solid #cccc;">
        <div class="row">

            <div class="col-md-9" style="margin-bottom: 10px;"><h2><a href="dashboard.php" data-toggle="tooltip" data-title="Back"><i class="pg-icon" style="font-size: 22px;">chevron_left</i></a> Expenditure Details</h2></div>
            <div class="col-md-3" style="float: right;"></div>
        </div>

      </div>
          

<div class="card card-borderless">
<div class="col-md-12" style="padding-top:10px;">

<div class="tab-content">
<div class="tab-pane active" id="home">
<div class="row column-seperation">



<div class="col-lg-12">
  
<form action="../admin/query/insert.php" method="POST">
<div class="row" style="padding: 4px 0 15px 0;border-bottom: 1px solid #cccc;margin-bottom: 15px;">
  

  <div class="col-md-4">
    <label>Select Reason</label>
    <select class="form-control" name="reason" style="text-transform: capitalize;cursor: pointer;"required>
      <option value="">Select reason</option>

      <?php 

        $sql003 = mysqli_query($conn,"SELECT * FROM `reason`");
        while($row003 = mysqli_fetch_assoc($sql003))
        {
          $reason_id001 = $row003['REASON_ID'];
          $reason001 = $row003['REASON'];

          echo '<option value="'.$reason_id001.'">'.$reason001.'</option>';
        }

       ?>
    </select>
  </div>

  <div class="col-md-4">
    <label>Payment</label>
    <input class="form-control" name="payment" style="text-transform: capitalize;" placeholder="Payment" onclick="this.select();"required>
  </div>

  <div class="col-md-4">

    <label style="padding-top: 14px;"></label>
    <button type="submit" class="btn btn-success btn-block btn-lg" name="add_expenditure"><i class="pg-icon">tick</i> Submit</button>
  </div>

</div>
</form>

  <div class="row">
    <div class="col-md-8">

      <div class="pull-left">
      </div>

    </div>
    <div class="col-md-4">
        <input type="text" id="myInput" class="form-control pull-right" style="width: 100%;" placeholder="Search" data-toggle="tooltip" data-title="අවශ්‍ය දත්ත සෙවීම'">
      
  </div>

</div>


<div class="table-responsive" style="height: 550px;overflow: auto;margin-top: 4px;">
<table class="table demo-table-search table-responsive-block text-left" id="tableWithSearch">
<thead>
<tr>
<th>Pay Date</th>
<th class=" text-center">Reason</th>
<th class="text-right">Payment</th>
</tr>
</thead>
<tbody id="myTable">

<tr class="no-data alert alert-danger" style="margin-top: 20px;display: none;">
  <td colspan="7" class="text-danger"><span class="fa fa-warning"></span> Not Found Data</td>
</tr>

<?php 
  $total_fees = 0;

  $start = date('Y-m-01');

  $end = date('Y-m-t');

  $sql001 = mysqli_query($conn,"SELECT * FROM `expenditure` WHERE `ADD_DATE` BETWEEN '$start' AND '$end'");

  $check = mysqli_num_rows($sql001);

  if($check>0){
  while($row001 = mysqli_fetch_assoc($sql001))
  {
    $payment = $row001['PAYMENT'];
    $pay_date = $row001['ADD_D_T'];
    $reason_id = $row001['REASON_ID'];
    $exp_id = $row001['EXP_ID'];

    $sql002 = mysqli_query($conn,"SELECT * FROM `reason` WHERE `REASON_ID` = '$reason_id'");
    while($row002 = mysqli_fetch_assoc($sql002))
    {
      $reason = $row002['REASON'];
    }

    echo '

      <tr>
        <td class="v-align-middle">'.$pay_date.'</td>
        <td class="v-align-middle text-center" style="text-transform:capitalize;">'.$reason.'</td>
        <td class="v-align-middle text-right">'.number_format($payment,2).'';?>

         <a href="../admin/query/delete.php?exp_delete_id=<?php echo $exp_id; ?>" onclick="return confirm('Are you sure Delete?')" class="text-danger"><span class="fa fa-times"></span></a>
  <?php  echo '</td>
      </tr>';

      $total_fees = $total_fees+$payment;

  }
}else
if($check == '0')
{
  echo '<tr><td colspan="7" class="text-center text-danger"><span class="fa fa-warning"></span> Not Found Data</td></tr>';
}

 ?>
</tbody>
<tr>
  <td colspan="2" style="font-weight: bold;font-size: 16px;">Total Expenditure</td>
  <td class="text-right" style="font-weight: bold;font-size: 16px;">LKR <?php echo number_format($total_fees,2); ?></td>
</tr>
</table>
</div>

              
</div>
</div>
</div>
</div>




</div>
</div>



      </div>
    </div>


  </div>





<script type="text/javascript">
  $('.save_btn').click(function(){
    
    var upload_btn0 = $('#selectedFile0').val();
    var upload_btn = $('#sel_file').val();
      
    if(empty(upload_btn0))
    {
      alert('Please select your audio file.');
    }else
    if(empty(upload_btn))
    {
      alert('Please select your document file.');
    }
   });
</script>

<script type="text/javascript">
  $('.navi').click(function(){
    $('.frm')[0].reset();
      
   });
</script>

<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
<!-- search data -->

<!-- no data message/no found data show in search-->

<script type="text/javascript">
  $(document).ready(function () {

    (function ($) {

        $('#myInput').keyup(function () {
            var rex = new RegExp($(this).val(), 'i');
            $('#myTable tr').hide();
            $('#myTable tr').filter(function () {
                return rex.test($(this).text());
            }).show();
            $('.no-data').hide();
            if($('#myTable tr:visible').length == 0)
            {
                $('.no-data').show();
            }

        })

    }(jQuery));

});
</script>




<?php include('footer/footer.php'); ?>